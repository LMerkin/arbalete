% vim:ts=2:syntax=matlab

%=============================================================================%
% Range-Based Variance Estimator (Rogers-Satchell):                           %
%=============================================================================%
function [var, vol] = RogersSatchell(M, tau)
  sz = size(M);
  NR = sz(1);
  NC = sz(2);
  assert(NR > 0 && NC == 9);

  % Over-all Open:
  O  = M(1,  2);
  % Over-All Close:
  C  = M(NR, 3);
  % Over-All High:
  H  = max(M(:, 4));
  % Over-All Low:
  L  = min(M(:, 5));
  assert(L <= O && L <= C && L <= H && O <= H && C <= H);

  % Rogers-Satchell Variance Estimator (over the interval of M):
  var = log(H/O) * log(H/C) + log(L/O) * log(L/C);
  assert(var >= 0);

  % "tau" is originally in milliseconds, and is larger than the diff if Time-
  % Stamps of "M", because it also includes the last interval:
  assert(tau > 0 && tau > M(NR,1) - M(1,1));

  % Get the interval in Years, assuming 1 year = 365.25 days = 31557600 sec:
  tau = tau / 31557600000;

  % Variance and volatility per year:
  var = var / tau;
  vol = sqrt(var);
endfunction;
