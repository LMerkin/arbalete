% vim:ts=2:syntax=matlab

% TimeStep in nominal (1-min) intervals:
tauN    = 60;

% Vol threshold which is a Jump indicator:
JumpThr = 4.5;

% Orig Matrix
% (1): TSmsec
% (2): Open
% (3): Close
% (4): High
% (5): Low
% (6): Vlm
M  = load("BTC-USD.csv");
NR = size(M)(1);

% Attach extra cols to the matrix:
% (7) StocVar
% (8) StocVol
% (9) VolSpikeDueToJump
M = [M, zeros(NR,3)];

% TimeStep (1h) in msec:
tau  = tauN * 60000;

% Separately, StocVols will be stored here:
Vols = [];

% Select intervals of length "tau" (or nearest larger ones):
i = 1;
while 1
  % Curr TimeStamp:
  ts = M(i,1);

  % Estimate the beginning of the next interval:
  i1  = i + tauN;

  % Stop now if it is beyond the matrix end:
  if (i1 > NR)
    break;
  end;

  % Get the next TimeStamp:
  ts1 = M(i1,1);

  % If the estimation is correct, the next TimeStamp must be equal to (ts+tau);
  % in reality, it could be larger, but never smaller:
  tsE  = ts  +  tau;
  assert(ts1 >= tsE)

  while M(i1-1,1) >= tsE
    % Move i1 backwards:
    i1  = i1 - 1;
    ts1 = M(i1,1);
  end;

  % Found the right i1: the smallest such as ts1 >= tsE still:
  assert(i1 > i && ts1 >= tsE);

  % The actual time interval:
  tauAct = ts1 - ts;
  assert(tauAct >= tau);

  % Estimate the Var and Vol over the hourRange:
  hourRange  = M(i:(i1-1), :);
  [var, vol] = RogersSatchell(hourRange, tauAct);

  Vols = [Vols; vol];

  % This "var" and "vol" apply to all candles in the specified range:
  M(i:(i1-1),7) = var;
  M(i:(i1-1),8) = vol;

  % Next iteration:
  i = i1;
end;

% Post-Processing: Detect Jumps by Vol Spikes:
meanVol   = mean(Vols);
NV        = size(Vols)(1);
VolSpikes = zeros(NV, 1);

for i = 1:NV
  if Vols(i) > meanVol * JumpThr
    VolSpikes(i) = Vols(i);
    Vols(i)      = Vols(i-1);
  end;
end;

% Similar in M: Collect the statistics of Jumps in the underlying:
% Jumps:
% (1) ArrivalTS
% (2) Magnitude (rel to S-)
Jumps  = [];
inJump = 0;

for i = 1:NR
  if M(i,8) > meanVol * JumpThr
    % If we are not in Jump mode yet, set this mode and record the initial "Sm":
    if !inJump
      inJump = 1;
      TSm    = M(i,1);
      Sm     = M(i,2);  % Open
    end;
    % Then in any case:
    M(i,9) = M(i,  8);  % VolSpike
    M(i,8) = M(i-1,8);  % Prev Vol
    M(i,7) = M(i-1,7);  % Prev Var
  else
    if inJump
      % Exiting the Jump mode:
      inJump = 0;
      TSp    = M(i,1);
      Sp     = M(i,2);  % Open
      Jumps  = [Jumps, [TSm; TSp; Sm; Sp]];
    end;
  end;
end;

% Save the results:
save("-binary", "M9.mat",   "M")
save("-binary", "Vols.mat", "Vols", "VolSpikes", "Jumps")
