// vim:ts=2:et
//===========================================================================//
//                                 "RNG.cpp":                                //
//                 Pre-Defined Instances of the "RNG" Class:                 //
//===========================================================================//
#include "MonteCarlo/RNG.hpp"

namespace Arbalete
{
  template class RNG<double>;
  template class RNG<float>;
}
