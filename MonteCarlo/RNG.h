// vim:ts=2:et
//===========================================================================//
//                                  "RNG.h":                                 //
//       Random Number Generators for Host- and CUDA-Based Monte Carlo       //
//===========================================================================//
#pragma once

#include "Common/Macros.h"
#include "Common/CUDAEnv.h"
#ifndef  __CUDACC__
#include <memory>
#include <random>
#include <functional>
#endif

namespace Arbalete
{
  //=========================================================================//
  // "RNGSetup":                                                             //
  //=========================================================================//
  class RNGSetup
  {
  public:
    // Method of Generating Uniformly Distributed Pseudo-Random Integers. In
    // Host mode,  only one (Default) "UniMethod" and one "NormalMethod" are
    // currently available, because boost::random is used. In CUDA mode, the
    // user has a wider selection of methods. XXX: TODO: make them available
    // in Host mode as well:
    //
    enum UniMethod
    {
      XORWOW    = 0,    // XXX: Currently, for CUDA only
      MRG32K3A  = 1     // XXX: Currently, for CUDA only
    };

    // Method of Generating Normal(0,1)-Distributed Pseudo-Random Numbers:
    enum NormalMethod
    {
      BoxMuller = 0,    // Default
      ICDF      = 1     // XXX: Currently, for CUDA only
    };

    // Size of "RNGInits":
    static int const RNGInitsSz = 30720 * 16;
  };

  //=========================================================================//
  // "RNG" Class:                                                            //
  //=========================================================================//
  template<typename F>
  class RNG
  {
  public:
    //-----------------------------------------------------------------------//
    // The "Core" class: Suitable for both Host and CUDA:                    //
    //-----------------------------------------------------------------------//
    // All ptrs are aliases. No Dtor is provided; auto-generated Copy Ctor is
    // used. No Default Ctor is provided: Objects of the "Core" class are cre-
    // ated by the outer "RNG" class:
    //
    class Core
    {
    private:
      //---------------------------------------------------------------------//
      // Data Flds:                                                          //
      //---------------------------------------------------------------------//
      friend class RNG;

      int                     m_maxThrds;     // #CUDA or Host Threads (States)
      RNGSetup::UniMethod     m_uMethod;
      RNGSetup::NormalMethod  m_nMethod;
      unsigned int*           m_rngInits;     // Inits for all Threads
      bool*                   m_cudaInited;   // Flags for all Threads
      void*                   m_states;       // CUDA or Host States
      void*                   m_geners;       // Host Geners
      mutable F               m_nextNormal;
      mutable bool            m_hasNext;

    public:
      //---------------------------------------------------------------------//
      // Normal(0,1) Generator: Both CUDA and Host modes:                    //
      //---------------------------------------------------------------------//
      DEVICE F Normal01();
    };

#   ifndef __CUDACC__
  private:
    //-----------------------------------------------------------------------//
    // Data Flds:                                                            //
    //-----------------------------------------------------------------------//
    // CUDA-related Flds:
    int                       m_maxCUDAThrds; // # of CUDA States to be created
    RNGSetup::UniMethod       m_uMethod;
    RNGSetup::NormalMethod    m_nMethod;
    unsigned int*             m_rngInits;     // Flags  for all Inits
    bool*                     m_cudaInited;   // CUDA state fully initialised?
    void*                     m_cudaStates;   // CUDA or Host States
    mutable F                 m_nextNormal;   // For the Box-Muller method
    mutable bool              m_hasNext;      //  in CUDA...

    // Host-related Flds:
    int                       m_maxHostThrds; // # of Host States to be created
    void*                     m_hostStates;
    std::function<F()>**      m_geners;
    std::shared_ptr<CUDAEnv>  m_cudaEnv;      // Copy of CUDAEnv, for mem mgmt

    // Default and Copy Ctor are always hidden:
    RNG();
    RNG(const RNG&);

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor, Dtor: Host mode only:                               //
    //-----------------------------------------------------------------------//
    RNG
    (
      std::shared_ptr<CUDAEnv> const& cudaEnv,
      RNGSetup::UniMethod             u_method = RNGSetup::UniMethod(0),
      RNGSetup::NormalMethod          n_method = RNGSetup::NormalMethod(0)
    );

    ~RNG();

    //-----------------------------------------------------------------------//
    // Accessors:                                                            //
    //-----------------------------------------------------------------------//
    CUDAEnv const* GetCUDAEnv() const { return m_cudaEnv.get(); }

    Core  GetCore()    const;

#   endif // !__CUDACC__
  };
}
