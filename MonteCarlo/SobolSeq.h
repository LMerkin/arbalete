// vim:ts=2:et
//===========================================================================//
//                                "SobolSeq.h":                              //
//              Sobol' Low-Discrepancy-Sequence (LDS) Generation             //
//===========================================================================//
#pragma once

#include "Common/Macros.h"
#ifndef  __CUDACC__
#include <vector>
#include <cassert>
#endif
#include <cstdint>

namespace Arbalete
{
  //=========================================================================//
  // "SobolSeq" Class:                                                       //
  //=========================================================================//
  // XXX: TODO: This class is CUDA-ready (eg no STL) but not CUDA-enabled yet:
  //
  class SobolSeq
  {
  private:
    //-----------------------------------------------------------------------//
    // Data Flds:                                                            //
    //-----------------------------------------------------------------------//
    int               m_sdim;   // Dimension of sequence being generated
    uint32_t*         m_mdata;  // Array of length (32 * m_sdim)
    uint32_t*         m_m[32];  // More convenient ptrs to m_mdata, of dir #s
    mutable uint32_t* m_x;      // Previous x = x_n, array of length m_sdim
    mutable int*      m_b;      // Pos of fixed point in x[i] is after bit b[i]
    mutable uint32_t  m_n;      // Number of x's generated so far

    static int const  s_MAXDIM = 1111;
    static int const  s_MAXDEG =   12;

    // Default Ctor is hidden:
    SobolSeq();

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor, Dtor, Accessors:                                    //
    //-----------------------------------------------------------------------//
#   ifndef __CUDACC__
    SobolSeq(int sdim);

    ~SobolSeq();
#   endif

    constexpr static int GetMaxDim() { return s_MAXDIM; }
    constexpr static int GetMaxDeg() { return s_MAXDEG; }

    //-----------------------------------------------------------------------//
    // Generators:                                                           //
    //-----------------------------------------------------------------------//
    // "xs" must be an array or vector of size "sdim":
    //
    // Output is scaled between 0 and 1:
    DEVICE void Next01(double* xs) const;

    // Output is scaled between lb[i] and ub[i]:
    DEVICE void NextUB(double const* lb, double const* ub, double* xs) const;

    // If we know in advance how many points (n) we want to compute, then adopt
    // the suggestion of the Joe and Kuo paper, which in turn is taken from Ac-
    // worth et al (1998), of skipping a number of points equal to the largest
    // power of 2 smaller than "n":
    DEVICE void Skip(int n, double* xs) const;

#   ifndef __CUDACC__
    // Vector versions of the above methods:
    //
    void Next01(std::vector<double>* xs) const;

    void NextUB
    (
      std::vector<double> const& lb, std::vector<double>const& ub,
      std::vector<double>* xs
    )
    const;

    void Skip (int n, std::vector<double>* xs) const;
#   endif
  };
}
