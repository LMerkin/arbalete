// vim:ts=2:et
//===========================================================================//
//                           "PortfolioPaths.cu":                            //
//                CUDA Kernels for Portfolio Paths Analysis                  //
//===========================================================================//
#include "MonteCarlo/PortfolioPaths_Core.hpp"

namespace Arbalete
{
  template struct PortfolioPaths_KernelInvocator<double>;
  template struct PortfolioPaths_KernelInvocator<float>;
}
