// vim:ts=2:et
//===========================================================================//
//                                  "RNG.hpp":                               //
//       Random Number Generators for Host- and CUDA-Based Monte Carlo       //
//===========================================================================//
#pragma once

#include "Common/CUDAEnv.hpp"
#include "MonteCarlo/RNG.h"
#ifndef  __CUDACC__
#include <random>
#include <stdexcept>
#ifndef __clang__
#include <omp.h>
#endif
#endif

#if defined(__clang__) && !defined(__CUDACC__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-function"
#endif

#if defined(__GNUC__)  && !defined(__CUDACC__) && !defined(__INTEL_COMPILER)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored   "-Wstrict-aliasing"
#endif

#include <curand_kernel.h>

#if defined(__clang__) && !defined(__CUDACC__)
#pragma clang diagnostic pop
#endif

#if defined(__GNUC__)  && !defined(__CUDACC__) && !defined(__INTEL_COMPILER)
#pragma GCC diagnostic pop
#endif

namespace Arbalete
{
  //=========================================================================//
  // RNG Initiliser / State Data:                                            //
  //=========================================================================//
# ifndef __CUDACC__
  using namespace std;
  // Host-space data from which all per-thread states will be initialised:
  extern unsigned int const RNGInits[RNGSetup::RNGInitsSz];
# endif

  //=========================================================================//
  // CUDA RNG States:                                                        //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // "XORWOW":                                                               //
  //-------------------------------------------------------------------------//
  class XORWOW
  {
  public:
    typedef curandStateXORWOW_t StateT;

    DEVICE static void Init
      (int k, unsigned int const* rndInits, StateT* state)
    {
      unsigned int const* init_k = rndInits + 2 * k;
      curand_init
        (*((unsigned long long int const*)(init_k)), 0UL, 0UL, state);
    }
  };

  //-------------------------------------------------------------------------//
  // "MRG32K3A":                                                             //
  //-------------------------------------------------------------------------//
  class MRG32K3A
  {
  public:
    typedef curandStateMRG32k3a_t StateT;

    DEVICE static void Init
      (int k, unsigned int const* rndInits, StateT* state)
    {
      unsigned int const* init_k = rndInits + 2 * k;
      curand_init
        (*((unsigned long long int const*)(init_k)), 0UL, 0UL, state);
    }
  };

  //=========================================================================//
  // "CUDA_Pair":                                                            //
  //=========================================================================//
  template<typename F>
  class CUDA_Pair;

  template<>
  class CUDA_Pair<double>
  {
  public:
    typedef double2 F2;
  };

  template<>
  class CUDA_Pair<float>
  {
  public:
    typedef float2 F2;
  };

  //=========================================================================//
  // "CUDA_RNG":                                                             //
  //=========================================================================//
  template<typename F, typename UniRNG>
  class CUDA_RNG;

  template<typename UniRNG>
  class CUDA_RNG<double, UniRNG>
  {
  public:
    DEVICE static double2 Normal01_BoxMuller(typename UniRNG::StateT* state)
      { return curand_box_muller_double<typename UniRNG::StateT>(state); }

    DEVICE static double  Normal01_ICDF     (typename UniRNG::StateT* state)
      { return _curand_normal_icdf_double(curand(state)); }
  };

  template<typename UniRNG>
  class CUDA_RNG<float, UniRNG>
  {
  public:
    DEVICE static float2  Normal01_BoxMuller(typename UniRNG::StateT* state)
      { return curand_box_muller<typename UniRNG::StateT>(state); }

    DEVICE static float   Normal01_ICDF     (typename UniRNG::StateT* state)
      { return _curand_normal_icdf(curand(state)); }
  };

# ifndef __CUDACC__
  //=========================================================================//
  // "RNG" Non-Default Ctor (Host Space Only):                               //
  //=========================================================================//
  template<typename F>
  RNG<F>::RNG
  (
    shared_ptr<CUDAEnv> const& cudaEnv,    // May contain NULL
    RNGSetup::UniMethod        u_method,
    RNGSetup::NormalMethod     n_method
  )
  : m_maxCUDAThrds(0),
    m_uMethod     (u_method),
    m_nMethod     (n_method),
    m_rngInits    (nullptr),
    m_cudaInited  (nullptr),
    m_cudaStates  (nullptr),
    m_nextNormal  (NaN<F>()),
    m_hasNext     (false),
    m_maxHostThrds(0),
    m_hostStates  (nullptr),
    m_cudaEnv     (cudaEnv)
  {
    bool useCUDA = (m_cudaEnv.get() != nullptr);

    // XXX: If CUDA is not used, "m_uMethod" and "m_nMethod" on Core have no
    // effect, but we do not regard that condition as an error.

    // Get the maximum number of CUDA and Host (OpenMP) threads -- each of
    // them will need to use their own RNG initialisers and states:
    //
    m_maxCUDAThrds = useCUDA ? m_cudaEnv->MaxThreads() : 0;
#   ifndef __clang__
    m_maxHostThrds = omp_get_max_threads();
#   else
    m_maxHostThrds = 1;   // No OpenMP on Mac OS X yet
#   endif

    // Check if we got enough RNG Inits for the selected Methods and a given
    // number of threads. In CUDA space, 2 32-bit inits per thread/state are
    // required; in Host space, only 1;  XXX:  it is OK if same initialisers
    // are shared between CUDA and Host threads:
    //
    int cudaInitsSz = m_maxCUDAThrds * 2;
    int hostInitsSz = m_maxHostThrds * 1;
    int maxInitsSz  = max<int>(cudaInitsSz, hostInitsSz);

    if (maxInitsSz > RNGSetup::RNGInitsSz)
      // The following error is very unlikely given the size of "RNGInits":
      throw invalid_argument("RNG Ctor: Too many Threads, too few Inits");

    if (useCUDA)
    {
      // CUDA Mode:
      // Put the required number of initialisers into the CUDA space:
      //
      m_rngInits   = cudaEnv->CUDAAlloc<unsigned int>(cudaInitsSz);
      cudaEnv->ToCUDA<unsigned int>(RNGInits, m_rngInits, cudaInitsSz);

      // Set all "m_cudaInited" flags to "false":
      //
      m_cudaInited = cudaEnv->CUDAAlloc<bool> (cudaInitsSz);
      cudaEnv->CUDAZeroOut<bool>(m_cudaInited, cudaInitsSz);

      // Allocate States in CUDA space (device memory). The allocated States
      // remain uninitialised -- initialisation can only be done from within
      // the CUDA code:
      //
      switch (m_uMethod)
      {
        case RNGSetup::XORWOW:
          m_cudaStates =
            cudaEnv->CUDAAlloc<XORWOW::StateT>  (m_maxCUDAThrds);
          break;
        case RNGSetup::MRG32K3A:
          m_cudaStates =
            cudaEnv->CUDAAlloc<MRG32K3A::StateT>(m_maxCUDAThrds);
          break;
        default:
         assert(false);
      }
    }
    // Host Mode -- ALWAYS AWAILABLE (i.e. can be used in together  with CUDA
    // mode: the former runs from Host code, the latter - from CUDA code), on
    // the same RNG object:
    //
    // Use "RNGInits" directly, without copying:

    // Allocate States for the MT19927 Generator, and the Generators proper:
    //
    mt19937* states = new mt19937[m_maxHostThrds];
    m_hostStates    = states;
    m_geners        = new function<F()>*[m_maxHostThrds];

    for (int k = 0; k < m_maxHostThrds; ++k)
    {
      (states + k)->seed(RNGInits[k]);
      m_geners[k]   = new function<F()>
                          (bind(normal_distribution<F>(), states[k]));
    }
  }

  //=========================================================================//
  // Dtor:                                                                   //
  //=========================================================================//
  template<typename F>
  RNG<F>::~RNG()
  {
    if (m_cudaEnv.get() != nullptr)
    {
      // CUDA Mode: De-allocate the States in the CUDA space:
      //
      assert (m_cudaStates != nullptr);
      switch (m_uMethod)
      {
        case RNGSetup::XORWOW:
        {
          XORWOW::StateT*   states = (XORWOW::StateT*)(m_cudaStates);
          m_cudaEnv->CUDAFree<XORWOW::StateT>(states);
          break;
        }
        case RNGSetup::MRG32K3A:
        {
          MRG32K3A::StateT* states = (MRG32K3A::StateT*)(m_cudaStates);
          m_cudaEnv->CUDAFree<MRG32K3A::StateT>(states);
          break;
        }
        default:
          assert(false);
      }

      // Also, need to de-allocate "m_rngInits" and "m_cudaInited":
      assert(m_rngInits != nullptr &&  m_cudaInited != nullptr);

      m_cudaEnv->CUDAFree<unsigned int>(m_rngInits);
      m_cudaEnv->CUDAFree<bool>(m_cudaInited);
    }
    else
    {
      // Host Mode: De-allocate the States and Generators in the Host space:
      //
      assert(m_hostStates != nullptr);
      mt19937* states = (mt19937*)(m_hostStates);
      delete[] states;

      for (int k = 0; k < m_maxHostThrds; ++k)
      {
        assert(m_geners[k] != nullptr);
        delete m_geners[k];
      }
      delete[] m_geners;

      // NB: In the Host mode, "m_rngInits" is not used:
      assert(m_rngInits == nullptr);
    }
  }
# endif // !__CUDACC__

  //=========================================================================//
  // Normal(0,1) Generator: CUDA mode:                                       //
  //=========================================================================//
# ifdef __CUDACC__
  template<typename F>
  DEVICE F RNG<F>::Core::Normal01()
  {
    //-----------------------------------------------------------------------//
    // State Initialisation (if necessary):                                  //
    //-----------------------------------------------------------------------//
    // Get the Thread#:
    int k = blockDim.x * blockIdx.x + threadIdx.x;

    if (!m_cudaInited[k])
    {
      // Initialise the State corresponding to this Thread before proceeding:
      switch (m_uMethod)
      {
        case RNGSetup::XORWOW:
        {
          XORWOW::StateT* states = (XORWOW::StateT*)(m_states);
          XORWOW::Init(k, m_rngInits, states + k);
          break;
        }
        case RNGSetup::MRG32K3A:
        {
          MRG32K3A::StateT* states = (MRG32K3A::StateT*)(m_states);
          MRG32K3A::Init(k, m_rngInits, states + k);
          break;
        }
        default:
          assert(false);
      }
      m_cudaInited[k] = true;
    }

    //-----------------------------------------------------------------------//
    // Std Normal Variate Generation:                                        //
    //-----------------------------------------------------------------------//
    if (m_nMethod == RNGSetup::BoxMuller)
    {
      //---------------------------------------------------------------------//
      // Box-Muller Method:                                                  //
      //---------------------------------------------------------------------//
      // With this method, 2 normal variates are computed at a time, so one
      // could already be available:
      if (m_hasNext)
      {
        m_hasNext = false;
        return m_nextNormal;
      };

      // OTHERISE: Perform actual new generation:
      typename CUDA_Pair<F>::F2 res2;

      switch (m_uMethod)
      {
        case RNGSetup::XORWOW:
        {
          XORWOW::StateT* states = (XORWOW::StateT*)(m_states);
          res2 = CUDA_RNG<F, XORWOW>::Normal01_BoxMuller(states + k);
          break;
        }
        case RNGSetup::MRG32K3A:
        {
          MRG32K3A::StateT* states = (MRG32K3A::StateT*)(m_states);
          res2 = CUDA_RNG<F, MRG32K3A>::Normal01_BoxMuller(states + k);
          break;
        }
        default:
          assert(false);
      }
      // Return one normal straight away, save the other one for the next
      // call:
      F res        = res2.x;
      m_nextNormal = res2.y;
      m_hasNext    = true;
      return res;
    }
    else
    {
      //---------------------------------------------------------------------//
      // ICDF Method:                                                        //
      //---------------------------------------------------------------------//
      // With this method, only 1 normal is produced at a time:
      assert(m_nMethod == RNGSetup::ICDF);

      switch (m_uMethod)
      {
        case RNGSetup::XORWOW:
        {
          XORWOW::StateT* states = (XORWOW::StateT*)(m_states);
          return CUDA_RNG<F, XORWOW>::Normal01_ICDF(states + k);
        }
        case RNGSetup::MRG32K3A:
        {
          MRG32K3A::StateT* states = (MRG32K3A::StateT*)(m_states);
          return CUDA_RNG<F, MRG32K3A>::Normal01_ICDF(states + k);
        }
        default:
          assert(false);
          return NaN<F>();
      }
    }
  }
# else
  //=========================================================================//
  // Normal(0,1) Generator: Host mode:                                       //
  //=========================================================================//
  // Use the Default method (actually Box-Muller):
  //
  template<typename F>
  F RNG<F>::Core::Normal01()
  {
    // Get the current thread number:
#   ifndef __clang__
    int k = omp_get_thread_num();
    assert(0 <= k && k < m_maxThrds);
#   else
    int k = 0;
    assert(m_maxThrds == 1);
#   endif

    assert(m_geners != nullptr);
    function<F()>* gener = ((function<F()>**)(m_geners))[k];

    assert(gener != nullptr);
    return (*gener)();
  }

  //=========================================================================//
  // "GetCore":                                                              //
  //=========================================================================//
  template<typename F>
  typename RNG<F>::Core RNG<F>::GetCore() const
  {
    Core res;

    if (m_cudaEnv.get() != nullptr)
    {
      res.m_maxThrds = this->m_maxCUDAThrds;
      res.m_states   = this->m_cudaStates;
    }
    else
    {
      res.m_maxThrds = this->m_maxHostThrds;
      res.m_states   = this->m_hostStates;
    }

    // The following flds are used only by CUDA, but we fill them in anyway:
    res.m_uMethod    = this->m_uMethod;
    res.m_nMethod    = this->m_nMethod;
    res.m_rngInits   = this->m_rngInits;
    res.m_cudaInited = this->m_cudaInited;

    // The following fld(s) are for Host only:
    res.m_geners     = this->m_geners;

    // Reset the buffer of Normal Variates:
    res.m_nextNormal = NaN<F>();
    res.m_hasNext    = false;

    return res;
  }
# endif // __CUDACC__
}
