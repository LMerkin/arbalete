// vim:ts=2:et
//===========================================================================//
//                           "PortfolioPath.hpp":                            //
//           Evaluation of Portfolio PnL along an Undetlying Path            //
//                  (Monte Carlo-Generated or Observed)                      //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Common/CUDAEnv.hpp"
#include "Common/TradingCalendar.h"
#ifndef  __CUDACC__
#include <stdexcept>
#endif

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "PortfolioPath_Core":                                                   //
  //=========================================================================//
  // Returns the portfolio P&L at the end (when the pay-off is evaluated), as
  // well as the MIN cash on the account  (i.e. the max drawdown, MDD):
  //
  template<typename F>
  DEVICE static void PortfolioPath_Core
  (
    int                     L,
    TimeNode<F> const*      timeLine,    // len = L
    F const*                S,           // len = L
    F                       K,           // Strike
    bool                    isCall,      // Call or Put?
    bool                    isVol,       // true: V is vol; false: V is var
    F const*                V,           // Vols or vars, len = L
    int                     oQty,        // Options qty (+ or -)
    F                       oPx,         // Initial option price
    F                       dF,          // Futures price step
    F                       r,           // Interest rate
    F*                      PnL,         // len = 1
    F*                      MDD          // len = 1
  )
  {
    assert(S != nullptr && V != nullptr && PnL != nullptr && MDD != nullptr);

    // Delta: initially 0:
    int delta = 0;
    F   Fprev = F(0.0);

    // Cash: initially, the inverted option cost (i.e., cash < 0 if oQty > 0):
    F  cash   = - F(oQty) * oPx;
    F  mdd    = F(0.0);

    // Margin account (cash to be set aside):
    F margin  = F(0.0);

    // Go through the TimeLine:
    //
    for (int i = 0; i < L - 1; ++i)
    {
      // NB: Delta-hedging is only done during the trading hours; however, by
      // "timeLine" construction, each instant is either within trading hours,
      // or on the boundary, so we can always re-hedge:
      // Compute the size of the delta hedge using the BSM formula, but the
      // stochastic (in practice estimated) volatility:
      //
      F   sigma = isVol ? V[i] : SqRt(V[i]);
#     ifndef __CUDACC__
      if (sigma <= F(0.0) || !IsFinite(sigma))
        throw runtime_error("PortfolioPath["+ to_string(i) +": Invalid sigma");
#     endif

      // Time to option expiration:
      F  tau = timeLine[L-1].m_ty - timeLine[i].m_ty;
      F stau = sigma * SqRt(tau);
      F rtau = r     * tau;
      assert(stau > F(0.0) && IsFinite(stau));

      // Option Moneyness:
      assert(S[i] > F(0.0) && K > F(0.0));
      F  x = Log(S[i] / K);

      // Theoretical BSM delta.   XXX: We currently assume no dividends; the
      // problem is that  we are using a risk-neutal delta formula  in a NON-
      // risk-neutral setup!
      //
      F bsmDelta = NCDF((x + rtau) / stau + stau / F(2.0));
      if (!isCall)
        bsmDelta -= F(1.0);

      // We assume that we hedge with futures, so the delta needs to be multi-
      // plied by the discount factor since it will actually be applied to the
      // futures price:
      //
      F DF      = Exp(- rtau);
      bsmDelta *= DF;

      // Integralise it, taking into account the Options Qty. The sign of delta
      // is opposite to that of Options Qty; "newDelta" is our algebraic positn
      // in the underlying:
      //
      int newDelta = - int(Round(bsmDelta * F(oQty)));

      // Re-hedge, and adjust the cash. We hedge with futures, so adjust the
      // price:
      F Fi = S[i] / DF;
      if (dF > F(0.0))
        Fi = dF * Round(Fi / dF);

      // Cash adjustment comes in 2 parts:
      // (1) adjusting the margin on the futures account, still using old
      //     delta:
      cash += F(delta) * (Fi - Fprev);

      // (2) adjusting the delta itself is "free" (it's a futures) except for
      //     the margin requirements:
      delta = newDelta;
      Fprev = Fi;

      // XXX: Margin calculations presented here are for illustrative point
      // only. They need to be made completely user-configurable:
      // (1) On futures, there is afixed rate per contract:
      margin = F(189.30) * Abs(F(delta));

      // (2) On options, it depends on the position -- only short positions are
      // margined, for simplicity we assume 2.75% of the underlying's price
      // plus the option price, which is just estimated:
      if (oQty < 0)
      {
        F currOptPx = F(0.4) * stau;        // For ATM and small r, Call =~ Put
        margin     += Abs(F(oQty)) * (F(0.0275) + currOptPx) * S[i];
      }
      F margCash = cash - margin;

      // Adjust the MDD:
      if (margCash < mdd)
        mdd = margCash;
    }
    // At the option expiration time, evaluate the pay-off and liquidate the
    // hedge. Then the total P&L is:
    //
    F payOff = isCall ? Max(S[L-1] - K, F(0.0)) : Max(K - S[L-1], F(0.0));
    cash    += F(delta) *  (S[L-1] - Fprev);

    // The final cash adjustment on the futures -- then we close the futures
    // position for free:
    *PnL     = F(oQty) * payOff + cash;
    *MDD     = mdd;
  }

  //=========================================================================//
  // "PortfolioPaths_Core":                                                  //
  //=========================================================================//
  // Running "PortfolioPath_Core" for multiple paths in parallel:
  //
  template<typename F>
  GLOBAL static void PortfolioPaths_Core
  (
    CUDAEnv::Sched1D const* CUDACC_OR_DEBUG(sched),
    int                     L,           // Length of each Path
    int                     P,           // Number of Paths
    TimeNode<F> const*      timeLine,    // len =  L
    F const*                S,           // len = (L * P)
    F                       K,           // Strike
    bool                    isCall,      // Call or Put?
    bool                    isVol,       // true: V is vol; false: V is var
    F const*                V,           // Vols or vars, len = (L * P)
    int                     oQty,        // Options qty (+ or -)
    F                       oPx,         // Initial option price
    F                       dF,          // Futures price step
    F                       r,           // Interest rate
    F*                      PnL,         // len = P
    F*                      MDD          // len = P
  )
  {
#   ifdef __CUDACC__
    // Get the info from "sched" for the curr global Thread Number:
    assert(sched != nullptr);
    int Tn = blockDim.x * blockIdx.x + threadIdx.x;
    int jFrom = sched[Tn].m_from;
    int jTo   = sched[Tn].m_to;
    assert(0 <= jFrom && jTo <= P-1);

#   else
    // Host-based computation:
    assert(sched == nullptr);
    int jFrom = 0;
    int jTo   = P-1;

#   ifndef __clang__
#   pragma omp parallel for
#   endif
#   endif
    for (int j = jFrom; j <= jTo; ++j)
    {
      // Analyse this Path:
      PortfolioPath_Core<F>
      (
        L, timeLine, S + L*j, K, isCall, isVol, V + L*j, oQty, oPx, dF, r,
        PnL + j,     MDD + j
      );
    }
  }

  //=========================================================================//
  // "PortfolioPaths_KernelInvocator":                                       //
  //=========================================================================//
  template<typename F>
  struct PortfolioPaths_KernelInvocator
  {
    static void Run
    (
      CUDAEnv const&          cudaEnv,
      int                     L,           // Length of each Path
      int                     P,           // Number of Paths
      TimeNode<F> const*      timeLine,    // len =  L
      F const*                S,           // len = (L * P)
      F                       K,           // Strike
      bool                    isCall,      // Call or Put?
      bool                    isVol,       // true: V is vol; false: V is var
      F const*                V,           // Vols or vars, len = (L * P)
      int                     oQty,        // Options qty (+ or -)
      F                       oPx,         // Initial option price
      F                       dF,          // Futures price step
      F                       r,           // Interest rate
      F*                      PnL,         // len = P
      F*                      MDD          // len = P
    )
#   ifdef __CUDACC__
    {
      // NB: Here the Schedule is constructed INSIDE "Run"  (in other Kernel
      // Invocators, it is usually constructed outside, so it can be re-used,
      // e.g. in multiple integration steps). The Schedule is saved right in
      // CUDA space:
      //
      CUDAEnv::Sched1D* sched = cudaEnv.MkSched1D(P);

      // Set the CUDA device:
      CUDAEnv::Lock();
      cudaEnv.SetCUDADevice();

      // Invoke the kernel:
      PortfolioPaths_Core<F>
        <<<cudaEnv.NThreadBlocks(), cudaEnv.ThreadBlockSize(), 0,
           cudaEnv.Stream()
        >>>
        (sched, L, P, timeLine, S, K, isCall, isVol, V, oQty, oPx, dF, r,
         PnL,   MDD);

      CUDAEnv::Unlock();
      CUDA_CHECK_STATUS("PortfolioPaths Kernel invocation failed")
    }
#   else
    ; // Spec only
#   endif
  };
}
