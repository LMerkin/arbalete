# Arbalete

A framework for Option Risks and Volatility Arbitrage Strategies.

* Models: 1D and 2D diffusions (in particular LocalStocVol models).

* Mathematical methods: Fokker-Planck PDE and Monte-Carlo methods.

* Parallel computing:   Multi-core CPUs and NVIDIA CUDA GPUs.

* History: Based on the original developments of 2011--2013. Resumed in 2020.
