      DOUBLE PRECISION FUNCTION F06BMF( SCALE, SSQ )                            
C     MARK 12 RELEASE. NAG COPYRIGHT 1986.                                      
C     MARK 18 REVISED (THREAD SAFETY). (SEP 1996).                              
C     .. Scalar Arguments ..                                                    
      DOUBLE PRECISION                  SCALE, SSQ                              
C     ..                                                                        
C                                                                               
C  F06BMF returns the value norm given by                                       
C                                                                               
C     norm = ( scale*sqrt( ssq ), scale*sqrt( ssq ) .lt. flmax                  
C            (                                                                  
C            ( flmax,             scale*sqrt( ssq ) .ge. flmax                  
C                                                                               
C  via the function name.                                                       
C                                                                               
C                                                                               
C  Nag Fortran 77 O( 1 ) basic linear algebra routine.                          
C                                                                               
C  -- Written on 22-October-1982.                                               
C     Sven Hammarling, Nag Central Office.                                      
C                                                                               
C                                                                               
C     .. Parameters ..                                                          
      DOUBLE PRECISION FLMAX
      PARAMETER (FLMAX =4.49423283715580D+307)
C     .. Local Scalars ..                                                       
      DOUBLE PRECISION      NORM, SQT                                           
C     .. Intrinsic Functions ..                                                 
      INTRINSIC             SQRT                                                
C     ..                                                                        
C     .. Executable Statements ..                                               
      SQT = SQRT( SSQ )                                                         
      IF( SCALE.LT.FLMAX/SQT )THEN                                              
         NORM = SCALE*SQT                                                       
      ELSE                                                                      
         NORM = FLMAX                                                           
      END IF                                                                    
C                                                                               
      F06BMF = NORM                                                             
      RETURN                                                                    
C                                                                               
C     End of F06BMF. ( SNORM )                                                  
C                                                                               
      END                                                                       
