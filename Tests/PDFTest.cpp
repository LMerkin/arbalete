// vim:ts=2:et
//===========================================================================//
//                                "PDFTest.cpp":                             //
//           Computation of PDFs for Various Diffusions and Grids            //
//===========================================================================//
#include "Common/Maths.hpp"
#include "Common/CUDAEnv.hpp"
#include "PDF/PDF2D.h"
#include <boost/program_options.hpp>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <stdexcept>

using namespace std;
using namespace Arbalete;
namespace pro = boost::program_options;

namespace
{
  // Here Return PDFs are accumulated:
  template<typename F>
  struct PDFRT
  {
    DateTime  m_t;
    Vector<F> m_pdfr;
  };

  template<typename F>
  static void RunTest(int argc, char* argv[])
  {
    //-----------------------------------------------------------------------//
    // Default Params:                                                       //
    //-----------------------------------------------------------------------//
    F      S0             = F(100.0);
    F      sigma0         = F(0.3);
    F      V0             = sigma0 * sigma0;
    F      mu             = F(0.03);    // Trend
    F      xiN            = F(0.5);     // "S" vol night scaling factor
    F      beta           = F(0.9);     // CEV
    F      kappa          = F(20.0);    // Mean reversion -- strong
    F      a              = F(0.0);     // Vol/Variance Trend Slope
    F      b              = F(0.15);    // Vol/Variance Trend Intercept
    F      epsTH          = F(0.2);     // Vol of Vol/Variance, trading hours
    F      etaN           = F(0.5);     // Out-of-trading hrs factor for "eps"
    F      rho            = F(-0.7);    // corr(S,V)
    F      ir             = F(0.01);    // Interest rate
    F      xa             = F(0.0);     // "Aplitude" of "x"s in Returns PDF
    int    nx             = 1;          // Number of "x"s to evaluate PDFR at
    string t0             = "2012-02-24 18:00:00";
                                        // DateTime the diffusion starts
    string calName        = "SX5E";
    string diffusion      = "3/2-CEV";
    bool   isFwd          = true;
    string method         = "Yanenko";
    string outFile        = "";
    int    m              = 501;
    int    n              = 501;
    int    sts            = 5;
    F      loS            = Inf<F>();
    F      upS            = Inf<F>();
    F      loV            = Inf<F>();
    F      upV            = Inf<F>();
    F      minRelS        = F(0.075);
    F      minRelV        = F(0.2);
    F      maxRelS        = F(100.0);
    F      maxRelV        = F(100.0);
    F      NSigmasS       = F(7.0);
    F      NSigmasV       = F(7.0);
    bool   S0OnGrid       = true;
    bool   V0OnGrid       = true;
    F      hV             = F(0.0);     // Only used for "BayesOptFilt" Grids
    string T              = "30d";
    string dt             = "1h";
    bool   withRegSw      = true;
    int    debugLevel     = 0;
    bool   returns        = false;
    bool   gridTest       = false;
    bool   perfTest       = false;
    bool   useDouble      = false;
    bool   useDirichlet   = false;
    int    cudaBlksPerSM  = 0;
    int    cudaBlkSz      = 0;          // CUDA is NOT used by default
    int    cudaDevID      = 0;

    //-----------------------------------------------------------------------//
    // Command-Line Options:                                                 //
    //-----------------------------------------------------------------------//
    pro::options_description desc("Allowed options");
    desc.add_options()
      // Whitch Program to Run?
      ( "double",                           "Use \"double\" arithmetic"    )
      ( "float",                            "Use \"float\"  arithmetic"    )
      // Diffusion Initials:
      ( "diffusion",  pro::value<string>(),
                      "Diffusion (Model), e.g. 3o2CEVCal" )
      ( "S0",         pro::value<F>(),      "Initial S value"     )
      ( "V0",         pro::value<F>(),      "Initial V value"     )
      ( "t0",         pro::value<string>(),
                      "YYYY-MM-DD hh:mm:ss for initial time" )
      ( "cal",        pro::value<string>(), "Business Calendar Name"  )
      ( "T",          pro::value<string>(), "Expiration Time (postfix: [d]" )
      ( "dt",         pro::value<string>(), "Time step (postfix: [mhd])" )
      // Diffusion Params:
      ( "mu",         pro::value<F>(),      "S trend coeff"    )
      ( "xiN",        pro::value<F>(),      "S vol night scaling factor" )
      ( "beta",       pro::value<F>(),      "CEV exponent"     )
      ( "kappa",      pro::value<F>(),      "V mean-reversion rate"   )
      ( "a",          pro::value<F>(),      "V trend slope"    )
      ( "b",          pro::value<F>(),      "V trend intercept" )
      ( "epsTH",      pro::value<F>(),      "Vol-of-V, trading hours" )
      ( "etaN",       pro::value<F>(),      "Non-trading-hrs factor for eps" )
      ( "rho",        pro::value<F>(),      "corr(S,V)" )
      ( "IR",         pro::value<F>(),      "Interest Rate"    )
      ( "noRegSw",                          "Disable Regime Switching" )
      // Fokker-Planck or Feynman-Kac?
      ( "Fwd",        "Forward  Induction (solving Fokker-Planck" )
      ( "Bwd",        "Backward Induction (solving Feynman-Kac"   )
      // Grid Params:
      ( "method",     pro::value<string>(),
                      "Grid: Yanenko|PR-ADI|FullyImpl|CN|RKC1-n|RKC2-n|RKF5"  )
      ( "m",          pro::value<int>(),    "Number of S grid nodes" )
      ( "n",          pro::value<int>(),    "Number of V grid nodes" )
      ( "sts",        pro::value<int>(),    "Stencil Size: 3 or 5"  )
      // Grid axes construction params:
      ( "loS",        pro::value<F>(),      "LoS bound (eg 0), default: auto" )
      ( "upS",        pro::value<F>(),      "UpS bound,        default: auto" )
      ( "loV",        pro::value<F>(),      "LoV bound (eg 0), default: auto" )
      ( "upV",        pro::value<F>(),      "UpV bound,        default: auto" )
      ( "minRelS",    pro::value<F>(),      "Min rel size of S moves (0.075)" )
      ( "minRelV",    pro::value<F>(),      "Min rel size of V moves (0.2)"   )
      ( "maxRelS",    pro::value<F>(),      "Max rel size of S moves (100.0)" )
      ( "maxRelV",    pro::value<F>(),      "Max rel size of S moves (100.0)" )
      ( "NSigmasS",   pro::value<F>(),      "#StdDevs in S" )
      ( "NSigmasV",   pro::value<F>(),      "#StdDevs in V" )
      ( "S0OnGrid",   pro::value<bool>(),   "S0 placed directly on the Grid?" )
      ( "V0OnGrid",   pro::value<bool>(),   "V0 placed directly on the Grid?" )
      ( "hV",         pro::value<F>(),      "Only for BayesOptFilt Grids" )
      ( "Dirichlet",                        "Use Dirichlet BCs, not Neumann"  )
      // PDFR Params:
      ( "xa",         pro::value<F>(),      "Amplitude of PDFR args" )
      ( "nx",         pro::value<int>(),    "Number of PDFR args (>=1)"  )
      // CUDA Params:
      ( "CDevID",     pro::value<int>(),    "CUDA Device ID" )
      ( "CBpSM",      pro::value<int>(),    "CUDA Blocks per SM (0..16)" )
      ( "CBSz",       pro::value<int>(),    "CUDA Block Size (0..1024, ~32" )
      // Test Running Params:
      ( "debug",      pro::value<int>(),    "Debugging level [012]"  )
      ( "gridTest",                         "Test the Grid convergence"  )
      ( "perfTest",                         "Test the CUDA performance"  )
      ( "out",        pro::value<string>(), "Output file (- for stdout)" );

    pro::variables_map vm;
    pro::store (pro::parse_command_line(argc, argv, desc), vm);
    pro::notify(vm);

    // First, check the Double/Float settings. If the current instance uses a
    // wrong type, STOP IMMEDIATELY:
    if (vm.count("double") && vm.count("float"))
    {
      cerr << "\"double\" and \"float\" flags are mutually-exclusive" << endl;
      exit(1);
    }
    if (vm.count("double"))
      useDouble = true;

    if (vm.count("float"))
      useDouble = false;

    if ((useDouble && !IsDouble<F>()) || (!useDouble && IsDouble<F>()))
      return;

    // Now continue with params:
    // Diffusion Initials:
    if (vm.count("diffusion"))
      diffusion = vm["diffusion"].as<string>();

    if (vm.count("S0"))
      S0  = vm["S0"].as<F>();

    if (vm.count("V0"))
      V0  = vm["V0"].as<F>();

    if (vm.count("t0"))
      t0  = vm["t0"].as<string>();

    if (vm.count("cal"))
      calName = vm["cal"].as<string>();

    if (vm.count("T"))
      T  = vm["T"].as<string>();

    if (vm.count("dt"))
      dt = vm["dt"].as<string>();

    // Diffusion Params:
    if (vm.count("mu"))
      mu = vm["mu"].as<F>();

    if (vm.count("xiN"))
      xiN = vm["xiN"].as<F>();

    if (vm.count("beta"))
      beta  = vm["beta"].as<F>();

    if (vm.count("kappa"))
      kappa = vm["kappa"].as<F>();

    if (vm.count("a"))
      a = vm["a"].as<F>();

    if (vm.count("b"))
      b = vm["b"].as<F>();

    if (vm.count("epsTH"))
      epsTH = vm["epsTH"].as<F>();

    if (vm.count("etaN"))
      etaN  = vm["etaN"].as<F>();

    if (vm.count("rho"))
      rho = vm["rho"].as<F>();

    if (vm.count("IR"))
      ir  = vm["IR"].as<F>();

    if (vm.count("noRegSw") != 0)
      withRegSw = false;

    // Fokker-Planck or Feynman-Kac?
    if (vm.count("Fwd") && vm.count("Bwd"))
    {
      cerr << "Fwd and Bwd flags are mutually exclusive" << endl;
      exit(1);
    }
    if (vm.count("Fwd"))
      isFwd = true;

    if (vm.count("Bwd"))
      isFwd = false;

    // Grid Params:
    if (vm.count("method"))
      method  = vm["method"].as<string>();

    if (vm.count("m"))
      m = vm["m"].as<int>();

    if (vm.count("n"))
      n = vm["n"].as<int>();

    if (vm.count("sts"))
      sts = vm["sts"].as<int>();

    // Grid axes construction params:
    if (vm.count("loS"))
      loS = vm["loS"].as<F>();

    if (vm.count("upS"))
      upS = vm["upS"].as<F>();

    if (vm.count("loV"))
      loV = vm["loV"].as<F>();

    if (vm.count("upV"))
      upV = vm["upV"].as<F>();

    if (vm.count("minRelS"))
      minRelS = vm["minRelS"].as<F>();

    if (vm.count("minRelV"))
      minRelV = vm["minRelV"].as<F>();

    if (vm.count("maxRelS"))
      maxRelS = vm["maxRelS"].as<F>();

    if (vm.count("maxRelV"))
      maxRelV = vm["maxRelV"].as<F>();

    if (vm.count("NSigmasS"))
      NSigmasS = vm["NSigmasS"].as<F>();

    if (vm.count("NSigmasV"))
      NSigmasV = vm["NSigmasV"].as<F>();

    if (vm.count("S0OnGrid"))
      S0OnGrid = vm["S0OnGrid"].as<bool>();

    if (vm.count("V0OnGrid"))
      V0OnGrid = vm["V0OnGrid"].as<bool>();

    if (vm.count("hV"))
      hV = vm["hV"].as<F>();

    // NB: for "[SV]0OnGrid", there must be explicit boolean values:

    useDirichlet = (vm.count("Dirichlet") != 0);

    // PDFR Params:
    if (vm.count("xa"))
      xa = vm["xa"].as<F>();

    if (vm.count("nx"))
      nx = vm["nx"].as<int>();

    if (xa < F(0.0) || nx <= 0)
    {
      cerr << "Invalid PDFR args" << endl;
      exit(1);
    }

    // CUDA Params:
    if (vm.count("CDevID") != 0)
      cudaDevID = vm["CDevID"].as<int>();

    if (vm.count("CBpSM") != 0)
      cudaBlksPerSM = vm["CBpSM"].as<int>();

    if (vm.count("CBSz") != 0)
      cudaBlkSz = vm["CBSz"].as<int>();

    // Test Running Params; "gridTest" implies "debug":
    if (vm.count("debug") != 0)
      debugLevel = vm["debug"].as<int>();

    gridTest = vm.count("gridTest") != 0;

    perfTest = vm.count("perfTest") != 0;

    if (gridTest && (debugLevel == 0))
      debugLevel = 1;

    if ((debugLevel != 0) && perfTest)
    {
      cerr << "Debug and CUDAPerfTest are mutually exclusive" << endl;
      exit(1);
    }

    returns = vm.count("returns") != 0;

    if (vm.count("out"))
      outFile = vm["out"].as<string>();

    //-----------------------------------------------------------------------//
    // Create the Diffusion Params:                                          //
    //-----------------------------------------------------------------------//
    // Common part of any DiffusionParams:
    Diffusion_Params<F> diffParams;

    diffParams.m_type    = diffusion;
    diffParams.m_t0      = DateTimeOfStr(t0);
    diffParams.m_S0      = S0;
    diffParams.m_withReg = withRegSw;
    int nCoeffs          = 11;

    Vector<F>& coeffs    = diffParams.m_coeffs;
    coeffs.resize(nCoeffs);

    int i = 0;
    coeffs[i++] = V0;
    coeffs[i++] = mu;
    coeffs[i++] = xiN;
    coeffs[i++] = beta;
    coeffs[i++] = kappa;
    coeffs[i++] = a;
    coeffs[i++] = b;
    coeffs[i++] = epsTH;
    coeffs[i++] = etaN;
    coeffs[i++] = rho;
    coeffs[i++] = ir;
    if (i != nCoeffs)
      throw runtime_error("Invalid number of diffusion coeffs");

    //-----------------------------------------------------------------------//
    // Create the Grid Params:                                               //
    //-----------------------------------------------------------------------//
    // (No CUDA Config yet -- will be installed later):
    typename Grid2D<F>::Params gridParams;

    gridParams.m_method     = method;
    gridParams.m_m          = m;
    gridParams.m_n          = n;
    gridParams.m_hV         = hV;              // XXX: Only for "BayesOptFilt"
    gridParams.m_sts        = sts;
    gridParams.m_TS         = TimeOfStr(T);    // Same time horisons for S, V
    gridParams.m_TV         = gridParams.m_TS; // In normal case (no conds)
    gridParams.m_dtTH       = TimeOfStr(dt);
    gridParams.m_dtON       = gridParams.m_dtTH;
    gridParams.m_dtWEH      = gridParams.m_dtON;
    gridParams.m_etTH       = gridParams.m_dtTH;
    gridParams.m_etN        = gridParams.m_dtON;
    gridParams.m_LoS        = loS;
    gridParams.m_UpS        = upS;
    gridParams.m_LoV        = loV;
    gridParams.m_UpV        = upV;
    gridParams.m_minRelS    = minRelS;
    gridParams.m_minRelV    = minRelV;
    gridParams.m_maxRelS    = maxRelS;
    gridParams.m_maxRelV    = maxRelV;
    gridParams.m_S0OnGrid   = S0OnGrid;
    gridParams.m_V0OnGrid   = V0OnGrid;
    gridParams.m_debugLevel = debugLevel;
    gridParams.m_cal        = TradingCalendar::MkTradingCalendar(calName);
    gridParams.m_NSigmasS   = NSigmasS;
    gridParams.m_NSigmasV   = NSigmasV;
    // gridParams.m_cudaConf remains empty for the moment

    //-----------------------------------------------------------------------//
    // Create the Call-Back:                                                 //
    //-----------------------------------------------------------------------//
    vector<PDFRT<F>> pdfrts;

    typename PDF2D<F>::CallBack callBack
    (
      [returns, debugLevel, &pdfrts]
        (PDF2D<F> const& pdf, DateTime t, int UNUSED_PARAM(stk), F const* f,
         DateTime t_next,     TradingCalendar::CalReg reg)
      -> Time
      {
        // Output the time stamp:
        if (debugLevel >= 1)
        {
          cout << '\r' << t;
          if (t == t_next)
            cout << endl;
        }

        if (t < t_next && returns)
        {
          // Fwd run, and need to compute PDFR over the NEXT interval:
          // For uniformity, only do that if it happens to be over trading hrs,
          // and over the standard time step:
          //
          Time dt  = t_next - t;
          if (dt  == pdf.GetGrid().GetDt(reg)    &&
              reg == TradingCalendar::TradingHours)
          {
            // Create new PDFRT and attach it to the results vector:
            pdfrts.push_back(PDFRT<F>());
            PDFRT<F>& curr = pdfrts.back();

            // Resize the output vector:
            curr.m_t = t;
            int nx   = pdf.GetRetPDFKern().GetNParams();
            curr.m_pdfr.resize(nx);

            // Actually compute the Returns PDF:
            pdf.PDFR(t, dt, reg, f, &curr.m_pdfr);
          }
        }

        // Last step: output the density from the Grid:
        if (debugLevel == 1 && t_next == t)
        {
          Grid2D<F> const& grid = pdf.GetGrid();
          grid.template GridDebug<true>(t, f);
        }
        return ZeroTime();
      }
    );

    //-----------------------------------------------------------------------//
    // Create and run a "PDF2D":                                             //
    //-----------------------------------------------------------------------//
    int NBfrom = cudaBlksPerSM;
    int NBto   = NBfrom;
    int BSfrom = cudaBlkSz;
    int BSto   = BSfrom;

    if (perfTest)
    {
      // Disregard the above values:
      NBfrom =    1;
      NBto   =   16;
      BSfrom =   32;
      BSto   = 1024;
    }

    for (int NB = NBfrom; NB <= NBto; ++NB)
      for (int BS = BSfrom; BS <= BSto; BS += 32)
      {
        int  SMT     = NB * BS;
        bool useCUDA = (SMT != 0);
        if  (useCUDA && SMT < 128)
          continue;

        // Create CUDA Config and install it in "gridParams" (unless CUDA is not
        // used at all):
        if (useCUDA)
          try
          {
            gridParams.m_cudaEnv =
              shared_ptr<CUDAEnv>
              (new CUDAEnv(CUDAEnv::Params(cudaDevID, NB, BS, perfTest)));
          }
          catch(...)
          {
            // Any error here -- skip this config if in Perf Test:
            if (perfTest)
              continue;
            else
              throw;
          }

        // Create the PDF object, including the Diffusion and the Grid:
        PDF2D<F> pdf(diffParams, gridParams, isFwd);

        // Set the PDFR args:
        pdf.SetXs(-xa, xa, nx);

        // RUN!!!
        try
        {
          if (gridTest)
            // Test mode. XXX: No discounting yet; interested in intermediate
            // debugging output:
            (void)pdf.GridTest(false);
          else
          {
            DateTime tStart = pdf.StartTime();
            DateTime tEnd   = pdf.EndTime  ();

            pdf.IntegrateTo
            (isFwd ? tEnd : tStart, nullptr, &callBack, useDirichlet);
          }
        }
        catch(...)
        {
          // Skip any errors if in Perf Test:
          if (perfTest)
            continue;
          else
            throw;
        }

        if (useCUDA && perfTest)
        {
          fprintf(stderr, "NB=%d\tBS=%d\tTime=%.3f sec\n",
            NB, BS, pdf.GetGrid().GetCUDATiming());
          fflush(stderr);
        }
      }

    //-----------------------------------------------------------------------//
    // Output the returns PDF (if enabled):                                  //
    //-----------------------------------------------------------------------//
    if (returns)
    {
      FILE* of = nullptr;
      if (outFile != "" && !perfTest)
      {
        if (outFile == "-")
          of = stdout;
        else
          of = fopen(outFile.c_str(), "w");
      }

      // Output PDFRs in Matlab-like format. Also verify that each PDFR integ-
      // rates to 1 (with the degree of approximation depending on the number
      // of points used):
      //
      if (of != nullptr)
      {
        for (typename vector<PDFRT<F>>::const_iterator it = pdfrts.begin();
             it != pdfrts.end(); ++it)
        {
          F rsum = F(0.0);
          Vector<F> const& pdfr = it->m_pdfr;
          int              nx   = int(pdfr.size());
          fprintf(of, "%%\n%% %s\n", to_simple_string(it->m_t).c_str());
    
          for (int i = 0; i < nx; ++i)
          {
            F p   = pdfr[i];
            rsum += p;
            fprintf(of, "%.9e  ", p);
          }
          fputc('\n', of);
          if (nx >= 2)
          {
            rsum *= (F(2.0) * xa / F(nx-1));
            fprintf(of, "%% |1-mass| = %.9e\n", Abs(F(1.0) - rsum));
          }
        }
        fclose(of);
      }
    }
    // All done!
  }
}

//===========================================================================//
// "main":                                                                   //
//===========================================================================//
int main(int argc, char* argv[])
{
  try
  {
    // Run both test instances, but one of them exits immediately:
    RunTest<double>(argc, argv);
    RunTest<float> (argc, argv);
    return 0;
  }
  catch (exception const& e)
  {
    cerr << "ERROR: " << e.what() << endl;
    return 1;
  }
}
