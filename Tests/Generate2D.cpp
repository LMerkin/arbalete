// vim:ts=2:et
//===========================================================================//
//                            "Generate2D.cpp":                           //
//    Time Series Generation for 2D Ergodic Calibration Convergence Tests //
//===========================================================================//
#include "Common/Maths.hpp"
#include "Common/VecMatTypes.h"
#include "Common/TradingCalendar.h"
#include "Diffusions/Diffusion2D_Fact.h"
#include "MonteCarlo/RNG.h"
#include "Calibration/MarketData.h"
#include <boost/program_options.hpp>
#include <cstdlib>
#include <string>
#include <cassert>
#include <memory>

using namespace std;
using namespace Arbalete;
namespace pro = boost::program_options;


int main()
{
  //-------------------------------------------------------------------------//
  // Diffusion Params:                                                       //
  //-------------------------------------------------------------------------//
  string diffType = "SABR";

  double V0       =  0.3;
  double mu       =  0.0;
  double xiN      =  0.5;
  double beta     =  1.0;
  double kappa    =  0.0;   // Currently the process is non-mean-reverting...
  double a        =  0.0;   // and stationary...
  double b        =  0.0;
  double eps      =  0.75;
  double etaN     =  0.3;
  double rho      = -0.7;
  double IR       =  0.0;

  DateTime t0(Date(2013, 3, 25), Time(10, 0, 0));
  double S0       = 100.0;
  Time T          = TimeOfStr("365d");
  Time dt         = TimeOfStr("15m");

  //-------------------------------------------------------------------------//
  // Construct the Diffusion:                                                //
  //-------------------------------------------------------------------------//
  Vector<double> coeffs(11);
  coeffs[0]  = V0;
  coeffs[1]  = mu;
  coeffs[2]  = xiN;
  coeffs[3]  = beta;
  coeffs[4]  = kappa;
  coeffs[5]  = a;
  coeffs[6]  = b;
  coeffs[7]  = eps;
  coeffs[8]  = etaN;
  coeffs[9]  = rho;
  coeffs[10] = IR;

  // NB: Diffusion is regime-swtching:
  Diffusion_Params<double> diffParams{ diffType, t0, S0, true, coeffs };

  shared_ptr<Diffusion2D<double>> diff =
    shared_ptr<Diffusion2D<double>>(MkDiffusion2D(diffParams));

  //-------------------------------------------------------------------------//
  // Simulate a Path:                                                        //
  //-------------------------------------------------------------------------//
  // Construct the Time Line. Evening and week-end periods made 0, so we will
  // step over the whole periods:
  //
  vector<TimeNode<double>> timeLine;
  Time dts[3]    = { dt, ZeroTime(), ZeroTime() };

  string calName = "SX5E";
  shared_ptr<TradingCalendar> cal =
    TradingCalendar::MkTradingCalendar(calName);

  // IsFwd=true:
  MkTimeLine<true, double>(t0, dts, t0 + T, cal.get(), nullptr, &timeLine);
  int nk = int(timeLine.size());

  // Create the RNG. No need for CUDA as we generate only 1 path:
  shared_ptr<CUDAEnv> none(nullptr);
  RNG<double> rng(none);

  // Output vector:
  Matrix<double> S, V;

  // Run MC: No IR, no Antithetic:
  diff->MCPaths(timeLine, 1, rng, &S, &V, nullptr, false);

  assert(int(S.size1()) == nk && int(S.size2()) == 1);

  //-------------------------------------------------------------------------//
  // Output the Path:                                                        //
  //-------------------------------------------------------------------------//
  for (int i = 0; i < nk; ++i)
  {
    TimeNode<double> const& tn = timeLine[i];
    DateTime ti = t0 + TimeGener::seconds(tn.m_secs);

    cout << DateTimeToStr(ti) << '\t' << S(i,0) << endl;
  }
}
