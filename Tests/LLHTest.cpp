// vim:ts=2:et
//===========================================================================//
//                              "LLHTest.cpp":                               //
//     Computation of LLH Functions for Various Diffusions and Grids         //
//===========================================================================//
#include "Common/Maths.hpp"
#include "Common/CUDAEnv.hpp"
#include "PDF/CondLLH2D_FokkerPlanck.h"
#include "PDF/CondLLH2D_BayesOptFilt.h"
#include "Calibration/MarketData.h"
#include <boost/program_options.hpp>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <stdexcept>

using namespace std;
using namespace Arbalete;
namespace pro = boost::program_options;

namespace
{
  template<typename F>
  static void RunTest(int argc, char* argv[])
  {
    //-----------------------------------------------------------------------//
    // Default Params:                                                       //
    //-----------------------------------------------------------------------//
    typename CondLLH2D_FokkerPlanck<F>::Params llhParams;

    F      sigma0         = F(0.3);
    F      V0             = sigma0 * sigma0;
    F      mu             = F(0.03);    // Trend
    F      xiN            = F(0.5);     // "S" vol night scalining factor
    F      beta           = F(0.9);     // CEV
    F      kappa          = F(1.0);     // Variance mean reversion rate -- weak
    F      a              = F(0.0);     // Vol/Variance Trend Slope
    F      b              = F(0.15);    // Vol/Variance Trend Intercept
    F      epsTH          = F(0.2);     // Vol of Vol/Variance, trading hours
    F      etaN           = F(0.5);     // Out-or-trading-hrs factor for "eps"
    F      rho            = F(-0.7);    // corr(S,V)
    F      ir             = F(0.01);    // Interest rate
    string t0str          = "2012-02-24 16:30:00";
    string t1str          = "2012-03-01 00:00:00";
    string t2str          = "2012-04-01 00:00:00";
    string dts            = "1h";
    string calName        = "SX5E";
    string diffusion      = "SABR";
    string method         = "Yanenko";
    string mdFile         = "";
    int    m              = 501;
    int    n              = 501;
    int    sts            = 5;
    F      minRelS        = F(0.075);
    F      minRelV        = F(0.2);
    F      maxRelS        = F(100.0);
    F      maxRelV        = F(100.0);
    F      NSigmasS       = F(7.0);
    F      NSigmasV       = F(7.0);
    F      loS            = -Inf<F>();
    F      upS            =  Inf<F>();
    F      loV            = -Inf<F>();
    F      upV            =  Inf<F>();
    bool   S0OnGrid       = true;
    bool   V0OnGrid       = true;
    F      hV             = F(0.0);     // Only for "BayesOptFilt" Grids
    F      hS             = F(0.0);     // Currently unused
    string dt             = "1h";       // Initially; then by conditioning
    bool   withRegSw      = true;
    bool   perfTest       = false;
    bool   useDouble      = false;
    int    cudaBlksPerSM  = 0;          // CUDA is NOT used by default
    int    cudaBlkSz      = 0;
    int    cudaDevID      = 0;
    int    volWinSz       = 100;

    //-----------------------------------------------------------------------//
    // Command-Line Options:                                                 //
    //-----------------------------------------------------------------------//
    pro::options_description desc("Allowed options");
    desc.add_options()
      // Whitch Program to Run?
      ( "double",                           "Use \"double\" arithmetic"    )
      ( "float",                            "Use \"float\"  arithmetic"    )
      // Diffusion Initials:
      ( "diffusion",  pro::value<string>(),
                      "Diffusion (Model), e.g. 3o2CEVCal" )
      ( "V0",         pro::value<F>(),      "Initial V value"     )
      ( "t0",         pro::value<string>(),
                      "Diffusion/Grid start: YYYY-MM-DD hh:mm:ss, MANDATORY" )
      ( "cal",        pro::value<string>(), "Business Calendar Name"  )
      ( "dt",         pro::value<string>(), "Time step (postfix: [mhD])" )
      // Diffusion Params:
      ( "mu",         pro::value<F>(),      "S trend coeff"    )
      ( "xiN",        pro::value<F>(),      "S vol night scaling factor" )
      ( "beta",       pro::value<F>(),      "CEV exponent"     )
      ( "kappa",      pro::value<F>(),      "V mean-reversion rate"   )
      ( "a",          pro::value<F>(),      "V trend slope"     )
      ( "b",          pro::value<F>(),      "V trend intercept" )
      ( "epsTH",      pro::value<F>(),      "Vol-of-V, trading hours" )
      ( "etaN",       pro::value<F>(),      "Non-trading-hrs factor for eps" )
      ( "rho",        pro::value<F>(),      "corr(S,V)" )
      ( "IR",         pro::value<F>(),      "Integrest Rate"   )
      ( "noRegSw",                          "Disable Regime Switching" )
      // Grid Params:
      ( "method",     pro::value<string>(),
                      "Grid: Yanenko|PR-ADI|...|BayesOptFilt"  )
      ( "m",          pro::value<int>(),    "Number of S grid nodes" )
      ( "n",          pro::value<int>(),    "Number of V grid nodes" )
      ( "sts",        pro::value<int>(),    "Stencil Size: 3 or 5"  )
      ( "minRelS",    pro::value<F>(),      "Min rel move in S (0.075)" )
      ( "minRelV",    pro::value<F>(),      "Min rel move in V (0.2)"   )
      ( "maxRelS",    pro::value<F>(),      "Max rel move in S (100.0)" )
      ( "maxRelV",    pro::value<F>(),      "Max rel move in V (100.0)" )
      ( "NSigmasS",   pro::value<F>(),      "#StdDevs in S" )
      ( "NSigmasV",   pro::value<F>(),      "#StdDevs in V" )
      ( "loS",        pro::value<F>(),      "Fixed Lo Grid Bound on S" )
      ( "upS",        pro::value<F>(),      "Fixed Up Grid Bound on S" )
      ( "loV",        pro::value<F>(),      "Fixed Lo Grid Bound on V" )
      ( "upV",        pro::value<F>(),      "Fixed Up Grid Bound on V" )
      ( "S0OnGrid",   pro::value<bool>(),   "S0 placed directly on Grid?" )
      ( "V0OnGrid",   pro::value<bool>(),   "V0 placed directly on Grid?" )
      ( "hV",         pro::value<F>(),      "Only for BayesOptFilt Grids" )
      ( "hS",         pro::value<F>(),      "Currently unused" )
      ( "Dirichlet",                        "Use Dirichlet BCs (not Neumann)" )
      ( "minSbC",     pro::value<int>(),    "Min number of Steps Btwn Conds"  )
      // Market Data:
      ( "md",         pro::value<string>(), "Market Data file  (MANDATORY)" )
      ( "ts1",        pro::value<string>(), "Calibration start (MANDATORY)" )
      ( "ts2",        pro::value<string>(), "Calibration end   (MANDATORY)" )
      ( "dts",        pro::value<string>(), "Calibration step" )
      ( "volWinSz",   pro::value<int>(),    "Window size for vol-of-vol ests" )
      // CUDA Params:
      ( "CBSz",       pro::value<int>(),    "CUDA Block Size (0..1024, ~32" )
      ( "CBpSM",      pro::value<int>(),    "CUDA Blocks per SM (0..16)" )
      ( "CDevID",     pro::value<int>(),    "CUDA Device ID" )
      // Test Running Params:
      ( "debug",      pro::value<int>(),    "Debug flag [012]" )
      ( "perfTest",                         "Test the CUDA performance"     )
      ( "instVols",                         "Print Instanteneous Vols" );

    pro::variables_map vm;
    pro::store (pro::parse_command_line(argc, argv, desc), vm);
    pro::notify(vm);

    // First, check the Double/Float settings. If the current instance uses a
    // wrong type, STOP IMMEDIATELY:
    if (vm.count("double") && vm.count("float"))
    {
      cerr << "\"double\" and \"float\" flags are mutually-exclusive" << endl;
      exit(1);
    }
    if (vm.count("double"))
      useDouble = true;

    if (vm.count("float"))
      useDouble = false;

    if ((useDouble && !IsDouble<F>()) || (!useDouble && IsDouble<F>()))
      return;

    // Now continue with params:
    // Diffusion Initials:
    if (vm.count("diffusion"))
      diffusion = vm["diffusion"].as<string>();

    if (vm.count("V0"))
      V0  = vm["V0"].as<F>();

    if (vm.count("t0"))
      t0str = vm["t0"].as<string>();

    if (vm.count("cal"))
      calName = vm["cal"].as<string>();

    if (vm.count("dt"))
      dt = vm["dt"].as<string>();

    // Diffusion Params:
    if (vm.count("mu"))
      mu = vm["mu"].as<F>();

    if (vm.count("xiN"))
      xiN = vm["xiN"].as<F>();

    if (vm.count("beta"))
      beta  = vm["beta"].as<F>();

    if (vm.count("kappa"))
      kappa = vm["kappa"].as<F>();

    if (vm.count("a"))
      a = vm["a"].as<F>();

    if (vm.count("b"))
      b = vm["b"].as<F>();

    if (vm.count("epsTH"))
      epsTH = vm["epsTH"].as<F>();

    if (vm.count("etaN"))
      etaN  = vm["etaN"].as<F>();

    if (vm.count("rho"))
      rho = vm["rho"].as<F>();

    if (vm.count("IR"))
      ir  = vm["IR"].as<F>();

    if (vm.count("noRegSw") != 0)
      withRegSw = false;

    // Grid Params:
    if (vm.count("method"))
      method  = vm["method"].as<string>();

    if (vm.count("m"))
      m = vm["m"].as<int>();

    if (vm.count("n"))
      n = vm["n"].as<int>();

    if (vm.count("sts"))
      sts = vm["sts"].as<int>();

    if (vm.count("minRelS"))
      minRelS = vm["minRelS"].as<F>();

    if (vm.count("minRelV"))
      minRelV = vm["minRelV"].as<F>();

    if (vm.count("maxRelS"))
      maxRelS = vm["maxRelS"].as<F>();

    if (vm.count("maxRelV"))
      maxRelV = vm["maxRelV"].as<F>();

    if (vm.count("NSigmasS"))
      NSigmasS = vm["NSigmasS"].as<F>();

    if (vm.count("NSigmasV"))
      NSigmasV = vm["NSigmasV"].as<F>();

    if (vm.count("loS"))
      loS      = vm["loS"].as<F>();

    if (vm.count("upS"))
      upS      = vm["upS"].as<F>();

    if (vm.count("loV"))
      loV      = vm["loV"].as<F>();

    if (vm.count("upV"))
      upV      = vm["upV"].as<F>();

    // NB: for "[SV]0OnGrid", explicit boolean params are required:
    if (vm.count("S0OnGrid"))
      S0OnGrid = vm["S0OnGrid"].as<bool>();

    if (vm.count("V0OnGrid"))
      V0OnGrid = vm["V0OnGrid"].as<bool>();

    if (vm.count("hV"))
      hV = vm["hV"].as<F>();  // Only for "BayesOptFilt"Grids

    if (vm.count("hS"))
      hS = vm["hS"].as<F>();  // Only for "BayesOptFilt"Grids

    if (vm.count("Dirichlet") != 0)
      llhParams.m_useDirichlet = true;

    if (vm.count("minSbC") != 0)
    {
      int minSbC = vm["minSbC"].as<int>();
      if (minSbC < 1)
        throw invalid_argument("minSbC must be >= 1");
      llhParams.m_minSbC = minSbC;
    }

    // CUDA Params:
    if (vm.count("CBpSM") != 0)
      cudaBlksPerSM = vm["CBpSM"].as<int>();

    if (vm.count("CBSz") != 0)
      cudaBlkSz = vm["CBSz"].as<int>();

    if (vm.count("CDevID") != 0)
      cudaDevID = vm["CDevID"].as<int>();

    // Test Running Params:
    if (vm.count("debug") != 0)
    {
      int debug = vm["debug"].as<int>();
      llhParams.m_debugLevel = debug;
    }

    perfTest = vm.count("perfTest") != 0;

    if (llhParams.m_debugLevel >= 1 && perfTest)
      throw invalid_argument("Debug and PerfTest modes are incompatible");

    if (vm.count("md"))
      mdFile  = vm["md"].as<string>();
    else
      throw invalid_argument("MktData File must be specified");

    if (vm.count("ts1"))
      t1str = vm["ts1"].as<string>();

    if (vm.count("dts"))
      dts = vm["dts"].as<string>();

    if (vm.count("ts2"))
      t2str = vm["ts2"].as<string>();

    if (vm.count("volWinSz"))
      volWinSz = vm["volWinSz"].as<int>();

    //-----------------------------------------------------------------------//
    // Create and evaluate an "LLH2D":                                       //
    //-----------------------------------------------------------------------//
    // Diffusion Params Stub:
    //
    Diffusion_Params<F> diffParams =
      CondLLH<F>::MkDiffParamsStub(diffusion, withRegSw);

    // Diffusion Coeffs:
    //
    int nCoeffs = 11;

    Vector<F> diffCoeffs(nCoeffs);

    int i = 0;
    diffCoeffs[i++] = V0;
    diffCoeffs[i++] = mu;
    diffCoeffs[i++] = xiN;
    diffCoeffs[i++] = beta;
    diffCoeffs[i++] = kappa;
    diffCoeffs[i++] = a;
    diffCoeffs[i++] = b;
    diffCoeffs[i++] = epsTH;
    diffCoeffs[i++] = etaN;
    diffCoeffs[i++] = rho;
    diffCoeffs[i++] = ir;
    if (i != nCoeffs)
      throw runtime_error("Invalid number of diffusion coeffs");

    // Trading Calendar:
    shared_ptr<TradingCalendar> cal =
      TradingCalendar::MkTradingCalendar(calName);

    // Grid Params stub: "cudaEnv" will be installed later:
    //
    bool isBOF = (method == "BayesOptFilt");

    typename Grid2D<F>::Params gridParams =
      (!isBOF)
      ? // Generic LLH method based on the Fokker-Planck PDE; the latter is
        // solved using the specified Grid ("method"):
        //
        CondLLH2D_FokkerPlanck<F>::MkGridParamsStub
          (method,  m, n, sts, TimeOfStr(dt), nullptr, cal)

        // Use the BOF LLH method which internally uses the BOF Grid which
        // internally uses the "TransPDF" method on the Diffusion:
        //
      : CondLLH2D_BayesOptFilt<F>::MkGridParamsStub
          (n, diffParams.m_S0, minRelV, maxRelV, nullptr, cal);

    // Install "fine axes generation controls" in "gridParams":
    gridParams.m_NSigmasS = NSigmasS;
    gridParams.m_NSigmasV = NSigmasV;
    gridParams.m_S0OnGrid = S0OnGrid;
    gridParams.m_V0OnGrid = V0OnGrid;
    gridParams.m_minRelS  = minRelS;
    gridParams.m_maxRelS  = maxRelS;
    gridParams.m_minRelV  = minRelV;
    gridParams.m_maxRelV  = maxRelV;
    gridParams.m_LoS      = loS;
    gridParams.m_UpS      = upS;
    gridParams.m_LoV      = loV;
    gridParams.m_UpV      = upV;
    gridParams.m_hS       = hS;
    gridParams.m_hV       = hV;

    // Load the Market Data, with a "TransformLogNormId" call-back. NB: the
    // call-back DOES NOT modify the data -- it only accumulates the stats:
    //
    DateTime t0  = DateTimeOfStr(t0str);
    DateTime ts1 = DateTimeOfStr(t1str);
    DateTime ts2 = DateTimeOfStr(t2str);

    vector<MDItem<F>> mktData;

    MDTransform<F>* tr = MkMDTransform<F>("LogNormId", volWinSz);
    assert(tr != nullptr);

    LoadMktData<F>(mdFile, t0, ts1, TimeOfStr(dts), ts2, cal, tr, &mktData);

    // Get the Stats:
    StatsSV<F> stats = tr->m_statsAcc();

    // Output the Stats:
    cout << "Vol estimate (Trading Hours): " << (stats.m_volSTH  * F(100.0))
         << "%; err: " << (stats.m_errVolSTH * F(100.0)) << '%' << endl;
    cout << "Vol estimate (Over-Night)   : " << (stats.m_volSON  * F(100.0))
         << "%; err: " << (stats.m_errVolSON * F(100.0)) << '%' << endl;
    cout << "Vol estimate (Week-Ends/Hol): " << (stats.m_volSWEH * F(100.0))
         << "%; err: " << (stats.m_errVolSWEH* F(100.0)) << '%' << endl;

    // Estimate "epsTH" for the given Diffusion:
    F epsEst = F(0.0);
    if (diffusion == "3/2-CEV")
    {
      // "V" is variance of "S":
      //  V      = vol(S)^2;
      //  vol(V) = 2 * vol(S) * vol(vol(S));
      //  eps   ~= vol(V) / V^(3/2) = 2 * vol(vol(S)) / vol(S)^2
      //                            = 2 * vol(vol(S)) / V:
      F V    = stats.m_volSTH * stats.m_volSTH;
      epsEst = F(2.0) * stats.m_volVolSTH / V;
    }
    else
    if (diffusion == "Heston-CEV")
      // "V" is variance of "S":
      //  V      = vol(S)^2;
      //  vol(V) = 2 * vol(S) * vol(vol(S));
      //  eps   ~= vol(V) / sqrt(V) = vol(V) / vol(S) = 2 * vol(vol(S)):
      //
      epsEst = F(2.0) * stats.m_volVolSTH;
    else
    if (diffusion.find("SABR") != string::npos)
      // "V" is the vol of "S";
      // eps    ~= vol(V) / V = vol(vol(S)) / vol(S):
      epsEst = stats.m_volVolSTH / stats.m_volSTH;
    else
      assert(false);

    cout << "Eps estimate (Trading Hours): " << (epsEst * F(100.0))
         << "%; SNR: " << stats.m_volVolSNR  << endl;

    if (vm.count("instVols"))
    {
      vector<pair<DateTime,F>> const& instVols = tr->m_statsAcc.GetInstVols();

      for (typename vector<pair<DateTime,F>>::const_iterator it =
           instVols.begin(); it != instVols.end(); ++it)
        cout << it->first << '\t' << it->second << endl;
    }

    // CUDA Params:
    //
    int NBfrom = cudaBlksPerSM;
    int NBto   = NBfrom;
    int BSfrom = cudaBlkSz;
    int BSto   = BSfrom;

    if (perfTest)
    {
      // Disregard the above values. The limites below are for ComputeCapabilty
      // 3.0, lower CC's would produce some exceptions so unsupported vals will
      // be skipped:
      NBfrom =    1;
      NBto   =   16;
      BSfrom =   32;
      BSto   = 1024;
    }

    // NB: Loops reduce to a single (NB, BS) if it is not a performance test:
    //
    for (int NB = NBfrom; NB <= NBto; ++NB)
      for (int BS = BSfrom; BS <= BSto; BS += 32)
      {
        int  SMT     = NB * BS;
        bool useCUDA = (SMT != 0);
        if (useCUDA && SMT < 128)
          continue;     // Too few

        // Create the CUDA Config and install it in Grid Params:
        if (useCUDA)
        try
        {
          gridParams.m_cudaEnv =
            shared_ptr<CUDAEnv>
            (new CUDAEnv(CUDAEnv::Params(cudaDevID, NB, BS, perfTest)));
        }
        catch(...)
        {
          // Any error here -- skip this config if in Perf Test:
          if (perfTest)
            continue;
          else
            throw;
        }

        // Create the LLH object, including PDF, Grid and Diffusion:
        shared_ptr<CondLLH<F>> llh =
          (!isBOF)
          ? shared_ptr<CondLLH<F>>
            (new CondLLH2D_FokkerPlanck<F>
              (diffParams, gridParams, llhParams, mktData))

          : shared_ptr<CondLLH<F>>
            (new CondLLH2D_BayesOptFilt<F>
              (diffParams, gridParams, mktData, llhParams.m_debugLevel));

        // RUN!!!
        F mle = Inf<F>();
        try
        {
          llh->NLLH(diffCoeffs, &mle, nullptr, nullptr, nullptr);
        }
        catch(...)
        {
          // Skip any errors if in Perf Test:
          if (perfTest)
            continue;
          else
            throw;
        }

        cerr << "MLE = " << mle << endl;
        if (useCUDA && perfTest)
        {
          fprintf(stderr, "NB=%d\tBS=%d\tTime=%.3f sec\n",
            NB, BS, llh->GetCUDATiming());
          fflush(stderr);
        }
      }
    // Clean-up (not really necessary):
    delete tr;
  }
}

//===========================================================================//
// "main":                                                                   //
//===========================================================================//
int main(int argc, char* argv[])
{
  try
  {
    // Run both test instances, but one of them exits immediately:
    RunTest<double>(argc, argv);
    RunTest<float> (argc, argv);
    return 0;
  }
  catch (exception const& e)
  {
    cerr << "ERROR: " << e.what() << endl;
    return 1;
  }
}
