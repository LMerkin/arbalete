// vim:ts=2:et
//===========================================================================//
//                            "StrategyTest.cpp":                            //
//     Calibration and Back-Testing of a Simple VolArb Trading Strategy      //
//===========================================================================//
#include "Common/CUDAEnv.h"
#include "Common/VecMatTypes.h"
#include "Common/TradingCalendar.h"
#include "Common/BSM.hpp"
#include "Diffusions/Diffusion2D_Fact.h"
#include "MonteCarlo/RNG.h"
#include "MonteCarlo/PortfolioPaths_Core.hpp"
#include "PDF/CondLLH2D_BayesOptFilt.h"
#include "Calibration/MarketData.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <string>
#include <iostream>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <stdexcept>
#include <vector>
#include <memory>
#include <functional>
#include <cmath>

using namespace std;

namespace
{
  using namespace Arbalete;
  using namespace boost::property_tree;

  //=========================================================================//
  // Utils:                                                                  //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // "HedgingMethod":                                                        //
  //-------------------------------------------------------------------------//
  enum class HedgingMethod
  {
    ModelVols    = 0,
    ImpliedVols  = 1,
    FilteredVols = 2
  };

  //-------------------------------------------------------------------------//
  // "ParamsError":                                                          //
  //-------------------------------------------------------------------------//
  void ParamsError()
  {
    cerr << "PARAMETERS: {--float|--double} ConfigFile.ini" << endl;
    exit(1);
  }

  //-------------------------------------------------------------------------//
  // "SaveMatrix":                                                           //
  //-------------------------------------------------------------------------//
  // NB: Matrix is saved in a Matlab-compatible format, in a TRANSPOSED form,
  // so that rows correspond to Paths:
  //
  template<typename F>
  void SaveMatrix(Matrix<F> const& M, string const& fileName)
  {
    FILE* f = fopen(fileName.c_str(), "w");
    if (f == nullptr)
      throw runtime_error("SaveMatrix: Cannot open file: "+ fileName);

    int nk = int(M.size1());
    int P  = int(M.size2());
    fprintf(f, "%% Paths: %d; PathLen: %d\n", P, nk);

    for (int j = 0; j < P; ++j)
    {
      for (int i = 0; i < nk; ++i)
        fprintf(f, "%.9e  ", M(i,j));
      fprintf(f, "\n");
    }
    fclose(f);
  }

  // As above, but the Matrix is represented as Vector of Column Vectors.
  // It is again  saved in the TRANSPOSED form -- the columns (Paths) become
  // rows:
  template<typename F>
  void SaveMatrix(vector<Vector<F>> const& M, string const& fileName)
  {
    int P  = int(M.size());
    if (P == 0)
      return;
    int nk = int(M[0].size());

    FILE* f = fopen(fileName.c_str(), "w");
    if (f == nullptr)
      throw runtime_error("SaveMatrix: Cannot open file: "+ fileName);

    for (int j = 0; j < P; ++j)
    {
      if (int(M[j].size()) != nk)
          throw runtime_error
                ("SaveMatrix: Inhomogeneous Path #"+ to_string(j));
        
      for (int i = 0; i < nk; ++i)
        fprintf(f, "%.9e  ", M[j][i]);
      fprintf(f, "\n");
    }
    fclose(f);
  }

  //-------------------------------------------------------------------------//
  // Statistics:                                                             //
  //-------------------------------------------------------------------------//
  template<typename F>
  void DoStats(vector<F> const& X, int oQty, int H, char const* title, F alpha)
  {
    // Histogram of "X":
    int P = int(X.size());

    F minX =  Inf<F>();
    F maxX = -Inf<F>();
    for (int j = 0; j < P; ++j)
    {
      if (X[j] < minX)
        minX = X[j];
      if (X[j] > maxX)
        maxX = X[j];
    }

    // Divide the range [minX .. maxX] into (H+1) intervals:
    if (H <= 0)
      H = int(Ceil(F(5.0) * Log10(F(P))));
    assert(H >= 1);

    F  dX = (maxX - minX) / F(H);

    assert  (dX >= F(0.0));
    if (dX == F(0.0))
      H = 0;

    // Assemble the Histogram:
    vector<F> XHist(H + 1, F(0.0));

    if (dX != F(0.0))
      for (int j = 0; j < P; ++j)
      {
        int h = int(Floor((X[j] - minX) / dX));
        assert(0 <= h && h <= H);
        XHist[h] += F(1.0);
      }
    else
      // The X distribution is a delta-function:
      XHist[0] = F(P);

    // Normalise the Histogram:
    for (int h = 0; h <= H; ++h)
      XHist[h] /= F(P);

    // Print the Histogram, PER ONE OPTION:
    printf("\n%s Distribution (per 1 Option):\n", title);
    for (int h = 0; h <= H; ++h)
    {
      printf("%12.5e ", (minX + F(h) * dX) / F(abs(oQty)));
      int stars = int(Round(XHist[h] * F(100.0)));
      for (int s = 0; s <= stars; ++s)
        putchar('*');
      putchar('\n');
    }

    // Compute Standard Deviation:
    F sumX  = F(0.0);
    F sumX2 = F(0.0);
    for (int i = 0; i < P; ++i)
    {
      sumX  += X[i];
      sumX2 += X[i] * X[i];
    }
    F StdDev = SqRt((sumX2 - sumX * sumX / F(P)) / F(P-1));

    printf("\nStdDev (per 1 Option): %f\n\n", StdDev / F(abs(oQty)));

    // If we are given a finite "alpha", do also the VaR:
    if (IsFinite(alpha) && alpha > F(0.0))
    {
      F cum   = F(0.0);
      F VaR   = F(0.0);
      F node  = - Inf<F>();

      for (int h = 0; h <= H; ++h)
      {
        F prevCum  = cum;
        cum       += XHist[h];

        F prevNode = node;
        node       = minX + F(h) * dX;

        if (cum >= alpha)
        {
          if (h >= 1)
            // "alpha" is bracketed, can do interpolation:
            VaR = prevNode + (alpha - prevCum) * dX / XHist[h];
          else
            // Flat approximation only:
            VaR = node;
          break;
        }
      }
      printf("\nVaR at %f%% (per 1 Option): %f\n",
             F(100.0) * alpha, VaR / F(abs(oQty)));
    }
  }

  //=========================================================================//
  // "VolArbStrat":                                                          //
  //=========================================================================//
  // XXX: This Strategy assumes that we are using a "Diffusion2D" with unobser-
  // vable StochVol (this has an implication on the hedging strategy):
  //
  template<typename F>
  void VolArbStrat(string const& config_file)
  {
    //-----------------------------------------------------------------------//
    // Get the Params:                                                       //
    //-----------------------------------------------------------------------//
    ptree pt;
    ini_parser::read_ini(config_file, pt);

    // Diffusion Params:
    // "t0" is the same as Pricing Time:
    Diffusion_Params<F> diffParams;

    DateTime   tp        = DateTimeOfStr (pt.get<string>("MktData.Tp"));
    diffParams.m_type    = pt.get<string>("DiffusionParams.Type");
    diffParams.m_t0      = tp;
    diffParams.m_S0      = pt.get<F>     ("MktData.Sp");
    diffParams.m_withReg = true;

    // Diffusion Coeffs:
    Vector<F>  coeffs(11);
    string coeffNames[11] =
      { "V0", "mu", "xiN", "beta", "kappa", "a", "b", "eps", "etaN", "rho",
        "IR" };
    for (int i = 0; i < 11; ++i)
      coeffs[i] = pt.get<F>("DiffusionParams." + coeffNames[i]);

    diffParams.m_coeffs  = coeffs;

    //-----------------------------------------------------------------------//
    // Create the Diffusion:                                                 //
    //-----------------------------------------------------------------------//
    shared_ptr<Diffusion2D<F>> diff(MkDiffusion2D<F>(diffParams));

    //-----------------------------------------------------------------------//
    // Set other Params:                                                     //
    //-----------------------------------------------------------------------//
    // Option expiration time:
    DateTime te = DateTimeOfStr(pt.get<string>("OptionParams.Te"));
    if (te <= tp)
      throw invalid_argument
            ("VolArbStrat: Option expiration occurs in the past");

    int  P          = pt.get<int>("Misc.P");
    Time dt         = TimeOfStr(pt.get<string>("Misc.dt"));
    int  debugLevel = pt.get<int>("Misc.debug");

    if (P <= 0 || dt <= ZeroTime())
      throw invalid_argument("VolArbStrat: Invalid Monte Carlo param(s)");

    // Trading Calendar:
    string calName = pt.get<string>("OptionParams.cal");
    shared_ptr<TradingCalendar> cal =
      TradingCalendar::MkTradingCalendar(calName);

    // Create the TimeLine (IsFw=true):
    // For out-of-trading hours, use 0 time steps (so that these periods will
    // be bridged over):
    //
    Time dts[3]  = { dt, ZeroTime(), ZeroTime() };
    vector<TimeNode<F>> timeLine;

    MkTimeLine<true,F>(tp, dts, te, cal.get(), nullptr, &timeLine);
    int nk = int(timeLine.size());
    assert(nk >= 2);

    // Check that there are no time instants completely within non-trading hrs
    // in the "timeLine" (otherwise, we would not be able to re-hedge at that
    // time):
    for (int i = 0; i < nk; ++i)
      if (!((i >= 1 && timeLine[i-1].m_reg == TradingCalendar::TradingHours) ||
            timeLine[i].m_reg == TradingCalendar::TradingHours))
        throw runtime_error
              ("VolArbStrat: TimeLine[" + to_string(i) + "] is non-trading");

    // Is CUDA available?
    bool useCUDA  = pt.get<bool>("CUDA.ON");
    shared_ptr<CUDAEnv> cudaEnv;     // Initially contains NULL

    TimeNode<F>* cudaTimeLine = nullptr;
    F*           cudaS        = nullptr;
    F*           cudaV        = nullptr;

    if (useCUDA)
    {
      int  cudaDevID       = pt.get<int>("CUDA.DevID");
      int  cudaBlocksPerSM = pt.get<int>("CUDA.BlocksPerSM");
      int  cudaBlockSize   = pt.get<int>("CUDA.BlockSize");
      CUDAEnv::Params cudaParams(cudaDevID, cudaBlocksPerSM, cudaBlockSize);

      cudaEnv = shared_ptr<CUDAEnv>(new CUDAEnv(cudaParams));
    }

    // Create the Random Number Generator (with or without CUDA):
    RNG<F> rng(cudaEnv);

    // Matrices of Monte Carlo-simulated paths:
    Matrix<F> S, V;

    //-----------------------------------------------------------------------//
    // Option and Futures Params:                                            //
    //-----------------------------------------------------------------------//
    // XXX: Currently, a rather simplistic (time-periodic) hedging policy is
    // assumed. However, the effects of stock undivisibility ARE  taken into
    // account: we operate with intrgral stock numbers only:
    //
    // The option type and payoff:
    string oType  = pt.get<string>("OptionParams.OType");
    F      K      = pt.get<F>     ("OptionParams.K");
    bool   isCall =
      (oType == "Call")
      ? true
      :
      (oType == "Put")
      ? false
      : throw invalid_argument("VolArbStrat: Invalid Option Type: "+ oType);

    // The option price:
    F   oPx  = pt.get<F>  ("MktData.OPx");

    // The options qty:
    int oQty = pt.get<int>("OptionParams.OQty");

    // What is actually in "V"? Vols or Vars? Currently, it is Vols for SABR*
    // and Vars otherwise:
    bool isVol = (diffParams.m_type.find("SABR") != string::npos);

    // Hedging Method:
    string        hedgingMethodStr = pt.get<string>("Misc.hedging");
    HedgingMethod HM;
    if (hedgingMethodStr == "ModelVols")
      HM = HedgingMethod::ModelVols;
    else
    if (hedgingMethodStr == "ImpliedVols")
      HM = HedgingMethod::ImpliedVols;
    else
    if (hedgingMethodStr == "FilteredVols")
      HM = HedgingMethod::FilteredVols;
    else
      throw runtime_error
            ("VolArbStat: Invalid Hedging Method: "+ hedgingMethodStr);

    // Futures params (for hedging):
    F  dF = pt.get<F>("MktData.dF");    // Futures price step
    F  IR = pt.get<F>("MktData.r");     // Interest Rate (for Futures picing)

    //-----------------------------------------------------------------------//
    // Run Monte-Carlo!!! No Antithetic paths:                               //
    //-----------------------------------------------------------------------//
    cerr << "Running " << P << " Monte-Carlo Simulations... ";
    diff->MCPaths(timeLine,   P, rng,    &S, &V, nullptr, false,
                  &cudaTimeLine, &cudaS, &cudaV, nullptr);
    cerr << "Done"     << endl;

    if (int(S.size1()) != nk || int(S.size2()) != P ||
        int(V.size1()) != nk || int(V.size2()) != P)
      throw runtime_error("VolArbStrat: Invalid S and/or V size(s)");

    // Save the simulated results if required:
    string fileS = pt.get<string>("Misc.FileS");
    if (!fileS.empty())
      SaveMatrix<F>(S, fileS);

    string fileV = pt.get<string>("Misc.FileV");
    if (!fileV.empty())
      SaveMatrix<F>(V, fileV);

    //-----------------------------------------------------------------------//
    // Which Vols are to be used in Portfolio simulation?                    //
    //-----------------------------------------------------------------------//
    // Vectors of filtered (estimated) vols: one per path (as Matrix columns):
    Matrix<F> Ve;

    // Vector of implied vols: All Matrix columns will be same:
    Matrix<F> Vi;

    // The actual "V" we are going to use for hedging: Will be "V", "Ve" or
    // "Vi" depending on the Hedging Method:
    F const* Va = nullptr;

    //---------------------------------//
    if (HM == HedgingMethod::FilteredVols)
    //---------------------------------//
    {
      Ve.resize(nk, P);
      Va = &(Ve(0, 0));

      // Construct the BOF Grid params; "maxV" is the upper bound of "V" (stoch
      // vol or variance) on the Grid:
      F maxV = pt.get<F>  ("Misc.maxV");
      int n  = pt.get<int>("Misc.n");

      typename Grid2D<F>::Params gridParams =
        CondLLH2D_BayesOptFilt<F>::MkGridParamsStub
        (n, diffParams.m_S0, F(0.0), maxV, cudaEnv, cal);

      vector<MDItem<F>> simulatedMktData;

      for (int j = 0; j < P; ++j)
      {
        cerr << "Filtering Path #" << j;

        // Construct "synthetic MktData" from the "j"th simulated column:
        ZipMktData(tp, timeLine, &S(0,j), &simulatedMktData);
        assert(int(simulatedMktData.size()) == nk);

        // Construct the BOF LLH:
        CondLLH2D_BayesOptFilt<F> llh
          (diffParams, gridParams, simulatedMktData, debugLevel);

        // Run the filtering; the "NLLH" value itself  is of little interest
        // here -- we need the vols! NB: need to specify "coeffs" again:
        //
        F tmp =   F(0.0);
        llh.NLLH(coeffs, &tmp, nullptr, nullptr, &(Ve(0,j)));

        cerr << '\r';
      }

      // Save the filtered vols if requested:
      string fileVe = pt.get<string>("Misc.FileVe");
      if (!fileVe.empty())
        SaveMatrix<F>(Ve, fileVe);
    }
    else
    //---------------------------------//
    if (HM == HedgingMethod::ImpliedVols)
    //---------------------------------//
    {
      DateTime ts1 = tp + TimeGener::seconds(timeLine[1].m_secs);
      DateTime ts2 = te - dt;

      // Load Underlying Prices:
      string underlyingFile = pt.get<string>("MktData.UnderlyingPricesFile");
      vector<MDItem<F>> underlying;
      LoadMktData<F>
        (underlyingFile, tp, ts1, dt, ts2, cal, nullptr, &underlying);

      // Load Option Prices:
      string optionsFile    = pt.get<string>("MktData.OptionPricesFile");
      vector<MDItem<F>> options;
      LoadMktData<F>(optionsFile, tp, ts1, dt, ts2, cal, nullptr, &options);

      // Verify the times of all prices:
      bool timesOK = (int(underlying.size()) >= nk-1 &&
                      int(options.size())    >= nk-1);

      for (int i = 0; timesOK && i < nk-1; ++i)
      {
        DateTime ti = tp + TimeGener::seconds(timeLine[i].m_secs);
        timesOK    &= (underlying[i].m_t == ti && options[i].m_t == ti);
      }
      if (!timesOK)
        throw runtime_error("VolArbStrat: MktData not in sync with TimeLine");

      // Compute the Implied Vols. Make the size "nk" for compatibility with
      // other options:
      Vi.resize(nk, P);

      for (int i = 0; i < nk-1; ++i)
      {
        DateTime ti  = tp + TimeGener::seconds(timeLine[i].m_secs);
        F        Ci  = options[i].m_val;
        F        tau = YF<F>(ti, te);
        F        Si  = underlying[i].m_val;
        F    implVol = F(0.0);
        try
        {
          implVol = BSMImplVol<F>(isCall, Ci, Si, K, tau, IR, 0.0);
        }
        catch (exception const& e)
        {
          // XXX: This may happen; then just propagate the prev value:
          cerr << "WARNING: Cannot compute Implied Vol: t=" << ti
               << ", OptPx=" << Ci << ", Underlying=" << Si << ": "
               << e.what()   << endl;
          if (i >= 1)
            implVol = Vi(i-1,0);
          else
            throw;
        }

        // Set "implVol" is all colimns of "Vi", for consistency with other
        // matrices:
        for (int j = 0; j < P; ++j)
          Vi(i,j) = implVol;
      }
      // Propagate the final row (does not matter):
      assert(nk >= 2);
      for (int j = 0; j < P; ++j)
        Vi(nk-1, j) = Vi(nk-2, j);

      Va = &(Vi(0,0));
    }
    else
    //---------------------------------//
    // ModelVols:                      //
    //---------------------------------//
      Va = &(V(0,0));

    //-----------------------------------------------------------------------//
    // Simulate a Hedged Portfolio (for all Paths):                          //
    //-----------------------------------------------------------------------//
    // Accumulated Statistics:
    vector<F> PnL(P, F(0.0));
    vector<F> MDD(P, F(0.0));

    F* cudaVa  = nullptr;
    F* cudaPnL = nullptr;
    F* cudaMDD = nullptr;

    // Analyse the Paths:
    //
    if (useCUDA)
    {
      cudaEnv->Start();

      // Which "V" will CUDA use?
      if (HM == HedgingMethod::ImpliedVols ||
          HM == HedgingMethod::FilteredVols)
      {
        // Put Va (= Ve or Vi) into CUDA space as well:
        cudaVa = cudaEnv->CUDAAlloc<F>(nk * P);
        cudaEnv->ToCUDA<F>(Va, cudaVa, nk * P);
      }
      else
        // Va is already in the CUDA space:
        cudaVa = cudaV;

      // Allocate the CUDA output buffers:
      cudaPnL = cudaEnv->CUDAAlloc<F>(P);
      cudaMDD = cudaEnv->CUDAAlloc<F>(P);

      // Invoke the CUDA kernel:
      PortfolioPaths_KernelInvocator<F>::Run
      (
        *cudaEnv,
        nk, P,  cudaTimeLine,  cudaS, K,  isCall,  isVol,
        cudaVa, oQty,    oPx,  dF,    IR, cudaPnL, cudaMDD
      );

      // Copy the results back to Host space:
      cudaEnv->ToHost<F>(cudaPnL, PnL.data(), P);
      cudaEnv->ToHost<F>(cudaMDD, MDD.data(), P);

      cudaEnv->Stop();
    }
    else
      // Invoke the CPU version:
      PortfolioPaths_Core<F>
      (
        nullptr, nk, P,   timeLine.data(),    &(S(0,0)), K, isCall, isVol,
        Va, oQty, oPx, dF, IR, PnL.data(), MDD.data()
      );

    //-----------------------------------------------------------------------//
    // Statistics:                                                           //
    //-----------------------------------------------------------------------//
    F   alpha = pt.get<F>  ("Misc.alpha");
    int H     = pt.get<int>("Misc.H");

    // Do the PnL Distribution:
    DoStats(PnL, oQty, H, "PnL", alpha);

    // For comparison, get an estimated standad deviation of the underlying:
    F V0    = diff->GetV0();
    F sigma = isVol ? V0 : SqRt(V0);
    F sT    = SqRt(timeLine.back().m_ty - timeLine.front().m_ty);
    F S0    = diff->GetS0();
    F stdev = sigma * sT * S0;

    cout << "1 StdDev of the Underlying: " << stdev << endl << endl;

//  DoStats(MDD, oQty, H, "MDD", NaN<F>());

    //-----------------------------------------------------------------------//
    // De-Allocated CUDA Buffers:                                            //
    //-----------------------------------------------------------------------//
    if (cudaTimeLine != nullptr)
      cudaEnv->CUDAFree(cudaTimeLine);

    if (cudaS != nullptr)
      cudaEnv->CUDAFree(cudaS);

    if (cudaV != nullptr)
      cudaEnv->CUDAFree(cudaV);

    if (cudaVa != nullptr && HM != HedgingMethod::ModelVols)
      cudaEnv->CUDAFree(cudaVa);

    if (cudaPnL != nullptr)
      cudaEnv->CUDAFree(cudaPnL);

    if (cudaMDD != nullptr)
      cudaEnv->CUDAFree(cudaMDD);
  }
}

//===========================================================================//
// "main":                                                                   //
//===========================================================================//
int main(int argc, char* argv[])
{
  if (argc != 3)
    ParamsError();

  try
  {
    if (strcmp(argv[1], "--float")  == 0)
      VolArbStrat<float> (argv[2]);
    else
    if (strcmp(argv[1], "--double") == 0)
      VolArbStrat<double>(argv[2]);
    else
      ParamsError();
  }
  catch (exception const& e)
  {
    cerr << "ERROR: " << e.what() << endl;
    return 1;
  }
  return 0;
}
