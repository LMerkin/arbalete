// vim:ts=2:et
//===========================================================================//
//                            "DiagS35Test.cpp":                             //
//          Test of Solvers for 3- and 5-Diagonal Linear Systems             //
//===========================================================================//
#include "Common/Maths.hpp"
#include "Grids/DiagSolvers35.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <functional>
#include <random>
#include <cassert>

using namespace Arbalete;
using namespace std;


namespace
{
  void RandomVector(function<double()> const& rng, Vector<double>* v)
  {
    assert(v != nullptr);
    int n = int(v->size());
    for (int i = 0; i < n; ++i)
      (*v)[i] = rng();
  }
}

int main(int argc, char* argv[])
{
  if (argc != 3 && argc != 4)
  {
    cerr << "PARAMETERS: <SP (3|5)> <MainDiagSize (>=2)> [Scale]" << endl;
    return 1;
  }
  int    const SP    = atoi(argv[1]);
  int    const K     = atoi(argv[2]);
  double const scale = (argc == 4) ? atof(argv[3]) : 1000.0;

  if ((SP != 3 && SP != 5) || K <= 1)
  {
    cerr << "ERROR: Invalid arg(s)" << endl;
    return 1;
  }

  int const M = SP / 2;
  assert(M == 1 || M == 2);

  // Generate a random system:
  //
  uniform_real_distribution<double> distr(-scale, scale);
  ranlux48_base eng;
  function<double()> rng = bind(distr, eng);

  Vector<double>* diags0 = new Vector<double>[SP];
  Vector<double>* diags1 = new Vector<double>[SP];

  Vector<double>  rhs0;
  Vector<double>  rhs1;

  diags0[M].resize(K);
  diags1[M].resize(K);
  rhs0.resize(K);
  rhs1.resize(K);
  for (int q = 1; q <= M; ++q)
  {
    diags0[M+q].resize(K-q);
    diags0[M-q].resize(K-q);
    diags1[M+q].resize(K-q);
    diags1[M-q].resize(K-q);
  }

  // NB: also Make copies of the vectors created, as they will be destroyed
  // by the solver:
  for (int q = 0; q < SP; ++q)
  {
    RandomVector(rng, diags0 + q);
    noalias(diags1[q]) = diags0[q];
  }
  RandomVector(rng, &rhs0);
  noalias(rhs1) = rhs0;

  // Solve the system:
  if (SP == 3)
    DiagSolver<double, 1>::Solve(diags0, &rhs0);
  else
    DiagSolver<double, 2>::Solve(diags0, &rhs0);

  // Verify the solution:
  double err = 0.0;

  for (int i = 0; i < K; ++i)
  {
    double res = 0.0;
    for (int j = max<int>(i-M, 0); j <= min<int>(i+M, K-1); ++j)
    {
      // The diag and pos to which this "j" corresponds:
      int d = j + M - i;
      int p = min<int>(i,j);
      res += diags1[d][p] * rhs0[j];
    }
    err = max<double>(err, Abs(res - rhs1[i]));
  }
  cout << "Max Error = " << err << endl;

  delete[] diags0;
  delete[] diags1;
  return 0;
}
