// vim:ts=2:et
//===========================================================================//
//                                "OUTest.cpp":                              //
//                   Convergence Test for the OU Diffusion                   //
//===========================================================================//
#include "Common/Maths.hpp"
#include "Common/VecMatTypes.h"
#include "Common/TradingCalendar.h"
#include "Diffusions/Diffusion1D_OU.hpp"
#include "MonteCarlo/RNG.h"
#include "Calibration/MarketData.h"
#include "Calibration/RealCalibrParams.h"
#include "Calibration/RealCalibrator.h"
#include <boost/program_options.hpp>
#include <cstdlib>
#include <string>
#include <cassert>

using namespace std;
using namespace Arbalete;
namespace pro = boost::program_options;


int main(int argc, char* argv[])
{
  //-------------------------------------------------------------------------//
  // Get the command-line options:                                           //
  //-------------------------------------------------------------------------//
  double a       =  0.0;
  double b       =  1.0;
  double kappa   = 25.0;
  double sigma   =  0.3;
  double z0      =  0.8;

  int    P       =  100;
  int debugLevel = 0;

  string T_str   = "60d";  // Time horizon
  string dt_str  = "1h";   // Time step

  string llhMethod    = "Analytical";
  string calibrMethod = "MLSL-IPOpt";

  pro::options_description desc("Allowed options");
  desc.add_options()
    ("a",      pro::value<double>(), "Intercept of the trend: a*(t-t0) + b")
    ("b",      pro::value<double>(), "Slope     of the trend: a*(t-t0) + b")
    ("kappa",  pro::value<double>(), "Mean-reversion rate")
    ("sigma",  pro::value<double>(), "Volatility" )
    ("z0",     pro::value<double>(), "Initial value at t0")
    ("P",      pro::value<int>(),    "Number of Paths to simulate")
    ("T",      pro::value<string>(), "Simulation time horizon, eg days")
    ("dt",     pro::value<string>(), "Simulation time step,    eg mins")
    ("LLH",    pro::value<string>(), "LLH    Method: Analytical | TransPDF")
    ("calibr", pro::value<string>(), "Calibr Method: MLSL-IPOpt | DIRect-IPOpt"
                                     " | MLSL-E04JYF | DIRect-E04JYF")
    ("debug",  pro::value<int>(),    "Debug Level")
  ;
  pro::variables_map vm;
  pro::store (pro::parse_command_line(argc, argv, desc), vm);
  pro::notify(vm);

  if (vm.count("a"))
    a = vm["a"].as<double>();

  if (vm.count("b"))
    b = vm["b"].as<double>();

  if (vm.count("kappa"))
    kappa = vm["kappa"].as<double>();

  if (vm.count("z0"))
    z0 = vm["z0"].as<double>();

  if (vm.count("P"))
    P  = vm["P"].as<int>();

  if (vm.count("T"))
    T_str = vm["T"].as<string>();

  if (vm.count("dt"))
    dt_str = vm["dt"].as<string>();

  if (vm.count("LLH"))
    llhMethod = vm["LLH"].as<string>();

  if (vm.count("calibr"))
    calibrMethod = vm["calibr"].as<string>();

  if (vm.count("debug"))
    debugLevel = vm["debug"].as<int>();

  // Verify the Params:
  if (P <= 1)
  {
    cerr << "Invalid number of Paths: " << P << ", min = 2" << endl;
    return 1;
  }
  Time   T  = TimeOfStr(T_str);
  Time   dt = TimeOfStr(dt_str);

  //-------------------------------------------------------------------------//
  // Construct the Diffusion:                                                //
  //-------------------------------------------------------------------------//
  DateTime t0(Date(2013, 3, 25), Time(10, 0, 0));

  Vector<double> coeffsO(4);
  coeffsO[0] = a;
  coeffsO[1] = b;
  coeffsO[2] = kappa;
  coeffsO[3] = sigma;

  Diffusion_Params<double> qpParams { "OU", t0, z0, false, coeffsO };
  Diffusion1D_OU  <double> qpDiff   (qpParams);

  //-------------------------------------------------------------------------//
  // Simulate the Paths:                                                     //
  //-------------------------------------------------------------------------//
  // Construct the Time Line:
  //
  vector<TimeNode<double>> timeLine;
  Time dts[3]    = { dt, dt, dt };

  string calName = "SX5E";
  shared_ptr<TradingCalendar> cal =
    TradingCalendar::MkTradingCalendar(calName);

  // IsFwd = true:
  MkTimeLine<true, double>(t0, dts, t0 + T, cal.get(), nullptr, &timeLine);
  int nk = int(timeLine.size());

  if (debugLevel >= 2)
  {
    cout << "TimeLine for MC Simulations of Paths:" << endl;
    for (int i = 0; i < nk; ++i)
    {
      TimeNode<double> const& tn = timeLine[i];
      DateTime ti = t0 + TimeGener::seconds(tn.m_secs);
      cout << DateTimeToStr(ti)   << ": reg=" << int(tn.m_reg) << ", ty="
           << tn.m_ty << ", stk=" << tn.m_stk << endl;
    }
    cout << endl;
  }

  // Create the RNG:
  shared_ptr<CUDAEnv> none(nullptr);
  RNG<double> rng(none);

  // Output vector:
  Matrix<double> zs;

  // Run MC: No IR, no Antithetic:
  qpDiff.MCPaths(timeLine, P, rng, &zs, nullptr, false);

  assert(int(zs.size1()) == nk && int(zs.size2()) == P);

  //-------------------------------------------------------------------------//
  // Set up Calibration Params for MLSL-IPOpt:                               //
  //-------------------------------------------------------------------------//
  // Construct a vector of "MDItem"s in which simulated paths will be
  // installed:
  shared_ptr<vector<MDItem<double>>> mktData(new vector<MDItem<double>>(nk));

  // Calibration params for coeffs re-construction:
  RealCalibrParams<double> params(qpParams.m_type, mktData, calName);

  params.m_withReg        = qpParams.m_withReg;
  params.m_gridMethod     = llhMethod;  // "LLH method" is a more correct term
  params.m_calibrMethod   = calibrMethod;
  params.m_maxGlobalPts   = 500;        // Per dim
  params.m_rectFrac       = 0.005;      // Per dim
  params.m_mlsl_maxRounds = 10;
  params.m_mlsl_maxBest   = 3;
  params.m_debugLevel     = debugLevel;

  typedef RealCalibrParams<double>::CIGType         CT;
  typedef RealCalibrParams<double>::CalibrInitGuess IG;

  typedef RealCalibrParams<double>::CBType          CB;
  typedef RealCalibrParams<double>::CalibrBounds    Bounds;

  // XXX: The Initial Guess is not really used by MLSL?
  vector<IG> igs
  {
    { CT::CIGExpl, a + 1.0    },   // a
    { CT::CIGExpl, b + 0.5    },   // b
    { CT::CIGExpl, kappa / 2.0 },  // kappa
    { CT::CIGExpl, sigma * 2.0 }   // sigma
  };
  params.m_initGuess = igs;

  // Bounds:
  vector<Bounds> bounds
  {
    { CB::CBExpl,  a - 1.0, CB::CBExpl, a + 2.0    },
    { CB::CBExpl,  0.0,     CB::CBExpl, b + 2.0    },
    { CB::CBExpl,  0.01,    CB::CBExpl, kappa * 3.0 },
    { CB::CBExpl,  0.01,    CB::CBExpl, sigma * 3.0 }
  };
  params.m_bounds = bounds;

  //-------------------------------------------------------------------------//
  // Run Calibration:                                                        //
  //-------------------------------------------------------------------------//
  // Expected values and standard deviations of coeffs obtained:
  //
  Vector <double>  CS (4);   // Sums
  ZeroOut<double>(&CS);

  Vector <double>  CS2(4);   // Sums of Squares
  ZeroOut<double>(&CS2);

  for (int i = 0; i < P; ++i)
  {
    // Synthesise MktData from the generated path. The MktData are installed
    // straight into the "params":
    ZipMktData(t0, timeLine, &(zs(0,i)), mktData.get());
    assert(int(mktData->size()) == nk);

    if (debugLevel >= 2)
    {
      cout << "Synthetic MktData for Calibration: nk=" << nk << ", MktDataSz="
           << mktData->size() << endl;
      for (int j = 0; j < nk; ++j)
      {
        MDItem<double> const& md = (*mktData)[j];
        cout << DateTimeToStr(md.m_t) << ": reg=" << md.m_reg << ", z="
             << md.m_val << endl;
      }
      cout << endl;
    }

    // Coeffs (initial guess -> calibration result):
    Vector<double> coeffsR(4);
    double llh;

    // Run the Calibrator:
    RealCalibrator<double> calibr(params);
    calibr.Calibrate     (&coeffsR, &llh);

    cout << "Calibrated: LLH=" << llh << '\n' << coeffsR << endl;
    for (int i = 0; i < 4; ++i)
    {
      CS [i] += coeffsR[i];
      CS2[i] += Sqr<double>(coeffsR[i]);
    }
  }
  // Compute expected values and standard deviations:
  for (int i = 0; i < 4; ++i)
  {
    CS2[i]  = SqRt((CS2[i] - Sqr<double>(CS[i]) / double(P)) / double(P-1));
    CS [i] /= double(P);
  }
  cout << endl << endl;
  cout << "Expected Values     of Calibrated Params:\n" << CS  << endl;
  cout << "Standard Deviations of Calibrated Params:\n" << CS2 << endl;
  cout << "True     Values:\n" << coeffsO << endl << endl;

  return 0;
}
