// vim:ts=2:et
//===========================================================================//
//                          "TimeStepperTest.cpp":                           //
//===========================================================================//
#include "Common/Maths.hpp"
#include "Grids/TimeStepper_Fact.h"
#include <boost/program_options.hpp>
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <string>

using namespace std;
using namespace Arbalete;
namespace pro = boost::program_options;

namespace
{
  DateTime t0(Date(2012, 1, 1));

  //-------------------------------------------------------------------------//
  // RHS for Testing:                                                        //
  //-------------------------------------------------------------------------//
  void TestDDt
    (DateTime t, TradingCalendar::CalReg UNUSED_PARAM(reg),
     double*  f, double* DfDt)
  {
    double ty = YF<double>(t0, t);
    DfDt[0] = - 2.0 * f[0] + ty;
    DfDt[1] =   3.0 * f[1] + 2.0 * ty;
  };
}

//===========================================================================//
// "main":                                                                   //
//===========================================================================//
int main(int argc, char* argv[])
{
  //-------------------------------------------------------------------------//
  // Get the command-line options:                                           //
  //-------------------------------------------------------------------------//
  // Default values:
  string time_stepper = "RKF5";
  double T            = 5.0;      // Years
  double dt           =   1;      // Days

  pro::options_description desc("Allowed options");
  desc.add_options()
    ( "method", pro::value<string>(),
                "Time Stepper Method: RKC1_n|RKC2_n|RKF5" )
    ( "T",      pro::value<double>(), "Time to Expiration (years)" )
    ( "dt",     pro::value<double>(), "Time Step (days)"  );

  pro::variables_map vm;
  pro::store (pro::parse_command_line(argc, argv, desc), vm);
  pro::notify(vm);

  if (vm.count("method"))
    time_stepper = vm["method"].as<string>();

  if (vm.count("T"))
    T  = vm["T"].as<double>();

  if (vm.count("dt"))
    dt = vm["dt"].as<double>();

  if (T <= 0.0 || dt <= 0.0)
  {
    cerr << "ERROR: T, dt must be > 0" << endl;
    return 1;
  }
  double dty = dt / 365.25; // Now in years

  cout << "dty = " << dty << endl;
  cout << "T   = " << T   << endl;

  //-------------------------------------------------------------------------//
  // Construct the Diffusion and the Grid:                                   //
  //-------------------------------------------------------------------------//
  TimeStepper<double> const* stepper =
    MkTimeStepper<double>(time_stepper, 2, shared_ptr<CUDAEnv>(nullptr));

  double f[2] = { 1.0, 1.0 };

  //-------------------------------------------------------------------------//
  // Run Integration:                                                        //
  //-------------------------------------------------------------------------//
  double   errS  = 0.0;
  double   errV  = 0.0;
  double   ty    = 0.0;

  DateTime tc    = t0;
  while (true)
  {
    double t_next = ty + dty;
    if (t_next > T)
      break;

    // Make the time step:
    DateTime tn = t0 + TimeOfYF<double>(t_next);

    stepper->Propagate(tc, TradingCalendar::TradingHours, f, tn, TestDDt);
    ty = t_next;
    tc = tn;

    // Theoretical values:
    double f1th =  1.25 * Exp(-2.0 * ty) + 0.5 * ty   - 0.25;
    double f2th = (11.0 * Exp( 3.0 * ty) - 2.0) / 9.0 - (2.0/3.0) * ty;

    // The errors are relative:
    errS = max<double>(errS, Abs(1.0 - f[0] / f1th));
    errV = max<double>(errV, Abs(1.0 - f[1] / f2th));
  }
  cout << "errS  = " << errS << endl;
  cout << "errV  = " << errV << endl;

  return 0;
}
