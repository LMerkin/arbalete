// vim:ts=2:et
//===========================================================================//
//                          "PayoffReplication.cpp":                         //
//                   Monte-Carlo Simulation of Option Paths                  //
//===========================================================================//
#include <Common/TradingCalendar.h>
#include <Common/BSM.hpp>
#include "Diffusions/Diffusion1D_Fact.h"
#include <boost/program_options.hpp>
#include <iostream>
#include <cstring>
#include <stdexcept>
#include <fstream>
#include <memory>

using namespace Arbalete;
using namespace std;
namespace pro = boost::program_options;

namespace
{
  //=========================================================================//
  // Product (Option) Types:                                                 //
  //=========================================================================//
  enum class OptionT
  {
    ECall          = 0, // Classical European Call
    EPut           = 1, // Classical European Put
    ZeroCostLoan   = 2, // Zero-IR Loan in RFC (eg USD), collateral in X
    EnhancedRetX   = 3, // Enhanced return in X   (eg BTC)
    EnhancedRetRFC = 4, // Enhanced return in RFC (eg USD)
    DualCcy        = 5
  };
  //=========================================================================//
  // "Along-the-Path" Data:                                                  //
  //=========================================================================//
  struct PathElt
  {
    // Fields:
    double m_td;        // Time in Days from the commencement
    double m_S;         // Curr S_t
    double m_Delta;     // Delta of the portfolio
    double m_DeltaVal;  // Delta  in RFC
    double m_dDelta;    // Delta change from the prev PathElt
    double m_dDeltaVal; // dDelta in RFC
    double m_cash;      // Cash position
    double m_totalVal;  // Total pos value

    // Default Ctor:
    PathElt()  { memset(this, '\0', sizeof(PathElt)); }
  };
  using SamplePath = vector<PathElt>;

  //=========================================================================//
  // "PathEvaluator" Struct:                                                 //
  //=========================================================================//
  // "S": array of underlying pxs of length "L":
  //
  struct PathEvaluator
  {
    //-----------------------------------------------------------------------//
    // Data Flds:                                                            //
    //-----------------------------------------------------------------------//
    double m_Ty;     // Tenor in Years
    double m_S;      // Curr Underlying/RFC px
    double m_delta;  // Position in Underlying Asset (eg BTC)
    double m_cash;   // Position in Risk-Free  Asset (eg USD)
    double m_total;  // Total portfolio value

    // Min/Max/Avg vals:
    double m_minDelta;
    double m_maxDelta;
    double m_minCash;
    double m_maxCash;
    double m_minTotal;
    double m_maxTotal;
    double m_avgBorrow;

    //-----------------------------------------------------------------------//
    // Default Ctor:                                                         //
    //-----------------------------------------------------------------------//
    PathEvaluator()
      { memset(this, '\0', sizeof(PathEvaluator)); }

    //-----------------------------------------------------------------------//
    // "OnPath":                                                             //
    //-----------------------------------------------------------------------//
    // "L" is the length of both "t" and "S":
    // Option::OptionDelta: (S, tau) -> (Delta, IR):
    //
    template<typename Option>
    void OnPath
    (
      TimeNode<double> const* t,
      double const*           S,
      int                     L,
      Option const&           option,
      double                  initCash,
      SamplePath*             samplePath  // May be NULL
    )
    {
      // Initialize the obj:
      m_Ty    = 0.0;
      m_S     = 0.0;
      m_delta = 0.0;
      m_cash  = initCash;
      m_total = initCash;

      assert(L  > 0 && m_delta == 0.0);
      double Ty = t[L-1].m_ty;

      // SamplePath, if non-NULL, is of length "L" as well:
      assert(samplePath == nullptr || int(samplePath->size()) == L);
      PathElt* elt =
        (samplePath == nullptr) ? nullptr : &((*samplePath)[0]);

      // Traverse the Path:
      for (int l = 0; l < L; ++l)
      {
        // Time to expiration:
        double ty  = t[l].m_ty;
        double tau = Ty - ty;
        assert(tau >= 0.0);

        // Compute the new Delta:
        double St       = S[l];
        auto   res      = option.OptionDelta(St, tau);
        double delta    = res.first;       // Delta
        double r        = res.second;      // RFC IR

        // ReHedging: Changing Delta, at a new Px: this incurs change in Cash:
        double dCash    = (m_delta - delta) * St;
        double dty      = (l == 0) ? 0.0 : (ty - t[l-1].m_ty);    // Time step
        double prevCash = m_cash;

        // Update the state:
        m_Ty            = ty;
        m_S             = St;
        m_delta         = delta;
        m_cash         *= Exp(r * dty);
        m_cash         += dCash;
        m_total         = m_cash  + m_S * m_delta;

        // Set the Min/Max vals:
        m_minDelta      = Min(m_minDelta, m_delta);
        m_maxDelta      = Max(m_maxDelta, m_delta);
        m_minCash       = Min(m_minCash,  m_cash);
        m_maxCash       = Max(m_maxCash,  m_cash);
        m_minTotal      = Min(m_minTotal, m_total);
        m_maxTotal      = Max(m_maxTotal, m_total);

        // For Cash, we also need the Integral Average over Time: NB: use the
        // "prevCash" over the (prev) "dty" here:
        double borrow   = (prevCash < 0.0) ? (- prevCash) : 0.0;
        m_avgBorrow    += borrow * dty;

        // Save data to "samplePath" if provided:
        if (elt != nullptr)
        {
          elt->m_td        = ty * 365.0;
          elt->m_S         = St;
          elt->m_Delta     = delta;
          elt->m_DeltaVal  = delta * St;
          // Cannot compute "dDelta*" at the initial point, replace with 0:
          elt->m_dDelta    =
            (l == 0) ? 0.0 : (elt->m_Delta    - (elt-1)->m_Delta);
          elt->m_dDeltaVal =
            (l == 0) ? 0.0 : (elt->m_DeltaVal - (elt-1)->m_DeltaVal);
          elt->m_cash      = m_cash;
          elt->m_totalVal  = elt->m_DeltaVal + elt->m_cash;
          ++elt;
        }
      }
      m_avgBorrow /= Ty;
    }
  };

  //=========================================================================//
  // "OutputRess":                                                           //
  //=========================================================================//
  template<typename Option>
  void OutputRess
  (
    fstream*             a_statsS,
    fstream*             a_pathS,
    PathEvaluator const& a_pv,
    Option        const& a_option,
    SamplePath*          a_sample
  )
  {
    // The final (presulably) point:
    double ST     = a_pv.m_S;

    // The payoff at that point is the final Option Px (when tau=T-t=0):
    double payoff = a_option.OptionPx(ST, 0.0).first;

    if (a_statsS != nullptr)
    {
      // Output the Stats:
      // The remaining balance (P&L) after the payoff -- going to be our PnL:
      double total  = a_pv.m_total;
      double PnL    = total - payoff;

      // Return relative to the AVERAGE cash borrowing, %/month (1/12 year):
      double monRet = (PnL / a_pv.m_avgBorrow) / a_pv.m_Ty / 12.0 * 100.0;

      // Output (also including the Min Cash pos over the path):
      *a_statsS << ST << '\t' << total << '\t' << PnL << '\t'
                << a_pv.m_minCash      << '\t' << (- a_pv.m_avgBorrow) << '\t'
                << monRet              << endl;
    }
    if (a_pathS != nullptr && a_sample != nullptr)
    {
      // Output the Sample Path, incl the threshold for the Final PayOff:
      int      L   = int(a_sample->size());
      PathElt* elt = &((*a_sample)[0]);

      for (int i = 0; i < L; ++i, ++elt)
        *a_pathS << elt->m_td     << '\t' << elt->m_S         << '\t'
                 << elt->m_Delta  << '\t' << elt->m_DeltaVal  << '\t'
                 << elt->m_dDelta << '\t' << elt->m_dDeltaVal << '\t'
                 << elt->m_cash   << '\t' << elt->m_totalVal  << '\t'
                 << payoff        << endl;
    }
  }

  //=========================================================================//
  // "Option" Class:                                                         //
  //=========================================================================//
  // It is for convenience only (for use in "main"); "PathEvaluator" uses its
  // derived classes via a template parameter, for efficiency:
  // ces here:
  //
  class Option
  {
  public:
    // Default Ctor, Virtual Dtor:
    Option() {}
    virtual ~Option() {}

    // "OptionPx":    Returns (OptionPx, IR):
    virtual pair<double, double> OptionPx
      (double a_S, double a_tau) const = 0;

    // "OptionDelta": Returs (Delta,     IR):
    virtual pair<double, double> OptionDelta
      (double a_S, double a_tau) const = 0;
  };

  //=========================================================================//
  // "ECall" Class: European Call:                                           //
  //=========================================================================//
  class ECall: public Option
  {
  private:
    //-----------------------------------------------------------------------//
    // Data Flds:                                                            //
    //-----------------------------------------------------------------------//
    // Payoff:
    double m_K;

    // Model Params (XXX: must not be here):
    double m_sigma;
    double m_r;

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor:                                                     //
    //-----------------------------------------------------------------------//
    ECall(double a_k, double a_sigma, double a_r)
    : Option (),
      m_K    (a_k),
      m_sigma(a_sigma),
      m_r    (a_r)
    {
      if (m_K <= 0.0 || m_sigma <= 0.0)
        throw invalid_argument("ECall: Invalid Strike or Vol");
    }

    //-----------------------------------------------------------------------//
    // "OptionPx":                                                           //
    //-----------------------------------------------------------------------//
    // Returns (Px, IR):
    //
    pair<double, double> OptionPx(double a_S, double a_tau)    const override
    {
      double px = BSMCallPx(a_S, m_K, m_sigma, a_tau, m_r);
      return make_pair(px, m_r);
    }

    //-----------------------------------------------------------------------//
    // "OptionDelta":                                                        //
    //-----------------------------------------------------------------------//
    // Returns (Delta, IR):
    //
    pair<double, double> OptionDelta(double a_S, double a_tau) const override
    {
      double delta = BSMCallDelta(a_S, m_K, m_sigma, a_tau, m_r);
      return make_pair(delta, m_r);
    }
  };

  //=========================================================================//
  // "EPut" Class: European Put:                                             //
  //=========================================================================//
  class EPut: public Option
  {
  private:
    //-----------------------------------------------------------------------//
    // Data Flds:                                                            //
    //-----------------------------------------------------------------------//
    // Payoff:
    double m_K;

    // Model Params (XXX: must not be here):
    double m_sigma;
    double m_r;

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor:                                                     //
    //-----------------------------------------------------------------------//
    EPut(double a_k, double a_sigma, double a_r)
    : Option (),
      m_K    (a_k),
      m_sigma(a_sigma),
      m_r    (a_r)
    {
      if (m_K <= 0.0 || m_sigma <= 0.0)
        throw invalid_argument("EPut: Invalid Strike or Vol");
    }

    //-----------------------------------------------------------------------//
    // "OptionPx":                                                           //
    //-----------------------------------------------------------------------//
    // Returns (Px, IR):
    //
    pair<double, double> OptionPx(double a_S, double a_tau)    const override
    {
      double px = BSMPutPx(a_S, m_K, m_sigma, a_tau, m_r);
      return make_pair(px, m_r);
    }

    //-----------------------------------------------------------------------//
    // "OptionDelta":                                                        //
    //-----------------------------------------------------------------------//
    // Returns (Delta, IR):
    //
    pair<double, double> OptionDelta(double a_S, double a_tau) const override
    {
      double delta = BSMPutDelta(a_S, m_K, m_sigma, a_tau, m_r);
      return make_pair(delta, m_r);
    }
  };

  //=========================================================================//
  // "ZeroCostLoan" Class:                                                   //
  //=========================================================================//
  // The payoff is Call(K1) - Call(K2), so the Delta is the difference of
  // two Call deltas as well:
  //
  class ZeroCostLoan: public Option
  {
  private:
    //-----------------------------------------------------------------------//
    // Data Flds:                                                            //
    //-----------------------------------------------------------------------//
    // Payoff Params:
    double m_K1;
    double m_K2;

    // XXX: Model Params: They should NOT be here, actually:
    double m_sigma;
    double m_r;

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor:                                                     //
    //-----------------------------------------------------------------------//
    ZeroCostLoan(double a_k1, double a_k2, double a_sigma, double a_r)
    : Option (),
      m_K1   (a_k1),
      m_K2   (a_k2),
      m_sigma(a_sigma),
      m_r    (a_r)   // Risk-free interest rate on RFC, can be of any sign
    {
      if (m_K1 <= 0.0 || m_K1 >= m_K2 || m_sigma <= 0.0)
        throw invalid_argument("ZeroCostLoan: Invalid Strike(s) or Vol");
    }
    //-----------------------------------------------------------------------//
    // "OptionPx":                                                           //
    //-----------------------------------------------------------------------//
    // Returns (Px, IR):
    //
    pair<double, double> OptionPx(double a_S, double a_tau)    const override
    {
      double px = BSMCallPx      (a_S, m_K1, m_sigma, a_tau, m_r) -
                  BSMCallPx      (a_S, m_K2, m_sigma, a_tau, m_r);
      return make_pair(px, m_r);
    }
    //-----------------------------------------------------------------------//
    // "OptionDelta":                                                        //
    //-----------------------------------------------------------------------//
    // Returs (Delta, IR):
    //
    pair<double, double> OptionDelta(double a_S, double a_tau) const override
    {
      double delta = BSMCallDelta(a_S, m_K1, m_sigma, a_tau, m_r) -
                     BSMCallDelta(a_S, m_K2, m_sigma, a_tau, m_r);
      return make_pair(delta, m_r);
    }
  };

  //=========================================================================//
  // "EnhancedRetX":                                                         //
  //=========================================================================//
  // The payoff is S - Call(K):
  //
  class EnhancedRetX: public Option
  {
  private:
    //-----------------------------------------------------------------------//
    // Data Flds:                                                            //
    //-----------------------------------------------------------------------//
    // Payoff Params:
    double m_alpha;
    double m_S0;
    double m_K1;
    double m_K2;
    double m_K3;
    double m_c0;
    double m_c1;

    // XXX: Model Params: They should NOT be here, actually:
    double m_sigma;
    double m_r;

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor:                                                     //
    //-----------------------------------------------------------------------//
    EnhancedRetX(double a_alpha, double a_s0, double a_k1,    double a_k2,
                 double a_k3,    double a_p,  double a_sigma, double a_r)
    : Option  (),
      m_S0    (a_s0),
      m_K1    (a_k1),
      m_K2    (a_k2),
      m_K3    (a_k3),
      m_c0    ( 1.0  + a_alpha),
      m_c1    ((m_c0 * m_K1 - a_p) / (m_K3 - m_K2)),
      m_sigma (a_sigma),
      m_r     (a_r)       // Risk-free interest rate on RFC, can be of any sign
    {
      // We must have   0 < K1 < K2 < S0 < K3 <= K4 < K5;
      // the synthetic interest rate "alpha" is achieved only for S in [0, K1]:
      //
      if (m_S0 <= 0.0 || m_S0    >  m_K1 || m_K1 > m_K2 || m_K2 >= m_K3 ||
          a_p  <  0.0 || m_sigma <= 0.0)
        throw invalid_argument("EnhancedRetX: Invalid S0, Strike(s) or Vol");
    }
    //-----------------------------------------------------------------------//
    // "OptionPx":                                                           //
    //-----------------------------------------------------------------------//
    // Returns (Px, IR):
    //
    pair<double, double> OptionPx(double a_S, double a_tau)    const override
    {
      double px =
          m_c0 * (a_S - BSMCallPx(a_S, m_K1, m_sigma, a_tau, m_r))
        - m_c1 * (      BSMCallPx(a_S, m_K2, m_sigma, a_tau, m_r) -
                        BSMCallPx(a_S, m_K3, m_sigma, a_tau, m_r));
      return make_pair(px, m_r);
    }
    //-----------------------------------------------------------------------//
    // "OptionDelta":                                                        //
    //-----------------------------------------------------------------------//
    // Returs (Delta, IR):
    //
    pair<double, double> OptionDelta(double a_S, double a_tau) const override
    {
      double delta =
          m_c0 * (1.0 - BSMCallDelta(a_S, m_K1, m_sigma, a_tau, m_r))
        - m_c1 * (      BSMCallDelta(a_S, m_K2, m_sigma, a_tau, m_r) -
                        BSMCallDelta(a_S, m_K3, m_sigma, a_tau, m_r));
      return make_pair(delta, m_r);
    }
  };
}

//===========================================================================//
// "main":                                                                   //
//===========================================================================//
int main(int argc, char* argv[])
{
  try
  {
    //-----------------------------------------------------------------------//
    // CmdL Params:                                                          //
    //-----------------------------------------------------------------------//
    pro::options_description desc("Allowed options");
    desc.add_options()
      // Diffusion params:
      ("Diffusion", pro::value<string>(), "Diffusion (Model), eg GBM")
      ("S0",        pro::value<double>(), "S(t0)")
      ("mu",        pro::value<double>(), "Trend coeff for GBM")
      ("sigmaMod",  pro::value<string>(), "Vol   coeff for GBM")
      ("sigmaAct",  pro::value<string>(), "Actual volatility" )
      ("r",         pro::value<double>(), "Risk-free interest rate")
      // Option params:
      ("OptionT",   pro::value<string>(), "Payoff,  eg ZeroCostLoan")
      ("K1",        pro::value<double>(), "1st Strike")
      ("K2",        pro::value<double>(), "2nd Strike")
      ("K3",        pro::value<double>(), "3rd Strike (EnhancedRetX only)")
      ("LoanAmt",   pro::value<double>(), "ZeroCostLoan: Loan Amt")
      ("LiqThresh", pro::value<double>(), "ZeroCostLoan: Liq'tion Threshold")
      ("ProtAmt",   pro::value<double>(), "EnhancedRetX: Protected Amt")
      ("alpha",     pro::value<double>(), "EnhancedRetX: Return rate")
      // Simulation params:
      ("t0",        pro::value<string>(), "Start     : YYYY-MM-DD_hh:mm:ss")
      ("T",         pro::value<string>(), "Expiration: YYYY-MM-DD_hh:mm:ss")
      ("tauMin",    pro::value<int>   (), "Simulation Step, minutes")
      ("P",         pro::value<int>   (), "Number of Minte-Carlo paths")
      // Output Files:
      ("statsFile", pro::value<string>(), "Output file for over-all Stats")
      ("pathFile",  pro::value<string>(), "Output file for a smaple Path");
  
    // Load the CmdL args:
    pro::variables_map vm;
    pro::store (pro::parse_command_line(argc, argv, desc), vm);
    pro::notify(vm);
  
    //-----------------------------------------------------------------------//
    // Diffusion Params:                                                     //
    //-----------------------------------------------------------------------//
    string diffName = "GBM";
    if (vm.count("Diffusion"))
      diffName = vm["Diffusion"].as<string>();
  
    double S0 = 10000.0;
    if (vm.count("S0"))
    {
      S0 = vm["S0"].as<double>();
      if (S0  <= 0.0)
        throw invalid_argument("S0=" + to_string(S0));
    }
    double mu = 0.0;
    if (vm.count("mu"))
      mu = vm["mu"].as<double>();
  
    double sigmaMod = 0.5;
    if (vm.count("sigmaMod"))
    {
      string sigmaModStr = vm["sigmaMod"].as<string>();
      sigmaMod =
        (!sigmaModStr.empty() && sigmaModStr.back() == '%')
        ? atof(sigmaModStr.data()) / 100.0
        : atof(sigmaModStr.data());
      if (sigmaMod <= 0.0)
        throw invalid_argument("sigmaMod=" + to_string(sigmaMod));
    }

    double sigmaAct = sigmaMod;
    if (vm.count("sigmaAct"))
    {
      string sigmaActStr = vm["sigmaAct"].as<string>();
      sigmaAct =
        (!sigmaActStr.empty() && sigmaActStr.back() == '%')
        ? atof(sigmaActStr.data()) / 100.0
        : atof(sigmaActStr.data());
      if (sigmaAct <= 0.0)
        throw invalid_argument("sigmaAct=" + to_string(sigmaAct));
    }
    double r = 0.01;     // On RFC = USD
    if (vm.count("r"))
      r = vm["r"].as<double>();
  
    //-----------------------------------------------------------------------//
    // Option (Payoff) Params:                                               //
    //-----------------------------------------------------------------------//
    // Option Type:
    OptionT optionT = OptionT::ZeroCostLoan;
    if (vm.count("OptionT"))
    {
      string optName = vm["OptionT"].as<string>();
      optionT =
        (optName == "ECall")
        ?   OptionT::ECall :
        (optName == "EPut")
        ?   OptionT::EPut  :
        (optName == "ZeroCostLoan")
        ?   OptionT::ZeroCostLoan :
        (optName == "EnhancedRetX")
        ?   OptionT::EnhancedRetX
        :   throw invalid_argument("UnSupported OptionT=" + optName);
    }

    double K1 =   // 1st Strike
      (optionT == OptionT::ZeroCostLoan)
      ? 5000.0 :
      (optionT == OptionT::EnhancedRetX)
      ? 13000.0
      : 0.0;
    if (vm.count("K1"))
    {
      K1 = vm["K1"].as<double>();
      if (K1 <= 0.0)
        throw invalid_argument("K1=" + to_string(K1));
    }

    double K2 =   // 2nd Strike
      (optionT == OptionT::ZeroCostLoan)
      ? 15000.0 :
      (optionT == OptionT::EnhancedRetX)
      ? 17000.0
      : 0.0;
    if (vm.count("K2"))
    {
      K2 = vm["K2"].as<double>();
      if (K2 <= K1)
        throw invalid_argument("K2=" + to_string(K2));
    }

    // K3 is currently for EnhancedRetX only:
    double K3 = 25000.0;
    if (vm.count("K3"))
    {
      if (optionT == OptionT::EnhancedRetX)
      {
        K3 = vm["K3"].as<double>();
        if (K3 <= K2 || K3 < S0)
          throw invalid_argument("K3=" + to_string(K3));
      }
      else
        throw invalid_argument("K3: UnSupported for this OptionT");
    }

    // "LoanAmt" is for ZeroCostLoan only:
    double loanAmt = K1;
    if (vm.count("LoanAmt"))
    {
      if (optionT == OptionT::ZeroCostLoan)
        loanAmt = vm["LoanAmt"].as<double>();
      else
        throw invalid_argument("LoanAmt: UnSupported for this OptionT");
    }

    // "LiqThresh" is for ZeroCostLoan only. If specified, it overrides "K1";
    // it is the main control param for the PnL:
    double liqThresh = 0.0;
    if (vm.count("LiqThresh"))
    {
      if (optionT == OptionT::ZeroCostLoan)
      {
        liqThresh = vm["LiqThresh"].as<double>();
        K1        = loanAmt + liqThresh;
      }
      else
        throw invalid_argument("LiqThresh: UnSupported for this OptionT");
    }

    // "ProtAmt" is for EnhancedRetX only:
    double protAmt = S0;
    if (vm.count("ProtAmt"))
    {
      if (optionT == OptionT::EnhancedRetX)
        protAmt = vm["ProtAmt"].as<double>();
      else
        throw invalid_argument("ProtAmt: UnSupported for this OptionT");
    }

    // "alpha"   is for EnhancedRetX only:
    double alpha = 0.1;
    if (vm.count("alpha"))
    {
      if (optionT == OptionT::EnhancedRetX)
        alpha = vm["alpha"].as<double>();
      else
        throw invalid_argument("Alpha: UnSupported for this OptionT");
    }
    //-----------------------------------------------------------------------//
    // Timing and Simulation Params:                                         //
    //-----------------------------------------------------------------------//
    string tFromStr = "2020-03-09 00:00:00";
    if (vm.count("t0"))
      tFromStr = vm["t0"].as<string>();

    string tToStr   = "2020-06-09 00:00:00";
    if (vm.count("T"))
      tToStr = vm["T"].as<string>();

    DateTime tFrom = DateTimeOfStr(tFromStr);
    DateTime tTo   = DateTimeOfStr(tToStr);
  
    int    tauMin = 5;        // MC Time Step: 5min
    if (vm.count ("tauMin"))
    {
      tauMin = vm["tauMin"].as<int>();
      if (tauMin <= 0)
        throw invalid_argument("tauMin=" + to_string(tauMin));
    }
    int P = 10'000;           // Number of MC Paths
    if (vm.count("P"))
    {
      P = vm["P"].as<int>();
      if (P <= 0)
        throw invalid_argument("P=" + to_string(P));
    }

    // Files:
    unique_ptr<fstream> statsS;
    if (vm.count("statsFile"))
    {
      string statsFileName = vm["statsFile"].as<string>();
      statsS.reset(new fstream(statsFileName, ios_base::out));
    }

    unique_ptr<fstream> pathS;
    if (vm.count("pathFile"))
    {
      string pathFileName  = vm["pathFile"] .as<string>();
      pathS.reset (new fstream(pathFileName,  ios_base::out));
    }
    //-----------------------------------------------------------------------//
    // Diffusion:                                                            //
    //-----------------------------------------------------------------------//
    Vector<double> coeffs;
    if (diffName == "GBM")
    {
      coeffs.resize(2);
      coeffs[0]     = mu;
      coeffs[1]     = sigmaAct; // Use the "actual" vol for the path simulation
    }
    else
      throw invalid_argument("Unsupported Diffusion: "+ diffName);
  
    // Construct the DiffParams:
    Diffusion_Params<double> diffParams
    {
      diffName,
      tFrom,
      S0,
      false, // No Regime-Switching currently: Only TradingHours
      coeffs
    };
  
    // Construct the Diffusion:
    Diffusion1D<double>* diff = MkDiffusion1D(diffParams);
    assert(diff != nullptr);
  
    //-----------------------------------------------------------------------//
    // TimeLine (IsFwd = true, no TradingCalendar / SpecialTimes currently): //
    //-----------------------------------------------------------------------//
    Time dts[3]{ TimeGener::seconds(tauMin * 60), ZeroTime(), ZeroTime() };
    std::vector<TimeNode<double>> timeLine;
    MkTimeLine <true,    double>(tFrom, dts, tTo, nullptr, nullptr, &timeLine);
  
    //-----------------------------------------------------------------------//
    // Perform Monte-Carlo Simulations:                                      //
    //-----------------------------------------------------------------------//
    int    L = int(timeLine.size());
    assert(L > 0 && P > 0 && timeLine[0].m_ty == 0.0);
  
    // XXX: CUDA is not used at the moment:
    RNG   <double> rng(shared_ptr<CUDAEnv>(nullptr));
    Matrix<double> paths;  // Of size (L, P), where L is TimeLine length
  
    // NB: UseAntithetic = true:
    diff->MCPaths(timeLine, P, rng, &paths, nullptr, true);
  
    assert(int(paths.size1()) == L && int(paths.size2()) == P);
  
    //-----------------------------------------------------------------------//
    // Evaluate the Paths:                                                   //
    //-----------------------------------------------------------------------//
    // The Option Spec -- XXX: now using the ModelVol (though that should be a
    // separate obj):
    //
    unique_ptr<Option const> option
    (
      (optionT == OptionT::ECall)
      ? (Option*)(new ECall(K1, sigmaMod, r))
      :
      (optionT == OptionT::EPut)
      ? (Option*)(new EPut (K1, sigmaMod, r))
      :
      (optionT == OptionT::ZeroCostLoan)
      ? (Option*)(new ZeroCostLoan(K1, K2, sigmaMod, r))
      :
      (optionT == OptionT::EnhancedRetX)
      ? (Option*)(new EnhancedRetX
                      (alpha, S0, K1, K2, K3, protAmt, sigmaMod, r))
      :
      throw invalid_argument("UnSupported OptionT")
    );

    // Initial Cash for this Option: Must be at least the OptionPx:
    double T         = timeLine[L-1].m_ty;
    double optPx     = option->OptionPx(S0, T).first;
    double initCash  =
      (optionT == OptionT::ZeroCostLoan)
      ? (S0 - loanAmt)
      :
      (optionT == OptionT::EnhancedRetX)
      ? S0
      : optPx;    // In all other cases

    double targetPnL = initCash - optPx;
    if (targetPnL < 0.0)
    {
      // Insufficient "initCash" to build the hedge:
      cerr << "ERROR: InitCash="  << initCash << " but OptionPx=" << optPx
           << ": Cannot proceed"  << endl;
      return 1;
    }

    // Output the Header:
    if (statsS)
    {
      *statsS   << "# K1=" << K1  << ", K2="  << K2 << ", S0=" << S0
                << ", TargetPnL=" << targetPnL      << endl
                << "# sigmaMod="  << sigmaMod << ", sigmaAct=" << sigmaAct
                << endl;
      if (optionT == OptionT::ZeroCostLoan)
        *statsS << "# LoanAmt="   << loanAmt  << ", LiqThresh=" << liqThresh
                << endl;

      *statsS   << "# OptionPx="  << optPx    << ", InitCash="  << initCash
                << endl;
    }
    // If OK: Path Evaluators:
    vector<PathEvaluator> pevs(P);
  
    // Each path is a column (represented as a contiguous 1D array):
    double const* S = &(paths(0,0));

    // A sample path we fill in (for outputting): [(S_t, Delta_t, M_t)]:
    SamplePath samplePath(L);

#   pragma omp parallel for
    for (int p = 0; p < P; ++p)
    {
      // The last path is a sample one (otherwise NULL):
      SamplePath* sPath  = (p == P-1) ? &samplePath : nullptr;
  
      // XXX: Currently, for just 1 sample path, we record Deltas and Cash along
      // it:
      PathEvaluator& pev = pevs[p];
      pev.OnPath(&(timeLine[0]), S + p * L, L, *option, initCash, sPath);

      // Output the results of the run:
#     pragma omp critical
      OutputRess(statsS.get(), pathS.get(), pev, *option, sPath);
    }
    // All done:
    return 0;
  }
  //-------------------------------------------------------------------------//
  // Error Handling:                                                         //
  //-------------------------------------------------------------------------//
  catch (exception const& exn)
  {
    cerr << "EXCEPTION: " << exn.what() << endl;
    return 1;
  }
}
