// vim:ts=2:et
//===========================================================================//
//                            "SobolSeqTest.cpp":                            //
//===========================================================================//
#include "Common/Maths.hpp"
#include "MonteCarlo/SobolSeq.h"
#include <stdio.h>
#include <time.h>

/* test integrand from Joe and Kuo paper ... integrates to 1 */
namespace
{
  using namespace Arbalete;

  double testfunc(int n, double const* x)
  {
    double f = 1.0;
    for (int j = 1; j <= n; ++j)
    {
      double cj = Pow(double(j), 0.3333333333333333333);
      f *= (Abs(4.0 * x[j-1] - 2.0) + cj) / (1.0 + cj);
    }
    return f;
  }
}

int main(int argc, char* argv[])
{
  if (argc < 3)
  { 
    fprintf(stderr, "Usage: %s <sdim> <ngen>\n", argv[0]);
    return 1;
  }
  int sdim = atoi(argv[1]);
  int n    = atoi(argv[2]);

  SobolSeq s(sdim);

  double x[s.GetMaxDim()];
  s.Skip(n, x);

  double testint_sobol = 0.0;

  for (int j = 1; j <= n; ++j)
  {
    s.Next01(x);
    testint_sobol += testfunc(sdim, x);
  }

  printf("Test integral = %g using Sobol.\n",
         testint_sobol / double(n));
  printf("        error = %g using Sobol.\n",
         testint_sobol / double(n) - 1.0);
  return 0;
}
