// vim:ts=2:et
//===========================================================================//
//                            "CalibrTest2D.cpp":                            //
//                     Calibration of 2D Diffusion Coeffs                    //
//===========================================================================//
#include "Common/Maths.hpp"
#include "Calibration/RealCalibrator.h"
#include <cstdlib>
#include <cstring>
#include <stdexcept>
#include <iostream>

using namespace std;
using namespace Arbalete;

namespace
{
  //-------------------------------------------------------------------------//
  // "RunTest":                                                              //
  //-------------------------------------------------------------------------//
  template<typename F>
  void RunTest(string const& xml_config_file)
  {
    try
    {
      RealCalibrator<F> calibr(xml_config_file);

      F optLLH = - Inf<F>();
      Vector<F>   optCoeffs;  // Resized automatically by "Calibrate"

      calibr.Calibrate(&optCoeffs, &optLLH);
      optLLH = - optLLH;                // The max achieved

      cout << "Optimal Coeffs:\n\t" << optCoeffs << endl;
      cout << "Maximum LLH:\n\t"    << optLLH    << endl;
    }
    catch (exception const& e)
    {
      cerr << "ERROR: " << e.what() << endl;
      exit(1);
    }
  }

  //-------------------------------------------------------------------------//
  // "ParamsError":                                                          //
  //-------------------------------------------------------------------------//
  void ParamsError()
  {
    cerr << "PARAMETERS: {--float|--double} ConfigFile.xml" << endl;
    exit(1);
  }
}

//===========================================================================//
// "main":                                                                   //
//===========================================================================//
int main(int argc, char* argv[])
{
  if (argc != 3)
    ParamsError();

  if (strcmp(argv[1], "--float")  == 0)
    RunTest<float> (argv[2]);
  else
  if (strcmp(argv[1], "--double") == 0)
    RunTest<double>(argv[2]);
  else
    ParamsError();

  return 0;
}
