// vim:ts=2:et
//===========================================================================//
//                                "DateTime.h":                              //
//                       Date / Time Types and Utils                         //
//===========================================================================//
#pragma once

#ifdef   __CUDACC__
#error   "DateTime.h cannot be used in CUDA compilations"
#endif

#include "Common/Maths.hpp"
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <string>
#include <chrono>

namespace Arbalete
{
  //=========================================================================//
  // Date/Time:                                                              //
  //=========================================================================//
  using DateTime = boost::posix_time::ptime;
  using Date     = boost::gregorian::date;
  using DateYMD  = boost::gregorian::greg_year_month_day;
  using Days     = boost::gregorian::date_duration;
  using Time     = boost::posix_time::time_duration; 

  // Memorable namespace for generating "Time" values:
  // can use hours(), minutes(), seconds(), milliseconds(), microseconds()
  // and possibly nanoseconds() from "TimeGener":
  //
  namespace DateGener = boost::gregorian;
  namespace TimeGener = boost::posix_time;

  // Zero Time:
  inline Time      ZeroTime() { return TimeGener::seconds(0);      }

  // Current, Init and Last DateTime:
  DateTime         CurrUTC();

  inline DateTime InitDateTime()
    { return DateTime(Date(2000, 1, 1)); }

  inline DateTime LastDateTime()
    { return DateTime(Date(2100, 1, 1)); }

  // Invalid DateTime:
  boost::date_time::special_values const InvalidDateTime =
    boost::date_time::not_a_date_time;

  namespace Chrono = std::chrono;

  // XXX: In Boost, "Time" only provides implementation-specific "fractional_
  // seconds", so need to convert them into absolute time units; NB: the va-
  // lues returned are properly rounded:
  //
  inline int microseconds(Time t, bool round = true)
  {
    return
      round
      ? int(Round(double(t.fractional_seconds()) * 1000000.0 /
                  double(t.ticks_per_second())))
      : int((t.fractional_seconds() * 1000000L) / t.ticks_per_second());
  }

  inline int milliseconds(Time t, bool round = true)
  {
    return
      round
      ? int(Round(double(t.fractional_seconds()) * 1000.0    /
                  double(t.ticks_per_second())))
      : int((t.fractional_seconds() * 1000L)    / t.ticks_per_second());
  }

  //=========================================================================//
  // Some Conversions from / to Strings:                                     //
  //=========================================================================//
  // String -> Time:
  // String Format: X{msec|sec|s|min|m|hr|n|day|d}:
  //
  Time TimeOfStr    (std::string const& str);

  // Time   -> String:
  //
  inline std::string TimeToStr (Time const& time, bool extended = false)
  {
    return
      extended
      ? boost::posix_time::to_iso_string   (time)
      : boost::posix_time::to_simple_string(time);
  }

  // String -> DateTime:
  //
  inline DateTime DateTimeOfStr(std::string const& str)
    { return boost::posix_time::time_from_string  (str); }

  // DateTime -> String:
  //
  inline std::string DateTimeToStr(DateTime dt, bool extended = false)
  {
    return
      extended
      ? boost::posix_time::to_iso_extended_string(dt)
      : boost::posix_time::to_simple_string      (dt);
  }
  
  std::string DateTimeToFStr(DateTime dt, char const* str);

  //=========================================================================//
  // Misc:                                                                   //
  //=========================================================================//
  int DaysInMonth(int year, int month);

  // The following is for simulation purposes only:
  void SetUTCDiddle   (DateTime utc);
  void RemoveUTCDiddle();
}
