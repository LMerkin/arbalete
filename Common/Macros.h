// vim:ts=2:et
//===========================================================================//
//                                "Macros.h":                                //
//           Cross-Platform Linking and CUDA Compilation Support:            //
//===========================================================================//
//---------------------------------------------------------------------------//
// Detecting CUDA compilations:                                              //
//---------------------------------------------------------------------------//
// (*) __CUDACC__ is defined whenever NVCC is used to compile host of device
//     code;
// (*) __CUDA_ARCH__ (eg 200) is defined whenever  NVCC compiles DEVICE code
//     with that compute capability. We currently do NOT use it in condition-
//     al compilation:
//
#pragma once

#ifdef  __CUDACC__
#define DEVICE __device__
#define GLOBAL __global__
#else
#define DEVICE
#define GLOBAL
#endif

#if defined(__CUDACC__) || !defined(NDEBUG) || !NDEBUG
#define CUDACC_OR_DEBUG(x)  x
#else
#define CUDACC_OR_DEBUG(x)
#endif

//---------------------------------------------------------------------------//
// "CHECK_ONLY":                                                             //
// "DEBUG_ONLY":                                                             //
//---------------------------------------------------------------------------//
// Do not generate unused symbols (if only used in "assert" which is only enabl-
// ed in the Debug mode):
// "CHECK_ONLY": Symbol is used unless UNCHECKED_MODE (and thus NDEBUG) are set:
//
#ifdef CHECK_ONLY
#undef CHECK_ONLY
#endif
#if (defined(NDEBUG) && defined(UNCHECKED_MODE))
#  define CHECK_ONLY(x)           // UnChecked Mode only
#else
#  define CHECK_ONLY(x) x         // Release, RelWithDebInfo, Debug
#endif

// "DEBUG_ONLY": Symbol is used unless NDEBUG is set, which is a STRONGER cond
// compared to "CHECK_ONLY":
#ifdef DEBUG_ONLY
#undef DEBUG_ONLY
#endif
#ifdef NDEBUG
#  define DEBUG_ONLY(x)           // UnChecked or Release Mode
#else
#  define DEBUG_ONLY(x) x         // RelWithDebInfo or Debug
#endif

//---------------------------------------------------------------------------//
// "UNUSED_PARAM" (unconditionally):                                         //
//---------------------------------------------------------------------------//
#ifdef  UNUSED_PARAM
#undef  UNUSED_PARAM
#endif
#define UNUSED_PARAM(x)

