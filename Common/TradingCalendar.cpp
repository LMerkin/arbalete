// vim:ts=2:et
//===========================================================================//
//                           "TradingCalendar.cpp":                          //
//               Trading Calendars for Diffusions and Grids                  //
//===========================================================================//
#include "Common/TradingCalendar.h"
#include "Common/Maths.hpp"
#include <algorithm>
#include <cassert>
#include <stdexcept>

namespace Arbalete
{
  using namespace std;
  using boost::local_time::local_date_time;
  using boost::local_time::posix_time_zone;
  using boost::date_time::Monday;
  using boost::date_time::Friday;
  using boost::date_time::Saturday;
  using boost::date_time::Sunday;

  //=========================================================================//
  // Trading Calendars:                                                      //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // Non-Default Ctor:                                                       //
  //-------------------------------------------------------------------------//
  TradingCalendar::TradingCalendar
  (
    Time                open_time_local,
    Time                close_time_local,
    string const&       tz,
    vector<Date> const& holidays
  )
  : m_openTimeLocal (open_time_local),
    m_closeTimeLocal(close_time_local),
    m_TZ            (new posix_time_zone(tz)),
    m_hols          (holidays)
  {}


  //-------------------------------------------------------------------------//
  // "IsHoliday":                                                            //
  //-------------------------------------------------------------------------//
  bool TradingCalendar::IsHoliday(Date d) const
  {
    return find(m_hols.begin(), m_hols.end(), d) != m_hols.end();
  }

  //-------------------------------------------------------------------------//
  // "OpenTimeGMT":                                                          //
  //-------------------------------------------------------------------------//
  Time TradingCalendar::OpenTimeGMT(Date d) const
  {
    local_date_time ldt
      (d, m_openTimeLocal, m_TZ, local_date_time::EXCEPTION_ON_ERROR);
    return ldt.utc_time().time_of_day();
  }

  //-------------------------------------------------------------------------//
  // "CloseTimeGMT":                                                         //
  //-------------------------------------------------------------------------//
  Time TradingCalendar::CloseTimeGMT(Date d) const
  {
    local_date_time ldt
      (d, m_closeTimeLocal, m_TZ, local_date_time::EXCEPTION_ON_ERROR);
    return ldt.utc_time().time_of_day();
  }

  //-------------------------------------------------------------------------//
  // "ToGMT": Convert any "DateTime" to GMT:                                 //
  //-------------------------------------------------------------------------//
  // "t" is Local Time, the result is GMT:
  //
  DateTime TradingCalendar::ToGMT(DateTime t) const
  {
    local_date_time ldt
      (t.date(), t.time_of_day(), m_TZ, local_date_time::EXCEPTION_ON_ERROR);
    return ldt.utc_time();
  }

  //=========================================================================//
  // Factory of Trading Calendars:                                           //
  //=========================================================================//
  // XXX: With more calendars available, will need to be factored out:
  //
  shared_ptr<TradingCalendar> TradingCalendar::MkTradingCalendar
    (string const& cal_name)
  {
    TradingCalendar* res = nullptr;

    //-----------------------------------------------------------------------//
    // NULL is currently interpreted as "Generic24/7":                       //
    //-----------------------------------------------------------------------//
    if (cal_name.empty() || cal_name == "Generic24/7")
      ; // "res" remains NULL
    else
    //-----------------------------------------------------------------------//
    // "SX5E":                                                               //
    //-----------------------------------------------------------------------//
    // XXX: This is an example only:
    if (cal_name == "SX5E")
    {
      res = new TradingCalendar
      (
        Time( 9,  0, 0),    // OpenTime -- 
        Time(17, 30, 0),    // CloseTime
        "CET+1CEST,M3.5.0/02:00:00,M10.5.0/03:00:00", // TZ String
        vector<Date>(11)    // See below
      );
      vector<Date>& hols = res->m_hols;
      // 2011:
      hols[ 0] = Date(2011, 4,22);
      hols[ 1] = Date(2011, 4,25);
      hols[ 2] = Date(2011,12,26);
      // 2012:
      hols[ 3] = Date(2012, 1, 2);
      hols[ 4] = Date(2012, 4, 6);
      hols[ 5] = Date(2012, 4, 9);
      hols[ 6] = Date(2012, 5, 1);
      hols[ 7] = Date(2012,12,24);
      hols[ 8] = Date(2012,12,25);
      hols[ 9] = Date(2012,12,26);
      hols[10] = Date(2012,12,31);
    }
    else
      throw invalid_argument("MkTradingCalendar: Unknown CalName: "+ cal_name);

    return shared_ptr<TradingCalendar>(res);
  }

  //=========================================================================//
  // Computing the Regime-Switching Events:                                  //
  //=========================================================================//
  // XXX: This logic will need to be changed if there are multiple trading
  // sessions per day, or if, because the times are given in GMT, would split
  // the trading session between two GMT days (but in reality, this is not
  // the case even for Tokyo):
  //
  //-------------------------------------------------------------------------//
  // "GetNextFwdEvent":                                                      //
  //-------------------------------------------------------------------------//
  void GetNextFwdEvent
  (
    DateTime t0, TradingCalendar const* cal, DateTime* t1,
    TradingCalendar::CalReg* reg01
  )
  {
    assert(t1 != nullptr && reg01 != nullptr);

    Date date0  = t0.date();
    Time time0  = t0.time_of_day();
    Date::day_of_week_type day0 = date0.day_of_week();

    if (cal != nullptr)
    {
      Time   open0  = cal->OpenTimeGMT (date0);
      Time   close0 = cal->CloseTimeGMT(date0);
      assert(open0 < close0);

      if (cal->IsHoliday(date0) || day0 == Sunday)
      {
        // Next Fwd event is the next day opening, provided that there are no
        // further holidays. So set it provisionally and adjust later:
        //
        Date date1 = date0 + Days(1);
        *t1        = DateTime(date1, cal->OpenTimeGMT(date1));
        *reg01     = TradingCalendar::WeekEndHol;
      }
      else
      if (day0 == Saturday)
      {
        // Similar to above, but add 2 days:
        Date date2 = date0 + Days(2);
        *t1        = DateTime(date2, cal->OpenTimeGMT(date2));
        *reg01     = TradingCalendar::WeekEndHol;
      }
      else
      if (day0 == Friday && time0 >= close0)
      {
        // Similar to above, but add 3 days:
        Date date3 = date0 + Days(3);
        *t1        = DateTime(date3, cal->OpenTimeGMT(date3));
        *reg01     = TradingCalendar::WeekEndHol;
      }
      else
      if ((day0 == Monday || cal->IsHoliday(date0 - Days(1))) && time0 < open0)
      {
        // Still a week-end or holiday:
        *t1    = DateTime(date0, open0);
        *reg01 = TradingCalendar::WeekEndHol;
      }
      else
      //
      // Week-day and not after-close-on-Friday or before-open-Monday:
      //
      if (time0 < open0)
      {
        // Next Fwd event is today's opening. No adjustment will be required
        // as we know that "date0" is NOT a holiday:
        *t1    = DateTime(date0, open0);
        *reg01 = TradingCalendar::OverNight;
      }
      else
      if (time0 < close0)
      {
        // Next Fwd event is today's closing. Again, no adjustment:
        *t1    = DateTime(date0, close0);
        *reg01 = TradingCalendar::TradingHours;
      }
      else
      {
        // Next Fwd event is tomorrow's opening. Adjustment may be required
        // as we don't know if it is a holiday tomorrow:
        Date date1 = date0 + Days(1);
        *t1        = DateTime(date0 + Days(1), cal->OpenTimeGMT(date1));
        *reg01     = TradingCalendar::OverNight;
      }

      // In general, adjustment may be required iff the date has changed. In
      // all those cases, the time of "t1" is OpenTime. Iterate if and while
      // we are still on a non-trading day:
      //
      Date date1 = t1->date();
      if (date1 != date0)
      {
        assert(date1 > date0 && t1->time_of_day() == cal->OpenTimeGMT(date1));
        while (true)
        {
          Date::day_of_week_type day1 = date1.day_of_week();
          if (!cal->IsHoliday(date1) && day1 != Saturday && day1 != Sunday)
            break;
          date1 += Days(1);
          // If we did NOT exit here, we are in the WeekEndHol regime:
          *reg01 = TradingCalendar::WeekEndHol;
        }
      }
    }
    else
    {
      // No Calendar, so no week-ends or holidays, ie assume 24/7 trading. In
      // that case, the only regime is Trading Hours, and "t1" is set at +oo:
      //
      *t1    = LastDateTime();
      *reg01 = TradingCalendar::TradingHours;
    }

    // All done:
#   ifndef NDEBUG
    TradingCalendar::CalReg regE = GetRegE<true>( t0, cal);
    TradingCalendar::CalReg regI = GetRegI<true>(*t1, cal);
    assert(t0 < *t1 && *reg01 == regE && *reg01 == regI);
#   endif
  }

  //-------------------------------------------------------------------------//
  // "GetNextBwdEvent":                                                      //
  //-------------------------------------------------------------------------//
  void GetNextBwdEvent
  (
    DateTime t0, TradingCalendar const* cal, DateTime* t1,
    TradingCalendar::CalReg* reg01
  )
  {
    assert(t1 != nullptr && reg01 != nullptr);

    Date date0 = t0.date();
    Time time0 = t0.time_of_day();
    Date::day_of_week_type day0 = date0.day_of_week();

    if (cal != nullptr)
    {
      Time   open0  = cal->OpenTimeGMT (date0);
      Time   close0 = cal->CloseTimeGMT(date0);
      assert(open0 < close0);

      if (cal->IsHoliday(date0) || day0 == Saturday)
      {
        // Next Bwd event is the prev day closing, provided that there are no
        // further holidays. So set it provisionally and adjust later:
        //
        Date date1 = date0 - Days(1);
        *t1    = DateTime(date1, cal->CloseTimeGMT(date1));
        *reg01 = TradingCalendar::WeekEndHol;
      }
      else
      if (day0 == Sunday)
      {
        // Similar to above, but subtract 2 days:
        Date date2 = date0 - Days(2);
        *t1    = DateTime(date2, cal->CloseTimeGMT(date2));
        *reg01 = TradingCalendar::WeekEndHol;
      }
      else
      if (day0 == Monday && time0 <= open0)
      {
        // Similar to above, but subtract 3 days:
        Date date3 = date0 - Days(3);
        *t1    = DateTime(date3, cal->CloseTimeGMT(date3));
        *reg01 = TradingCalendar::WeekEndHol;
      }
      else
      if ((day0 == Friday || cal->IsHoliday(date0 + Days(1))) &&
          time0 > close0)
      {
        // Already a week-end!
        *t1    = DateTime(date0, close0);
        *reg01 = TradingCalendar::WeekEndHol;
      }
      else
      //
      // Week-day and not after-close-on-Friday or before-open-Monday:
      //
      if (time0 <= open0)
      {
        // Next Bwd event is prev day's closing. May require adjustment if
        // that day was a holiday:
        Date date1 = date0 - Days(1);
        *t1    = DateTime(date1, cal->CloseTimeGMT(date1));
        *reg01 = TradingCalendar::OverNight;
      }
      else
      if (time0 <= close0)
      {
        // Next Bwd event is today's opening. No adjustment here:
        *t1    = DateTime(date0, open0);
        *reg01 = TradingCalendar::TradingHours;
      }
      else
      {
        // Next Bwd event is today's closing. Again, no adjustment:
        *t1    = DateTime(date0, close0);
        *reg01 = TradingCalendar::OverNight;
      }

      // In general, adjustment may be required iff the date has changed.  In
      // all those cases, the time of "t1" is CloseTime. Iterate if and while
      // we are still on a non-trading day:
      //
      Date date1 = t1->date();
      if (date1 != date0)
      {
        assert(date1 < date0 && t1->time_of_day() == cal->CloseTimeGMT(date1));
        while (true)
        {
          Date::day_of_week_type day1 = date1.day_of_week();
          if (!cal->IsHoliday(date1) && day1 != Saturday && day1 != Sunday)
            break;
          date1 -= Days(1);
          // If we did NOT exit here, we are in the WeekEndHol regime:
          *reg01 = TradingCalendar::WeekEndHol;
        }
      }
    }
    else
    {
      // No Calendar, so no week-ends or holidays: assume 24/7 trading; "t1" is
      // set at the beginning of time:
      *t1    = InitDateTime();
      *reg01 = TradingCalendar::TradingHours;
    }
    // All done:
#   ifndef NDEBUG
    TradingCalendar::CalReg regE = GetRegE<false>( t0, cal);
    TradingCalendar::CalReg regI = GetRegI<false>(*t1, cal);
    assert(*t1 < t0 && *reg01 == regE && *reg01 == regI);
#   endif
  }

  //=========================================================================//
  // Computing the Regime of a Given Time Instant:                           //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // "GetRegI":                                                              //
  //-------------------------------------------------------------------------//
  // "Left-continuous" (in the direction of time flow):
  // reg(t0) = reg(t0 - eps) if "t0" is a regime-switching point:
  //
  template<bool IsFwd>
  TradingCalendar::CalReg GetRegI(DateTime t0, TradingCalendar const* cal)
  {
    Date date0  = t0.date();
    Time time0  = t0.time_of_day();
    Date::day_of_week_type day0 = date0.day_of_week();

    if (cal != nullptr)
    {
      // Calendar and trading hours are available:
      Time open0  = cal->OpenTimeGMT (date0);
      Time close0 = cal->CloseTimeGMT(date0);

      // May need to know if the prev or next day is a holiday:
      bool isPrevHol = cal->IsHoliday(date0 - Days(1));
      bool isNextHol = cal->IsHoliday(date0 + Days(1));

      if (cal->IsHoliday(date0) || day0 == Saturday || day0 == Sunday)
        // On those days, time is ignored, so we cannot be at a CalReg changing
        // point anyway:
        return TradingCalendar::WeekEndHol;
      else
        // Now there is a subtle difference depending on the Fwd/Bwd direction:
      if (IsFwd)
        if (open0 < time0 && time0 <= close0)
          return TradingCalendar::TradingHours;
        else
          // Is it OverNight or WeekEndHol?
        if ((time0 <= open0  && (day0 == Monday || isPrevHol)) ||
            (time0 >  close0 && (day0 == Friday || isNextHol)))
          return TradingCalendar::WeekEndHol;
        else
          return TradingCalendar::OverNight;
      else
        // Bwd: Note the differences in strict/non-strict inequalities:
        if (open0 <= time0 && time0 < close0)
          return TradingCalendar::TradingHours;
        else
        if ((time0 <  open0  && (day0 == Monday || isPrevHol)) ||
            (time0 >= close0 && (day0 == Friday || isNextHol)))
          return TradingCalendar::WeekEndHol;
        else
          return TradingCalendar::OverNight;
    }
    else
      // No calendar, so assume 24/7 TradingHours:
      return TradingCalendar::TradingHours;
  }

  //-------------------------------------------------------------------------//
  // "GetRegE":                                                              //
  //-------------------------------------------------------------------------//
  // "Right-continuous" (in the direction of time flow):
  // reg(t0) = reg(t0 + eps) if "t0" is a regime-switching point:
  //
  template<bool IsFwd>
  TradingCalendar::CalReg GetRegE(DateTime t0, TradingCalendar const* cal)
  {
    Date date0  = t0.date();
    Time time0  = t0.time_of_day();
    Date::day_of_week_type day0 = date0.day_of_week();

    if (cal != nullptr)
    {
      // Calendar and trading hours are available:
      Time open0  = cal->OpenTimeGMT (date0);
      Time close0 = cal->CloseTimeGMT(date0);

      // May need to know if the prev or next day is a holiday:
      bool isPrevHol = cal->IsHoliday(date0 - Days(1));
      bool isNextHol = cal->IsHoliday(date0 + Days(1));

      if (cal->IsHoliday(date0) || day0 == Saturday || day0 == Sunday)
        // On those days, time is ignored, so we cannot be at a CalReg changing
        // point anyway:
        return TradingCalendar::WeekEndHol;
      else
        // Now there is a subtle difference depending on the Fwd/Bwd direction:
      if (IsFwd)
        if (open0 <= time0 && time0 < close0)
          return TradingCalendar::TradingHours;
        else
        if ((time0 <  open0  && (day0 == Monday || isPrevHol)) ||
            (time0 >= close0 && (day0 == Friday || isNextHol)))
          return TradingCalendar::WeekEndHol;
        else
          return TradingCalendar::OverNight;
      else
      {
        // Bwd: Note the differences in strict/non-strict inequalities:
        if (open0 < time0 && time0 <= close0)
          return TradingCalendar::TradingHours;
        else
        if ((time0 <= open0  && (day0 == Monday || isPrevHol)) ||
            (time0 >  close0 && (day0 == Friday || isNextHol)))
          return TradingCalendar::WeekEndHol;
        else
          return TradingCalendar::OverNight;
      }
    }
    else
      // No calendar, so assume 24/7 Trading hours:
      return TradingCalendar::TradingHours;
  }

  //-------------------------------------------------------------------------//
  // "GetRegE" and "GetRegI" Instances:                                      //
  //-------------------------------------------------------------------------//
  template TradingCalendar::CalReg GetRegI<true>
    (DateTime t0, TradingCalendar const* cal);

  template TradingCalendar::CalReg GetRegI<false>
    (DateTime t0, TradingCalendar const* cal);

  template TradingCalendar::CalReg GetRegE<true>
    (DateTime t0, TradingCalendar const* cal);

  template TradingCalendar::CalReg GetRegE<false>
    (DateTime t0, TradingCalendar const* cal);

  //=========================================================================//
  // "TimeMarshalUtils":                                                     //
  //=========================================================================//
  // Time Steps with Regime-Switching Events:
  //-------------------------------------------------------------------------//
  // "StepFwd":                                                              //
  //-------------------------------------------------------------------------//
  void TimeMarshalUtils::StepFwd
  (
    DateTime                  t0,
    Time                      dts[3],
    DateTime                  t_end,
    TradingCalendar  const*   cal,
    vector<DateTime> const*   special_times,
    int*                      sti,
    DateTime*                 tn,
    TradingCalendar::CalReg*  reg
  )
  {
    assert(t0 < t_end && tn != nullptr && reg != nullptr && sti != nullptr);

    // Determine the time of the next (Fwd) regime-changing event, and get
    // the closest of that and "t_end":
    DateTime t1;
    GetNextFwdEvent(t0, cal, &t1, reg);

    t1 = min<DateTime>(t1, t_end);
    assert(t0 < t1);

    // Also, if there are "special_times", move no further than the next such
    // instant: NB: *sti may be (-1) if there are no special times at all, or
    // it may be beyound the "special_times" range, so check it:
    //
    int n = (special_times != nullptr) ? int(special_times->size()) : 0;
    DateTime  nextSp;   // Initially invalid!

    if (0 <= *sti && *sti < n)
    {
      assert(special_times != nullptr);
      nextSp = (*special_times)[*sti];
      assert(nextSp >= t0);
      // BEWARE: Do not constrain "t1" by "nextSp" if nextSp = t0!
      // In that case, advance "sti" if possible:
      if (nextSp == t0)
      {
        (*sti)++;
        if (*sti < n)
        {
          nextSp = (*special_times)[*sti];
          assert(t0 < nextSp);
        }
        // Otherwise, "nextSp" remains at "t0" -- cannot advance it further...
      }
      if (t0 < nextSp && nextSp < t1)
        t1 = nextSp;
    }
    assert(t0 < t1 && t1 <= t_end);

    // Now make the Fwd step using the time step value for the computed "reg":
    int ireg = int(*reg);
    assert(0 <= ireg && ireg < 3);

    Time  dt = dts[ireg];
    *tn = t0 + dt;

    // If "tn" is beyond "t1", or sufficiently close to "t1", or "dt" is 0:
    // use "t1":
    if (*tn >= t1 || dt == ZeroTime())
      *tn = t1;
    else
    {
      assert(*tn < t1);
      Time ddt  = t1 - *tn;
      // XXX: Do not use "total_microseconds()" or so below, as it may result
      // in overflow on Windows with MSVC (where "long" is still 32-bit):
      if (ddt.total_seconds() < dt.total_seconds() / 10L)
        *tn = t1;
    }
    assert(t0 < *tn && *tn <= t1 && t1 <= t_end);

    // Move the next "special time" (in the Fwd dir) beyond "tn" -- this should
    // take at most 1 step because we already have     EITHER
    // (*) sti < n, special_times[sti] = t1 > t0, so tn <= t1, so do 0 steps if
    //     tn  < t1 or 1 step if tn = t1;              OR
    // (*) sti = n: cannot step further, so make 0 steps:
    //
    if (special_times != nullptr && *sti >= 0)
    {
      int adv = 0;
      while (*sti < n && (*special_times)[*sti] <= *tn)
      {
        (*sti)++;
        adv++;
      }
      // NB: "sti" can eventually become equal to "n"!
      assert(adv <= 1);
    }
    // Final check on "tn":
    assert(t0 < *tn && *tn <= t_end);
  }

  //-------------------------------------------------------------------------//
  // "StepBwd":                                                              //
  //-------------------------------------------------------------------------//
  void TimeMarshalUtils::StepBwd
  (
    DateTime                  t0,
    Time                      dts[3],
    DateTime                  t_end,
    TradingCalendar  const*   cal,
    vector<DateTime> const*   special_times,
    int*                      sti,
    DateTime*                 tn,
    TradingCalendar::CalReg*  reg
  )
  {
    assert(t0  > t_end && tn  != nullptr && reg != nullptr && sti != nullptr);

    // Determine the time of the next (Bwd) regime-changing event, and get
    // the closest of that and "t_end":
    DateTime t1;
    GetNextBwdEvent(t0, cal, &t1, reg);

    t1 = max<DateTime>(t1,  t_end);
    assert(t0 > t1 && t1 >= t_end);

    // Also, if there are "special_times", move no further than the next such
    // instant: NB: *sti may be (-1) if there are no special times at all, or
    // it may be beyound the "special_times" range, so check it:
    //
    int n = (special_times != nullptr) ? int(special_times->size()) : 0;
    DateTime  nextSp;   // Initially invalid!
    if (0 <= *sti && *sti < n)
    {
      assert(special_times != nullptr);
      nextSp = (*special_times)[*sti];
      assert(nextSp <= t0);
      // BEWARE: Do not constrain "t1" by "nextSp" if nextSp = t0!
      // In that case, advance "sti" if possible:
      if (nextSp == t0)
      {
        (*sti)--;
        if (*sti >= 0)
        {
          nextSp = (*special_times)[*sti];
          assert(nextSp < t0);
        }
        // Otherwise, "nextSp" remains at "t0" -- cannot advance it further...
      }
      if (t1 < nextSp && nextSp < t0)
        t1 = nextSp;
    }
    assert(t0 > t1 && t1 >= t_end);

    // Now make the Bwd step using the time step value for the computed "reg":
    int ireg = int(*reg);
    assert(0 <= ireg && ireg < 3);

    Time dt  = dts[ireg];
    *tn = t0 - dt;

    // If "tn" is beyond "t1", or sufficiently close to "t1", or "dt" is 0:
    // use "t1":
    if (*tn <= t1 || dt == ZeroTime())
      *tn = t1;
    else
    {
      assert(*tn > t1);
      Time ddt  = *tn - t1;
      // XXX: Again, do not use "total_microseconds()" or so here to avoid
      // overlow on Windows with MSVC:
      if (ddt.total_seconds() < dt.total_seconds() / 10L)
        *tn = t1;
    }
    assert(t0 > *tn && *tn >= t1 && t1 >= t_end);

    // Move the next "special time" (in the Bwd dir) beyond "tn" -- this should
    // take at most 1 step because we already have     EITHER
    // (*) sti >= 0, special_times[sti] = t1 < t0, so tn >= t1, so do 0 steps if
    //     tn  > t1 or 1 step if tn = t1;              OR
    // (*) sti <  0: cannot step further, so make 0 steps:
    //
    if (special_times != nullptr && *sti >= 0)
    {
      int adv = 0;
      while (*sti >= 0 && (*special_times)[*sti] >= *tn)
      {
        (*sti)--;
        adv++;
      }
      // NB: "sti" can eventually become (-1)!
      assert(adv <= 1);
    }
    // Final check on "tn":
    assert(t0 > *tn && *tn >= t_end);
  }

  //-------------------------------------------------------------------------//
  // "IsSpecialTime":                                                        //
  //-------------------------------------------------------------------------//
  // If "t_curr" is a member of "special_times",  returns its index there;
  // otherwise returns -1; uses "sti" for guidance when lookip up "special_
  // times" to avoid full search:
  //
  template<bool IsFwd>
  int TimeMarshalUtils::IsSpecialTime
  (
    DateTime                t_curr,
    vector<DateTime> const* special_times,
    int                     sti
  )
  {
    if (special_times == nullptr || special_times->empty() || sti < 0)
      return -1;

    int n = int(special_times->size());

    // INVARIANTS:
    // IsFwd:   special_times[sti] >= t_curr, so "t_curr" can only corresp to
    //          "sti" or (sti-1);
    // !IsFwd:  special_times[sti] <= t_curr, so "t_curr" can only corresp to
    //          "sti" or (sti+1):
    //
    if (sti < n && (*special_times)[sti] == t_curr)
      return sti;
    else
    if ( IsFwd && 1 <= sti   && sti <= n && (*special_times)[sti-1] == t_curr)
      return sti-1;
    else
    if (!IsFwd && sti <= n-2 && (*special_times)[sti+1] == t_curr)
      return sti+1;

    // Otherwise, "t_curr" is not there (do full check in the debug mode):
    assert(find(special_times->begin(), special_times->end(), t_curr) ==
           special_times->end());
    return -1;
  }

  //-------------------------------------------------------------------------//
  // "IsSpecialTime" Instances:                                              //
  //-------------------------------------------------------------------------//
  template int TimeMarshalUtils::IsSpecialTime<true>
  (
    DateTime                t_curr,
    vector<DateTime> const* special_times,
    int                     sti
  );

  template int TimeMarshalUtils::IsSpecialTime<false>
  (
    DateTime                t_curr,
    vector<DateTime> const* special_times,
    int                     sti
  );

  //-------------------------------------------------------------------------//
  // "ChkSpecialTimes":                                                      //
  //-------------------------------------------------------------------------//
  template<bool IsFwd>
  int TimeMarshalUtils::ChkSpecialTimes
  (
    vector<DateTime> const* special_times,
    DateTime                tFrom,
    DateTime                tTo
  )
  {
    if (special_times == nullptr || special_times->empty())
      // No "special times" -- nothing to do:
      return -1;

    // Otherwise: GENERIC CASE:
    // Irrespective to the Fwd or Bwd mode, "special_times" are must be sorted
    // in the strictly ascending order:
    //
    int n   = int(special_times->size());
    int sti = -1;

    if (IsFwd)
      // Check the ordering and determine the first index "i" s.t.
      // special_times[i] >= tFrom:
      //
      for (int i = 0; i < n; ++i)
      {
        DateTime st = (*special_times)[i];

        if (i >= 1 && st <= (*special_times)[i-1])
          throw invalid_argument("SpecialTimes must be strictly increasing");

        if (sti == -1 && st >= tFrom)
          sti = i;
      }
    else
      // Bwd: check the ordering and determine the first (from back) "i" s.t.
      // special_times[i] <= tTo:
      //
      for (int i = n-1; i >= 0; --i)
      {
        DateTime st = (*special_times)[i];

        if (i <= n-2 && st >= (*special_times)[i+1])
          throw invalid_argument("SpecialTimes must be strictly increasing");

        if (sti == -1 && st <= tTo)
          sti = i;
      }
    return sti;
  }

  //-------------------------------------------------------------------------//
  // "ChkSpecialTimes" Instances:                                            //
  //-------------------------------------------------------------------------//
  template int TimeMarshalUtils::ChkSpecialTimes<true>
  (
    vector<DateTime> const* special_times,
    DateTime                tFrom,
    DateTime                tTo
  );

  template int TimeMarshalUtils::ChkSpecialTimes<false>
  (
    vector<DateTime> const* special_times,
    DateTime                tFrom,
    DateTime                tTo
  );

  //=========================================================================//
  // "MkTimeLine":                                                           //
  //=========================================================================//
  // For both Fokker-Planck (Fwd) and Feynman-Kac (Bwd) eqns. Can also be used
  // in a stand-alone mode (e.g., for non-Finite-Difference-based Grids).
  // Pre-condition: K = m * n;
  // NB: if any dts[*] is 0, the corresp step is not used -- TimeLine advances
  // to the next "special time" or regime switching point. So THERE IS NO NEED
  // to use huge steps to achieve same effect (of going through special or re-
  // gime switching points only -- typical for Conditioning applications):
  //
  template<bool IsFwd, typename F>
  void MkTimeLine
  (
    DateTime                t_from,         // TimeLine beginning (t0)
    Time                    dts[3],         // [dtTH, dtON, dtWEH]
    DateTime                t_to,
    TradingCalendar  const* cal,            // May be NULL, then "24/7" is used
    vector<DateTime> const* special_times,  // May be NULL  or empty
    vector<TimeNode<F>>*    res             // Resulting TimeLine (auto-sized)
  )
  {
    //-----------------------------------------------------------------------//
    // Initialisation and Checks:                                            //
    //-----------------------------------------------------------------------//
    assert(res != nullptr);
    res->clear();

    bool timesOK = (IsFwd && t_from <= t_to) || (!IsFwd && t_to <= t_from);
    if (!timesOK)
      throw invalid_argument
            ("MkTimeLine: Invalid time(s): IsFwd=" + to_string(IsFwd) +
             ", t_from=" + DateTimeToStr(t_from)   + ", t_to="        +
             DateTimeToStr(t_to));

    // Check "special_times" (if any) and determine the first index there   (in
    // the Fwd or Bwd direction) which is within the [t_from .. t_to] range (in
    // the respective order); "special_times",  if given, must be sorted in the
    // STRICTLY ASCENDING order anyway:
    //
    int sti =
      TimeMarshalUtils::ChkSpecialTimes<IsFwd>(special_times, t_from, t_to);
    // NB: may have sti=-1

    // Initialise the curr time and compute the initial regime (it always Expl
    // as it is adjacent to the INITIAL time):
    DateTime t_curr = t_from;

    //-----------------------------------------------------------------------//
    // Main Fwd/Bwd Time Marshalling Loop:                                   //
    //-----------------------------------------------------------------------//
    while (true)
    {
      // "stk" is the index of "t_curr" in the "*special_times" if "t_curr" is
      // indeed a "special" time, and (-1) otherwise:
      //
      int stk =
        TimeMarshalUtils::IsSpecialTime<IsFwd>(t_curr, special_times, sti);

      // Get the next time instant ("t_next") and the corresp Regime applied
      // within [t_curr..t_next] (will never cross regime change boundaries).
      //
      DateTime                t_next;
      TradingCalendar::CalReg reg;

      if (IsFwd)
        TimeMarshalUtils::StepFwd
          (t_curr, dts, t_to, cal, special_times, &sti, &t_next, &reg);
      else
        TimeMarshalUtils::StepBwd
          (t_curr, dts, t_to, cal, special_times, &sti, &t_next, &reg);

      if (IsFwd)
        assert(t_curr < t_next && t_next <= t_to);
      else
        assert(t_curr > t_next && t_next >= t_to);

      // Store the "t_curr" (NB: not "t_next"!!!):
      Time delta =  t_curr - t_from;

      TimeNode<F>    node { delta.total_seconds(), YF<F>(delta), reg, stk };
      res->push_back(node);

      // Next step?
      if (t_next == t_to)
        break;
      t_curr = t_next;
    }

    //-----------------------------------------------------------------------//
    // After the final step:                                                 //
    //-----------------------------------------------------------------------//
    int stk = TimeMarshalUtils::IsSpecialTime<IsFwd>(t_to, special_times, sti);

    // "reg" from "t_to" onwards is not to be used, but we determine it anyway:
    TradingCalendar::CalReg reg = GetRegE<IsFwd>(t_to, cal);
    Time delta = t_to - t_from;

    TimeNode<F> node { delta.total_seconds(), YF<F>(delta), reg, stk };
    res->push_back(node);
  }

  //-------------------------------------------------------------------------//
  // "MkTimeLine" Pre-Defined Instances:                                     //
  //-------------------------------------------------------------------------//
  template void MkTimeLine<true,  double>
  (
    DateTime                  t_from,
    Time                      dts[3],
    DateTime                  t_to,
    TradingCalendar  const*   cal,
    vector<DateTime> const*   special_times,
    vector<TimeNode<double>>* res
  );

  template void MkTimeLine<false, double>
  (
    DateTime                  t_from,
    Time                      dts[3],
    DateTime                  t_to,
    TradingCalendar  const*   cal,
    vector<DateTime> const*   special_times,
    vector<TimeNode<double>>* res
  );

  template void MkTimeLine<true,  float>
  (
    DateTime                  t_from,
    Time                      dts[3],
    DateTime                  t_to,
    TradingCalendar  const*   cal,
    vector<DateTime> const*   special_times,
    vector<TimeNode<float>>*  res
  );

  template void MkTimeLine<false, float>
  (
    DateTime                  t_from,
    Time                      dts[3],
    DateTime                  t_to,
    TradingCalendar  const*   cal,
    vector<DateTime> const*   special_times,
    vector<TimeNode<float>>*  res
  );
}
