// vim:ts=2:et
//===========================================================================//
//                                "CUDAEnv.h":                               //
//                  CUDA Configs, Environment and Helpers:                   //
//===========================================================================//
#pragma once

#include "Common/Macros.h"
#ifndef  __CUDACC__
#include "Common/Threading.h"
#endif
#include <cuda_runtime_api.h>

namespace Arbalete
{
  //=========================================================================//
  // "CUDAEnv":                                                              //
  //=========================================================================//
  // NB: this class is NOT thread-safe on the Host side even when its instances
  // are not shared between multiple threads. Only one thread globally may use
  // this class:
  //
  class CUDAEnv
  {
  public:
    //-----------------------------------------------------------------------//
    // "Params":                                                             //
    //-----------------------------------------------------------------------//
    struct Params
    {
      int   m_DevID;
      int   m_BlocksPerSM;
      int   m_BlockSize;
      int   m_WithTimer;

      // NB: Default Ctor is not generated since a Non-Default one is provided:

      // Non-Default Ctor:
      Params(int  DevID, int BpSM, int BSz, bool WithTimer = false)
      : m_DevID      (DevID),
        m_BlocksPerSM(BpSM),
        m_BlockSize  (BSz),
        m_WithTimer  (WithTimer)
      {}
    };

  private:
    //-----------------------------------------------------------------------//
    // Config Data:                                                          //
    //-----------------------------------------------------------------------//
    int           m_DevID;
    int           m_BlocksPerSM;
    int           m_ShMemPerSM;
    int           m_NSMs;
    int           m_NBlocks;
    int           m_BlockSize;
    int           m_NThreads;
    int           m_MaxThreads;
    bool          m_WithTimer;

    // CUDA Stream, Start / Stop Events and Cumulative Timer (ms). NB: Stream
    // and Events are not declared "mutable" as they are actually ptrs:
    cudaStream_t  m_Stream;
    cudaEvent_t   m_StartEv;
    cudaEvent_t   m_StopEv;
    mutable float m_CumTimeMS;

#   ifndef __CUDACC__
    // Global lock to prevent "DevID" interleaving.; NB: "mutex" is not
    // available in NVCC:
    static Mutex  s_DevMtx;
#   endif

    // Default Ctor and Copy Ctor are forbidden:
    CUDAEnv();
    CUDAEnv(const CUDAEnv& old);

  public:
    //-----------------------------------------------------------------------//
    // Ctors and Dtor:                                                       //
    //-----------------------------------------------------------------------//
    CUDAEnv(const Params& cudaParams);

    ~CUDAEnv();

    //-----------------------------------------------------------------------//
    // Accessors:                                                            //
    //-----------------------------------------------------------------------//
    // Access to Config Params:
    //
    int   DevID()             const { return m_DevID;       }
    int   ThreadBlocksPerSM() const { return m_BlocksPerSM; }
    int   ShMemPerSM()        const { return m_ShMemPerSM;  }
    int   NSMs()              const { return m_NSMs;        }
    int   NThreadBlocks()     const { return m_NBlocks;     }
    int   ThreadBlockSize()   const { return m_BlockSize;   }
    int   NThreads()          const { return m_NThreads;    }
    int   MaxThreads()        const { return m_MaxThreads;  }

    // Access to the Mutex, Stream, Events and Timer. NB: "Stream" and "Events"
    // are actually ptrs, this allows us to return them in "const" methods:
    //
    cudaStream_t Stream()     const { return m_Stream;      }
    cudaEvent_t  StartEv()    const { return m_StartEv;     }
    cudaEvent_t  StopEv()     const { return m_StopEv;      }
    float        CumTimeSec() const { return m_CumTimeMS / 1000.0f; }

    // Lock / unlock the above mutex:
    static void Lock();
    static void Unlock();

    //-----------------------------------------------------------------------//
    // Memory Management:                                                    //
    //-----------------------------------------------------------------------//
    // CUDA Memory Allocation / De-Allocation / Copy:
    //
    // "CUDAAlloc": allocates a CUDA array of "K" entries of type "T":
    template<typename T>
    T*   CUDAAlloc(int K) const;

    // "CUDAZeroOut": zeros out the CUDA array at "ptr" of "K" entries of type
    // "T":
    template<typename T>
    void CUDAZeroOut(T* ptr, int K) const;

    // "CUDAFree":  de-allocates a CUDA array over "T" pointed to by "ptr":
    template<typename T>
    void CUDAFree(T*& ptr) const;

    // "ToCUDA":    copying "K" items of type "T" from Host to CUDA memory:
    template<typename T>
    void ToCUDA(T const* hostSrc, T* cudaDst, int K) const;

    // "ToHost":    inverse of "ToCUDA":
    template<typename T>
    void ToHost(T const* cudaSrc, T* hostDst, int K) const;

    //-----------------------------------------------------------------------//
    // Device Setting, Event Synchronisation and Timer:                      //
    //-----------------------------------------------------------------------//
    // Make the configured "DevID" current (normally used before launching a
    // Kernel):
    void SetCUDADevice() const;

    // "Stop" also updates "m_CumTimeMS" automatically, which can then be re-
    // trieved by "CumTimeSec". XXX: these methods are "const" because "m_Cum
    // TimeMS" is "mutable" -- this might not be quite OK:
    //
    void  Start()       const;  // Issue this at the start of a CUDA section
    void  Stop()        const;  // ...        at the end   ...
    void  TimerReset()  const   { m_CumTimeMS = 0.0f; }

    //-----------------------------------------------------------------------//
    // "Sched1D":                                                            //
    //-----------------------------------------------------------------------//
    // Simple load-balancing schedule of CUDA Threads: "K" work items are div-
    // ided between all available Threads:
    //
    struct Sched1D
    {
      int m_from;     // Initial work item index for the curr thread
      int m_to;       // Final   work item index for the curr thread (incl)

      // Default Ctor: Invalid indices:
      Sched1D(): m_from(-1), m_to(0) {}
    };

    // Constructing the schedule in the CUDA space (returns a CUDA mem ptr):
    Sched1D* MkSched1D(int K) const;

    //-----------------------------------------------------------------------//
    // "Sched2D":                                                            //
    //-----------------------------------------------------------------------//
    // More complex load-balancing schedule of CUDA Threads. The space of work
    // items is 2D, of size (E,L). Each 1st index "e" (in [0..E-1]) is done by
    // threads from just one Thread Block, so they can be synchronised; on the
    // other hand, one Thread Block can do more than one "e". Each thread with-
    // in the block works on a particular "e", and does a range on 2nd indices
    // "l" (in [0..L-1]):
    //
    struct Sched2D
    {
      int  m_outer;       // "Outer" index processed by the curr thread
      int  m_innerFrom;   // Initial  "inner" index for the curr thread
      int  m_innerTo;     // Final    "inner" index for the curr thread
      bool m_outerStart;  // This is the 1st Thread working on this "m_outer"

      // Default Ctor: Invalid indices:
      Sched2D():
        m_outer(-1), m_innerFrom(-1), m_innerTo(0), m_outerStart(false) {}
    };

    // Constructing the schedule in the CUDA space (returns a CUDA mem ptr):
    // Args: OuterDim, InnerDim:
    Sched2D*  MkSched2D(int O, int I) const;
  };
}
