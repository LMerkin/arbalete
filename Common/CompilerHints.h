// vim:ts=2:et
//===========================================================================//
//                          "CompilerHints.h":                               //
//                     Optimization-related macros                           //
//===========================================================================//
#pragma once

// Branch prediction optimization (see http://lwn.net/Articles/255364/)
#if !defined(likely) && !defined(unlikely)
#  ifdef __GNUC__
#    define unlikely(expr) __builtin_expect(!!(expr), 0)
#    define likely(expr)   __builtin_expect(!!(expr), 1)
#  else
#    define unlikely(expr) (expr)
#    define likely(expr)   (expr)
#  endif
#endif
