// vim:ts=2:et
//===========================================================================//
//                               "Threading.h":                              //
//    Multi-Threading Support (via C++11 Standard Library, or via Boost):    //
//===========================================================================//
#pragma once

#ifdef __CUDACC__
#error "Threading.h cannot be used in CUDA compilations"
#endif

//---------------------------------------------------------------------------//
// C++11 standard threading support is available:                            //
//---------------------------------------------------------------------------//
#include <thread>
#include <mutex>
#include <condition_variable>

namespace Arbalete
{
  using     Thread     = std::thread;
  using     Mutex      = std::mutex;
  using     CondVar    = std::condition_variable;
  using     LockGuard  = std::lock_guard<Mutex>;
  using     UniqLock   = std::unique_lock<Mutex>;
  namespace ThisThread = std::this_thread;
}
