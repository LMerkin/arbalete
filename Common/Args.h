// vim:ts=2 et sw=2
//===========================================================================//
//                               "Args.h":                                   //
//                     Argument-processing functions                         //
//===========================================================================//
#pragma once

#include <cstdlib>
#include <string>
#include <string.h>

//===========================================================================//
// "out":                                                                    //
//===========================================================================//
/// A helper function implemented to signify a calling convetion where an
/// argument is an "out" parameter in a function call
//
template <typename T>
constexpr T& out(T& arg) { return arg; }

//===========================================================================//
// "inout":                                                                  //
//===========================================================================//
/// A helper function implemented to signify a calling convetion where an
/// argument is an "out" parameter in a function call
//
template <typename T>
constexpr T& inout(T& arg) { return arg; }


namespace Arbalete
{
  //=========================================================================//
  // "ProgramArgs":                                                          //
  //=========================================================================//
  /// Iteratively parses command options.
  //
  class ProgramArgs
  {
    int     m_argc;
    char**  m_argv;
    int     m_idx;

    static void Store(int&    a_val, const char* s) { a_val = std::stoi(s); }
    static void Store(long&   a_val, const char* s) { a_val = std::stol(s); }
    static void Store(double& a_val, const char* s) { a_val = std::stod(s); }
    static void Store(std::string& a,const char* s) { a     = s;            }

  public:
    ProgramArgs(int a_argc, char** a_argv)
    : m_argc(a_argc), m_argv(a_argv), m_idx(0)
    {}

    //-----------------------------------------------------------------------//
    // "Match":                                                              //
    //-----------------------------------------------------------------------//
    /// Checks if \a a_opt is present at i-th position in the m_argv list.
    /// @param a_opt name of the option to match
    //
    bool Match(const char* a_opt) const
    {
      return strcmp(m_argv[m_idx], a_opt) == 0;
    }

    bool Match(const char* a_opt, bool& a_value) const
    {
      a_value = Match(a_opt);
      return a_value;
    }

    bool Match(const char* a_short, const char* a_long) const
    {
      return Match(a_short) || Match(a_long);
    }

    bool Match(const char* a_short, const char* a_long, bool& a_value) const
    {
      return Match(a_short, a_value) || Match(a_long, a_value);
    }

    bool IsHelp() const
    {
      bool res;
      return Match("-h", "--help", res);
    }

    //-----------------------------------------------------------------------//
    // "MatchArg":                                                           //
    //-----------------------------------------------------------------------//
    /// Checks if \a a_opt is present at i-th position in the m_argv list.
    /// @param a_opt name of the option to match
    /// @param i is the position of the option in the m_argv argument list.
    //
    template <typename T>
    bool MatchArg(const char* a_opt, T& a_value)
    {
      auto n   = strlen(a_opt);
      if (m_argv[m_idx][0] == '-')
      {
        if (!strcmp(m_argv[m_idx], a_opt) &&
            (m_idx < m_argc-1 && m_argv[m_idx+1][0] != '-'))
        {
          Store(a_value, m_argv[++m_idx]);
          return true;
        }
        else if (!strncmp(m_argv[m_idx], a_opt, n) &&
                  strlen (m_argv[m_idx]) > n && m_argv[m_idx][n] == '=')
        {
          Store(a_value, m_argv[m_idx] + n + 1);
          return true;
        }
      }
      return false;
    }

    template <typename T>
    bool MatchArg(const char* a_short_opt, const char* a_long_opt, T& a_value)
    {
      return MatchArg(a_short_opt, a_value) || MatchArg(a_long_opt, a_value);
    }

    void Reset()        { m_idx = 0;               }
    bool Next()         { return ++m_idx < m_argc; }
    bool End()    const { return m_idx >= m_argc;  }

    const char* operator()() const { return m_idx < m_argc ? m_argv[m_idx]:""; }
  };

} // End namespace Arbalete
