// vim:ts=2:et
//===========================================================================//
//                             "TradingCalendar.h":                          //
//                Trading Calendars for Diffusions and Grids                 //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#ifndef  __CUDACC__
#include "Common/DateTime.h"
#include <boost/date_time/local_time/local_time.hpp>
#include <string>
#include <vector>
#include <utility>
#include <memory>
#endif   // !__CUDACC__

namespace Arbalete
{
  //=========================================================================//
  // "TradingCalendar":                                                      //
  //=========================================================================//
  class TradingCalendar
  {
  public:
    //-----------------------------------------------------------------------//
    // "CalReg": The Calendar-Based Regime:                                  //
    //-----------------------------------------------------------------------//
    // Used in regime-switching Diffusions and Grids:
    //
    enum CalReg
    {
      TradingHours = 0,   // Normal day-time trading hours
      OverNight    = 1,   // Night between trading hours on a week-day
      WeekEndHol   = 2    // Week-end / holiday
    };

  // The rest of this class is NOT compiled under CUDA:
# ifndef __CUDACC__

  private:
    //-----------------------------------------------------------------------//
    // Data Members:                                                         //
    //-----------------------------------------------------------------------//
    Time              m_openTimeLocal;  // Exchange opening/closing time
    Time              m_closeTimeLocal; // (on trading days)
    boost::local_time::time_zone_ptr
                      m_TZ;             // Time Zone
    std::vector<Date> m_hols;           // Public holidays (for any period)

    // The default ctor is hidden:
    TradingCalendar();

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor and Accessors:                                       //
    //-----------------------------------------------------------------------//
    TradingCalendar
    (
      Time                     open_time_local,
      Time                     close_time_local,
      std::string const&       tz,
      std::vector<Date> const& holidays
    );

    // NB: Open and Close times are in GMT, and depend on the resp calendar
    // date (at least because of summer/winter time):
    //
    Time OpenTimeGMT (Date date) const;
    Time CloseTimeGMT(Date date) const;

    // Converting any time (interpreted as local) to GMT:
    DateTime ToGMT(DateTime t)   const;

    std::vector<Date> const&   Holidays() const { return m_hols; }
    bool IsHoliday(Date d)       const;

    //-----------------------------------------------------------------------//
    // The Factory method:                                                   //
    //-----------------------------------------------------------------------//
    // Currently supported names: see the implementation
    //
    static std::shared_ptr<TradingCalendar>
      MkTradingCalendar(std::string const& cal_name);

# endif // !__CUDACC__
  };

  //=========================================================================//
  // "YF":                                                                   //
  //=========================================================================//
  // Convert Time "delta" to a Year Fraction using the "ACT/365.35" convention.
  // NB:
  // (*) these functions are calendar- and CalReg-independent;
  // (*) "delta" CAN be negative, but currently no longer than 68 years on Win-
  //     dows with MSVC:
  //-------------------------------------------------------------------------//
  // Interval expressed in floating-point seconds:                           //
  //-------------------------------------------------------------------------//
  // Number of seconds in a year with ACT/365.25 convention:
  //
  template<typename F>
  DEVICE inline F SecsPerYear_365_25()  { return F(31557600.0); }

  template<typename F>
  DEVICE inline F YF(F delta_sec)
    { return delta_sec / SecsPerYear_365_25<F>(); }
  
# ifndef __CUDACC__
  //-------------------------------------------------------------------------//
  // Interval expressed as "Time":                                           //
  //-------------------------------------------------------------------------//
  template<typename F>
  inline F YF(Time delta)
  {
    F res = F(delta.total_seconds()) / SecsPerYear_365_25<F>();
    assert((!delta.is_negative()) == (res >= F(0.0)));
    return res;
  }

  // Same for two "DateTime" instants:
  template<typename F>
  inline F YF(DateTime t0, DateTime t1)
    { return YF<F>(t1 - t0); }

  //-------------------------------------------------------------------------//
  // Back: YF -> Time:                                                       //
  //-------------------------------------------------------------------------//
  template<typename F>
  inline Time TimeOfYF(F yf)
  {
    Time res = TimeGener::seconds(long(Round(SecsPerYear_365_25<F>() * yf)));
    assert((!res.is_negative()) == (yf >= F(0.0)));
    return res;
  }

  //-------------------------------------------------------------------------//
  // Related: MidPoint between two instants:                                 //
  //-------------------------------------------------------------------------//
  template<typename F>
  inline DateTime MidPoint(DateTime t0,  DateTime t1)
    { return t0 + TimeOfYF<F>(F(0.5) * YF<F>(t1 - t0)); }

  //=========================================================================//
  // Regime-Switching Events:                                                //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // Computing the Regime-Switching Events:                                  //
  //-------------------------------------------------------------------------//
  // "Fwd" and "Bwd" refer to the time flow direction; "t1" is the time of the
  // next regime-switching event in that direction; if "t0" is a switching in-
  // stant itself, it is NEVER returned by this funcs. I.e.: post-conds:
  // Fwd:  t1 > t0;
  // Bwd:  t1 < t0;
  // reg01 is the regime over [t0..t1];
  // NB : "cal" may be NULL, in that case the "24/7" calendar without any week-
  // ends or holidays is used implicitly -- as typical for Crypto Exchanges:
  //
  void GetNextFwdEvent
  (
    DateTime t0, TradingCalendar const* cal, DateTime* t1,
    TradingCalendar::CalReg* reg01
  );

  void GetNextBwdEvent
  (
    DateTime t0, TradingCalendar const* cal, DateTime* t1,
    TradingCalendar::CalReg* reg01
  );

  //-------------------------------------------------------------------------//
  // Computing the Regime of a Given Time Instant:                           //
  //-------------------------------------------------------------------------//
  // The following "GetRegE" and "RegRegI" funcs are only different  in the
  // treatment of "t0" which is a regime-switching instant itself, as speci-
  // fied below.   In all cases, "cal" may be NULL (implies "24/7").

  // "GetRegI":
  // Returns the previous regime (reg(t0) = reg(t0 -+ eps)) for Fwd/Bwd time
  // flow (resp), hence "I" for "Implicit";
  //
  template<bool IsFwd>
  TradingCalendar::CalReg GetRegI(DateTime t0, TradingCalendar const* cal);

  // "GetRegE":
  // Returns the subsequent regime (reg(t0) = reg(t0 +- eps)) for Fwd/Bwd time
  // flow, hence "E" for "Explicit":
  //
  template<bool IsFwd>
  TradingCalendar::CalReg GetRegE(DateTime t0, TradingCalendar const* cal);

# endif  // !__CUDACC__

  //=========================================================================//
  // "TimeLine" Generation:                                                  //
  //=========================================================================//
  // The following "TimeLine" (a vector of "TimeNode"s) can be used eg for Time
  // Marshalling in Grids or LLH functions. It is suitable for use in CUDA mode
  // (contains only CUDA-friendly types).
  template<typename F>
  struct TimeNode
  {
    long                    m_secs; // Time in seconds since a given "t_from"
    F                       m_ty;   // Same time interval as a Year Frac
    TradingCalendar::CalReg m_reg;  // Regime from this time on (Fwd)
    int                     m_stk;  // Node's idx in "special_times", or (-1)
  };

# ifndef __CUDACC__
  //-------------------------------------------------------------------------//
  // "MkTimeLine":                                                           //
  //-------------------------------------------------------------------------//
  // NB: The resulting TimeLine is fully meaningful only in the context of
  // "t_from" and "special_times" which are NOT stored in the result  (for
  // CUDA compatibility).
  // Unlike eg "Grid2D::TimeMarshal", this function does NOT allow for dynamic
  // time step adjustments (via a user call-back).
  // Use ZERO time step(s) if they are to be ignored (and then only "special_
  // times" and regime-switching times used in the schedule constructed:
  //
  template<bool IsFwd, typename F>
  void MkTimeLine
  (
    DateTime                      t_from, // TimeLine beginning
    Time                          dts[3], // [dtTH, dtON, dtWEH]
    DateTime                      t_end,
    TradingCalendar const*        cal,    // May be NULL, then "24/7" is used
    std::vector<DateTime> const*  special_times,
                                          // May be NULL  or empty
    std::vector<TimeNode<F>>*     res     // Resulting TimeLine (auto-sized)
  );

  //=========================================================================//
  // Utils used by "MkTimeLine" and Friends:                                 //
  //=========================================================================//
  // Exported (but in a nested namespace) for use in Grids:
  //
  namespace TimeMarshalUtils
  {
    //-----------------------------------------------------------------------//
    // "ChkSpecialTimes":                                                    //
    //-----------------------------------------------------------------------//
    // Returns the index "sti" which is the 1st index in "special_times"
    // such that:
    // special_times[sti] >= tFrom (for Fwd mode), or
    // special_times[sti] <= tFrom (for Bwd mode):
    //
    template<bool IsFwd>
    int ChkSpecialTimes
    (
      std::vector<DateTime> const*  special_times,
      DateTime                      tFrom,
      DateTime                      tTo
    );

    //-----------------------------------------------------------------------//
    // "StepFwd":                                                            //
    //-----------------------------------------------------------------------//
    // Returns:
    // "tn" : next step boundary, tn > t0;
    // "sti": 1st idx such that special_times[sti] > tn, in particular can get
    //        sti = special_times.size() (so not a valid idx);
    //        NB: unlike "ChkSpecialTimes", inequality is STRICT here (>);
    // "reg": regime for [t0 -> tn]:
    //
    void StepFwd
    (
      DateTime                      t0,     // Curr time from which step made
      Time                          dts[3],
      DateTime                      t_end,
      TradingCalendar const*        cal,
      std::vector<DateTime> const*  special_times,
      int*                          sti,
      DateTime*                     tn,
      TradingCalendar::CalReg*      reg
    );

    //-----------------------------------------------------------------------//
    // "StepBwd":                                                            //
    //-----------------------------------------------------------------------//
    // Returns:
    // "tn" : next step boundary, tn < t0;
    // "sti": 1st idx such that special_times[sti] < tn, in particular can get
    //        sti = -1 (so not a valid idx);
    //        NB: unlike "ChkSpecialTimes", inequality is STRICT here (<);
    // "reg": regime for [tn <- t0]:
    //
    void StepBwd
    (
      DateTime                      t0,     // Curr time from which step made
      Time                          dts[3],
      DateTime                      t_end,
      TradingCalendar const*        cal,
      std::vector<DateTime> const*  special_times,
      int*                          sti,
      DateTime*                     tn,
      TradingCalendar::CalReg*      reg
    );

    //-----------------------------------------------------------------------//
    // "IsSpecialTime":                                                      //
    //-----------------------------------------------------------------------//
    // Pre-condition:
    // "sti" is an initial guess for the result ("stk"); if not (-1), it is
    //       such that:
    //       special_times[sti] >= t_curr (Fwd), or
    //       special_times[sti] <= t_curr (Bwd);
    // Returns "stk" such that:
    //       special_times[stk]  = t_curr, or (-1) if such idx does not exist:
    //
    template<bool IsFwd>
    int IsSpecialTime
    (
      DateTime                      t_curr,
      std::vector<DateTime> const*  special_times,
      int                           sti
    );
  }
# endif // !__CUDACC__
}
