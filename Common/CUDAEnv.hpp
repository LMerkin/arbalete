// vim:ts=2:et
//===========================================================================//
//                              "CUDAEnv.hpp":                               //
//                  CUDA Configs, Environment and Helpers:                   //
//===========================================================================//
// NB: Include this header to get implementations of "CUDAEnv" templated meth-
// ods (there are no default intances of them) as well  as for Load Balancing
// functions on the CUDA side:
//
#pragma once

#include "Common/Maths.hpp"
#include "Common/CUDAEnv.h"
#include <cassert>
#include <string>
#include <stdexcept>

//===========================================================================//
// Error Handling Macros:                                                    //
//===========================================================================//
#define CUDA_SAFE_CALL(Call, ErrMsg) \
{ \
  cudaError_t rc = (Call); \
  if (rc != cudaSuccess) \
  { \
    std::string fullMsg = std::string(ErrMsg ": ") + cudaGetErrorString(rc); \
    throw std::runtime_error(std::string(fullMsg)); \
  } \
}

#define CUDA_CHECK_STATUS(ErrMsg) \
{ \
  cudaError_t rc = cudaGetLastError(); \
  if (rc != cudaSuccess) \
  { \
    std::string fullMsg = std::string(ErrMsg ": ") + cudaGetErrorString(rc); \
    throw std::runtime_error(fullMsg); \
  } \
}

namespace Arbalete
{
  //=========================================================================//
  // HOST Mode Functions:                                                    //
  //=========================================================================//
  // NB: "SetCUDADevice" does not use any mutexes, so it can (and should) be
  // inlined in NVCC compilations.   All other functions are Host-mode-only:
  //
  //=========================================================================//
  // Device Management, Synchronisation and Timing:                          //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // "SetCUDADevice":                                                        //
  //-------------------------------------------------------------------------//
  inline void CUDAEnv::SetCUDADevice() const
    { CUDA_SAFE_CALL(cudaSetDevice(m_DevID), "Cannot set CUDA device"); }

# ifndef __CUDACC__
  //
  // If CUDA Timer is disabled, the following calls are simply ignored instead
  // of raising an exception:
  //
  //-------------------------------------------------------------------------//
  // "Start":                                                                //
  //-------------------------------------------------------------------------//
  // If the CUDA timer is not used, the Start Event is not posted:
  //
  inline void CUDAEnv::Start() const
  {
    if (m_WithTimer)
    {
      LockGuard lock(s_DevMtx);
      SetCUDADevice();
      CUDA_SAFE_CALL
        (cudaEventRecord(m_StartEv, m_Stream), "Cannot post Start event");
    }
  }

  //-------------------------------------------------------------------------//
  // "Stop":                                                                 //
  //-------------------------------------------------------------------------//
  // Post the Stop Event, synchronise and possibly update the Cumulative Timer:
  //
  inline void CUDAEnv::Stop() const
  {
    // Post the Stop Event -- in any case:
    {
      LockGuard lock(s_DevMtx);
      SetCUDADevice();
      CUDA_SAFE_CALL
        (cudaEventRecord(m_StopEv, m_Stream), "Cannot post Stop event");
    }

    // Now synchronisation: Setting the current CUDA device is NOT required.
    // Start event is normally not required for synchronisation in any case,
    // and is anyway not used without the Timer, so:
    if (m_WithTimer)
      CUDA_SAFE_CALL
      (cudaEventSynchronize(m_StartEv), "Cannot sync Start event");

    // But the Stop Event is always synchronised upon:
    CUDA_SAFE_CALL(cudaEventSynchronize(m_StopEv), "Cannot sync Stop  event");

    // Possibly update the Cumulative Timer:
    if (m_WithTimer)
    {
      float dt = 0.0f;
      CUDA_SAFE_CALL
      (
        cudaEventElapsedTime(&dt, m_StartEv, m_StopEv),
        "Cannot get CUDA time"
      );
      m_CumTimeMS += dt;
    }
  }

  //=========================================================================//
  // Host <-> Device(CUDA) Data Transfers:                                   //
  //=========================================================================//
  // Use async mode over the given Stream to prevent unnecessory synchronisati-
  // on across multiple Streams:
  //
  //-------------------------------------------------------------------------//
  // "ToCUDA":                                                               //
  //-------------------------------------------------------------------------//
  // Copy one array of length "K" into CUDA device memory:
  //
  template<typename  T>
  inline void CUDAEnv::ToCUDA(T const* hostSrc, T* cudaDst, int K) const
  {
    assert(hostSrc != nullptr && cudaDst != nullptr && K >= 0);
    LockGuard lock(s_DevMtx);

    SetCUDADevice();
    CUDA_SAFE_CALL
    (
      cudaMemcpyAsync
        (cudaDst, hostSrc, K * sizeof(T), cudaMemcpyHostToDevice, m_Stream),
      "Host->CUDA copy error"
    )
  }

  //-------------------------------------------------------------------------//
  // "ToHost":                                                               //
  //-------------------------------------------------------------------------//
  // The inverse of "ToCUDA":
  //
  template<typename  T>
  inline void CUDAEnv::ToHost(T const* cudaSrc, T* hostDst, int K) const
  {
    assert(cudaSrc != nullptr && hostDst != nullptr && K >= 0);
    LockGuard lock(s_DevMtx);

    SetCUDADevice();
    CUDA_SAFE_CALL
    (
      cudaMemcpyAsync
        (hostDst, cudaSrc, K * sizeof(T), cudaMemcpyDeviceToHost, m_Stream),
      "CUDA->Host copy error"
    )
  }

  //=========================================================================//
  // CUDA Device Memory Allocations:                                         //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // "CUDAAlloc":                                                            //
  //-------------------------------------------------------------------------//
  // Allocates a single array of type "F" entries and length "K" in the CUDA
  // device memory:
  //
  template<typename T>
  inline T* CUDAEnv::CUDAAlloc(int K) const
  {
    LockGuard lock(s_DevMtx);
    T* res = nullptr;

    SetCUDADevice();
    CUDA_SAFE_CALL
    (
      cudaMalloc((void**)(&res), K * sizeof(T)),
      "CUDA memory allocation error"
    )
    assert(res != nullptr);
    return res;
  }

  //-------------------------------------------------------------------------//
  // "CUDAZeroOut":                                                          //
  //-------------------------------------------------------------------------//
  // Zeros out the given chunk of CUDA device memory:
  //
  template<typename T>
  inline void CUDAEnv::CUDAZeroOut(T* ptr, int K) const
  {
    LockGuard lock(s_DevMtx);

    SetCUDADevice();
    CUDA_SAFE_CALL
    (
      cudaMemset((void*)(ptr), 0, K * sizeof(T)),
      "CUDA memory zeroing-out error"
    );
  }

  //-------------------------------------------------------------------------//
  // "CUDAFree":                                                             //
  //-------------------------------------------------------------------------//
  template<typename T>
  inline void CUDAEnv::CUDAFree(T*& ptr) const
  {
    if (ptr == nullptr)
      return;
    LockGuard lock(s_DevMtx);

    SetCUDADevice();
    CUDA_SAFE_CALL(cudaFree(ptr), "CUDA memory de-allocation error")
    ptr = nullptr;
  }
# endif // !__CUDACC__
}
