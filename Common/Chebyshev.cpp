// vim:ts=2:et
//===========================================================================//
//                             "Chebyshev.cpp":                              //
//        Pre-Defined Instances of Chebyshev Polynomial Functions            //
//===========================================================================//
#include "Common/Chebyshev.hpp"

namespace Arbalete
{
  template struct Chebyshev<double>;
  template struct Chebyshev<float>;
}
