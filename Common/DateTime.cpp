// vim:ts=2:et
//===========================================================================//
//                              "DateTime.cpp":                              //
//                       Date / Time Types and Utils                         //
//===========================================================================//
#include "Common/DateTime.h"
#include "Common/Maths.hpp"
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <sstream>
#include <stdexcept>
#include <Common/CompilerHints.h>

namespace
{
  using namespace Arbalete;

  //-------------------------------------------------------------------------//
  // "DiddledUTC":                                                           //
  //-------------------------------------------------------------------------//
  bool     UseDiddledUTC = false;
  DateTime DiddledUTC;
}

namespace Arbalete
{
  using namespace std;

  //-------------------------------------------------------------------------//
  // "TimeOfStr":                                                            //
  //-------------------------------------------------------------------------//
  // String Format: X{msec|sec|s|min|m|hr|h|day|d}
  //
  Time TimeOfStr(string const& str)
  {
    char*       end = nullptr;
    char const* st  = str.c_str();
    long        val = strtol(st, &end, 10);

    assert(end != nullptr);
    if (end == st)
      // No valid digits at all:
      throw invalid_argument("TimeOfStr: Invalid value in "+ str);

    Time res;
    if (strcmp(end, "msec") == 0 || strcmp(end, "ms") == 0)
      res = TimeGener::milliseconds(val);
    else
    if (strcmp(end, "sec")  == 0 || strcmp(end, "s")  == 0)
      res = TimeGener::seconds(val);
    else
    if (strcmp(end, "min")  == 0 || strcmp(end, "m")  == 0)
      res = TimeGener::minutes(val);
    else
    if (strcmp(end, "hr")   == 0 || strcmp(end, "h")  == 0)
      res = TimeGener::hours(val);
    else
    if (strcmp(end, "day")  == 0 || strcmp(end, "d")  == 0)
      res = TimeGener::hours(val * 24);

    // Otherwise:
    if (res == Time())
      throw invalid_argument("TimeOfStr: Invalid postfix in "+ str);

    assert((!res.is_negative()) == (val >= 0));
    return res;
  }

  //-------------------------------------------------------------------------//
  // "DaysInMonth":                                                          //
  //-------------------------------------------------------------------------//
  int DaysInMonth (int year, int mon)
  {
    static const int s_days[] = { 0, 31,28,31,30, 31,30,31,31, 30,31,30,31 };

    if (unlikely(mon < 1 || mon > 12))
        throw invalid_argument("DaysInMonth: Invalid month");
    int res = s_days[mon];
    if (mon == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
      ++res;

    return res;
  }

  //-------------------------------------------------------------------------//
  // "CurrUTC":                                                              //
  //-------------------------------------------------------------------------//
  DateTime CurrUTC()
  {
    return
      (!UseDiddledUTC)
      ? // Normal "wall clock" UTC:
        boost::posix_time::microsec_clock::universal_time()
      : // "Diddled" UTC:
        DiddledUTC;
  }

  //-------------------------------------------------------------------------//
  // "SetUTCDiddle":                                                         //
  //-------------------------------------------------------------------------//
  // Sets a "diddle" on Current UTC:
  //
  void SetUTCDiddle(DateTime utc)
  {
    UseDiddledUTC = true;
    DiddledUTC    = utc;
  }

  //-------------------------------------------------------------------------//
  // "RemoveUTCDiddle":                                                      //
  //-------------------------------------------------------------------------//
  void RemoveUTCDiddle()
  {
    UseDiddledUTC = false;
    DiddledUTC    = DateTime();
  }
  
  string DateTimeToFStr(DateTime dt, char const* str)
  {
    locale loc(locale::classic(), new boost::posix_time::time_facet(str));
    stringstream ss;
    ss.imbue(loc);
    ss << dt;
    return ss.str();
  }
}
