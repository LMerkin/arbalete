// vim:ts=2:et
//===========================================================================//
//                                "CUDAEnv.cpp":                             //
//                   CUDA Configs, Environment and Helpers:                  //
//===========================================================================//
#include "Common/Maths.hpp"
#include "Common/CUDAEnv.hpp"
#include <cassert>
#include <stdexcept>
#include <vector>

namespace
{
  using namespace Arbalete;

  //=========================================================================//
  // "LoadBalanceX":                                                         //
  //=========================================================================//
  // Returns the range [from..to] (inclusive) of Items (out of the full range
  // [0..I-1]) to be processed by the Worker "w" (e.g. a Thread of Block); on
  // average, there are "IpW" Items per Worker:
  //
  inline void LoadBalanceX(int I, double IpW, int w, int* from, int* to)
  {
    assert(0 <= I && 0 <= w && from != nullptr && to != nullptr);

    *from = Min<int>(int(Round(double(w)     * IpW)),     I-1);
    *to   = Min<int>(int(Round(double(w + 1) * IpW)) - 1, I-1);

    // NB: this provides PARTITIONING on the range of Items between Workers:
    // moving to the next Worker  (w+1)  would give  it a directly adjacent,
    // and not overlapping, range of Items to process.
  }
}

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // Static Mutex:                                                           //
  //=========================================================================//
  mutex CUDAEnv::s_DevMtx;

  void CUDAEnv::Lock()
    { s_DevMtx.lock(); }

  void CUDAEnv::Unlock()
    { s_DevMtx.unlock(); }

  //-------------------------------------------------------------------------//
  // Non-Default Ctor:                                                       //
  //-------------------------------------------------------------------------//
  CUDAEnv::CUDAEnv(Params const& params)
  {
    // If any of the args is outside valid range, CUDA is not to be used:
    if (params.m_DevID     <  0 || params.m_BlocksPerSM <= 0 ||
        params.m_BlockSize <= 0)
      throw invalid_argument("CUDAEnv Ctor: Invalid CUDA param(s)");

    // Get the CUDA Device Properties:
    cudaDeviceProp devProps;
    CUDA_SAFE_CALL
    (
      cudaGetDeviceProperties(&devProps, params.m_DevID),
      "CUDA device unavailable"
    )

    // Device found:
    m_DevID = params.m_DevID;

    // Thread Blocks per SM: the maximum depends on Compute Capability.  Excee-
    // ding this maximum will not result in an error but will negatively affect
    // persormance, so:
    //
    int maxThrBlocksPerSM = (devProps.major == 3) ? 16 : 8;
    if (params.m_BlocksPerSM > maxThrBlocksPerSM)
      throw invalid_argument("Too many thread blocks per SM");
    m_BlocksPerSM = params.m_BlocksPerSM;

    // Number of SMs and the total number of thread blocks:
    //
    m_ShMemPerSM = int(devProps.sharedMemPerBlock); // In fact, per SM!
    m_NSMs       = devProps.multiProcessorCount;
    m_NBlocks    = m_NSMs *  m_BlocksPerSM;

    if (params.m_BlockSize > devProps.maxThreadsPerBlock ||
        params.m_BlockSize % devProps.warpSize != 0)
    {
      char errMsg[256];
      sprintf(errMsg, "CUDA Thread Block Size: %d..%d, ~%d",
              devProps.warpSize, devProps.maxThreadsPerBlock,
              devProps.warpSize);
      throw invalid_argument(string(errMsg));
    }
    m_BlockSize  = params.m_BlockSize;
    m_NThreads   = m_NBlocks * m_BlockSize;
    assert(m_NThreads > 0);   // Over-all check

    m_MaxThreads = m_NSMs * devProps.maxThreadsPerMultiProcessor;
    if (m_NThreads > m_MaxThreads)
    {
      char errMsg[256];
      snprintf(errMsg, 256,
               "Number of CUDA Threads (%d) Exceeds the Limit (%d)",
               m_NThreads, m_MaxThreads);
      throw invalid_argument(string(errMsg));
    }

    // Initialise the Stream and the Events:
    //
    CUDA_SAFE_CALL(cudaSetDevice(m_DevID),      "Cannot set CUDA device")
    CUDA_SAFE_CALL(cudaStreamCreate(&m_Stream), "Cannot create CUDA Stream")

    m_WithTimer = params.m_WithTimer;
    m_CumTimeMS = 0.0f;

    unsigned int eventFlags =
      cudaEventBlockingSync |                     // To avoid busy-wait by CPU
      (m_WithTimer ? 0 : cudaEventDisableTiming); // Timer m.b. optimised off

    CUDA_SAFE_CALL
    (
      cudaEventCreateWithFlags(&m_StartEv, eventFlags),
      "Cannot create CUDA Start event"
    )
    CUDA_SAFE_CALL
    (
      cudaEventCreateWithFlags(&m_StopEv,  eventFlags),
      "Cannot create CUDA Stop event"
    )
  }

  //=========================================================================//
  // Dtor:                                                                   //
  //=========================================================================//
  // NB: No "CUDA_SAFE_CALL" here to avoid raising exceptions in the Dtor:
  //
  CUDAEnv::~CUDAEnv()
  {
    assert(m_Stream != nullptr && m_StartEv != nullptr && m_StopEv != nullptr);
    cudaSetDevice    (m_DevID);
    cudaStreamDestroy(m_Stream);
    cudaEventDestroy (m_StartEv);
    cudaEventDestroy (m_StopEv);
  }

  //=========================================================================//
  // "MkSched1D":                                                            //
  //=========================================================================//
  CUDAEnv::Sched1D* CUDAEnv::MkSched1D(int K) const
  {
    // Allocate a Host-based tmp array to be filled in:
    assert(K >= 0 && m_NThreads > 0);
    vector<Sched1D> tmp(m_NThreads);

    // Fill it in:
    double KsT = double(K) / double(m_NThreads);

    for (int ti = 0; ti < m_NThreads; ++ti)
    {
      // Get the range of items (out of [0..K-1]) to be processed by this "ti":
      int from =  0;
      int to   = -1;
      LoadBalanceX(K, KsT, ti, &from, &to);

      // Trivially, if ti==0, then from==0 as well. We should also have
      // to==K-1 for the last "ti":
      assert(ti != m_NThreads - 1 || to == K-1);

      // We may have from > to; the user will decide what to do in that case:
      assert(0 <= from && to <= K-1);

      tmp[ti].m_from = from;
      tmp[ti].m_to   = to;
    }

    // Now allocate "Sched1D" array in the CUDA memory, and copy "tmp"
    // into it:
    Sched1D* res = CUDAAlloc<Sched1D>(m_NThreads);
    ToCUDA<Sched1D>(&(tmp[0]), res, m_NThreads);
    return   res;
  }

  //=========================================================================//
  // "MkSched2D":                                                            //
  //=========================================================================//
  CUDAEnv::Sched2D* CUDAEnv::MkSched2D(int O, int I) const
  {
    // Allocate a Host-based tmp array to be filled in:
    assert(O >= 0 && I >= 0 && m_NBlocks > 0 && m_BlockSize > 0 &&
           m_NThreads  == m_NBlocks * m_BlockSize);

    vector<Sched2D> tmp(m_NThreads);

    // Each Thread Block will process some number of "e"s, and each "e" is pro-
    // cessed by Threads belonging to just one Block (so the Treads can be syn-
    // chronised):
    double avOpB = double(O) / double(m_NBlocks);

    for (int bi = 0; bi < m_NBlocks; ++bi)
    {
      // Get the range of "e"s (out of the total range [0..O-1]) to be procd by
      // by the curr Block "bi":
      int oFrom = -1;
      int oTo   =  0;
      LoadBalanceX(O, avOpB, bi, &oFrom, &oTo);

      if (oFrom > oTo)
        // The whole Block has nothing to do! XXX: inefficiency:
        continue;

      // Number of "e"s to be processed by this block:
      int OsB = oTo - oFrom + 1;
      assert(OsB >= 1 && 0 <= oFrom && oFrom <= oTo && oTo <= O-1);

      // Now: there are "m_BlockSize" Threads in the curr Block, and they are
      // processing "OsB" "e"s;  XXX: we currently require that there must be
      // at least 1 Thread for each "e", so each Thread works with just 1 "e"
      // (more precisely, a sub-range of its "l" indices) -- otherwise implem-
      // entation would be more complicated (would need to memoise individual
      // "l" ranges for each "e" processed by the curr thread):
      //
      double avTpO = double(m_BlockSize) / double(OsB);

      for (int o = oFrom; o <= oTo; ++o)
      {
        // Get the range of Threads (within the Block) to work on this "e":
        int thrFrom =  0;
        int thrTo   = -1;
        LoadBalanceX(m_BlockSize, avTpO, o - oFrom, &thrFrom, &thrTo);

        if (thrFrom > thrTo)
          // This is an error condition: Not enough threads to do this "e"!
          throw runtime_error("CUDAEnv::Sched2D: Insufficient parallelism");

        // Otherwise: Number of Threads for this "o":
        int TsO = thrTo - thrFrom + 1;
        assert(1 <= TsO && 0 <= thrFrom && thrFrom <= thrTo &&
               thrTo <= m_BlockSize-1);

        // Finally, for each Thread, get the range of "i" indices (from the
        // full range [0..I-1]) it will process:
        //
        double avIpT = double(I) / double(TsO);

        for (int ti  = thrFrom; ti <= thrTo; ++ti)
        {
          int iFrom =  0;
          int iTo   = -1;
          LoadBalanceX(I, avIpT, ti - thrFrom, &iFrom, &iTo);

          // If iFrom > iTo, this might be OK: even if this Thread does not
          // process any "i"s, it can (unlicke an unused Block) be involved
          // in data post-processing, so keep it:
          //
          assert(0 <= iFrom && iTo <= I-1);

          // Save the results under the global "Tn" for this Thread:
          int Tn = bi * m_BlockSize + ti;
          assert(0 <= Tn && Tn <= m_NThreads-1);

          tmp[Tn].m_outer      = o;
          tmp[Tn].m_innerFrom  = iFrom;
          tmp[Tn].m_innerTo    = iTo;
          tmp[Tn].m_outerStart = (ti == thrFrom);

#         ifndef NDEBUG
          // Verify the continuity of the schedule created:
          if (Tn != 0)
          {
            assert((tmp[Tn].m_outer == tmp[Tn-1].m_outer      ||
                    tmp[Tn].m_outer == tmp[Tn-1].m_outer + 1));
            if (tmp[Tn].m_innerFrom != 0)
              assert(tmp[Tn].m_innerFrom == tmp[Tn-1].m_innerTo + 1);
            else
              assert(tmp[Tn-1].m_innerTo == I-1);
          }
          else
            assert(tmp[Tn].m_outer == 0   && tmp[Tn].m_innerFrom == 0);

          if (Tn == m_NThreads - 1)
            assert(tmp[Tn].m_outer == O-1 && tmp[Tn].m_innerTo == I-1);
#         endif
        }
      }
    }
    // "tmp" is done -- copy it into the CUDA space:
    //
    Sched2D* res = CUDAAlloc<Sched2D>(m_NThreads);
    ToCUDA<Sched2D>(&(tmp[0]),  res,  m_NThreads);
    return res;
  }
}
