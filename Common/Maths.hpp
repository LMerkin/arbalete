// vim:ts=2:et
//===========================================================================//
//                            "Common/Maths.hpp":                            //
//  Portable Mathematical Functions (Including Templated and CUDA Versions)  //
//===========================================================================//
#pragma  once

#include "Common/Macros.h"
#include <cstdlib>
#include <cfloat>
#include <climits>
#include <cassert>

// In CUDA compilation mode, include CUDA-specific Math header:
#ifdef   __CUDACC__
#include <math_constants.h>
#else    // !__CUDACC__
#include <cmath>
#include <limits>
#include <string>
#include <stdexcept>
#endif   // __CUDACC__

namespace Arbalete
{
  //=========================================================================//
  // Constants:                                                              //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // "Eps":                                                                  //
  //-------------------------------------------------------------------------//
  template<typename F>
  DEVICE inline constexpr F Eps();

  template<>
  DEVICE inline constexpr float  Eps<float>()       { return FLT_EPSILON; }

  template<>
  DEVICE inline constexpr double Eps<double>()      { return DBL_EPSILON; }

  //-------------------------------------------------------------------------//
  // "NaN":                                                                  //
  //-------------------------------------------------------------------------//
  template<typename F>
  DEVICE inline constexpr F NaN();

# ifdef __CUDACC__
  template<>    // XXX NOT constexpr -- requires DEVICE computation!
  DEVICE inline float  NaN<float>()                 { return CUDART_NAN_F; }

  template<>    // XXX NOT constexpr -- requires DEVICE computation!
  DEVICE inline double NaN<double>()                { return CUDART_NAN; }

# else
  template<typename F>
  inline constexpr  F NaN()  { return std::numeric_limits<F>::quiet_NaN();  }
# endif

  //-------------------------------------------------------------------------//
  // "Inf":                                                                  //
  //-------------------------------------------------------------------------//
  template<typename F>
  DEVICE inline constexpr F Inf();

# ifdef __CUDACC__
  template<>    // XXX NOT constexpr -- requires DEVICE computation!
  DEVICE inline float  Inf<float>() { return CUDART_INF_F; }

  template<>    // XXX NOT constexpr -- requires DEVICE computation!
  DEVICE inline double Inf<double>() { return CUDART_INF; }

# else
  template<typename F>
  inline constexpr  F Inf()   { return std::numeric_limits<F>::infinity();  }
# endif

  //-------------------------------------------------------------------------//
  // Algebraic Functions:                                                    //
  //-------------------------------------------------------------------------//
  // "Min", "Max" (templated):
  template<typename F>
  DEVICE inline constexpr F   Min (F x, F y)  { return (x < y) ? x : y; }

  template<typename F>
  DEVICE inline constexpr F   Max (F x, F y)  { return (x > y) ? x : y; }

  // Absolute Value for various types:
  // Generic Case:
  template<typename T>
  DEVICE inline constexpr T   Abs (T x)       { return (x < 0) ? (-x) : x; }

  // Specialisations: Real Absolute Value:
  template<>
  DEVICE inline float  Abs<float> (float  x)  { return fabsf(x);  }

  template<>
  DEVICE inline double Abs<double>(double x)  { return fabs (x);  }

  // Generic Square:
  template<typename F>
  DEVICE inline constexpr F   Sqr    (F x)    { return x * x;     }

  // Square Root:
  DEVICE inline float  SqRt   (float  x)  { return sqrtf(x);  }
  DEVICE inline double SqRt   (double x)  { return sqrt (x);  }

  // Power:
  DEVICE inline float  Pow    (float  x, float  y)  { return powf(x, y); }
  DEVICE inline double Pow    (double x, double y)  { return pow (x, y); }

  // Cubic Root:
  DEVICE inline float  CbRt   (float  x)  { return cbrtf(x);  }
  DEVICE inline double CbRt   (double x)  { return cbrt (x);  }

  // Exp:
  DEVICE inline float  Exp    (float  x)  { return expf (x);  }
  DEVICE inline double Exp    (double x)  { return exp  (x);  }

  // Log:
  DEVICE inline float  Log    (float  x)  { return logf (x);  }
  DEVICE inline double Log    (double x)  { return log  (x);  }

  // Log10:
  DEVICE inline float  Log10  (float  x)  { return log10f(x); }
  DEVICE inline double Log10  (double x)  { return log10 (x); }

  // Log(1+x):
  DEVICE inline float  Log1P  (float  x)  { return log1pf(x); }
  DEVICE inline double Log1P  (double x)  { return log1p (x); }

  //-------------------------------------------------------------------------//
  // "Pi", 1/SqRt(2*Pi):                                                     //
  //-------------------------------------------------------------------------//
  template<typename F>
  DEVICE inline constexpr F Pi();

# ifdef __CUDACC__
  template<>
  DEVICE inline constexpr float  Pi<float>()        { return CUDART_PI_F; }

  template<>
  DEVICE inline constexpr double Pi<double>()       { return CUDART_PI;   }

# else
  template<> inline constexpr float  Pi<float> ()   { return float(M_PI); }
  template<> inline constexpr double Pi<double>()   { return M_PI;        }
# endif

  template<typename F>
  DEVICE inline constexpr F SqRt2Pi()    { return SqRt(F(2.0) * Pi<F>()); }

  template<typename F>
  DEVICE inline constexpr F InvSqRt2Pi()
    { return F(1.0) / SqRt(F(2.0) * Pi<F>()); }

  //-------------------------------------------------------------------------//
  // Trigonomtric Functions and Their Inverses:                              //
  //-------------------------------------------------------------------------//
  DEVICE inline float  Cos    (float  x)  { return cosf (x);  }
  DEVICE inline double Cos    (double x)  { return cos  (x);  }

  DEVICE inline float  Sin    (float  x)  { return sinf (x);  }
  DEVICE inline double Sin    (double x)  { return sin  (x);  }

  DEVICE inline float  Tan    (float  x)  { return tanf(x);   }
  DEVICE inline double Tan    (double x)  { return tan (x);   }

  DEVICE inline float  ASin   (float  x)  { return asinf(x);  }
  DEVICE inline double ASin   (double x)  { return asin (x);  }

  DEVICE inline float  ACos   (float  x)  { return acosf(x);  }
  DEVICE inline double ACos   (double x)  { return acos (x);  }

  DEVICE inline float  ATan   (float  x)  { return atanf(x);  }
  DEVICE inline double ATan   (double x)  { return atan (x);  }

  //-------------------------------------------------------------------------//
  // Hyperbolic Functions and Their Inverses:                                //
  //-------------------------------------------------------------------------//
  DEVICE inline float  CosH   (float  x)  { return coshf(x);  }
  DEVICE inline double CosH   (double x)  { return cosh (x);  }

  DEVICE inline float  SinH   (float  x)  { return sinhf(x);  }
  DEVICE inline double SinH   (double x)  { return sinh (x);  }

  DEVICE inline float  TanH   (float  x)  { return tanhf(x);  }
  DEVICE inline double TanH   (double x)  { return tanh (x);  }

  DEVICE inline float  ACosH  (float  x)  { return acoshf(x); }
  DEVICE inline double ACosH  (double x)  { return acosh (x); }

  DEVICE inline float  ASinH  (float  x)  { return asinhf(x); }
  DEVICE inline double ASinH  (double x)  { return asinh (x); }

  DEVICE inline float  ATanH  (float  x)  { return atanhf(x); }
  DEVICE inline double ATanH  (double x)  { return atanh (x); }

  //-------------------------------------------------------------------------//
  // PDF and CDF of the Standard Normal Distribution:                        //
  //-------------------------------------------------------------------------//
  DEVICE inline double NPDF   (double x)
    { return Exp(- 0.5  * x * x) * InvSqRt2Pi<double>(); }

  DEVICE inline float  NPDF   (float  x)
    { return Exp(- 0.5f * x * x) * InvSqRt2Pi<float>();  }

  DEVICE inline double NCDF   (double x)
    { return 0.5  * (1.0  + erf (x / M_SQRT2)); }

  DEVICE inline float  NCDF   (float  x)
    { return 0.5f * (1.0f + erff(x / float(M_SQRT2))); }

  //-------------------------------------------------------------------------//
  // Rounding, Finiteness:                                                   //
  //-------------------------------------------------------------------------//
  // Floor:
  DEVICE inline float  Floor    (float  x)  { return floorf(x); }
  DEVICE inline double Floor    (double x)  { return floor (x); }

  // Ceil:
  DEVICE inline float  Ceil     (float  x)  { return ceilf(x);  }
  DEVICE inline double Ceil     (double x)  { return ceil (x);  }

  // Round:
  DEVICE inline float  Round    (float  x)  { return roundf(x); }
  DEVICE inline double Round    (double x)  { return round (x); }

  // IsFinite:
# ifdef  __CUDACC__
  DEVICE inline bool   IsFinite (float  x)  { return bool(isfinite(x)); }
  DEVICE inline bool   IsFinite (double x)  { return bool(isfinite(x)); }
# else
         inline bool   IsFinite (float  x)  { return std::isfinite(x);  }
         inline bool   IsFinite (double x)  { return std::isfinite(x);  }
# endif

  // IsNaN:
# ifdef  __CUDACC__
  DEVICE inline bool   IsNaN    (float  x)  { return bool(isnan(x)); }
  DEVICE inline bool   IsNaN    (double x)  { return bool(isnan(x)); }
# else
         inline bool   IsNaN    (float  x)  { return std::isnan(x);  }
         inline bool   IsNaN    (double x)  { return std::isnan(x);  }
# endif

  // IsInf:
# ifdef __CUDACC__
   DEVICE inline bool  IsInf    (float  x)  { return bool(isinf(x)); }
   DEVICE inline bool  IsInf    (double x)  { return bool(isinf(x)); }
# else
          inline bool  IsInf    (float  x)  { return std::isinf(x);  }
          inline bool  IsInf    (double x)  { return std::isinf(x);  }
# endif

  //=========================================================================//
  // Floating-Point Conversions and Types Identification:                    //
  //=========================================================================//
  // "IsFloat":
  template<typename F>
  DEVICE inline constexpr bool IsFloat()                    { return false; }

  template<> DEVICE inline constexpr bool IsFloat<float>()  { return true;  }

  // "IsDouble":
  template<typename F>
  DEVICE inline constexpr bool IsDouble()                   { return false; }

  template<> DEVICE inline constexpr bool IsDouble<double>(){ return true;  }

  //=========================================================================//
  // "ContFrac": Continuous Fraction Expansion:                              //
  //=========================================================================//
  // Approximates x =~= m / n, where min(m,n) <= Limit:
  //
  template<typename F>
  DEVICE inline void ContFrac(F x, long* m, long* n, long Limit)
  {
    assert(m != nullptr && n != nullptr);
    *m = 0;
    *n = 0;

    if (x <= 0.0)
#     ifndef __CUDACC__
      throw std::invalid_argument("ContFrac: Currently requires x > 0");
#     else
      return;
#     endif

    bool inv = false;
    if (x > 1.0)
    {
      x   = 1.0 / x;
      inv = true;
    }
    assert(0.0 < x && x < 1.0);

    // Now proceed:
    long m0 = 0, n0 = 0;

    long m2 = 0;
    long m1 = 1;
    long n2 = 1;
    long n1 = 0;
    long a  = 0;

    while (m0 <= Limit || n0 <= Limit)    // min(m0,n0) <= Limit
    {
      long mA = a * m1 + m2;
      long nA = a * n1 + n2;
      assert(mA >= 0 && nA >= 1 && mA < nA);

      if (mA > Limit && nA > Limit)       // min(mA,nA) > Limit
        // Keep the current (m0, n0):
        break;

      m0 = mA;
      n0 = nA;

      // For the next iteartion:
      x  = 1.0 / x;
      a  = long(Floor(x));
      x -= F(a);

      m2 = m1;  m1 = m0;
      n2 = n1;  n1 = n0;
    }
    // Store the result:
    if (!inv)
      { *m = m0;  *n = n0; }
    else
      { *m = n0;  *n = m0; }
  }

  //=========================================================================//
  // Compile-Time GCD and Normalisation Functions:                           //
  //=========================================================================//
  DEVICE constexpr int GCDrec(int p, int q)
  {
    // 0 <= p <= q
    return (p == 0) ? q : GCDrec(q % p, p);
  }

  DEVICE constexpr int GCD(int m, int n)
  {
    return Abs(m) <= Abs(n)
            ? GCDrec(Abs(m), Abs(n))
            : GCDrec(Abs(n), Abs(m));
  }

  DEVICE constexpr int NormaliseNumer(int m, int n)
  {
    // NB: GCD is always >= 0. Compile-time error occurs if GCD == 0 (which can
    // happen only if m == n == 0):
    return (n > 0)
            ?   (m / GCD(m, n))
            : - (m / GCD(m, n));
  }

  DEVICE constexpr int NormaliseDenom(int m, int n)
    { return Abs(n) / GCD(m, n); }

  //=========================================================================//
  // Power Templated Functions:                                              //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // Power Expr with a Natural Degree:                                       //
  //-------------------------------------------------------------------------//
  // The Even Positive case:
  template<typename T, int M, bool IsEven = (M % 2 == 0)>
  struct NatPower
  {
    static_assert((M >= 2) && IsEven, "Invalid degree in NatPower");
    DEVICE constexpr static T res(T base)
      { return Sqr(NatPower<T, M/2>::res(base)); }
  };

  // The Odd case:
  template<typename T, int M>
  struct NatPower<T, M, false>
  {
    static_assert(M >= 1, "Invalid degree in NatPower");
    DEVICE constexpr static T res(T base)
      { return base * Sqr(NatPower<T, M/2>::res(base)); }
  };

  // M==1: This case is not really necessary, but is provided for efficiency:
  template<typename T>
  struct NatPower<T, 1, false>
  {
    DEVICE constexpr static T res(T base)
      { return base; }
  };

  // M==0: Trivial case:
  template<typename T>
  struct NatPower<T, 0, true>
  {
    DEVICE constexpr static T res(T)
      { return T(1.0); }  // XXX: We do not check if base==0
  };

  //-------------------------------------------------------------------------//
  // Power Expr with a General Integral Degree:                              //
  //-------------------------------------------------------------------------//
  // Natural case:
  template<typename T, int M, bool IsNatural = (M >= 0)>
  struct IntPower
  {
    static_assert(IsNatural, "IntPower: Case match error");
    DEVICE constexpr static T res(T base)
      { return NatPower<T, M>::res(base); }
  };

  // The Negative Integral case:
  template<typename T, int M>
  struct IntPower<T, M, false>
  {
    DEVICE constexpr static T res(T base)
      { return T(1.0) / NatPower<T, -M>::res(base); }
  };

  //-------------------------------------------------------------------------//
  // "SqRtPower":                                                            //
  //-------------------------------------------------------------------------//
  // The degree is a (normalised) Rational; testing the denom "N" for being a
  // multiple of 2 -- if so, rewriting the power expr via "SqRt".
  //
  // Generic case: N != 1 and N is NOT a multiple of 2: use "pow":
  template<typename T, int M, int N, bool IsSqrt = (N % 2 == 0)>
  struct SqRtPower
  {
    static_assert(N >= 3 && !IsSqrt, "SqRtPower: Degree not normalised");
    DEVICE constexpr static T res(T base)
      { return pow(base, T(M) / T(N)); }
  };

  // The Integral degree case (N==1):
  template<typename T, int M>
  struct SqRtPower<T, M, 1, false>
  {
    DEVICE constexpr static T res(T base)
      { return IntPower<T, M>::res(base); }
  };

  // "SqRt" case: NB: "N" is indeed a multiple of 2:
  template<typename T, int M, int N>
  struct SqRtPower <T, M,  N, true>
  {
    static_assert(N >= 2 && N % 2 == 0, "SqRtPower: Case match error");
    DEVICE constexpr static T res(T base)
      { return SqRtPower<T, M, N/2>::res(SqRt(base)); }
  };

  //-------------------------------------------------------------------------//
  // "CbRtPower":                                                            //
  //-------------------------------------------------------------------------//
  // Similar to "SqRtPower"; testing "N" for being a multiple of 3,  and if so,
  // rewriting the power expr via "CbRt". However, "CbRt" is only available for
  // real types.
  //
  // Generic case: N is NOT a multiple of 3, or inappropritate type "T":   Fall
  // back to "SqRtPower":
  template<typename T, int M, int N, bool IsCbrt = (N % 3 == 0)>
  struct CbRtPower
  {
    DEVICE constexpr static T res(T base)
      { return SqRtPower<T, M, N>::res(base); }
  };

  // The following types allow us to use "CbRt":
  template<typename T, int M, int N>
  struct CbRtPower <T, M,  N, true>
  {
    static_assert(N >= 3 && N % 3 == 0, "CbRtPower: Case match error");
    DEVICE constexpr static T res(T base)
      { return CbRtPower<T, M, N/3>::res(CbRt(base)); }
  };

  //-------------------------------------------------------------------------//
  // Power Expr with a General Fractional Degree:                            //
  //-------------------------------------------------------------------------//
  // NB: This is a template function, not a struct -- it has no partial specs.
  // First attempt "CbRtPower", then it gets reduced further:
  template<int M, int N,  typename T>
  DEVICE inline constexpr T FracPower(T base)
  {
    static_assert(N != 0, "Zero denom in fractional degree");
    return CbRtPower<T, NormaliseNumer(M,N), NormaliseDenom(M,N)>::res(base);
  }

  //=========================================================================//
  // "StrToF":                                                               //
  //=========================================================================//
  // XXX: currently implemented via "strtod", so could lead to loss of accuracy
  // if "F" is larger than "double" (but this does not happen on modern archs):
  // NB: "to" is the pos of the last char to be scanned; if to < 0, the pos is
  // counted from th eend of the string (Python-like):
  //
# ifndef __CUDACC__
  template<typename F>
  F StrToF(std::string const& str, int from, int to)
  {
    int l = int(str.length());
    if (to < 0)
      to += l + 1;

    if (!(0 <= from && from < to && to <= l))
      throw std::invalid_argument("StrToF: Invalid substr");

    char const* start = str.data() + from;
    char const* end   = str.data() + to;
    char*       got   = nullptr;

    double val = strtod(start, &got);
    if (got != end)
      throw std::invalid_argument("StrToF: Invalid string format");

    return F(val);
  }
# endif

  //=========================================================================//
  // Misc:                                                                   //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // Power-of-2 Test:                                                        //
  //-------------------------------------------------------------------------//
  template<typename T>
  constexpr bool IsPow2(T a_n)
  {
    return
      (a_n <= 0)
      ? false
      : ((a_n & (~a_n + 1)) == a_n);
  }
} // End namespace Arbalete
