// vim:ts=2:et
//===========================================================================//
//                                 "BSM.hpp":                                //
//  Black-Scholes-Merton (BSM) Pxs, Deltas and Implied Vols for Calls & Puts //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#ifndef  __CUDACC__
#include "Common/DateTime.h"
#include <stdexcept>
#endif

namespace Arbalete
{
  //=========================================================================//
  // "BSMImplVol":                                                           //
  //=========================================================================//
  // XXX: Currently, this is for European options only (Americal options would
  // be different in case of Put):
  //
  template<typename F>
  F BSMImplVol
  (
    bool  is_call,
    F     C0,             // Option price at pricing time
    F     S0,             // Spot price at pricing time
    F     K,              // Strike
    F     tau,            // Time to expiration, as a Year Fraction
    F     r,              // Risk-free interest rate
    F     D     = F(0.0), // Dividend rate
    F*    delta = nullptr // If non-NULL, "delta" is returned here
  )
  {
    if (C0 <= F(0.0) || tau <= F(0.0) || S0 <= F(0.0) || K <= F(0.0))
#     ifndef __CUDACC__
      throw std::invalid_argument("BSMImplVol: Invalid arg(s)");
#     else
      return NaN<F>();
#     endif

    // Tolerance:
    F Tol   = F(500.0) * Eps<F>();

    // Initial guess: Should be fine in most normal situations:
    F sigma = F(0.3);

    // Moneyness:
    F sqrT   = SqRt(tau);
    F x      = Log(S0 / K) / sqrT + (r - D) * sqrT;
    F z      = F(0.5) * sqrT;

    // Exponential Coeffs:
    F D0     = Exp(- D * tau);
    F E0     = S0 * D0;
    F K0     = K  * Exp(- r * tau);
    F Vega0  = E0 * sqrT * InvSqRt2Pi<F>();

    // Check the limits. XXX: Lower limits need to be made more precise, eg for
    // at-the-money options:
    //
    F LoC    = F(0.0);
    F UpC    = F(0.0);
    if (is_call)
    {
      LoC = Max(E0 - K0, F(0.0));
      UpC = E0;
    }
    else
    {
      LoC = Max(K0 - E0, F(0.0));
      UpC = K0;
    }
    if (!(LoC < C0 && C0 < UpC))
#     ifndef __CUDACC__
      throw std::invalid_argument("BSMImplVol: Unattaianble Option Price");
#     else
      return NaN<F>();
#     endif

    int const MAX_ITERS = 1000000;
    int  i = 0;
    for (; i < MAX_ITERS; ++i)
    {
      // "C" is the option price at "sigma":
      F xs  = x / sigma;
      F zs  = z * sigma;

      F d1  = xs + zs;
      F d2  = xs - zs;
      if (!is_call)
      {
        d1 = - d1;
        d2 = - d2;
      }

      F N1 = NCDF(d1);
      F N2 = NCDF(d2);
      F C = E0 * N1 - K0 * N2;
      if (!is_call)
        C = - C;

      // Discrepancy:
      F dC = C - C0;

      // Vega (same for call and Put):
      F Vega  = Vega0 * Exp(- F(0.5) * d1 * d1);
      assert(Vega > F(0.0));

      // Next approximation or exit?
      F dSigma = dC / Vega;
      if (Abs(dSigma) < Tol || Abs(dC) < Tol)
      {
        // Exit condition is satisfied:
        // Save "delta" if requested:
        if (delta != nullptr)
          *delta = D0 * (is_call ? N1 : (N1 - F(1.0)));
        break;
      }

      // If next approximation: apply some limits on "sigma":
      sigma -= dSigma;
      if (sigma <= F(0.0))
        sigma = F(1e-4);
      else
      if (sigma >= F(2.0))
        sigma = F(2.0);
    }
    if (i == MAX_ITERS)
    {
      if (delta != nullptr)
        *delta = NaN<F>();

#     ifndef __CUDACC__
      throw std::runtime_error("BSMImplVol: Divergence");
#     else
      return NaN<F>();
#     endif
    }
    return sigma;
  }

  //=========================================================================//
  // "BSMCallPx":                                                            //
  //=========================================================================//
  template<typename F>
  F BSMCallPx(F S0, F K, F sigma, F tau, F r, F D = F(0.0))
  {
    assert(tau >= F(0.0) && S0 > F(0.0) && K > F(0.0) && sigma > F(0.0));
    if (tau > F(0.0))
    {
      // Before expiration:
      double x1 = sigma  * SqRt(tau);
      double x2 = (Log(S0 / K) + (r - D) * tau) / x1;
      double d1 = x2 + F(0.5) * x1;
      double d2 = d1 - x1;
      double C  = S0 * Exp(-D * tau) * NCDF(d1) -
                  K  * Exp(-r * tau) * NCDF(d2);
      assert(F(0.0) < C && C < S0);
      return C;
    }
    else
      // At expiration: Return the Call pay-off:
      return Max(S0 - K, F(0.0));
  }

  //=========================================================================//
  // "BSMCallDelta":                                                         //
  //=========================================================================//
  template<typename F>
  F BSMCallDelta(F S0, F K, F sigma, F tau, F r, F D = F(0.0))
  {
    assert(tau >= F(0.0) && S0 > F(0.0) && K > F(0.0) && sigma > F(0.0));
    if (tau > F(0.0))
    {
      // Before expiration:
      double x1     = sigma  * SqRt(tau);
      double x2     = (Log(S0 / K)  + (r - D) * tau) / x1;
      double d1     = x2 + F(0.5)   * x1;
      double delta  = Exp(-D * tau) * NCDF(d1);
      // XXX: Although Delta is in (0,1), it may be equal to 0 or 1 due to
      // rounding:
      assert(F(0.0) <= delta && delta <= F(1.0));
      return delta;
    }
    else
      // At expiration: XXX: For At-the-Money expiration, Delta is discontinu-
      // ous, so return the mid-point (0.5):
      return
        (S0 < K) ? F(0.0) : (S0 > K) ? F(1.0) : F(0.5);
  }

  //=========================================================================//
  // "BSMPutPx":                                                             //
  //=========================================================================//
  template<typename F>
  F BSMPutPx(F S0, F K, F sigma, F tau, F r, F D = F(0.0))
  {
    assert(tau >= F(0.0) && S0 > F(0.0) && K > F(0.0) && sigma > F(0.0));
    if (tau > F(0.0))
    {
      // Before expiration:
      double x1 = sigma  * SqRt(tau);
      double x2 = (Log(S0 / K) + (r - D) * tau) / x1;
      double d1 = x2 + F(0.5) * x1;
      double d2 = d1 - x1;
      double P  = K  * Exp(-r * tau) * NCDF(-d2) -
                  S0 * Exp(-D * tau) * NCDF(-d1);
      assert(F(0.0) < P && P < K);
      return P;
    }
    else
      // At expiration: Return the Put pay-off:
      return Max(K - S0, F(0.0));
  }

  //=========================================================================//
  // "BSMPutDelta":                                                          //
  //=========================================================================//
  template<typename F>
  F BSMPutDelta(F S0, F K, F sigma, F tau, F r, F D = F(0.0))
  {
    assert(tau >= F(0.0) && S0 > F(0.0) && K > F(0.0) && sigma > F(0.0));
    if (tau > F(0.0))
    {
      // Before expiration:
      double x1     = sigma  * SqRt(tau);
      double x2     = (Log(S0 / K)  + (r - D) * tau) / x1;
      double d1     = x2 + F(0.5)   * x1;
      double delta  = Exp(-D * tau) * (NCDF(d1) - F(1.0));
      assert(F(-1.0) < delta && delta < F(0.0));
      return delta;
    }
    else
      // At expiration:
      return F(-1.0);
  }
}
