// vim:ts=2:et
//===========================================================================//
//                             "VecMatTypes.h":                              //
//                Boost uBLAS-Based Vector and Matrix Types                  //
//===========================================================================//
#pragma once

#ifdef   __CUDACC__
#error   "VecMatTypes.h cannot be used in CUDA compilations"
#endif

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-function"
#endif

#include "Common/Macros.h"
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/triangular.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <stdexcept>
#include <cassert>

#ifdef __clang__
#pragma GCC diagnostic pop
#endif

namespace Arbalete
{
  // Use the folowing short-cut if access to other uBLAS types or functions is
  // required:
  namespace uBLAS = boost::numeric::ublas;

  //=========================================================================//
  // "*Vector", "*Matrix":                                                   //
  //=========================================================================//
  template<typename F>
  using Vector     = uBLAS::vector<F>;

  template<typename F>
  using ZeroVector = uBLAS::zero_vector<F>;

  template<typename F>
  using Matrix     =
        uBLAS::matrix<F, boost::numeric::ublas::column_major>;

  template<typename F>
  using ZeroMatrix =
        uBLAS::zero_matrix<F, boost::numeric::ublas::column_major>;

  template<typename F>
  using IdentityMatrix =
        uBLAS::identity_matrix<F, boost::numeric::ublas::column_major>;

  template<typename F>
  inline void ZeroOut(Vector<F>* vec)
    { noalias(*vec) = ZeroVector<F>(vec->size()); }

  template<typename F>
  inline void ZeroOut(Matrix<F>* mat)
    { noalias(*mat) = ZeroMatrix<F>(mat->size1(), mat->size2()); }

  //=========================================================================//
  // XXX: Searching for a better place, put "MatrixInverse" here:            //
  //=========================================================================//
  template<typename F>
  void MatrixInverse(Matrix<F> const& src, Matrix<F>* inverse)
  {
    assert
      (src.size1() == src.size2() && src.size1() >= 1 && inverse != nullptr);

    // Make copy of the "src":
    Matrix<F> A(src);

    // Permutation matrix for the LU-Factorisation:
    uBLAS::permutation_matrix<int> P(A.size1());

    // Perform LU-Factorisation:
    if (uBLAS::lu_factorize(A, P) != 0)
      throw std::runtime_error("MatrixInverse: LU-Factorization failed");

    // Inverse is initialised to the Identity Matrix:
    inverse->assign(IdentityMatrix<F>(A.size1()));

    // Back-substitute to get the inverse:
    uBLAS::lu_substitute(A, P, *inverse);
  }
}
