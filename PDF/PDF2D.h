// vim:ts=2:et
//===========================================================================//
//                                "PDF2D.h":                                 //
//            Functional Object for Probability Density Functions            //
//                        for 2D StocVol Diffusions                          //
//===========================================================================//
#pragma once

#include "Common/Macros.h"
#ifndef  __CUDACC__
#include "Diffusions/Diffusion2D.h"
#include "Grids/Grid2D.h"
#include "PDF/ReturnsPDFKern.h"
#include <string>
#include <memory>
#include <functional>
#endif   // __CUDACC__

namespace Arbalete
{

  //=========================================================================//
  // "PDF2D":                                                                //
  //=========================================================================//
  template<typename F>
  class PDF2D
  {
  public:
    //-----------------------------------------------------------------------//
    // MLE-Related Return Types:                                             //
    //-----------------------------------------------------------------------//
    // "MLEResUC":
    // "Global" (UnConditioned) Min, Max and Normalised Sum of the PDF:
    //
    struct MLEResUC
    {
      F   m_minP;
      F   m_maxP;
      F   m_sumP;

      // Default Ctor:
      MLEResUC()
      :   m_minP(F(0.0)),
          m_maxP(F(0.0)),
          m_sumP(F(0.0))
      {}
    };

    // "MLEResY":
    // For PDF conditioned on S=Y (i.e., a cross-section of the global PDF):
    // "MinY" and "MaxY"  are taken  directly from the cross-section BEFORE
    // re-normalisation (so that LLH Function can be applied on "MaxY") but
    // "SumY" is already normalised; "optJ" is the "Vx" index of the ArgMax
    // of conditioned PDF wrt "V":
    //
    struct MLEResY
    {
      F   m_minPY;
      F   m_maxPY;
      int m_optJ;
      F   m_sumPY;

      // Default Ctor:
      MLEResY()
      :   m_minPY(F(0.0)),
          m_maxPY(F(0.0)),
          m_optJ (0),
          m_sumPY(F(0.0))
      {}
    };

    // "MLERes":
    // Integrated result returned to the external caller:
    //
    struct  MLERes
    {
      MLEResUC  m_UC;
      MLEResY   m_Y;
    };

# ifndef __CUDACC__
  private:
    //-----------------------------------------------------------------------//
    // The State:                                                            //
    //-----------------------------------------------------------------------//
    std::shared_ptr<Grid2D<F>>    m_grid;       // The Computational Grid
    bool                          m_isFwd;      // Perform Fwd integration
    mutable DateTime              m_currT;      // The current time

    mutable Matrix<F>             m_pSV;        // Soltn space in Host mem
    mutable ReturnsPDFKern<F>     m_retKern;    // Kernel for Returns PDF

    // For "ConditionOnS":
    mutable MLEResUC*             m_cudaMLEUCs; // CUDA UC output (blk)
    mutable MLEResY*              m_cudaMLEY;   // CUDA Y  output
    mutable std::vector<MLEResUC> m_MLEUCs;     // UC output in Host mem
    CUDAEnv::Sched2D*             m_cudaSchCS;  // Schedule of CUDA thrds
                                                //    for "CondS"
    void ResetGrid() const;                     // Grid (re-)init proc

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor, Dtor and Access:                                    //
    //-----------------------------------------------------------------------//
    // NB: "is_fwd" is "true" for the Fokker-Planck solver,
    //                "false" for the Feynman-Kac   solver:
    PDF2D
    (
      Diffusion_Params<F>        const& diff_params,
      typename Grid2D<F>::Params const& grid_params,
      bool                              isFwd
    );

    ~PDF2D();

    Grid2D<F> const&    GetGrid()   const { return *m_grid;   }
    DateTime            StartTime() const { return m_grid->StartTime(); }
    DateTime            EndTime()   const { return m_grid->EndTime();   }
    DateTime            CurrTime()  const { return m_currT;   }
    bool                IsFwd()     const { return m_isFwd;   }
    Matrix<F>&          GetPSV()    const { return m_pSV;     }

    ReturnsPDFKern<F>&  GetRetPDFKern() const
      { return m_retKern; }

    //-----------------------------------------------------------------------//
    // Integration to a given "t" (Fwd or Bwd) on the Grid:                  //
    //-----------------------------------------------------------------------//
    // The user call-back type. NB: "pdf" is "*this", other args and the return
    // val are as in "Grid2D<F>::CallBack":
    typedef
      std::function
        <Time(PDF2D<F> const& pdf, DateTime t,  int stk,  F* f,
              DateTime next_t,     TradingCalendar::CalReg reg)>
      CallBack;

    // The actual integrator. NB: The initial cond is set up automatically; the
    // integration direction (Fwd or Bwd) was set by the Ctor;  "t_to" must al-
    // ways be within the time range of the constructed Grid.    Integration is
    // performed from "GetCurrTime()" to "t_to", at the end CurrTime is updated
    // by "t_to". For Fwd induction, the initial CurrTime is "StartTime", for
    // Bwd induction, it is "EndTime".
    // The Bound Conds are normally Neumann-0 for Fwd run and Dirichlet-0 for
    // Bwd run, unless the "force_Dirichlet" flag is set, in which case  they
    // are set to Dirichlet in both cases:
    //
    void IntegrateTo
    (
      DateTime                     t_to,
      std::vector<DateTime> const* special_times   = nullptr,
      CallBack const*              call_back       = nullptr,
      bool                         force_Dirichlet = false
    )
    const;

    //-----------------------------------------------------------------------//
    // "ConditionOnS":                                                       //
    //-----------------------------------------------------------------------//
    // Conditioning the curr result "f" on S=Y. After conditioning, "f" remains
    // a valid 2D PDF, i.e. it is multiplied by discretised delta(S-Y).   This
    // func is typically used in a call-back, so "f" points either to Host  or
    // to CUDA memory;
    // "nextDt" is optional; when specified, it can be used for fine "Sx" axis
    // adjustment in the Grid:
    //
    MLERes ConditionOnS
      (DateTime t, F* f, F SY, Time next_dt = Time()) const;

    //-----------------------------------------------------------------------//
    // "PDFR": PDF of Returns:                                               //
    //-----------------------------------------------------------------------//
    // Helper functions which may be invoked, e.g., from the Call-Back:
    //
    // (1) For a single "x", specified explicitly:
    //
    F PDFR
    (
      DateTime t, Time dt, TradingCalendar::CalReg reg, F const* p, F x
    )
    const;

    // (2) For multiple "x"s which need to be set on the kernel beforehand via
    //     "SetX" / "SetXs":
    //     pre-condition: res->size() == max(pdf2d.GetKRet().GetNParams(), 1):
    //
    void PDFR
    (
      DateTime t, Time dt, TradingCalendar::CalReg reg, F const* p,
      Vector<F>*  res
    )
    const;

    void SetX (F X) const     { m_retKern.SetX(X); }

    void SetXs(F xFrom, F xTo, int nXs) const
      { m_retKern.SetXs(*m_grid, xFrom, xTo, nXs); }

    //-----------------------------------------------------------------------//
    // Grid Test:                                                            //
    //-----------------------------------------------------------------------//
    // NB: The grid is re-set at the end of the test, so the test results are
    // only available via the "call_back":
    //
    void GridTest
      (bool with_discount, CallBack const* call_back = nullptr) const;

  private:
    //-----------------------------------------------------------------------//
    // "MkGridCallBack":                                                     //
    //-----------------------------------------------------------------------//
    // Constructs the internal Grid call-back which performs some house-keeping
    // and then invokes the user-specified call-back:
    //
    typename Grid2D<F>::CallBack MkGridCallBack
      (DateTime t_to,   CallBack const* call_back) const;

# endif // !__CUDACC__
  };
}
