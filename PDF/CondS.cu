// vim:ts=2:et
//===========================================================================//
//                                "CondS.cu":                                //
//            CUDA Kernels for Computation of "S"-Conditional PDFs           //
//===========================================================================//
#include "PDF/CondS_Core.hpp"

namespace Arbalete
{
  template struct ConditionOnS_KernelInvocator<double>;
  template struct ConditionOnS_KernelInvocator<float>;
}
