// vim:ts=2:et
//===========================================================================//
//                                "CondLLH.hpp":                             //
//     Base Class for Conditional Log-LikeliHood Functions for Diffusions    //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "PDF/CondLLH.h"
#include <iostream>
#include <stdexcept>

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // CondLLH: Non-Default Ctor:                                              //
  //=========================================================================//
  template<typename F>
  CondLLH<F>::CondLLH
  (
    Diffusion_Params<F> const&  diff_params,
    vector<MDItem<F>>   const&  mkt_data,
    int                         debug_level
  )
  : m_diffParams(diff_params),
    m_debugLevel(debug_level)
  {
    int nk = int(mkt_data.size());
    if (nk <= 1)
      throw invalid_argument
            ("CondLLH Ctor: Min size(MktData) is 2, got "+ to_string(nk));

    // Unzip "mkt_data", and check the time stamps. XXX: This is somewhat
    // inefficinet...
    //
    MDItem<F> const* mds = mkt_data.data();

    m_mdTimes.resize(nk);
    m_mdTYs  .resize(nk);
    m_mdS    .resize(nk);
    m_mdRegs .resize(nk);

    DateTime t0 = mds[0].m_t;
    F        S0 = mds[0].m_val;

    for (int i = 0; i < nk; ++i)
    {
      MDItem<F> const& mdi = mds[i];
      m_mdTimes[i] = mdi.m_t;
      m_mdTYs  [i] = YF<F>(t0, mdi.m_t);
      m_mdS    [i] = mdi.m_val;
      m_mdRegs [i] = mdi.m_reg;

      if (i >= 1 && m_mdTimes[i] <= m_mdTimes[i-1])
        throw invalid_argument
              ("CondLLH Ctor: Non-ascending time stamps in MktData: ["     +
               to_string(i-1)       + "]=" + DateTimeToStr(m_mdTimes[i-1]) +
               ", [" + to_string(i) + "]=" + DateTimeToStr(m_mdTimes[i]));
    }
    // Install "t0" and "S0" from Mkt Data into "diffParams":
    m_diffParams.m_t0 = t0;
    m_diffParams.m_S0 = S0;
  }

  //=========================================================================//
  // Params Construction: Helper Function(s):                                //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // "MkDiffParamsStub":                                                     //
  //-------------------------------------------------------------------------//
  template<typename F>
  Diffusion_Params<F> CondLLH<F>::MkDiffParamsStub
  (
    string const& diff_type,
    bool          with_reg
  )
  {
    Diffusion_Params<F> res;

    //  "m_t0" is unset, initialised to an invalid special value
    res.m_type    = diff_type;
    res.m_S0      = F(0.0);           // Also invalid, in general
    res.m_withReg = with_reg;
    //  "m_coeffs" is initially empty (invalid for DiffusionParams)

    return res;
  }
}
