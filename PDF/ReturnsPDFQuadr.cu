// vim:ts=2:et
//===========================================================================//
//                           "ReturnsPDFQuadr.cu":                           //
//          Pre-Defined Instances of "Quadrature2D_KernelInvocator"          //
//                           with "ReturnsPDFKern"                           //
//===========================================================================//
#include "Grids/Quadrature2D_Core.hpp"
#include "PDF/ReturnsPDFKern.hpp"

namespace Arbalete
{
  template struct Quadrature2D_KernelInvocator
    <double, ReturnsPDFKern<double> >;

  template struct Quadrature2D_KernelInvocator
    <float,  ReturnsPDFKern<float> >;
}
