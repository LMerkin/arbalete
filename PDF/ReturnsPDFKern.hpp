// vim:ts=2:et
//===========================================================================//
//                           "ReturnsPDFKern.hpp":                           //
//   Integration Kernel for Returns PDF Computed from PDF(S,V) and N(0,dt)   //
//===========================================================================//
#pragma once

#ifndef  __CUDACC__
#include "Common/CUDAEnv.hpp"
#include "Common/TradingCalendar.h"
#endif
#include "Common/Maths.hpp"
#include "PDF/ReturnsPDFKern.h"
#include <cassert>
#include <stdexcept>

namespace Arbalete
{
  using namespace std;

# ifndef __CUDACC__
  //-------------------------------------------------------------------------//
  // Non-Default Ctor:                                                       //
  //-------------------------------------------------------------------------//
  template<typename F>
  ReturnsPDFKern<F>::ReturnsPDFKern(Grid2D<F> const& grid)
  {
    // Data layout:
    // 0               :  dty
    // 1       ..   m  :  A = S       / sigmaS1(S,t), len = m
    // m+1     .. 2*m  :  B = mu(S,t) / sigmaS1(S,t), len = m
    // 2*m+1   .. 2*m+n:  C =           sigmaS2(V,t), len = n
    // Total:     2*m+n+1
    //
    int m      = int(grid.GetSx().size());
    int n      = int(grid.GetVx().size());
    m_dataLen  = 2 * m + n + 1;

    m_data     = new F[m_dataLen];
    m_cudaData =
      ( grid.UseCUDA() )
      ? grid.GetCUDAEnv().template CUDAAlloc<F>(m_dataLen)
      : nullptr;

    m_DToff    =  0;
    m_Aoff     =  m_DToff + 1;
    m_Boff     =  m_Aoff  + m;
    m_Coff     =  m_Boff  + m;
#   ifndef NDEBUG
    assert(m_dataLen == m_Coff + n);
#   endif

    // Scratch vectors for Diffusion Coeffs -- Host only:
    m_muS      = new F[m];
    m_sigS1    = new F[m];
    m_muV      = new F[n];
    m_sigV     = new F[n];

    // Params: initially there is one param, set to 0:
    m_oneX     = true;
    m_X        = F(0.0);
    m_Xs       = nullptr;
    m_cudaXs   = nullptr;
    m_nXs      = 0;

    // Initialise the coeffs. NB: "dt" is set to 0 -- it is OK for now,  but
    // using this kernel in integration will result in an error;  for "reg",
    // assume Fwd run:
    //
    DateTime t0                   = grid.StartTime();
    TradingCalendar const* cal    = grid.GetCal();
    TradingCalendar::CalReg regE  = GetRegE<true>(t0, cal);

    SetTimeInterval(grid, t0, ZeroTime(), regE);
  }

  //-------------------------------------------------------------------------//
  // Dtor:                                                                   //
  //-------------------------------------------------------------------------//
  template<typename F>
  void ReturnsPDFKern<F>::Destroy(Grid2D<F> const& grid)
  {
    delete[] m_data;
    delete[] m_muS;
    delete[] m_sigS1;
    delete[] m_muV;
    delete[] m_sigV;
    if (m_Xs != nullptr)
      delete[] m_Xs;

    if (grid.UseCUDA())
    {
      assert(m_cudaData != nullptr);
      CUDAEnv const& cudaEnv = grid.GetCUDAEnv();

      cudaEnv.Stop();  // Just in case...
      cudaEnv.template CUDAFree<F>(m_cudaData);
      if (m_cudaXs != nullptr)
        cudaEnv.template CUDAFree<F>(m_cudaXs);
    }
    m_data = m_cudaData = m_muS = m_sigS1 = m_muV = m_sigV = m_Xs = m_cudaXs =
    nullptr;
    m_nXs  = 0;
  }

  //-------------------------------------------------------------------------//
  // "SetTimeInterval":                                                      //
  //-------------------------------------------------------------------------//
  // NB: A non-const method!
  //
  template<typename F>
  void ReturnsPDFKern<F>::SetTimeInterval
    (Grid2D<F> const& grid,  DateTime t,  Time dt,
     TradingCalendar::CalReg DEBUG_ONLY(reg))
  {
    // Set the "dty". NB: we currently require "dt" to be non-negative:
    F dty           = YF<F>(dt);
    if (dty < F(0.0))
      throw invalid_argument("ReturnsPDFKern::SetTimeInterval: Need dt >= 0");
    m_data[m_DToff] = dty;

    // Get the Diffusion:
    //
    Diffusion2D<F>  const& diff = grid.GetDiffusion();

    // Extract the TradingCalendar from the Diffusion (if available) and get
    // the Diffusion Regime. XXX: Always assume Fwd run:
    //
    TradingCalendar const* cal  = grid.GetCal();
    DateTime  nextSw;
    TradingCalendar::CalReg regE;
    GetNextFwdEvent(t, cal, &nextSw, &regE);

    assert(nextSw > t && regE == reg);
    if (nextSw < t + dt)
      throw invalid_argument("ReturnsPDFKern::SetTimeInterval: "
                             "Got regime switch in (t,t+dt)");
    // Compute the SDE Coeffs:
    // NB: this kernel is for a PDF of non-discounted returns, so no need to
    // compute the interest rates:
    //
    Vector<F> const& Sx = grid.GetSx();
    Vector<F> const& Vx = grid.GetVx();

    F* sigS2 = m_data + m_Coff;
    F  rho   = F(0.0);

    diff.GetSDECoeffs
      (Sx,    Vx,      YF<F>(grid.StartTime(), t), regE,
       m_muS, m_sigS1, sigS2, m_muV, m_sigV, &rho, nullptr);

    // Compute "A" and "B" coeffs ("C" is "sigS2" -- already in):
    int m = int(Sx.size());
#   ifndef NDEBUG
    int n = int(Vx.size());
    assert(m >= 2 && n >= 2);
#   endif

    F*  A = m_data + m_Aoff;
    F*  B = m_data + m_Boff;

    A[0] = B[0] = F(0.0);
    for (int i = 1; i < m; ++i)
    {
      // Volatility must not vanish except for bound points (which will be
      // excluded from integration anyway):
      assert(m_sigS1[i] > F(0.0));

      A[i] = Sx[i]    / m_sigS1[i];
      B[i] = m_muS[i] / m_sigS1[i] * dty;
    }

    // If CUDA is used, flush the data to CUDA memory:
    if (grid.UseCUDA())
    {
      assert(m_cudaData != nullptr);
      grid.GetCUDAEnv().template ToCUDA<F>(m_data, m_cudaData, m_dataLen);
    }
  }

  //-------------------------------------------------------------------------//
  // "SetX":                                                                 //
  //-------------------------------------------------------------------------//
  // NB: A non-const method!
  //
  template<typename F>
  void ReturnsPDFKern<F>::SetX(F X)
  {
    m_oneX = true;
    m_X    = X;
    // "m_Xs" and "m_nXs" are unchanged -- they are just not used currently
  }

  //-------------------------------------------------------------------------//
  // "SetXs":                                                                //
  //-------------------------------------------------------------------------//
  // NB: A non-const method!
  //
  template<typename F>
  void ReturnsPDFKern<F>::SetXs
    (Grid2D<F> const& grid, F xFrom, F xTo, int nXs)
  {
    if (xFrom > xTo || nXs <= 0 || (nXs == 1 && xFrom != xTo))
      throw invalid_argument("ReturnsPDFKern::SetXs: Invalid Xs range");

    if (nXs == 1)
    {
      SetX(xFrom);
      return;
    }

    // Otherwise: GENERIC CASE:
    assert(nXs >= 2);
    m_oneX = false;

    bool           useCUDA = grid.UseCUDA();
    CUDAEnv const* cudaEnv = useCUDA ? &(grid.GetCUDAEnv()) : nullptr;

    // Do we need to re-allocate the memory?
    //
    if (nXs != m_nXs && m_Xs != nullptr)
    {
      // Yes, re-allocation is required: de-allocate "m_Xs" first:
      delete[] m_Xs;
      if (useCUDA)
      {
        assert(m_cudaXs != nullptr);
        cudaEnv->CUDAFree<F>(m_cudaXs);
      }
      m_Xs     = nullptr;
      m_cudaXs = nullptr;
      m_nXs    = 0;
    }

    // New allocation:
    if (m_Xs == nullptr)
    {
      assert(m_cudaXs == nullptr && m_nXs == 0);
      m_Xs     = new F[nXs];
      if (useCUDA)
        m_cudaXs = cudaEnv->CUDAAlloc<F>(nXs);
      m_nXs    = nXs;
    }

    // "Xs" are arguments of the Returns PDF to be constructed by convolution
    // of a 2D PDF(S,V) with this Kernel. XXX: should we use non-uniform "Xs"
    // intervals? -- Currently not used.
    //
    // "X" step: does not matter if there is only 1 "X" value:
    F hX = (nXs >= 2) ? (xTo - xFrom) / F(nXs-1) : F(0.0);

    for (int r = 1; r < nXs-1; ++r)
      m_Xs[r] = xFrom + F(r) * hX;
    m_Xs[0]     = xFrom;
    m_Xs[nXs-1] = xTo;

    // In the CUDA mode, copy "m_Xs" into CUDA memory:
    if (useCUDA)
      cudaEnv->ToCUDA<F>(m_Xs, m_cudaXs, nXs);
  }
# endif // !__CUDACC__

  //-------------------------------------------------------------------------//
  // Evaluation:                                                             //
  //-------------------------------------------------------------------------//
  // Computes the Integration Kernel to get the Returns PDF at "x":
  //
  template<typename F>
  DEVICE F ReturnsPDFKern<F>::operator()(int i, int j, F x) const
  {
    // XXX: The validity of "i" and "j" is NOT verified -- no "grid" here:
#   ifndef __CUDACC__
    F* data = m_data;
#   else
    F* data = m_cudaData;
#   endif
    F  dty  = data[m_DToff];
    F* A    = data + m_Aoff;
    F* B    = data + m_Boff;
    F* C    = data + m_Coff;

    assert(dty > F(0.0) && C[j] > F(0.0));
    return A[i] / C[j] * Exp(- F(0.5) / dty * Sqr<F>((A[i] * x - B[i]) / C[j]));
  }
}
