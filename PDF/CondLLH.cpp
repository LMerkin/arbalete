// vim:ts=2:et
//===========================================================================//
//                               "CondLLH.cpp":                              //
//                    Pre-Defined Instances of "CondLLH"                     //
//===========================================================================//
#include "PDF/CondLLH.hpp"

namespace Arbalete
{
  template class  CondLLH<double>;
  template class  CondLLH<float>;
}
