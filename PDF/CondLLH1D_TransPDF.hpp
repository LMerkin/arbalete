// vim:ts=2:et
//===========================================================================//
//                          "CondLLH2D_TransPDF.hpp":                        //
// Conditional Log-LikeliHood Function Using Transition PDF on 1D Diffusions //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Common/DateTime.h"
#include "Diffusions/Diffusion1D_Fact.h"
#include "PDF/CondLLH1D_TransPDF.h"
#include <cstdio>
#include <iostream>
#include <stdexcept>
#include <cassert>

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // Evaluation:                                                             //
  //=========================================================================//
  template<typename F>
  void CondLLH1D_TransPDF<F>::NLLH
  (
    Vector<F> const& diff_coeffs,
    F*               val,
    Vector<F>*       gradient,
    Matrix<F>*       hessian,
    F*               vs
  )
  const
  {
    // Check the args  :
    assert(val   != nullptr);
    if (gradient != nullptr || hessian != nullptr || vs != nullptr)
      throw invalid_argument
            ("CondLLH1D_TransPDF::NLLH: Gradient, Hessian and Vs are not "
             "supported");

    int nk = int(this->m_mdTimes.size());
    if (nk <= 1)
      throw runtime_error
            ("CondLLH1D_TransPDF::NLLH: Expected MktData size >= 2");

    // Install "diff_coeffs" in the "DiffParams":
    this->m_diffParams.m_coeffs = diff_coeffs;

    // Construct the Diffusion:
    shared_ptr<Diffusion1D<F>> diff(MkDiffusion1D<F>(this->m_diffParams));

    // Construct the TimeLine (use 0 steps -- only conditioning points are to
    // be used; regime switching points must be conditioning pts as well):
    //
    DateTime t0 = this->m_mdTimes[0];        // Diffusion start
    DateTime te = this->m_mdTimes.back();    // End of Mkt Data
    assert  (t0 == diff->GetT0());

    vector<TimeNode<F>> timeLine;
    Time dts[3]  = { ZeroTime(), ZeroTime(), ZeroTime() };

    MkTimeLine<true, F> // IsFwd=true
      (t0, dts, te, m_cal.get(), &(this->m_mdTimes), &timeLine);

    if (int(timeLine.size()) != nk || timeLine[0].m_secs != 0)
      throw runtime_error
            ("CondLLH1D_TransPDF::NLLH: Invalid TimeLine constructed?");

    // Check that the TimeLine is stepping right through all MkdData pts:
    for (int i = 0; i < nk;  ++i)
      if (timeLine[i].m_stk != i)
        // XXX: Is it safe to raise an exception in OpenMP?
        throw runtime_error
              ("CondLLH1D_TransPDF::(): Must be a cond point: "+ to_string(i));

    // ITERATE OVER THE TIMELINE: XXX: No CUDA support yet, but have OpenMP:
    //
    // LLH is a sum of logs of conditional PDFs over all conditioning points,
    // so initialise it to 0:
    F res  = F(0.0);

    // The initial point (t0) is skipped, CondLLH evaluation starts at i=1:

#   ifndef __clang__
#   pragma omp parallel for reduction (+:res)
#   endif
    for (int i = 1; i < nk; ++i)
    {
      // IMPORTANT: Each point on the "timeLine" must be a conditioning point,
      // in particular we must have market data for all regime switching pts:
      //
      DateTime t = t0 + TimeGener::seconds(timeLine[i].m_secs);
      assert(this->m_mdTimes[i] == t);

      // Get the Transition PDF from the last step (ending at "t"):
      F S1  = this->m_mdS[i-1];
      F ty1 = timeLine[i-1].m_ty;

      F S2  = this->m_mdS[i];
      F ty2 = timeLine[i].m_ty;
      F dty = ty2 - ty1;

      F p   = diff->TransPDF(S1, ty1, S2, dty, timeLine[i-1].m_reg);

      // Negated LLH increment:
      F NL   =- Log(p);
      bool ok = IsFinite(NL);

      if (ok)
        res += NL;

      // Debugging Summary at Cond Point:
      if (this->m_debugLevel >= 2 || (!ok && p != F(0.0)))
#       pragma omp critical
        cout << '\n' << t << ": S=" << S2 << ", NLLH=" << NL << endl;

      // XXX: Cannot terminate the loop if (!ok): OpenMP prohibits this...
    }
    // Move the result to *val:
    *val = res;
  }
}
