// vim:ts=2:et
//===========================================================================//
//                                "CondLLH.h":                               //
//  Base Class for Conditional Log-LikeliHood Functions for All Diffusions   //
//===========================================================================//
#pragma once

#include "Common/DateTime.h"
#include "Diffusions/Diffusion_Params.h"
#include "Calibration/MarketData.h"
#include <vector>
#include <memory>
#include <cassert>

namespace Arbalete
{
  //=========================================================================//
  // "CondLLH" class:                                                        //
  //=========================================================================//
  template<typename F>
  class CondLLH
  {
  protected:
    //-----------------------------------------------------------------------//
    // Common Data Flds:                                                     //
    //-----------------------------------------------------------------------//
    // "DiffParams" are mutable, as the Diffusion Coeffs are set just before
    // LLH evaluation:
    mutable Diffusion_Params<F>           m_diffParams;

    // The Mkt Data is to be unzipped so that it can (TODO) be used in the CUDA
    // environment:
    std::vector<DateTime>                 m_mdTimes;    // MktData TimeStamps
    std::vector<F>                        m_mdTYs;      // Corresp Year Fracs
    std::vector<F>                        m_mdS;        // Price Values
    std::vector<TradingCalendar::CalReg>  m_mdRegs;     // Regimes (Fwd)
    int                                   m_debugLevel;

  private:
    // Default ctor is hidden:
    CondLLH();

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor, Dtor:                                               //
    //-----------------------------------------------------------------------//
    // Ctor Args:
    // diff_params:  diffusion params; "m_t0", "m_S0", "m_coeffs" may be unset;
    //               "t0" and "S0" are set from "mkt_data",   "coeffs" are set
    //               at evaluation time. Use "MkDiffParamsStub" to create such
    //               a "stub" params;
    // mkt_data:     must contain (t0, S0) as the 0th entry, and at least 1
    //               more entry for conditioning:
    CondLLH
    (
      Diffusion_Params<F>    const&  diff_params,  // Stub
      std::vector<MDItem<F>> const&  mkt_data,
      int                            debug_level
    );

    // Virtual Dtor:
    virtual ~CondLLH() {}

    //-----------------------------------------------------------------------//
    // Accessors:                                                            //
    //-----------------------------------------------------------------------//
    // Mkt Data Length:
    int GetMDLen() const
    {
      int nk = int(m_mdTimes.size());
      assert(nk >= 2 && nk == int(m_mdTYs.size()) && nk == int(m_mdS.size()) &&
             nk == int(m_mdRegs.size()));
      return nk;
    }

    // Mkt Data Times:
    std::vector<DateTime> const& GetMktDataTimes() const { return m_mdTimes; }

    // Mkt Data Year Fracs:
    std::vector<F> const&        GetMktDataYFs  () const { return m_mdTYs;   }

    // Mkt Data Price Vals:
    std::vector<F> const&        GetMktDataVals () const { return m_mdS;     }

    // Mkt Data Fwd Regimes:
    std::vector<TradingCalendar::CalReg> const& GetMktDataRegs() const
      { return m_mdRegs; }

    // NB: "GetDiffParams" returns the current (mutable) DiffParams:
    Diffusion_Params<F> const&   GetDiffParams() const { return m_diffParams; }

    // Debug Level:
    int GetDebugLevel() const    { return m_debugLevel; }

    // CUDA Timing:
    virtual F   GetCUDATiming() const = 0;

    //-----------------------------------------------------------------------//
    // Evaluation:                                                           //
    //-----------------------------------------------------------------------//
    // Abstract evaluation method. Supposed to compute NEGATED LOG-LIKELIHOOD
    // for given "coeffs", and possibly its Gradient and Hessian:
    //
    // llh = Sum_{i=1}^{NCondPts} ln (max_V (P(Y_i, V | Y_j, j=0..(i-1)))
    //    where S = Y_j are observed values of "S" used for conditioning;
    // "vs", if non-NULL, are estimated (filtered) V_j vals (j = 0..NCondPts);
    //    XXX: This is only applicable to some Diffusions (in particulr, 2+D),
    //    hence this parameter is optional:
    //
    virtual void NLLH
    (
      Vector<F> const& diff_coeffs,
      F*               val,
      Vector<F>*       gradient = nullptr,   // Optional output
      Matrix<F>*       hessian  = nullptr,   // Optional output
      F*               vs       = nullptr    // Optional output
    )
    const = 0;

    //-----------------------------------------------------------------------//
    // Static Helper Functions:                                              //
    //-----------------------------------------------------------------------//
    // "MkDiffParamsStub":
    //
    // Creates a "stub" of Diffusion Params (without "t0", "S0", "coeffs")
    // suitable for use in the "CondLLH" ctor:
    //
    static Diffusion_Params<F> MkDiffParamsStub
    (
      std::string const& diff_type,          // Diffusion name
      bool               with_reg = true     // Use Regime Switching?
    );
  };
}
