// vim:ts=2:et
//===========================================================================//
//                                 "PDF2D.hpp":                              //
//           Functional Object for Probability Density Functions             //
//                         for 2D StocVol Diffusions                         //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Common/CUDAEnv.hpp"
#include "Diffusions/Diffusion2D_Fact.h"
#include "Grids/BoundConds2D.h"
#include "Grids/Grid2D_Fact.h"
#include "PDF/PDF2D.h"
#include "PDF/CondS_Core.hpp"
#include <cassert>
#include <stdexcept>

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // Non-Default Ctor:                                                       //
  //=========================================================================//
  template<typename F>
  PDF2D<F>::PDF2D
  (
    Diffusion_Params<F>        const& diff_params,
    typename Grid2D<F>::Params const& grid_params,
    bool                              isFwd
  )
  : m_grid(shared_ptr<Grid2D<F>>
            (MkGrid2D<F>
              (shared_ptr<Diffusion2D<F>>(MkDiffusion2D<F>(diff_params)),
               grid_params)
            )),
    m_isFwd  (isFwd),
    m_retKern(*m_grid)
  {
    // Get the actual Grid geometry:
    //
    assert(m_grid != nullptr);
    Vector<F> const& Sx = m_grid->GetSx();
    Vector<F> const& Vx = m_grid->GetVx();

    int m = int(Sx.size());
    int n = int(Vx.size());
    assert(m >= 2 && n >= 2);

    // Allocate and initialise the initial solution space:
    //
    m_pSV.resize(m, n);
    ResetGrid();

    if (m_grid->UseCUDA())
    {
      // Allocate the CUDA data structures:
      CUDAEnv const&  cudaEnv = m_grid->GetCUDAEnv();
      int NB       =  cudaEnv.NThreadBlocks();
      assert(NB >= 1);

      m_cudaMLEUCs =  cudaEnv.CUDAAlloc<MLEResUC>(NB);
      m_cudaMLEY   =  cudaEnv.CUDAAlloc<MLEResY> (1);

      // To copy CUDA "UC" data back into host memory:
      m_MLEUCs.resize(NB);

      // Also generate the CUDA Threads Schedule  for  "CondS":
      // "i" indices [0..m-1] are processed by Blocks, "j" indices [0..n-1] --
      // by Threads within a given Block; see "CondS_Core.hpp":
      //
      m_cudaSchCS  =  cudaEnv.MkSched2D(m, n);
    }
    else
    {
      m_cudaMLEUCs =  nullptr;
      m_cudaMLEY   =  nullptr;
      m_cudaSchCS  =  nullptr;
    }
  }

  //=========================================================================//
  // Dtor:                                                                   //
  //=========================================================================//
  template<typename F>
  PDF2D<F>::~PDF2D()
  {
    m_retKern.Destroy(*m_grid);

    if (m_grid->UseCUDA())
    {
      assert(m_cudaMLEUCs != nullptr && m_cudaMLEY != nullptr);
      CUDAEnv const&  cudaEnv = m_grid->GetCUDAEnv();

      cudaEnv.CUDAFree<MLEResUC>(m_cudaMLEUCs);
      cudaEnv.CUDAFree<MLEResY> (m_cudaMLEY);
    }
    else
      assert(m_cudaMLEUCs == nullptr && m_cudaMLEY == nullptr);
  }

  //=========================================================================//
  // "ResetGrid":                                                            //
  //=========================================================================//
  // Set up the initial conds on the Grid at t=0:
  //
  template<typename F>
  void PDF2D<F>::ResetGrid() const
  {
    // Position "m_currT" at the beginning or at the end of tge Grid's temporal
    // span:
    m_currT = m_isFwd ? StartTime() : EndTime();

    if (m_isFwd)
      //---------------------------------------------------------------------//
      // Fwd: Fokker-Planck: use a 2D Delta function or GBMS init:           //
      //---------------------------------------------------------------------//
      // Initialise the solution space as a 2D delta functnion, but only if
      // (S0,V0) are on the Grid -- otherwise the initial conds are not act-
      // ually used, and cannot be correctly set:
      //
      if (m_grid->IsS0OnGrid() && m_grid->IsV0OnGrid())
        m_grid->DeltaSV(&m_pSV);
      else
        ZeroOut<F>(&m_pSV);    // XXX: invalid PDF on first call-back

    else
      //---------------------------------------------------------------------//
      // !Fwd => Arrow-Debreu:  Use a 1D Delta function:                     //
      //---------------------------------------------------------------------//
      // V0 is ignored apart from the grid scale; "forS" flag is set. XXX: if
      // S0 is not on the Grid, this will result in an error:
      //
      m_grid->DeltaS(&m_pSV);
  }

  //=========================================================================//
  // The Integrator:                                                         //
  //=========================================================================//
  template<typename F>
  void PDF2D<F>::IntegrateTo
  (
    DateTime                t_to,
    vector<DateTime> const* special_times,
    CallBack const*         call_back,
    bool                    force_Dirichlet
  )
  const
  {
    // Verify "t_to" (though the Grid will do it later anyway):
    if (t_to < StartTime() || t_to > EndTime())
      throw invalid_argument("PDF2D::IntegrateTo: Invalid t_to");

    // The call-back to be invoked on the Grid:
    typename Grid2D<F>::CallBack gridCallBack =
      MkGridCallBack(t_to, call_back);

    if (m_isFwd)
    {
      // Fwd integration:
      if (t_to < m_currT)
        // Will have to integrate from the beginning again, so reset the grid:
        ResetGrid();

      // Perform fwd integration from "m_currT" to "t_to". NB: Use "Neumann-0"
      // BCs to reduce probability mass leakage, although Dirichlet-0 can also
      // be used if the integration domain is wide enough:
      //
      BoundConds2D<F> bcs
      (
        force_Dirichlet
        ? BCType::DirichletBCT
        : BCType::NeumannBCT
      );
      m_grid->SolveFokkerPlanck
        (m_currT, &m_pSV, t_to, bcs, &gridCallBack, special_times);
    }
    else
    {
      // Bwd integration:
      if (t_to > m_currT)
        // Will have to integrate from the beginning again, so reset the grid:
        ResetGrid();

      // Perform bwd integration from "m_currT" to "t_to". Here the probability
      // mass is not conserved so use "Dirichlet-0" BCs:
      //
      BoundConds2D<F> bcs(BCType::DirichletBCT);
      m_grid->ArrowDebreu
        (m_currT, &m_pSV, t_to, bcs, &gridCallBack, special_times);
    }
  }

  //=========================================================================//
  // "GridTest":                                                             //
  //=========================================================================//
  template<typename F>
  void PDF2D<F>::GridTest
    (bool with_discount, CallBack const* call_back) const
  {
    // Same grid call-back as in the main integrator:
    typename Grid2D<F>::CallBack gridCallBack =
      MkGridCallBack(EndTime(), call_back);

    // NB: "GridTest" always runs Bwd induction:
    m_grid->GridTest(&m_pSV, with_discount, &gridCallBack);

    // Reset the grid as its state is now inconsistent with the PDF2D state
    // (the test results are only available via the call-back):
    ResetGrid();
  }

  //=========================================================================//
  // "MkGridCallBack":                                                       //
  //=========================================================================//
  template<typename F>
  typename Grid2D<F>::CallBack PDF2D<F>::MkGridCallBack
    (DateTime t_to,   CallBack const* call_back)  const
  {
    return
      [this, t_to, call_back]
      (Grid2D<F> const& DEBUG_ONLY(grid), DateTime t,  int stk,  F* p,
       DateTime  t_next,                  TradingCalendar::CalReg reg)
      -> Time
      {
        assert(&grid == m_grid.get());

        // Invoke the user call-back first:
        Time alt_dt =
          (call_back != nullptr)
          ? (*call_back)(*this, t, stk, p, t_next, reg)
          : ZeroTime();

        // NB: "f" is not necesserily "m_pSV", it may be the CUDA memory ptr if
        // "grid" operates in the CUDA mode!  In that case, we do NOT update
        // "m_pSV"  at intermediate steps  (although the user call-back MAY do
        // that). At the last step, "m_pSV" is updated automatically by "Grid2D
        // ::TimeMarshal" which also applies the final Bound Conds to it:
        //
        if (t == t_to || alt_dt.is_negative())
          m_currT = t;   // Will terminate now

        return alt_dt;
      };
  }

  //=========================================================================//
  // "PDFR" (Returns Density for one or more "x" arg(s)):                    //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // Single "x":                                                             //
  //-------------------------------------------------------------------------//
  // Helper method, typically to be unvoked from the user call-back. Computes
  // the PDF of the Returns over (t, t+dt) at a given point "x":
  //
  template<typename F>
  F PDF2D<F>::PDFR
    (DateTime t, Time dt, TradingCalendar::CalReg reg, F const* p, F x) const
  {
    // Set the time interval in the kernel. XXX: this is NOT thread-safe if
    // "retKern" is shared between multiple threads:
    m_retKern.SetTimeInterval(*m_grid, t, dt, reg);

    // Set the "x" param on the kernel:
    m_retKern.SetX(x);

    // Run the Quadrature:
    F q = m_grid->template Quadrature2D<ReturnsPDFKern<F>>(m_retKern, p);

    // Apply the scaling factor:
    return q / SqRt(F(2.0) * Pi<F>() * YF<F>(dt));
  }

  //-------------------------------------------------------------------------//
  // Multiple "x"s:                                                          //
  //-------------------------------------------------------------------------//
  template<typename F>
  void PDF2D<F>::PDFR
  (
    DateTime t, Time dt, TradingCalendar::CalReg reg, F const* p,
    Vector<F>*  res
  )
  const
  {
    assert(res != nullptr);

    // Set the time interval on the kernel (XXX: see the above note on thread-
    // safety):
    m_retKern.SetTimeInterval(*m_grid, t, dt, reg);

    // Run the Quadrature; NB: "X"s are assumed to be already set on the
    // "m_retKern":
    m_grid->template Quadrature2D<ReturnsPDFKern<F>>(m_retKern, p, res);

    // Apply the scaling factor:
    F sf  = SqRt(F(2.0) * Pi<F>() * YF<F>(dt));
    *res /= sf;
  }

  //=========================================================================//
  // "ConditionOnS":                                                         //
  //=========================================================================//
  template<typename F>
  typename PDF2D<F>::MLERes PDF2D<F>::ConditionOnS
    (DateTime t, F* f, F SY, Time next_dt) const
  {
    // Get the "Sx" axis:
    Vector<F> const& Sx = m_grid->GetSx();
    Vector<F> const& Vx = m_grid->GetVx();
    int m = int(Sx.size());
    int n = int(Vx.size());
    assert(m >= 2 && n >= 2);

    // Veryfy "SY"; in fact, more stringinet check is applied when it comes to
    // adjusting the grid:
    if (SY < Sx[0] || SY > Sx[m-1])
      throw invalid_argument("PDF2D::ConditionOnS: Invalid SY");

    // Bracket "SY": Sx[iF] <= SY < Sx[iF+1]; "F" in "iF" for "Floor":
    F hSold = (Sx[m-1] - Sx[0]) / F(m-1);
    assert(hSold > F(0.0));

    int iF = int(Floor((SY - Sx[0]) / hSold));

    if (!(Sx[iF] <= SY && SY < Sx[iF+1]))
    {
      // This may happen ONLY if "SY" actually coincides with Sx[iF+1] (i.e.,
      // we got 2 identical "S" vals at different times -- this is unlikely
      // but may happen) but its index was rounded down by "Floor". So move
      // "iF" up:
      assert(Abs(Sx[iF+1] - SY) < F(100.0) * Eps<F>());
      ++iF;
    }
    assert(0 <= iF && iF <= m-2 && Sx[iF] <= SY && SY < Sx[iF+1]);

    // Step fraction (from Sx[iF]) corresponding to Y: will be used for
    // interpolation:
    F frac = (SY - Sx[iF]) / hSold;

    // Now adjust the "S" axis in the Grid: "R" in "iR" for "Round".   NB: this
    // only adjusts the axis itself, not the solution space; the latter is done
    // by "ConditionOnS_Core" below:
    //
    int iR = m_grid->AdjustSx(t, SY, next_dt);
    assert(0 <= iR && iR < m);

    // Replace the current PDF in "f" by p(Y,V)/p(Y) * delta(S-Y): this is "2D"
    // conditioning on S=Y:
    F hSnew   = Sx[1] - Sx[0];
    F hV      = Vx[1] - Vx[0];
    F hSnewV  = hSnew * hV;

    // Over-all result accumulator; its components are aliased:
    MLERes    res;
    MLEResUC& resUC = res.m_UC;
    MLEResY & resY  = res.m_Y;

    // NB: Do both static and dynamic checks for CUDA availability, to avoid
    // link errors:
    //
    if (!m_grid->UseCUDA())
      // Host-based computation -- result comes straight into "res":
      ConditionOnS_Core<F>
        (nullptr, m, n, iF, frac, iR, hSnewV, f, &resUC, &resY);
    else
    {
      // CUDA-based computation:
      CUDAEnv const& cudaEnv = m_grid->GetCUDAEnv();
      cudaEnv.Start();

      ConditionOnS_KernelInvocator<F>::Run
      (
        m_grid->GetCUDAEnv(), m_cudaSchCS, m, n, iF, frac, iR, hSnewV, f,
        m_cudaMLEUCs, m_cudaMLEY
      );

      // Copy the data back into host memory:
      int NB       = cudaEnv.NThreadBlocks();
      assert(NB >= 1);

      cudaEnv.ToHost<MLEResUC>(m_cudaMLEUCs, &(m_MLEUCs[0]),  NB);
      cudaEnv.ToHost<MLEResY> (m_cudaMLEY,   &resY,            1);
      cudaEnv.Stop();

      // Compute the accumulated UC vals across all blocks -> into "resUC":
      for (int i = 0; i < NB; ++i)
      {
        MLEResUC const& bresUC = m_MLEUCs[i]; // Alias!

        resUC.m_minP  = min<F>(resUC.m_minP,  bresUC.m_minP);
        resUC.m_maxP  = max<F>(resUC.m_maxP,  bresUC.m_maxP);
        resUC.m_sumP += bresUC.m_sumP;
      }
    }
    // XXX: "sumP" is still NOT multiplied by "hSV" at this point (but "sumPY"
    // is!), so adjust normalise "sumP" -- NB: must still use the "hSold":
    resUC.m_sumP *= (hSold * hV);

    // Return the result. XXX: its validity is still NOT checked at this point,
    // this is left to the caller:
    return res;
  }
}
