// vim:ts=2:et
//===========================================================================//
//                         "CondLLH2D_BayesOptFilt.h":                       //
//    Conditional Log-LikeliHood Function Using Bayesian Optimal Filtering   //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Common/CUDAEnv.h"
#include "Grids/Grid2D.h"
#include "PDF/CondLLH.h"
#include <memory>
#include <string>

namespace Arbalete
{
  //=========================================================================//
  // "CondLLH2D_BayesOptFilt" Class:                                         //
  //=========================================================================//
  template<typename F>
  class CondLLH2D_BayesOptFilt final: public CondLLH<F>
  {
  private:
    //-----------------------------------------------------------------------//
    // Data Flds:                                                            //
    //-----------------------------------------------------------------------//
    // NB: We still need "Grid Params" here, although simplified ones -- a BOF
    // Grid will be used. See "MkGridParamsStub" below:
    //
    typename Grid2D<F>::Params  m_gridParams;

    CondLLH2D_BayesOptFilt();   // Default ctor is hidden

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor, Dtor:                                               //
    //-----------------------------------------------------------------------//
    // Args:
    // diff_params: as in the base class ctor;
    // grid_params: also stub ("m_TS", "m_TV", "m_debugLevel", "m_dt*", "m_sts"
    //              may be unset); use "MkGridParamsStub" to create a stub;
    // mkt_data:    as in the base class ctor:
    // debug_level:
    // 0:   No debugging output at all
    // 1:   Reserved for top-level Calibrator
    // 2:   Summary at conditioning points
    // 3,4: "V" densities at conditioning points
    //
    CondLLH2D_BayesOptFilt
    (
      Diffusion_Params<F>        const&  diff_params,    // Stub
      typename Grid2D<F>::Params const&  grid_params,    // Stub
      std::vector<MDItem<F>>     const&  mkt_data,
      int                                debug_level = 0
    );

    // Dtor:
    ~CondLLH2D_BayesOptFilt() {}

    //-----------------------------------------------------------------------//
    // Evaluation:                                                           //
    //-----------------------------------------------------------------------//
    // Implementation of abstract functions from the base class:
    //
    void NLLH
    (
      Vector<F> const& diff_coeffs,
      F*               val,
      Vector<F>*       gradient = nullptr, // MUST BE NULL
      Matrix<F>*       hessian  = nullptr, // MUST BE NULL
      F*               vs       = nullptr
    )
    const override;

    // CUDA Timing:
    F   GetCUDATiming() const override
      { return F(m_gridParams.m_cudaEnv->CumTimeSec()); }

    //-----------------------------------------------------------------------//
    // Static Helper Functions:                                              //
    //-----------------------------------------------------------------------//
    // "MkGridParamsStub":
    //
    // Creates a "stub" of "GridParams2D" (w/o "TS", "TV", "debug", "dt*",  and
    // "sts"), suitable for use in the ctor of this class -- the missing params
    // are taken by the ctor from other inputs.
    // XXX: same default time step ("dt") is applied to all Regimes; if requi-
    // red otherwise, the constructed "Params" object can be modified later:
    //
    static typename Grid2D<F>::Params MkGridParamsStub
    (
      int    n,
      F      S0,
      F      loV = F(0.0),    // Vol or var =   0%
      F      upV = F(1.0),    // Vol or var = 100%
      std::shared_ptr<CUDAEnv>         const& cudaEnv = nullptr,
      std::shared_ptr<TradingCalendar> const& cal     = nullptr
    );
  };
}
