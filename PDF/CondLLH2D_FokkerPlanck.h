// vim:ts=2:et
//===========================================================================//
//                        "CondLLH2D_FokkerPlanck.h":                        //
//      Conditional Log-LikeliHood Function Using the Fokker-Planck PDE      //
//===========================================================================//
#pragma once

#include "Common/CUDAEnv.h"
#include "PDF/CondLLH.h"
#include "PDF/PDF2D.h"
#include <memory>
#include <string>

namespace Arbalete
{
  //=========================================================================//
  // "CondLLH2D_FokkerPlanck" Class:                                         //
  //=========================================================================//
  template<typename F>
  class CondLLH2D_FokkerPlanck final: public CondLLH<F>
  {
  public:
    //=======================================================================//
    // "Params":                                                             //
    //=======================================================================//
    // These are params specific to this particulat class; Diff and Grid params
    // are to be specified on their own:
    // Debug Level:
    // 0:   No debugging output at all
    // 1:   Reserved for top-level Calibrator
    // 2:   Summary at conditioning points
    // 3:   Full densities at conditioning points
    // 4:   Full densities at each time step:
    //
    struct Params
    {
      int   m_debugLevel;
      bool  m_useDirichlet;   // Normally NOT set -- Neumann is used
      int   m_minSbC;         // Minimim number of time steps btwn cond points

      // Ctor (Default and Non-Default):
      Params
        (int debug_level = 0, bool useDirichlet = false, int minSbC = 60)
      : m_debugLevel  (debug_level),
        m_useDirichlet(useDirichlet),
        m_minSbC      (minSbC)
      {}
    };

  private:
    //-----------------------------------------------------------------------//
    // Data Flds:                                                            //
    //-----------------------------------------------------------------------//
    typename Grid2D<F>::Params  m_gridParams;
    bool                        m_useDirichlet;
    int                         m_minSbC;

    CondLLH2D_FokkerPlanck();   // Default ctor is hidden

  public:
    //=======================================================================//
    // Non-Default Ctor, Dtor, Access:                                       //
    //=======================================================================//
    // Args:
    // diff_params: as in the base class ctor;
    // grid_params: also stub ("m_TS", "m_TV", "m_debugLevel"  may be unset);
    //              use "MkGridParamsStub" to create such a stub;
    // llh_params:  as above;
    // mkt_data:    as in the base class ctor:
    //
    CondLLH2D_FokkerPlanck
    (
      Diffusion_Params<F>        const&  diff_params,  // Stub
      typename Grid2D<F>::Params const&  grid_params,  // Stub
      Params                     const&  llh_params,   // as above
      std::vector<MDItem<F>>     const&  mkt_data
    );

    // Dtor:
    ~CondLLH2D_FokkerPlanck() {}

    //-----------------------------------------------------------------------//
    // Evaluation:                                                           //
    //-----------------------------------------------------------------------//
    void NLLH
    (
      Vector<F> const& diff_coeffs,
      F*               val,
      Vector<F>*       gradient = nullptr,   // MUST BE NULL
      Matrix<F>*       hessian  = nullptr,   // MUST BE NULL
      F*               vs       = nullptr
    )
    const override;

    // CUDA Timing:
    F   GetCUDATiming() const override
      { return F(m_gridParams.m_cudaEnv->CumTimeSec()); }

    //-----------------------------------------------------------------------//
    // Static Helper Functions:                                              //
    //-----------------------------------------------------------------------//
    // "MkGridParamsStub":
    //
    // Creates a "stub" of "GridParams2D" (w/o "TS", "TV", "debug"), suitable
    // for use in the LLH2D ctor -- the missing params  are taken by the ctor
    // from other inputs.
    // XXX: same default time step ("dt") is applied to all Regimes; if requi-
    // red otherwise, the constructed "Params" object can be modified later:
    //
    static typename Grid2D<F>::Params MkGridParamsStub
    (
      std::string method, int m, int n, int sts, Time dt,
      std::shared_ptr<CUDAEnv>          const& cudaEnv = nullptr,
      std::shared_ptr<TradingCalendar>  const& cal     = nullptr
    );
  };
}
