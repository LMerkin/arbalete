// vim:ts=2:et
//===========================================================================//
//                                  "PDF2D.cpp":                             //
//                        Pre-Defined Instances of "PDF2D"                   //
//===========================================================================//
#include "PDF/PDF2D.hpp"

namespace Arbalete
{
  template class PDF2D<double>;
  template class PDF2D<float>;
}
