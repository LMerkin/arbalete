// vim:ts=2:et
//===========================================================================//
//                      "CondLLH2D_FokkerPlanck.cpp":                        //
//           Pre-Defined Instances of "CondLLH2D_FokkerPlanck"               //
//===========================================================================//
#include "PDF/CondLLH2D_FokkerPlanck.hpp"

namespace Arbalete
{
  template class CondLLH2D_FokkerPlanck<double>;
  template class CondLLH2D_FokkerPlanck<float>;
}
