// vim:ts=2:et
//===========================================================================//
//                          "CondLLH_Analytical.h":                          //
//        Conditional LLH for Analytical Diffusions (Any Dimensions)         //
//===========================================================================//
// NB: This is a "thin interface" between the "CondLLH" abstract class and Ana-
// lytical Diffusions: the actual LLH computations  are implemented by the Ana-
// lytical Diffuions:
//
#pragma once

#include "PDF/CondLLH.h"
#include <memory>

namespace Arbalete
{
  //=========================================================================//
  // "CondLLH_Analytical":                                                   //
  //=========================================================================//
  template<typename F>
  class CondLLH_Analytical final: public CondLLH<F>
  {
  private:
    //-----------------------------------------------------------------------//
    // Data Flds:                                                            //
    //-----------------------------------------------------------------------//
    bool    m_debugLevel;

    // Default Ctor is hidden:
    CondLLH_Analytical();

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor, Dtor:                                               //
    //-----------------------------------------------------------------------//
    CondLLH_Analytical
    (
      Diffusion_Params<F>    const& diff_params,    // Stub
      std::vector<MDItem<F>> const& mkt_data,
      int                           debug_level = 0
    )
    : CondLLH<F>(diff_params, mkt_data, debug_level)
    {}

    ~CondLLH_Analytical() {}

    //-----------------------------------------------------------------------//
    // Evaluation:                                                           //
    //-----------------------------------------------------------------------//
    // Implementation of abstract functions from the base class:
    //
    void NLLH
    (
      Vector<F> const& diff_coeffs,
      F*               val,
      Vector<F>*       gradient = nullptr,
      Matrix<F>*       hessian  = nullptr,
      F*               vs       = nullptr
    )
    const override;

    // XXX: This class currently does not use CUDA:
    F   GetCUDATiming() const override { return F(0.0);       }
  };
}
