// vim:ts=2:et
//===========================================================================//
//                         "CondLLH_Analytical.hpp":                         //
//        Conditional LLH for Analytical Diffusions (Any Dimensions)         //
//===========================================================================//
#pragma once

#include "Diffusions/DiffusionAnalytical_Fact.h"
#include "PDF/CondLLH_Analytical.h"

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // Evaluation:                                                             //
  //=========================================================================//
  // XXX: TODO: Provide evaluation in CUDA space as well:
  //
  template<typename F>
  void CondLLH_Analytical<F>::NLLH
  (
    Vector<F> const& diff_coeffs,
    F*               val,
    Vector<F>*       gradient,
    Matrix<F>*       hessian,
    F*               vs
  )
  const
  {
    // NB: "gradient" and "hessian" may still be NULL, if pure evaluation was
    // requested:
    assert(val != nullptr);

    // Install "diff_coeffs" in the "DiffParams":
    assert(!diff_coeffs.empty());
    this->m_diffParams.m_coeffs = diff_coeffs;

    // Construct the Analytical Diffusion:
    shared_ptr<DiffusionAnalytical<F>> diff
      (MkDiffusionAnalytical<F>(this->m_diffParams));

    // Invoke "NLLH" on the Diffusion:
    assert(diff != nullptr);
    diff->NLLH
      (this->m_mdTYs, this->m_mdS, this->m_mdRegs, val, gradient, hessian, vs);
  }
}
