// vim:ts=2:et
//===========================================================================//
//                           "CondLLH_Analytical.cpp":                       //
//              Pre-Defined Instances of "CondLLH_Analytical"                //
//===========================================================================//
#include "PDF/CondLLH_Analytical.hpp"

namespace Arbalete
{
  template class  CondLLH_Analytical<double>;
  template class  CondLLH_Analytical<float>;
}
