// vim:ts=2:et
//===========================================================================//
//                              "CondS_Core.hpp":                            //
//        Computation of Conditional Probabilities: Templated Core           //
//===========================================================================//
#pragma once

#include "Common/CUDAEnv.hpp"
#include "PDF/PDF2D.h"
#include <cassert>

namespace Arbalete
{
  using namespace std;

# ifdef __CUDACC__
  // The following array is actually comprised of "MLEResUC" structs:
  extern __shared__ char CondS_ShRes[];
# endif

  //=========================================================================//
  // "ConditionOnS_Core":                                                    //
  //=========================================================================//
  // On the current axis, the conditioning point  "SY"  is bracketed between
  // [iF .. iF+1] nodes, at the step fraction "frac" from "iF".
  // On the new (adjusted) axis, the same conditioning point "SY" will be at
  // "iR" node:
  //
  template<typename F>
  GLOBAL static void ConditionOnS_Core
  (
    CUDAEnv::Sched2D const* CUDACC_OR_DEBUG(sched),
    int m, int n, int iF, F frac, int iR, F hSV, F* f,
    typename PDF2D<F>::MLEResUC*  ressUC,
    typename PDF2D<F>::MLEResY*   resY
  )
  {
    assert(0 < iF && iF < m-2  && 0 < iR && iR < m-1  && F(0.0) <= frac &&
           frac < F(1.0) && hSV > F(0.0) && f != nullptr && ressUC != nullptr &&
           resY != nullptr);

    // Accumulators on the ORIGINAL PDF:
    F minP  = F(0.0);   // Over-all min
    F maxP  = F(0.0);   // Over-all max
    F sumP  = F(0.0);   // Over-all sum (for detection of prob mass leaks)

    // 2nd interpolation coeff:
    F frac1 = F(1.0) - frac;

#   ifdef __CUDACC__
    // In the CUDA mode, each thread block will process some number  of "i"s
    // (typically, 1..5), so that each "i" is processed by threads belonging
    // to only one block (so they can be synchronised).
    // Get the range of "i"s (out of the total range 0..(m-1)) to be processed
    // by the curr block; there are "m" "i"s and "gridDim.x" thread blocks:
    //
    assert(sched != nullptr);
    int Tn = blockDim.x * blockIdx.x + threadIdx.x;
    CUDAEnv::Sched2D schn = sched[Tn];

    int i = schn.m_outer;
    if (i < 0)
      // This Thread (and the whole Block it belongs to) is not in use:
      return;

    // For the curr block, the array of "MLEResUC" structs is in shared memory
    // (one struct per each thread within the block):
    //
    typename PDF2D<F>::MLEResUC* thrRess =
      (typename PDF2D<F>::MLEResUC*)(CondS_ShRes);

#   else
    // In the Host mode, do the full "i" range. XXX: OpenMP parallelisation
    // does not work correctly here (because of "sumP" accumulation etc):
    //
    assert(sched == nullptr);

    for (int i = 0; i < m; ++i)
#   endif
    //-----------------------------------------------------------------------//
    // "i" Processing:                                                       //
    //-----------------------------------------------------------------------//
    {
#     ifdef __CUDACC__
      int jFrom = schn.m_innerFrom;
      int jTo   = schn.m_innerTo;

      // NB: it is possible that jFrom > jTo, so the curr thread will have no-
      // thing to do in the loop below. However, do NOT exit such threads now:
      // they may be needed for post-processing!
#     else
      // Host mode: Do the full range of "j" indices:
      int jFrom = 0;
      int jTo   = n-1;
#     endif

      int off = m * jFrom + i;        // (i, jFrom)

      if (i == iF)
        for (int j = jFrom; j <= jTo; ++j, off += m)
        {
          //----------------------------------------------------------------//
          // i==iF, iF+1 and iR for all "j"s:                               //
          //----------------------------------------------------------------//
          // NB: IN CUDA mode, these 3 "i"s are processed together, to avoid
          // race conditions between different thereads / blocks:
          //
          int offR = off - iF + iR;   // (iR, j)

          // Interpolate p(S,V) wrt S between i=iF and i=iF+1. The result is
          // stored at i=iR:
          F f0 = f[off];
          F f1 = f[off+1];
          F fR = f[offR];
          F fI = frac * f0 + frac1 * f1;  // Interpolated value

          // Update the accumulators: NB: neither of iF, iF+1, iR is allowed
          // to be a boundary point,  so they all contributed  to "sumP" and
          // "sumPY" (if j >= 1), "maxP" and "minP":
          //
          minP  = Min<F>(minP,  Min<F>(f0, Min<F>(f1, fR)));
          maxP  = Max<F>(maxP,  Max<F>(f0, Max<F>(f1, fR)));

          // Update "sumP" (i, j >= 1 as in Rectangles rule); NB: beware of
          // double count of "fR":
          assert(i >= 1);
          if (j >= 1)
          {
            sumP += (f0 + f1);              // iF, iF+1
            if (iR != iF && iR != iF+1)
              sumP += fR;                   // iR (if not already used)
          }

          // Zero out f[off] and f[off+1] (i.e., iF and iF+1):
          f[off]   = F(0.0);
          f[off+1] = F(0.0);

          // Store the interpolation result at i=iR (which MAY coincide with
          // iF or iF+1):
          f[offR]  = fI;
        }
      else
      if (i != iF+1 && i != iR)
        for (int j = jFrom; j <= jTo; ++j, off += m)
        {
          //-----------------------------------------------------------------//
          // All other "i"s, all "j"s:                                       //
          //-----------------------------------------------------------------//
          // Update the over-all accumulators:
          F f0 = f[off];
          minP = Min<F>(minP, f0);
          maxP = Max<F>(maxP, f0);

          // NB: again, summation is consistent with the Rectangles rule:
          if (i >= 1 && j >= 1)
            sumP += f0;

          // Zero-out f[off]:
          f[off] = F(0.0);
        }
    }
    // "i" processing done

#   ifdef __CUDACC__
    // Save the results of the "j" range in per-thread shared memory:
    typename PDF2D<F>::MLEResUC& tres = thrRess[threadIdx.x];   // Alias!
    tres.m_minP  = minP;
    tres.m_maxP  = maxP;
    tres.m_sumP  = sumP;

    // Synchronise all threads in the curr block, then compute per-block reslts
    // (using threadIdx=0):
    __syncthreads();

    if (threadIdx.x == 0)
    {
      // Construct "MLEResUC" vals for the whole block:
      minP  = F(0.0);
      maxP  = F(0.0);
      sumP  = F(0.0);
      for (int tid = 0; tid < blockDim.x; ++tid)
      {
        typename PDF2D<F>::MLEResUC const& tres = thrRess[tid]; // Alias!
        minP   = Min<F>(minP, tres.m_minP);
        maxP   = Max<F>(maxP, tres.m_maxP);
        sumP  += tres.m_sumP;
      }
      // Save the results in the per-block struct:
      typename PDF2D<F>::MLEResUC&  bres = ressUC[blockIdx.x];  // Alias!
      bres.m_minP   = minP;
      bres.m_maxP   = maxP;
      bres.m_sumP   = sumP;
    }
#   else
    // Host-based computations: "blkRess[]" is just one struct:
    ressUC->m_minP  = minP;
    ressUC->m_maxP  = maxP;
    ressUC->m_sumP  = sumP;
#   endif
    // NB: XXX: "sumP" is NOT multiplied by "hSV" here, and hence is NOT ~1,
    // because it would require OLD "hS" for that. So normalisation must be
    // performed by the CALLER!

    //-----------------------------------------------------------------------//
    // Compute the *Y flds and normalise the cond density at f[iR]:          //
    //-----------------------------------------------------------------------//
    // This is to be done by one thread of the block which processed "iF":
    //
#   ifdef __CUDACC__
    if (i == iF && schn.m_outerStart)
#   endif
    {
      // Compute the *Y flds over i=iR and all "j"s:
      //
      F   minPY = F(0.0);
      F   maxPY = F(0.0);
      int optJ  = 0;
      F   sumPY = F(0.0);

      int off   = iR;
      for (int j = 0; j < n; ++j, off += m)
      {                   // NB: Min/Max is computed before cond PDF is
        F pc = f[off];    // normalised!
        minPY = Min<F>(minPY, pc);
        if (pc > maxPY)
        {
          maxPY = pc;
          optJ  = j;
        }
        if (j >= 1)
          sumPY += pc;
      }
      sumPY *= hSV;       // NB: "sumPY" IS already multiplied by "hSV"

      // Save the result:
      resY->m_minPY = minPY;
      resY->m_maxPY = maxPY;
      resY->m_optJ  = optJ;
      resY->m_sumPY = sumPY;

      // Normalise the conditional PDF  (XXX: we do not currently check for
      // "sumPY" validity -- this will be done by the caller which provides
      // better error control):
      //
      int offMax  = m * (n-1) + iR;
      for(off = iR; off <= offMax; off += m)
        f[off] /= sumPY;
    }
    // All done!
  }

  //=========================================================================//
  // "ConditionOnS_KernelInvocator":                                         //
  //=========================================================================//
  template<typename F>
  struct ConditionOnS_KernelInvocator
  {
    static void Run
    (
      CUDAEnv const& cudaEnv, CUDAEnv::Sched2D const* sched,
      int m,  int n, int iF,  F frac,   int iR,   F hSV,  F* f,
      typename PDF2D<F>::MLEResUC* ressUC,
      typename PDF2D<F>::MLEResY*  resY
    )
#   ifdef __CUDACC__
    {
      // Compute the shared memory size:
      int ShMemSz =
        int(cudaEnv.ThreadBlockSize() * sizeof(typename PDF2D<F>::MLERes));

      // Check for excessively-large "ShMemSz values -- this would result in
      // major inefficiencies:
      if (ShMemSz * cudaEnv.ThreadBlocksPerSM() > cudaEnv.ShMemPerSM())
        throw invalid_argument("ConditionOnS Kernel: ShMemSz too large");

      // Set the CUDA device:
      CUDAEnv::Lock();
      cudaEnv.SetCUDADevice();

      // Invoke the kernel:
      ConditionOnS_Core<F>
        <<<cudaEnv.NThreadBlocks(), cudaEnv.ThreadBlockSize(), ShMemSz,
           cudaEnv.Stream()
        >>>
        (sched, m, n, iF, frac, iR, hSV, f, ressUC, resY);

      CUDAEnv::Unlock();
      CUDA_CHECK_STATUS("ConditionOnS Kernel invocation failed")
    }
#   else
    ; // Spec only
#   endif
  };
}
