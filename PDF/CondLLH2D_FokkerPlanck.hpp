// vim:ts=2:et
//===========================================================================//
//                        "CondLLH2D_FokkerPlanck.hpp":                      //
//      Conditional Log-LikeliHood Function Using the Fokker-Planck PDE      //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "PDF/CondLLH2D_FokkerPlanck.h"
#include <cstdio>
#include <iostream>
#include <stdexcept>

namespace
{
  using namespace Arbalete;

  //-------------------------------------------------------------------------//
  // Tolerance Params (XXX: Currently Not User-Configurable):                //
  //-------------------------------------------------------------------------//
  // Tolerance to Probability Mass Leaks: 1% (because it will be re-normal-
  // ised frequently anyway):
  //
  template<typename F>
  F ProbMassTol() { return F(1e-2); }

  // Tolerance to Negative PDF vals (at conditioning points):
  // |min|/max < 10bp:
  //
  template<typename F>
  F NegPDFTol()   { return F(1e-3); }
}

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "CondLLH2D_FokkerPlanck": Non-Default Ctor:                             //
  //=========================================================================//
  // NB: This ctor does NOT create the Diffusion, Grid and PDF objects -- this
  // cannot be done before the Diffusiion Coeffs are knowm.   It just verifies
  // and memoises all params (and MktData):
  //
  template<typename F>
  CondLLH2D_FokkerPlanck<F>::CondLLH2D_FokkerPlanck
  (
    Diffusion_Params<F>         const&  diff_params,
    typename Grid2D<F>::Params  const&  grid_params,
    Params                      const&  llh_params,
    vector<MDItem<F>>           const&  mkt_data
  )
  : CondLLH<F>    (diff_params, mkt_data, llh_params.m_debugLevel),
    m_gridParams  (grid_params),
    m_useDirichlet(llh_params.m_useDirichlet),
    m_minSbC      (llh_params.m_minSbC)
  {
    // Modify "m_gridParams": The initial time span for "S" is just that btwn
    // "t0" and the 1st conditioning point (if available);   for "V", use the
    // full time length as there is no conditioning / adjustments for "V":
    //
    int nk = this->GetMDLen();
    if (nk >= 2)
    {
      m_gridParams.m_TS = this->m_mdTimes[1]    - this->m_mdTimes[0];
      m_gridParams.m_TV = this->m_mdTimes[nk-1] - this->m_mdTimes[0];
    }

    // Set the Grid debugging level:
    switch (this->m_debugLevel)
    {
    case 0:
    case 1:
      m_gridParams.m_debugLevel = 0;
      break;
    case 2:
    case 3:
      m_gridParams.m_debugLevel = 1;
      break;
    case 4:
      m_gridParams.m_debugLevel = 2;
      break;
    default:
      throw invalid_argument
            ("CondLLH2D_FokkerPlanck Ctor: Invalid DebugLevel");
    }
    if (m_minSbC <= 0)
      throw invalid_argument
      ("CondLLH2D_FokkerPlanck Ctor: Required >= 1 Steps Before Conditioning");

    // Install m_etTH" and "m_etN" on the Grid Params. See "LLH2D_BayesOptFilt"
    // Ctor for more information:
    //
    Time minTH = ZeroTime();
    Time minN  = ZeroTime();

    for (int i = 0; i < nk - 1; ++i)
    {
      // NB: Must take "m_minSbC" into account:
      F      dty = (this->m_mdTYs[i+1] - this->m_mdTYs[i]) / F(m_minSbC);
      Time   dt  = TimeOfYF(dty);
      assert(dt  > ZeroTime());

      if (this->m_mdRegs[i]  == TradingCalendar::TradingHours)
      {
        if (minTH == ZeroTime() || dt < minTH)
          minTH = dt;
      }
      else
      {
        if (minN  == ZeroTime() || dt < minN)
          minN  = dt;
      }
    }
    m_gridParams.m_etTH = minTH;
    m_gridParams.m_etN  = minN;
  }

  //=========================================================================//
  // Evaluation:                                                             //
  //=========================================================================//
  template<typename F>
  void CondLLH2D_FokkerPlanck<F>::NLLH
  (
    Vector<F> const& diff_coeffs,
    F*               val,
    Vector<F>*       gradient,
    Matrix<F>*       hessian,
    F*               vs
  )
  const
  {
    // Check the args:
    assert(val != nullptr);
    if (gradient != nullptr || hessian != nullptr)
      throw invalid_argument
            ("CondLLH2D_FokkerPlanck::NLLH: Gradient and Hessian are not "
             "supported");

    // Install "diff_coeffs" in the "DiffParams" (contain at least "V0"):
    assert(!diff_coeffs.empty());
    this->m_diffParams.m_coeffs = diff_coeffs;

    // Construct the PDF object for Fwd run:
    PDF2D<F> PDF(this->m_diffParams, m_gridParams, true);

    // LLH is a sum of logs of conditional PDFs over all conditioning points,
    // so initialise it to 0:
    *val = F(0.0);

    int nk = this->GetMDLen();
    assert(nk >= 1);
    if (vs != nullptr)
      vs[0] = diff_coeffs[0];   // "V0" is always at coeffs[0]

    //-----------------------------------------------------------------------//
    // Call-Back:                                                            //
    //-----------------------------------------------------------------------//
    typename PDF2D<F>::CallBack call_back
    (
#     ifndef NDEBUG
      [this, nk, val, vs, &PDF]
#     else
      [this, nk, val, vs]
#     endif
        (PDF2D<F> const& pdf,    DateTime t,  int stk,  F* f,
         DateTime                UNUSED_PARAM(next_t),
         TradingCalendar::CalReg UNUSED_PARAM(reg))
      ->Time
      {
        assert(&pdf == &PDF);

        if (stk <= 0)
          // stk < 0: not a conditioning point at all, so just continue
          // stk== 0: initial time, no conditioning either:
          return ZeroTime();

        // Otherwise: Conditioning Point:
        assert(1 <= stk && stk < nk && this->m_mdTimes[stk] == t);

        if (this->m_debugLevel == 3)
          // Debug output before conditioning, but only if it is not done by
          // the Grid itself ("-B" for "Before"); copying from CUDA space is
          // required:
          pdf.GetGrid().template GridDebug<true>(t, f, "-B");

        // Condition the PDF on S[stk]. This adjusts the Grid, so provide the
        // time interval to the next conditioning point:
        Time nextDt =
          (stk < nk-1)
          ? this->m_mdTimes[stk+1] - this->m_mdTimes[stk]
          : ZeroTime();

        F SYk = this->m_mdS[stk];

        typename PDF2D<F>::MLERes mle  =
          pdf.ConditionOnS(t, f, SYk, nextDt);

        typename PDF2D<F>::MLEResUC const& mleUC = mle.m_UC;  // Alias!
        typename PDF2D<F>::MLEResY  const& mleY  = mle.m_Y;   // Alias!

        F NLe = - Log(mleY.m_maxPY);  // NB: "Log" may fail, checked below;
                                      // "-" because of Negated LLH
        // Summary at Cond Point:
        if (this->m_debugLevel >= 2)
          cout << '\n' <<  t   << ": SY="<< SYk << ":\nmin(P)="
               << mleUC.m_minP << ", defect(P)="
               << (Abs(F(1.0) - mleUC.m_sumP))
               << ", min(PY)=" << mleY.m_minPY  << ", max(PY)="
               << mleY.m_maxPY << ", optJ="     << mleY.m_optJ
               << ", LLH="     << NLe           << endl;

        // Debug Output after ("-A") conditioning:
        if (this->m_debugLevel >= 3)
          pdf.GetGrid().template GridDebug<true>(t, f, "-A");

        //-------------------------------------------------------------------//
        // Quality Control:                                                  //
        //-------------------------------------------------------------------//
        if (Abs(F(1.0) - mleUC.m_sumP) > ProbMassTol<F>())
          throw runtime_error("CondLLH2D_FokkerPlanck::(): ProbMass Leak");
        assert(mleUC.m_maxP  > F(0.0));

        if (mleUC.m_minP  <  F(0.0) &&
            Abs(mleUC.m_minP  / mleUC.m_maxP ) > NegPDFTol<F>())
          throw runtime_error("CondLLH2D_FokkerPlanck::(): "
                              "Min(P) too negative");

        if (mleY.m_sumPY <= F(0.0))
          throw runtime_error("CondLLH2D_FokkerPlanck::(): Sum(PY) <= 0)");
        assert(mleY.m_maxPY > F(0.0) && IsFinite(NLe));

        if (mleY.m_minPY <  F(0.0) &&
            Abs(mleY.m_minPY / mleY.m_maxPY) > NegPDFTol<F>())
          throw runtime_error("CondLLH2D_FokkerPlanck::(): "
                              "Min(PY) too negative");

        // Also, "minPY" and "maxPY" are local wrt "minP" and "maxP", so:
        // XXX: The following is actually an assertion,  but we check it
        // explicitly anyway, even if assertions are optimised off:
        //
        if (mleY.m_minPY < mleUC.m_minP || mleY.m_maxPY > mleUC.m_maxP)
          throw runtime_error("CondLLH2D_FokkerPlanck::(): "
                              "Invalid {Min|Max}(PY) vs {Min|Max}(P)");

        //-------------------------------------------------------------------//
        // Accumulate / memoise results of the estimation:                   //
        //-------------------------------------------------------------------//
        Vector<F> const& Vx = pdf.GetGrid().GetVx();

#       ifndef NDEBUG
        int n = int(Vx.size());
        assert(0 <= mleY.m_optJ && mleY.m_optJ < n);
#       endif

        // Update the result:
        *val += NLe;
        if (vs != nullptr)
          vs[stk] = Vx[mleY.m_optJ];

        //-------------------------------------------------------------------//
        // Next time step:                                                   //
        //-------------------------------------------------------------------//
        // Need a certain minimum number of steps before the next cond point:
        //
        if (stk != nk-1)
        {
          Time toNextCond = this->m_mdTimes[stk+1] - this->m_mdTimes[stk];
          Time alt_dt     =
            TimeGener::seconds(toNextCond.total_seconds() / m_minSbC);

          if (alt_dt <= ZeroTime())   // Actually, it cannot be negative!
            throw runtime_error
                  ("CondLLH2D_FokkerPlanck::(): Time step too small");
          return alt_dt;
        }
        else
          // In all other cases, keep the curr time step:
          return ZeroTime();
      }
    );
    //-----------------------------------------------------------------------//
    // Run the Fwd Integrator from "t0" to "ts2":                            //
    //-----------------------------------------------------------------------//
    // Time horison:
    DateTime ts2 = this->m_mdTimes.back();

#   ifndef NDEBUG
    DateTime t0  = this->m_mdTimes[0];
    assert(PDF.StartTime() == t0 && PDF.EndTime() >= ts2 &&
           PDF.CurrTime () == t0);
#   endif

    // The result is accumulated in "*val" via the call-back:
    PDF.IntegrateTo
      (ts2, &(this->m_mdTimes), &call_back, m_useDirichlet);
  }

  //=========================================================================//
  // "MkGridParamsStub":                                                     //
  //=========================================================================//
  // NB: m_TS, m_TV, m_debug remain uninitialised;
  //     m_NSigmas{S|V} remain at their default vals (e.g. 7.0);
  //     m_{Lo|Up}{S|V} remain at their default (ignored) vals;
  // XXX:
  //     Same default time step is applied to all Regimes; if required, this
  //     can later be modified explicitly on the constructed object;
  //
  template<typename F>
  typename Grid2D<F>::Params
    CondLLH2D_FokkerPlanck<F>::MkGridParamsStub
  (
    string method, int m, int n, int sts, Time dt,
    shared_ptr<CUDAEnv>         const& cudaEnv,
    shared_ptr<TradingCalendar> const& cal
  )
  {
    typename Grid2D<F>::Params res;
    res.m_method    = method;
    res.m_m         = m;
    res.m_n         = n;
    res.m_sts       = sts;
    res.m_dtTH      = dt;
    res.m_dtON      = dt;
    res.m_dtWEH     = dt;
    res.m_cudaEnv   = cudaEnv;
    res.m_cal       = cal;
    // Some hard0wired defaults for other params:
    res.m_minRelS   = F(0.075);
    res.m_minRelV   = F(0.2);

    // All other flds are set to their default values, and can be modified by
    // the caller
    return res;
  }
}
