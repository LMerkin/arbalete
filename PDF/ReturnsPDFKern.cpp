// vim:ts=2:et
//===========================================================================//
//                           "ReturnsPDFKern.cpp":                           //
//        Pre-Defined Instances of "ReturnsPDFKern" (Host-Compiled)          //
//===========================================================================//
#include "Grids/Grid2D.hpp"
#include "PDF/ReturnsPDFKern.hpp"

namespace Arbalete
{
  //=========================================================================//
  // "ReturnsPDFKern" instances:                                             //
  //=========================================================================//
  template class ReturnsPDFKern<double>;
  template class ReturnsPDFKern<float>;

  //=========================================================================//
  // "Grid2D::Quadrature2D" with "ReturnsPDFKern" instances:                 //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // With "double":                                                          //
  //-------------------------------------------------------------------------//
  template void Grid2D<double>::Quadrature2D<ReturnsPDFKern<double>>
  (
    ReturnsPDFKern<double> const& kern,
    double const*                 f,
    int                           rsize,
    double*                       res
  )
  const;

  template double Grid2D<double>::Quadrature2D<ReturnsPDFKern<double>>
  (
    ReturnsPDFKern<double> const& kern,
    double const*                 f
  )
  const;

  template void Grid2D<double>::Quadrature2D<ReturnsPDFKern<double>>
  (
    ReturnsPDFKern<double> const& kern,
    double const*                 f,
    Vector<double>*               res
  )
  const;

  //-------------------------------------------------------------------------//
  // With "float":                                                           //
  //-------------------------------------------------------------------------//
  template void Grid2D<float>::Quadrature2D<ReturnsPDFKern<float>>
  (
    ReturnsPDFKern<float> const&  kern,
    float const*                  f,
    int                           rsize,
    float*                        res
  )
  const;

  template float Grid2D<float>::Quadrature2D<ReturnsPDFKern<float>>
  (
    ReturnsPDFKern<float> const&  kern,
    float const*                  f
  )
  const;

  template void Grid2D<float>::Quadrature2D<ReturnsPDFKern<float>>
  (
    ReturnsPDFKern<float> const&  kern,
    float const*                  f,
    Vector<float>*                res
  )
  const;
}
