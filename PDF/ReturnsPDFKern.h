// vim:ts=2:et
//===========================================================================//
//                             "ReturnsPDFKern.h":                            //
//   Integration Kernel for Returns PDF Computed from PDF(S,V) and N(0,dt)
//===========================================================================//
// NB: This class needs to be compilable by both the host compiler and NVCC, so
// it cannot contain any standard library refs (in CUDA mode):
//
#pragma once

#include "Common/Macros.h"

#ifndef  __CUDACC__
#include "Grids/Grid2D.h"
#endif

namespace Arbalete
{
  //=========================================================================//
  // "ReturnsPDFKern":                                                       //
  //=========================================================================//
  // NB: This class has a non-const "Update" method:
  //
  template<typename F>
  class ReturnsPDFKern
  {
  private:
    // Params ("x"s) area:
    // There could be either a single "x" stored on this object directly,   or
    // a ptr to Host or CUDA array holding multiple "x"s; "m_oneX" is a switch
    // indicating which value(s) to use:
    //
    bool  m_oneX;
    F     m_X;
    F*    m_Xs;
    F*    m_cudaXs;
    int   m_nXs;          // Length of "m_Xs" and "m_cudaXs"

    // Other Data area:
    // [dty, A=S/sigma1(S,t), B=mu(S,t)/sigma1(S,t), C=sigma2(V,t)]
    // in Host and possibly in CUDA memory:
    F*    m_data;
    F*    m_cudaData;

    // Offsets of data area components:
    int   m_dataLen;
    int   m_DToff;
    int   m_Aoff;
    int   m_Boff;
    int   m_Coff;

    // Scratch vectors for diffusion coeffs (Host only). NB: "sigS2" is part of
    // "m_data" (same as the "C" vector):
    F*    m_muS;
    F*    m_sigS1;
    F*    m_muV;
    F*    m_sigV;

    // Default ctor is hidden:
    ReturnsPDFKern();

  public:
#   ifndef __CUDACC__
    // NB: This part is not seen by the NVCC compiler:

    //-----------------------------------------------------------------------//
    // Non-Default Ctor and Dtor:                                            //
    //-----------------------------------------------------------------------//
    // NB: The Grid ptr is not owned or stored by the "ReturnsPDFKern" object.
    // Sets the initial diffusion coeffs from "StartTime()", dt=0    (INVALID,
    // must be overridden by Update() before use) and x=0:
    //
    ReturnsPDFKern(Grid2D<F> const& grid);

    // XXX: The Dtor is explicit -- must be invoked from a containing object,
    // with the same "grid" arg:
    void Destroy (Grid2D<F> const& grid);

    //-----------------------------------------------------------------------//
    // "SetTimeInterval":                                                    //
    //-----------------------------------------------------------------------//
    // Sets the interval over which the PDF of returns is computed to (t,t+dt).
    // NB: The interval must NOT cross regime-switching points of the underly-
    // ing Diffusion:
    //
    void SetTimeInterval
    (
      Grid2D<F> const& grid, DateTime t, Time dt, TradingCalendar::CalReg reg
    );

    //-----------------------------------------------------------------------//
    // Params Management:                                                    //
    //-----------------------------------------------------------------------//
    void SetX (F X);
    void SetXs(Grid2D<F> const& grid, F xFrom, F xTo, int nXs);
#   endif

    // NB: The following part is seen by both Host and NVCC compilers:
    //
    DEVICE int      GetNParams() const
      { return m_oneX ? 1 : m_nXs; }

    DEVICE F const* GetParams () const
    {
      return m_oneX ? &m_X :
#       ifndef __CUDACC__
        m_Xs;
#       else
        m_cudaXs;
#       endif
    }

    //-----------------------------------------------------------------------//
    // Evaluation:                                                           //
    //-----------------------------------------------------------------------//
    DEVICE F operator()(int i, int j, F x) const;
  };
}
