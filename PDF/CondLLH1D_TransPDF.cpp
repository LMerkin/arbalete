// vin:ts=2:et
//===========================================================================//
//                        "CondLLH1D_TransPDF.cpp":                          //
//             Pre-Defined Instances of "CondLLH1D_TransPDF":                //
//===========================================================================//
#include "PDF/CondLLH1D_TransPDF.hpp"

namespace Arbalete
{
  template class CondLLH1D_TransPDF<double>;
  template class CondLLH1D_TransPDF<float>;
}
