// vim:ts=2:et
//===========================================================================//
//                            "CondLLH1D_TransPDF.h":                        //
// Conditional Log-LikeliHood Function Using Transition PDF on 1D Diffusions //
//===========================================================================//
// NB: This is similar to "CondLLH2D_BayesOptFilt" in the sense that CondLLH is
// computed directly from (analytical or approximated) TransPDF, w/o solving
// the Fokker-Planck PDE. However, in 1D case the whole Markovian process is
// observable, so there is no need for any filtering:
//
#pragma once

#include "Common/Maths.hpp"
#include "Common/CUDAEnv.h"
#include "Common/TradingCalendar.h"
#include "PDF/CondLLH.h"
#include <memory>
#include <string>

namespace Arbalete
{
  //=========================================================================//
  // "CondLLH1D_TransPDF" Class:                                             //
  //=========================================================================//
  template<typename F>
  class CondLLH1D_TransPDF final: public CondLLH<F>
  {
  private:
    std::shared_ptr<TradingCalendar>  m_cal;

    CondLLH1D_TransPDF();      // Default ctor is hidden

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor, Dtor:                                               //
    //-----------------------------------------------------------------------//
    // Ctor Args:
    // diff_params: as in the base class ctor;
    // mkt_data:    as in the base class ctor:
    // debug_level:
    // 0:   No debugging output at all
    // 1:   Reserved for top-level Calibrator
    // 2:   Summary at conditioning points
    //
    CondLLH1D_TransPDF
    (
      Diffusion_Params<F>              const& diff_params,  // Stub
      std::vector<MDItem<F>>           const& mkt_data,
      std::shared_ptr<TradingCalendar> const& cal,          // May be NULL
      int                                     debug_level = 0
    )
    : CondLLH<F>(diff_params, mkt_data, debug_level),
      m_cal(cal)
    {}

    // Dtor:
    ~CondLLH1D_TransPDF() {}

    //-----------------------------------------------------------------------//
    // Evaluation:                                                           //
    //-----------------------------------------------------------------------//
    // Implementation of abstract functions from the base class:
    //
    void NLLH
    (
      Vector<F> const& diff_coeffs,
      F*               val,
      Vector<F>*       gradient = nullptr, // MUST BE NULL
      Matrix<F>*       hessian  = nullptr, // MUST BE NULL
      F*               vs       = nullptr  // N/A, must be NULL
    )
    const override;

    // CUDA Timing: XXX: None as yet:
    //
    F GetCUDATiming() const override { return F(0.0); }
  };
}
