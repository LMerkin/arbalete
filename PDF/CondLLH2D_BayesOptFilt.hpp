// vim:ts=2:et
//===========================================================================//
//                        "CondLLH2D_BayesOptFilt.hpp":                      //
//    Conditional Log-LikeliHood Function Using Bayesian Optimal Filtering   //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Common/DateTime.h"
#include "Diffusions/Diffusion2D_Fact.h"
#include "Grids/Grid2D_BayesOptFilt.hpp"  // Re-use some internals of it...
#include "PDF/CondLLH2D_BayesOptFilt.h"
#include <cstdio>
#include <iostream>
#include <stdexcept>
#include <cassert>

namespace
{
  using namespace Arbalete;
  using namespace std;

  // Stop value to indicate immediate exit from Grid Time Marshalling:
  Time  StopNow = TimeGener::seconds(-1);
}

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "CondLLH2D_BayesOptFilt": Non-Default Ctor:                             //
  //=========================================================================//
  // NB: This ctor does NOT create the Diffusion, Grid and PDF objects -- this
  // cannot be done before the Diffusiion Coeffs are knowm.   It just verifies
  // and memoises all params (and MktData):
  //
  template<typename F>
  CondLLH2D_BayesOptFilt<F>::CondLLH2D_BayesOptFilt
  (
    Diffusion_Params<F>        const&   diff_params,
    typename Grid2D<F>::Params const&   grid_params,
    vector<MDItem<F>>          const&   mkt_data,
    int                                 debug_level
  )
  : CondLLH<F>  (diff_params, mkt_data, debug_level),
    m_gridParams(MkBOFParams<F>(grid_params, diff_params.m_S0))
  {
    // Modify "m_gridParams" further:
    // The time span for "V" is the full time length of the Mkt Data used:
    //
    int nk = this->GetMDLen();
    if (nk >= 2)
      m_gridParams.m_TV = this->m_mdTimes[nk-1] - this->m_mdTimes[0];
    else
      // This is likely to be an error, as "m_TV" will remain 0, and will
      // cause an error in Grid construction:
      throw runtime_error("CondLLH2D_BayesOptFilt Ctor: Mkt Data too short?");

    // Debugging mode: NB: For Grids, the Debug levels are different:
    //
    switch (this->m_debugLevel)
    {
    case 0:
      m_gridParams.m_debugLevel = 0;
      break;
    case 1:
    case 2:
      m_gridParams.m_debugLevel = 1;
      break;
    case 3:
    case 4:
      m_gridParams.m_debugLevel = 2;
      break;
    default:
      throw invalid_argument
            ("CondLLH2D_BayesOptFilt Ctor: Invalid DebugLevel");
    }
    // Install info on typical time steps in the Grid (for reasoning about the
    // Grids stability etc). We are probably interested in MINIMAL intervals
    // (as they exacerbate the delta-function-related instabilities):
    //
    Time minTH = ZeroTime();
    Time minN  = ZeroTime();

    for (int i = 0; i < nk - 1; ++i)
    {
      Time   dt = this->m_mdTimes[i+1] - this->m_mdTimes[i];
      assert(dt > ZeroTime());

      if (this->m_mdRegs[i]  == TradingCalendar::TradingHours)
      {
        if (minTH == ZeroTime() || dt < minTH)
          minTH = dt;
      }
      else
      {
        if (minN  == ZeroTime() || dt < minN)
          minN  = dt;
      }
    }
    m_gridParams.m_etTH = minTH;
    m_gridParams.m_etN  = minN;
  }

  //=========================================================================//
  // Params Construction: Helper Functions:                                  //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // "MkGridParamsStub":                                                     //
  //-------------------------------------------------------------------------//
  // NB: m_TS, m_TV, m_debug remain uninitialised;
  //     m_NSigmas{S|V} remain at their default vals (e.g. 7.0);
  //     m_{Lo|Up}{S|V} remain at their default (ignored) vals;
  // XXX: same default time step is applied to all Regimes; if required, this
  //     can later be modified explicitly on the constructed object:
  //
  template<typename F>
  typename Grid2D<F>::Params
    CondLLH2D_BayesOptFilt<F>::MkGridParamsStub
  (
    int n, F S0, F loV, F upV,
    shared_ptr<CUDAEnv>         const& cudaEnv,
    shared_ptr<TradingCalendar> const& cal
  )
  {
    typename Grid2D<F>::Params res;
    res.m_n         = n;
    res.m_LoV       = loV;
    res.m_UpV       = upV;
    res.m_cudaEnv   = cudaEnv;
    res.m_cal       = cal;
    // All other flds are set to their default values, and can be modified by
    // the caller

    // Project "res" to the set of params specifically used for "Grid2D_Bayes-
    // OptFilt" (XXX: the "CondLLH2D_BayesOptFilt" ctor and the Grid ctor will
    // do this several more times again -- this is a minor inefficiency price
    // for getting a consistent external view):
    //
    return MkBOFParams<F>(res, S0);
  }

  //=========================================================================//
  // Evaluation:                                                             //
  //=========================================================================//
  template<typename F>
  void CondLLH2D_BayesOptFilt<F>::NLLH
  (
    Vector<F> const& diff_coeffs,
    F*               val,
    Vector<F>*       gradient,
    Matrix<F>*       hessian,
    F*               vs
  )
  const
  {
    // Check the args  :
    assert(val != nullptr);
    if (gradient != nullptr || hessian != nullptr)
      throw invalid_argument
            ("CondLLH2D_BayesOptFilt::NLLH: Gradient and Hessian are not "
             "supported");

    // Install "diff_coeffs" in the "DiffParams" (contain at least "V0"):
    assert(!diff_coeffs.empty());
    this->m_diffParams.m_coeffs = diff_coeffs;

    // Construct the Diffusion:
    shared_ptr<Diffusion2D<F>> diff(MkDiffusion2D<F>(this->m_diffParams));

    // Construct the special Grid -- of "BayesOptFilt" type:
    Grid2D_BayesOptFilt<F>   gridBOF(diff, this->m_gridParams);

    // LLH is a sum of logs of conditional PDFs over all conditioning points,
    // so initialise it to 0:
    *val = F(0.0);

    // Initialise the "vs" output vector (MUST already be allocated!):
#   ifndef NDEBUG
    int nk = this->GetMDLen();
    assert(nk >= 2);
#   endif
    if (vs != nullptr)
      vs[0] = diff_coeffs[0];   // "V0" is always at coeffs[0]

    //-----------------------------------------------------------------------//
    // Grid Call-Back:                                                       //
    //-----------------------------------------------------------------------//
    typename Grid2D<F>::CallBack call_back
    (
#     ifdef NDEBUG
      [this, val, vs, &gridBOF]
#     else
      [this, val, vs, nk, &gridBOF]
#     endif
        (Grid2D<F> const&        DEBUG_ONLY(grid),  DateTime t,  int stk,
         F* UNUSED_PARAM(f),     DateTime next_t,
         TradingCalendar::CalReg UNUSED_PARAM(reg))
      ->Time
      {
        assert(&grid == &gridBOF);

        // IMPORTANT: Each point must be a conditioning point, in particular we
        // must have market data for all regime switching points:
        if (stk <  0)
          throw runtime_error
            ("CondLLH2D_BayesOptFilt::(): Must be a cond point: "+
             DateTimeToStr(t));

        assert(0 <= stk && stk <= nk-1 && this->m_mdTimes[stk] == t);

        if (t != next_t)
        {
          // This is NOT the last point on the Grid, so set the next condition-
          // ing point (for "next_t")  which the Grid will use after returning
          // from this call-back:
          assert(t < next_t && 0 <= stk && stk <= nk-2);

          // NB: In this case, "AdjustSx" does NOT use a time interval to the
          // next cond point. Place the next conditioning point on the Grid:
          //
          gridBOF.AdjustSx(t, this->m_mdS[stk+1], Time());
        }

        if (stk >= 1)
        {
          // Get the density from the last step (ending at "t"):
          F maxP = gridBOF.GetMaxP();
          F optV = gridBOF.GetOptV();
          F N    = gridBOF.GetDenomP();

          F      NLe;
          bool   ok;

          if (maxP > F(0.0))
          {
            assert(maxP > F(0.0));
            NLe  =  - Log(maxP);  // NB: "-=" because of Negated LLH:
            *val += NLe;

            if (vs != nullptr)
              vs[stk] = optV;
            ok = true;
          }
          else
          if (maxP == F(0.0))
          {
            // This may happen -- eg, if we did conditioning far away from the
            // PDF, we can get maxP==0. Then LH=0, LLH=-oo, NLLH=+oo, so there
            // is no point in continuing NLLH computation: the curr params are
            // sub-optimal anyway, and in addition, the normalised CondPDF may
            // become NaN:
            //
            NLe  = *val = Inf<F>();
            ok   = false;   // No point in continuing the NLLH computation...
          }
          else
          {
            // "maxP" is negative or not finite:
            NLe  = *val = NaN<F>();
            ok   = false;   // As above
          }

          // Debugging Summary at Cond Point (ALWAYS generated if there was an
          // evaluation error):
          if (this->m_debugLevel >= 2 || !ok)
          {
            // The last used conditioning point (NB: at the last time instant,
            // there was no "SY" shift above):
            F SY   = this->m_mdS[stk];

            assert((t <  next_t && SY == gridBOF.GetPrevSY()) ||
                   (t == next_t && SY == gridBOF.GetSY()) );

            cout << '\n' << t << ": SY=" << SY << ", OptV=" << optV
                 << ", MaxP=" << maxP    << ", DenomP="     << N
                 << ", NLLH=" << NLe     << ", CumNLLH="    << (*val) << endl;
          }

          // Terminate the Grid if the NLLH value being computed is already not
          // finite:
          if (!ok)
            return StopNow;
        }
        // Default time step is unchanged:
        return ZeroTime();
      }
    );

    //-----------------------------------------------------------------------//
    // Run the Fwd Integrator from "t0" to "ts2":                            //
    //-----------------------------------------------------------------------//
    // Time horison:
    DateTime t0  = this->m_mdTimes[0];
    DateTime ts2 = this->m_mdTimes.back();
    assert(gridBOF.StartTime() == t0 && gridBOF.EndTime() == ts2);

    // "p": a host-based solution space (only required as a placeholder). Init-
    // ialse it to all-0s (which is clearly invalid -- not normalised to 1, but
    // will do):
    int n = gridBOF.n();
    assert(n >= 2);
    Vector<F> p(n);
    ZeroOut<F>(&p);

    // The result is accumulated in "*val" via the call-back:
    gridBOF.RunFwd(t0, &p, ts2, &call_back, &(this->m_mdTimes));
  }
}
