// vim:ts=2:et
//===========================================================================//
//                         "CondLLH2D_BayesOptFilt.cpp":                     //
//              Pre-Defined Instances of "CondLLH2D_BayesOptFilt"            //
//===========================================================================//
#include "PDF/CondLLH2D_BayesOptFilt.hpp"

namespace Arbalete
{
  template class  CondLLH2D_BayesOptFilt<double>;
  template class  CondLLH2D_BayesOptFilt<float>;
}
