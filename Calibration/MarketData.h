// vim:ts=2:et
//===========================================================================//
//                             "MarketData.h":                               //
//       Loading Market Data and Constructing Descriptive Statistics:        //
//===========================================================================//
#pragma once

#include "Common/DateTime.h"
#include "Common/TradingCalendar.h"
#include "Calibration/StatsSV.h"
#include <vector>
#include <string>
#include <memory>

namespace Arbalete
{
  //=========================================================================//
  // Market Data and Their Transforms:                                       //
  //=========================================================================//
  template<typename F>
  struct MDItem
  {
    DateTime                m_t;    // Time instant
    TradingCalendar::CalReg m_reg;  // Regime at "t" (or from "t" on, ie Fwd)
    F                       m_val;  // Value: price, IR or similar
  };

  //-------------------------------------------------------------------------//
  // "MDTransform":                                                          //
  //-------------------------------------------------------------------------//
  // Base struct for "LoadMktData" call-back function types. Each function may
  // transform "mdi" and/or attach it to "mdis", or skip it,   and / or modify
  // items already stored in "mdis":
  //
  template<typename F>
  struct MDTransform
  {
    mutable StatsAccumulatorSV<F> m_statsAcc;

    MDTransform(int stats_acc_param)
    : m_statsAcc(stats_acc_param)
    {}
    // NB: Then the default ctor is not generated;  but this class is abstract
    // anyway -- sub-classes are defined in the implementation and returned by
    // the factory function below.

    // Also need a virtual dtor:
    virtual ~MDTransform() {}

    virtual void operator()
      (MDItem<F> const& mdi, std::vector<MDItem<F>>* mdis) = 0;
  };

  // Factory of "MDTransform"s:
  // NB: Actual sub-classes  are not exposed by this header:
  // For currently recognised names, see the implementation;
  // "stats_acc_param" is actually the window size for the vol-of-vol
  //    estimation, passed to the "StatsAccumulatorSV" ctor:
  //
  template<typename F>
  MDTransform<F>* MkMDTransform
    (std::string const& mdtr_name, int stats_acc_param);

  //=========================================================================//
  // Loading Market Data from a File:                                        //
  //=========================================================================//
  // Args:
  // md_file:     market data file, line format: [TimeStamp Price];
  // t0 :         start time of the Diffusion and Grid to be used;
  // ts1:         start of market data sample to be used;
  // dts:         time step of market data sample;
  // ts2:         end   of market data sample to be used;
  // NB: "ts1", "ts2" do not need to correspond to time stamps in "md_file"
  //     exactly (they will just select a subset of data from that file);
  //     HOWEVER, "t0" MUST be on file, and t0 <= ts1 <= ts2, dts >= 0;
  // NB: All times are in GMT; time stamps on "md_file" are assumed to be
  //     in GMT as well.
  // NB: "mdtr" can be NULL; if not NULL, it is invoked only for tCurr in
  //     [t0..ts2]:
  //
  template<typename F>
  void LoadMktData
  (
    std::string const&      md_file,
    DateTime                t0,
    DateTime                ts1,
    Time                    dts,
    DateTime                ts2,
    std::shared_ptr<TradingCalendar> const& cal,
    MDTransform<F>*         mdtr, // May be modified by (), hence not "const"
    std::vector<MDItem<F>>* res
  );

  //=========================================================================//
  // Constructing Market Data from a TimeLine and values:                    //
  //=========================================================================//
  // XXX: This function is primarily intended for creating "synthetic"  MktData
  // for testing purposes. Arg2 ("vals") is deliberately made a raw ptr; it can
  // eg point to a Vector or a Matrix column. The caller is responsible for ma-
  // king sure that "t0" is indeed the correct starting point for "timeLine",
  // and that "vals" are of sufficient length  (at least same as "timeLine"):
  //
  template<typename F>
  void ZipMktData
  (
    DateTime                        t0,
    std::vector<TimeNode<F>> const& timeLine,  // Of some length "L"
    F const*                        vals,      // Of size >= L
    std::vector<MDItem<F>>*         res        // Will be auto-resized
  );
}
