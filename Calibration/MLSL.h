// vim:ts=2:et
//===========================================================================//
//                                 "MLSL.h":                                 //
//               Multi-Level Single Linkage Optimisation (Driver)            //
//===========================================================================//
#pragma once

#include "Common/Macros.h"
#include <functional>
#include <vector>
#include <climits>

namespace Arbalete
{
  //=========================================================================//
  // "MLSL" namespace (could also be a class with static members):           //
  //=========================================================================//
  namespace MLSL
  {
    //-----------------------------------------------------------------------//
    // Return Codes:                                                         //
    //-----------------------------------------------------------------------//
    // Same codes are used by:
    // (*) "LocalMinimiser" call-back (see below), including single evaluations
    //     of the Cost Function;
    // (*) top-level "Minimise" itself;
    // the actual return code may be a logical OR of the codes listed below:
    //
    int const SUCCESS     = 0;
    int const FAILURE     = 1;
    int const FORCED_STOP = 2;  // Stop on request from the user call-back 

    //-----------------------------------------------------------------------//
    // "LocalMinimiser": A Callable Object:                                  //
    //-----------------------------------------------------------------------//
    // Invoked from the "Minimise" driver. Replaces the curr point "xs"  with a
    // locally-optimised solution (in the trivial case, or if the "full_minimi-
    // sation" flag is not set, it leaves "xs" unchanged) and computes the Cost
    // Function value fmin" in the locally-optimal point. Therefore,  the Call-
    // able Object must in particular contain the Cost Function:
    //
    // If the "full_minimisation" flag is NOT set, the MLSL driver expects that
    // this function may be invoked ASYNCHRONOUSLY, so it does not immediately
    // catch possible exceptions in it -- the function itself must do so,  and
    // convert exceptions into the "rc" code. Othrerwise, SYNCHRONOUS invocati-
    // on is assumed, with full exception handling within the MLSL driver:
    //
    typedef
      std::function
        <void(int n, double* xs, double* fmin, int* rc, bool full_minimisation)>
      LocalMinimiser;

    //-----------------------------------------------------------------------//
    // MLSL Minimiser (Driver):                                              //
    //-----------------------------------------------------------------------//
    // XXX: Currently, the only stopping param for this func is "max_elems":
    //
    int Minimise
    (
      LocalMinimiser const& LM,   // Contains the Cost Function
      int                   n,    // Number of dims: size of "lbs", "ubs", "xs"
      double const*         lbs,  // Lower bounds
      double const*         ubs,  // Upper bounds
      double*               xs,   // Initial guess -> replaced by solution
      double*               minf, // Minimal Cost Function value
      int    max_rounds,          // Max # of sampling / minimisation rounds
      int    sample_size  = 10,   // Size of each sample
      int    max_best     =  2,   // Max # of best pts to descend, per sample
      int    debug_level  =  0
    );
  }
}
