// vim:ts=2:et
//===========================================================================//
//                                "StatsSV.cpp":                             //
//                Pre-Defined Instances of "StatsAccumulatorSV":             //
//===========================================================================//
#include "Calibration/StatsSV.hpp"

namespace Arbalete
{
  template class StatsAccumulatorSV<double>;
  template class StatsAccumulatorSV<float>;
}
