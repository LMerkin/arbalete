// vim:ts=2:et
//===========================================================================//
//                            "MarketData.hpp" :                             //
//       Loading Market Data and Constructing Descriptive Statistics:        //
//===========================================================================//
#pragma once

#include "Calibration/MarketData.h"
#include <stdexcept>

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "LoadMktData":                                                          //
  //=========================================================================//
  template<typename F>
  void LoadMktData
  (
    string const&         md_file,
    DateTime              t0,
    DateTime              ts1,
    Time                  dts,
    DateTime              ts2,
    shared_ptr<TradingCalendar> const&  cal,
    MDTransform<F>*       mdtr,
    vector<MDItem<F>>*    res
  )
  {
    assert(res != nullptr);
    res->clear();

    if (t0 > ts1 || ts1 > ts2 || dts.is_negative())
      throw invalid_argument
            ("LoadMktData: Invalid time range: t0="   + DateTimeToStr(t0)  +
             ", ts1=" + DateTimeToStr(ts1) + ", ts2=" + DateTimeToStr(ts2));

    // Read the file and fill in the Market Data:
    // Expected lines format:
    // YYYY-MM-DD_hh:mm:ss PRICE
    //
    FILE* f = fopen(md_file.c_str(), "r");
    if (f == nullptr)
      throw runtime_error("LoadMktData: Invalid MktData file: "+ md_file);

    // Time stamps to be added:
    bool     t0_found = false;

    // All data items in [t0 .. ts2]:
    vector<MDItem<F>> mdis;

    // Next regime changing point (XXX: these initialisations are formal; the
    // actual vals are computed at t0):
    //
    DateTime nextRCh = t0;
    TradingCalendar::CalReg reg = TradingCalendar::TradingHours;

    //-----------------------------------------------------------------------//
    // File Reading Loop:                                                    //
    //-----------------------------------------------------------------------//
    while (true)
    try
    {
      //---------------------------------------------------------------------//
      // Get the next data line:                                             //
      //---------------------------------------------------------------------//
      int year, month, date, hh, mm, sec;
      float px; // Required for "fscanf"

      int rc = fscanf(f, "%04d-%02d-%02d_%02d:%02d:%02d\t%f\n",
                      &year, &month, &date, &hh, &mm, &sec, &px);
      if (rc == EOF)
        break;

      if (rc != 7)
        throw runtime_error("LoadMktData: Invalid MktData format");

      // The curr time stamp and price:
      DateTime t
        (Date((unsigned short)(year),
              (unsigned short)(month),
              (unsigned short)(date)),
         Time(hh, mm, sec));
      F St   = F(px);

      if (t < t0)
        continue;
      if (t > ts2)
        break;

      //---------------------------------------------------------------------//
      // Initial point (t0)?                                                 //
      //---------------------------------------------------------------------//
      if (t == t0)
      {
        if (t0_found || !res->empty())
          // "t0" found again? non-ascending time stamps? some data preceding
          // "t0" already in?
          throw runtime_error("LoadMktData: Unordered data");

        t0_found = true;

        // Initialise the Regime; "reg" is valid from "t0" until "nextRCh":
        GetNextFwdEvent (t0, cal.get(), &nextRCh, &reg);
        assert(nextRCh > t0);
      }
      else
      {
        assert(t0 < t && t <= ts2);
        if (!t0_found)
          throw runtime_error("LoadMktData: Missing t0 point");
      }

      //---------------------------------------------------------------------//
      // Generic Point:                                                      //
      //---------------------------------------------------------------------//
      if (t > nextRCh)
        // This must not happen: this means that "t" over-stepped the next
        // regime changing point, i.e. there were no data in such a point.
        // This is currently not allowed:
        throw runtime_error("LoadMktData: Missing data at regime change");
      else
      if (t == nextRCh)
        // "t" is a regime-switching point: Advance "nextRCh" and re-compute
        // "reg":
        GetNextFwdEvent(t, cal.get(), &nextRCh, &reg);
      else
        // "t" is NOT a regime-switching point: XXX: if it occurs outside of
        // trading hours, we ignore such a data point as unreliable; "t0" is
        // excluded from this rule because (a) it is needed anyway;   (b) in
        // that point "nextRC" is first defined and indeed refers to the NEXT
        // point, so this check would be wrong:
        //
        if (reg != TradingCalendar::TradingHours && t != t0)
          continue;

      // Make the data item (with the Fwd "reg"):
      MDItem<F> mdi = { t, reg, St };

      // Invoke the call-back if present, which may modify the item and/or the
      // list! E.g. it may compute a return or IR from "mdi" and attribute the
      // value to the PREVIOUS time stamp:
      //
      if (mdtr != nullptr)
        (*mdtr)(mdi, &mdis);
      else
        // Attach the item unmodified:
        mdis.push_back(mdi);
    }
    catch(...)
    {
      // Do clean-up, and re-throw the exception:
      fclose(f);
      throw;
    }
    fclose(f);

    //-----------------------------------------------------------------------//
    // Now process "t0" and conditioning points:                             //
    //-----------------------------------------------------------------------//
    // NB: This could not have been done in the file-reading loop because the
    // user-provided Transform may modify previously-installed items in "mdis"
    // (e.g. if Rate(i) is installed once Price(i+1) becomes known), so their
    // final vals are availablle only now:
    //
    if (mdis.empty() || mdis[0].m_t != t0)
      throw runtime_error("LoadMktData: MDIs must begin with t0");

    res->push_back(mdis[0]);

    // Next conditioning point: the nearest of "ts1" and the next regime-swtch
    // (XXX: so "ts1" itself may NOT get on the final list, actually - this is
    // only a suggestion:
    //
    GetNextFwdEvent(t0, cal.get(),  &nextRCh, &reg);
    DateTime nextCond = Min<DateTime>(ts1, nextRCh);
    assert  (nextCond > t0);

    int L = int(mdis.size());

    for (int i = 1; i < L; ++i)
    {
      MDItem<F> const& mdi = mdis[i];

      // Skip all "mdis" which come strictly BEFORE "nextCond". If "nextCond"
      // is actually a regime-switching point, there MUST be an "mdi" exactly
      // there (not after):
      assert(nextCond <= nextRCh);

      if (mdi.m_t >= nextCond)
      {
        if (nextCond == nextRCh && mdi.m_t != nextCond)
          throw runtime_error
                ("LoadMktData: NextRegimeSwitch = "+ DateTimeToStr(nextRCh) +
                 " but NextMktData = "             + DateTimeToStr(mdi.m_t));

        // If the above check is OK, add the Market Data item to "res":
        res->push_back(mdi);

        // Align "nextCond" woth "mdi" (e.g., to get same "nextCond"s  every
        // day -- otherwise they would siffer from phase shifts); it will be
        // advanced later:
        nextCond = mdi.m_t;

        if (i == L-1)
          break;    // Nothing more to do!

        // OTHERWISE:
        // Move "nextCond" fwd as many times as necessary to surpass "mdi.m_t"
        // (but not beyond the next Regime Switching point):
        //
        GetNextFwdEvent(mdi.m_t, cal.get(), &nextRCh, &reg);

        // NB: If dts = 0, then "nextCond" will be left behind and ALL "mdis"
        // installed as cond points:
        if (dts > ZeroTime())
          while (nextCond <= mdi.m_t)
            nextCond += dts;
        else
          nextCond = mdis[i+1].m_t;

        assert(nextCond > mdi.m_t && nextRCh > mdi.m_t);

        // Thus, if we take min of "nextCond" and "nextRCh", it will still be
        // beyond mdi.m_t, as required:
        //
        nextCond = Min<DateTime>(nextCond, nextRCh);
        assert(nextCond > mdi.m_t);
      }
    }
    // All done!
  }

  //=========================================================================//
  // Standard Transforms for "LoadMktData":                                  //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // "TransformLogNormId":                                                   //
  //-------------------------------------------------------------------------//
  // Price (Spot / Fwd) is returned as is, statistics of *returns* is obtained
  // (assuming a log-normal price distribution).   Suitable for Equity, FX and
  // even Bonds with assumed log-normal dynamics:
  //
  template<typename F>
  struct TransformLogNormId final: public MDTransform<F>
  {
    //-----------------------------------------------------------------------//
    // Non-Default Ctor:                                                     //
    //-----------------------------------------------------------------------//
    TransformLogNormId(int stats_acc_param)
    : MDTransform<F>(stats_acc_param)
    {}

    //-----------------------------------------------------------------------//
    // Transform Itself:                                                     //
    //-----------------------------------------------------------------------//
    void operator()
      (MDItem<F> const& mdi, vector<MDItem<F>>* mdis) override
    {
      assert(mdis != nullptr && mdi.m_val > F(0.0));

      // Attach the item as is:
      mdis->push_back(mdi);

      // If not the first one, compute the return and update stats:
      int n = int(mdis->size());
      if (n >= 2)
      {
        MDItem<F> const& prev = (*mdis)[n-2];
        assert(prev.m_val > F(0.0));

        F ret   = Log(mdi.m_val / prev.m_val);
        Time dt = mdi.m_t - prev.m_t;

        // NB: use "prev":
        this->m_statsAcc.UpdateStats(prev.m_t, ret, dt, prev.m_reg);
      }
    }
  };

  //-------------------------------------------------------------------------//
  // "TransformBondPxToIR":                                                  //
  //-------------------------------------------------------------------------//
  // Bond Prices converted to Short Rates for which a normal distribution is
  // assumed to get the statistics:
  //
  template<typename F>
  class TransformBondPx2IR final: public MDTransform<F>
  {
  private:
    //-----------------------------------------------------------------------//
    // Tmp storage for an item which had not been converted into a rate yet: //
    //-----------------------------------------------------------------------//
    MDItem<F> m_tmp;
    bool      m_tmpOK;

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor:                                                     //
    //-----------------------------------------------------------------------//
    // There is no real item in "m_tmp" yet:
    //
    TransformBondPx2IR(int stats_acc_param)
    : MDTransform<F>(stats_acc_param),
      m_tmpOK(false)
    {}

    //-----------------------------------------------------------------------//
    // Transform Itself:                                                     //
    //-----------------------------------------------------------------------//
    void operator()
      (MDItem<F> const& mdi, vector<MDItem<F>>* mdis) override
    {
      assert(mdis != nullptr && mdi.m_val > F(0.0));

      // If there is an item in the "tmp" buffer, convert into a Short Rate
      // using the new "mdi", and attach it to the "mdis" list:
      //
      if (m_tmpOK)
      {
        Time dt  = mdi.m_t - m_tmp.m_t;
        F    dtf = YF<F>(dt);
        assert(m_tmp.m_val > F(0.0) &&  dtf > F(0.0));

        m_tmp.m_val = Log(mdi.m_val / m_tmp.m_val) / dtf; // Short Rate
        mdis->push_back(m_tmp);

        // If there is a prev rate already in "mdis", compute "dr" and update
        // the stats:
        int n  = int(mdis->size());
        if (n >= 2)
        {
          MDItem<F> const& prev = (*mdis)[n-2];
          F dr     = m_tmp.m_val - prev.m_val;
          Time dtr = m_tmp.m_t   - prev.m_t;

          // NB: use "prev":
          this->m_statsAcc.UpdateStats(prev.m_t, dr, dtr, prev.m_reg);
        }
      }
      // In any case, save "mdi" in "m_tmp":
      m_tmp   = mdi;
      m_tmpOK = true;
    }
  };

  //-------------------------------------------------------------------------//
  // Factory of Transforms:                                                  //
  //-------------------------------------------------------------------------//
  // Currently supported transforms:
  // "LogNormId"
  // "BondPx2IR"
  //
  template<typename F>
  MDTransform<F>* MkMDTransform
    (string const& mdtr_name, int stats_acc_param)
  {
    if (mdtr_name == "LogNormId")
      return new TransformLogNormId<F>(stats_acc_param);
    else
    if (mdtr_name == "BondPx2IR")
      return new TransformBondPx2IR<F>(stats_acc_param);
    else
      throw invalid_argument("MkMDTransform: Invalid transform: "+ mdtr_name);
  }

  //=========================================================================//
  // "ZipMktData":                                                           //
  //=========================================================================//
  template<typename F>
  void ZipMktData
  (
    DateTime                   t0,
    vector<TimeNode<F>> const& timeLine,
    F const*                   vals,
    vector<MDItem<F>>*         res
  )
  {
    assert(vals != nullptr && res != nullptr);
    int L = int(timeLine.size());
    res->resize(L);

    if (timeLine.empty())
      // XXX: We currently do not produce an error here:
      return;

    if (timeLine[0].m_secs != 0 || timeLine[0].m_ty != F(0.0))
      throw invalid_argument("ZipMktData: TimeLine does not start from 0");

    for (int i = 0; i < L; ++i)
    {
      MDItem<F>& md = (*res)[i];  // Alias!
      md.m_t   = t0 + TimeGener::seconds(timeLine[i].m_secs);
      md.m_reg = timeLine[i].m_reg;
      md.m_val = vals[i];
    }
  }
}
