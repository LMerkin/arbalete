// vim:ts=2:et
//===========================================================================//
//                          "RealCalibrator.cpp":                            //
//                        Some Pre-Defined Instances                         //
//===========================================================================//
#include "Calibration/RealCalibrator.hpp"

namespace Arbalete
{
  template class RealCalibrator<double>;
  template class RealCalibrator<float>;
}
