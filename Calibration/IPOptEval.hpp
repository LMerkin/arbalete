// vim:ts=2:et
//===========================================================================//
//                            "IPOptEval.hpp":                               //
//            An Interface between "RealCalibrator" and "IPOpt"              //
//===========================================================================//
#pragma once

#include <Common/Maths.hpp>
#include <Calibration/RealCalibrator.h>
#include <coin/IpTNLP.hpp>
#include <cstdlib>
#include <cstring>
#include <stdexcept>
#include <cassert>

namespace Arbalete
{
  using namespace Ipopt;
  using namespace std;

  //=========================================================================//
  // "IPOptEval" Class:                                                      //
  //=========================================================================//
  // XXX: This class is not CUDA-enabled (and is unlikely to be enabled in the
  // future, due to heavy virtualisation):
  //
  template<typename F>
  class IPOptEval final: public TNLP
  {
  private:
    //-----------------------------------------------------------------------//
    // Data Flds:                                                            //
    //-----------------------------------------------------------------------//
    RealCalibrator<F>     const*  m_calibr; // Non-NULL, NOT OWNED!
    CondLLH_Analytical<F> const*  m_llha;   // Alias!

    // Evaluation Buffers / Memoisation of Results:
    mutable bool           m_done;          // "finalize" was called
    mutable double         m_lastVal;
    mutable Vector<double> m_initX;         // Init cond, set by Ctor
    mutable Vector<double> m_lastX;         // Set by "finalize"
    mutable Vector<double> m_lastGradient;  // size = NV
    mutable Matrix<double> m_lastHessian;   // size = NV^2
    int                    m_debugLevel;

    IPOptEval();                            // Default Ctor: hidden

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor:                                                     //
    //-----------------------------------------------------------------------//
    IPOptEval
      (RealCalibrator<F> const* calibr, int NV, double* init, int debugLevel)
    : m_calibr      (calibr),
      m_llha        (nullptr),
      m_done        (false),
      m_lastVal     (Inf<F>()),
      m_initX       (NV),
      m_lastX       (NV),
      m_lastGradient(NV),
      m_lastHessian (NV, NV),
      m_debugLevel  (debugLevel)
    {
      assert(calibr != nullptr);
      if (NV != calibr->GetNV())
        throw invalid_argument("IPOptEval Ctor: Invonsistent NV values");

      // Memoise the initial conds. NB: BEWARE:  They are not necesserily the
      // same as initial conds from the Calibrator: the former are local ones
      // (eg set by current MLSL iteration) whereas the latter are global:
      //
      memcpy(&(m_initX[0]), init, NV * sizeof(double));

      // Get the ptr (alias) to the Analytical LLH: XXX: Currently, if the LLH
      // is analytical, it is the only one available:
      //
      vector<CondLLH<F> const*> const& llhs= calibr->GetLLHs();
      assert(llhs.size() == 1);

      m_llha = dynamic_cast<CondLLH_Analytical<F> const*>(llhs[0]);
      if (m_llha == nullptr)
        throw invalid_argument("IPOptEval Ctor: Not an Analytical LLH");
    }

    // Copy Ctor, Dtor: auto-generated (no memory management required)

  private:
    //-----------------------------------------------------------------------//
    // "get_nlp_info":                                                       //
    //-----------------------------------------------------------------------//
    bool get_nlp_info
    (
      Index& nv,        Index& m,       Index& nnz_jac_g,
      Index& nnz_h_lag, IndexStyleEnum& idx_style
    )
    override
    {
      nv         = m_calibr->GetNV();   // Number of variables
      assert(nv >= 1);
      m          = 0;                   // Number of constraints
      nnz_jac_g  = 0;                   // No constrs -> no Jacobian entries
      nnz_h_lag  = (nv * (nv + 1)) / 2; // Lower Triangle in Hessian
      idx_style  = TNLP::C_STYLE;       // C-style (0-based) indices
      return true;
    }

    //-----------------------------------------------------------------------//
    // "get_bounds_info":                                                    //
    //-----------------------------------------------------------------------//
    bool get_bounds_info
      (Index   nv,              Number* x_l,   Number* x_u,
       Index   DEBUG_ONLY  (m), Number* UNUSED_PARAM(g_l),
       Number* UNUSED_PARAM(g_u))
    override
    {
      // Bounds on variables -- come from the Calibrator (they will be the same
      // on all local sub-problems):
      //
      vector<double> const& lbs = m_calibr->GetBoundsLo();
      vector<double> const& ubs = m_calibr->GetBoundsUp();

      assert(int(lbs.size()) == nv && int(ubs.size()) == nv && x_l != nullptr &&
             x_u != nullptr  && m == 0);
      // So "g_l" and "g_u" are not used

      memcpy(x_l, &(lbs[0]), nv * sizeof(double));
      memcpy(x_u, &(ubs[0]), nv * sizeof(double));
      return true;
    }

    //-----------------------------------------------------------------------//
    // "get_starting_point":                                                 //
    //-----------------------------------------------------------------------//
    // XXX: Once again: It comes from a local sub-problem ("m_initX"), not from
    // the Calibrator global initial conds:
    //
    bool get_starting_point
    (
      Index   nv,                         bool    DEBUG_ONLY(init_x),
      Number* x,
      bool    DEBUG_ONLY  (init_z),       Number* UNUSED_PARAM(z_L),
      Number* UNUSED_PARAM(z_U),          Index   UNUSED_PARAM(m),
      bool    DEBUG_ONLY  (init_lambda),  Number* UNUSED_PARAM(lambda)
    )
    override
    {
      // The initial values are to be provided for all variables. Inequality or
      // equality constraints are not used, so the initial values for bound and
      // lambda multipliers should not be requested:
      //
      assert(init_x && x != nullptr && !init_z && !init_lambda &&
             int(m_initX.size()) == nv);

      memcpy(x, &(m_initX[0]), nv * sizeof(double));
      return true;
    }

    //-----------------------------------------------------------------------//
    // "DoEval":                                                             //
    //-----------------------------------------------------------------------//
    bool DoEval(Index nv, Number const* x, bool new_x)
    {
      assert(nv == m_calibr->GetNV() && x != nullptr);

      if (new_x)
        // Perform full new evaluation and memoise the results:
        try
        {
          int flag = 0;

          // XXX: Use ThreadID of 0 -- this is consistent with how "m_llha"
          // was extracted:
          m_calibr->EvalLLH
            (0, nv, x, &m_lastVal, &m_lastGradient, &m_lastHessian, &flag);

          if (flag != 0)
            return false;
        }
        catch(...)
          { return false; }

      // Successful evaluation, or no evaluation at all: so no failure:
      return true;
    }

    //-----------------------------------------------------------------------//
    // "eval_f": Cost Function Evaluation:                                   //
    //-----------------------------------------------------------------------//
    bool eval_f(Index nv, Number const* x, bool new_x, Number& obj_value)
    override
    {
      if (!DoEval(nv, x, new_x))
        return false;

      // Use the new / last cached value:
      obj_value = m_lastVal;
      return true;
    }

    //-----------------------------------------------------------------------//
    // "eval_grad_f": Gradient of Cost Function Evaluation:                  //
    //-----------------------------------------------------------------------//
    bool eval_grad_f(Index nv, Number const* x, bool new_x, Number* grad_f)
    override
    {
      assert(grad_f != nullptr);
      if (!DoEval(nv, x, new_x))
        return false;

      // Use the new / last cached Gradient:
      memcpy(grad_f, &(m_lastGradient[0]), nv * sizeof(double));
      return true;
    }

    //-----------------------------------------------------------------------//
    // "eval_g": Constraints Evaluation:                                     //
    //-----------------------------------------------------------------------//
    bool eval_g
      (Index   UNUSED_PARAM(n),      Number const* UNUSED_PARAM(x),
       bool    UNUSED_PARAM(new_x),  Index         DEBUG_ONLY(m),
       Number* UNUSED_PARAM(g))
    override
    {
      // We have no constraints, so this menthod should not be called at all:
      assert(m == 0);
      return false;
    }

    //-----------------------------------------------------------------------//
    // "eval_jac_g": Jacobian of Constrains Evaluation:                      //
    //-----------------------------------------------------------------------//
    bool eval_jac_g
    (
      Index  UNUSED_PARAM(n),        Number const* UNUSED_PARAM(x),
      bool   UNUSED_PARAM(new_x),    Index         DEBUG_ONLY(m),
      Index  UNUSED_PARAM(nele_jac), Index*        UNUSED_PARAM(iRow),
      Index* UNUSED_PARAM(jCol),     Number*       UNUSED_PARAM(values)
    )
    override
    {
      // We have no constraints, so this menthod should not be called at all:
      assert(m == 0);
      return false;
    }

    //-----------------------------------------------------------------------//
    // "eval_h": Hessian Evaluation:                                         //
    //-----------------------------------------------------------------------//
    bool eval_h
    (
      Index nv,                             Number  const* x,     bool new_x,
      Number obj_factor,                    Index   DEBUG_ONLY(m),
      Number const* DEBUG_ONLY(lambda),     bool    UNUSED_PARAM(new_lambda),
      Index         DEBUG_ONLY(nele_hess),  Index*  iRow,
      Index* jCol,                          Number* values
    )
    override
    {
      if (values != nullptr)
      {
        assert(iRow == nullptr && jCol == nullptr && x != nullptr && m == 0 &&
               nele_hess == (nv * (nv+1)) / 2);  // NB: "lambda" is irrelevant

        if (!DoEval(nv, x, new_x))
          return false;

        // Use the new / last cached Hessian: copy the lower triangle over,
        // multiplied by "obj_factor:
        //
        double* targ = values;
        for (int i = 0; i < nv; ++i)
          for (int j = 0; j <= i; ++j, ++targ)
            *targ = obj_factor * m_lastHessian(i,j);
        assert(targ ==  values + nele_hess);
      }
      else
      {
        // Fill in the indices of the lower triangle (0-based):
        assert(iRow   != nullptr && jCol != nullptr && x == nullptr &&
               lambda == nullptr);

        int k = 0;
        for (int i = 0; i < nv; ++i)
          for (int j = 0; j <= i; ++j, ++k)
          {
            iRow[k] = i;
            jCol[k] = j;
          }
      }
      return true;
    }

    //-----------------------------------------------------------------------//
    // "finalize_solution":                                                  //
    //-----------------------------------------------------------------------//
    // XXX: This is NOT a finalisation method. Rather, it reports the solution
    // achieved (or the last approximation):
    //
    void finalize_solution
    (
      SolverReturn                UNUSED_PARAM(status),
      Index                       nv,
      Number const*               x,
      Number const*               UNUSED_PARAM(z_L),
      Number const*               UNUSED_PARAM(z_U),
      Index                       DEBUG_ONLY(m),
      Number const*               UNUSED_PARAM(g),
      Number const*               UNUSED_PARAM(lambda),
      Number                      obj_value,
      IpoptData const*            UNUSED_PARAM(ip_data),
      IpoptCalculatedQuantities*  UNUSED_PARAM(ip_cq)
    )
    override
    {
      assert(nv == m_calibr->GetNV() && m == 0 && x != nullptr &&
             nv == int(m_lastX.size()));

      // Save the solution:
      m_lastVal = obj_value;
      memcpy(&(m_lastX[0]), x, nv * sizeof(double));
      m_done    = true;
    }

  public:
    //-----------------------------------------------------------------------//
    // "GetSolution":                                                        //
    //-----------------------------------------------------------------------//
    // Pre-condition: "xs" must be of size "NV":
    //
    void GetSolution(int nv, double* xs, double* fmin) const
    {
      assert(xs != nullptr && fmin != nullptr && nv == m_calibr->GetNV());

      if (!m_done)
        throw runtime_error("IPOPtEval: Not yet done");

      memcpy(xs, &(m_lastX[0]), nv * sizeof(double));
      *fmin = m_lastVal;
    }
  };
}
