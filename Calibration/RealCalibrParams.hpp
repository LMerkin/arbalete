// vim:ts=2:et
//===========================================================================//
//                         "RealCalibrParams.hpp":                           //
//                       Params for "RealCalibrator"                         //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Calibration/RealCalibrParams.h"
#include <boost/optional.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <cstdlib>
#include <stdexcept>

namespace
{
  //=========================================================================//
  // Utils:                                                                  //
  //=========================================================================//
  using boost::property_tree::ptree;
  using boost::optional;

  using namespace Arbalete;
  using namespace std;

  //-------------------------------------------------------------------------//
  // "ParseXMLInit":                                                         //
  //-------------------------------------------------------------------------//
  template<typename F>
  void ParseXMLInit
  (
    ptree const& iv,
    typename RealCalibrParams<F>::CIGType* type,
    F* val
  )
  {
    // Only one of the sub-elements listed below may actually be present:
    bool gotIt = false;

    for (ptree::const_iterator cit = iv.begin(); cit != iv.end(); ++cit)
    {
      string const& tag  = cit->first;

      // NB: For all tags below, "val" is irrelevant and remains unchanged:
      if (tag == "TrendS"  && !gotIt)
      {
        *type = RealCalibrParams<F>::CIGTrendS;
        gotIt = true;
      }
      else
      if (tag == "VolSTH"  && !gotIt)
      {
        *type = RealCalibrParams<F>::CIGVolSTH;
        gotIt = true;
      }
      else
      if (tag == "VarSTH"  && !gotIt)
      {
        *type = RealCalibrParams<F>::CIGVarSTH;
        gotIt = true;
      }
      else
      if (tag == "VolSON"  && !gotIt)
      {
        *type = RealCalibrParams<F>::CIGVolSON;
        gotIt = true;
      }
      else
      if (tag == "VolSWEH" && !gotIt)
      {
        *type = RealCalibrParams<F>::CIGVolSWEH;
        gotIt = true;
      }
      else
      if (tag != "<xmlcomment>")
        throw invalid_argument("ParseXMLInit: Invalid/Improper Tag: "+ tag);
    }
    // If none of the above applies, there must be a direct value:
    if (!gotIt)
    {
      *type = RealCalibrParams<F>::CIGExpl;
      *val  = iv.get_value<F>();
    }
  }

  //-------------------------------------------------------------------------//
  // "ParseXMLBounds":                                                       //
  //-------------------------------------------------------------------------//
  template<typename F>
  void ParseXMLBounds
  (
    ptree const& be,
    typename RealCalibrParams<F>::CBType* type,
    F* val
  )
  {
    // Only one of the sub-elements listed below may actually be present:
    bool gotIt = false;

    for (ptree::const_iterator cit = be.begin(); cit != be.end(); ++cit)
    {
      string const& tag  = cit->first;
      ptree  const& xval = cit->second;

      if (tag == "DeltaAbs")
      {
        *type = RealCalibrParams<F>::CBDeltaAbs;
        *val  = xval.get_value<F>();
        gotIt = true;
      }
      else
      if (tag == "DeltaRel" && !gotIt)
      {
        *type = RealCalibrParams<F>::CBDeltaRel;
        *val  = xval.get_value<F>();
        gotIt = true;
      }
      else
      if (tag != "<xmlcomment>")
        throw invalid_argument("ParseXMLBounds: Invalid/Improper Tag: "+ tag);
    }
    if (!gotIt)
    {
      // Get direct value:
      *type = RealCalibrParams<F>::CBExpl;
      *val  = be.get_value<F>();
    }
  }
}

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "CalibrBounds::IsFixed":                                                //
  //=========================================================================//
  // Fixed Diffusion Coeffs are excluded from the ArgMax of LLH:
  //
  template<typename F>
  bool RealCalibrParams<F>::CalibrBounds::IsFixed() const
  {
    bool loFixed =
      (m_typeLo == RealCalibrParams<F>::CBDeltaRel ||
       m_typeLo == RealCalibrParams<F>::CBDeltaAbs) && (m_valLo  == F(0.0));

    bool upFixed =
      (m_typeUp == RealCalibrParams<F>::CBDeltaRel ||
       m_typeUp == RealCalibrParams<F>::CBDeltaAbs) && (m_valUp  == F(0.0));

    bool explFixed =
      (m_typeLo == RealCalibrParams<F>::CBExpl      &&
       m_typeUp == RealCalibrParams<F>::CBExpl      && m_valLo  == m_valUp);

    return (loFixed && upFixed) || explFixed;
  }

  //=========================================================================//
  // "RealCalibrParams" Ctors:                                               //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // "CommonInit":                                                           //
  //-------------------------------------------------------------------------//
  template<typename F>
  void RealCalibrParams<F>::CommonInit
  (
    string const& diff_type,
    string const& cal_name
  )
  {
    m_diffType       = diff_type;
    m_withReg        = true;
    m_calName        = cal_name;
    m_gridMethod     = "Yanenko";
    m_m              = 501;
    m_n              = 501;
    m_hS             = F(0.0);      // Normally not used
    m_hV             = F(0.0);      // Normally not used
    m_sts            = 5;
    m_dt             = Time(1,0,0);
    m_minRelS        = F(0.075);
    m_maxRelS        =  Inf<F>();
    m_minRelV        = F(0.2);
    m_maxRelV        =  Inf<F>();
    m_NSigmasS       = F(7.0);
    m_NSigmasV       = F(7.0);
    m_LoS            = -Inf<F>();
    m_UpS            =  Inf<F>();
    m_LoV            = -Inf<F>();
    m_UpV            =  Inf<F>();
    m_S0OnGrid       = true;
    m_V0OnGrid       = true;
    // "m_cudaParamss" are initially empty -- no CUDA!
    m_mixedCUDAHost  = true;
    m_minSbC         = 60;
    m_useDirichlet   = false;
    m_debugLevel     = 0;
    // "m_mdFile"   is initially empty
    // "m_t0", "m_ts1", "m_dts", "m_ts2" are initially empty (Time())
    // "m_mdTrName" and "m_mktData" are initially empty
    m_calibrMethod   = "DIRect-E04JYF";
    m_rectFrac       = F(0.01);
    m_maxGlobalPts   = 500;
    m_volWinSz       = 100;
    m_mlsl_maxRounds = 10;
    m_mlsl_maxBest   = 2;
    // "m_initGuess" and "m_bounds" are initially empty
  }

  //-------------------------------------------------------------------------//
  // Directly-provided MktData:                                              //
  //-------------------------------------------------------------------------//
  template<typename F>
  RealCalibrParams<F>::RealCalibrParams
  (
    string const&                        diff_type,
    shared_ptr<vector<MDItem<F>>> const& mkt_data,
    string const&                        cal_name
  )
  {
    CommonInit(diff_type, cal_name);
    assert(m_mktData.get() == nullptr && m_mdFile.empty() && m_mdTrName.empty());

    m_mktData = mkt_data;
    if (mkt_data.get() == nullptr)
      throw invalid_argument("RealCalibrParams Ctor: nullptr MktData");

    // XXX: Although in this case "m_t0", "m_ts1", "m_dts" and "m_ts2" are
    // not supposed to be used, initialise them if possible:
    if (!mkt_data->empty())
    {
      m_t0 = (*mkt_data)[0].m_t;

      if (mkt_data->size() >= 2)
      {
        m_ts1 = (*mkt_data)[1].m_t;
        m_ts2 = mkt_data->back().m_t;
        // But "m_dts" still cannot be initialised exactly in this case...
      }
    }
  }

  //-------------------------------------------------------------------------//
  // MktData from a File:                                                    //
  //-------------------------------------------------------------------------//
  template<typename F>
  RealCalibrParams<F>::RealCalibrParams
  (
    string const& diff_type,
    string const& md_file,
    DateTime      t0,
    DateTime      ts1,
    Time          dts,
    DateTime      ts2,
    string const& cal_name,
    string const& mdTr_name
  )
  {
    CommonInit(diff_type, cal_name);
    assert
      (m_mktData.get() == nullptr && m_mdFile.empty() && m_mdTrName.empty());

    m_mdFile    = md_file;
    m_t0        = t0;
    m_ts1       = ts1;
    m_dts       = dts;
    m_ts2       = ts2;
    m_mdTrName  = mdTr_name;
  }

  //=========================================================================//
  // "FromXML":                                                              //
  //=========================================================================//
  // Returns "RealCalibrParams" (because no default ctor exists for them, so
  // they cannot be created initially by the caller) and fills in Initial Gu-
  // esses and Bounds:
  //
  template<typename F>
  RealCalibrParams<F> RealCalibrParams<F>::FromXML
    (string const& xml_file)
  {
    using boost::property_tree::ptree;
    using boost::optional;

    // The minimum set of values required to create "RealCalibrParams" with
    // the externally-visible (validating) ctor:
    string    diff_type;
    string    md_file;
    DateTime  t0;
    DateTime  ts1;
    Time      dts;
    DateTime  ts2;

    // Create the Property Tree and populate it from XML file:
    ptree xmlDoc;
    try
    {
      read_xml(xml_file, xmlDoc);
    }
    catch(exception const& e)
    {
      throw runtime_error
            ("FromXML: Unable to parse "+ xml_file +": "+ e.what());
    }

    // There must be only one top-level element: "RealCalibrParams":
    if (int(xmlDoc.size()) != 1)
      throw invalid_argument
            ("RealCalibrParams::FromXML: Invalid top-level Doc");

    ptree const& root = xmlDoc.get_child("RealCalibrParams");

    // Fill in the compulsory args for "RealCalibrParams":
    //
    diff_type = root.get<string>("DiffusionType");
    md_file   = root.get<string>("MktDataFile");
    t0        = DateTimeOfStr   (root.get<string>("DiffusionStart"));
    ts1       = DateTimeOfStr   (root.get<string>("ConditionStart"));
    dts       = TimeOfStr       (root.get<string>("ConditionStep"));
    ts2       = DateTimeOfStr   (root.get<string>("ConditionEnd"));

    // Create the initial "RealCalibrParams" object (all other flds set to
    // their default vals):
    RealCalibrParams<F> params(diff_type, md_file, t0, ts1, dts, ts2);

    // Fill in the optional params:
    //
    for (ptree::const_iterator cit = root.begin(); cit != root.end(); ++cit)
    {
      string const& tag  = cit->first;
      ptree  const& elem = cit->second;

      if (tag == "UseRegimes")
        params.m_withReg  = elem.get_value<bool>();
      else
      if (tag == "TradingCalendar")
        params.m_calName  = elem.get_value<string>();
      else
      if (tag == "GridMethod")
        params.m_gridMethod = elem.get_value<string>();
      else
      if (tag == "TimeStep")
        params.m_dt = TimeOfStr(elem.get_value<string>());
      else
      if (tag == "SNodes")
        params.m_m   = elem.get_value<int>();
      else
      if (tag == "VNodes")
        params.m_n   = elem.get_value<int>();
      else
      if (tag == "StepS")
        params.m_hS  = elem.get_value<F>();
      else
      if (tag == "StepV")
        params.m_hV  = elem.get_value<F>();
      else
      if (tag == "StencilSz")
        params.m_sts = elem.get_value<int>();
      else
      if (tag == "MinRelS")
        params.m_minRelS = elem.get_value<F>();
      else
      if (tag == "MaxRelS")
        params.m_maxRelS = elem.get_value<F>();
      else
      if (tag == "MinRelV")
        params.m_minRelV = elem.get_value<F>();
      else
      if (tag == "MaxRelV")
        params.m_maxRelV = elem.get_value<F>();
      else
      if (tag == "NSigmasS")
        params.m_NSigmasS = elem.get_value<F>();
      else
      if (tag == "NSigmasV")
        params.m_NSigmasV = elem.get_value<F>();
      else
      if (tag == "LoS")
        params.m_LoS      = elem.get_value<F>();
      else
      if (tag == "UpS")
        params.m_UpS      = elem.get_value<F>();
      else
      if (tag == "LoV")
        params.m_LoV      = elem.get_value<F>();
      else
      if (tag == "UpV")
        params.m_UpV      = elem.get_value<F>();
      else
      if (tag == "S0OnGrid")
        params.m_S0OnGrid = elem.get_value<bool>();
      else
      if (tag == "V0OnGrid")
        params.m_V0OnGrid = elem.get_value<bool>();
      else
      if (tag == "UseDirichletBoundConds")
        params.m_useDirichlet = elem.get_value<bool>();
      else
      if (tag == "MinStepsBtwnConds")
        params.m_minSbC = elem.get_value<int>();
      else
      if (tag == "MixedCUDAHostMode")
        params.m_mixedCUDAHost = elem.get_value<bool>();
      else
      if (tag == "CUDAParams")
      {
        // Create a new "CalibrCUDAParam3" and add it to the list:
        if (elem.size() != 4)
          throw invalid_argument
                ("RealCalibrParams::FromXML: Invalid CUDAParams");
        CUDAEnv::Params cp
        (
          elem.get<int> ("DevID"),
          elem.get<int> ("BlksPerSM"),
          elem.get<int> ("BlkSz"),
          elem.get<bool>("WithTimer")
        );
        params.m_cudaParamss.push_back(cp);
      }
      else
      if (tag == "OptimMethod")
        params.m_calibrMethod   = elem.get_value<string>();
      else
      if (tag == "RectFrac")
        params.m_rectFrac       = elem.get_value<F>();
      else
      if (tag == "MaxGlobalPts")
        params.m_maxGlobalPts   = elem.get_value<int>();
      else
      if (tag == "VolWindowSz")
        params.m_volWinSz       = elem.get_value<int>();
      else
      if (tag == "MLSLMaxRounds")
        params.m_mlsl_maxRounds = elem.get_value<int>();
      else
      if (tag == "MLSLMaxBest")
        params.m_mlsl_maxBest   = elem.get_value<int>();
      else
      if (tag == "DebugLevel")
        params.m_debugLevel     = elem.get_value<int>();
      else
      if (tag == "VarCalibrRegion")
      {
        // Get the InitVal and Bounds for a particular var to be calibarted:
        CalibrInitGuess cig;
        CalibrBounds    cbs;

        cig.m_val       = F(0.0);
        cbs.m_valLo     = F(0.0);
        cbs.m_valUp     = F(0.0);

        bool gotInit    = false;
        bool gotBoundLo = false;
        bool gotBoundUp = false;

        for (ptree::const_iterator cr = elem.begin(); cr != elem.end(); ++cr)
        {
          string const& crTag = cr->first;
          ptree  const& crVal = cr->second;

          if (crTag == "InitVal" && !gotInit)
          {
            ParseXMLInit<F>(crVal, &cig.m_type, &cig.m_val);
            gotInit = true;
          }
          else
          if (crTag == "BoundLo" && !gotBoundLo)
          {
            ParseXMLBounds<F>(crVal, &cbs.m_typeLo, &cbs.m_valLo);
            gotBoundLo = true;
          }
          else
          if (crTag == "BoundUp" && !gotBoundUp)
          {
            ParseXMLBounds<F>(crVal, &cbs.m_typeUp, &cbs.m_valUp);
            gotBoundUp = true;
          }
          else
          if (crTag == "Bounds"  && !gotBoundLo && !gotBoundUp)
          {
            // Lo and Up Bounds are the same (up to signs, unless the value is
            // Explicit):
            ParseXMLBounds<F>(crVal, &cbs.m_typeUp, &cbs.m_valUp);

            cbs.m_typeLo = cbs.m_typeUp;
            cbs.m_valLo  = 
              (  cbs.m_typeLo == RealCalibrParams<F>::CBExpl)
              ?  cbs.m_valUp
              : -cbs.m_valUp;
            gotBoundLo   = true;
            gotBoundUp   = true;
          }
          else
          if (crTag != "<xmlcomment>")
            throw invalid_argument
                  ("RealCalibrParams::FromXML: Invalid/Improper ValCalibration"
                   "Region Sub-Tag: "+ crTag);
        }
        // Now, both Init and Bounds must be in:
        if (!gotInit)
          throw invalid_argument
                ("RealCalibrParams::FromXML: CalibrRegion: Missing InitVal");
        if (!gotBoundLo)
          throw invalid_argument
                ("RealCalibrParams::FromXML: CalibrRegion: Missing BoundLo");
        if (!gotBoundUp)
          throw invalid_argument
                ("RealCalibrParams::FromXML: CalibrRegion: Missing BoundUp");

        // Otherwise, attach the structs created:
        params.m_initGuess.push_back(cig);
        params.m_bounds   .push_back(cbs);
      }
      else
      // Skip comments and the tags already processed. All other tags are
      // invalid:
      if (tag != "<xmlcomment>"   && tag != "DiffusionType"  &&
          tag != "MktDataFile"    && tag != "DiffusionStart" &&
          tag != "ConditionStart" && tag != "ConditionStep"  &&
          tag != "ConditionEnd")
        throw invalid_argument
              ("RealCalibrParams::FromXML: Invalid tag: "+ tag);
    }
    // All done!
    return params;
  }
}
