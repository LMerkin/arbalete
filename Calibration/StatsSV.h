// vim:ts=2:et
//===========================================================================//
//                                "StatsSV.h":                               //
//        Statistics Accumulator for Non-Uniform Regime-Switching Data       //
//===========================================================================//
#pragma once

#include "Common/Macros.h"
#include "Common/TradingCalendar.h"
#include <utility>
#include <cstring>

namespace Arbalete
{
  //=========================================================================//
  // "StatsSV":                                                              //
  //=========================================================================//
  // Output data produced by "StatsAccumulatorLLH" (see below). XXX: currently
  // provides statistics for "S" (or some transform of it) only, not "V":
  //
  template<typename F>
  struct StatsSV
  {
    // Data Flds:
    F   m_trendS;       // "S" trend (drift) rate

    F   m_volSTH;       // "S" volatility, Trading Hours
    F   m_errVolSTH;    // StdDev of the above estimate

    F   m_volSON;       // "S" volatility, Over-Night
    F   m_errVolSON;    // StdDev of the above estimate

    F   m_volSWEH;      // "S" volatility, week-ends and holidays
    F   m_errVolSWEH;   // StdDev of the above stimate

    F   m_volVolSTH;    // Vol of vol of "S", trading hours
    F   m_volVolSNR;    // Signal-to-noise ratio of the above estimate

    // Default Ctor: Zero-out the obj:
    StatsSV() { memset(this, '\0', sizeof(StatsSV)); }
  };

  //=========================================================================//
  // "StatsAccumulatorSV":                                                   //
  //=========================================================================//
  // Accumulates the statistics, generates "StatsSV":
  //
  template<typename F>
  class StatsAccumulatorSV
  {
  private:
    //-----------------------------------------------------------------------//
    // Sums and sums of squares of data items:                               //
    //-----------------------------------------------------------------------//
    // See more detailed comments in the implementation part:
    //
    F   m_sumDT;    // Total time and S sum -- for the mean
    F   m_sumDS;

    int m_nTH;      // Trading Hours vol accumulators
    F   m_sumDSTH;
    F   m_sumDSTH2;
    F   m_sumDTTH;
    F   m_sumDTTH2;
    F   m_sumDSDTTH;

    int m_nON;      // Over-Night vol accumulators
    F   m_sumDSON;
    F   m_sumDSON2;
    F   m_sumDTON;
    F   m_sumDTON2;
    F   m_sumDSDTON;

    int m_nWEH;     // Week-Ends and Holidays vol accumulators
    F   m_sumDSWEH;
    F   m_sumDSWEH2;
    F   m_sumDTWEH;
    F   m_sumDTWEH2;
    F   m_sumDSDTWEH;

    // Window of (dS,dt) pairs for short-term vol calculations:
    std::deque <std::pair<F,F>>         m_Win;
    std::vector<std::pair<DateTime,F>>  m_instVols;
    int m_nWin;
    F   m_sumDSWin;
    F   m_sumDSWin2;
    F   m_sumDTWin;
    F   m_sumDTWin2;
    F   m_sumDSDTWin;

    int m_nVol;   // Short-term vol accumulators
    F   m_maxVol;
    F   m_sumDV;
    F   m_sumDV2;
    F   m_sumDTVol;
    F   m_sumDTVol2;
    F   m_sumDVDT;

  public:
    //-----------------------------------------------------------------------//
    // Methods:                                                              //
    //-----------------------------------------------------------------------//
    // Default Ctor: all flds are initialised to 0s:
    StatsAccumulatorSV(int vol_win_size = 100);

    // Update stats -- install a new data item:
    void UpdateStats
      (DateTime t, F dS, Time dt, TradingCalendar::CalReg reg);

    // Evaluation: Return the current stats:
    StatsSV<F> operator()() const;

    // Get "instanteneous" vols accumulated. XXX: because no weighting (exp or
    // another) is currently applied in "inst vols" estimation,  the resulting
    // vols are NOTIONALLY associated with the last instant of the window used
    // to produce them -- which is not reliable   (e.g. why not use the window
    // mid-point?):
    std::vector<std::pair<DateTime,F>> const& GetInstVols() const
      { return m_instVols; }
  };
}
