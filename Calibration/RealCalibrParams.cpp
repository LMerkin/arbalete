// vim:ts=2:et
//===========================================================================//
//                         "RealCalibrParams.cpp":                           //
//                        Some Pre-Defined Instances                         //
//===========================================================================//
#include "Calibration/RealCalibrParams.hpp"

//---------------------------------------------------------------------------//
// Bug Fix for the Intel Compiler:                                           //
//---------------------------------------------------------------------------//
// (It fails to auto-generate some instances):
//
#ifdef __INTEL_COMPILER
namespace boost
{
  namespace multi_index
  {
    namespace detail
    {
      template
      class bidir_node_iterator
      <sequenced_index_node
        <ordered_index_node
          <index_node_base
            <std::pair
              <std::string const,
               boost::property_tree::basic_ptree
                <std::string,
                 std::string,
                 std::less<std::string>
                >
              >,
              std::allocator
                <std::pair
                  <std::string const,
                   boost::property_tree::basic_ptree
                    <std::string, std::string, std::less<std::string>>
                  >
                >
            >
          >
        >
      >;
    }
  }
}
#endif  // __INTEL_COMPILER

//---------------------------------------------------------------------------//
// General Instantiations:                                                   //
//---------------------------------------------------------------------------//
namespace Arbalete
{
  template struct RealCalibrParams<double>;
  template struct RealCalibrParams<float>;
}
