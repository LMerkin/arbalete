// vim:ts=2:et
//===========================================================================//
//                          "RealCalibrator.hpp":                            //
//               LLH-Based Calibration of Diffusion Params                   //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Common/TradingCalendar.h"
#include "Diffusions/Diffusion2D_Fact.h"
#include "Diffusions/DiffusionAnalytical_Fact.h"
#include "PDF/CondLLH1D_TransPDF.h"
#include "PDF/CondLLH2D_FokkerPlanck.h"
#include "PDF/CondLLH2D_BayesOptFilt.h"
#include "PDF/CondLLH_Analytical.h"
#include "Calibration/RealCalibrator.h"
#include "Calibration/MLSL.h"
#include "Calibration/StatsSV.h"
#include "Calibration/MarketData.h"
#include "Calibration/IPOptEval.hpp"
#include <coin/IpIpoptApplication.hpp>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cctype>
#include <iostream>
#include <algorithm>
#include <memory>
#include <cassert>
#include <stdexcept>

//===========================================================================//
// Fortran Optimisation Routines:                                            //
//===========================================================================//
//---------------------------------------------------------------------------//
// Types of Fortran Call-Back Functions:                                     //
//---------------------------------------------------------------------------//
// For E04JYF:
// NV >= 1: size of "xs", >= 1; NV == 0 indicates a sync point;
// xs     : curr arg point (size = NV);
// f      : OUT: curr function value;
// ips    : user-specified "int"    params, any size;
// dps    : user-specified "double" params, any size:
//
typedef void (*NAGCB)
(
  int const* NV,  double const* xs, double* f,
  int const* ips, double const* dps
);

// For DIRect:
// NV     : size of "xs", >= 1; NV == 0 indicates a sync point;
// xS     : curr arg point (size = NV);
// f      : OUT: curr function value;
// flag   : OUT: 0 if "f" evaluated OK, >= 1: wrong point, <= -1: wrong setup;
// iData, Li, dData, Ld, cData, Lc: user-sspecified data, see "DIRect" below:
//
typedef char const char40[40];

typedef void (*DIRectCB)
(
  int    const* NV,    double const* xs, double* f, int* flag,
  int    const* iData, int const* Li,    double const* dData, int const* Ld,
  char40 const* cData, int const* Lc
);

//---------------------------------------------------------------------------//
// "E04JYF": Local (Newtonian) Minimisation Function from NAG:               //
//---------------------------------------------------------------------------//
// NV     : dimensionality of the domain, >= 1;
// ibtype : type of constraints "bl" and "bu" below (0..3);
// evalf  : evaluation function;
// bls    : lower bounds for "x", size = NV (or 1, depending on "ibtype");
// bus    : upper bounds for "x", similar to "bl";
// x      : initial guess on input, argmin on output;
// f      : OUT: function minimum found;
// iw     : workspace and output flags, size = Liw >= NV+2;
// Liw    : as above;
// w      : workspace and grad vectors, size = Lw = max(NV((NV-1)/2+12), 13);
// Lw     : as above;
// ips    : user-specified "int"     params, any size, passed to the "evalf";
// dps    : user-dpecifiied "double" params, any size, passed to the "evalf";
// irc    : return code
//          0: success;
//          3: optimisation has failed;
//          8: optimisation is very unlikely to have worked;
//          7: optimisation is unlikely to have worked;
//        ...
extern "C" void e04jyf_
(
  int const* NV,     int const* ibound,   NAGCB evalf,
  double const* bls, double const* bus,   double* x,      double* f,
  int* iw,           int const* Liw,      double* w,      int const* Lw,
  int const* ips,    double const* dps,   int* rc
);

//---------------------------------------------------------------------------//
// "DIRect": Global Minimisation Function:                                   //
//---------------------------------------------------------------------------//
// evalf  : evaluation function;
// xs     : OUT: solution vector, size = NV;
// NV     : dimensionality of the domain, >= 1;
// eps    : precision of fmax, may be < 0 (then adaptive), recomm. 1e-4;
// maxEvs : max number of function evaluations (eg 20000);
// maxIts : max number of iterations           (eg  6000);
// fMin   : OUT: the minimal value found;
// bls    : lower bounds, size = NV;
// bus    : upper bounds, size = NV;
// method : 0: original (Jones), 1: new  (Gablonsky);
// errc   : OUT: error code (0: OK; < 0: some error);
// logFD  : log file descriptor (handle);
// fGlob  : set it to -1e100;
// fRel   : set it to 0.0;
// minVol : min hyperrect volume in (set it to -1 for no limit?);
// minSgm : min heperrect measure   (??? how is it different from "minVol"?);
// iData  : user-specified "int" data array, size = Li;
// Li     : size of the above;
// dData  : user-specified "double" data array, size = Ld;
// Ld     : size of the above;
// cData  : user-specified char string, size = Lc;
// Lc     : size (length) of the above:
//
extern "C" void direct_
(
  DIRectCB evalf,       double* xs,           int const* NV,

  double const* eps,    int const* maxEvs,    int const* maxIts,
  double*       fMin,
  double const* bls,    double const* bus,    int const* method,
  int*          errc,
  int const*    logFD,  double const* fGlob,  double const* fRel,
  double const* minVol, double const* minSgm,

  int const* iData,     int const* Li,        double const* dData,
  int const* Ld,        char40 const* cData,  int const*    Lc
);

namespace
{
  using namespace Arbalete;
  using namespace std;

  //-------------------------------------------------------------------------//
  // "EvalReqOrSync":                                                        //
  //-------------------------------------------------------------------------//
  // Common part of Optimisation Routine Call-Backs -- submits an Evaluation
  // Request, or makes Synchronisation, or calls "EvalLLH" synchronously.
  // NB: This method uses a Fortran-style interface (so it is directly call-
  // able from Fortran optimisation routines, as well as from C++):
  //
  template<typename F>
  void EvalReqOrSync
  (
    int const*                NV,
    double const*             xs,
    double*                   res,
    Arbalete::Vector<double>* gradient,
    Arbalete::Matrix<double>* hessian,
    int*                      flag,
    void*                     env
  )
  {
    assert(NV != nullptr && *NV >= 0 && env != nullptr);

    // "env" is actually a ptr to the RealCalibrator object:
    RealCalibrator<F> const* calibr = (RealCalibrator<F> const*)(env);
    assert(calibr  != nullptr);

    // Get the number of Evaluators:
    int nEvals = int(calibr->GetLLHs().size());
    assert(nEvals >= 1);

    if (nEvals == 1)
    { //---------------------------------------------------------------------//
      // The Evaluator is synchronous, no threads:                           //
      //---------------------------------------------------------------------//
      // Perform direct evaluation:
      if (*NV >= 1)
        calibr->EvalLLH(0, *NV, xs, res, gradient, hessian, flag);

      // If *NV == 0, this is a synchronisation call which is a No-Op in this
      // case...
    }
    else
      //---------------------------------------------------------------------//
      // The evaluator is asynchronous:                                      //
      //---------------------------------------------------------------------//
      if (*NV == 0)
        // SYNC:
        // No new evaluation is to be performed: Wait for completion of all
        // previously scheduled evaluations:
        calibr->Synchronise();
      else
      {
        // SCHEDULE an EVALUATION:
        // NB: "flag", "gradient" and "hessian" may still be NULL:
        assert(xs != nullptr && res != nullptr && env != nullptr);

        // Put an invalid value into the "res" (XXX: this is for debugging only
        // -- to detect potential race conditions more easily):
        *res = Inf<double>();

        // Form a "WorkItem" and submit it for asynchronous evaluation:
        if (*NV > RealCalibrator<F>::WorkItem::MaxSize)
          throw runtime_error("EvalReqOrSync: Args vector too long");

        typename RealCalibrator<F>::WorkItem wi;
        for (int i = 0; i < *NV; ++i)
          wi.m_xs[i] = xs[i];
        wi.m_Nx       = *NV;
        wi.m_res      = res;
        wi.m_gradient = gradient;
        wi.m_hessian  = hessian;
        wi.m_flag     = flag;

        calibr->SubmitWorkItem(wi);
      }
  }

  //-------------------------------------------------------------------------//
  // "NAGEval":                                                              //
  //-------------------------------------------------------------------------//
  // Evaluation of the Cost Function, to be invoked from the NAG Local Optimi-
  // ser (E04JYF). Conforms to the "NAGCB" type:
  //
  template<typename F>
  void NAGEval
  (
    int const* NV,  double const* xs,  double* f,
    int const* ips, double const* DEBUG_ONLY(dps)
  )
  {
    // Unused args must be NULL:
    assert(dps == nullptr);

    // Invoke the common implementation. There is no Result Flag, Gradient or
    // Hessian here:
    EvalReqOrSync<F>
      (NV, xs, f, nullptr, nullptr, nullptr,
       const_cast<void*>((void const*)(ips)));
  }

  //-------------------------------------------------------------------------//
  // "DIRectEval":                                                           //
  //-------------------------------------------------------------------------//
  // Evaluation of the Cost Function, to be invoked from the DIRect Gloabl Opt-
  // imiser. Conforms to the "DIRectCB" type:
  //
  template<typename F>
  void DIRectEval
  (
    int    const* NV,                 double const* xs,
    double*       f,                  int*          flag,
    int    const* iData,              int const*    UNUSED_PARAM(Li),
    double const* DEBUG_ONLY(dData),  int const*    UNUSED_PARAM(Ld),
    char40 const* DEBUG_ONLY(cData),  int const*    UNUSED_PARAM(Lc)
  )
  {
    // Unused args must be NULL:
    assert(dData == nullptr && cData == nullptr);

    // Invoke the common implementation; no Gradient or Hessian here:
    EvalReqOrSync<F>
      (NV, xs, f, nullptr, nullptr, flag,
       const_cast<void*>((void const*)(iData)));
  }

  //-------------------------------------------------------------------------//
  // "RaiseOptimError":                                                      //
  //-------------------------------------------------------------------------//
  inline void RaiseOptimError(char const* method, int rc)
  {
    char buff[256];
    snprintf(buff, 256, "RealCalibrator::Calibrate: Optimiser %s Failed: "
             "ErrCode=%d\n", method, rc);
    throw runtime_error(buff);
  }

  //-------------------------------------------------------------------------//
  // "MkBound":                                                              //
  //-------------------------------------------------------------------------//
  // Utility function used by "RealCalibrator::Init":
  //
  template<typename F>
  inline F MkBound
    (F init_val, typename RealCalibrParams<F>::CBType bound_type, F bound_size)
  {
    switch (bound_type)
    {
    case RealCalibrParams<F>::CBDeltaRel:
      // NB: typically, bound_size < 0 for lo bound and > 0 for upper bound,
      // unless init_val < 0, in which case the signs of "bound_size"s would
      // need to be inverted by the caller to get a valid range:
      //
      return init_val * (F(0.0) + bound_size);

    case RealCalibrParams<F>::CBDeltaAbs:
      // Again, typically bound_size < 0 for lo bound and > 0 for upper bound:
      return init_val + bound_size;

    case RealCalibrParams<F>::CBExpl:
      return bound_size;

    default:
      assert(false);
      return Inf<F>();
    }
  }
}

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "Init:                                                                  //
  //=========================================================================//
  // Common part of the "RealCalibrator" Ctors:
  //
  template<typename F>
  void RealCalibrator<F>::Init(RealCalibrParams<F> const& params)
  {
    //-----------------------------------------------------------------------//
    // Create the Trading Calendar:                                          //
    //-----------------------------------------------------------------------//
    m_allDone = false;    // Just starting...

    if (!m_llhs.empty())
      throw runtime_error("RealCalibrator::Init: Repeated Initialisation");

    shared_ptr<TradingCalendar> cal =
      TradingCalendar::MkTradingCalendar(params.m_calName);

    //-----------------------------------------------------------------------//
    // Load the MktData:                                                     //
    //-----------------------------------------------------------------------//
    shared_ptr<vector<MDItem<F>>> mktData;    // Initially NULL
    StatsSV<F>                    stats;
    bool                          hasStats = false;

    if (!params.m_mdFile.empty())
    {
      //---------------------------------------------------------------------//
      // MktData are to be loaded from the specified file:                   //
      //---------------------------------------------------------------------//
      if (params.m_mktData.get() != nullptr)
        throw invalid_argument
              ("RealCalibrator::Init: Conflicting MktData sources");

      mktData = shared_ptr<vector<MDItem<F>>>(new vector<MDItem<F>>);

      // Construct the MktData Transform:
      MDTransform<F>* mdTr =
        MkMDTransform<F>(params.m_mdTrName, params.m_volWinSz);

      // Do load:
      try
      {
        LoadMktData<F>
          (params.m_mdFile, params.m_t0,  params.m_ts1, params.m_dts,
           params.m_ts2,    cal,          mdTr,         mktData.get());
      }
      catch(...)
      {
        delete mdTr;
        throw;
      }
      // Get the accumulated stistics:
      stats    = mdTr->m_statsAcc();
      hasStats = true;

      // Remove the Transform:
      delete mdTr;
    }
    else
    {
      //---------------------------------------------------------------------//
      // MktData are given explicitly:                                       //
      //---------------------------------------------------------------------//
      if (params.m_mktData.get() == nullptr)
        throw invalid_argument
              ("RealCalibrator::Init: Empty MktData, and no file specified");

      // In this case, "mktData" must be consistent with the time ranges pro-
      // vided in the "params". XXX: We do not compute any stats:
      //
      mktData = params.m_mktData;
    }
    assert(mktData.get() != nullptr);

    //-----------------------------------------------------------------------//
    // Construct the LLH Object(s):                                          //
    //-----------------------------------------------------------------------//
    // Number of LLH Objects to be constructed:
    int nl = 0;

    // Create the Diffusion Params Stub:
    Diffusion_Params<F> diffParams =
      CondLLH<F>::MkDiffParamsStub(params.m_diffType, params.m_withReg);

    if (params.m_gridMethod == "Analytical")
    {
      //---------------------------------------------------------------------//
      // Analytical Case: No Grid:                                           //
      //---------------------------------------------------------------------//
      // Construct Analytical LLH Object directly on the Diffusion.  The latter
      // must be of the Analytical type (any Dimension) - will get an exception
      // if it is not:
      //
      m_llhs.push_back(new CondLLH_Analytical<F>
                           (diffParams, *mktData, params.m_debugLevel));
      nl = 1;
    }
    else
    if (params.m_gridMethod == "TransPDF")
    {
      //---------------------------------------------------------------------//
      // TransPDF Case: No Grid:                                             //
      //---------------------------------------------------------------------//
      // This is for 1D Diffusions only (will get an exception otherwise). LLH
      // is evaluated directly on the Diffusion:
      //
      m_llhs.push_back(new CondLLH1D_TransPDF<F>
                           (diffParams, *mktData, cal, params.m_debugLevel));
      nl = 1;
    }
    else
    {
      //---------------------------------------------------------------------//
      // Generic Case: With a Grid:                                          //
      //---------------------------------------------------------------------//
      // XXX: Note that we only set up Diffusion and Grid params   (in the LLH
      // object) atthe moment;  Diffusion and Grid  cannot be constructed  yet
      // because Diffusion Params are not known.   This results in a risk that
      // Diffusion and Grid may become incompatible. This error condition will
      // only be detected down the line, and may result in eg lack of feasible
      // points in Global Optimisation.
      // So do a basic check here: XXX: all Grids in this section are currently
      // 2D, so need a 2D Diffusion as well:
      //
      if (!IsDiffusion2D<F>(diffParams))
        throw invalid_argument
              ("RealCalibrator::Init: 2D Grid: "+ params.m_gridMethod +
               " but not a 2D Diffusion: "      + diffParams.m_type);

      // Is the Grid of the Bayesian Optimal Filtering type? Then special con-
      // structs will apply:
      bool isBOF = (params.m_gridMethod == "BayesOptFilt");

      // Create the 2D Grid Params Stub: NB: this depends on the  "isBOF" mode
      // (actually, it would still be OK to use a generic Fokker-Planck type
      // stub in all cases -- it would be automatically converted to BOF if
      // BOF is required);
      // CUDA env is installed later:
      //
      typename Grid2D<F>::Params gridParams0 =
        isBOF
        ?
          // 2D BOF case:
          CondLLH2D_BayesOptFilt<F>::MkGridParamsStub
            (params.m_n, diffParams.m_S0, params.m_LoV, params.m_UpV, nullptr,
             cal)
        :
          // Generic 2D Case: XXX: Will need to amend it further to distinguish
          // between 1D and 2D; currently, 1D is TransPDF only:
          CondLLH2D_FokkerPlanck<F>::MkGridParamsStub
          (params.m_gridMethod, params.m_m,    params.m_n,
            params.m_sts,       params.m_dt,   nullptr,     cal);

      // Install extra axis construction params: NB: BEWARE of the interplay
      // between the params passed through the above ctors, and those set di-
      // rectly:
      //
      gridParams0.m_S0OnGrid =  params.m_S0OnGrid;
      gridParams0.m_NSigmasS =  params.m_NSigmasS;
      gridParams0.m_minRelS  =  params.m_minRelS;
      gridParams0.m_maxRelS  =  params.m_maxRelS;
      gridParams0.m_hS       =  params.m_hS;

      gridParams0.m_V0OnGrid =  params.m_V0OnGrid;
      gridParams0.m_NSigmasV =  params.m_NSigmasV;
      gridParams0.m_minRelV  =  params.m_minRelV;
      gridParams0.m_maxRelV  =  params.m_maxRelV;
      gridParams0.m_hV       =  params.m_hV;

      // NB: There may be a conflict here between the following flds which may
      // already be set on "gridParams0", and their new values coming from
      // "params". So be conservative:
      //
      if (!IsFinite(gridParams0.m_LoS) && IsFinite(params.m_LoS))
      {
        assert(!gridParams0.m_LoSFixed);
        gridParams0.m_LoS      = params.m_LoS;
      }
      gridParams0.m_LoSFixed = IsFinite(gridParams0.m_LoS);

      if (!IsFinite(gridParams0.m_UpS) && IsFinite(params.m_UpS))
      {
        assert(!gridParams0.m_UpSFixed);
        gridParams0.m_UpS      = params.m_UpS;
        gridParams0.m_UpSFixed = true;
      }
      gridParams0.m_UpSFixed = IsFinite(gridParams0.m_UpS);

      if (!IsFinite(gridParams0.m_LoV) && IsFinite(params.m_LoV))
      {
        assert(!gridParams0.m_LoVFixed);
        gridParams0.m_LoV      = params.m_LoV;
        gridParams0.m_LoVFixed = true;
      }
      gridParams0.m_LoVFixed = IsFinite(gridParams0.m_LoV);

      if (!IsFinite(gridParams0.m_UpV) && IsFinite(params.m_UpV))
      {
        assert(!gridParams0.m_UpVFixed);
        gridParams0.m_UpV      = params.m_UpV;
        gridParams0.m_UpVFixed = true;
      }
      gridParams0.m_UpVFixed = IsFinite(gridParams0.m_UpV);

      // NB: We want this Grid to be "INVARIANT", that is, axes being independ-
      // ent of Diffusion params and initial conds. (Normally, axes are rescal-
      // ed dynamically). The following are the invariance conds:
      //
      if (!gridParams0.IsInvariantS() || !gridParams0.IsInvariantV())
        cout << "WARNING: RealCalibrator::Init: Axes are NOT invariant; "
                "this may adversely affect the stability of calibration"
             << endl;

      // Now create CUDA Envs (if provided) and install them in Grid Params
      // instead of the NULL placeholder:
      //
      vector<typename Grid2D<F>::Params> gridParamss;
      int nc = int(params.m_cudaParamss.size());

      for (int i = 0; i < nc; ++i)
      {
        // Verify that CUDA IDs are not repeated:
        for (int j = 0; j < i; ++j)
          if (params.m_cudaParamss[j].m_DevID ==
              params.m_cudaParamss[i].m_DevID)
            throw invalid_argument
                  ("RealCalibrator::Init: Repeated CUDA Dev ID(s)");

        // If the CUDA ID is unique: create the corresp CUDA Env and Grid
        // Params:
        gridParamss.push_back(gridParams0);
        gridParamss.back().m_cudaEnv =
          shared_ptr<CUDAEnv>(new CUDAEnv(params.m_cudaParamss[i]));
      }

      // At the end, install the Host-only Grid Params. NB: We do that only if
      // there are no CUDA-based grids, or the mixed CUDA-Host mode is allowed:
      if (nc == 0 || params.m_mixedCUDAHost)
        gridParamss.push_back(gridParams0);

      // Now construct LLH objects for all Grid Params constructed.  NB: Mkt
      // Data are shared between multiple LLH objects. At least 1 LLH object
      // is constructed. LLH objects serve as Cost Function Evaluators (when
      // provided with the current Coeffs):
      //
      nl = int(gridParamss.size());
      assert((nl == nc + 1 || nl == nc) && nl >= 1 && m_llhs.empty());

      m_llhs.resize(nl);
      assert(int(m_llhs.size()) >= 1);

      for (int i = 0; i < nl; ++i)
        if (!isBOF)
        {
          // Construct Fokker-Planck-type LLH Params:
          typename CondLLH2D_FokkerPlanck<F>::Params llhParams
            (params.m_debugLevel, params.m_useDirichlet, params.m_minSbC);

          // Construct a Fokker-Planck-type LLH Object:
          m_llhs[i] =
            new CondLLH2D_FokkerPlanck<F>
                (diffParams, gridParamss[i], llhParams, *mktData);
        }
        else
          // Construct a BayesOptFilt-type LLH object directly (no LLH params):
          m_llhs[i] =
            new CondLLH2D_BayesOptFilt<F>
                (diffParams, gridParamss[i], *mktData, params.m_debugLevel);
    }
    assert(nl >= 1 && nl == int(m_llhs.size()));

    //-----------------------------------------------------------------------//
    // Create the Initial Guesses and Boundaries:                            //
    //-----------------------------------------------------------------------//
    // Those "coeffs" which are fixed will be left out and not optimised over:
    //
    assert(m_initVals.empty() && m_boundsLo .empty() && m_boundsUp.empty() &&
           m_varInds .empty() && m_fixedVals.empty() && m_fixedInds.empty());

    int n = int(params.m_initGuess.size());
    if (n == 0 || n != int(params.m_bounds.size()))
      throw invalid_argument
            ("RealCalibrator::Init: Invalid inits / bounds size(s)");

    for (int i = 0; i < n; ++i)
    {
      // Compute the actual initial guess for coeffs[i]:
      F iv = NaN<F>();
      switch (params.m_initGuess[i].m_type)
      {
        case RealCalibrParams<F>::CIGExpl:
          iv = params.m_initGuess[i].m_val; // As given explicitly
          break;
        case RealCalibrParams<F>::CIGTrendS:
          if (hasStats)
            iv = stats.m_trendS;            // "S" trend
          break;
        case RealCalibrParams<F>::CIGVolSTH:
          if (hasStats)
            iv = stats.m_volSTH;            // "S" trading-hours vol
          break;
        case RealCalibrParams<F>::CIGVarSTH:
          if (hasStats)
            iv = stats.m_volSTH * stats.m_volSTH;
          break;                            // "S" trading-hours variance
        case RealCalibrParams<F>::CIGVolSON:
          if (hasStats)
            iv = stats.m_volSON;            // "S" over-night vol
          break;
        case RealCalibrParams<F>::CIGVolSWEH:
          if (hasStats)
            iv = stats.m_volSWEH;           // "S" vol for week-ends/holidays
          break;
        default:
          assert(false);
      }
      // NB: "iv" may remain uninitialised if the required stats are missing:
      if (!IsFinite(iv))
        throw runtime_error
              ("Invalid InitialVal #" + to_string(iv) + ": hasStats=" +
               to_string(hasStats));

      // Compute the bounds for coeffs[i]:
      //
      F bLo = MkBound<F>
              (iv, params.m_bounds[i].m_typeLo, params.m_bounds[i].m_valLo);
      F bUp = MkBound<F>
              (iv, params.m_bounds[i].m_typeUp, params.m_bounds[i].m_valUp);

      // Verify the bounds:
      if (!(bLo <= iv && iv <= bUp))
      {
        char buff[128];
        sprintf(buff, "Var #%d: BoundLo=%f, InitVal=%f, BoundUp=%f",
                i, bLo, iv, bUp);
        throw runtime_error
              (string("RealCalibrator::Init: Inconsistent inits / bounds:\n") +
               buff);
      }
      // Now, if the curr coeff is not fixed, push its init value and bounds
      // into the Solver vectors; otherwise, memoise them in "fixed":
      //
      if (!params.m_bounds[i].IsFixed())
      {
        m_varInds  .push_back(i);
        m_initVals .push_back(double(iv));
        m_boundsLo .push_back(double(bLo));
        m_boundsUp .push_back(double(bUp));
      }
      else
      {
        m_fixedInds.push_back(i);
        m_fixedVals.push_back(iv);
      }
    }
    // All "coeffs" have been processed:
    int NV = int(m_varInds.size());
    if (NV == 0)
      throw runtime_error("RealCalibrator::Init: No variables to calibrate");

#   ifndef NDEBUG
    int NF = int(m_fixedInds.size());
    assert(NV + NF == n);
#   endif

    //-----------------------------------------------------------------------//
    // Other settings:                                                       //
    //-----------------------------------------------------------------------//
    m_calibrMethod    = params.m_calibrMethod;
    m_rectFrac        = params.m_rectFrac;
    m_maxGlobalPts    = params.m_maxGlobalPts;

    // MLSL-Specific Params:
    m_mlsl_maxRounds  = params.m_mlsl_maxRounds;
    m_mlsl_maxBest    = params.m_mlsl_maxBest;

    // Create Coeff Buffs: must be of full size "n":
    m_coeffBuffs.resize(nl);
    for (int i = 0; i < nl; ++i)
      m_coeffBuffs[i].resize(n);

    // Create Gradient and Hessian buffs: must be of full size "n" as well, as
    // they are interfaced to "NLLH". However, this is currently only required
    // for IPOpt methods, which in turn are only available for Analytical LLHs:
    //
    if (m_calibrMethod.find("IPOpt") != string::npos)
    {
      if (params.m_gridMethod != "Analytical")
        throw invalid_argument
              ("RealCalibrator::Init: IPOpt requires Analytical Grid/LLH");

      // If an Analytical LLH was successfully created, then the Diffusion must
      // be Analytical as well:
      assert(IsDiffusionAnalytical(diffParams));

      m_gradient.resize(n);
      m_hessian.resize (n, n);
    }

    //-----------------------------------------------------------------------//
    // Initialise the threading component:                                   //
    //-----------------------------------------------------------------------//
    // Threads start immediately -- unless nl=1, in which case evaluation is
    // to be performed synchronously, so no threads are created:
    //
    if (nl >= 2)
    {
      assert(m_thrds.empty() && m_thrdAvail.empty());
      m_thrds    .resize(nl);
      m_thrdAvail.resize(nl);
      for (int i = 0; i < nl; ++i)
      {
        m_thrdAvail[i] = true;
        m_thrds    [i] =
          new Thread([this, i]()->void { this->ThreadEvalBody(i); });
      }
    }
    // All done!
  }

  //=========================================================================//
  // Dtor:                                                                   //
  //=========================================================================//
  template<typename F>
  RealCalibrator<F>::~RealCalibrator()
  {
    // Set the "All Done" flag for all threads, and notify them:
    {
      LockGuard guard(m_wisMut);
      m_allDone = true;
      m_wisCV.notify_all();
    }

    // De-allocate all "LLH" objects, wait for all threads to terminate, and
    // destroy them. NB: once again, if nl <= 1, no threads were created; re-
    // call that nl == 0 in case of an analytical LLH function:
    //
    int    nl =  int(m_llhs.size());

    assert((nl >= 2 && nl == int(m_thrds.size()) &&
            nl == int(m_thrdAvail.size()))       ||
           (nl <= 1 && m_thrds.empty() && m_thrdAvail.empty()));

    for (int i = 0; i < nl; ++i)
    {
      // All threads must be idle by now:
      if (nl >= 2)
      {
        assert(m_thrdAvail[i]);
        m_thrds[i]->join();
        delete m_thrds[i];  m_thrds[i] = nullptr;
      }
      // In any case, de-allocate the LLHs:
      delete m_llhs [i];    m_llhs [i] = nullptr;
    }
  }

  //=========================================================================//
  // Accessors (with verification):                                          //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // "GetNV": Number of Variables to be Calibrated:                          //
  //-------------------------------------------------------------------------//
  template<typename F>
  int RealCalibrator<F>::GetNV() const
  {
    int    NV =  int(m_initVals.size());
    assert(NV == int(m_boundsLo.size()) && NV == int(m_boundsUp.size()) &&
           NV == int(m_varInds.size())  && NV >= 1);
    return NV;
  }

  //-------------------------------------------------------------------------//
  // "GetNF": Number of Fixed Coeffs:                                        //
  //-------------------------------------------------------------------------//
  template<typename F>
  int RealCalibrator<F>::GetNF() const
  {
    int    NF =  int(m_fixedVals.size());
    assert(NF == int(m_fixedInds.size()));
    return NF;
  }

  //-------------------------------------------------------------------------//
  // "GetN": Total Number of Coeffs (Variable and Fixed Ones):               //
  //-------------------------------------------------------------------------//
  template<typename F>
  int RealCalibrator<F>::GetN()  const
  {
    int    n  =  GetNV() + GetNF();
#   ifndef NDEBUG
    for (int i = 0; i < int(m_coeffBuffs.size()); ++i)
      assert(n ==int(m_coeffBuffs[i].size()));

    // The sizes of "m_gradient" and "m_hessian" must be either "n" or 0 (if
    // not allocated):
    int gs  = int(m_gradient.size());
    int hs1 = int(m_hessian.size1());
    int hs2 = int(m_hessian.size2());
    assert((gs  == 0 || gs  == n) && (hs1 == 0 || hs1 == n) &&
           (hs2 == 0 || hs2 == n));
#   endif
    return n;
  }

  //=========================================================================//
  // WorkFlow Management:                                                    //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // "SubmitWorkItem":                                                       //
  //-------------------------------------------------------------------------//
  // Called by the common Fortran call-back ("EvalReqOrSync"):
  //
  template<typename F>
  void RealCalibrator<F>::SubmitWorkItem(WorkItem const& wi) const
  {
    LockGuard guard(m_wisMut);
    m_wisQ.push_back(wi);
    m_wisCV.notify_one();
  }

  //-------------------------------------------------------------------------//
  // "Synchronise":                                                          //
  //-------------------------------------------------------------------------//
  // Called by the common Fortran call-back ("EvalReqOrSync"). Waits for the
  // Work Items Queue to become empty and all threads to become available:
  //
  template<typename F>
  void RealCalibrator<F>::Synchronise() const
  {
    UniqLock lock(m_wisMut);
    // Wait for all "m_thrdAvail" entries to become "true":
    m_doneCV.wait
    (
      lock,
      [this]()->bool
      {
        // XXX: Must wait for BOTH "wisQ" being empty and all threads becoming
        // available (otherwise there might be a race condition at the beginng
        // when the threads did not pick up their Work Item(s) yet):
        //
        return (this->m_wisQ.empty() &&
                find(this->m_thrdAvail.begin(), this->m_thrdAvail.end(), false)
                ==   this->m_thrdAvail.end());
      }
    );
    lock.unlock();
  }

  //=========================================================================//
  // "ThreadEvalBody":                                                       //
  //=========================================================================//
  // Infinite loop: Picking up and evaluating Work Items:
  //
  template<typename F>
  void RealCalibrator<F>::ThreadEvalBody(int thrdID) const
  {
    assert(0 <= thrdID   && thrdID < int(m_llhs.size()) &&
           m_llhs.size() == m_thrdAvail.size());
 
    int debugLevel = m_llhs[thrdID]->GetDebugLevel();

    while (true)
    {
      //---------------------------------------------------------------------//
      // Get the next Work Item:                                             //
      //---------------------------------------------------------------------//
      UniqLock lock(m_wisMut);

      // Before going to wait for a new Work Item, indicate that this thread is
      // currently available (but ONLY if the Queue is currently empty - other-
      // wise the thread may become unavailable immediately afterwards -- will
      // cause unnecessary notification):
      //
      if (m_wisQ.empty())
      {
        m_thrdAvail[thrdID] = true;

        if (debugLevel >= 1)
          cout << "Calibrator Thread #" << thrdID << ": Waiting for a WI"
               << endl;

        // Signal that this thread is done for now; only 1 thread (the main one)
        // may wait for that condition:
        m_doneCV.notify_one();
      }

      // Now wait (if necessary) for the next Work Item to become available,
      // or for the "All Done" flag to be set by the main thread:
      m_wisCV.wait
        (lock,
        [this]()->bool { return (!this->m_wisQ.empty() || this->m_allDone); });

      // If the "All Done" flag is set, terminate:
      if (m_allDone)
      {
        lock.unlock();
        break;
      }

      // Otherwise: Got a new Work Item; extract it:
      WorkItem wi = m_wisQ.front();
      m_wisQ.pop_front();

      // Mark the calling thread as being unavailable:
      m_thrdAvail[thrdID] = false;
      lock.unlock();

      // XXX: The following output is produced outside of the critical section
      // to reduce unnecessary blocking:
      if (debugLevel >= 1)
        cout << "Calibrator Thread #" << thrdID << ": Picked up a WI" << endl;

      //---------------------------------------------------------------------//
      // Assemble the full Diffusion Coeffs, and Perform Evaluation:         //
      //---------------------------------------------------------------------//
      // XXX: No Gradient or Hessian is currently required here:
      //
      EvalLLH(thrdID, GetNV(), wi.m_xs, wi.m_res, nullptr, nullptr, wi.m_flag);
    }
  }

  //=========================================================================//
  // "AssembleCoeffs":                                                       //
  //=========================================================================//
  // Re-constructs a vector of Diffusion Coeffs from both variable ("double"
  // array) and Fixed (F-vector) ones:
  //
  template<typename F>
  void RealCalibrator<F>::AssembleCoeffs(double const* xs, Vector<F>* res) const
  {
    assert(xs != nullptr && res != nullptr);

    int NV = GetNV();
    int NF = GetNF();
#   ifndef NDEBUG
    int n  = GetN();
    assert(n == NV + NF && int(res->size()) == n);
#   endif

    // Install the variable part:
    for (int j = 0; j < NV; ++j)
    {
      int i = m_varInds[j];
      assert(0 <= i && i < n);
      (*res)[i] = F(xs[j]);
    }

    // Install the fixed part:
    for (int j = 0; j < NF; ++j)
    {
      int i = m_fixedInds[j];
      assert(0 <= i && i < n);
      (*res)[i] = F(m_fixedVals[j]);
    }
  }

  //=========================================================================//
  // "EvalLLH":                                                              //
  //=========================================================================//
  // Actual evaluation of the Cost Function occurs here: LLH object is evaled:
  //
  template<typename F>
  void RealCalibrator<F>::EvalLLH
  (
    int             thrdID,
    int             DEBUG_ONLY(NV),
    double const*   xs,        // Non-NULL,              size = NV
    double*         res,       // Non-NULL,              size = 1
    Vector<double>* gradient,  // May be NULL, otherwise size = NV
    Matrix<double>* hessian,   // May be NULL, otherwise size = NV^2
    int*            flag       // May be NULL
  )
  const
  {
    //-----------------------------------------------------------------------//
    // Prepare the Args:                                                     //
    //-----------------------------------------------------------------------//
    assert(xs != nullptr && res != nullptr && NV >= 1 && NV == GetNV());

    int nl = int(m_llhs.size());
    assert(nl == 1 ||
          (nl == int(m_thrds.size()) && nl == int(m_thrdAvail.size())));

    if (thrdID < 0 || thrdID >= nl)
      throw invalid_argument("RealCalibrator::EvalLLH: Invalid ThrdID");

    // If Hessian is to be filled, then Gradient must be filled as well:
    if (gradient == nullptr && hessian != nullptr)
      throw invalid_argument
            ("RealCalibrator::EvalLLH: Hessian requires Gradient");

    // Assemble the Diffusion Coeffs:
    Vector<F>& coeffs = m_coeffBuffs[thrdID]; // Alias!

    int    n = int(coeffs.size());
#   ifndef NDEBUG
    assert(n == GetN());
#   endif
    AssembleCoeffs(xs, &coeffs);

    // Construct the LLH Object to be evaluated:
    CondLLH<F> const& llh = *(m_llhs[thrdID]);            // Alias!
    int debugLevel  = llh.GetDebugLevel();

    // Construct the Args. If "gradient" and "hessian" are not required by the
    // Caller, they are not requested from NLLH either.
    // NB: "m_gradient" and "m_hessian" are to be interfaced to "NLLH", so they
    // must be of FULL SIZE (n and n^2, resp, rather than NV and NV^2), unless
    // the sizes are 0 (not allocated):
    F val = Inf<F>();

    int gs  = int(m_gradient.size());
    int hs1 = int(m_hessian.size1());
    int hs2 = int(m_hessian.size2());

    assert((gs  == 0 || gs  == n) && (hs1 == 0 || hs1 == n) &&
           (hs2 == 0 || hs2 == n));

    Vector<F>* grad_ptr  =
      (gradient != nullptr && gs == n)
      ? const_cast<Vector<F>*>(&m_gradient)
      : nullptr;

    Matrix<F>* hessn_ptr =
      (hessian  != nullptr && hs1 == n && hs2 == n)
      ? const_cast<Matrix<F>*>(&m_hessian)
      : nullptr;

    //-----------------------------------------------------------------------//
    // RUN "NLLH" (Negated LLH, Minimised to Maximise the LLH):              //
    //-----------------------------------------------------------------------//
    try
    {
      // XXX: Filtered "vs" values are currently not used (hence the corresp
      // NULL argument):
      llh.NLLH(coeffs, &val, grad_ptr, hessn_ptr, nullptr);

      if (debugLevel >= 1)
        cout << "\n===> Thrd #" << thrdID << ": " << coeffs << " ===> "
             << val << endl << endl;

      // NB: +oo is regarded to be a valid result:
      if (!IsFinite(val) && val != Inf<F>())
        throw runtime_error("RealCalibrator::EvalLLH: Evaluation Failed");

      // Set the flag (if available) indicating successful evaluation:
      if (flag != nullptr)
         *flag = 0;
    }
    catch(exception const& exc)
    {
      if (debugLevel >= 1)
        cout << "\n===> Thrd #" << thrdID << ": WARNING: LLH FUNC EVAL ERROR: "
             << coeffs << " ===> " << exc.what() << endl << endl;

      // Set the error flag (if available), otherwise re-throw an excepion:
      if (flag != nullptr)
      {
        *flag = 1;
        return;
      }
      else
        throw;
    }
    //-----------------------------------------------------------------------//
    // Convert the results back to "double" (with projection: n -> NV):      //
    //-----------------------------------------------------------------------//
    // NB: The output of "NLLH" is of size "n", whereas the "EvalLLH" output is
    // to be of size "NV". XXX: Thus, some Gradient / Hessian components (those
    // corresponding to Fixed coeffs) may be wasted. TODO: Pass a list of requ-
    // ired indices to "NLLH":
    //
    *res = double(val);

    if (gradient != nullptr)
    {
      int NV = GetNV();
      for (int j = 0; j < NV; ++j)
      {
        int i = m_varInds[j];
        assert(0 <= i && i < n);
        (*gradient)[j] = double(m_gradient[i]);

        if (hessian != nullptr)
          for (int l = 0; l < NV; ++l)
          {
            int k = m_varInds[l];
            (*hessian)(j,l) = double(m_hessian(i,k));
          }
      }
    }
    // All done!
  }

  //=========================================================================//
  // "Calibrate":                                                            //
  //=========================================================================//
  // Top-Level Request for Calibration; "opt_args" is the output vector of opt-
  // imised args of the cost function;  "opt_val"  is the optimal cost itself:
  //
  template<typename F>
  void RealCalibrator<F>::Calibrate(Vector<F>* opt_args, F* opt_val)
  {
    assert(opt_args != nullptr && opt_val != nullptr);
    int  NV  = GetNV();
    int  n   = GetN ();

    // NB: Resize the output automatically -- it may be inconvenient to do from
    // the caller, as the latter would need to examine the Diffusion...:
    opt_args->resize(n);

    // Result and Return Code (for all methods):
    vector<double> xs  = m_initVals;
    double         res = Inf<double>();

    // Determine the DebugLevel for the top-level Minimiser:
    int debugLevel = 0;
    for (int i = 0; i <int( m_llhs.size()); ++i)
      debugLevel = max<int>(debugLevel, m_llhs[i]->GetDebugLevel());

    //-----------------------------------------------------------------------//
    if (m_calibrMethod == "MLSL-IPOpt")
    //-----------------------------------------------------------------------//
      // Run MLSL-IPOpt:
      Run_MLSL_IPOpt (NV, xs.data(), &res, debugLevel);
    else
    //-----------------------------------------------------------------------//
    if (m_calibrMethod == "MLSL-E04JYF")
    //-----------------------------------------------------------------------//
      // Run MLSL-E04JYF:
      Run_MLSL_E04JYF(NV, xs.data(), &res, debugLevel);
    else
    //-----------------------------------------------------------------------//
    if (m_calibrMethod == "DIRect-E04JYF")
    //-----------------------------------------------------------------------//
    {
      // Initial     Global Optimisation: DIRect:
      Run_DIRect(NV, xs.data(), &res, debugLevel);

      // "Polishing" Local  Optimisation: E04JYF: Catch standard exceptions:
      try
      {
        Run_E04JYF(NV, xs.data(), &res, debugLevel);
      }
      catch (exception const& e)
      {
        cout << "WARNING: E04JYF Failed: " << e.what() << endl;
      }
    }
    else
    //-----------------------------------------------------------------------//
    if (m_calibrMethod == "DIRect-IPOpt")
    //-----------------------------------------------------------------------//
    {
      // Initial     Global Optimisation: DIRect:
      Run_DIRect(NV, xs.data(), &res, debugLevel);

      // "Polishing" Local  Optimisation: IPOpt: Catch standard exceptions:
      try
      {
        Run_IPOpt (NV, xs.data(), &res, debugLevel);
      }
      catch (exception const& e)
      {
        cout << "WARNING: IPOpt Failed: " << e.what() << endl;
      }
    }
    else
      throw invalid_argument
            ("RealCalibrator::Calibrate: Invalid method: "+ m_calibrMethod);

    //-----------------------------------------------------------------------//
    // Return the results:                                                   //
    //-----------------------------------------------------------------------//
    assert(opt_args != nullptr && opt_val != nullptr);

    // Assemble the results in "opt_args" (including const values). The optimi-
    // sation results proper are in "xs":
    //
    AssembleCoeffs(xs.data(), opt_args);

    // Store the cost function value:
    *opt_val = F(res);
  }

  //=========================================================================//
  // "Run_DIRect" (Global Optimisation):                                     //
  //=========================================================================//
  template<typename F>
  void RealCalibrator<F>::Run_DIRect
    (int NV, double* xs, double* res, int UNUSED_PARAM(debugLevel)) const
  {
    assert(NV >= 1 && xs != nullptr && res != nullptr);

    double  const eps       = 1e-4;
    int     const maxEvs    = m_maxGlobalPts * NV;
    int     const maxIts    = maxEvs / 10;
    int     const methodVar = 1;        // Modified version (Gablonsky)
    int     const logFD     = 6;        // "stdout" in Fortran terms
    double  const fGlob     = -1e100;   // Unknown  in advance
    double  const fRel      = 0.0;
    double  const minVol    = Pow(double(m_rectFrac), double(NV));
    double  const minSgm    = minVol;
    int     const Li        = 0;
    int     const Ld        = 0;
    int     const Lc        = 0;
    int           rc        = 0;

    // Invoke Fortran:
    direct_
    (
      DIRectEval<F>,      xs,           &NV,      &eps,
      &maxEvs,            &maxIts,      res,      m_boundsLo.data(),
      m_boundsUp.data(),  &methodVar,   &rc,      &logFD,
      &fGlob,             &fRel,        &minVol,  &minSgm,
      (int const*)(this), &Li,          nullptr,  &Ld,
      nullptr,            &Lc
    );

    // rc < 0 means a "hard error" within DIRect;  rc == 0 is full success,
    // rc > 0 means optimisation stopped eg because the localisation volume
    // has been achived:
    if (rc < 0)
      RaiseOptimError("DIRect", rc);
  }

  //=========================================================================//
  // "Run_E04JYF" (Local Optimisation):                                      //
  //=========================================================================//
  template<typename F>
  void RealCalibrator<F>::Run_E04JYF
    (int NV, double* xs, double* res, int UNUSED_PARAM(debugLevel)) const
  {
    assert(NV >= 1 && xs != nullptr && res != nullptr);

    int const       ibound  = 0;
    int const       Liw     = NV + 2;
    vector<int>     iw(Liw);
    int const       Lw      = max<int>((NV*(NV-1))/2 + 12*NV, 13);
    vector<double>  w(Lw);
    int             rc      = -1;
    // NB: Setting rc=-1 prevents E04JYF from terminating the whole application
    // in case of an error. In this case, NAG E04JYF always returns; on return,
    // "rc" is set to the error code which we examine:
    // Invoke Fortran:
    e04jyf_
    (
      &NV,     &ibound,   NAGEval<F>, m_boundsLo.data(), m_boundsUp.data(),
      xs,      res,       iw.data(),  &Liw,    w.data(), &Lw,
      (int const*)(this), nullptr,    &rc
    );

    // Check the result code. Raise an error ONLY if there was a guaranteed
    // failure (even in this case, this exception can be caught and ignored
    // by the caller):
    if (rc == 3)
      RaiseOptimError("E04JYF", rc);
  }

  //=========================================================================//
  // "Run_IPOpt" (Local Optimisation):                                       //
  //=========================================================================//
  template<typename F>
  void RealCalibrator<F>::Run_IPOpt
    (int NV, double* xs, double* res, int debugLevel) const
  {
    using namespace Ipopt;

    // Create a minimisation problem instance referring to "this" Calibrator:
    IPOptEval<F>*  optRaw = new IPOptEval<F>(this, NV, xs, debugLevel);
    SmartPtr<TNLP> opt(optRaw);

    // Create a minimisation application:
    SmartPtr<IpoptApplication> app = IpoptApplicationFactory();

    // Set some options:
    app->Options()->SetNumericValue("tol", 1e-9);
    app->Options()->SetStringValue ("mu_strategy",     "adaptive");
    app->Options()->SetStringValue ("derivative_test", "second-order");

    // Initialise the application:
    ApplicationReturnStatus status = app->Initialize();
    if (status != Solve_Succeeded)
      throw runtime_error
            ("RealCalibrator::Run_IPOpt: IPOpt app init failed");
    try
    {
      // Run the "IPOpt" Minimiser:
      status = app->OptimizeTNLP(opt);

      // Try to extract the solution: this will propagate an exception if
      // "finalize_solution" was not called for any reason:
      optRaw->GetSolution(NV, xs, res);
    }
    catch(...)
    {
      // Normally, "OptimizeTNLP" should not propagate any exceptions.
      // This clause is just for extra safety (see above):
      status = Unrecoverable_Exception;
    }
    if (status != Solve_Succeeded                    &&
        status != Solved_To_Acceptable_Level         &&
        status != Search_Direction_Becomes_Too_Small &&
        status != User_Requested_Stop                &&
        status != Feasible_Point_Found               &&
        status != Maximum_Iterations_Exceeded)
      // Signal an IPOpt failure to the caller:
      throw runtime_error
            ("RealCalibrator::Run_IPOpt: IPOPt Status = "+ to_string(status));
  }

  //=========================================================================//
  // "Run_MLSL_E04JYF": MLSL Global Driver with E04JYF Local Optimiser:      //
  //=========================================================================//
  template<typename F>
  void RealCalibrator<F>::Run_MLSL_E04JYF
    (int NV, double* xs, double* res, int debugLevel) const
  {
    assert(NV >= 1 && xs != nullptr && res != nullptr);

    //-----------------------------------------------------------------------//
    // Create the MLSL Local Optimiser (a Callable Object):                  //
    //-----------------------------------------------------------------------//
    MLSL::LocalMinimiser LM
    (
      [this, NV, xs, res, debugLevel]
        (int nv, double* lxs, double* lres, int* ret, bool full_minimisation)
      -> void
      {
        if (!full_minimisation)
        {
          //-----------------------------------------------------------------//
          // Single Eval or Sync:                                            //
          //-----------------------------------------------------------------//
          assert
             // Eval:
            ((nv == 0  && lxs == nullptr && lres == nullptr && ret == nullptr)
             ||
             // Sync:
             (nv == NV && lxs != nullptr && lres != nullptr && ret != nullptr));

          // This mode is used at MLSL start-up: Cost Function evaluation, or
          // synchronisation (depending on whether "n" is non-0). There is no
          // Gradient or Hessian:
          //
          EvalReqOrSync<F>
            (&nv, lxs, lres, nullptr, nullptr,  ret,
             const_cast<void*>((void const*)(this)));
        }
        else
        { //-----------------------------------------------------------------//
          // Perform local minimisation with "E04JYF":                       //
          //-----------------------------------------------------------------//
          assert(nv  == NV && lxs != nullptr && lres != nullptr &&
                 ret != nullptr);
          try
          {
            this->Run_E04JYF(NV, lxs, lres, debugLevel);
            *ret = MLSL::SUCCESS;
          }
          catch(...)
            { *ret = MLSL::FAILURE; }
        }
      }
    );
    //-----------------------------------------------------------------------//
    // Run the MLSL Global Driver:                                           //
    //-----------------------------------------------------------------------//
    MLSL::Minimise
    (
      LM, NV, m_boundsLo.data(), m_boundsUp.data(), xs, res,
      m_mlsl_maxRounds, m_maxGlobalPts * NV, m_mlsl_maxBest, debugLevel
    );
  }

  //=========================================================================//
  // "Run_MLSL_IPOpt": MLSL Global Driver with IPOpt Local Optimiser:        //
  //=========================================================================//
  // NB: "IPOpt" is for Analytical LLHs only. XXX: Also, it is not CUDA-enabled
  // (because IPOpt is not):
  //
  template<typename F>
  void RealCalibrator<F>::Run_MLSL_IPOpt
    (int NV, double* xs, double* res, int debugLevel) const
  {
    assert(NV >= 1 && xs != nullptr && res != nullptr);

    //-----------------------------------------------------------------------//
    // Create the MLSL Local Optimiser (a Callable Object):                  //
    //-----------------------------------------------------------------------//
    MLSL::LocalMinimiser LM
    (
      [this, NV, debugLevel]
        (int nv, double* lxs, double* lres, int* ret, bool full_minimisation)
      -> void
      {
        if (!full_minimisation)
        {
          //-----------------------------------------------------------------//
          // Single Eval or Sync:                                            //
          //-----------------------------------------------------------------//
          assert
             // Sync:
            ((nv == 0  && lxs == nullptr && lres == nullptr && ret == nullptr)
             ||
             // Eval:
             (nv == NV && lxs != nullptr && lres != nullptr && ret != nullptr));

          // This mode is used at MLSL start-up: Cost Function evaluation, or
          // synchronisation (depending on whether "n" is non-0). There is no
          // Gradient or Hessian:
          //
          EvalReqOrSync<F>
            (&nv, lxs, lres, nullptr, nullptr, ret,
             const_cast<void*>((void const*)(this)));
        }
        else
        {
          //-----------------------------------------------------------------//
          // Perform local minimisation with "IPOpt":                        //
          //-----------------------------------------------------------------//
          assert
            (nv == NV && lxs != nullptr && lres != nullptr && ret != nullptr);
          try
          {
            this->Run_IPOpt(nv, lxs, lres, debugLevel);
            *ret = MLSL::SUCCESS;
          }
          catch(...)
            { *ret = MLSL::FAILURE; }
        }
      }
    );
    //-----------------------------------------------------------------------//
    // Run the MLSL Global Driver:                                           //
    //-----------------------------------------------------------------------//
    MLSL::Minimise
    (
      LM, NV, m_boundsLo.data(), m_boundsUp.data(), xs, res,
      m_mlsl_maxRounds, m_maxGlobalPts * NV, m_mlsl_maxBest,  debugLevel
    );
  }
}
