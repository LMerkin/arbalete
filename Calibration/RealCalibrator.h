// vim:ts=2:et
//===========================================================================//
//                             "RealCalibrator.h":                           //
//    Calibration of StocVol Diffusion Params in Real Measure, i.e. Using    //
//       Maximisation of Conditional LLH Functions on "S" Observations       //
//===========================================================================//
#pragma once

#include "Common/DateTime.h"
#include "Common/VecMatTypes.h"
#include "Common/Threading.h"
#include "PDF/CondLLH.h"
#include "Calibration/RealCalibrParams.h"
#include <string>
#include <vector>
#include <deque>

namespace Arbalete
{
  //=========================================================================//
  // "RealCalibrator":                                                       //
  //=========================================================================//
  template<typename F>
  class RealCalibrator
  {
  public:
    //-----------------------------------------------------------------------//
    // "Work Item" to evaluate:                                              //
    //-----------------------------------------------------------------------//
    // NB: "xs" are the args (variables only, no constant params) which need to
    // be COPIED into the "WorkItem" from the caller as they may be overwritten
    // by the caller during the async evaluation. On the contrary, "res" ptr is
    // provided directly by the caller and is assumed to be safe to use  -- the
    // result is not to be consumed or overwritten before synchronisation:
    //
    // NB: "m_gradient", "m_hessian" and "m_flag" may be NULL if not used:
    //
    struct WorkItem
    {
      static int const MaxSize = 32;
      double           m_xs[MaxSize]; // Args for this evaluation: a COPY!!!
      int              m_Nx;          // Actual size of "m_xs"   (<= MaxSize)
      double*          m_res;         // Address to store the Func Value
      Vector<double>*  m_gradient;    // Address to store the Gradient
      Matrix<double>*  m_hessian;     // Address to store the Hessian
      int*             m_flag;        // Address to store the res flag (0=OK)

      // Default Ctor:
      WorkItem()
      : m_Nx      (0),
        m_res     (nullptr),
        m_gradient(nullptr),
        m_hessian (nullptr),
        m_flag    (nullptr)
      {}
    };

  private:
    //-----------------------------------------------------------------------//
    // Data Flds:                                                            //
    //-----------------------------------------------------------------------//
    bool                            m_allDone;      // Terminate all threads
    std::string                     m_calibrMethod; // Optim method name
    std::vector<CondLLH<F> const*>  m_llhs;         // LLH Evaluators
    std::vector<double>             m_initVals;     // Initial vals for vars
    std::vector<double>             m_boundsLo;     // Low   bounds for vars
    std::vector<double>             m_boundsUp;     // Upper bounds for vars
    std::vector<int>                m_varInds;      // Inds of vars
    std::vector<double>             m_fixedVals;    // Vals of fixed params
    std::vector<int>                m_fixedInds;    // Inds of fixed params
    F                               m_rectFrac;     // Stopping param, per dim
    int                             m_maxGlobalPts; // Stopping param, per dim

    // MLSL-specific params (see MLSL.h for comments):
    int                             m_mlsl_maxRounds;
    int                             m_mlsl_sampleSz;
    int                             m_mlsl_maxBest;

    // Buffer data structs for double <-> F convers (only used if F != double):
    Vector<F>                       m_gradient;
    Matrix<F>                       m_hessian;

    // Queue of "Work Items" waiting for evaluation, protected by a mutex and
    // a cond var:
    mutable std::deque<WorkItem>    m_wisQ;
    mutable Mutex                   m_wisMut;
    mutable CondVar                 m_wisCV;

    // Pool of evaluation threads, with availability flags and a cond var used
    // to wait on those flags:
    std::vector<Thread*>            m_thrds;
    mutable std::vector<bool>       m_thrdAvail;
    mutable CondVar                 m_doneCV;

    // Per-thread buffers for assembling Diffusion Coeffs:
    mutable std::vector<Vector<F>>  m_coeffBuffs;

    //-----------------------------------------------------------------------//
    // Ctor Implementation:                                                  //
    //-----------------------------------------------------------------------//
    void Init(RealCalibrParams<F> const& params);

    //-----------------------------------------------------------------------//
    // For Calibration Threads:                                              //
    //-----------------------------------------------------------------------//
    void AssembleCoeffs(double const* xs, Vector<F>* res) const;

    // Body of the Calibration Threads:
    void ThreadEvalBody(int thrdID) const;

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctors, Dtor:                                              //
    //-----------------------------------------------------------------------//
    // With Initial Values and Bounds specified explicitly:
    //
    RealCalibrator(RealCalibrParams<F> const& params)
      { Init(params); }

    // With all params provided in an XML file:
    //
    RealCalibrator(std::string const& xml_file)
      { Init(RealCalibrParams<F>::FromXML(xml_file)); }

    ~RealCalibrator();

    //-----------------------------------------------------------------------//
    // Accessors:                                                            //
    //-----------------------------------------------------------------------//
    std::vector<CondLLH<F> const*> const& GetLLHs() const   { return m_llhs; }

    std::string const&  GetCalibrMethod()     const { return m_calibrMethod; }

    // The Number of Variables to be calibrated (as opposed to Fixed Values):
    int GetNV() const;

    // The Number of Fixed Coeffs:
    int GetNF() const;

    // The total number of coeffs (variable / calibrated and fixed ones):
    int GetN()  const;

    std::vector<double> const& GetInitVals()  const { return m_initVals;  }
    std::vector<double> const& GetBoundsLo()  const { return m_boundsLo;  }
    std::vector<double> const& GetBoundsUp()  const { return m_boundsUp;  }
    std::vector<double> const& GetFixedVals() const { return m_fixedVals; }

    std::vector<int>    const& GetVarInds()   const { return m_varInds;   }
    std::vector<int>    const& GetFixedInds() const { return m_fixedInds; }

    //-----------------------------------------------------------------------//
    // Evaluator:                                                            //
    //-----------------------------------------------------------------------//
    // XXX InHomogeneous API: "xs" is an array ptr (of size "NV"),     whereas
    // "gradient" and "hessian" are Vector / Matrix ptrs. This is for internal
    // optimisation reasons:
    //
    void EvalLLH
    (
      int             thrdID,
      int             NV,         // Just for extra safety
      double const*   xs,         // Non-NULL,              size = NV
      double*         res,        // Non-NULL,              size = 1
      Vector<double>* gradient,   // May be NULL, otherwise size = NV
      Matrix<double>* hessian,    // May be NULL, otherwise size = NV^2
      int*            flag        // May be NULL
    )
    const;

    //-----------------------------------------------------------------------//
    // "Calibrate":                                                          //
    //-----------------------------------------------------------------------//
    // Top-level request for calibration. The args are output locations; on
    // return, "opt_args" will be of size given by GetN() and will  contain
    // ALL coeffs in the proprer order (both calibrated variables and fixed
    // ones):
    void Calibrate(Vector<F>* opt_args, F* opt_val);

    //-----------------------------------------------------------------------//
    // Call-Backs from Fortran Calibration Routines:                         //
    //-----------------------------------------------------------------------//
    // Submit a Work Item for (asynchronous) evaluation (XXX: this method is
    // declared "const" because the queue of Work Items is "mutable"):
    //
    void SubmitWorkItem(WorkItem const& wi) const;

    // Synchronisation: Wait for completion of all previously submitted Work
    // Items:
    void Synchronise() const;

  private:
    //-----------------------------------------------------------------------//
    // Internal Methods:                                                     //
    //-----------------------------------------------------------------------//
    // Invocation of actual optimisation routines:
    //
    void Run_DIRect     (int NV, double* xs, double* res, int debugLevel) const;
    void Run_E04JYF     (int NV, double* xs, double* res, int debugLevel) const;
    void Run_IPOpt      (int NV, double* xs, double* res, int debugLevel) const;
    void Run_MLSL_E04JYF(int NV, double* xs, double* res, int debugLevel) const;
    void Run_MLSL_IPOpt (int NV, double* xs, double* res, int debugLevel) const;
  };
}
