// vim:ts=2:et
//===========================================================================//
//                               "StatsSV.hpp":                              //
//        Statistics Accumulator for Non-Uniform Regime-Switching Data       //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Calibration/StatsSV.h"
#include <boost/math/special_functions/gamma.hpp>

namespace
{
  using namespace Arbalete;

  //-------------------------------------------------------------------------//
  // "VolErrFactor":                                                         //
  //-------------------------------------------------------------------------//
  template<typename F>
  inline F VolErrFactor(int n)
  {
    F lgn = boost::math::lgamma<F>(F(n)  /F(2.0));
    F lgd = boost::math::lgamma<F>(F(n-1)/F(2.0));
    return SqRt(F(1.0) - F(2.0)/F(n-1) * Exp(F(2.0) * (lgn - lgd)));
  }

  //-------------------------------------------------------------------------//
  // "StdDev":                                                               //
  //-------------------------------------------------------------------------//
  // Computes a Variance estimate over non-uniform time intervals:
  // "X" is "S" or "Vol", for example:
  //
  template<typename F>
  inline F StdDev(F sumDX, F sumDX2, F sumDT, F sumDT2, F sumDXDT)
  {
    assert(sumDT > F(0.0));
    F mean = sumDX / sumDT;

    // Correction factor to get an unbiased variance estimate:
    F C     = F(1.0) / (F(1.0) - sumDT2 / sumDT / sumDT);

    // Variance and Vol:
    F var   = C / sumDT * (sumDX2 - mean * (F(2.0) * sumDXDT - mean * sumDT2));
    assert(var >= F(0.0));
    return SqRt(var);
  }
}

namespace Arbalete
{
  using namespace std;

  //-------------------------------------------------------------------------//
  // "StatsAccumulatorSV": Default Ctor:                                     //
  //-------------------------------------------------------------------------//
  // Arg: windows size for short-term vol estimations.  NB: "S" are in general
  // the TRANSFORMED DIFFERENTIAL values (e.g. returns, rates differences etc),
  // with approximately normal distribution -- so not just spots:
  //
  template<typename F>
  StatsAccumulatorSV<F>::StatsAccumulatorSV(int vol_win_size)
  : m_sumDT     (F(0.0)),       // Total time duration
    m_sumDS     (F(0.0)),       // Sum of all "dS" vals -- for the mean

    m_nTH       (0),            // Number of Trading Hrs time instants / vals
    m_sumDSTH   (F(0.0)),       // Sum of Trading Hrs "dS" vals
    m_sumDSTH2  (F(0.0)),       // Sum of Trading Hrs dS^2 vals
    m_sumDTTH   (F(0.0)),       // Cumulative Trading Hrs time
    m_sumDTTH2  (F(0.0)),       // Sum of dt^2 during Trading Hours
    m_sumDSDTTH (F(0.0)),       // Sum of dS dt vals during Trading Hours

    m_nON       (0),            // Number of Over-Night time instants / vals
    m_sumDSON   (F(0.0)),       // Sum of Over-Night "dS" vals
    m_sumDSON2  (F(0.0)),       // Sum of Over-Night dS^2 vals
    m_sumDTON   (F(0.0)),       // Cumulative  Over-Night time
    m_sumDTON2  (F(0.0)),       // Sum of dt^2 during Over-Night time
    m_sumDSDTON (F(0.0)),       // Sum of dS dt vals during Trading Hours

    m_nWEH      (0),            // Number of Week-End/Holidays insts / vals
    m_sumDSWEH  (F(0.0)),       // Sum of Week-End/Holiday "dS" vals
    m_sumDSWEH2 (F(0.0)),       // Sum of Week-End/Holiday dS^2 vals
    m_sumDTWEH  (F(0.0)),       // Cumulative Week-End/Holiday time
    m_sumDTWEH2 (F(0.0)),       // Sum of dt^2  during Week-End/Holiday time
    m_sumDSDTWEH(F(0.0)),       // Sum of dS dt vals during Week-Ends/Holidays

    m_Win       (),             // Window itself: queue of (dS, dt) pairs
    m_instVols  (),             // Estimated "inst" vols accumulated here
    m_nWin      (vol_win_size), // Max size of the above window
    m_sumDSWin  (F(0.0)),       // Sum of "dS"  vals in the window
    m_sumDSWin2 (F(0.0)),       // Sum of dS^2  vals in the window
    m_sumDTWin  (F(0.0)),       // Cumulative  time  in the window
    m_sumDTWin2 (F(0.0)),       // Sum of dt^2  vals in the window
    m_sumDSDTWin(F(0.0)),       // Sum of dS dt vals in the window

    m_nVol      (0),            // Number of short-term vol estimates made
    m_sumDV     (F(0.0)),       // Sum of  "dV" vals
    m_sumDV2    (F(0.0)),       // Sum of  dV^2 vals
    m_sumDTVol  (F(0.0)),       // Total time over which "dV"s are available
    m_sumDTVol2 (F(0.0)),       // Sum of  dt^2 vals for the corresp "dV"s
    m_sumDVDT   (F(0.0))        // Sum of  the corresp dV dt vals
  {
    if (vol_win_size <= 1)
      throw invalid_argument("StatsAccumulatorSV Ctor: SV Window too small");
  }

  //-------------------------------------------------------------------------//
  // Statistics Update:                                                      //
  //-------------------------------------------------------------------------//
  template<typename F>
  void StatsAccumulatorSV<F>::UpdateStats
    (DateTime t, F dS, Time dT, TradingCalendar::CalReg reg)
  {
    // Update the over-all stats:
    F   dt  = YF<F>(dT);
    if (dt <= F(0.0))
      throw invalid_argument("StatsAccumulatorSV::UpdateStats: Invalid dT");

    assert(IsFinite(dS));
    F dS2   = dS * dS;
    F dt2   = dt * dt;
    F dSdt  = dS * dt;

    // For the over-all mean:
    m_sumDT  += dt;
    m_sumDS  += dS;

    // Update the regime-specific vol stats:
    switch (reg)
    {
    //-----------------------------------------------------------------------//
    // Trading Hours:                                                        //
    //-----------------------------------------------------------------------//
    case TradingCalendar::TradingHours:
      m_nTH++;
      m_sumDSTH   += dS;
      m_sumDSTH2  += dS2;
      m_sumDTTH   += dt;
      m_sumDTTH2  += dt2;
      m_sumDSDTTH += dSdt;

      // Also, update the "SWin" -- short-term windows for estimating the
      // "instanteneous" vols:
      //
      if (int(m_Win.size()) < m_nWin)
      {
        // Push the new (dS, dt) pair at the back of the queue:
        m_Win.push_back(pair<F,F>(dS, dt));

        // Increment the accumuators:
        m_sumDSWin    += dS;
        m_sumDSWin2   += dS2;
        m_sumDTWin    += dt;
        m_sumDTWin2   += dt2;
        m_sumDSDTWin  += dSdt;

        // If the window is now full, compute the first Vol:
        if (int(m_Win.size()) == m_nWin)
        {
          F vol = StdDev<F>(m_sumDSWin, m_sumDSWin2, m_sumDTWin, m_sumDTWin2,
                            m_sumDSDTWin);
          assert(m_instVols.empty());
          m_instVols.push_back(pair<DateTime,F>(t,vol));
        }
      }
      else
      {
        // Window is full, "instVols" are not empty:
        assert(int(m_Win.size()) == m_nWin && !m_instVols.empty());

        // Pop the oldest (dS, dt) pair out of the window front:
        pair<F,F> old = m_Win.front();
        m_Win.pop_front();

        // Store the new  (dS, dt) pair at the window back:
        m_Win.push_back(pair<F,F>(dS, dt));
        assert(int(m_Win.size()) == m_nWin);

        // Differential update to the accumulators:
        F dSo   = old.first;
        F dto   = old.second;
        F dSo2  = dSo * dSo;
        F dto2  = dto * dto;
        F dSdto = dSo * dto;

        m_sumDSWin    += (dS   - dSo  );
        m_sumDSWin2   += (dS2  - dSo2 );
        m_sumDTWin    += (dt   - dto  );
        m_sumDTWin2   += (dt2  - dto2 );
        m_sumDSDTWin  += (dSdt - dSdto);

        // Compute the new vol and dV:
        F vol     = StdDev<F>(m_sumDSWin, m_sumDSWin2, m_sumDTWin, m_sumDTWin2,
                              m_sumDSDTWin);
        F dV      = vol - m_instVols.back().second;
        m_instVols.push_back(pair<DateTime,F>(t, vol));

        // Update the "dV" accumulators:
        m_sumDV     += dV;
        m_sumDV2    += dV * dV;
        m_sumDTVol  += dt;
        m_sumDTVol2 += dt2;
        m_sumDVDT   += dV * dt;
      }
      break;

    //-----------------------------------------------------------------------//
    // Over-Night and Week-End / Holiday Periods:                            //
    //-----------------------------------------------------------------------//
    // NB: Vol-of-vol is not measured for them:
    //
    case TradingCalendar::OverNight:
      m_nON++;
      m_sumDSON     += dS;
      m_sumDSON2    += dS2;
      m_sumDTON     += dt;
      m_sumDTON2    += dt2;
      m_sumDSDTON   += dSdt;
      break;

    case TradingCalendar::WeekEndHol:
      m_nWEH++;
      m_sumDSWEH    += dS;
      m_sumDSWEH2   += dS2;
      m_sumDTWEH    += dt;
      m_sumDTWEH2   += dt2;
      m_sumDSDTWEH  += dSdt;
      break;

    default:
      assert(false);
    }
  }
 
  //-------------------------------------------------------------------------//
  // Statistics Evaluation:                                                  //
  //-------------------------------------------------------------------------//
  template<typename F>
  StatsSV<F> StatsAccumulatorSV<F>::operator()() const
  {
    if (m_nTH <= 1 || m_nON <= 1 || m_nWEH <= 1)
      throw runtime_error("StatsAccumulator(): Too few data items");
    assert(m_sumDT > F(0.0));

    StatsSV<F> res;

    // Over-all "S" Trend:
    res.m_trendS = m_sumDS / m_sumDT;

    // Trading Hours "S" Vol and its error:
    res.m_volSTH =
      StdDev<F>(m_sumDSTH,  m_sumDSTH2,  m_sumDTTH,  m_sumDTTH2,  m_sumDSDTTH);

    res.m_errVolSTH     = res.m_volSTH  * VolErrFactor<F>(m_nTH);

    // Over-Night "S" Vol and its error:
    res.m_volSON =
      StdDev<F>(m_sumDSON,  m_sumDSON2,  m_sumDTON,  m_sumDTON2,  m_sumDSDTON);

    res.m_errVolSON     = res.m_volSON  * VolErrFactor<F>(m_nON);

    // Week-End and Holiday "S" Vol and its error:
    res.m_volSWEH =
      StdDev<F>(m_sumDSWEH, m_sumDSWEH2, m_sumDTWEH, m_sumDTWEH2, m_sumDSDTWEH);

    res.m_errVolSWEH    = res.m_volSWEH * VolErrFactor<F>(m_nWEH);

    // Vol-of-vol, Trading Hours:
    res.m_volVolSTH =
      StdDev<F>(m_sumDV,    m_sumDV2,    m_sumDTVol, m_sumDTVol2, m_sumDVDT);

    // Signal-to-Noise Ratio of the above:
    // Signal: vol-of-vol (assumed to be due to the actual vol of vol);
    // Noise : max std dev of the vol estimate in a window:
    //
    F noise = res.m_volSTH  * VolErrFactor<F>(m_nWin);
    res.m_volVolSNR     = res.m_volVolSTH / noise;

    return res;
  }
}
