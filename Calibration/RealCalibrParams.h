// vim:ts=2:et
//===========================================================================//
//                            "RealCalibrParams.h":                          //
//                         Params for "RealCalibrator"                       //
//===========================================================================//
#pragma once

#include "Common/CUDAEnv.h"
#include "Common/DateTime.h"
#include "Common/VecMatTypes.h"
#include "Calibration/MarketData.h"
#include <string>
#include <vector>

namespace Arbalete
{
  //=========================================================================//
  // "RealCalibrParams":                                                     //
  //=========================================================================//
  // All Diffusion, Grid, LLH and Calibration proper params together:
  //
  template<typename F>
  struct RealCalibrParams
  {
  public:
    //=======================================================================//
    // Component Data Types:                                                //
    //=======================================================================//
    //-----------------------------------------------------------------------//
    // "CalibrInitGuess":                                                    //
    //-----------------------------------------------------------------------//
    // Tags of Calibration Initialisers:
    enum CIGType
    {
      CIGExpl,      // Explicit value (e.g. "m_val" below)
      CIGTrendS,    // Use the  "S" trend estimate from Stats
      CIGVolSTH,    // Use the Trading Hours "S" vol estimate from Stats
      CIGVarSTH,    // As above, but squared
      CIGVolSON,    // Similarly for Over-Night "S" vol
      CIGVolSWEH    // Similarly for Week-Ends/Holidays "S" vol
                    // XXX: More to follow when extra stats are implemented
    };

    struct CalibrInitGuess
    {
      CIGType m_type;
      F       m_val;  // Set to 0 unless "m_type" is "CIGExpl"
    };

    //-----------------------------------------------------------------------//
    // "CalibrBounds":                                                       //
    //-----------------------------------------------------------------------//
    // Tags of Calibration Bounds:
    enum CBType
    {
      CBExpl,       // Explicit value
      CBDeltaRel,   // Relative Delta as fraction (neg or pos) of Initial Guess
      CBDeltaAbs    // Absolute Delta (neg or pos) from the Initial Guess
    };

    struct CalibrBounds
    {
      // If "m_type*" is "Fixed", the corresponding "m_val" is unused and
      // should be set to 0:
      //
      CBType  m_typeLo;
      F       m_valLo;
      CBType  m_typeUp;
      F       m_valUp;

      // Is the value actually fixed?
      //
      bool IsFixed() const;
    };
  private:
    //-----------------------------------------------------------------------//
    // Ctors Support:                                                        //
    //-----------------------------------------------------------------------//
    // Default ctor is hidden:
    RealCalibrParams();

    // Copy Ctor and Dtor will be auto-generated

    // Common part of public ctors:
    void CommonInit
    (
      std::string const& diff_type,
      std::string const& cal_name
    );

  public:
    //-----------------------------------------------------------------------//
    // Data Flds:                                                            //
    //-----------------------------------------------------------------------//
    // For Diffusion (to be used with "MkDiffParamsStub"):
    std::string     m_diffType;       //
    bool            m_withReg;        // "true"  by default
    std::string     m_calName;        // "" by default -- no specific calendar

    // For Grid (to be used with "MkGridParamsStub" and directly):
    std::string     m_gridMethod;     // "Yanenko" by default
    int             m_m;              // 501 by default
    int             m_n;              // same
    F               m_hS;             // Overrides "m" if given (XXX: not yet )
    F               m_hV;             // Overrides "n" if given (XXX: only BoF)
    int             m_sts;            // 5 by default (to use 5-point stencils)
    Time            m_dt;             // Time step: 1h by def, less for conds
    F               m_minRelS;        // 0.075 by default
    F               m_maxRelS;        // +oo   by default
    F               m_minRelV;        // 0.2   by default
    F               m_maxRelV;        // +oo   by default
    F               m_NSigmasS;       // 7.0   by default
    F               m_NSigmasV;       // 7.0   by default
    F               m_LoS;            // -oo   by default
    F               m_UpS;            // +oo   by default
    F               m_LoV;            // -oo   by default
    F               m_UpV;            // +oo   by default
    bool            m_S0OnGrid;       // "S0" is placed exactly on "Sx" axis
    bool            m_V0OnGrid;       // "V0" is placed exactly on "Vx" axis

    // CUDA Params (0 or more):
    std::vector<CUDAEnv::Params> m_cudaParamss;
    bool            m_mixedCUDAHost;  // Run also on Host when CUDA is used?

    // LLH Params:
    int             m_minSbC;         // Min steps btwn Cond pts, default 20
    int             m_useDirichlet;   // "false" by default
    int             m_debugLevel;     // 0 by default (no debugging)

    // MktData Params:
    // (1) File-based MktData:
    //
    std::string     m_mdFile;         // MktData File Name
    DateTime        m_t0;             // Diffusion / Grid start time,
    DateTime        m_ts1;            // Calibration sample start,
    Time            m_dts;            // Calibration pts step (1h def)
    DateTime        m_ts2;            // Calibration sample end
    std::string     m_mdTrName;       // "LogNormId" by default

    // (2) Explicitly-provided MktData. This is mostly for testing purposes. In
    //     this case, "m_mdFile" must be empty, and  "m_t*"  and "m_dts" params
    //     have no effect:
    std::shared_ptr<std::vector<MDItem<F>>> m_mktData;

    // Calibration Params proper:
    // "m_calibrMethod" can currently be one of the following:
    //
    // (*) empty          : by default, "DIRect-E04JYF" is used for generic
    //                      LLHs, "MLSL-IPOpt" for Analytical LLHs;
    // (*) "DIRect-E04JYF": derivative-free, any LLHs: DIRect global, then
    //                      E04JYF local "polishing";
    // (*) "DIRect-IPOpt" : for Analytical LLHs: DIRect global, then IPOpt
    //                      local "polishing";
    // (*) "MLSL-E04JYF"  : derivative-free, any LLHs: MLSL global seeding
    //                      with E04JUF local optimisations;
    // (*) "MLSL-IPOpt"   : for Analytical LLHs: MLSL global seeding  with
    //                      IPOpt local optimisations;
    // "m_maxGlobalPts"   : for DIRect: Limit on the total number of function
    //                      evals, per VAR DIM;
    //                      for MLSL  : Seed size in eacj round, per VAR DIM:
    //
    std::string     m_calibrMethod;   // As above
    F               m_rectFrac;       // (DIRect only): stop condition, per dim
    int             m_maxGlobalPts;   // Stop condition, per dim
    int             m_volWinSz;       // Window size for Vol-of-Vol-of-S ests

    // The following params are for "MLSL_*" methods:
    int             m_mlsl_maxRounds; // Max # of sampling / minimisation rnds
    int             m_mlsl_maxBest;   // Max # of best pts to try, per sample

    // Initial Guess and Bounds:
    std::vector<CalibrInitGuess>  m_initGuess;
    std::vector<CalibrBounds>     m_bounds;

    //=======================================================================//
    // Ctor and Factory:                                                     //
    //=======================================================================//
    //-----------------------------------------------------------------------//
    // Non-Default Ctors:                                                    //
    //-----------------------------------------------------------------------//
    // Use this ctor to set flds without meaningful defaults, then modify all
    // other flds if necessary. NB: CUDA params need to be set up afterwards:
    //
    RealCalibrParams
    (
      std::string const& diff_type,
      std::string const& md_file,
      DateTime           t0,
      DateTime           ts1,
      Time               dts,
      DateTime           ts2,
      std::string const& cal_name  = "",
      std::string const& mdTr_name = "LogNormId"
    );

    // When MktData are already available:
    //
    RealCalibrParams
    (
      std::string const&                             diff_type,
      std::shared_ptr<std::vector<MDItem<F>>> const& mkt_data,
      std::string const&                             cal_name = ""
    );

    //-----------------------------------------------------------------------//
    // "FromXML":                                                            
    //-----------------------------------------------------------------------//
    // Static Factory Method:
    // Parses an XML file and creates "RealCalibrParams" and vecrors of "CIG*"
    // and "CB*". See the implementation for the file layout.
    // NB: "CalibrParams"  are returned directly rather than being filled in:
    // there is no default ctor for them, so they cannot be created initially
    // by the caller.
    // Also note: the object returned cannot contain "m_mktData" already set
    // as this would not be in XML file; on the other hand, the XML file can
    // contain the name of an "mdFile":
    //
    static RealCalibrParams<F> FromXML(std::string const&  xml_file);
  };
}
