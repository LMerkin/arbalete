// vim:ts=2:et
//===========================================================================//
//                                "MLSL.cpp":                                //
//             Multi-Level Single Linkage Optimisation (Driver)              //
//===========================================================================//
/* Copyright (c) 2007-2012 Massachusetts Institute of Technology
 * Ported to STL and integrated with Arbalete Framework by Leonid Merkin, 2013
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * 
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

/* Multi-Level Single-Linkage (MLSL) algorithm for
   global optimization by random local optimizations (a multistart algorithm
   with "clustering" to avoid repeated detection of the same local minimum), 
   modified to optionally use a Sobol' low-discrepancy sequence (LDS) instead 
   of pseudorandom numbers.  See:

   A. H. G. Rinnooy Kan and G. T. Timmer, "Stochastic global optimization
   methods," Mathematical Programming, vol. 39, p. 27-78 (1987).
       [ actually 2 papers -- part I: clustering methods (p. 27), then 
                              part II: multilevel methods (p. 57) ]

   and also:

   Sergei Kucherenko and Yury Sytsko, "Application of deterministic
   low-discrepancy sequences in global optimization," Computational
   Optimization and Applications, vol. 30, p. 297-318 (2005).

   Compared to the above papers, I made a couple other modifications
   to the algorithm besides the use of a LDS.

   1) first, the original algorithm suggests discarding points
      within a *fixed* distance of the boundaries or of previous
      local minima; I changed this to a distance that decreases with,
      iteration, proportional to the same threshold radius used
      for clustering.  (In the case of the boundaries, I make
      the proportionality constant rather small as well.)

   2) Kan and Timmer suggest using fancy "spiral-search" algorithms
      to quickly locate nearest-neighbor points, reducing the
      overall time for N sample points from O(N^2) to O(N log N)
      However, recent papers seem to show that such schemes (kd trees,
      etcetera) become inefficient for high-dimensional spaces (d > 20),
      and finding a better approach seems to be an open question.  Therefore,
      since I am mainly interested in MLSL for high-dimensional problems
      (in low dimensions we have DIRECT etc.), I punted on this question
      for now.  In practice, O(N^2) (which does not count the #points
      evaluated in local searches) does not seem too bad if the objective
      function is expensive.
*/
#include "Common/Maths.hpp"
#include "Calibration/MLSL.h"
#include "MonteCarlo/SobolSeq.h"
#include <cstdlib>
#include <cstring>
#include <vector>
#include <set>
#include <stdexcept>
#include <memory>
#include <cassert>
#include <iostream>

namespace
{
  using namespace std;
  using namespace Arbalete;

  //=========================================================================//
  // Data Types:                                                             //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // Data structure for each random/quasirandom point in the population:     //
  //-------------------------------------------------------------------------//
  struct pt
  {
  private:
    // Default Ctor is hidden:
    pt();

  public:
    double         m_f;             // Function value at "x"
    vector<double> m_x;             // Vector of length "n" (#dimensions)
    mutable bool   m_minimised;     // Already minimised starting from "x"?
    mutable double m_closest_pt_d;  // Distance^2 to closest "pt", < "f"
    mutable double m_closest_lm_d;  // Distance^2 to closest "lm", < "f"

    // Non-Default Ctor:
    pt(int n):
      m_f           (Inf<double>()),
      m_x           (n, Inf<double>()),
      m_minimised   (false),
      m_closest_pt_d(Inf<double>()),
      m_closest_lm_d(Inf<double>())
    {}

    // "Setters":
    void SetClosestPtD(double d) const { m_closest_pt_d = d; }
    void SetClosestLMD(double d) const { m_closest_lm_d = d; }
    void SetMinimised ()         const { m_minimised = true; }
  };

  //-------------------------------------------------------------------------//
  // Comparators:                                                            //
  //-------------------------------------------------------------------------//
  struct pt_compare
  {
    // The "less" operator:
    bool operator()(pt const& p1, pt const& p2) const
    {
      // NB: "p1" or "p2" may be a dummy value with empty "m_x" -- so do not
      // check the "mx" size -- we only need "m_f":
      return (p1.m_f < p2.m_f);
    }
  };

  struct lm_compare
  {
    // The "less" operator:
    bool operator()(vector<double> const& lm1, vector<double> const& lm2) const
    {
      // NB: "fictitious" vectors of size 1 can be used ln comparisons here, so
      // the sizes of "lm1" and "lm2| may not be the same -- but always >= 1:
      assert(!lm1.empty() && !lm2.empty());

      // The "lm" value is the 0th element of each vector:
      return (lm1[0] < lm2[0]);
    }
  };

  //=========================================================================//
  // Utils:                                                                  //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // "distance2":                                                            //
  //-------------------------------------------------------------------------//
  // Euclidean distance |x1 - x2|^2:
  //
  inline double distance2(int n, double const* x1, double const* x2)
  {
    assert(n >= 1);
    double d = 0.0;

    for (int i = 0; i < n; ++i)
    {
      double dx = x1[i] - x2[i];
      d += dx * dx;
    }
    return d;
  }

  //-------------------------------------------------------------------------//
  // "find_closest_pt":                                                      //
  //-------------------------------------------------------------------------//
  // Find the closest "pt" to "p" with a smaller function value; this function
  // is called when "p" is first added to our set:
  //
  void find_closest_pt(set<pt, pt_compare> const& pts, pt* p)
  {
    assert(p != nullptr && !pts.empty());
    int    n  = int(p->m_x.size());
    assert(n >= 1);

    // Find greatest point in "pts" which is < "p". To this end, we first find
    // the smallest  point in "pts" which is >= p, then iterate back in the
    // loop below:
    set<pt, pt_compare>::const_iterator cit = pts.lower_bound(*p);

    // It is OK if we got cit == pts.end(): It is still decrementable because
    // "pts" is non-empty:
    assert(cit == pts.end() || cit->m_f >= p->m_f);

    // Iterate back to beginning, find the minimal distance:
    double closest_d = HUGE_VAL;
    while (cit != pts.begin())
    {
      // After initial decrement, we should get the largest  point in "pts"
      // which is < "p"; then continue to the beginning:
      cit--;
      assert(cit != pts.end() && cit->m_f < p->m_f &&
             int(cit->m_x.size()) == n);

      double d = distance2(n, p->m_x.data(), cit->m_x.data());
      if (d < closest_d)
        closest_d = d;
    }
    // Memoise the resulting closest distance in "p":
    p->m_closest_pt_d = closest_d;
  }

  //-------------------------------------------------------------------------//
  // "find_closest_lm":                                                      //
  //-------------------------------------------------------------------------//
  // Find the closest local minimiser ("lm") to "p" with a smaller function
  // value; this function is called when "p" is first added to our set:
  //
  void find_closest_lm(set<vector<double>, lm_compare> const& lms, pt* p)
  {
    // NB: Unlike "pts" is "find_closest_pt" above, "lms" MAY be empty!
    assert(p != nullptr);
    int    n  = int(p->m_x.size());
    assert(n >= 1);

    double closest_d = HUGE_VAL;

    if (!lms.empty())
    {
      // Vector to be used in cpmparisons below:
      vector<double> tmp_lm(1, p->m_f);

      // Find the greatest point in "lms" which is < "p". To thsi end, we first
      // find the smallest point in "lms" which is >= p, then iterate back in
      // the loop below:
      set<vector<double>, lm_compare>::const_iterator cit =
        lms.lower_bound(tmp_lm);

      // It is OK if we got cit == lms.end(): It is still decrementable
      // because in this case "lms" is non-empty:
      assert(cit == lms.end() || (*cit)[0] >= p->m_f);

      // Iterate back to beginning, find the minimal distance:
      while (cit != lms.begin())
      {
        // After initial decrement, we should get the largest point in "lms"
        // which is < "p"; then continue to the beginning:
        cit--;
        assert(cit != lms.end() && int(cit->size()) == n+1 &&
              (*cit)[0] < p->m_f);

        double d = distance2(n, p->m_x.data(), cit->data() + 1);
        if (d < closest_d)
          closest_d = d;
      }
    }
    // Memoise the resulting closest distance (or HUGE_VAL) in "p":
    p->m_closest_lm_d = closest_d;
  }

  //-------------------------------------------------------------------------//
  // "pts_update_newpt":                                                     //
  //-------------------------------------------------------------------------//
  // Given "newpt", which presumably has just been added to our set, update all
  // "pts" with a greater function value in case "newpt" is closer to them than
  // their previous "closest_pt"...
  // We can ignore already-minimised points since we never do local minimizatn
  // from the same point twice:
  //
  void pts_update_newpt(set<pt, pt_compare>* pts, pt const& newpt)
  {
    assert(pts != nullptr);
    int    n = int(newpt.m_x.size());
    assert(n >= 1);

    // Find the smallest point in the set which is > newpt.m_f;
    // NB: "lower_bound" below only yields ">=" (if non-end), so extra checks
    // are required:
    //
    set<pt, pt_compare>::iterator it = pts->lower_bound(newpt);
    if ((it == pts->end()) ||
        (it->m_f == newpt.m_f && ++it == pts->end()))
      // All "pts" are <= pt, so cannot do anything:
      return;

    assert(it != pts->end() && it->m_f > newpt.m_f);
    do
    {
      if (!it->m_minimised)
      {
        double d = distance2(n, newpt.m_x.data(), it->m_x.data());
        if (d < it->m_closest_pt_d)
          // XXX: (*it) is read-only but force assignment to an auxiliary fld:
          it->SetClosestPtD(d);
      }
      it++;
    }
    while (it != pts->end());
  }

  //-------------------------------------------------------------------------//
  // "pts_update_newlm":                                                     //
  //-------------------------------------------------------------------------//
  // Given "newlm", which presumably has just been added to our set, update all
  // "pts" with a greater function value in case "newlm" is closer to them than
  // their previous "closest_lm"...
  // We can ignore already-minimised points since we never do local minimizatn
  // from the same point twice:
  //
  void pts_update_newlm(set<pt, pt_compare>* pts, vector<double> const& newlm)
  {
    assert(pts != nullptr);
    int    n = int(newlm.size() - 1);
    assert(n >= 1);

    pt tmp_pt(0);
    tmp_pt.m_f = newlm[0];

    // Find the smallest point in the set which is > newlm[0];
    // NB: "lower_bound" below only yields ">=" (if non-end), so extra checks
    // are required:
    //
    set<pt, pt_compare>::iterator it = pts->lower_bound(tmp_pt);
    if ((it == pts->end()) ||
        (it->m_f == newlm[0] && ++it == pts->end()))
      // All "pts" are <= newlm[0], so cannot do anything:
      return;

    assert(it != pts->end() && it->m_f > newlm[0]);
    do
    {
      if (!it->m_minimised)
      {
        double d = distance2(n, newlm.data() + 1, it->m_x.data());
        if (d < it->m_closest_lm_d)
          it->SetClosestLMD(d);
       }
       it++;
    }
    while (it != pts->end());
  }

  //-------------------------------------------------------------------------//
  // "is_potential_minimiser":                                               //
  //-------------------------------------------------------------------------//
  bool is_potential_minimiser
  (
    int              n,
    double const*    lb,
    double const*    ub,
    pt const&        p,
    double           dpt_min,
    double           dlm_min,
    double           dbound_min
  )
  {
    assert(n >= 1 && lb != nullptr && ub != nullptr && int(p.m_x.size()) == n);
    double const* x = p.m_x.data();

    if (p.m_minimised ||
        p.m_closest_pt_d <= dpt_min * dpt_min ||
        p.m_closest_lm_d <= dlm_min * dlm_min)
      return false;

    for (int i = 0; i < n; ++i)
      if ((x [i] - lb[i] <= dbound_min || ub[i] - x[i] <= dbound_min) &&
          (ub[i] - lb[i] >  dbound_min))
        return false;

    // Otherwise:
    return true;
  }

  //-------------------------------------------------------------------------//
  // "get_minf":                                                             //
  //-------------------------------------------------------------------------//
  void get_minf
  (
    int                                    n,
    double*                                x,
    double*                                minf,
    set<pt,             pt_compare> const& pts,
    set<vector<double>, lm_compare> const& lms
  )
  {
    assert(n >= 1 && x != nullptr && minf != nullptr);

    // Get the minimal element in "pts":
    set<pt, pt_compare>::const_iterator pcit = pts.begin();

    if (pcit != pts.end())
    {
      *minf = pcit->m_f;
      assert(int(pcit->m_x.size()) == n);
      memcpy(x,  pcit->m_x.data(), sizeof(double) * n);
    }

    // Get the minimal element in "lms":
    set<vector<double>, lm_compare>::const_iterator lcit = lms.begin();

    if (lcit != lms.end() && (*lcit)[0] < *minf)
    {
      *minf = (*lcit)[0];
      assert(int(lcit->size()) == n+1);
      memcpy(x, (lcit->data()) + 1, sizeof(double) * n);
    }
  }

  //-------------------------------------------------------------------------//
  // "gam":                                                                  //
  //-------------------------------------------------------------------------//
  // Compute Gamma(1 + n/2)^{1/n} ... this is a constant factor used in MLSL as
  // part of the minimum-distance cutoff:
  //
  // Use Stirling's approximation:
  // Gamma(1 + z) ~ sqrt(2*pi*z) * z^z / exp(z)
  // ... this is more than accurate enough for our purposes
  // (error in gam < 15% for d=1, < 4% for d=2, < 2% for d=3, ...)
  // and avoids overflow problems because we can combine it with
  // the ^{1/n} exponent:
  //
  double gam(int n)
  {
    double const ExpMH = 0.6065306597126334;  // exp(-0.5)
    double       nd    = double(n);
    return   SqRt(Pow(Pi<double>() * nd, 1.0 / nd) * 0.5 * nd) * ExpMH;
  }

  //-------------------------------------------------------------------------//
  // Consts:                                                                 //
  //-------------------------------------------------------------------------//
  double const MLSL_SIGMA = 2.0;
  double const MLSL_GAMMA = 0.3;
  double const SPI        = 1.0 / SqRt(Pi<double>());
  double const DLM        = 1.0;    // Min distance/R to local minima 
  double const DBound     = 1e-6;   // Min distance/R to ub/lb boundaries
}

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // The Actual Minimiser:                                                   //
  //=========================================================================//
  // Array Version:
  //
  int MLSL::Minimise
  (
    LocalMinimiser const& LM,
    int                   n,
    double const*         lbs,
    double const*         ubs,
    double*               xs,
    double*               minf,
    int                   max_rounds,
    int                   sample_size,
    int                   max_best,
    int                   debug_level
  )
  {
    // Check the args:
    assert(n >= 1 && lbs != nullptr && ubs != nullptr && xs != nullptr && minf != nullptr);

    if (sample_size <= 0 || max_best <= 0)
      throw invalid_argument("MLSL::Minimise: Invalid sample_size / max_best");

    // The following sets are initially empty:
    set<pt,             pt_compare> pts;
    set<vector<double>, lm_compare> lms;

    double in    = 1.0 / double(n);
    double R_prefactor = SPI * Pow(gam(n) * MLSL_SIGMA, in);

    for (int i = 0; i < n; ++i)
    {
      assert(ubs[i] > lbs[i]);
      R_prefactor *= Pow(ubs[i] - lbs[i], in);
    }

    // Allocate sampling points:
    vector<shared_ptr<pt>> p(sample_size);
    for (int i = 0; i < sample_size; ++i)
      p[i] = shared_ptr<pt>(new pt(n));

    // Local minima:
    vector<double> lm(n+1);

    // Return codes from evaluations / local minimisations:
    vector<int> ret(sample_size, MLSL::SUCCESS);

    // Create the Sobol Sequence and skip some initial points, using "p[0]" as
    // an output buffer:
    SobolSeq s(n);
    s.Skip(10 * n + sample_size, p[0]->m_x.data());

    // Now set other flds of "p[0]": Perform initial evaluation of  the Cost
    // Func, but no minimisation. Because of that, this call is ASYNCHRONOUS,
    // so need to wait for its completion and do NOT use a "try-catch" block:
    //
    memcpy(p[0]->m_x.data(), xs, n * sizeof(double));

    LM(n, p[0]->m_x.data(), &(p[0]->m_f), &(ret[0]), false);
    p[0]->m_minimised = false;
    LM(0, nullptr, nullptr, nullptr, false);

    // If initial evaluation was not successful, return immediately:
    if (ret[0] != MLSL::SUCCESS)
      return ret[0];

    // Save the point in "pts". Since "pts" is yet empty, the insertion MUST
    // succeed:
    (void) pts.insert(*(p[0]));

    //-----------------------------------------------------------------------//
    // Main Iteration Loop:                                                  //
    //-----------------------------------------------------------------------//
    for (int r = 0; r < max_rounds; ++r)
    {
      // Get the curr best result into "xs" and "minf":
      get_minf(n, xs, minf, pts, lms);

      //---------------------------------------------------------------------//
      // Sampling Phase: Add Quasi-Random Points:                            //
      //---------------------------------------------------------------------//
      if (debug_level >= 1)
        cout << "\n*** MLSL: Starting Sampling Phase for Round " << r
             << endl << endl;

      for (int k = 0; k < sample_size; ++k)
      {
        // Fill in the sampling point "p[k]" with a Sobol vector, and evaluate
        // the Cost Func at that point, in general ASYNCHRONOUSLY and  without
        // full minimisation; no exception catching:
        //
        s.NextUB(lbs, ubs, p[k]->m_x.data());

        LM(n, p[k]->m_x.data(), &(p[k]->m_f), &(ret[k]), false);
        p[k]->m_minimised = false;
      }
      // Wait for the above evaluations to complete:
      LM(0, nullptr, nullptr, nullptr, false);

      bool stopNow = false;
      for (int k = 0; k < sample_size; ++k)
      {
        // If the curr point was correctly evaluated, save it in "pts":
        if (ret[k] != MLSL::FAILURE)
          (void) pts.insert(*(p[k]));
        stopNow  |= ((ret[k] & MLSL::FORCED_STOP) != 0);
      }
      // So we stop immediately if FORCED_STOP was requested:
      if (stopNow)
      {
        get_minf(n, xs, minf, pts, lms);
        return MLSL::FORCED_STOP;
      }

      // Otherwise: Update the global structs, but again using non-failed pts
      // only:
      for (int k = 0; k < sample_size; ++k)
        if (ret[k] != MLSL::FAILURE)
        {
          find_closest_pt (pts,  p[k].get());
          find_closest_lm (lms,  p[k].get());
          pts_update_newpt(&pts, *(p[k]));
        }

      // Distance threshhold parameter R in MLSL:
      double ptsN = double(pts.size());
      double R    = R_prefactor * Pow(Log(ptsN) / ptsN, in);

      //---------------------------------------------------------------------//
      // Local Search Phase: Do Local Minimisation for Promising Points:     //
      //---------------------------------------------------------------------//
      set<pt, pt_compare>::iterator it = pts.begin();

      for (int j = Min(int(Ceil(MLSL_GAMMA * ptsN + 0.5)), max_best); 
           it != pts.end() && j >= 1;
           j--, it++)
      { // NB: MLSL also suggests setting some minimum distance from points to
        // previous local minimiza and to the boundaries; we don't know how to
        // choose this as a fixed number, so set it proportional to "R"; "DLM"
        // and "DBound" are the proportionality constants:
        //
        if (is_potential_minimiser(n, lbs, ubs, *it, R, DLM * R, DBound * R))
        {
          // Fill in the "lm" vector; lm[0] is for function value, lm[1..(n+1)]
          // are the args:
          memcpy(lm.data() + 1, (it->m_x).data(), sizeof(double) * n);
          lm[0] = it->m_f;

          if (debug_level >= 1)
            cout << "\n*** Starting MLSL Local Minimiser " << j
                 << " for Round " << r << " from f=" << it->m_f << endl << endl;

          //-----------------------------------------------------------------//
          // Invoke the Local Minimiser:                                     //
          //-----------------------------------------------------------------//
          // This time, perform full minimisation -- SYNCHRONOUSLY and with
          // exception handling:
          int lmret = MLSL::SUCCESS;
          try
          {
            LM(n, lm.data()+1, lm.data(), &lmret, true);
            it->SetMinimised();

            if (debug_level >= 1)
            {
              cout << "\n*** MLSL Local Minimiser for Round " << r
                   << " has SUCCEEDED:\nf=" << lm[0]  << "\nxs=[";

              for (int i = 1; i <= n; ++i)
                cout << lm[i] << ((i==n) ? ']' : ',');
              cout << endl << endl;
            }
          }
          catch(...)
            { lmret =  MLSL::FAILURE; }

          if (lmret == MLSL::FAILURE)
          {
            // Local mimimisation failed -- "lm" is unreliable, do not use it
            // but continue with other potential minimisers:
            if (debug_level >= 1)
              cout << "\n*** MLSL Local Mimimiser for Round " << r
                   << " has FAILED..." << endl << endl;
            continue;
          }
          // Otherwise, save this "lm" vector -- it is valid:
          (void) lms.insert(lm);

          // Should we stop immediately?
          if (lmret == MLSL::FORCED_STOP)
          {
            get_minf(n, xs, minf, pts, lms);
            return lmret;
          }

          // If continuing: Update all stored pts with greater Cost Func value
          // using the curr "lm":
          pts_update_newlm(&pts, lm);
        }
      }
    }

    // Get the curr result into "xs" and "minf":
    get_minf(n, xs, minf, pts, lms);

    // XXX: Return SUCCESS even if there were intermediate failures (we just
    // skipped those pts!):
    return MLSL::SUCCESS;
  }
}
