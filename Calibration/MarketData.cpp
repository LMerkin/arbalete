// vim:ts=2:et
//==========================================================================//
//                             "MarketData.cpp":                            //
//      Pre-Defined Instances of Market Data Loader(s) and Transform(s)     //
//==========================================================================//
#include "Calibration/MarketData.hpp"

namespace Arbalete
{
  //-------------------------------------------------------------------------//
  // "MDTransform": base and derived structs:                                //
  //-------------------------------------------------------------------------//
  // "MDTransform" itself (base):
  template struct MDTransform<double>;
  template struct MDTransform<float>;

  // "TransformLogNormId":
  template struct TransformLogNormId<double>;
  template struct TransformLogNormId<float>;

  // "TransformBondPx2IR":
  template class  TransformBondPx2IR<double>;
  template class  TransformBondPx2IR<float>;

  // Transforms Factory:
  template MDTransform<double>* MkMDTransform<double>
    (string const& mdtr_name, int stats_acc_param);

  template MDTransform<float>*  MkMDTransform<float>
    (string const& mdtr_name, int stats_acc_param);

  //-------------------------------------------------------------------------//
  // "LoadMktData":                                                          //
  //-------------------------------------------------------------------------//
  template void LoadMktData<double>
  (
    string const& md_file,
    DateTime      t0,
    DateTime      ts1,
    Time          dts,
    DateTime      ts2,
    shared_ptr<TradingCalendar> const& cal,
    MDTransform  <double>*            mdtr,
    vector<MDItem<double>>*           res
  );

  template void LoadMktData<float>
  (
    string const& md_file,
    DateTime      t0,
    DateTime      ts1,
    Time          dts,
    DateTime      ts2,
    shared_ptr<TradingCalendar> const& cal,
    MDTransform  <float>*             mdtr,
    vector<MDItem<float>>*            res
  );

  //-------------------------------------------------------------------------//
  // "ZipMktData":                                                           //
  //-------------------------------------------------------------------------//
  template void ZipMktData<double>
  (
    DateTime                          t0,
    vector<TimeNode<double>> const&   timeLine,
    double const*                     vals,
    vector<MDItem<double>>*           res
  );

  template void ZipMktData<float>
  (
    DateTime                          t0,
    vector<TimeNode<float>> const&    timeLine,
    float const*                      vals,
    vector<MDItem<float>>*            res
  );
}
