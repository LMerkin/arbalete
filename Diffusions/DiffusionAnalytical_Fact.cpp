// vim:ts=2:et
//===========================================================================//
//                      "DiffusionAnalytical_Fact.cpp":                      //
//            Pre-Defined Instances of "DiffusionAnalytical_Fact"            //
//===========================================================================//
#include "Diffusions/DiffusionAnalytical_Fact.hpp"

namespace Arbalete
{
  //-------------------------------------------------------------------------//
  // DO NOT instantiate the Analytical Diffusions themselves!                //
  //-------------------------------------------------------------------------//
  // This is done in their corresp main class (1D, 2D, ...)
  // NB: We do NOT need any "extern template" constructs here to prevent auto-
  // instantiation, since this Factory  does not include any  actual Diffusion
  // definitions -- it works through 1D and 2D Factories:

  //-------------------------------------------------------------------------//
  // Explicitly instantiate the Analytical Factories:                        //
  //-------------------------------------------------------------------------//
  template DiffusionAnalytical<double>* MkDiffusionAnalytical<double>
    (Diffusion_Params<double> const& params);

  template DiffusionAnalytical<float>*  MkDiffusionAnalytical<float>
    (Diffusion_Params<float>  const& params);

  //-------------------------------------------------------------------------//
  // Instantiate the Analytical Diffusion Checkers:                          //
  //-------------------------------------------------------------------------//
  template bool IsDiffusionAnalytical<double>
    (Diffusion_Params<double> const& params);

  template bool IsDiffusionAnalytical<float>
    (Diffusion_Params<float> const& params);
}
