// vim:ts=2:et
//===========================================================================//
//                              "Diffusion2D.cpp":                           //
//          Pre-Defined Instances of "Diffusion2D" (the Base Class):         //
//===========================================================================//
#include "Diffusions/Diffusion2D.hpp"
#include "MonteCarlo/RNG.hpp"

namespace Arbalete
{
  // NB: These instances include "DataHolder" instances as well:
  template class Diffusion2D<double>;
  template class Diffusion2D<float>;
}
