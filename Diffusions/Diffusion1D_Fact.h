// vim:ts=2:et
//===========================================================================//
//                          "Diffusion1D_Fact.h":                            //
//                    Factory of Various 1D Diffusions                       //
//===========================================================================//
#pragma once

#include "Diffusions/Diffusion1D.h"

namespace Arbalete
{
  //=========================================================================//
  // "MkDiffusion1D":                                                        //
  //=========================================================================//
  // Currently accepted "params.m_type" values:
  // (*) "GBM"
  // (*) "OU"
  //
  template<typename F>
  Diffusion1D<F>* MkDiffusion1D(Diffusion_Params<F> const& params);

  //=========================================================================//
  // "IsDiffusion1D":                                                        //
  //=========================================================================//
  // Simply checks whether the arg contains a correct "Diffusion1D" name; no
  // other params checks are performed:
  //
  template<typename F>
  bool IsDiffusion1D(Diffusion_Params<F> const& params);
}
