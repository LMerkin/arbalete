// vim:ts=2:et
//===========================================================================//
//                       "Diffusion2D_BoilerPlate2.h":                       //
//              BoilerPlate code for all 2D Diffusions, Part 2               //
//===========================================================================//
// Implementation of virtual "GetSDECoeffs" and "TransPDF" methods declared in
// the base class "Diffusion2D", for a derived "Diffusion2D_XXX" class,  using
// the nested "Diffusion2D_XXX::DataHolder" class. Also provides implementation
// of "Simple Accessors":
//
// Enclosing Syntactic Context:
// namespace Arbalete
// class Diffusion2D_XXX:

    //-----------------------------------------------------------------------//
    // "GetSDECoeffs", Single-Point:                                         //
    //-----------------------------------------------------------------------//
    void GetSDECoeffs
    (
      F S,  F V, F ty,     TradingCalendar::CalReg reg,
      F* trendS, F* volS1, F* volS2, F* trendV, F* volV, F* corrSV, F* IR
    )
    const override
    {
      m_dh.GetSDECoeffs
        (S, V, ty, reg, trendS, volS1, volS2, trendV, volV, corrSV, IR);
    }

    //-----------------------------------------------------------------------//
    // "GetSDECoeffs", All-Points-Together:                                  //
    //-----------------------------------------------------------------------//
    void GetSDECoeffs
    (
      int m, F const* Sx,   int n,    F const* Vx, F ty,
      TradingCalendar::CalReg reg,
      F* trendS,  F* volS1, F* volS2, F* trendV,   F* volV, F* corrSV, F* IR
    )
    const override
    {
      m_dh.GetSDECoeffs
        (m, Sx, n, Vx, ty, reg, trendS, volS1, volS2, trendV, volV,
        corrSV, IR);
    }

    //-----------------------------------------------------------------------//
    // "TransPDF":                                                           //
    //-----------------------------------------------------------------------//
    F TransPDF
      (F S1, F V1, F ty1, F S2, F V2, F dty, TradingCalendar::CalReg reg)
    const override
      { return m_dh.TransPDF(S1, V1, ty1, S2, V2, dty, reg); }

    //-----------------------------------------------------------------------//
    // "Simple Accessors":                                                   //
    //-----------------------------------------------------------------------//
    bool IsIRModel()   const override { return m_dh.IsIRModel();  }

    F GetIR(F S, F ty, TradingCalendar::CalReg reg) const override
      { return m_dh.IR(S, ty, reg);   }

    F        GetS0()   const override { return m_dh.GetS0();      }
    F        GetV0()   const override { return m_dh.GetV0();      }
    bool HasRegime()   const override { return m_dh.HasRegime();  }

    //-----------------------------------------------------------------------//
    // "MCPaths":                                                            //
    //-----------------------------------------------------------------------//
    // HIGH-LEVEL Monte Carlo API:
    //
    void MCPaths
    (
      std::vector<TimeNode<F>> const& timeLine,     // (L)
      int                             P,            // Must be even
      RNG<F> const&                   rng,          // Incl CUDAEnv
      Matrix<F>*                      S,            // NULL or (L,  P)
      Matrix<F>*                      V,            // NULL or (L,  P)
      Matrix<F>*                      IR,           // NULL or (L,  P)
      bool                            useAntithetic = false,
      // Output CUDA ptrs (set to NULL if provided but CUDA is not used):
      TimeNode<F>**                   cudaTimeLine  = nullptr,
      F**                             cudaS         = nullptr,
      F**                             cudaV         = nullptr,
      F**                             cudaIR        = nullptr
    )
    const override
    {
      Diffusion2D<F>::DataHolder::template MCPathsGen<DataHolder>
      (
        m_dh, timeLine, P, rng, S, V, IR, useAntithetic,
        cudaTimeLine, cudaS, cudaV, cudaIR
      );
    }

