// vim:ts=2:et
//===========================================================================//
//                          "Diffusion1D_MC_Core.hpp":                       //
//            Monte Carlo Path Generation Core for "Diffusion1D":            //
//===========================================================================//
// To be included into "Diffusion1D.hpp" and "Diffusion1D_XXX.cu":
//
#pragma once

#include "Common/CUDAEnv.hpp"
#include "Common/TradingCalendar.h"
#include "MonteCarlo/RNG.h"
#include <stdexcept>
#include <cstdlib>
#include <cassert>

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "MCPathsGen1D_Core":                                                    //
  //=========================================================================//
  // "LOW-LEVEL Diffusion1D Monte Carlo API":
  // Args:
  // "timeLine": times and fwd regimes for MC marshalling;
  // "L"       : length of each Path (size of "timeLine");
  // "P"       : the total number of Paths to be generated; "P" must be EVEN
  //             because antithetic variates are used;
  // "rngc"    : Random Number Generator (Core suitable for both Host and CUDA);
  // "S"       : output matrix (L, P) of paths, MUST be of    size = L * P,
  //             stored as col-major, S(0,*) = S0;
  // "IR"      : similar to "S", stores Interest Rates at each point;
  // NB: All arrays may be located either in Host or in CUDA space; "der" is
  //     passed by copy because this method is GLOBAL (may be invoked from a
  //     CUDA kernel):
  //
  // NB: "static" is essential here -- otherwise Host code could get called
  //     instead of CUDA code!
  //
  template<typename F, typename D>
  GLOBAL static void MCPathsGen1D_Core
  (
    D                              der,
    CUDAEnv::Sched1D const*        CUDACC_OR_DEBUG(sched),
    typename RNG<F>::Core          rngc,  // NB: By COPY -- a GLOBAL func arg!
    TimeNode<F> const*             timeLine,
    int                            L,
    int                            P,
    F*                             S,
    F*                             IR,
    bool                           useAntithetic
  )
  {
    assert(timeLine != nullptr && S != nullptr);
    // "IR" may be NULL
    assert(L >= 1 && timeLine[0].m_ty == F(0.0));
    // Otherwise cannot even store "S0"

    // If Antithetic mode is used, the number of paths must be even:
    assert(!useAntithetic || P % 2 == 0);

    //-----------------------------------------------------------------------//
    // Paths Parallelisation: CUDA or Host:                                  //
    //-----------------------------------------------------------------------//
    // NB: k = p / 2:
    //
#   ifdef __CUDACC__
    // CUDA-based simulation: Using a plain liner array of CUDA threads to
    // simulate all Paths -- no interaction between the Threads:
    assert(sched != nullptr);

    int Tn    = blockDim.x * blockIdx.x + threadIdx.x;
    int kFrom = sched[Tn].m_from;
    int kTo   = sched[Tn].m_to;

    assert(0 <= kFrom &&
          (useAntithetic && kTo <= (P/2)-1 || !useAntithetic && kTo <= P-1));

    // NB: Can still have kFrom > kTo, in which case the curr thread simply
    // does nothing:
    if (kFrom > kTo)
      return;
#   else
    // Host-based simulation:
    assert(sched == nullptr);
    int kFrom = 0;
    int kTo   = useAntithetic ? ((P/2) - 1) : (P-1);

#   ifndef __clang__
#   pragma omp parallel for
#   endif
#   endif  // __CUDACC__
    for (int k = kFrom; k <= kTo; ++k)
    {
      //---------------------------------------------------------------------//
      // Generate 1 or 2 MC Paths (Main and possibly Antithetic ones):       //
      //---------------------------------------------------------------------//
      // NB: "S" and "IR" may be data blocks from (L,P) matrices stored in Col-
      // Major format, so each Column is a continuous path of size L:
      //
      F* Sk0   = nullptr;
      F* IRk0  = nullptr;
      F* Sk1   = nullptr;       // Only used in the Antithetic mode
      F* IRk1  = nullptr;       //

      int off0 = useAntithetic ? (2*k*L) : (k*L);
      Sk0      = S  + off0;     // "Main" or the only path
      if (IR != nullptr)
        IRk0   = IR + off0;

      if (useAntithetic)
      {
        int off1 = off0  + L;   // Antithetic path
        Sk1      = S  + off1;
        if (IR != nullptr)
          IRk1   = IR + off1;
      }

      // Put the initial "S0" into one or both paths:
      *Sk0 = der.GetS0();
      if (useAntithetic)
        *Sk1 = *Sk0;

      for (int l = 1; l < L; ++l)
      {
        //-------------------------------------------------------------------//
        // Make a single MC step:                                            //
        //-------------------------------------------------------------------//
        F  ty1  = timeLine[l-1].m_ty;
        F  ty2  = timeLine[l].m_ty;
        F  dty  = ty2 - ty1;

        TradingCalendar::CalReg reg1 = timeLine[l-1].m_reg;

        // NB: Negative "ty1" or "dty" values are in general NOT allowed:
        assert(ty1 >= F(0.0) && dty > F(0.0));

        // Generate an N(0,1) random variate for the current Thread ID:
        F W = rngc.Normal01();

        // Compute the Diffusion Coeffs for the Main Path at "ty1":
        F  trendS, volS;
        F* ir = (IR != nullptr) ? (IRk0 + l) : nullptr;

        der.GetSDECoeffs(Sk0[l-1], ty1, reg1, &trendS, &volS, ir);

        // MAKE a STEP:
        //
        F sdt = SqRt(dty);
        F dS  = trendS * dty + volS * sdt * W;  // +W

        // Compute the next value:
        Sk0[l] = Sk0[l-1] + dS;

        if (useAntithetic)
        {
          // Do another path: with (-W):
          ir = (IR != nullptr) ? (IRk1 + l) : nullptr;

          // Get the Diffusion Coeffs for the Antithetic Path:
          der.GetSDECoeffs(Sk1[l-1], ty1, reg1, &trendS, &volS, ir);

          dS = trendS * dty - volS * sdt * W;   // -W

          // Compute the next value:
          Sk1[l] = Sk1[l-1] + dS;
        }
        // Check if the Diffusion values must be positive, in which case we
        // apply Reflection for negative values computed. NB: Do not use Ab-
        // sorbtion as it would wrongly produce many 0s:
        //
        if (der.IsPositive())
        {
          if (Sk0[l] < F(0.0))
            Sk0[l] = - Sk0[l];

          if (useAntithetic && Sk1[l] < F(0.0))
            Sk1[l] = - Sk1[l];
        }
      }
    }
  }

  //=========================================================================//
  // "MCPathsGen1D_KernelInvocator":                                         //
  //=========================================================================//
  template<typename F, typename D>
  struct MCPathsGen1D_KernelInvocator
  {
    // NB: All ptrs are already to CUDA memory:
    static void Run
    (
      D const&                       der,
      CUDAEnv const*                 cudaEnv,        // Alias!
      CUDAEnv::Sched1D const*        sched,
      typename RNG<F>::Core const&   rngc,           // NB: Here ref is OK!
      TimeNode<F> const*             timeLine,
      int                            L,
      int                            P,
      F*                             S,
      F*                             IR,
      bool                           useAntithetic
    )
#   ifdef __CUDACC__
    {
      assert(cudaEnv != nullptr && sched != nullptr && timeLine != nullptr &&
             S       != nullptr && P  >= 1  && L >= 1);

      // Set the CUDA device:
      CUDAEnv::Lock();
      cudaEnv->SetCUDADevice();

      // Invoke the kernel. XXX: CUDA Shared Memory is not used as it would
      // normally be too small for holding the RNG states anyway:
      //
      MCPathsGen1D_Core<F, D>
        <<<cudaEnv->NThreadBlocks(), cudaEnv->ThreadBlockSize(), 0,
           cudaEnv->Stream()
        >>>
        (der, sched, rngc, timeLine, L, P, S, IR, useAntithetic);

      CUDAEnv::Unlock();
      CUDA_CHECK_STATUS("MCPaths1D Kernel invocation failed");
    }
#   else
    ; // Just a prototype
#   endif
  };
}
