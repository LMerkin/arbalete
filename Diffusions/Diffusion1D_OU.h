// vim:ts=2:et
//===========================================================================//
//                          "Diffusion1D_OU.h":                              //
//         Ornstein-Uhlenbeck Diffusion with Time-Linear MR Target           //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Diffusions/Diffusion1D.h"
#include "Diffusions/DiffusionAnalytical.h"
#include <stdexcept>

namespace Arbalete
{
  //=========================================================================//
  // "Diffusion1D_OU":                                                       //
  //=========================================================================//
  // Trading-Time Dynamics, non-IR model, 1D:
  //
  // dS = (a + kappa * (a*t + b - S)) * dt + sigma * dW
  //
  template<typename F>
  class Diffusion1D_OU final: public Diffusion1D<F>,
                              public DiffusionAnalytical<F>
  {
  public:
    //=======================================================================//
    // "DataHolder" (non-virtual, suitable for CUDA):                        //
    //=======================================================================//
    class DataHolder final: public Diffusion1D<F>::DataHolder
    {
    private:
      friend class Diffusion1D_OU<F>;

      F     m_a;        // Trend slope
      F     m_b;        // Trend intercept
      F     m_kappa;    // Mean-reversion rate
      F     m_sigma;    // Trading-hours vol

      // Default Ctor is hidden:
      DataHolder();

    public:
      //---------------------------------------------------------------------//
      // Non-Default Ctor (not for CUDA):                                    //
      //---------------------------------------------------------------------//
      // params.m_coeffs (4):
      // [ a, b, kappa, sigma ]
      //
#     ifndef __CUDACC__
      DataHolder(Diffusion_Params<F> const& params);
#     endif

      // Copy Ctor and Dtor are auto-generated

      //---------------------------------------------------------------------//
      // SDE Coeffs (Individual):                                            //
      //---------------------------------------------------------------------//
      // XXX: Currently, this diffusion is for day-trading only:
      //
      // XXX: For the moment, this diffusion is purely intra-day: if "reg" is
      // not "TradingHours", evaluation of the diffusion coeffs results in an
      // error. However, if the user DOES need to run it over-night, they may
      // switch off the "m_withReg" flag in "DiffusionParams",  then all time
      // instants would be treated as "TradingHours":
      //
      // "TrendS", Regime-Dependent:
      //
      DEVICE F TrendS(F S, F ty, TradingCalendar::CalReg reg) const
      { 
        return 
          (!this->m_withReg || reg == TradingCalendar::TradingHours)
          ? (m_a + m_kappa * (m_a * ty + m_b - S))
          : NaN<F>();
      }

      // "VolS": Local Vol, Regime-Dependent:
      //
      DEVICE F VolS
        (F UNUSED_PARAM(S), F UNUSED_PARAM(ty), TradingCalendar::CalReg reg)
      const
      {
        return
          (!this->m_withReg || reg == TradingCalendar::TradingHours)
          ? m_sigma
          : NaN<F>();
      }

      // "IR":
      // XXX: A model of this type CAN actually be viewed as a Hull-White
      // interest rate model, in particular in short term; however, we do
      // NOT use it as such currently -- and the rate is assumed to be 0:
      //
      DEVICE F IR
        (F UNUSED_PARAM(S), F UNUSED_PARAM(ty), TradingCalendar::CalReg reg)
      const
      {
        return
          (!this->m_withReg || reg == TradingCalendar::TradingHours)
          ? F(0.0)
          : NaN<F>();
      }

      // "IsIRModel":
      DEVICE bool IsIRModel()    const    { return false;  }

      // "IsPositive":
      DEVICE bool IsPositive()   const    { return false;  }

      //---------------------------------------------------------------------//
      // "GetSDECoeffs" and "TransPDF":                                      //
      //---------------------------------------------------------------------//
      // NB: Do NOT auto-generate "TransPDF": it is provided explicitly below:
      //
#     define  USE_TRANSPDF_SPECIAL 1
#     include "Diffusions/Diffusion1D_BoilerPlate1.h"
#     undef   USE_TRANSPDF_SPECIAL

      DEVICE F TransPDF
        (F S1, F ty1, F S2, F dty, TradingCalendar::CalReg reg)
      const;

      // XXX: It might be a good idea to actually implement the "LLH" function
      // here so it could be computed in CUDA space as well. However,  this is
      // is not done yet -- "LLH" is only provided in the outer class (below).

      //---------------------------------------------------------------------//
      // "NLLH" (Negated Log-LikeliHood Function):                           //
      //---------------------------------------------------------------------//
      void NLLH
      (
        int                            n,       // Mkt data length
        F const*                       md_tys,  // Len = n
        F const*                       md_S,    // Len = n
        TradingCalendar::CalReg const* md_regs, // Len = n
        F*                             val,
        F                              g[4],    // Gradient
        F                              h[16],
        F*                             vs       // NULL, or of length "n"
      )
      const;
    };
    // End of "DataHolder"

# ifndef __CUDACC__
    //=======================================================================//
    // "Diffusion1D_OU" itself (virtual, not for CUDA):                      //
    //=======================================================================//
  private:
    DataHolder  m_dh;   // All data are stored here

    // Default Ctor is hidden:
    Diffusion1D_OU();

  public:
    //-----------------------------------------------------------------------//
    // Ctors and Accessors:                                                  //
    //-----------------------------------------------------------------------//
    // Non-Default Ctor:
    //
    Diffusion1D_OU(Diffusion_Params<F> const& params)
    : Diffusion1D<F>        (params.m_t0),
      DiffusionAnalytical<F>(),
      m_dh                  (params)
    {}

    // Copy Ctor and Dtor are auto-generated

    // Data Export (without "t0"!):
    DataHolder const& GetData() const  { return m_dh; }

    //-----------------------------------------------------------------------//
    // Implementation of abstract methods from the base class:               //
    //-----------------------------------------------------------------------//
#   include "Diffusions/Diffusion1D_BoilerPlate2.h"

    //-----------------------------------------------------------------------//
    // Negated Log-Likelihood Function and Its Derivatives:                  //
    //-----------------------------------------------------------------------//
    // From "DiffusionAnalytical". NB: "Vs" is unused; if non-NULL, it will be
    // filled with consts ("sigma"):
    //
    void NLLH
    (
      std::vector<F>                       const& tys,
      std::vector<F>                       const& S,
      std::vector<TradingCalendar::CalReg> const& regs,
      F*                                          val,
      Vector<F>*                                  gradient,
      Matrix<F>*                                  hessian,
      F*                                          Vs = nullptr
    )
    const override;

# endif // !__CUDACC__
  };
}
