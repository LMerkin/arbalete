// vim:ts=2:et
//===========================================================================//
//                           "Diffusiob2D_DeMulti":                          //
//        Explicit ("Manual") De-Multiplexor Macro for "Diffision2D"         //
//===========================================================================//
#pragma once

#include "Diffusions/Diffusion2D_HestonCEV.h"
#include "Diffusions/Diffusion2D_3o2CEV.h"
#include "Diffusions/Diffusion2D_SABR.h"

//---------------------------------------------------------------------------//
// De-Multiplexor Macro:                                                     //
//---------------------------------------------------------------------------//
// XXX: Assumption on the templated function to be invoked:
// (1) void Class <F, DiffusionDataHolderType>::Method(dataHolder, args...)
// or
// (2) void Method<F, DiffusionDataHolderType>        (dataHolder, args...);
// so the actual macro args will be:
// (1) Class=SomeClass,  Method=::SomeMethod;
// (2) Class=SomeMethod, Method=EMPTY_MACRO_ARG:
//
#define  EMPTY_MACRO_ARG

#define  DIFFUSION2D_DEMULTIPLEXOR(F, diff, Class, Method, ...) \
{ \
  Diffusion2D_HestonCEV<F> const* diffHeston = \
    dynamic_cast<Diffusion2D_HestonCEV<F> const*>(diff); \
  \
  if (diffHeston != nullptr) \
    /* Heston-CEV: */ \
    Class<F, typename Diffusion2D_HestonCEV<F>::DataHolder>Method \
      (diffHeston->GetData(), __VA_ARGS__); \
  \
  else \
  { \
    Diffusion2D_3o2CEV<F> const* diff3o2 = \
      dynamic_cast<Diffusion2D_3o2CEV<F> const*>(diff); \
    \
    if (diff3o2 != nullptr) \
      /* "3/2-CEV": */ \
      Class<F, typename Diffusion2D_3o2CEV<F>::DataHolder>Method \
        (diff3o2->GetData(), __VA_ARGS__); \
    \
    else \
    { \
      Diffusion2D_SABR<F> const* diffSABR = \
        dynamic_cast<Diffusion2D_SABR<F> const*>(diff); \
      \
      if (diffSABR != nullptr) \
        /* SABR: */ \
        Class<F, typename Diffusion2D_SABR<F>::DataHolder>Method \
          (diffSABR->GetData(), __VA_ARGS__); \
      \
      else \
        throw std::runtime_error \
              ("Diffusion2D_DeMultiplexor: Unrecognised Diffusion"); \
    } \
  } \
}
