// vim:ts=2:et
//===========================================================================//
//                            "Diffusion1D_GBM.hpp":                         //
//                      Geometric Brownian Motion Diffusion                  //
//===========================================================================//
#pragma once

#include "Diffusions/Diffusion1D_GBM.h"
#include "Diffusions/Diffusion1D.hpp"
#include <cassert>
#include <stdexcept>
#include <cstring>

namespace Arbalete
{
  using namespace std;

  //-------------------------------------------------------------------------//
  // Non-Default Ctor:                                                       //
  //-------------------------------------------------------------------------//
# ifndef __CUDACC__
  template<typename F>
  Diffusion1D_GBM<F>::DataHolder::DataHolder(Diffusion_Params<F> const& params)
  : Diffusion1D  <F>::DataHolder(params)
  {
    int from = this->m_fromIdx;
    assert(from == 0);

    // There are NP=2 coeffs of this model proper:
    Vector<F> const& coeffs = params.m_coeffs;
    int nc = int(coeffs.size());
    if (from > nc - NP)
      throw invalid_argument
            ("Diffusion1D_GBM::DataHolder Ctor: Too few coeffs");

    m_mu    = coeffs[from++];
    m_sigma = coeffs[from++];

    if (Abs(m_sigma) <= F(0.0))
      // NB: In theory, "kappa" can be <= 0 as well, although this means that
      // the spread is not mean-reverting:
      throw invalid_argument
            ("Diffusion1D_GBM::DataHolder Ctor: Invalid volatility");

    // Now compute the vol-adjusted trend:
    m_muAdj = m_mu - F(0.5) * m_sigma * m_sigma;
  }
# endif // !__CUDACC__

  //-------------------------------------------------------------------------//
  // Transition PDF:                                                         //
  //-------------------------------------------------------------------------//
  template<typename F>
  DEVICE F Diffusion1D_GBM<F>::DataHolder::TransPDF
    (F S1, F UNUSED_PARAM(ty1), F S2, F dty, TradingCalendar::CalReg reg)
  const
  {
    assert(dty > F(0.0) && S1 > F(0.0) && S2 > F(0.0));

    // Only the "TradingHours" regime is supported:
    if (this->m_withReg && reg != TradingCalendar::TradingHours)
      return NaN<F>();

    // Generic case:
    F u     = Log(S2/S1) - m_muAdj * dty;
    F v2    = m_sigma  * m_sigma   * dty * F(2.0);
    F denom = m_sigma  * S1 * SqRt(dty);

    // The result:
    F res = InvSqRt2Pi<F>() / denom * Exp(- u * u / v2);
    return res;
  }

  //-------------------------------------------------------------------------//
  // Negated Log-Likelihood Function and Its Derivatives:                    //
  //-------------------------------------------------------------------------//
# ifndef __CUDACC__
  template<typename F>
  void Diffusion1D_GBM<F>::NLLH
  (
    vector<F>                       const& tys,
    vector<F>                       const& S,
    vector<TradingCalendar::CalReg> const& regs,
    F*                                     val,
    Vector<F>*                             gradient,
    Matrix<F>*                             hessian,
    F*                                     vs
  )
  const
  {
    // Verify the params. NB: "gradient" and/or "hessian" may still be NULL(s)
    // if pure evaluation was requested; "vs" is always NULL  for a 1D Diff:
    //
    assert(val != nullptr && vs == nullptr);

    if (gradient != nullptr)
      gradient->resize(NP);

    if (hessian  != nullptr)
      hessian->resize(NP, NP);

    // Initialise (zero-out) the results -- "memset" to 0 is OK for floating-
    // point 0s as well:
    //
    *val = F(0.0);
    F* g = (gradient != nullptr) ? &((*gradient)[0])   : nullptr;
    F* h = (hessian  != nullptr) ? &((*hessian) (0,0)) : nullptr;

    if (g != nullptr)
      memset(g, 0, NP      * sizeof(F));

    if (h != nullptr)
      memset(h, 0, NP * NP * sizeof(F));

    // Check the Time Instants and MktData:
    int const nk = int(tys.size());

    if (int(S.size()) != nk || int(regs.size()) != nk)
      throw invalid_argument
            ("Diffusion1D_GBM::NLLH: size(tys)=" + to_string(nk)   +
             ", size(S)=" + to_string(S.size())  + ", size(regs)=" +
             to_string(regs.size()));

    if (nk <= 1)
      // Nothing more to do -- the results are all 0...
      return;

    // Verify that the initial mkt data item is (t0, S0):
    if (tys[0] != F(0.0) || S[0] != this->GetS0())
      throw invalid_argument
            ("Diffusion1D_GBM<F>::LLH: Diffusion and MktData initial conds"
             " do not match");

    // Invoke the actual "NLLH" implementation on the "DataHolder":
    m_dh.NLLH(nk, tys.data(), S.data(), regs.data(), val, g, h, vs);
  }
# endif // !__CUDACC__

  //-------------------------------------------------------------------------//
  // "NLLH" implementation in "DataHolder", suitable for CUDA:               //
  //-------------------------------------------------------------------------//
  template<typename F>
  void Diffusion1D_GBM<F>::DataHolder::NLLH
  (
    int                            nk,
    F const*                       md_tys,
    F const*                       md_S,
    TradingCalendar::CalReg const* md_regs,
    F*                             val,
    F                              g[NP],
    F                              h[NP * NP],
    F*                             vs       // NULL, or of length "nk"
  )
  const
  {
    assert(nk      >= NP      && md_tys != nullptr && md_S != nullptr &&
           md_regs != nullptr && val    != nullptr && vs   == nullptr);

    // Hessian cannot be requested without the Gradient:
    assert(h == nullptr || g != nullptr);

    // NB: The initial Diffusion values must be the same as start of Mkt Data:
    assert(md_tys[0] == F(0.0));

    // (Sk, tyk): PREVIOUS "mkt_data" entry; "i": CURRENT entry:
    F Sk  = md_S[0];
    F tyk = F(0.0);

    if (vs != nullptr)
      vs[0]  = m_sigma * Sk;     // Initial vol

    F sigma2 = m_sigma * m_sigma;

    // The main loop -- initial data item is skipped:
    //
    for (int i = 1; i < nk; ++i)
    {
      // Check the mode for [(i-1)..i]: non-trading hours are to be SKIPPED:
      if (md_regs[i-1] != TradingCalendar::TradingHours)
        continue;

      // Market data:
      //
      F Si  = md_S  [i];
      F tyi = md_tys[i];
      F dty = tyi - tyk;
      assert (tyi > F(0.0) && dty > F(0.0));

      // Vol:
      if (vs != nullptr)
        vs[i] = m_sigma * Si;

      // The negated log-likelihood term (XXX: in order to make different Diff-
      // usions comparable, and to make analytical results comparable with nu-
      // merical ones, do NOT omit constant terms here -- ie those which do not
      // depend on the Diffusion params):
      F u     = (Log(Si/Sk) - m_muAdj * dty) / m_sigma;
      F u2    = u * u;
      F denom = m_sigma    * Sk *  SqRt(dty) * SqRt2Pi<F>();
      *val   += Log(denom) + u2 /  (dty      * F(2.0));

      // The Gradient components:
      //
      if (g != nullptr)
      {
        F uds = u / m_sigma;
        g[0] -= uds;
		    g[1] += u + (F(1.0) - u2 / dty) / m_sigma;

        // The Hessian components. Stored in the column-first order:
        //
        if (h != nullptr)
        {
          // h11 = d^2 L / dmu^2:
          h[0]  += dty / sigma2;

          // h12 = d^2 L / (dmu * dsigma) = h21:
          h[1]  += (F(2.0) * u / sigma2 - dty / m_sigma);

          // h13 = d^2 L / (dsigma^2):
          h[2]  += (F(2.0) / sigma2 - F(3.0) * dty +
                    F(12.0)* uds *   (F(1.0) - uds / dty)) / m_sigma;
        }
      }
      // Shift the data for the next iteration:
      Sk    = Si;
      tyk   = tyi;
    }
  }
}
