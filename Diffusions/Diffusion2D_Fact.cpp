// vim:ts=2:et
//===========================================================================//
//                          "Diffusion2D_Fact.cpp":                          //
//               Pre-Defined Instances of "Diffusion2D_Fact"                 //
//===========================================================================//
#include "Diffusions/Diffusion2D_Fact.hpp"

namespace Arbalete
{
  //-------------------------------------------------------------------------//
  // Prevent instantiation of "Diffusion2D*" classes themselves:             //
  //-------------------------------------------------------------------------//
  // This is to reduce compilation time. The Diffusions are actually instantia-
  // ted in their respective ".cpp" files:
  //
  extern template class Diffusion2D<double>;
  extern template class Diffusion2D<float>;

  extern template class Diffusion2D_HestonCEV<double>;
  extern template class Diffusion2D_HestonCEV<float>;

  extern template class Diffusion2D_3o2CEV<double>;
  extern template class Diffusion2D_3o2CEV<float>;

  extern template class Diffusion2D_SABR<double>;
  extern template class Diffusion2D_SABR<float>;

  //-------------------------------------------------------------------------//
  // Instantiate the 2D Diffusion Factories:                                 //
  //-------------------------------------------------------------------------//
  template Diffusion2D<double>* MkDiffusion2D<double>
    (Diffusion_Params<double> const& params);

  template Diffusion2D<float>*  MkDiffusion2D<float>
    (Diffusion_Params<float>  const& params);

  //-------------------------------------------------------------------------//
  // Instantiate the 2D Diffusion Checkers:                                  //
  //-------------------------------------------------------------------------//
  template bool IsDiffusion2D<double>
    (Diffusion_Params<double> const& params);

  template bool IsDiffusion2D<float>
    (Diffusion_Params<float>  const& params);
}
