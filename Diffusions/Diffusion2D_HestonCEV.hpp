// vim:ts=2:et
//===========================================================================//
//                        "Diffusion2D_HestonCEV.hpp":                       //
//            Heston-CEV StocLocalVol Diffusion (with Vol Trend):            //
//===========================================================================//
#pragma once

#include "Diffusions/Diffusion2D.hpp"
#include "Diffusions/Diffusion2D_HestonCEV.h"
#include <cassert>
#include <stdexcept>

namespace Arbalete
{
  using namespace std;

  //-------------------------------------------------------------------------//
  // Non-Default Ctor:                                                       //
  //-------------------------------------------------------------------------//
# ifndef __CUDACC__
  template<typename F>
  Diffusion2D_HestonCEV<F>::DataHolder::DataHolder
    (Diffusion_Params<F> const& params)

  : Diffusion2D<F>::DataHolder(params)
  {
    int from = this->m_fromIdx;
    assert(from >= 0);

    // There are 10 coeffs of this Heston-type model proper:
    Vector<F> const& coeffs = params.m_coeffs;
    int nc = int(coeffs.size());
    if (from > nc - 10)
      throw invalid_argument
            ("Diffusion2D_HestonCEV::DataHolder Ctor: Too few coeffs");

    m_mu    = coeffs[from++];
    m_xiN   = coeffs[from++];
    m_beta  = coeffs[from++];
    m_kappa = coeffs[from++];
    m_a     = coeffs[from++];
    m_b     = coeffs[from++];
    m_epsTH = coeffs[from++];
    m_epsN  = coeffs[from++] * m_epsTH;
    m_rho   = coeffs[from++];
    m_ir    = coeffs[from++];

    if (Abs(m_rho) >= F(1.0) || m_epsTH <= F(0.0) || m_epsN <= F(0.0))
      // NB: We do NOT require "beta" to be in [0..1]; also no restrictions in
      // general on "kappa", "a", "b":
      throw invalid_argument
            ("Diffusion2D_HestonCEV::DataHolder Ctor: Invalid param(s)");
  }
# endif // !__CUDACC__
}
