// vim:ts=2:et
//===========================================================================//
//                          "Diffusion2D_Fact.hpp":                          //
//                      Factory of Various 2D Diffusions                     //
//===========================================================================//
#pragma once

#include "Diffusions/Diffusion2D_Fact.h"
#include "Diffusions/Diffusion2D_All.hpp"
#include <stdexcept>
#include <cassert>

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "MkDiffusion2D":                                                        //
  //=========================================================================//
  template<typename F>
  Diffusion2D<F>* MkDiffusion2D(Diffusion_Params<F> const& params)
  {
    //-----------------------------------------------------------------------//
    // 3/2-CEV SLV:                                                          //
    //-----------------------------------------------------------------------//
    if (params.m_type == "3/2-CEV")
      return new Diffusion2D_3o2CEV<F>(params);
    else
    //-----------------------------------------------------------------------//
    // Heston-CEV SLV:                                                       //
    //-----------------------------------------------------------------------//
    if (params.m_type == "Heston-CEV")
      return new Diffusion2D_HestonCEV<F>(params);
    else
    //-----------------------------------------------------------------------//
    // SABR SLV:                                                             //
    //-----------------------------------------------------------------------//
    if (params.m_type == "SABR")
      // Generic "TransPDF":
      return new Diffusion2D_SABR<F>
                 (params, Diffusion2D_SABR<F>::TransPDF_Method::Generic);
    else
    if (params.m_type == "SABR-HaganEtAl")
      // Hagan's  HeatKernel Perturbation:
      return new Diffusion2D_SABR<F>
                 (params, Diffusion2D_SABR<F>::TransPDF_Method::HaganEtAl);
    else
    if (params.m_type == "SABR-Paulot0")
      // Heat Kernel Expansions by L.Paulot:
      // Without "a1":
      return new Diffusion2D_SABR<F>
                 (params,  Diffusion2D_SABR<F>::TransPDF_Method::Paulot0);
    else
    if (params.m_type == "SABR-Paulot1")
      // With "a1":
      return new Diffusion2D_SABR<F>
                 (params,  Diffusion2D_SABR<F>::TransPDF_Method::Paulot1);
    else
    //-----------------------------------------------------------------------//
    // Anything else:                                                        //
    //-----------------------------------------------------------------------//
    {
      assert(!IsDiffusion2D<F>(params));
      throw invalid_argument
            ("MkDiffusion2D: Invalid Diffusion type: "+ params.m_type);
    }
  }

  //=========================================================================//
  // "IsDiffusion2D":                                                        //
  //=========================================================================//
  template<typename F>
  bool IsDiffusion2D(Diffusion_Params<F> const& params)
  {
    return
      params.m_type == "3/2-CEV"        ||
      params.m_type == "Heston-CEV"     ||
      params.m_type == "SABR"           ||
      params.m_type == "SABR-HaganEtAl" ||
      params.m_type == "SABR-Paulot0"   ||
      params.m_type == "SABR-Paulot1";
  }
}
