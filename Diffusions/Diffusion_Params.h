// vim:ts=2:et
//===========================================================================//
//                          "Diffusion_Params.h":                            //
//              Generic Parameters for Constructing Diffusions:              //
//===========================================================================//
#pragma once

// NB: Not for CUDA compulations yet:
#ifdef __CUDACC__
#error "Diffusion_Params.h must not be used with CUDA compilations"
#endif

#include "Common/DateTime.h"
#include "Common/VecMatTypes.h"
#include <string>

namespace Arbalete
{
  //=========================================================================//
  // "Diffusion_Params" struct:                                              //
  //=========================================================================//
  // The Diffusion Params are the initial "S0" and some  diffusion-specific
  // numerical Coeffs (unobservable params, possibly including "V0" -- they
  // will need to be calibrated):
  //
  template<typename F>
  struct Diffusion_Params
  {
    std::string m_type;     // "Standardised" diffusion name
    DateTime    m_t0;       // Calendar DateTime of (S0, possibly V0)
    F           m_S0;       // "S0" is directly observable
    bool        m_withReg;  // With time-based regime-switching?
    Vector<F>   m_coeffs;   // [poss.V0, other unobservable params]
  };
}
