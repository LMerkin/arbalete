// vim:ts=2:et
//===========================================================================//
//                             "Diffusion1D_OU.hpp":                         //
//            Ornstein-Unlenbeck Diffusion with Time-Linear MR Trend         //
//===========================================================================//
#pragma once

#include "Diffusions/Diffusion1D_OU.h"
#include "Diffusions/Diffusion1D.hpp"
#include <cassert>
#include <stdexcept>
#include <cstring>

namespace Arbalete
{
  using namespace std;

  //-------------------------------------------------------------------------//
  // Non-Default Ctor:                                                       //
  //-------------------------------------------------------------------------//
# ifndef __CUDACC__
  template<typename F>
  Diffusion1D_OU<F>::DataHolder::DataHolder
    (Diffusion_Params<F> const& params)

  : Diffusion1D<F>::DataHolder(params)
  {
    int from = this->m_fromIdx;
    assert(from == 0);

    // There are 4 coeffs of this model proper:
    Vector<F> const& coeffs = params.m_coeffs;
    int nc = int(coeffs.size());
    if (from > nc - 4)
      throw invalid_argument
            ("Diffusion1D_OU::DataHolder Ctor: Too few coeffs");

    m_a     = coeffs[from++];
    m_b     = coeffs[from++];
    m_kappa = coeffs[from++];
    m_sigma = coeffs[from++];

    if (Abs(m_sigma) <= F(0.0))
      // NB: In theory, "kappa" can be <= 0 as well, although this means that
      // the spread is not mean-reverting:
      throw invalid_argument
            ("Diffusion1D_OU::DataHolder Ctor: Invalid volatility");
  }
# endif // !__CUDACC__

  //-------------------------------------------------------------------------//
  // Transition PDF:                                                         //
  //-------------------------------------------------------------------------//
  template<typename F>
  DEVICE F Diffusion1D_OU<F>::DataHolder::TransPDF
    (F S1, F ty1, F S2, F dty, TradingCalendar::CalReg reg)
  const
  {
    assert(dty > F(0.0));

    // Only "Trading Hours" regime is supported:
    if (this->m_withReg && reg != TradingCalendar::TradingHours)
      return NaN<F>();

    // Generic case:
    F ty2 = ty1 + dty;
    F e1  = Exp(- m_kappa * dty);
    F e2  = e1 * e1;

    // Numer:
    F ui  = (S2 - m_a * ty2 - m_b) - (S1 - m_a * ty1 - m_b) * e1;

    // Denom:
    F tol = F(100.0) * Eps<F>();
    F I0  = (Abs(m_kappa) >= tol)
            ? ((F(1.0) - e2) / (F(2.0) * m_kappa))
            : dty;
    F vi  = m_sigma * m_sigma * I0;

    // The result:
    F res = InvSqRt2Pi<F>() / SqRt(vi) * Exp(- ui * ui / (F(2.0) * vi));
    return res;
  }

  //-------------------------------------------------------------------------//
  // Negated Log-Likelihood Function and Its Derivatives:                    //
  //-------------------------------------------------------------------------//
# ifndef __CUDACC__
  template<typename F>
  void Diffusion1D_OU<F>::NLLH
  (
    vector<F>                       const& tys,
    vector<F>                       const& S,
    vector<TradingCalendar::CalReg> const& regs,
    F*                                     val,
    Vector<F>*                             gradient,
    Matrix<F>*                             hessian,
    F*                                     vs
  )
  const
  {
    // Verify the params. NB: "gradient" and/or "hessian" may still be NULL(s)
    // if pure evaluation was requested; "vs" is always NULL  for a 1D Diff:
    //
    assert(val != nullptr && vs == nullptr);
    int const NV = 4;

    if (gradient != nullptr)
      gradient->resize(NV);

    if (hessian  != nullptr)
      hessian->resize(NV,  NV);

    // Initialise (zero-out) the results -- "memset" to 0 is OK for floating-
    // point 0s as well:
    //
    *val = F(0.0);
    F* g = (gradient != nullptr) ? &((*gradient)[0])   : nullptr;
    F* h = (hessian  != nullptr) ? &((*hessian) (0,0)) : nullptr;

    if (g != nullptr)
      memset(g, 0, NV      * sizeof(F));

    if (h != nullptr)
      memset(h, 0, NV * NV * sizeof(F));

    // Go through the mkt data. XXX: If this function is called repeatedly on
    // the same market data (e.g., in a model calibration process),   then it
    // would sub-optimally compute  same Year Fractions  over and over again;
    // however,  the cost of computing Year Fractions  is minimal compared to
    // the over-all function cost:
    //
    int const nk = int(tys.size());

    if (int(S.size()) != nk || int(regs.size()) != nk)
      throw invalid_argument
            ("Diffusion1D_OU::NLLH: size(tys)=" + to_string(nk)   +
             ", size(S)=" + to_string(S.size()) + ", size(regs)=" +
             to_string(regs.size()));

    if (nk <= 1)
      // Nothing more to do -- the results are all 0...
      return;

    // Verify that the initial mkt data item is (t0, S0):
    if (tys[0] != F(0.0) || S[0] != this->GetS0())
      throw invalid_argument
            ("Diffusion1D_OU<F>::LLH: Diffusion and MktData initial conds"
             " do not match");

    // Invoke the actual "NLLH" implementation on the "DataHolder":
    m_dh.NLLH(nk, tys.data(), S.data(), regs.data(), val, g, h, vs);
  }
# endif // !__CUDACC__

  //-------------------------------------------------------------------------//
  // "NLLH" implementation in "DataHolder", suitable for CUDA:               //
  //-------------------------------------------------------------------------//
  template<typename F>
  void Diffusion1D_OU<F>::DataHolder::NLLH
  (
    int                            nk,
    F const*                       md_tys,
    F const*                       md_S,
    TradingCalendar::CalReg const* md_regs,
    F*                             val,
    F                              g[4],
    F                              h[16],
    F*                             vs     // NULL, or of length "nk"
  )
  const
  {
    assert(nk      >= 2       && md_tys != nullptr && md_S != nullptr &&
           md_regs != nullptr && val    != nullptr && vs   == nullptr);

    // Hessian cannot be requested without the Gradient:
    assert(h == nullptr || g != nullptr);

    // NB: The initial Diffusion values must be the same as start of Mkt Data:
    assert(md_tys[0] == F(0.0));

    F        c2 = m_sigma  * m_sigma;
    F const tol = F(100.0) * Eps<F>();

    // (Sk, tyk): PREVIOUS "mkt_data" entry; "i": CURRENT entry:
    F Sk  = md_S[0];
    F tyk = F(0.0);
    if (vs != nullptr)
      vs[0] = m_sigma;  // See below

    // The main loop -- initial data item is skipped:
    //
    for (int i = 1; i < nk; ++i)
    {
      // The volatility is deterministic -- so if "vs" is given, fill it in
      // with a constant value:
      if (vs != nullptr)
        vs[i] = m_sigma;

      // Check the mode for [(i-1)..i]: non-trading hours are to be SKIPPED:
      //
      if (md_regs[i-1] != TradingCalendar::TradingHours)
        continue;

      // Market data:
      //
      F Si  = md_S  [i];
      F tyi = md_tys[i];
      F dt  = tyi - tyk;
      assert (tyi > F(0.0) && dt > F(0.0));

      // Exponential factors:
      F kdt = m_kappa * dt;
      F e1  = Exp(- kdt);
      F e2  = e1 * e1;

      // Numer:
      F ui  = (Si - m_a * tyi - m_b) - (Sk - m_a * tyk - m_b) * e1;

      // Integrals:
      F I0  = F(0.0), I1 = F(0.0), I2 = F(0.0);
      if (Abs(m_kappa) >= tol)
      {
        // No singularity:
        F kh = F(0.5) / m_kappa;

        I0   =   kh * (F(1.0)  - e2);
        I1   = - kh * (e2 * dt + kh *(e2 - F(1.0)));
        I2   = - kh * kh  / m_kappa *
                (e2 * (F(2.0) * kdt * (kdt + F(1.0)) + F(1.0)) - F(1.0));
      }
      else
      {
        // Simgularity resolved:
        F dt2 = dt  * dt;
        F dt3 = dt2 * dt;
        I0    = dt;
        I1    = dt2 / F(2.0);
        I2    = dt3 / F(3.0);
      }
      assert(I0 > F(0.0) && I1 > F(0.0) && I2 > F(0.0));

      // Denom:
      F vi  = c2 * I0;
      assert(vi > F(0.0));

      // The log-likelihood term:
      *val += Log(vi) + ui * ui / vi;

      // The gradient components:
      //
      if (g != nullptr)
      {
	      F  X  = (Sk - m_a *  tyk - m_b) * dt * e1;
        F  R  =  ui / I0;
        F  T  = tyk * e1 - tyi;
        F  E1 =  e1 - F(1.0);

        g[0] +=  R  * T;
		    g[1] +=  R  * E1;
		    g[2] += (ui / c2 * (X + R * I1) - I1) / I0;
		    g[3] -= (ui * ui / vi - F(1.0));

        // The Hessian components. Stored in the column-first order, but in
        // this case it does not matter since the Hessian matrix is symmetric:
        //
        if (h != nullptr)
        {
          F RX2  = F(2.0) * R * I1 + X;

          // h11 = d^2 L / da^2:
          h[0]  +=  T * T / I0;

          // h12 = d^2 L / (da * db):
          h[1]  +=  T * E1 / I0;

          // h13 = d^2 L / (da * d kappa):
          h[2]  += (T * RX2 - tyk * dt * ui * e1) / I0;

          // h[3]= h14 = d^2 L / (da * d c2) = - g[0] / c2
          // h[4]= h21 = h12 = h[1]

          // h22 = d^2 L / db^2:
          h[5]  +=  E1 * E1 / I0;

          // h23 = d^2 L / (db * d kappa):
          h[6]  += (E1 * RX2 - dt * ui * e1) / I0;

          // h[7]= h24  = d^2 L / (db * d c2) = - g[1] / c2
          // h[8]= h31 = h13 = h[2]
          // h[9]= h32 = h23 = h[6]

          // h33 = d^2 L / d kappa^2:
          h[10] += (F(2.0) / vi *
                    (X * (F(4.0) * R  * I1 + X  - ui * dt) +
                     F(2.0) *  R * ui * (F(2.0) * I1 * I1 / I0 - I2)) +
                    F(4.0) / I0 * (I2 - I1 * I1 / I0));

          // h34 = d^2 L / (d kappa * d c2):
          h[11] -= R * (R * I1 + X);

          // h[12] = h41 = h14 = h[3]
          // h[13] = h42 = h24 = h[7]
          // h[14] = h43 = h34 = h[11]
          h[15] += (F(2.0) * ui * ui / vi - F(1.0));
        }
      }
      // Shift the data:
      Sk    = Si;
      tyk   = tyi;
    }
    // Complete the gradient. NB: Verified with Maple:
    if (g != nullptr)
    {
      F ch  = F(2.0) / c2;
      g[0] *= ch;
      g[1] *= ch;
      g[2] *= F(2.0);
      g[3] *= F(2.0) / m_sigma;

      // Complete the Hessian.  NB: Verified with Maple:
      if (h != nullptr)
      {
        h[0] *= ch;
        h[1] *= ch;
        h[2] *= ch;
        h[3]  = - F(2.0) * g[0] / m_sigma;
        h[4]  = h[1];
        h[5] *= ch;
        h[6] *= ch;
        h[7]  = - F(2.0) * g[1] / m_sigma;
        h[8]  = h[2];
        h[9]  = h[6];
        // h[10] is unchanged
        h[11]*=  (F(2.0) * ch   / m_sigma);
        h[12] = h[3];
        h[13] = h[7];
        h[14] = h[11];
        h[15] = h[15] * F(4.0)  / c2 + g[3] / m_sigma;
      }
    }
  }
}
