// vim:ts=2:et
//===========================================================================//
//                        "Diffusion2D_3o2CEV.cpp":                          //
//              Pre-Defined Instances of "Diffusion2D_3o2CEV":               //
//===========================================================================//
#include "Diffusions/Diffusion2D.hpp"
#include "Diffusions/Diffusion2D_3o2CEV.hpp"
#include "MonteCarlo/RNG.hpp"

namespace Arbalete
{
  //-------------------------------------------------------------------------//
  // Templated methods from the base class:                                  //
  //-------------------------------------------------------------------------//
  // NB: It appears that the only templated method which needs to be instantia-
  // ted explicitly is "TransPDFGen" bacause it may not be called directly from
  // within the library (although its CUDA implementation IS called from within
  // the "Grids"!) (XXX: but why does ot create an undefined symbol then???):
  //
  template
  double Diffusion2D<double>::DataHolder::TransPDFGen
    <Diffusion2D_3o2CEV<double>::DataHolder>
  (
    Diffusion2D_3o2CEV<double>::DataHolder const& der,
    double S1, double V1, double ty1,  double S2, double V2, double dty,
    TradingCalendar::CalReg reg
  );

  template
  float  Diffusion2D<float>::DataHolder::TransPDFGen
    <Diffusion2D_3o2CEV<float>::DataHolder>
  (
    Diffusion2D_3o2CEV<float>::DataHolder const& der,
    float  S1, float  V1, float  ty1,  float  S2, float  V2, float  dty,
    TradingCalendar::CalReg reg
  );

  //-------------------------------------------------------------------------//
  // Instances of "Diffusion2D_3o2CEV" itself:                               //
  //-------------------------------------------------------------------------//
  template class Diffusion2D_3o2CEV<double>;
  template class Diffusion2D_3o2CEV<float>;
}
