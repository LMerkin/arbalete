// vim:ts=2:et
//===========================================================================//
//                        "Diffusion2D_HestonCEV.cu":                        //
//    CUDA Kernels for Monte Carlo Simulations of Diffusion2D_HestonCEV:     //
//===========================================================================//
#include "Diffusions/Diffusion2D_HestonCEV.h"
#include "Diffusions/Diffusion2D.hpp"
#include "MonteCarlo/RNG.hpp"

namespace Arbalete
{
  //-------------------------------------------------------------------------//
  // Instances of "MCPathsGen2D_KernelInvocator" w/ "HestonCEV::DataHolder": //
  //-------------------------------------------------------------------------//
  template struct MCPathsGen2D_KernelInvocator
    <double, Diffusion2D_HestonCEV<double>::DataHolder>;

  template struct MCPathsGen2D_KernelInvocator
    <float,  Diffusion2D_HestonCEV<float>::DataHolder>;
}
