// vim:ts=2:et
//===========================================================================//
//                          "Diffusion1D_Fact.cpp":                          //
//               Pre-Defined Instances of "Diffusion1D_Fact"                 //
//===========================================================================//
#include "Diffusions/Diffusion1D_Fact.hpp"

namespace Arbalete
{
  //-------------------------------------------------------------------------//
  // Prevent instantiation of "Diffusion1D*" classes themselves:             //
  //-------------------------------------------------------------------------//
  // This is to reduce compilation time. The Diffusions are actually instantia-
  // ted in their respective ".cpp" files:
  //
  extern template class Diffusion1D<double>;
  extern template class Diffusion1D<float>;

  extern template class Diffusion1D_OU<double>;
  extern template class Diffusion1D_OU<float>;

  //-------------------------------------------------------------------------//
  // Instantiate the 1D Diffusion Factories:                                 //
  //-------------------------------------------------------------------------//
  template Diffusion1D<double>* MkDiffusion1D<double>
    (Diffusion_Params<double> const& params);

  template Diffusion1D<float>*  MkDiffusion1D<float>
    (Diffusion_Params<float>  const& params);

  //-------------------------------------------------------------------------//
  // Instantiate the 1D Diffusion Checkers:                                  //
  //-------------------------------------------------------------------------//
  template bool IsDiffusion1D<double>
    (Diffusion_Params<double> const& params);

  template bool IsDiffusion1D<float>
    (Diffusion_Params<float>  const& params);
}
