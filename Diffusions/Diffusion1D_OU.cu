// vim:ts=2:et
//===========================================================================//
//                           "Diffusion1D_OU.cu":                            //
//        CUDA Kernels for Monte Carlo Simulations of Diffusion1D_OU:        //
//===========================================================================//
#include "Diffusions/Diffusion1D_OU.h"
#include "Diffusions/Diffusion1D.hpp"
#include "MonteCarlo/RNG.hpp"

namespace Arbalete
{
  //-------------------------------------------------------------------------//
  // Instances of "MCPathsGen1D_KernelInvocator" with "OU::DataHolder":      //
  //-------------------------------------------------------------------------//
  template struct MCPathsGen1D_KernelInvocator
    <double, Diffusion1D_OU<double>::DataHolder>;

  template struct MCPathsGen1D_KernelInvocator
    <float,  Diffusion1D_OU<float>::DataHolder>;
}
