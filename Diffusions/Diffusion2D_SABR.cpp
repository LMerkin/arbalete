// vim:ts=2:et
//===========================================================================//
//                           "Diffusion2D_SABR.cpp":                         //
//                Pre-Defined Instances of "Diffusion2D_SABR":               //
//===========================================================================//
#include "Diffusions/Diffusion2D.hpp"
#include "Diffusions/Diffusion2D_SABR.hpp"
#include "MonteCarlo/RNG.hpp"

namespace Arbalete
{
  // NB: Apparently, in this case there is no need to explicitly instantiate any
  // templated methods from  "Diffusion2D::DataHolder" with  "Diffusion2D_SABR::
  // DataHolder" -- they are all instantiated automatically when used:
  //
  template class Diffusion2D_SABR<double>;
  template class Diffusion2D_SABR<float>;
}
