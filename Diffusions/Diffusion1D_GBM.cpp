// vim:ts=2:et
//===========================================================================//
//                            "Diffusion1D_GBM.cpp":                         //
//                  Pre-Defined Instances of "Diffusion1D_GBM":              //
//===========================================================================//
#include "Diffusions/Diffusion1D.hpp"
#include "Diffusions/Diffusion1D_GBM.hpp"
#include "MonteCarlo/RNG.hpp"

namespace Arbalete
{
  //-------------------------------------------------------------------------//
  // Templated methods from the base class:                                  //
  //-------------------------------------------------------------------------//
  // At the moment, it appears that there is no need to explicitly instantiate
  // any...

  //-------------------------------------------------------------------------//
  // Instances of "Diffusion1D_GBM" itself:                                  //
  //-------------------------------------------------------------------------//
  template class Diffusion1D_GBM<double>;
  template class Diffusion1D_GBM<float>;
}
