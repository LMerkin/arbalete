// vim:ts=2:et
//===========================================================================//
//                           "Diffusion1D_GBM.cu":                           //
//        CUDA Kernels for Monte Carlo Simulations of Diffusion1D_GBM:       //
//===========================================================================//
#include "Diffusions/Diffusion1D_GBM.h"
#include "Diffusions/Diffusion1D.hpp"
#include "MonteCarlo/RNG.hpp"

namespace Arbalete
{
  //-------------------------------------------------------------------------//
  // Instances of "MCPathsGen1D_KernelInvocator" with "GBM::DataHolder":     //
  //-------------------------------------------------------------------------//
  template struct MCPathsGen1D_KernelInvocator
    <double, Diffusion1D_GBM<double>::DataHolder>;

  template struct MCPathsGen1D_KernelInvocator
    <float,  Diffusion1D_GBM<float> ::DataHolder>;
}
