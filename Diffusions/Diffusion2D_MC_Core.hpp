// vim:ts=2:et
//===========================================================================//
//                          "Diffusion2D_MC_Core.hpp":                       //
//            Monte Carlo Path Generation Core for "Diffusion2D":            //
//===========================================================================//
// To be included into "Diffusion2D.hpp" and "Diffusion2D_XXX.cu":
//
#pragma once

#include "Common/CUDAEnv.hpp"
#include "Common/TradingCalendar.h"
#include "MonteCarlo/RNG.h"
#include <cstdlib>
#include <stdexcept>
#include <cassert>

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "MCPathsGen2D_Core":                                                    //
  //=========================================================================//
  // "LOW-LEVEL Diffusion2D Monte Carlo API":
  // Args:
  // "timeLine": time instants and fwd regs for MC marshalling
  // "L"       : length of each Path (size of "timeLine");
  // "P"       : the total number of Paths to be generated; "P" must be EVEN
  //             because antithetic variates are used;
  // "rngc"    : Random Number Generator (Core suitable for both Host and CUDA);
  // "S"       : output matrix (L, P) of paths, MUST be of    size = L * P,
  //             stored as col-major, S(0,*) = S0;
  // "V"       : similar to "S";
  // "IR"      : similar to "S", "V";
  // NB: All arrays may be located either in Host or in CUDA space;
  //     "der" is passed by copy because this method is GLOBAL (may be in-
  //     voked from a CUDA kernel):
  //
  // NB: "static" is essential here -- otherwise Host code could get called
  //     instead of CUDA code!
  //
  template<typename F, typename D>
  GLOBAL static void MCPathsGen2D_Core
  (
    D                              der,
    CUDAEnv::Sched1D const*        CUDACC_OR_DEBUG(sched),
    typename RNG<F>::Core          rngc,   // NB: By COPY -- a GLOBAL func arg!
    TimeNode<F> const*             timeLine,
    int                            L,
    int                            P,
    F*                             S,
    F*                             V,
    F*                             IR,
    bool                           useAntithetic
  )
  {
    assert(timeLine != nullptr && S != nullptr);
    // "IR" may be NULL
    assert(L >= 1 && timeLine[0].m_ty == F(0.0));
    // Otherwise cannot even store "S0"

    // If Antithetic mode is used, the number of paths must be even:
    assert(!useAntithetic || P % 2 == 0);

    //-----------------------------------------------------------------------//
    // Paths Parallelisation: CUDA or Host:                                  //
    //-----------------------------------------------------------------------//
    // NB: k = p / 2:
    //
#   ifdef __CUDACC__
    // CUDA-based simulation: Using a plain liner array of CUDA threads to
    // simulate all Paths -- no interaction between the Threads:
    assert(sched != nullptr);

    int Tn    = blockDim.x * blockIdx.x + threadIdx.x;
    int kFrom = sched[Tn].m_from;
    int kTo   = sched[Tn].m_to;

    assert(0 <= kFrom &&
          (useAntithetic && kTo <= (P/2)-1 || !useAntithetic && kTo <= P-1));

    // NB: Can still have kFrom > kTo, in which case the curr thread simply
    // does nothing:
    if (kFrom > kTo)
      return;
#   else
    // Host-based simulation:
    assert(sched == nullptr);
    int kFrom = 0;
    int kTo   = useAntithetic ? ((P/2) - 1) : (P-1);

#   ifndef __clang__
#   pragma omp parallel for
#   endif
#   endif  // __CUDACC__
    for (int k = kFrom; k <= kTo; ++k)
    {
      //---------------------------------------------------------------------//
      // Generate 1 or 2 MC Paths (Main and possibly Antithetic ones):       //
      //---------------------------------------------------------------------//
      // NB: "S" and "IR" may be data blocks from (L,P) matrices stored in Col-
      // Major format, so each Column is a continuous path of size L:
      //
      F* Sk0   = nullptr;
      F* IRk0  = nullptr;
      F* Vk0   = nullptr;
      F* Sk1   = nullptr;       // Only used in the Antithetic mode
      F* Vk1   = nullptr;       //
      F* IRk1  = nullptr;       //

      int off0 = useAntithetic ? (2*k*L) : (k*L);
      Sk0      = S  + off0;     // "Main" or the only path
      Vk0      = V  + off0;
      if (IR != nullptr)
        IRk0   = IR + off0;

      if (useAntithetic)
      {
        int off1 = off0  + L;   // Antithetic path
        Sk1      = S  + off1;
        Vk1      = V  + off1;
        if (IR != nullptr)
          IRk1   = IR + off1;
      }

      // Put the initial "S0" into both paths:
      *Sk0 = der.GetS0();
      *Vk0 = der.GetV0();

      if (useAntithetic)
      {
        *Sk1 = *Sk0;
        *Vk1 = *Vk0;
      }

      for (int l = 1; l < L; ++l)
      {
        //-------------------------------------------------------------------//
        // Make a single MC step:                                            //
        //-------------------------------------------------------------------//
        F  ty1  = timeLine[l-1].m_ty;
        F  ty2  = timeLine[l].m_ty;
        F  dty  = ty2 - ty1;

        TradingCalendar::CalReg reg1 = timeLine[l-1].m_reg;

        // NB: Negative "ty1" or "dty" values are in general NOT allowed:
        assert(ty1 >= F(0.0) && dty > F(0.0));

        // Generate uncorrelated N(0,1) random variates for the current Thread
        // ID:
        F W = rngc.Normal01();
        F Z = rngc.Normal01();

        // Compute the Diffusion Coeffs for the Main Path at "ty1":
        F  trendS, volS1, volS2, trendV, volV, corrSV;
        F* ir = (IR != nullptr) ? (IRk0 + l) : nullptr;

        der.GetSDECoeffs
        (
          Sk0[l-1], Vk0[l-1],  ty1, reg1,
          &trendS,  &volS1, &volS2, &trendV, &volV, &corrSV, ir
        );

        // Make "W" correlated with "Z":
        F Wc = SqRt(F(1.0) - corrSV * corrSV) * W + corrSV * Z;
        F Zc = Z;

        // MAKE a STEP:
        //
        F sdt = SqRt(dty);
        F dS  = trendS * dty + volS1 * volS2 * sdt * Wc;  // +Wc
        F dV  = trendV * dty + volV          * sdt * Zc;  // +Zc

        // Compute the next values:
        Sk0[l] = Sk0[l-1] + dS;
        Vk0[l] = Vk0[l-1] + dV;

        if (useAntithetic)
        {
          // Do another path: with (-Wc) and (-Zc). Ie, we invert both Brownian
          // motions, otherwise their correlation would change sign! XXX: it is
          // not clear whether using antithetics is a good idea under stoch vol
          // in general, as "V" process is in general not monoitonic in "Zc":
          //
          ir = (IR != nullptr) ? (IRk1 + l) : nullptr;

          // Get the Diffusion Coeffs for the Antithetic Path:
          der.GetSDECoeffs
          (
            Sk1[l-1], Vk1[l-1], ty1, reg1,
            &trendS, &volS1, &volS2, &trendV, &volV, &corrSV, ir
          );

          // XXX: We currently assume that the correlation does not depend on
          // "S" or "V", so no need to re-compute "Wc" with new "corrSV"...

          dS = trendS * dty - volS1 * volS2 * sdt * Wc;   // -Wc
          dV = trendV * dty - volV          * sdt * Zc;   // -Zc

          // Compute the next value:
          Sk1[l] = Sk1[l-1] + dS;
          Vk1[l] = Vk1[l-1] + dV;
        }

        // Check if the Duffusion values must be positive, in which case we
        // apply Reflection for negative values computed. NB: Do not use Ab-
        // sorbtion as it would wrongly produce many 0s:
        //
        if (der.IsPositiveS())
        {
          if (Sk0[l] < F(0.0))
            Sk0[l] = - Sk0[l];

          if (useAntithetic && Sk1[l] < F(0.0))
            Sk1[l] = - Sk1[l];
        }
        if (der.IsPositiveV())
        {
          if (Vk0[l] < F(0.0))
            Vk0[l] = - Vk0[l];

          if (useAntithetic && Vk1[l] < F(0.0))
            Vk1[l] = - Vk1[l];
        }
      }
    }
  }

  //=========================================================================//
  // "MCPathsGen2D_KernelInvocator":                                         //
  //=========================================================================//
  template<typename F, typename D>
  struct MCPathsGen2D_KernelInvocator
  {
    // NB: All ptrs are already to CUDA memory:
    static void Run
    (
      D const&                       der,
      CUDAEnv const*                 cudaEnv,
      CUDAEnv::Sched1D const*        sched,
      typename RNG<F>::Core const&   rngc,   // NB: Here ref is OK!
      TimeNode<F> const*             timeLine,
      int                            L,
      int                            P,
      F*                             S,
      F*                             V,
      F*                             IR,
      bool                           useAntithetic
    )
#   ifdef __CUDACC__
    {
      assert(cudaEnv != nullptr && sched != nullptr && timeLine != nullptr &&
             S       != nullptr && V     != nullptr && P   >= 2   && L  >= 1);

      // Set the CUDA device:
      CUDAEnv::Lock();
      cudaEnv->SetCUDADevice();

      // Invoke the kernel:
      MCPathsGen2D_Core<F, D>
        <<<cudaEnv->NThreadBlocks(), cudaEnv->ThreadBlockSize(), 0,
           cudaEnv->Stream()
        >>>
        (der, sched, rngc, timeLine, L, P, S, V, IR, useAntithetic);

      CUDAEnv::Unlock();
      CUDA_CHECK_STATUS("MCPaths2D Kernel invocation failed");
    }
#   else
    ; // Just a prototype
#   endif
  };
}
