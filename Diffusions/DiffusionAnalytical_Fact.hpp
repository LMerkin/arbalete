// vim:ts=2:et
//===========================================================================//
//                      "DiffusionAnalytical_Fact.hpp":                      //
//                 Factory of Various Analytical Diffusions                  //
//===========================================================================//
// XXX: This is a "meta-factory":  It does NOT create  Analytical  Diffusions
// directly. Rather, it identifies Analytical Diffusions by name, invokes the
// corresp real factory (e.g., 1D, 2D ...) and casts (safely -- with a check)
// the result to the "DiffusionAnalytical" type. The reason for this mechanism
// is that it does not require including all individual  Analytical Diffusion
// headers (which could result in instantiation complications):
//
#pragma once

#include "Diffusions/DiffusionAnalytical_Fact.h"
#include "Diffusions/Diffusion1D_Fact.h"
#include "Diffusions/Diffusion2D_Fact.h"
#include <stdexcept>

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "MkDiffusionAnalytical":                                                //
  //=========================================================================//
  template<typename F>
  DiffusionAnalytical<F>* MkDiffusionAnalytical
    (Diffusion_Params<F> const& params)
  {
    DiffusionAnalytical<F>* res = nullptr;

    //-----------------------------------------------------------------------//
    // GBM or OU: Analytical Diffusions:                                     //
    //-----------------------------------------------------------------------//
    if (params.m_type == "GBM" || params.m_type == "OU")
      res = dynamic_cast<DiffusionAnalytical<F>*>(MkDiffusion1D<F>(params));
    else
    //-----------------------------------------------------------------------//
    // Anything else:                                                        //
    //-----------------------------------------------------------------------//
    {
      assert(!IsDiffusionAnalytical(params));
      throw invalid_argument
            ("MkDiffusionAnalytical: Invalid Diffusion type: "+ params.m_type);
    }

    if (res == nullptr)
    {
      assert(!IsDiffusionAnalytical(params));
      throw runtime_error("MkDiffusionAnalytical: Internal Error: Not an "
                          "Analytical Diffusion: "+ params.m_type);
    }
    return res;
  }

  //=========================================================================//
  // "IsDiffusionAnalytical":                                                //
  //=========================================================================//
  template<typename F>
  bool IsDiffusionAnalytical(Diffusion_Params<F> const& params)
  {
    return (params.m_type == "GBM" || params.m_type == "OU");
  }
}
