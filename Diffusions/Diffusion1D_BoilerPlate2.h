// vim:ts=2:et
//===========================================================================//
//                       "Diffusion1D_BoilerPlate2.h":                       //
//              BoilerPlate code for all 1D Diffusions, Part 2               //
//===========================================================================//
// Implementation of virtual "GetSDECoeffs" and "TransPDF" methods declared in
// the base class "Diffusion1D", for a derived "Diffusion1D_XXX" class,  using
// the nested "Diffusion1D_XXX::DataHolder" class. Also provides implementation
// of "Simple Accessors":
//
// Syntactic Enclosing Context:
// namespace Arbalete
// class Diffusion1D_XXX

    //-----------------------------------------------------------------------//
    // "GetSDECoeffs", Single-Point:                                         //
    //-----------------------------------------------------------------------//
    void GetSDECoeffs
      (F S, F ty, TradingCalendar::CalReg reg, F* trendS, F* volS, F* IR)
    const override
      { m_dh.GetSDECoeffs(S, ty, reg, trendS, volS, IR); }

    //-----------------------------------------------------------------------//
    // "GetSDECoeffs", All-Points-Together:                                  //
    //-----------------------------------------------------------------------//
    void GetSDECoeffs
    (
      int m, F const* Sx, F ty, TradingCalendar::CalReg reg,
      F* trendS, F* volS, F* IR
    )
    const override
      { m_dh.GetSDECoeffs(m, Sx, ty, reg, trendS, volS, IR); }

    //-----------------------------------------------------------------------//
    // "TransPDF":                                                           //
    //-----------------------------------------------------------------------//
    F TransPDF(F S1, F ty1, F S2, F dty, TradingCalendar::CalReg reg)
    const override
      { return m_dh.TransPDF(S1, ty1, S2, dty, reg); }

    //-----------------------------------------------------------------------//
    // "Simple Accessors":                                                   //
    //-----------------------------------------------------------------------//
    bool IsIRModel()   const override { return m_dh.IsIRModel();  }

    F GetIR(F S, F ty, TradingCalendar::CalReg reg) const override
      { return m_dh.IR(S, ty, reg);   }

    F        GetS0()   const override { return m_dh.GetS0();      }
    bool HasRegime()   const override { return m_dh.HasRegime();  }

    //-----------------------------------------------------------------------//
    // "MCPaths":                                                            //
    //-----------------------------------------------------------------------//
    // HIGH-LEVEL Monte Carlo API:
    //
    void MCPaths
    (
      std::vector<TimeNode<F>> const& timeLine,     // (L)
      int                             P,            // must be EVEN
      RNG<F> const&                   rng,          // incl CUDAEnv
      Matrix<F>*                      S,            // NULL or (L,  P)
      Matrix<F>*                      IR,           // NULL or (L,  P)
      bool                            useAntithetic = false,
      // Output CUDA ptrs (set to NULL if provided but CUDA is not used):
      TimeNode<F>**                   cudaTimeLine  = nullptr,
      F**                             cudaS         = nullptr,
      F**                             cudaIR        = nullptr
    )
    const override
    {
      Diffusion1D<F>::DataHolder::template MCPathsGen<DataHolder>
      (
        m_dh, timeLine, P, rng, S, IR, useAntithetic,
        cudaTimeLine,   cudaS,  cudaIR
      );
    }

