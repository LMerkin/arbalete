// vim:ts=2:et
//===========================================================================//
//                        "DiffusionAnalytical_Fact.h":                      //
//                  Factory of Various Analytical Diffusions                 //
//===========================================================================//
#pragma once

#include "Diffusions/Diffusion_Params.h"
#include "Diffusions/DiffusionAnalytical.h"

namespace Arbalete
{
  //=========================================================================//
  // "MkDiffusionAnalytical":                                                //
  //=========================================================================//
  // Currently accepted "params.m_type" values:
  // (*) "GBM"
  // (*) "OU"
  //
  template<typename F>
  DiffusionAnalytical<F>* MkDiffusionAnalytical
    (Diffusion_Params<F> const& params);

  //=========================================================================//
  // "IsDiffusionAnalytical":                                                //
  //=========================================================================//
  // Simply checks whether the arg contains a correct "Diffusion1D" name; no
  // other params checks are performed:
  //
  template<typename F>
  bool IsDiffusionAnalytical(Diffusion_Params<F> const& params);
}
