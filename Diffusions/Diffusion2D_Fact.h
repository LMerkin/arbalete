// vim:ts=2:et
//===========================================================================//
//                          "Diffusion2D_Fact.h":                            //
//                    Factory of Various 2D Diffusions                       //
//===========================================================================//
#pragma once

#include "Diffusions/Diffusion2D.h"

namespace Arbalete
{
  //=========================================================================//
  // "MkDiffusion2D":                                                        //
  //=========================================================================//
  // Currently accepted "params.m_type" values:
  // (*) "3/2-CEV"
  // (*) "Heston-CEV"
  // (*) "SABR" (with default TransPDF="Generic"),
  //     "SABR_HaganEtAl", "SABR_Paulot{0|1}"
  //
  template<typename F>
  Diffusion2D<F>* MkDiffusion2D(Diffusion_Params<F> const& params);

  //=========================================================================//
  // "IsDiffusion2D":                                                        //
  //=========================================================================//
  // Simply checks whether the arg contains a correct "Diffusion2D" name; no
  // other params checks are performed:
  //
  template<typename F>
  bool IsDiffusion2D(Diffusion_Params<F> const& Params);
}
