// vim:ts=2:et
//===========================================================================//
//                       "Diffusion1D_BoilerPlate1.h":                       //
//               BoilerPlate code for all 1D Diffusions, Part 1              //
//===========================================================================//
// Implementation of "GetSDECoeffs" and "TransPDF" methods  for a "DataHolder"
// class via templates declared in the base class  (and accessors provided in
// this derived class):
//
// Enclosing Syntactic Context:
// namespace Arbalete
// class Diffusion1D_XXX
// class DataHolder

      //---------------------------------------------------------------------//
      // "GetSDECoeffs", Single-Point:                                       //
      //---------------------------------------------------------------------//
      DEVICE void GetSDECoeffs
        (F S, F ty, TradingCalendar::CalReg reg, F* trendS, F* volS, F* IR)
      const
      {
        Diffusion1D<F>::DataHolder::template GetSDECoeffsGen1<DataHolder>
          (*this, S, ty, reg, trendS, volS, IR);
      }

      //---------------------------------------------------------------------//
      // "GetSDECoeffs", All-Points-Together:                                //
      //---------------------------------------------------------------------//
      DEVICE void GetSDECoeffs
      (
        int m, F const* Sx, F  ty, TradingCalendar::CalReg reg,
        F* trendS, F* volS, F* IR
      )
      const
      {
        Diffusion1D<F>::DataHolder::template GetSDECoeffsGenM<DataHolder>
          (*this, m, Sx, ty, reg, trendS, volS, IR);
      }

      //---------------------------------------------------------------------//
      // "TransPDF":                                                         //
      //---------------------------------------------------------------------//
#     ifndef USE_TRANSPDF_SPECIAL
      DEVICE F TransPDF
        (F S1, F ty1, F S2, F dty, TradingCalendar::CalReg reg) const
      {
        return Diffusion1D<F>::DataHolder::template TransPDFGen<DataHolder>
          (*this, S1, ty1, S2, dty, reg);
      }
#     endif

      // NB: No Monte Carlo functions here (in "DataHolder") -- if necessary,
      // use the corresp instance of "MCPathsGen1D_Core" directly!

