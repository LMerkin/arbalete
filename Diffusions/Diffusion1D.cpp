// vim:ts=2:et
//===========================================================================//
//                              "Diffusion1D.cpp":                           //
//          Pre-Defined Instances of "Diffusion1D" (the Base Class):         //
//===========================================================================//
#include "Diffusions/Diffusion1D.hpp"
#include "MonteCarlo/RNG.hpp"

namespace Arbalete
{
  // NB: These instances include "DataHolder" instances as well:
  template class Diffusion1D<double>;
  template class Diffusion1D<float>;
}
