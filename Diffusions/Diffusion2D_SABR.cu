// vim:ts=2:et
//===========================================================================//
//                          "Diffusion2D_SABR.cu":                           //
//       CUDA Kernels for Monte Carlo Simulations of Diffusion2D_SABR:       //
//===========================================================================//
#include "Diffusions/Diffusion2D_SABR.h"
#include "Diffusions/Diffusion2D.hpp"
#include "MonteCarlo/RNG.hpp"

namespace Arbalete
{
  //-------------------------------------------------------------------------//
  // Instances of "MCPathsGen2D_KernelInvocator" with "SABR::DataHolder":    //
  //-------------------------------------------------------------------------//
  template struct MCPathsGen2D_KernelInvocator
    <double, Diffusion2D_SABR<double>::DataHolder>;

  template struct MCPathsGen2D_KernelInvocator
    <float,  Diffusion2D_SABR<float>::DataHolder>;
}
