// vim:ts=2:et
//===========================================================================//
//                           "Diffusion2D_3o2CEV.h":                         //
//          Lewis's 3/2-CEV StocLocalVol Diffusion (with Vol Trend):         //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Diffusions/Diffusion2D.h"
#ifndef  __CUDACC__
#include <stdexcept>
#endif

namespace Arbalete
{
  //=========================================================================//
  // "Diffusion2D_3o2CEV":                                                   //
  //=========================================================================//
  // Trading-Time Dynamics, non-IR model:
  //
  // dS    = mu*S * dt                       + xi  * S^beta * sqrt(V) * dW
  // dV    = (a + kappa*(a*t + b - V)*V * dt + eps *V^(3/2)           * dZ
  // dW*dZ = rho  * dt
  //
  // NB: a=0 and beta=1 yield the classical Lewis "3/2" model.
  //
  // Mode-switching is provided between Trading Hours (TH) and Non-Trading Hrs
  // NT=(ON, WEH): xi=1 for TH, xi < 1 for NT; "eps" has 2 values as well:
  //
  template<typename F>
  class Diffusion2D_3o2CEV final: public Diffusion2D<F>
  {
  public:
    //=======================================================================//
    // "DataHolder" (non-virtual, suitable for CUDA):                        //
    //=======================================================================//
    class DataHolder final: public Diffusion2D<F>::DataHolder
    {
    private:
      F   m_mu;     // 3/2-CEV model coeffs
      F   m_xiN;    // Scaling (down) factor for vol out-of-trading-hours
      F   m_beta;
      F   m_kappa;
      F   m_a;      // theta = a * ty + b
      F   m_b;
      F   m_epsTH;  // Vol-of-Vol, trading hours
      F   m_epsN;   // Vol-of-Vol, out-of-trading-hours
      F   m_rho;
      F   m_ir;

      // Default Ctor is hidden:
      DataHolder();

    public:
      //---------------------------------------------------------------------//
      // Non-Default Ctor (not for CUDA):                                    //
      //---------------------------------------------------------------------//
      // params.m_coeffs (11):
      // [ V0; mu, xiN, beta, kappa, a, b, epsTH, etaN, rho, ir ]
      // where
      // etaN = epsN / epsTH:
      //
#     ifndef __CUDACC__
      DataHolder(Diffusion_Params<F> const& params);
#     endif

      // Copy Ctor and Dtor are auto-generated

      //---------------------------------------------------------------------//
      // SDE Coeffs (Individial):                                            //
      //---------------------------------------------------------------------//
      // "TrendS":
      DEVICE  F TrendS
        (F S, F UNUSED_PARAM(ty), TradingCalendar::CalReg UNUSED_PARAM(reg))
      const
        { return m_mu * S; }

      // "VolS1": Local Vol, Regime-Dependent:
      DEVICE  F VolS1(F S, F UNUSED_PARAM(ty), TradingCalendar::CalReg reg)
      const
      {
        F lv = (m_beta == F(1.0)) ? S : Pow(S, m_beta);
        return (!this->m_withReg || reg == TradingCalendar::TradingHours)
                ? lv
                : (lv * m_xiN);
      }

      // "VolS2": StocVol:
      DEVICE  F VolS2
        (F V, F UNUSED_PARAM(ty), TradingCalendar::CalReg UNUSED_PARAM(reg))
      const
        { return SqRt(V); }

      // "TrendV": Mean-Reversion of Vol:
      DEVICE  F TrendV(F V, F ty, TradingCalendar::CalReg UNUSED_PARAM(reg))
      const
        { return m_a + m_kappa * (m_a * ty + m_b - V) * V; }

      // "VolV": Vol-of-Vol, Regime-Dependent:
      DEVICE F VolV  (F V, F UNUSED_PARAM(ty), TradingCalendar::CalReg reg)
      const
      {
        return ((!this->m_withReg || reg == TradingCalendar::TradingHours)
                ? m_epsTH : m_epsN)
                * SqRt(V) * V;
      }

      // "CorrSV":
      DEVICE F CorrSV
        (F UNUSED_PARAM(ty), TradingCalendar::CalReg UNUSED_PARAM(reg)) const
        { return m_rho;   }

      // "IR":
      DEVICE  F IR
        (F UNUSED_PARAM(S),    F UNUSED_PARAM(ty),
         TradingCalendar::CalReg UNUSED_PARAM(reg)) const { return m_ir;    }

      //---------------------------------------------------------------------//
      // Properties:                                                         //
      //---------------------------------------------------------------------//
      DEVICE bool IsIRModel()    const    { return false;   }

      DEVICE bool IsPositiveS()  const    { return true;    }

      DEVICE bool IsPositiveV()  const    { return true;    }

      //---------------------------------------------------------------------//
      // "GetSDECoeffs":                                                     //
      //---------------------------------------------------------------------//
#     include "Diffusion2D_BoilerPlate1.h"

      // "TransPDF": Currently using the generic implementation from base class
    };

# ifndef __CUDACC__
    //=======================================================================//
    // "Diffusion2D_3o2CEV" itself (virtual, not for CUDA):                  //
    //=======================================================================//
  private:
    DataHolder  m_dh;     // All data are stored here

    // Default Ctor is hidden:
    Diffusion2D_3o2CEV();

  public:
    //-----------------------------------------------------------------------//
    // Ctors and Accessors:                                                  //
    //-----------------------------------------------------------------------//
    // Non-Default Ctor:
    //
    Diffusion2D_3o2CEV(Diffusion_Params<F> const& params)
    : Diffusion2D<F>(params.m_t0),
      m_dh          (params)
    {}

    // Copy Ctor and Dtor are auto-generated

    // Data Export (without "t0"!):
    DataHolder const& GetData() const  { return m_dh; }

    //-----------------------------------------------------------------------//
    // Implementation of abstract methods from the base class:               //
    //-----------------------------------------------------------------------//
#   include "Diffusions/Diffusion2D_BoilerPlate2.h"

# endif // !__CUDACC__
  };
}
