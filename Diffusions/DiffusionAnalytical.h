// vim:ts=2
//===========================================================================//
//                         "DiffusionAnalytical.h":                          //
//   An Interface to Diffusions with Analytical Log-Likelihood Functions     //
//===========================================================================//
#pragma once

#include "Common/Macros.h"
#include "Common/TradingCalendar.h"
#ifndef  __CUDACC__
#include "Common/VecMatTypes.h"
#endif

namespace Arbalete
{
  //=========================================================================//
  // "DiffusionAnalytical" Interface:                                        //
  //=========================================================================//
  template <typename F>
  class DiffusionAnalytical
  {
  public:
    //-----------------------------------------------------------------------//
    // Virtual Dtor:                                                         //
    //-----------------------------------------------------------------------//
    virtual ~DiffusionAnalytical() {}

    // Default and Copy Ctors are auto-generated

#   ifndef __CUDACC__
    //-----------------------------------------------------------------------//
    // "NLLH": Negated Log-Likelihood, its Gradient and Hessian:             //
    //-----------------------------------------------------------------------//
    // Suitable for minimisation, ie LLH maximisation. NB: "MktData" components
    // are passed as 3 different arrays (tys, S, regs) for better internal CUDA
    // compatibility:
    // NB: "Vs" is an optional output vector for filtered unobservable process
    // (eg StochVol):
    //
    virtual void NLLH
    (
      std::vector<F>                       const& tys,  // MktData time stamps
      std::vector<F>                       const& S,    // MktData prices
      std::vector<TradingCalendar::CalReg> const& regs, // MktData Fwd Regimes
      F*                                          val,
      Vector<F>*                                  gradient,
      Matrix<F>*                                  hessian,
      F*                                          Vs = nullptr
    )
    const = 0;
#   endif // !__CUDACC__
  };
}
