// vim:ts=2
//===========================================================================//
//                             "Diffusion1D.hpp":                            //
//                        Diffusions in the (S) Space:                       //
//===========================================================================//
#pragma once

#include "Common/CUDAEnv.hpp"
#include "Diffusions/Diffusion1D.h"
#include "Diffusions/Diffusion1D_MC_Core.hpp"
#include <cstdlib>
#include <stdexcept>
#include <cassert>

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "GetSDECoeffsGen1": One-Point:                                          //
  //=========================================================================//
  // NB: "reg" is passed as parameter, rather than copmputed internally, for 2
  // reasons:
  // (1) "t" may be a regime-switching point itself, then "reg" depends on the
  //     direction of time flow (only the Grid knows that);
  // (2) for efficiency -- same "reg" may be re-used by mutiple calls if there
  //     is no regime-switching between them.
  // NB: "reg" is ignored anyway if the diffusion is not regime-switching:
  //
  template<typename F>
  template<typename D>
  DEVICE void Diffusion1D<F>::DataHolder::GetSDECoeffsGen1
  (
    D const& der,  F S, F ty,  TradingCalendar::CalReg reg,
    F* trendS, F* volS, F* IR
  )
  {
    // NB: Negative "ty" values are in general NOT allowed:
    assert(trendS != nullptr && volS != nullptr && ty >= F(0.0));

    *trendS = der.TrendS(S, ty, reg);
    *volS   = der.VolS  (S, ty, reg);

    if (IR != nullptr)
      *IR  = der.IR(S, ty, reg);
  }

  //=========================================================================//
  // "GetSDECoeffsGenM": All-at-Once:                                        //
  //=========================================================================//
  // See "GetSDECoeffs" of the outer class for the array size info:
  //
  template<typename F>
  template<typename D>
  DEVICE void Diffusion1D<F>::DataHolder::GetSDECoeffsGenM
  (
    D const& der, int m, F const* Sx, F ty, TradingCalendar::CalReg reg,
    F* trendS,  F* volS, F* IR
  )
  {
    // NB: Negative "ty" values are in general NOT allowed:
    assert(Sx != nullptr && trendS != nullptr && volS != nullptr && m >= 2 &&
           ty >= F(0.0));

    // Iff "vectorIR" is set, IR is "S"-dependent:
    bool vectorIR = (IR != nullptr) && (der.IsIRModel());

    for (int i = 0; i < m; ++i)
    {
      F S = Sx[i];

      *(trendS++) = der.TrendS(S, ty, reg);
      *(volS  ++) = der.VolS  (S, ty, reg);

      if (vectorIR)
        // Variable IR:
        *(IR  ++) = der.IR    (S, ty, reg);
    }

    if (!vectorIR && IR != nullptr)
      // In this case, IR is scalar, too (it does not depend on "S", so use
      // S=0 in the following call):
      *IR = der.IR(F(0.0), ty, reg);
  }

  //=========================================================================//
  // "TransPDFGen":                                                          //
  //=========================================================================//
  // XXX: This is a generic LOW-ACCURACY implementation which uses a Normal
  // approximation (only suitable for sufficiently small "dty"s). Also note
  // the above remarks regarding externally-computed "reg":
  //
  template<typename F>
  template<typename D>
  DEVICE F Diffusion1D<F>::DataHolder::TransPDFGen
    (D const& der, F S1, F ty1, F S2, F dty, TradingCalendar::CalReg reg)
  {
    // NB: Negative "ty1" or "dty" values are in general NOT allowed:
    assert(ty1 >= F(0.0) && dty > F(0.0));

    // Compute the Diffusion Coeffs at (S1, ty1); IR is not required:
    F muS, sigmaS;
    der.GetSDECoeffs(S1, ty1, reg, &muS, &sigmaS, nullptr);

    // 1D Normal PDF:
    assert(sigmaS > F(0.0));
    sigmaS *= SqRt(dty);      // XXX: Inefficiency if called multiple times
                              // for same "dt"...
    F res =
      Exp(- F(0.5) * Sqr<F>(((S2 - S1) - muS * dty) / sigmaS)) *
      InvSqRt2Pi<F>() / sigmaS;
    return res;
  }

# ifndef __CUDACC__
  //=========================================================================//
  // "MCPathsGen": High-Level Interface to "MCPathsGen_Core":                //
  //=========================================================================//
  template<typename F>
  template<typename D>
  void Diffusion1D<F>::DataHolder::MCPathsGen
  (
    D const&                   der,
    vector<TimeNode<F>> const& timeLine,  // size = L
    int                        P,         // Number of Paths
    RNG<F> const&              rng,       // Incl CUDAEnv
    Matrix<F>*                 S,         // size =  (L,  P)
    Matrix<F>*                 IR,        // NULL or (L,  P)
    bool                       useAntithetic,
    TimeNode<F>**              cudaTimeLine,
    F**                        cudaS,
    F**                        cudaIR
  )
  {
    //-----------------------------------------------------------------------//
    // Check the Args:                                                       //
    //-----------------------------------------------------------------------//
    int L  = int(timeLine.size());
    if (L == 0 || P <= 0)
      throw invalid_argument
            ("Diffusion1D::DataHolder::MCPathsGen: Invalid L or P");
    int LP = L * P;

    // XXX: Check for an "int" overflow here. TODO: 64-bit size types:
    if (LP / P != L || LP / L != P || LP < 0)
      throw runtime_error("Diffusion1D::DataHolder::MCPathsGen: LP overflow");

    if (useAntithetic && P % 2 != 0)
      throw invalid_argument
            ("Diffusion1D::DataHolder::MCPathsGen: "
             "Number of Paths must be EVEN for Antithetic");

    // "timeLine" must begin with ty=0:
    if (timeLine[0].m_ty != F(0.0))
      throw invalid_argument
            ("Diffusion1D::DataHolder::MCPathsGen: TimeLine must start at 0");
    assert(timeLine[0].m_secs == 0);

    // Automatically resize the output if necessary -- XXX: this is probably OK
    // for a high-level function like this one:
    if (S  != nullptr)
      S->resize(L, P);
    if (IR != nullptr)
      IR->resize(L, P);

    // Initialise CUDA output ptrs (if used) to NULL:
    if (cudaTimeLine != nullptr)
      *cudaTimeLine = nullptr;

    if (cudaS  != nullptr)
      *cudaS  = nullptr;

    if (cudaIR != nullptr)
      *cudaIR = nullptr;

    CUDAEnv const* cudaEnv = rng.GetCUDAEnv();  // Alias ptr!

    if (cudaEnv == nullptr)
    {
      //---------------------------------------------------------------------//
      // Just invoke the Low-Level Version on Host:                          //
      //---------------------------------------------------------------------//
      // Here "S" must be non-NULL:
      if (S == nullptr)
        throw invalid_argument("Diffusion1D::DataHolder::MCPathsGen: "
                               "Host Mode: S must be non-nullptr");
      MCPathsGen1D_Core<F, D>
      (
        der,    nullptr, rng.GetCore(), timeLine.data(), L,  P,
        &((*S)(0,0)), (IR != nullptr) ? &((*IR)(0,0))  : nullptr,
        useAntithetic
      );
    }
    else
    {
      //---------------------------------------------------------------------//
      // CUDA-Based Simulation:                                              //
      //---------------------------------------------------------------------//
      // Start the CUDA section:
      cudaEnv->Start();

      // Copy the Input Data into the CUDA space:
      TimeNode<F>* tmpTimeLine = cudaEnv->CUDAAlloc<TimeNode<F>>(L);
      cudaEnv->ToCUDA<TimeNode<F>>(timeLine.data(), tmpTimeLine, L);

      // Allocate memory for output data:
      F* tmpS  = cudaEnv->CUDAAlloc<F>(LP);
      F* tmpIR =
        (IR != nullptr || cudaIR != nullptr)
        ? cudaEnv->CUDAAlloc<F>(LP)
        : nullptr;

      // Construct the Schedule: In the Antithetic mode, 2 paths are made at
      // once, so need to divide P/2 work items between all CUDA threads; NB:
      // The Schedule comes into CUDA space directly:
      //
      CUDAEnv::Sched1D* sched =
        cudaEnv->MkSched1D(useAntithetic ? (P/2) : P);

      // Invoke the CUDA Kernel. BEWARE of CUDA memory leaks if invocation
      // fails:
      try
      {
        MCPathsGen1D_KernelInvocator<F, D>::Run
        (
          der,  cudaEnv, sched, rng.GetCore(), tmpTimeLine, L, P,
          tmpS, tmpIR,   useAntithetic
        );
      }
      catch(...)
      {
        // De-allocate CUDA memory:
        cudaEnv->CUDAFree<TimeNode<F>>(tmpTimeLine);
        cudaEnv->CUDAFree<F>(tmpS);
        if (tmpIR != nullptr)
          cudaEnv->CUDAFree<F>(tmpIR);
        cudaEnv->CUDAFree<CUDAEnv::Sched1D>(sched);
        throw;
      }

      // De-allocate the CUDA Schedule:
      cudaEnv->CUDAFree<CUDAEnv::Sched1D>(sched);

      // Copy the data back into the Host space, if the corresp ptrs are
      // non-NULL. This has nothing to do with whether the CUDA data are
      // subsequently preserved (see below):
      assert(tmpS != nullptr);

      if (S != nullptr)
        cudaEnv->ToHost<F>(tmpS,  &((*S) (0,0)), LP);
      if (IR != nullptr)
      {
        assert(tmpIR != nullptr);
        cudaEnv->ToHost<F>(tmpIR, &((*IR)(0,0)), LP);
      }

      // Preserve or de-allocate the CUDA buffers:
      if (cudaTimeLine != nullptr)
        *cudaTimeLine = tmpTimeLine;
      else
        cudaEnv->CUDAFree<TimeNode<F>>(tmpTimeLine);

      if (cudaS  != nullptr)
        *cudaS = tmpS;
      else
        cudaEnv->CUDAFree<F>(tmpS);

      if (cudaIR != nullptr)
      {
        assert(tmpIR != nullptr);
        *cudaIR = tmpIR;
      }
      else
        if (tmpIR != nullptr)
          cudaEnv->CUDAFree<F>(tmpIR);

      // All done. Stop the CUDA section:
      cudaEnv->Stop();
    }
#   ifndef NDEBUG
    // Verify the basic layout of the "S" matrix:
    for (int j =  0; j < P; ++j)
      assert(S == 0 || (*S)(0,j) == der.GetS0());
#   endif
  }
# endif // !__CUDACC__
}
