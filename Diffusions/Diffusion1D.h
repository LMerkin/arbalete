// vim:ts=2
//===========================================================================//
//                              "Diffusion1D.h":                             //
//                        Diffusions in the (S) Space:                       //
//===========================================================================//
#pragma once

#include "Common/TradingCalendar.h"
#include "Common/CUDAEnv.h" // XXX: This is for MC -- not a good dependency...
#ifndef __CUDACC__
#include "Diffusions/Diffusion_Params.h"
#include "MonteCarlo/RNG.h"
#endif

namespace Arbalete
{
  //=========================================================================//
  // "Diffusion1D":                                                          //
  //=========================================================================//
  // dS = trendS(S,t) dt + volS(S,t) dW
  //
  template<typename F>
  class Diffusion1D
  {
  public:
    //=======================================================================//
    // "DataHolder" (non-virtual, suitable for CUDA):                        //
    //=======================================================================//
    // NB: "DataHolder" is an inner class declared in "Diffusion1D".   However,
    // instances of this class are NOT directly contained in "Duffusion1D" obj-
    // ects. Instead, classes derived (non-virtually) from "DataHolder" are de-
    // clared in the derived classes on "Diffusion1D", and derivative instances
    // are contained in the corresponding objects:
    //
    class DataHolder
    {
    protected:
      //---------------------------------------------------------------------//
      // Data Flds:                                                          //
      //---------------------------------------------------------------------//
      F         m_S0;       // "S" at t=t0
      bool      m_withReg;  // With time-based regime-switching?
      int       m_fromIdx;  // How many "coeffs" are consumed by this base cls
                            // ctor (the rest are used by the derived cls)? --
                            // currently none (0)
    private:
      // Default Ctor is hidden:
      DataHolder();

    public:
      //---------------------------------------------------------------------//
      // Non-Default Ctor (not for CUDA) and Accessors:                      //
      //---------------------------------------------------------------------//
#     ifndef __CUDACC__
      DataHolder(Diffusion_Params<F> const& params)
      : m_S0     (params.m_S0),
        m_withReg(params.m_withReg),
        m_fromIdx(0)
      {}
#     endif

      // Copy Ctor and Dtor are auto-generated

      DEVICE F    GetS0()     const { return m_S0; }
      DEVICE bool HasRegime() const { return m_withReg; }

      //---------------------------------------------------------------------//
      // Templates for "GetSDECoeffs" and "TransPDF":                        //
      //---------------------------------------------------------------------//
      // Template parameter "D" will be a subclass of this "DataHolder" class
      // (to be declated in a corresp subclass of "Diffusion1D"):
      //
      // (1) COEFFS, SINGLE-POINT:
      //
      template<typename D>
      DEVICE static void GetSDECoeffsGen1
      (
        D const& der,  F S, F ty, TradingCalendar::CalReg reg,
        F* trendS, F* volS, F* IR
      );

      // (2) COEFFS, ALL-POINTS-TOGETHER (Array Form):
      // See "GetSDECoeffs" of the outer class for the array size info:
      //
      template<typename D>
      DEVICE static void GetSDECoeffsGenM
      (
        D const& der, int m, F const* Sx, F ty, TradingCalendar::CalReg reg,
        F* trendS,  F* volS, F* IR
      );

      // (3) Generic "TransPDF" (Quasi-Normal Approximation):
      //
      template<typename D>
      DEVICE static F TransPDFGen
        (D const& der, F S1, F ty1, F S2, F dty, TradingCalendar::CalReg reg);

#     ifndef __CUDACC__
      //---------------------------------------------------------------------//
      // Templates for "MCPaths":                                            //
      //---------------------------------------------------------------------//
      // HIGH-LEVEL API (NOT for CUDA). NB: "Low-Level" CUDA-compatible generic
      // API is provided separately (in "Diffusion1D_MC_Core.hpp"):
      //
      template<typename D>
      static void MCPathsGen
      (
        D const&                        der,
        std::vector<TimeNode<F>> const& timeLine,     // size = L
        int                             P,
        RNG<F> const&                   rng,          // Incl CUDAEnv
        Matrix<F>*                      S,            // size = (L, P)
        Matrix<F>*                      IR,           // size = (L, P)
        bool                            useAntithetic = false,
        TimeNode<F>**                   cudaTimeLine  = nullptr,
        F**                             cudaS         = nullptr,
        F**                             cudaIR        = nullptr
      );
#     endif // !__CUDACC__
    };
    // End of "DataHolder"

# ifndef __CUDACC__
  //=========================================================================//
  // "Diffusion1D" itself (virtual, not for CUDA):                           //
  //=========================================================================//
  private:
    // Default Ctor is completely hidden:
    Diffusion1D();

  protected:
    DateTime    m_t0;    // The only data fld stored directly on the Diffusion:
                         // "DateTime" cannot go into CUDA-enabled DataHolder!

    // Non-default ctor: made "protected" as it cannot be used externally any-
    // way, s this class is abstract:
    //
    Diffusion1D(DateTime t0): m_t0(t0)  {}

  public:
    //-----------------------------------------------------------------------//
    // Virtual Dtor, Virtual Accessors:                                      //
    //-----------------------------------------------------------------------//
    virtual ~Diffusion1D() {}

    // The following method should return "true" iff this is an Interest Rate
    // model (then "S" is the rate itself or its stochastic component):
    //
    virtual bool IsIRModel() const = 0;

    // Computing the Interest Rate separately. For non-IR models, it does not
    // actually depend on "S";  for IR models, it typically depends on "S",
    // hence the "S" arg; "ty" is the Year Fraction from "t0":
    //
    virtual F GetIR(F S, F ty, TradingCalendar::CalReg reg) const = 0;

    // The following accessors are to be implemented by the derived classes via
    // the corresp sub-classes of "DataHolder". XXX: it is slightly sub-optimal
    // to make them virtual, but we cannot do otherwise, because  "Diffusion1D"
    // itself DOES NOT CONTAIN any "DataHolder" -- only the derived  Diffusions
    // contain corresp derivied DataHolders (to avoid data duplication):
    //
    virtual F        GetS0() const = 0;
    virtual bool HasRegime() const = 0;

    // However, GetT0() is implemented directly on the Diffusion -- "DateTime"
    // is not available for CUDA-compatible "DataHolder":
    //
    DateTime GetT0() const { return m_t0; }

    //-----------------------------------------------------------------------//
    // "GetSDECoeffs":                                                       //
    //-----------------------------------------------------------------------//
    // (1) SINGLE-POINT:
    // Evaluate both SDE coeffs at once for given (S, t):
    //
    virtual void GetSDECoeffs
      (F S, F ty, TradingCalendar::CalReg reg, F* trendS, F* volS, F* IR)
    const = 0;

    // (2) ALL-POINTS-TOGETHER:
    // As above, but for all "Sx" simultaneously. The input and output arrays
    // must be of the following lengths:
    // len(Sx)     = m
    // len(TrendS) = m
    // len(VolS)   = m
    // len(IR)     = 1 (if not a Rate model) or m (for a Rate model):
    //
    // (2a) Array Form:
    //
    virtual void GetSDECoeffs
    (
      int m, F const* Sx, F  ty, TradingCalendar::CalReg reg,
      F* trendS, F* volS, F* IR
    )
    const = 0;

    // (2b) Vector Form. NB: output args are still arrays, not vectors -- this
    // is for maximum efficiency:
    //
    void GetSDECoeffs
    (
      Vector<F> const& Sx, F  ty, TradingCalendar::CalReg reg,
      F* trendS,  F* volS, F* IR
    )
    const
      { GetSDECoeffs(int(Sx.size()), &(Sx[0]), ty, reg, trendS, volS, IR); }

    //-----------------------------------------------------------------------//
    // "TransPDF":                                                           //
    //-----------------------------------------------------------------------//
    // Density Function for Transition Probabilities: analytical or otherwise
    // (e.g. based on numerical Heat Kernel computations).
    // NB: the whole interval [ty1..ty2] must have same "reg";
    // S1: initial condition at "ty1" (expressed in Years since "t0"):
    // S2: curr point at (ty2 = ty1 + dty) ("dty" is a Year Fraction),
    //     for which the joint PDF is returned:
    //
    virtual F TransPDF
      (F S1, F ty1,  F S2, F dty, TradingCalendar::CalReg reg)
    const = 0;

    //-----------------------------------------------------------------------//
    // "MCPaths":                                                            //
    //-----------------------------------------------------------------------//
    // NB:
    // (*) "timeLine" must start from ty=0 (corresponds to "m_t0");
    // (*) all data are initially in Host memory; if CUDA is used, they are
    //     copied to/from CUDA memory;
    // (*) "S" and/or "IR", if non-NULL, are auto-resized to size (L, P);
    // (*) if futher in-CUDA processing of the generated paths is required (as
    //     opposed to returning them into the Host space), set "S" and/or "IR"
    //     to NULL but make "cudaTimeLine" and/or ... non-NULL;  the corr CUDA
    //     ptrs will be returned via those params;
    // (*) if CUDA is to be used, the corresp "CUDAEnv" is taken from "RNG";
    // (*) "useAntithetic" should normally be used for MC pricing but not for
    //     PDF construction, because pricing results  in non-linear function
    //     application to the PDF path values, thus providing a "good mix-up"
    //     for antithetic paths...
    // (*) the method is Euler-Maruyama only; Milstein scheme is currently NOT
    //     used because it has problems with extending it to time-dependent
    //     coeffs and/or multiple dimensions, and because there are some re-
    //     ports of it not having any real advantage over Euler-Maruyama:
    //
    virtual void MCPaths
    (
      std::vector<TimeNode<F>> const& timeLine, // size = L
      int                             P,        // Number of Paths
      RNG<F> const&                   rng,      // Incl CUDAEnv
      Matrix<F>*                      S,        // size =  (L, P)
      Matrix<F>*                      IR,       // NULL or (L, P)
      bool                            useAntithetic = false,
      // Output CUDA ptrs (set to NULL if provided but CUDA is not used):
      TimeNode<F>**                   cudaTimeLine  = nullptr,
      F**                             cudaS         = nullptr,
      F**                             cudaIR        = nullptr
    )
    const = 0;

# endif // !__CUDACC__
  };
}
