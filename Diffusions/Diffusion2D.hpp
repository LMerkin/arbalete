// vim:ts=2:et
//===========================================================================//
//                             "Diffusion2D.hpp":                            //
//                       Diffusions in the (S,V) Space:                      //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Common/CUDAEnv.hpp"
#include "Diffusions/Diffusion2D.h"
#include "Diffusions/Diffusion2D_MC_Core.hpp"
#include <cstdlib>
#include <stdexcept>
#include <cassert>

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "Diffusion2D::DataHolder": Non-Default Ctor: Not for CUDA:              //
  //=========================================================================//
# ifndef __CUDACC__
  template<typename F>
  Diffusion2D<F>::DataHolder::DataHolder
    (Diffusion_Params<F> const& params)
  : m_S0     (params.m_S0),
    m_V0     (F(0.0)),                // Initially
    m_withReg(params.m_withReg),
    m_fromIdx(0)                      // Initially
  {
    // Store the coeffs for "Diffusion2D" proper and memoise their number (so
    // the sub-classes can initialise themselves from the corresp position in
    // "coeffs"):
    //
    Vector<F> const& coeffs = params.m_coeffs;
    if (coeffs.empty())
      throw invalid_argument
            ("Diffusion2D Ctor: coeffs must contain at least V0");

    m_V0  = coeffs[0];
    if (m_V0 < F(0.0))
      throw invalid_argument("Diffusion2D Ctor: V0 must be >= 0");

    // Currently, only "V0" (the initial element of "coeffs") is consumed by
    // this ctor -- others left for ctors of derived classes:
    m_fromIdx = 1;
  }
# endif // !__CUDACC__

  //=========================================================================//
  // "GetSDECoeffsGen1": One-Point:                                          //
  //=========================================================================//
  // NB: "reg" is passed as parameter, rather than copmputed internally, for 2
  // reasons:
  // (1) "t" may be a regime-switching point itself, then "reg" depends on the
  //     direction of time flow (only the Grid knows that);
  // (2) for efficiency -- same "reg" may be re-used by mutiple calls if there
  //     is no regime-switching between them.
  // NB: "reg" is ignored anyway if the diffusion is not regime-switching:
  //
  template<typename F>
  template<typename D>
  DEVICE void Diffusion2D<F>::DataHolder::GetSDECoeffsGen1
  (
    D const& der,  F S,  F V,      F  ty,     TradingCalendar::CalReg reg,
    F* trendS, F* volS1, F* volS2, F* trendV, F* volV, F* corrSV, F* IR
  )
  {
    // NB: Negative "ty" values are in general NOT allowed:
    assert(ty >= F(0.0)   && trendS != nullptr && volS1  != nullptr &&
           volS2  != nullptr && trendV != nullptr && volV   != nullptr &&
           corrSV != nullptr);

    // "S" may be negative for IT models; "V" is always non-negative (vol/var):
    assert((der.IsIRModel() || S >= F(0.0)) && V >= F(0.0));

    *trendS = der.TrendS(S, ty, reg);
    *volS1  = der.VolS1 (S, ty, reg);
    *volS2  = der.VolS2 (V, ty, reg);
    *trendV = der.TrendV(V, ty, reg);
    *volV   = der.VolV  (V, ty, reg);
    *corrSV = der.CorrSV(ty,    reg);

    if (IR != nullptr)
      *IR = der.IR(S, ty, reg);
  }

  //=========================================================================//
  // "GetSDECoeffsGenM": All-at-Once:                                        //
  //=========================================================================//
  // NB: See the comments above regarding "reg":
  //
  template<typename F>
  template<typename D>
  DEVICE void Diffusion2D<F>::DataHolder::GetSDECoeffsGenM
  (
    D const& der, int m, F const* Sx, int n, F const* Vx, F ty,
    TradingCalendar::CalReg reg,
    F* trendS, F* volS1, F* volS2,    F* trendV, F* volV, F* corrSV, F* IR
  )
  {
    // NB: Negative "ty" values are in general NOT allowed:
    assert(m  >= 2 && n >= 2 && Sx     != nullptr && Vx    != nullptr &&
           ty >= F(0.0)      && trendS != nullptr && volS1 != nullptr &&
           volS2  != nullptr    && trendV != nullptr && volV  != nullptr &&
           corrSV != nullptr);

    // Iff "vectorIR" is set, IR is "S"-dependent:
    bool vectorIR = (IR != nullptr) && (der.IsIRModel());

    // "S"-dependent coeffs:
    //
    for (int i = 0; i < m; ++i)
    {
      F S = Sx[i];
      assert(der.IsIRModel() || S >= F(0.0));

      *(trendS++) = der.TrendS(S, ty, reg);
      *(volS1 ++) = der.VolS1 (S, ty, reg);

      if (vectorIR)
        // Variable IR:
        *(IR  ++) = der.IR    (S, ty, reg);
    }

    // "V"-dependent coeffs:
    //
    for (int j = 0; j < n; ++j)
    {
      F V = Vx[j];
      assert(V >= F(0.0));

      *(volS2++)  = der.VolS2 (V, ty, reg);
      *(trendV++) = der.TrendV(V, ty, reg);
      *(volV  ++) = der.VolV  (V, ty, reg);
    }

    // Scalar coeffs:
    //
    *corrSV = der.CorrSV(ty, reg);

    if (!vectorIR && IR != nullptr)
      // In this case, IR is scalar, too (it does not depend on "S", so use
      // S=0 in the following call):
      *IR = der.IR(F(0.0), ty, reg);
  }

  //=========================================================================//
  // "TransPDF":                                                             //
  //=========================================================================//
  // XXX: This is a generic low-accuracy implementation which uses Bi-Variate
  // Normal approximation (only suitable for sufficiently small "dty"s). Also
  // note the above remarks regarding externally-computed "reg":
  //
  template<typename F>
  template<typename D>
  DEVICE F Diffusion2D<F>::DataHolder::TransPDFGen
  (
    D const& der, F S1, F V1, F ty1, F S2, F V2, F dty,
    TradingCalendar::CalReg reg
  )
  {
    // NB: Negative "ty1" or "dty" values are in general NOT allowed:
    assert(ty1 >= F(0.0) && dty > F(0.0));

    // Compute the Diffusion Coeffs at (S1,V1,ty1); IR is not required:
    F muS, sigmaS1, sigmaS2, muV, sigmaV, corrSV;
    der.GetSDECoeffs(S1, V1, ty1, reg,
                     &muS, &sigmaS1, &sigmaS2, &muV, &sigmaV, &corrSV, nullptr);

    // 2D Normal PDF:
    F sigmaS = sigmaS1 * sigmaS2;
    assert(sigmaS > F(0.0) && sigmaV > F(0.0));

    F sqrdt  = SqRt(dty);  // XXX: Inefficiency if called mutiple times for
    sigmaS  *= sqrdt;      // same "dt"...
    sigmaV  *= sqrdt;

    F X      = ((S2 - S1) - muS * dty) / sigmaS;
    F Y      = ((V2 - V1) - muV * dty) / sigmaV;

    F sqrh2  = F(1.0) - corrSV * corrSV;
    F sqrh   = SqRt(sqrh2);

    F res    =
      Exp(- F(0.5) / sqrh2 * (X*X + Y*Y - F(2.0) * corrSV * X*Y)) /
      (F(2.0) * Pi<F>() * sigmaS * sigmaV * sqrh);
    return res;
  }

# ifndef __CUDACC__
  //=========================================================================//
  // "MCPathsGen": High-Level Interface to "MCPathsGen_Core":                //
  //=========================================================================//
  template<typename F>
  template<typename D>
  void Diffusion2D<F>::DataHolder::MCPathsGen
  (
    D const&                   der,
    vector<TimeNode<F>> const& timeLine, // size =  L
    int                        P,        // Number of Paths
    RNG<F> const&              rng,      // Incl CUDAEnv
    Matrix<F>*                 S,        // size =  (L,  P)
    Matrix<F>*                 V,        // size =  (L,  P)
    Matrix<F>*                 IR,       // NULL or (L,  P)
    bool                       useAntithetic,
    TimeNode<F>**              cudaTimeLine,
    F**                        cudaS,
    F**                        cudaV,
    F**                        cudaIR
  )
  {
    //-----------------------------------------------------------------------//
    // Check the Args:                                                       //
    //-----------------------------------------------------------------------//
    int L  = int(timeLine.size());
    if (L == 0 || P <= 0)
      throw invalid_argument
            ("Diffusion2D::DataHolder::MCPathsGen: Invalid L or P");
    int LP = L * P;

    // XXX: Check for an "int" overflow here. TODO: 64-bit size types:
    if (LP / P != L || LP / L != P || LP < 0)
      throw runtime_error("Diffusion2D::DataHolder::MCPathsGen: LP overflow");

    if (useAntithetic && P % 2 != 0)
      throw invalid_argument
            ("Diffusion2D::DataHolder::MCPathsGen: "
             "Number of Paths must be EVEN for Antithetic");

    // "timeLine" must begin with ty=0:
    if (timeLine[0].m_ty != F(0.0))
      throw invalid_argument
        ("Diffusion2D::DataHolder::MCPathsGen: TimeLine must start at 0");
    assert(timeLine[0].m_secs == 0);

    // Automatically resize the output if necessary -- XXX: this is probably OK
    // for a high-level function:
    if (S != nullptr)
      S->resize(L, P);
    if (V != nullptr)
      V->resize(L, P);
    if (IR != nullptr)
      IR->resize(L, P);

    // Initialise CUDA output ptrs (if used) to NULL:
    if (cudaTimeLine != nullptr)
      *cudaTimeLine = nullptr;

    if (cudaS  != nullptr)
      *cudaS  = nullptr;

    if (cudaV  != nullptr)
      *cudaV  = nullptr;

    if (cudaIR != nullptr)
      *cudaIR = nullptr;

    CUDAEnv const* cudaEnv = rng.GetCUDAEnv();  // Alias ptr!

    if (cudaEnv == nullptr)
    {
      //---------------------------------------------------------------------//
      // Just invoke the Low-Level Version on Host:                          //
      //---------------------------------------------------------------------//
      // Here "S" and "V" must be non-NULL:
      if (S == nullptr || V == nullptr)
        throw invalid_argument("Diffusion2D::DataHolder::MCPathsGen: "
                               "Host Mode: S and V must be non-nullptr");

      MCPathsGen2D_Core<F, D>
      (
        der,   nullptr,   rng.GetCore(), timeLine.data(),   L,  P,
        &((*S)(0,0)),  &((*V)(0,0)),  (IR != nullptr) ? &((*IR)(0,0)) : nullptr,
        useAntithetic
      );
    }
    else
    {
      //---------------------------------------------------------------------//
      // CUDA-Based Simulation:                                              //
      //---------------------------------------------------------------------//
      // Start the CUDA section:
      cudaEnv->Start();

      // Copy the Input Data into the CUDA space:
      TimeNode<F>* tmpTimeLine = cudaEnv->CUDAAlloc<TimeNode<F>>(L);
      cudaEnv->ToCUDA<TimeNode<F>>(timeLine.data(), tmpTimeLine, L);

      // Allocate memory for output data:
      F* tmpS  = cudaEnv->CUDAAlloc<F>(LP);
      F* tmpV  = cudaEnv->CUDAAlloc<F>(LP);
      F* tmpIR =
        (IR != nullptr || cudaIR != nullptr)
        ? cudaEnv->CUDAAlloc<F>(LP)
        : nullptr;

      // Construct the Schedule: In the Antithetic mode, 2 paths are made at
      // once, so need to divide P/2 work items between all CUDA threads; NB:
      // The Schedule comes into CUDA space directly:
      //
      CUDAEnv::Sched1D* sched =
        cudaEnv->MkSched1D(useAntithetic ? (P/2) : P);

      // Invoke the CUDA Kernel. BEWARE of CUDA memory leaks if invocation
      // fails:
      try
      {
        MCPathsGen2D_KernelInvocator<F, D>::Run
        (
          der,  cudaEnv, sched, rng.GetCore(), tmpTimeLine, L, P,
          tmpS, tmpV,    tmpIR, useAntithetic
        );
      }
      catch(...)
      {
        // De-allocate CUDA memory:
        cudaEnv->CUDAFree<TimeNode<F>>(tmpTimeLine);
        cudaEnv->CUDAFree<F>(tmpS);
        cudaEnv->CUDAFree<F>(tmpV);
        if (tmpIR != nullptr)
          cudaEnv->CUDAFree<F>(tmpIR);
        cudaEnv->CUDAFree<CUDAEnv::Sched1D>(sched);
        throw;
      }

      // De-allocate the CUDA Schedule:
      cudaEnv->CUDAFree<CUDAEnv::Sched1D>(sched);

      // Copy the data back into the Host space, if the corresp ptrs are
      // non-NULL. This has nothing to do with whether the CUDA data are
      // subsequently preserved (see below):
      assert(tmpTimeLine != nullptr && tmpS != nullptr && tmpV != nullptr);

      if (S != nullptr)
        cudaEnv->ToHost<F>(tmpS,  &((*S) (0,0)), LP);
      if (V != nullptr)
        cudaEnv->ToHost<F>(tmpV,  &((*V) (0,0)), LP);
      if (IR != nullptr)
      {
        assert(tmpIR != nullptr);
        cudaEnv->ToHost<F>(tmpIR, &((*IR)(0,0)), LP);
      }

      // Preserve or de-allocate the CUDA buffers:
      if (cudaTimeLine != nullptr)
        *cudaTimeLine = tmpTimeLine;
      else
        cudaEnv->CUDAFree<TimeNode<F>>(tmpTimeLine);

      if (cudaS  != nullptr)
        *cudaS = tmpS;
      else
        cudaEnv->CUDAFree<F>(tmpS);

      if (cudaV  != nullptr)
        *cudaV = tmpV;
      else
        cudaEnv->CUDAFree<F>(tmpV);

      if (cudaIR != nullptr)
      {
        assert(tmpIR != nullptr);
        *cudaIR = tmpIR;
      }
      else
        if (tmpIR != nullptr)
          cudaEnv->CUDAFree<F>(tmpIR);

      // All done. Stop the CUDA section:
      cudaEnv->Stop();
    }
#   ifndef NDEBUG
    // Verify the basic properties of the matrices generated:
    for (int j = 0; j < P; ++j)
    {
      assert(S == nullptr || (*S)(0,j) == der.GetS0());
      assert(V == nullptr || (*V)(0,j) == der.GetV0());
    }
#   endif
  }
# endif // !__CUDACC__
}
