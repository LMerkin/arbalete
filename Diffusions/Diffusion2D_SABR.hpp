// vim:ts=2:et
//===========================================================================//
//                         "Diffusion2D_SABR.hpp":                           //
//   SABR StocLocalVol Diffusion (with CEV, Mean Reversion and Vol Trend):   //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Diffusions/Diffusion2D.hpp"
#include "Diffusions/Diffusion2D_SABR.h"
#include <cassert>
#include <cstdio>
#ifndef  __CUDACC__
#include <stdexcept>
#endif

namespace
{
  using namespace Arbalete;

  //-------------------------------------------------------------------------//
  // "Tol":                                                                  //
  //-------------------------------------------------------------------------//
# ifndef NDEBUG
  template<typename F>
  DEVICE inline F Tol();

  template<>
  DEVICE inline double Tol<double>() { return 1e-5;   }

  template<>
  DEVICE inline float  Tol<float>()  { return 0.001f; }
# endif

  //=========================================================================//
  // "GeodesicSABR" Class:                                                   //
  //=========================================================================//
  // Used in "TransPDF_Paulot". Describes SABR geodesics in the (x,y) plane:
  //
  template<typename F>
  class GeodesicSABR
  {
  private:
    //-----------------------------------------------------------------------//
    // Data Flds:                                                            //
    //-----------------------------------------------------------------------//
    bool    m_isCircle; // Geodesic may be a Circle (generic) or vertical line
    F       m_X0;       // Circle center: (X0,0); line: (X0,y)
    F       m_R;        // Circle radius  (or +oo for a  line)
    F       m_u1;       // Polar angle of (x1,y1) (or just "y1" for a line)
    F       m_u2;       // Polar angle of (x2,y2) (or just "y2" for a line)

    F       m_x1;       // Memoise the end-point co-ords:
    F       m_y1;
    F       m_x2;
    F       m_y2;

  public:
    //-----------------------------------------------------------------------//
    // Default Ctor (for arrays initialisation):                             //
    //-----------------------------------------------------------------------//
    // Creates a degenerate straight line:
    //
    DEVICE GeodesicSABR()
    : m_isCircle(false),
      m_X0(F(0.0)),
      m_R (Inf<F>()),
      m_u1(F(0.0)),
      m_u2(F(0.0)),
      m_x1(F(0.0)),
      m_y1(F(0.0)),
      m_x2(F(0.0)),
      m_y2(F(0.0))
    {}

    //-----------------------------------------------------------------------//
    // Non-Default Ctor:                                                     //
    //-----------------------------------------------------------------------//
    DEVICE GeodesicSABR(F x1, F y1, F x2, F y2)
    : m_x1(x1),
      m_y1(y1),
      m_x2(x2),
      m_y2(y2)
    {
      assert(y1 > F(0.0) && y2 > F(0.0));

      // XXX: If the following criterion OK?  It is used to avoid circle arcs
      // with very large radius, leading to accuracy loss, so avoid them:
      //
      if (Abs((x1 - x2) / (y2 - y1)) > F(1e-4))
      {
        // Generic Case: Circle Arc:
        m_isCircle = true;

        m_X0  = (x2*x2 - x1*x1 + y2*y2 - y1*y1)  / (F(2.0)*(x2 - x1));

        // NB: R^2 = y1^2 + (x1-X0)^2
        //         = y2^2 + (x2-X0)^2;
        // for better computational accuracy, use the average of both formulas:
        //
        m_R   = SqRt(F(0.5)*
                    (y1*y1 + y2*y2 + Sqr<F>(x1 - m_X0) + Sqr<F>(x2 - m_X0)));
        assert(m_R > F(0.0));

        // Range of Polar Angles for this Arc:
        m_u1  = ACos((x1 - m_X0) / m_R);
        m_u2  = ACos((x2 - m_X0) / m_R);

        // Check: Must have x = X0 + R cos(u), y = R sin(u):
#       ifndef NDEBUG
        F x1re = Abs((m_X0 + m_R * Cos(m_u1)) / x1 - F(1.0));
        F y1re = Abs(        m_R * Sin(m_u1)  / y1 - F(1.0));
        F x2re = Abs((m_X0 + m_R * Cos(m_u2)) / x2 - F(1.0));
        F y2re = Abs(        m_R * Sin(m_u2)  / y2 - F(1.0));

        if (x1re > Tol<F>() || y1re > Tol<F>() ||
            x2re > Tol<F>() || y2re > Tol<F>())
        {
          printf("ERROR: Loss of Accuracy:  (%.6e, %.6e) -> (%.6e, %.6e):\n"
                 "X0=%.6e, R=%.6e, u1=%.6e, u2=%.6e,\n"
                 "err_x1=%.3e, err_y1=%.3e, err_x2=%.3e, err_y2=%.3e\n",
                 x1, y1, x2, y2, m_X0, m_R, m_u1, m_u2, x1re, y1re, x2re, y2re);
          assert(false);
        }
#       endif
      }
      else
      {
        // Degenerate Case: Vertical or nearly-vertical line:
        assert(y1 != y2);
        m_isCircle = false;

        m_R   = Inf<F>();
        m_X0  = (x1 + x2) / F(2.0);
        m_u1  = y1;
        m_u2  = y2;
      }
    }

    //-----------------------------------------------------------------------//
    // Accessors:                                                            //
    //-----------------------------------------------------------------------//
    DEVICE bool IsCircle() const { return m_isCircle; }
    DEVICE F    X0()       const { return m_X0;       }
    DEVICE F    R()        const { return m_R;        }
    DEVICE F    U1()       const { return m_u1;       }
    DEVICE F    U2()       const { return m_u2;       }
    DEVICE F    x1()       const { return m_x1;       }
    DEVICE F    y1()       const { return m_y1;       }
    DEVICE F    x2()       const { return m_x2;       }
    DEVICE F    y2()       const { return m_y2;       }
  };
  // End of "GeodesicSABR"
}

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "Diffusion2D_SABR": Non-Default Ctor:                                   //
  //=========================================================================//
  // Not for CUDA compilations:
  //
# ifndef __CUDACC__
  template<typename F>
  Diffusion2D_SABR<F>::DataHolder::DataHolder
  (
    Diffusion_Params<F> const& params,
    TransPDF_Method method
  )
  : Diffusion2D<F>::DataHolder(params),
    m_method                  (method)
  {
    int from = this->m_fromIdx;
    assert(from >= 0);

    // There are 10 coeffs of this SABR model proper:
    Vector<F> const& coeffs = params.m_coeffs;
    int nc = int(coeffs.size());
    if (from > nc - 10)
      throw invalid_argument
            ("Diffusion2D_SABR::DataHolder Ctor: Too few coeffs");

    m_mu    = coeffs[from++];
    m_xiN   = coeffs[from++];
    m_beta  = coeffs[from++];
    m_kappa = coeffs[from++];
    m_a     = coeffs[from++];
    m_b     = coeffs[from++];
    m_epsTH = coeffs[from++];
    m_epsN  = coeffs[from++] * m_epsTH;
    m_rho   = coeffs[from++];
    m_ir    = coeffs[from++];

    if (Abs(m_rho) >= F(1.0) || m_epsTH <= F(0.0) || m_epsN <= F(0.0))
      // NB: We do NOT require "beta" to be in [0..1]; also no restrictions in
      // general on "kappa", "a", "b":
      throw invalid_argument
            ("Diffusion2D_SABR::DataHolder Ctor: Invalid param(s)");
  }
# endif //!__CUDACC__

  //=========================================================================//
  // "TransPDF_HaganEtAl":                                                   //
  //=========================================================================//
  // Using the o(dty) solution by P.Hagan, A.Lesniewski and D.Woodward (2005):
  //
  template<typename F>
  DEVICE F Diffusion2D_SABR<F>::DataHolder::TransPDF_HaganEtAl
    (F S1, F V1, F UNUSED_PARAM(ty1), F S2, F V2, F dty,
     TradingCalendar::CalReg reg)
  const
  {
    // First, reduce spot price to real-measure "futures" price (de-trended).
    // However, in this process, the structure of the vol term is unaffected
    // only if mu=0 or beta=1 -- XXX: otherwise we cannot proceed:
    //
    if ((m_beta != F(1.0) && m_mu != F(0.0)) || m_kappa != F(0.0) ||
        m_a     != F(0.0))
#   ifndef __CUDACC__
      throw runtime_error
            ("Diffusion2D_SABR::TransPDF_HaganEtAl: Can't transform");
#   else
      return NaN<F>();
#   endif

    // Regime-dependent params:
    F xi    = (reg == TradingCalendar::TradingHours) ? F(1.0)  : m_xiN;
    F eps   = (reg == TradingCalendar::TradingHours) ? m_epsTH : m_epsN;
    assert(F(0.0) < xi && xi <= F(1.0) && eps > F(0.0));

    // Some short-cuts:
    F    sqrh2   = F(1.0) - m_rho * m_rho;
    F    sqrh    = SqRt(sqrh2);
    F    sqrh3   = sqrh2  * sqrh;
    F    b1      = F(1.0) - m_beta;

    // Solution uses Bwd Kolmogorov (Feynman-Kac) PDE  wrt (f1,V1)@t1  where
    // f1 = S1 * Exp(mu * (t2-t1)); (S2,V2)@t2 is the "fixed" terminal cond:
    // then x1 = f1, x2 = f(t2) = S2:
    //
    F x1  = S1 * Exp(m_mu * dty);
    F y1  = V1 / eps;
    F x2  = S2;
    F y2  = V2 / eps;
    assert(x1 > F(0.0) && y1 > F(0.0) && x2 > F(0.0) && y2 > F(0.0));

    // Compute the geodesic distance ("delta"):
    //
    F CII =
      ((m_beta == F(1.0)) ?  Log(x2 / x1) : (Pow(x2, b1) - Pow(x1, b1)) / b1)
      / xi;

    F dy      = y2 - y1;                    // NB: this way!
    F z       = (CII * CII - F(2.0) * m_rho * dy * CII + dy * dy) /
                (F(2.0) * sqrh2 * y1 * y2);
    F deltaCH =  F(1.0) + z;                // cosh(delta)
    F deltaSH =  SqRt(z * (F(2.0) + z));    // sinh(delta)

    // delta  = acosh(deltaCH) -- but do it correctly for small "z":
    F delta   = Log1P(z + deltaSH);
    F delta2  = delta * delta;

    // "deltaR" is actually the van Vleck - Morette determinant:
    F deltaR  = (z != F(0.0)) ? (delta  / deltaSH) : F(1.0);
    assert(F(0.0) < deltaR && deltaR <= F(1.0));

    // Compute the core density:
    //
    F tau     = eps * eps * dty;
    F C2      = xi  * Pow(S2,  m_beta);
    F C1der   = xi  * m_beta * Pow(S1, -b1);
    F res     =
      Exp(- delta2 / (F(2.0)  * tau)) /
      (F(2.0) * Pi<F>() * tau * sqrh  * y2 * y2 * C2 * eps) *
      SqRt(deltaR);

    // Compute the correction factor (error: o(dty)):
    //
    F q    = y1 * C1der * (CII - m_rho * dy) / (F(2.0) * sqrh3 * y2);
    F corr = F(1.0) - deltaR * q;
    F A    = F(0.0);
    F B    = F(0.0);

    if (delta > F(0.01))
    {
      // Generic case: "z", "delta" and "deltaSH" are bounded away from 0:
      //
      F dd = deltaR * deltaCH - F(1.0);   // delta * CoTH(delta) - 1
      A    = dd / delta2;
      B    = (F(3.0)* dd - delta2) / (delta * deltaSH);
    }
    else
    {
      // "delta" is close to 0, provide Taylor series approximations.
      // "A" is expanded up to delta^8:
      //
      A    = (((F(2.0 / 93555.0) * delta2 - F(1.0 / 4725.0)) * delta2 +
                F(2.0 / 945.0))  * delta2 - F(1.0 / 945.0))  * delta2 +
                F(1.0 / 3.0);

      B    = (((F(1073.0 / 2494800.0) * delta2 -
                F(113.0  / 37800.0))  * delta2 +
                F(11.0   / 630.0))    * delta2 -
                F(1.0    / 15.0))     * delta2;
    }

    // Adjust the correction factor:
    corr -= tau / F(8.0) * (F(1.0) + A + B * q);

    // Apply the correction factor:
    res *= corr;

    // For large "delta", may get a corr < 0 due to 1st-order in "dty", and
    // then a slightly negative "res" -- make it 0:
    //
    res = Max<F>(res, F(0.0));
    return res;
  }

  //=========================================================================//
  // "TransPDF_Paulot":                                                      //
  //=========================================================================//
  template<typename F>
  DEVICE F Diffusion2D_SABR<F>::DataHolder::TransPDF_Paulot
  (
    F S1, F V1, F ty1, F S2, F V2, F dty, TradingCalendar::CalReg reg,
    bool  with_a1
  )
  const
  {
    //-----------------------------------------------------------------------//
    // Transform (S,V) -> (x,V):                                             //
    //-----------------------------------------------------------------------//
    // Regime-dependent params:
    F xi    = (reg == TradingCalendar::TradingHours) ? F(1.0)  : m_xiN;
    F eps   = (reg == TradingCalendar::TradingHours) ? m_epsTH : m_epsN;
    assert(F(0.0) < xi && xi <= F(1.0) && eps > F(0.0));

    // Some short-cuts:
    F sqrh2 = F(1.0) - m_rho * m_rho;
    F sqrh  = SqRt(sqrh2);

    // If there is no CEV (i.e. we have beta = 1), then eliminate "mu" by using
    // "futures prices":
    // F(t) = S(t) * exp(mu * (t2 - t)),
    // i.e.
    // F1 = S1 * exp(mu * (t2 - t1)),
    // F2 = S2
    // Thus, no density re-scaling s at "t2": only the initial cond wrt "S" is
    // affected.
    // After this transform, will have  mu == 0, which facilitates convergence
    // of the approximations used below:
    //
    F F1 = (m_beta == F(1.0)) ? (S1 * Exp(m_mu * dty)) : S1;
    F F2 = S2;

    // Now do the transform to a Poincare structure (it is not unique though);
    // "V" remains as is; S -> x(S,V):
    //
    F x1      = - m_rho * V1 / sqrh;
    F b1      = F(1.0) - m_beta;
    F F1b1    =  (m_beta == F(1.0)) ? F(1.0) : Pow(F1,b1);
    F CII     = ((m_beta == F(1.0))
                 ? Log(F2 / F1) : (Pow(F2, b1) - F1b1) / b1) / xi;
    F x2      = (eps * CII - m_rho * V2) / sqrh;

    //-----------------------------------------------------------------------//
    // Geodesic Distance ("delta"):                                          //
    //-----------------------------------------------------------------------//
    F z       = (Sqr<F>(x2 - x1) + Sqr<F>(V2 - V1)) / (F(2.0) * V1 * V2);
    F deltaSH =  SqRt(z * (F(2.0) + z));    // sinh(delta)

    // delta  = acosh(deltaCH) -- but do it correctly for small "z":
    F delta   = Log1P(z + deltaSH);
    F delta2  = delta * delta;

    // "deltaR" is actually the van Vleck - Morette determinant:
    F deltaR  = (z != F(0.0)) ? (delta  / deltaSH) : F(1.0);
    assert(F(0.0) < deltaR && deltaR <= F(1.0));

    //-----------------------------------------------------------------------//
    // Abelian Connection in the (x,V) Space:                                //
    //-----------------------------------------------------------------------//
    // NB: in these co-ords, Abelian Connection only depends on the convective
    // (drift) terms.
    // Construct the geodesic (x1,V1) -> (x2,V2):
    //
    GeodesicSABR<F> geod(x1, V1, x2, V2);

    // Range of parameters (NB: may have u1 > u2 as well):
    F u1   = geod.U1();
    F u2   = geod.U2();

    F AI   = F(0.0);    // Abelian Connection to be computed

    F eps2 = eps * eps;
    F k    = m_a + m_kappa * (m_a * ty1 + m_b);
    F LV   = k / V2 - k / V1;

    if (geod.IsCircle())
    {
      // This is the Generic Case:
      F L1   = Log(((F(1.0) - Cos(u2)) * Sin(u1)) /
                   ((F(1.0) - Cos(u1)) * Sin(u2)));
      F L2   = Log(Sin(u2)  / Sin(u1));
      F L3   = u2 - u1;
      F R    = geod.R();

      // 1st integral (depends on "S" and "V"):
      F AI1  = F(0.0);

      if (m_beta != F(1.0))
      {
        // With CEV:
        F X0 = geod.X0();

        AI1  = m_mu / (R * eps2 * xi * sqrh) *
               ((F1b1 * eps  + b1 * X0 * xi * sqrh) * L1 +
                b1 * R * xi  * (sqrh * L2   + m_rho * L3));
      }
      else
        // Without CEV (beta = 1):  would be:
        // m_mu / (R * eps * xi * sqrh) * L1)
        // but after our transform to "futures", mu = 0, so:
        //
        AI1  = F(0.0);

      // 2nd integral  (depends on "V" only, does not depend on "beta"):
      F AI2  = (m_rho / sqrh * (m_kappa * L3 - k / R * L1) + m_kappa * L2 + LV)
               / eps2;

      // Finally:
      AI = AI1 + AI2;
    }
    else
      // Degenerate Case: Geodesic is a vertical straight line (x = const):
      // AI1 is 0 since it is an integral over dx;  AI2 greatly simplifies;
      // so:
      AI = (LV + m_kappa * Log(V2 / V1)) / eps2;

    //-----------------------------------------------------------------------//
    // Core Density (with Abelian Connection) in the (x,V) Space:            //
    //-----------------------------------------------------------------------//
    F tau     = eps * eps * dty;
    F res     =
      Exp(- delta2 / (F(2.0)  * tau) - AI) /
      (F(2.0) * Pi<F>() * tau * V2 * V2) * SqRt(deltaR);

    //-----------------------------------------------------------------------//
    // "a1" Coefficient (if enabled):                                        //
    //-----------------------------------------------------------------------//
    if (with_a1)
    {
      // XXX: "a1" coefficient is computed using a simplified method by P.Bour-
      // gade, O.Croissant (2005) -- essentially,  instead of integrating over
      // the geodesic, we evaluate the integrand at (x1, V1):
      //
      // Divergence of the drift: NB: it is constant for SABR:
      // NB: if beta = 1, then effectively mu = 0 (in "futures"), but in this
      // case b1 = 0 as well, so we get same effect:
      //
      F div   = (m_mu * b1 - m_kappa) / eps2;

      // Square of the Euclidian norm of the drift vector in (x,V) evaluated at
      // the initial point (x(F1,V1), V1):
      //
      F keV   = (k - m_kappa * V1) / eps;
      F kee   = keV / eps;

      F norm2 =
        (m_beta != F(1.0))
        ? ((Sqr<F>(m_mu / xi * F1b1 - m_rho * keV) / sqrh2 + keV * keV) / eps2)
        : (kee  * kee   / sqrh2);

      // Can now estimate "a1":
      F a1    =
        - F(1.0 / 6.0) + F(0.5) * div + (kee + F(1.5) * norm2 / V1) / V1;

      // Apply "a1":
      res    *= (F(1.0) + a1 * tau);
    }

    //-----------------------------------------------------------------------//
    // Map the Transition Density Back to the (S,V) Space:                   //
    //-----------------------------------------------------------------------//
    F C2      = xi  * ((m_beta == 1) ? F2 : Pow(F2, m_beta));
    res *= (eps / (sqrh * C2));
    return res;
  }

  //=========================================================================//
  // "TransPDF" Switch:                                                      //
  //=========================================================================//
  template<typename  F>
  DEVICE F Diffusion2D_SABR<F>::DataHolder::TransPDF
    (F S1, F V1, F ty1, F S2, F V2, F dty, TradingCalendar::CalReg reg)
  const
  {
    // Must have t0 < t, and the range [t0..t] must have constant regime
    // ("reg") -- XXX: the latter is not verified:
    if (dty <= F(0.0))
#     ifndef __CUDACC__
      throw invalid_argument
            ("Diffusion2D_SABR::DataHolder::TransPDF: Must have dty > 0");
#     else
      return NaN<F>();
#     endif

    // The probability of entering or exiting points with at least one non-
    // positive co-ordinate is 0:
    if (S1 <= F(0.0) || V1 <= F(0.0) || S2 <= F(0.0) || V2 <= F(0.0))
      return  F(0.0);

    switch (m_method)
    {
    case Diffusion2D_SABR<F>::Generic:
      // Generic implementation, always awailable:
      return this->template TransPDFGen<DataHolder>
                        (*this, S1, V1, ty1, S2, V2, dty, reg);

    case Diffusion2D_SABR<F>::HaganEtAl:
      // XXX: This only works under some conditions (checked inside the func):
      return TransPDF_HaganEtAl(S1, V1, ty1, S2, V2, dty, reg);

    case Diffusion2D_SABR<F>::Paulot0:
      // Method of L.Paulot  without computing "a1":
      return TransPDF_Paulot   (S1, V1, ty1, S2, V2, dty, reg, false);

    case Diffusion2D_SABR<F>::Paulot1:
      // Method of L.Paulot with (approximate) "a1":
      return TransPDF_Paulot   (S1, V1, ty1, S2, V2, dty, reg, true);

    default:
#     ifndef __CUDACC__
      throw runtime_error("Diffusion2D_SABR::DataHolder::TransPDF: Invalid "
                          "Method");
#     else
      return NaN<F>();
#     endif
    }
  }
}
