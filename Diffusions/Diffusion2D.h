// vim:ts=2:et
//===========================================================================//
//                            "Diffusion2D.h":                               //
//                     Diffusions in the (S,V) Space:                        //
//===========================================================================//
// NB: Although all Diffsions contain their Calendar initial DateTime (t0), it
// is  for reference only: all computations of Year Fractions and Regimes  are
// performed (based on t0) outside of Diffusions, e.g. by Grids.   This is for
// CUDA compatibility of all Diffusion methods:
//
#pragma once

#include "Common/TradingCalendar.h"
#ifndef  __CUDACC__
#include "Diffusions/Diffusion_Params.h"
#include "MonteCarlo/RNG.h"
#endif

namespace Arbalete
{
  //=========================================================================//
  // "Diffusion2D":                                                          //
  //=========================================================================//
  // XXX: we currently require separable volatility of "S"; "S" is "Spot" or
  // "Fwd" (may also be an Interest Rate), "V" is "StocVol" / "StocVariance":
  //
  // dS = trendS(S,t)  dt + volS1(S,t) volS2(V,t) dW
  // dV = trendV(V,t)  dt + volV (V,t) dZ
  // dW dZ = corrSV(t) dt
  //
  template<typename F>
  class Diffusion2D
  {
  public:
    //=======================================================================//
    // "DataHolder" (non-virtual, suitable for CUDA):                        //
    //=======================================================================//
    // NB: "DataHolder" is an inner class declared in "Diffusion2D".   However,
    // instances of this class are NOT directly contained in "Duffusion2D" obj-
    // ects. Instead, classes derived (non-virtually) from "DataHolder" are de-
    // clared in the derived classes on "Diffusion2D", and derivative instances
    // are contained in the corresponding objects:
    //
    class DataHolder
    {
    protected:
      //---------------------------------------------------------------------//
      // Data Flds:                                                          //
      //---------------------------------------------------------------------//
      F         m_S0;       // "S" at t=t0
      F         m_V0;       // "V" at t=t0
      bool      m_withReg;  // With time-based regime-switching?
      int       m_fromIdx;  // How many "coeffs" are consumed by this base cls
                            // ctor (the rest are used by the derived cls)? --
                            // currently, only the 1st coeff (V0)
    private:
      // Default Ctor is hidden:
      DataHolder();

    public:
      //---------------------------------------------------------------------//
      // Non-Default Ctor (not for CUDA) and Accessors:                      //
      //---------------------------------------------------------------------//
      // params.m_coeffs used by this ctor itelf:
      // currently only [ V0; ... ]:
      //
#     ifndef __CUDACC__
      DataHolder(Diffusion_Params<F> const& params);
#     endif

      // Copy Ctor and Dtor are auto-generated

      DEVICE F        GetS0() const   { return m_S0; }
      DEVICE F        GetV0() const   { return m_V0; }
      DEVICE bool HasRegime() const   { return m_withReg; }

      //---------------------------------------------------------------------//
      // Templated implementations of "GetSDECoeffs" and "TransPDF":         //
      //---------------------------------------------------------------------//
      // Template parameter "D" will be a subclass of this "DataHolder" class
      // (to be declated in a corresp subclass of "Diffusion2D"):
      //
      // (1) COEFFS, SINGLE-POINT:
      //
      template<typename D>
      DEVICE static void GetSDECoeffsGen1
      (
        D const& der, F S, F V, F ty,  TradingCalendar::CalReg reg,
        F* trendS, F* volS1, F* volS2, F* trendV, F* volV, F* corrSV, F* IR
      );

      // (2) COEFFS, ALL-POINTS-TOGETHER (Array Form):
      // See "GetSDECoeffs" of the outer class for the array size info:
      //
      template<typename D>
      DEVICE static void GetSDECoeffsGenM
      (
        D const& der, int m, F const* Sx,  int n, F const* Vx,  F ty,
        TradingCalendar::CalReg reg,
        F* trendS, F* volS1, F* volS2, F* trendV, F* volV, F* corrSV, F* IR
      );

      // (3) GENERIC "TransPDF" (Quasi-Normal Approximation):
      //
      template<typename D>
      DEVICE static F TransPDFGen
      (
        D const& der, F S1, F V1, F ty1, F S2, F V2, F dty,
        TradingCalendar::CalReg reg
      );

#     ifndef __CUDACC__
      //---------------------------------------------------------------------//
      // Templates for "MCPaths":                                            //
      //---------------------------------------------------------------------//
      // HIGH-LEVEL API (NOT for CUDA). NB: "Low-Level" CUDA-compatible generic
      // API is provided separately (in "Diffusion2D_MC_Core.hpp"):
      //
      template<typename D>
      static void MCPathsGen
      (
        D const&                        der,
        std::vector<TimeNode<F>> const& timeLine,    // size = L
        int                             P,
        RNG<F> const&                   rng,         // Incl CUDAEnv
        Matrix<F>*                      S,           // size = (L, P)
        Matrix<F>*                      V,           // size = (L, P)
        Matrix<F>*                      IR,          // size = (L, P)
        bool                            useAntithetic = false,
        TimeNode<F>**                   cudaTimeLine  = nullptr,
        F**                             cudaS         = nullptr,
        F**                             cudaV         = nullptr,
        F**                             cudaIR        = nullptr
      );
#     endif // !__CUDACC__
    };
    // End of "DataHolder"

# ifndef __CUDACC__
    //=======================================================================//
    // "Diffusion2D" itself (virtual, not for CUDA):                         //
    //=======================================================================//
  private:
    // Default ctor is completely hidden:
    Diffusion2D();

  protected:
    DateTime    m_t0;    // The only data fld stored directly on the Diffusion:
                         // "DateTime" cannot go into CUDA-enabled DataHolder!

    // Non-default ctor: made "protected" as it cannot be used externally any-
    // way, as this class is abstract:
    //
    Diffusion2D(DateTime t0): m_t0(t0)  {}

  public:
    //-----------------------------------------------------------------------//
    // Virtual Dtor, Virtual Accessors:                                      //
    //-----------------------------------------------------------------------//
    virtual ~Diffusion2D()     {}

    // The following method should return "true" iff this is an Interest Rate
    // model (then "S" is the rate itself or its stochastic component):
    //
    virtual bool IsIRModel()   const = 0;

    // Computing the Interest Rate separately. For non-IR models, it does not
    // actually depend on "S";  for IR models, it typically depends on "S",
    // hence the "S" arg; "ty" is the Year Fraction from "t0":
    //
    virtual F GetIR(F S, F ty, TradingCalendar::CalReg reg) const = 0;

    // The following accessors are to be implemented by the derived classes via
    // the corresp sub-classes of "DataHolder". XXX: it is slightly sub-optimal
    // to make them virtual, but we cannot do otherwise, because  "Diffusion2D"
    // itself DOES NOT CONTAIN any "DataHolder" -- only the derived  Diffusions
    // contain corresp derivied DataHolders (to avoid data duplication):
    //
    virtual F        GetS0()   const = 0;
    virtual F        GetV0()   const = 0;
    virtual bool HasRegime()   const = 0;

    // However, GetT0() is implemented directly on the Diffusion -- "DateTime"
    // is not available for CUDA-compatible "DataHolder":
    //
    DateTime GetT0() const { return m_t0; }

    //-----------------------------------------------------------------------//
    // "GetSDECoeffs":                                                       //
    //-----------------------------------------------------------------------//
    // (1) SINGLE-POINT:
    // Evaluate all SDE coeffs at once for given (S, V, t):
    //
    virtual void GetSDECoeffs
    (
      F S,  F V, F  ty,    TradingCalendar::CalReg reg,
      F* trendS, F* volS1, F* volS2, F* trendV, F* volV, F* corrSV, F* IR
    )
    const = 0;

    // (2) ALL-POINTS-TOGETHER:
    // As above, but for all "Sx" and "Vx" values simultaneously. The input and
    // output arrays must be of the following lengths:
    // len(Sx)     = m
    // len(Vx)     = n
    // len(TrendS) = m
    // len(VolS1)  = m
    // len(VolS2)  = n
    // len(TrendV) = n
    // len(VolV)   = n
    // len(corrSV) = 1 (scalar)
    // len(IR)     = 1 (if not a Rate model) or m (for a Rate model):
    //
    // (2a): Array Form:
    //
    virtual void GetSDECoeffs
    (
      int m,     F const* Sx, int n,    F const* Vx, F ty,
      TradingCalendar::CalReg reg,
      F* trendS, F* volS1,    F* volS2, F* trendV,   F* volV, F* corrSV, F* IR
    )
    const = 0;

    // (2b): Vector Form. NB: output args are still arrays, not vectors -- this
    // is for maximum efficiency:
    //
    void GetSDECoeffs
    (
      Vector<F> const& Sx,     Vector<F> const& Vx,       F ty,
      TradingCalendar::CalReg  reg,
      F* trendS, F* volS1,  F* volS2, F* trendV, F* volV, F* corrSV, F* IR
    )
    const
    {
      GetSDECoeffs(int(Sx.size()),  &(Sx[0]), int(Vx.size()), &(Vx[0]), ty,
                   reg, trendS, volS1, volS2, trendV,   volV, corrSV,   IR);
    }

    //-----------------------------------------------------------------------//
    // "TransPDF":                                                           //
    //-----------------------------------------------------------------------//
    // Density Function for Transition Probabilities: analytical or otherwise
    // (e.g. based on numerical Heat Kernel computations).
    // NB: the whole interval [ty1..ty2] must have same "reg";
    // (S1, V1): initial condition at "ty1" (expressed in Years since "t0"):
    // (S2, V2): curr point at (ty2 = ty1 + dty) ("dty" is a Year Fraction),
    //           for which the joint PDF is returned:
    //
    virtual F TransPDF
      (F S1, F V1, F ty1, F S2, F V2, F dty, TradingCalendar::CalReg reg)
    const = 0;

    //-----------------------------------------------------------------------//
    // "MCPaths":                                                            //
    //-----------------------------------------------------------------------//
    // (*) All data are initially in Host memory; if CUDA is used, they are
    //     copied to/from CUDA memory;
    // (*) sizes of vector / matrix args must be consistent (checked);
    // (*) if futher in-CUDA processing of the generated paths is required (as
    //     opposed to returning them into the Host space), set "S", "V" and/or
    //     "IR" to NULL but make "cudaTimeLine" and/or ... non-NULL;  the corr
    //     CUDA ptrs will be returned via those params;
    // (*) if CUDA is to be used, the corresp "CUDAEnv" is taken from "RNG";
    // (*) "useAntithetic" should noramlly be used for MC pricing but not for
    //     PDF construction;
    // (*) as in the 1D case, only the Euler-Maruyama scheme is used, NOT the
    //     Milstein one:
    //
    virtual void MCPaths
    (
      std::vector<TimeNode<F>> const& timeLine, // size =  L
      int                             P,        // Number of Paths
      RNG<F> const&                   rng,      // Incl CUDAEnv
      Matrix<F>*                      S,        // size =  (L,  P)
      Matrix<F>*                      V,        // size =  (L,  P)
      Matrix<F>*                      IR,       // NULL or (L,  P)
      // Output CUDA ptrs (set to NULL if provided but CUDA is not used):
      bool                            useAntithetic = false,
      TimeNode<F>**                   cudaTimeLine  = nullptr,
      F**                             cudaS         = nullptr,
      F**                             cudaV         = nullptr,
      F**                             cudaIR        = nullptr
    )
    const = 0;

# endif // !__CUDACC__
  };
}
