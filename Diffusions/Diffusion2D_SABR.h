// vim:ts=2:et
//===========================================================================//
//                            "Diffusion2D_SABR.h":                          //
//   SABR StocLocalVol Diffusion (with CEV, Mean Reversion and Vol Trend):   //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Diffusions/Diffusion2D.h"
#include <stdexcept>

namespace Arbalete
{
  //=========================================================================//
  // "Diffusion2D_SABR":                                                     //
  //=========================================================================//
  // Trading-Time Dynamics, non-IR model:
  //
  // dS    = mu*S * dt                        + xi  * S^beta * V * dW
  // dV    = (a + kappa * (a*t + b - V)) * dt + eps * V          * dZ
  // dW*dZ = rho  * dt
  //
  // NB: a=0 and b=0 (no mean-reversion or vol trend) and xi=1  ==> classical
  // SABR model.
  //
  // Mode-switching is provided between Trading Hours (TH) and Non-Trading Hrs
  // NT=(ON, WEH): xi=1 for TH, xi < 1 for NT; "eps" has 2 values as well:
  //
  template<typename F>
  class Diffusion2D_SABR final: public Diffusion2D<F>
  {
  public:
    //-----------------------------------------------------------------------//
    // Method used in computation of "TransPDF":                             //
    //-----------------------------------------------------------------------//
    enum TransPDF_Method
    {
      Generic    = 0,
      HaganEtAl  = 1,
      Paulot0    = 2,  // (Without the "a1" coeff)
      Paulot1    = 3   // (With an approximated "a1")
    };

    //=======================================================================//
    // "DataHolder" (non-virtual, suitable for CUDA):                        //
    //=======================================================================//
    class DataHolder final: public Diffusion2D<F>::DataHolder
    {
    private:
      F     m_mu;     // SABR (with mean-reversion) model coeffs
      F     m_xiN;    // Scaling (down) factor for vol out-of-trading-hours
      F     m_beta;
      F     m_kappa;
      F     m_a;      // theta = a * ty + b
      F     m_b;
      F     m_epsTH;  // Vol-of-Vol, trading hours
      F     m_epsN;   // Vol-of-Vol, out-of-trading-hours
      F     m_rho;
      F     m_ir;

      TransPDF_Method m_method;

      // Default Ctor is hidden:
      DataHolder();

    public:
      //---------------------------------------------------------------------//
      // Non-Default Ctor (not for CUDA):                                    //
      //---------------------------------------------------------------------//
      // params.m_coeffs (11):
      // [ V0; mu, xiN, beta, kappa, a, b, epsTH, etaN, rho, ir ]
      // where
      // etaN = epsN / epsTH:
      //
#     ifndef __CUDACC__
      DataHolder
      (
        Diffusion_Params<F> const& params,
        TransPDF_Method method = TransPDF_Method::Generic
      );
#     endif

      // Copy Ctor and Dtor are auto-generated

      //---------------------------------------------------------------------//
      // SDE Coeffs (Individial):                                            //
      //---------------------------------------------------------------------//
      // "TrendS":
      DEVICE  F TrendS
        (F S, F UNUSED_PARAM(ty), TradingCalendar::CalReg UNUSED_PARAM(reg))
      const
        { return m_mu * S; }

      // "VolS1": Local Vol, Regime-Dependent:
      DEVICE F VolS1 (F S, F UNUSED_PARAM(ty), TradingCalendar::CalReg reg)
      const
      {
        F lv = (m_beta == F(1.0)) ? S : Pow(S, m_beta);
        return (!this->m_withReg || reg == TradingCalendar::TradingHours)
                ? lv
                : (lv * m_xiN);
      }

      // "VolS2": StocVol:
      DEVICE F VolS2
        (F V, F UNUSED_PARAM(ty), TradingCalendar::CalReg UNUSED_PARAM(reg))
      const
        { return V;  }

      // "TrendV": Mean-Reversion of Vol:
      DEVICE F TrendV(F V, F ty, TradingCalendar::CalReg UNUSED_PARAM(reg))
      const
        { return m_a + m_kappa * (m_a * ty + m_b - V); }

      // "VolV": Vol-of-Vol, Regime-Dependent:
      DEVICE F VolV(F V, F UNUSED_PARAM(ty), TradingCalendar::CalReg reg) const
      {
        return ((!this->m_withReg  || reg == TradingCalendar::TradingHours)
                ? m_epsTH : m_epsN)
                * V;
      }

      // "CorrSV":
      DEVICE F CorrSV
        (F UNUSED_PARAM(ty), TradingCalendar::CalReg UNUSED_PARAM(reg)) const
        { return m_rho;  }

      // "IR":
      DEVICE F IR
        (F UNUSED_PARAM(S),    F UNUSED_PARAM(ty),
         TradingCalendar::CalReg UNUSED_PARAM(reg)) const
        { return m_ir;   }

      //---------------------------------------------------------------------//
      // Properties:                                                         //
      //---------------------------------------------------------------------//
      DEVICE bool IsIRModel()    const    { return false;  }
      DEVICE bool IsPositiveS()  const    { return true;   }
      DEVICE bool IsPositiveV()  const    { return true;   }

      //---------------------------------------------------------------------//
      // "GetSDECoeffs":                                                     //
      //---------------------------------------------------------------------//
      // NB: Do NOT auto-generate "TransPDF": it is provided explicitly below:
      //
#     define  USE_TRANSPDF_SPECIAL 1
#     include "Diffusions/Diffusion2D_BoilerPlate1.h"
#     undef   USE_TRANSPDF_SPECIAL

      //---------------------------------------------------------------------//
      // "TransPDF" Switch:                                                  //
      //---------------------------------------------------------------------//
      DEVICE F TransPDF
        (F S1, F V1, F ty1, F S2, F V2, F dty, TradingCalendar::CalReg reg)
      const;

    private:
      //---------------------------------------------------------------------//
      // "TransPDF*": Specialised Implementations via Heat Kernel Expansion: //
      //---------------------------------------------------------------------//
      // P.Hagan, A.Lesniewski, D.Woodward (2005) implementation:
      // Only works when there is no mean-reversion in "V", and either beta=1
      // or mu=0:
      //
      DEVICE F TransPDF_HaganEtAl
        (F S1, F V1, F ty1, F S2, F V2, F dty, TradingCalendar::CalReg reg)
      const;

      // L.Paulot (2009) implementation based on the general Heat Kernel Expan-
      // sion:
      //
      DEVICE F TransPDF_Paulot
      (
        F S1, F V1, F ty1, F S2, F V2, F dty, TradingCalendar::CalReg reg,
        bool with_a1
      )
      const;
    };
    // End of "DataHolder"

# ifndef __CUDACC__
    //=======================================================================//
    // "Diffusion2D_SABR" itself (virtual, not for CUDA):                    //
    //=======================================================================//
  private:
    DataHolder  m_dh;   // All data are stored here

    // Default Ctor is hidden:
    Diffusion2D_SABR();

  public:
    //-----------------------------------------------------------------------//
    // Ctors and Accessors:                                                  //
    //-----------------------------------------------------------------------//
    // Non-Default Ctor:
    //
    Diffusion2D_SABR
    (
      Diffusion_Params<F> const& params,
      TransPDF_Method method = TransPDF_Method::Generic
    )
    : Diffusion2D<F>(params.m_t0),
      m_dh          (params, method)
    {}

    // Copy Ctor and Dtor are auto-generated

    // Data Export (without "t0"!):
    DataHolder const& GetData() const  { return m_dh; }

    //-----------------------------------------------------------------------//
    // Implementation of abstract methods from the base class:               //
    //-----------------------------------------------------------------------//
#   include "Diffusions/Diffusion2D_BoilerPlate2.h"

# endif // !__CUDACC__
  };
}
