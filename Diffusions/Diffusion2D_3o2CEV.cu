// vim:ts=2:et
//===========================================================================//
//                        "Diffusion2D_3o2CEV.cu":                           //
//     CUDA Kernels for Monte Carlo Simulations of Diffusion2D_3o2CEV:       //
//===========================================================================//
#include "Diffusions/Diffusion2D_3o2CEV.h"
#include "Diffusions/Diffusion2D.hpp"
#include "MonteCarlo/RNG.hpp"

namespace Arbalete
{
  //-------------------------------------------------------------------------//
  // Instances of "MCPathsGen2D_KernelInvocator" with "3o2CEV::DataHolder":  //
  //-------------------------------------------------------------------------//
  template struct MCPathsGen2D_KernelInvocator
    <double, Diffusion2D_3o2CEV<double>::DataHolder>;

  template struct MCPathsGen2D_KernelInvocator
    <float,  Diffusion2D_3o2CEV<float>::DataHolder>;
}
