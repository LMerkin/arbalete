// vim:ts=2:et
//===========================================================================//
//                          "Diffusion1D_Fact.hpp":                          //
//                      Factory of Various 1D Diffusions                     //
//===========================================================================//
#pragma once

#include "Diffusions/Diffusion1D_Fact.h"
#include "Diffusions/Diffusion1D_All.hpp"
#include <stdexcept>
#include <cassert>

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "MkDiffusion1D":                                                        //
  //=========================================================================//
  template<typename F>
  Diffusion1D<F>* MkDiffusion1D(Diffusion_Params<F> const& params)
  {
    //-----------------------------------------------------------------------//
    // Geometric Brownian Motion (GBM):                                      //
    //-----------------------------------------------------------------------//
    if (params.m_type == "GBM")
      return new Diffusion1D_GBM<F>   (params);
    else
    //-----------------------------------------------------------------------//
    // Ornstein-Uhlenbeck (OU):                                              //
    //-----------------------------------------------------------------------//
    if (params.m_type == "OU")
      return new Diffusion1D_OU<F>(params);
    else
    //-----------------------------------------------------------------------//
    // Anything else:                                                        //
    //-----------------------------------------------------------------------//
    {
      assert(!IsDiffusion1D<F>(params));
      throw invalid_argument
            ("MkDiffusion1D: Invalid Diffusion type: "+ params.m_type);
    }
  }

  //=========================================================================//
  // "IsDiffusion1D":                                                        //
  //=========================================================================//
  template<typename F>
  bool IsDiffusion1D(Diffusion_Params<F> const& params)
  {
    return (params.m_type == "GB" || params.m_type == "OU");
  }
}
