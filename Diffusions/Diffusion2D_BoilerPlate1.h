// vim:ts=2:et
//===========================================================================//
//                       "Diffusion2D_BoilerPlate1.h":                       //
//               BoilerPlate code for all 2D Diffusions, Part 1              //
//===========================================================================//
// Implementation of "GetSDECoeffs" and "TransPDF" methods  for a "DataHolder"
// class via templates declared in the base class  (and accessors provided in
// this derived class):
//
// Enclosing Syntactic Context:
// namespace Arbalete
// class Diffusion2D_XXX
// class DataHolder

      //---------------------------------------------------------------------//
      // "GetSDECoeffs", Single-Point:                                       //
      //---------------------------------------------------------------------//
      DEVICE void GetSDECoeffs
      (
        F S,  F V, F  ty,    TradingCalendar::CalReg reg,
        F* trendS, F* volS1, F* volS2, F* trendV, F* volV, F* corrSV, F* IR
      )
      const
      {
        Diffusion2D<F>::DataHolder::template GetSDECoeffsGen1<DataHolder>
        (*this, S, V, ty, reg, trendS, volS1, volS2, trendV, volV, corrSV, IR);
      }

      //---------------------------------------------------------------------//
      // "GetSDECoeffs", All-Points-Together:                                //
      //---------------------------------------------------------------------//
      DEVICE void GetSDECoeffs
      (
        int m, F const* Sx,  int n, F const* Vx,  F  ty,
        TradingCalendar::CalReg reg,
        F* trendS, F* volS1, F* volS2, F* trendV, F* volV, F* corrSV, F* IR
      )
      const
      {
        Diffusion2D<F>::DataHolder::template GetSDECoeffsGenM<DataHolder>
          (*this, m, Sx, n, Vx, ty, reg, trendS, volS1, volS2, trendV, volV,
           corrSV, IR);
      }

      //---------------------------------------------------------------------//
      // "TransPDF":                                                         //
      //---------------------------------------------------------------------//
#     ifndef USE_TRANSPDF_SPECIAL
      DEVICE F TransPDF
        (F S1, F V1, F ty1, F S2, F V2, F dty, TradingCalendar::CalReg reg)
      const
      {
        return
          Diffusion2D<F>::DataHolder::template TransPDFGen<DataHolder>
          (*this, S1, V1, ty1, S2, V2, dty, reg);
      }
#     endif

      // NB: No Monte Carlo functions here (in "DataHolder") -- if necessary,
      // use the corresp instance of "MCPathsGen2D_Core" directly!

