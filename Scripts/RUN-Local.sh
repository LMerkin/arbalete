#! /bin/bash

../CalibrTest \
	--float \
	--CBpSM=1 \
	--CBSz=512 \
	--sigma0=0.45 \
	--diffusion=SABR \
	--eps0=0.5 \
	--minRelV=0.5 \
	--md=../../../Data/SX5E/SX5E-short.dat \
	--debug=2 \
|& tee Calibr-1.log

