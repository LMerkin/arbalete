#! /bin/bash -e
# vim:ts=2:et

exec ../PDFTest \
  --diffusion=SABR-Paulot1 \
  --method=Chapman-Kolmogorov \
  --dt=30m \
  --V0=0.3 \
  --xiN=0.5 \
  --mu=-0.2 \
  --beta=1.0 \
  --a=0.0 \
  --b=0.2 \
  --kappa=1.0 \
  --epsTH=1.0 \
  --etaN=0.5 \
  --rho=-0.9 \
  --IR=0 \
  --sts=0 \
  --t0='2012-02-24 16:30:00'  \
  --m=1001 \
  --n=1001 \
  --NSigmasS=5.0 \
  --NSigmasV=5.0 \
  --S0OnGrid=true \
  --V0OnGrid=true \
  --minRelS=0.1 \
  --minRelV=0.1 \
  --maxRelS=100.0 \
  --maxRelV=100.0 \
  --double \
  --debug=4 \
  --CDevID=0 \
  --CBpSM=1 \
  --CBSz=320

