#! /bin/bash

# Mkt Params:
Sigma="120%"
TauMin=30
S0=5300

# Option Params:
Tenor="3m"
LoanAmt=3000
LiqThresh=100
K2=10000

# Files:
Prefix="$Tenor"-"$Sigma"
StatsFile="$Prefix".stats.dat
PathFile="$Prefix".path.dat

# XXX: We need to create explicit TimeLine depending on the Tenor:
t0="2020-03-18 00:00:00"
case $Tenor in
  "3m")  T="2020-06-18 00:00:00";;
  "6m")  T="2020-09-18 00:00:00";;
  "9m")  T="2020-12-18 00:00:00";;
  "12m") T="2021-03-18 00:00:00";;
esac

# Run the simulations:
PayOffRepl \
  --OptionT=ZeroCostLoan \
  --S0="$S0" \
  --sigmaAct="$Sigma" \
  --sigmaMod="$Sigma" \
  --tauMin="$TauMin" \
  --LoanAmt="$LoanAmt" \
  --LiqThresh="$LiqThresh" \
  --K2="$K2" \
  --t0="$t0" \
  --T="$T"   \
  --statsFile="$Prefix".stats.dat \
  --pathFile="$Prefix".path.dat

# PayOff:
gnuplot <<EOF
set title "Vol=$Sigma, S0=\$$S0, LoanAmt=\$$LoanAmt, LiqThresh=\$$LiqThresh, Tenor=$Tenor"
set xlabel "S_T, \$"
set ylabel "PayOff, \$"
set terminal png
set output "$Prefix-PayOff.png"
plot "$StatsFile" using 1:2 with dots
EOF

# PnL:
gnuplot <<EOF
set title "Vol=$Sigma, S0=\$$S0, LoanAmt=\$$LoanAmt, LiqThresh=\$$LiqThresh, Tenor=$Tenor"
set xlabel "S_T, \$"
set ylabel "PnL, \$"
set terminal png
set output "$Prefix-PnL.png"
plot "$StatsFile" using 1:3 with dots
EOF

# MinCash:
gnuplot <<EOF
set title "Vol=$Sigma, S0=\$$S0, LoanAmt=\$$LoanAmt, LiqThresh=\$$LiqThresh, Tenor=$Tenor"
set xlabel "S_T, \$"
set ylabel "MinCash, \$"
set terminal png
set output "$Prefix-MinCash.png"
plot "$StatsFile" using 1:4 with dots
EOF

# AvgBorrowing:
gnuplot <<EOF
set title "Vol=$Sigma, S0=\$$S0, LoanAmt=\$$LoanAmt, LiqThresh=\$$LiqThresh, Tenor=$Tenor"
set xlabel "S_T, \$"
set ylabel "AvgBorrowing, \$"
set terminal png
set output "$Prefix-AvgBorrowing.png"
plot "$StatsFile" using 1:5 with dots
EOF

# AvgBorrowing:
gnuplot <<EOF
set title "Vol=$Sigma, S0=\$$S0, LoanAmt=\$$LoanAmt, LiqThresh=\$$LiqThresh, Tenor=$Tenor"
set xlabel "S_T, \$"
set ylabel "Return, %/month"
set terminal png
set output "$Prefix-MonthlyRet.png"
plot "$StatsFile" using 1:6 with dots
EOF
