// vim:ts=2:et
//===========================================================================//
//                    "Grid2D_ChapmanKolmogorov_Core.hpp":                   //
//    Grid for Iterative PDF Computation via Chapman-Kolmogorov Equation     //
//===========================================================================//
#pragma once

#include "Common/TradingCalendar.h"
#include "Common/CUDAEnv.hpp"
#include <cassert>

namespace Arbalete
{
  //=========================================================================//
  // "ChapmanKolmogorov2D_Core":                                             //
  //=========================================================================//
  // NB: "diff" must be passed by copy (for Host->CUDA calls).
  // NB: "static" is essential here -- otherwise Host code could get called in-
  // stead of CUDA code!
  // This function computes an integral of the density "p0" with the transition
  // density "diff.TransPDF()" to get the new density "p":
  //
  template<typename F, typename Diff>
  GLOBAL static void ChapmanKolmogorov2D_Core
  (
    Diff diff,     CUDAEnv::Sched1D const* CUDACC_OR_DEBUG(sched),
    int step_k,
    int m,         F const* Sx,     F S0,
    int n,         F const* Vx,     F V0,
    F ty0,         F dty,           TradingCalendar::CalReg reg,
    F const* p0,   F*  p
  )
  {
    //-----------------------------------------------------------------------//
    // "k": 2D index running over the result ("p"):                          //
    //-----------------------------------------------------------------------//
    assert(m >= 2    && n   >= 2 && Sx != nullptr && Vx != nullptr && p0 != nullptr &&
           p != nullptr && dty >  F(0.0));

    int K   = m * n;
    F   hS  = Sx[1] - Sx[0];
    F   hV  = Vx[1] - Vx[0];
    F   hSV = hS * hV;
    assert(hSV > F(0.0));

#   ifdef __CUDACC__
    // The order number of this thread (across all blocks):
    int Tn  = blockDim.x * blockIdx.x + threadIdx.x;
    assert(sched != nullptr);

    int kFrom = sched[Tn].m_from;
    int kTo   = sched[Tn].m_to;
    if (kFrom > kTo)
      // There is no post-processing to do, so this thread may exit:
      return;

    assert(0 <= kFrom && kFrom <= kTo && kTo <= K-1);
#   else
    // Host-based computations: use OpenMP (XXX: not on Mac OS X yet):
    assert(sched == nullptr);
    int kFrom = 0;
    int kTo   = K-1;

#   ifndef __clang__
#   pragma omp parallel for
#   endif
#   endif  // __CUDACC__
    for (int k = kFrom; k <= kTo; ++k)
    //-----------------------------------------------------------------------//
    // Target Loop (for "p"):                                                //
    //-----------------------------------------------------------------------//
    {
      //  k = m * j + i
      int i = k % m;
      int j = k / m;

      // Target co-ords:
      F   S = Sx[i];
      F   V = Vx[j];

      // If "p0" is A PRIORY known to be delta-function which corresponds to
      // the Diffusion's initial condition,  full integration loop is not re-
      // quired:
      F res = F(0.0);

      if (step_k == 0)
        // NB: This is an "analytical" integral over the initial cond which is
        // delta-function -- no multiplication by "hSV" here:
        //
        res = diff.TransPDF(S0, V0, ty0, S, V, dty, reg);
      else
      {
        assert(step_k >= 1);
        //-------------------------------------------------------------------//
        // Src Loop (over "p0"):                                             //
        //-------------------------------------------------------------------//
        for (int l = 0; l < K; ++l)
        {
          //  l = m * s + r
          int r = l % m;
          int s = l / m;

          // "0" indices are not used in integration:
          if (r == 0 || s == 0)
            continue;

          F S0 = Sx[r];
          F V0 = Vx[s];

          // Compute the Transitional Density:
          F trans = diff.TransPDF(S0, V0, ty0, S, V, dty, reg);

          // Update the result:
          res  += trans * p0[l];
        }
        res *= hSV;
      }
      // Save the result:
      p[k] = res;
    }
    // All done!
  }

  //=========================================================================//
  // "ChapmanKolmogorov2D_KernelInvocator":                                  //
  //=========================================================================//
  // NB: This is a Host-invoked function, so it's OK to pass "diff" by ref:
  //
  template<typename F, typename Diff>
  struct ChapmanKolmogorov2D_KernelInvocator
  {
    static void Run
    (
      Diff     const& diff,
      CUDAEnv  const& cudaEnv, CUDAEnv::Sched1D const* sched,
      int      step_k,
      int m,   F const* Sx,    F S0,
      int n,   F const* Vx,    F V0,
      F ty0,   F dty,          TradingCalendar::CalReg reg,
      F const* p0,             F* p
    )
#   ifdef __CUDACC__
    {
      // Set the CUDA device:
      CUDAEnv::Lock();
      cudaEnv.SetCUDADevice();

      // Invoke the kernel:
      ChapmanKolmogorov2D_Core<F, Diff>
        <<<cudaEnv.NThreadBlocks(), cudaEnv.ThreadBlockSize(), 0,
           cudaEnv.Stream()
        >>>
        (diff, sched, step_k, m, Sx, S0, n, Vx, V0, ty0, dty, reg, p0, p);

      CUDAEnv::Unlock();
      CUDA_CHECK_STATUS("Chapman-Kolmogorov Kernel invocation failed")
    }
#   else
    ; // Spec only
#   endif
  };
}
