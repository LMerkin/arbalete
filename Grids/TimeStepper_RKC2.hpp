// vim:ts=2:et
//===========================================================================//
//                           "TimeStepper_RKC2.h":                           //
//          2nd-Order Runge-Kutta-Chebyshev Time Propagation Method          //
//===========================================================================//
#pragma once

#include "Common/CUDAEnv.hpp"
#include "Common/Chebyshev.h"
#include "Grids/TimeStepper_RKC2.h"
#include "Grids/TimeStepper_RKC2_Core.hpp"

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // Non-Default Ctor:                                                       //
  //=========================================================================//
  template<typename F>
  TimeStepper_RKC2<F>::TimeStepper_RKC2
  (
    int K, int s, shared_ptr<CUDAEnv> const& cudaEnv, F eps
  )
  : TimeStepper<F>(K, cudaEnv),
    m_s(s)
  {
    if (s < 2 || eps < F(0.0))
      throw invalid_argument("TimeStepper_RKC2 Ctor");

    // Pre-compute Chebyshev polynomials and their derivatives. NB: the "per-
    // missive" mode is used -- Chebyshev args may be slightly beyond +-1:
    //
    F w0 = F(1.0) + eps / F(s * s);

    Vector<F>   ChebT(s + 1);
    Vector<F>  ChebDT(s + 1);
    Vector<F> ChebDDT(s + 1);

    Chebyshev<F>::Ts  (s, w0, &(ChebT  [0]), true);
    Chebyshev<F>::DTs (s, w0, &(ChebDT [0]), true); // DTs [0] = 0
    Chebyshev<F>::DDTs(s, w0, &(ChebDDT[0]), true); // DDTs[0] = DDTs[1] = 0

#   ifndef NDEBUG
    for (int i = 0; i <= s; ++i)
      assert(ChebT[i] != F(0.0)              &&
            (i <= 1 || ChebDT [i] != F(0.0)) &&
            (i <= 2 || ChebDDT[i] != F(0.0)));
#   endif

    F w1 = ChebDT[s] / ChebDDT[s];

    // Fill in "b":
    Vector<F> b(s + 1);
    for (int i = 2; i <= s; ++i)
    {
      b[i] = ChebDDT[i] / ChebDT[i] / ChebDT[i];
      assert(IsFinite(b[i]) && b[i] != F(0.0));
    }
    b[1] = b[2];
    b[0] = b[2];

    // Fill in "m_c":
    //
    m_c.resize(s + 1);
    m_c[0] = F(0.0);
    m_c[s] = F(1.0);
    for (int i = 2; i <= s-1; ++i)
      m_c[i] =   w1 * ChebDDT[i] / ChebDT[i];
    m_c[1] = m_c[2] / ChebDT[2];

    // Fill in "m_u":
    //
    m_u.resize(s);
    m_u[0] = b[1]   * w1;
    F cu = F(2.0) * w1;
    for (int i = 1; i <= s-1; ++i)
      m_u[i] = cu * b[i+1] / b[i];

    // Fill in "m_gamma", "m_mu", "m_nu":
    //
    m_gamma.resize(s - 1);
    m_mu   .resize(s - 1);
    m_nu   .resize(s - 1);
    F cm = F(2.0) * w0;
    for (int i = 0; i <= s-2; ++i)
    {
      m_mu[i]    = cm * b[i+2] / b[i+1];
      m_nu[i]    =   -  b[i+2] / b[i];
      m_gamma[i] =   - (F(1.0) - b[i+1] * ChebT[i+1]) * m_u[i+1];
    }

    // Allocate scratch matrices:
    //
    if (!this->UseCUDA())
    {
      // Host allocations:
      m_Y[0]    = new F[this->m_K];
      m_Y[1]    = new F[this->m_K];
      m_F0      = new F[this->m_K];
      m_F1      = new F[this->m_K];
    }
    else
    {
      // CUDA allocations:
      m_Y[0]    = this->m_cudaEnv->template CUDAAlloc<F>(this->m_K);
      m_Y[1]    = this->m_cudaEnv->template CUDAAlloc<F>(this->m_K);
      m_F0      = this->m_cudaEnv->template CUDAAlloc<F>(this->m_K);
      m_F1      = this->m_cudaEnv->template CUDAAlloc<F>(this->m_K);
    }
    // All done!
  }

  //=========================================================================//
  // Dtor:                                                                   //
  //=========================================================================//
  template<typename F>
  TimeStepper_RKC2<F>::~TimeStepper_RKC2()
  {
    if (!this->UseCUDA())
    {
      delete[] m_Y[0];
      delete[] m_Y[1];
      delete[] m_F0;
      delete[] m_F1;
    }
    else
    {
      this->m_cudaEnv->Stop();  // Just in case...
      this->m_cudaEnv->template CUDAFree<F>(m_Y[0]);
      this->m_cudaEnv->template CUDAFree<F>(m_Y[1]);
      this->m_cudaEnv->template CUDAFree<F>(m_F0);
      this->m_cudaEnv->template CUDAFree<F>(m_F1);
    }
  }

  //=========================================================================//
  // "Propagate":                                                            //
  //=========================================================================//
  // NB: In the CUDA mode, "p" is assumed to be a CUDA memory ptr (as well as
  // all other data ptrs):
  //
  template<typename F>
  void TimeStepper_RKC2<F>::Propagate
  (
    DateTime                t_from,
    TradingCalendar::CalReg reg,
    F*                      p,
    DateTime                t_to,
    typename TimeStepper<F>::RHS const& rhs
  )
  const
  {
    assert(p != nullptr);
    F tau = YF<F>(t_from, t_to);  // NB: "tau" may be negative, but not 0:
    assert(tau != F(0.0));

    //-----------------------------------------------------------------------//
    // Initial "DpDt" from the RHS function:                                 //
    //-----------------------------------------------------------------------//
    rhs(t_from, reg, p, m_F0);

    // Fill in "m_Y[0]"; NB: "SubStepRKC2_Core" is invoked in the "Init" mode:
    F cF0 = m_u[0] * tau;

    // NB: Do both static and dynamic checks for CUDA availability, to avoid
    // link errors:
    //
    if (!this->UseCUDA())
      SubStepRKC2_Core<F,true>
      (
        this->m_K,     nullptr,
        p,     m_F0,   nullptr,   nullptr,   nullptr, m_Y[0],
        F(1.0), cF0, F(0.0), F(0.0), F(0.0)
      );
    else
    {
      this->m_cudaEnv->Start();

      RKC2_KernelInvocator<F,true>::Run
      (
        *(this->m_cudaEnv),   this->m_K,    this->m_cudaSch,
        p,      m_F0,   nullptr, nullptr,   nullptr, m_Y[0],
        F(1.0),  cF0, F(0.0), F(0.0), F(0.0)
      );

      this->m_cudaEnv->Stop();
    }
    //-----------------------------------------------------------------------//
    // Do all intermediate points:                                           //
    //-----------------------------------------------------------------------//
    for (int i = 1; i <= m_s-1; ++i)
    {
      F*       Yi   = (i == m_s-1) ? p : m_Y[i % 2];  // out
      F*       Yim1 = m_Y[(i-1) % 2];                 // in, BCs can be appld
      F const* Yim2 = (i == 1)     ? p : m_Y[i % 2];  // in, act. [(i-2)%2]

      // Compute "DpDt" at the intermediate time instant:
      //
      DateTime ti = t_from + TimeOfYF<F>(m_c[i] * tau);
      rhs(ti, reg, Yim1, m_F1);

      // Make the sub-step (NB: NOT the "Init" mode):
      //
      F cP    = F(1.0) - m_mu[i-1] - m_nu[i-1];
      F cF0   = m_gamma[i-1] * tau;
      F cF1   = m_u [i]      * tau;
      F cYim2 = m_nu[i-1];
      F cYim1 = m_mu[i-1];

      // NB: Do both static and dynamic checks for CUDA availability, to avoid
      // link errors:
      //
      if (!this->UseCUDA())
        SubStepRKC2_Core<F,false>
        (
          this->m_K, nullptr,
          p, m_F0, m_F1,  Yim2,  Yim1, Yi,
          cP, cF0,  cF1, cYim2, cYim1
        );
      else
      {
        this->m_cudaEnv->Start();

        RKC2_KernelInvocator<F,false>::Run
        (
          *(this->m_cudaEnv),     this->m_K,  this->m_cudaSch,
          p,  m_F0, m_F1,  Yim2,  Yim1, Yi,
          cP,  cF0,  cF1, cYim2, cYim1
        );

        this->m_cudaEnv->Stop();
      }
    }
  }
}
