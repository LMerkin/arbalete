// vim:ts=2:et
//===========================================================================//
//                               "Grid2D_ADI.h":                             //
//           Fractional Step Method for 2D PDEs (PR-ADI, Yanenko)            //
//===========================================================================//
#pragma once

#include "Grids/Grid2D.h"
#include "Grids/DataADI.h"
#include <string>
#include <memory>

namespace Arbalete
{
  //=========================================================================//
  // "Grid2D_ADI":                                                           //
  //=========================================================================//
  // M = StencilSize / 2:
  // M = 1 for 3-Point Stencil;
  // M = 2 for 5-Point Stencil:
  //
  template<typename F, int M>
  class Grid2D_ADI final: public Grid2D<F>
  {
  private:
    //-----------------------------------------------------------------------//
    // Grid Params and Tmp Data:                                             //
    //-----------------------------------------------------------------------//
    bool                  m_isPRADI;  // Scheme: PR-ADI (if on) or Yanenko

    mutable DataADI<F,M>  m_HD;       // Host Data (scratch)
    mutable DataADI<F,M>  m_CD;       // CUDA Data (scratch)
    mutable TradingCalendar::CalReg m_regE;   // Previous regime

    // Scratch arrays for solving STS-diagonal linear systems. For parallelism,
    // maintain multiple copies of these arrays:
    //
    mutable F*            m_diags;    // Diags of implicit systems (Host|CUDA)
    mutable F*            m_rhss ;    // RHSs of those systems   (Host|CUDA)
    mutable F*            m_cudaF0;   // Solution at whole steps (CUDA only)
    mutable F*            m_fh;       // Solution at half-steps  (Host|CUDA)
    CUDAEnv::Sched2D*     m_cudaSch0; // Schedule for CUDA threads ( ImplS)
    CUDAEnv::Sched2D*     m_cudaSch1; // Schedule for CUDA threads (!ImplS)

    // Default Ctor is hidden:
    Grid2D_ADI();

    //-----------------------------------------------------------------------//
    // "InitInduct":                                                         //
    //-----------------------------------------------------------------------//
    // Implementation of the corresp abstract method of "Grid2D":
    //
    void InitInduct
    (
      bool                        isFwd,
      typename Grid2D<F>::DRType  DR,
      DateTime                    t_from,
      BoundConds2D<F> const&      bcs
    )
    const override;

    //-----------------------------------------------------------------------//
    // "DoSubStepGen": A Generic Fractional Step:                            //
    //-----------------------------------------------------------------------//
    // Templating is just for efficiency (to enable compile-time optimisation):
    // (*) "IsFwd"   is "true" for Fokker-Planck, "false" for Feynman-Kac;
    // (*) "IsPRADI" is "true" if the scheme is PR-ADI, "false" for Yanenko;
    // (*) "ImplS"   is "true" if the Implicit variable is "S", "false for "V":
    //
    template<bool IsFwd, bool IsPRADI, bool ImplS>
    void DoSubStepGen
    (
      typename Grid2D<F>::DRType  DR,
      DateTime                    t_fcurr,
      F*                          p,
      DateTime                    t_fnext,
      F                           dty,
      TradingCalendar::CalReg     reg,
      BoundConds2D<F> const&      bcs
    )
    const;

    //-----------------------------------------------------------------------//
    // "DoOneStep":                                                          //
    //-----------------------------------------------------------------------//
    // Implementation of the corresp abstract method of "Grid2D":
    //
    F* DoOneStep
    (
      bool                        isFwd,
      typename Grid2D<F>::DRType  DR,
      int                         step_k,
      DateTime                    t_curr,
      F*                          p,
      DateTime                    t_next,
      TradingCalendar::CalReg     reg,
      BoundConds2D<F> const&      bcs
    )
    const override;

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor, Access and Dtor:                                    //
    //-----------------------------------------------------------------------//
    // Currently accepted values for "params.m_method":
    // (*) "PR-ADI"  (Peaceman - Rachford)
    // (*) "Yanenko"
    // Currently accepted values for "params.m_sts":
    // (*) 3 (spatial stencil is 3*3 points)
    // (*) 5 (spatial stencil is 5*5 points)
    //
    Grid2D_ADI(std::shared_ptr<Diffusion2D<F>> const& diff,
                        typename Grid2D<F>::Params      const& params);

    ~Grid2D_ADI();

    // Is it a Yanenko grid (or PR-ADI)?
    bool IsYanenko() const  { return !m_isPRADI; }
  };
}
