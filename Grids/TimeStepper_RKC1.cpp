// vim:ts=2:et
//===========================================================================//
//                          "TimeStepper_RKC1.cpp":                          //
//                 Explicit Instances of "TimeStepper_RKC1"                  //
//===========================================================================//
#include "Grids/TimeStepper_RKC1.hpp"

namespace Arbalete
{
  template class TimeStepper_RKC1<double>;
  template class TimeStepper_RKC1<float>;
}
