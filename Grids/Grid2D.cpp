// vim:ts=2:et
//===========================================================================//
//                               "Grid2D.cpp":                               //
//                Explicit Instances of the "Grid2D" Base Class              //
//===========================================================================//
#include "Grid2D.hpp"

namespace Arbalete
{
  template class Grid2D<double>;
  template class Grid2D<float>;
}
