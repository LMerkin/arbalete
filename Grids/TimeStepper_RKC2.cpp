// vim:ts=2:et
//===========================================================================//
//                          "TimeStepper_RKC2.cpp":                          //
//                 Explicit Instances of "TimeStepper_RKC2"                  //
//===========================================================================//
#include "Grids/TimeStepper_RKC2.hpp"

namespace Arbalete
{
  template class TimeStepper_RKC2<double>;
  template class TimeStepper_RKC2<float>;
}
