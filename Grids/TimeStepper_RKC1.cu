// vim:ts=2:et
//===========================================================================//
//                           "TimeStepper_RKC1.cu":                          //
//              CUDA Kernel Invocator for the RKC1 Time Stepper              //
//===========================================================================//
#include "Grids/TimeStepper_RKC1_Core.hpp"

namespace Arbalete
{
  //=========================================================================//
  // All Instances of the "RKC1_KernelInvocator":                            //
  //=========================================================================//
  template struct RKC1_KernelInvocator<double, false>;
  template struct RKC1_KernelInvocator<double, true>;

  template struct RKC1_KernelInvocator<float,  false>;
  template struct RKC1_KernelInvocator<float,  true>;
}
