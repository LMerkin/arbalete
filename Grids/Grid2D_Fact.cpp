// vim:ts=2:et
//===========================================================================//
//                              "Grid2D_Fact.cpp":                           //
//                    Pre-Defined Instances of "Grid2D_Fact"                 //
//===========================================================================//
#include "Grids/Grid2D_Fact.hpp"
#include "Grids/Grid2D.hpp"

namespace Arbalete
{
  //=========================================================================//
  // Prevent auto-instantiation of "Grid2D*" classes:                        //
  //=========================================================================//
  // They are explicitly instantiated in the resp ".cpp" files:
  //
  // NB: 1 = 3-point Stencil; 2 = 5-point stencil:
  //
  extern template class Grid2D<double>;
  extern template class Grid2D<float>;

  extern template class Grid2D_Expl <double, 1>;
  extern template class Grid2D_Expl <double, 2>;
  extern template class Grid2D_Expl <float,  1>;
  extern template class Grid2D_Expl <float,  2>;

  extern template class Grid2D_ADI  <double, 1>;
  extern template class Grid2D_ADI  <double, 2>;
  extern template class Grid2D_ADI  <float,  1>;
  extern template class Grid2D_ADI  <float,  2>;

  extern template class Grid2D_Theta<double>;
  extern template class Grid2D_Theta<float>;

  extern template class Grid2D_ChapmanKolmogorov<double>;
  extern template class Grid2D_ChapmanKolmogorov<float>;

  extern template class Grid2D_BayesOptFilt     <double>;
  extern template class Grid2D_BayesOptFilt     <float>;

  //=========================================================================//
  // Instantiate the Grid Factory:                                           //
  //=========================================================================//
  template Grid2D<double>* MkGrid2D<double>
  (
    shared_ptr<Diffusion2D<double>> const& diff,
    Grid2D<double>::Params          const& params
  );

  template Grid2D<float>*  MkGrid2D<float>
  (
    shared_ptr<Diffusion2D<float>>  const& diff,
    Grid2D<float>::Params           const& params
  );

  //=========================================================================//
  // "Quadrature2D" Template Methods:                                        //
  //=========================================================================//
  // Explicitly instantiate them with the "ProbMassKern" param here -- these
  // instances are used in "Grid2D" itself  and  in some of its sub-classes:
  //-------------------------------------------------------------------------//
  // Method 1:                                                               //
  //-------------------------------------------------------------------------//
  template void   Grid2D<double>::Quadrature2D<ProbMassKern<double>>
  (
    ProbMassKern<double> const& kern,
    double const*               f,
    int                         rsize,
    double*                     res
  )
  const;

  template void   Grid2D<float>::Quadrature2D<ProbMassKern<float>>
  (
    ProbMassKern<float> const&  kern,
    float const*                f,
    int                         rsize,
    float*                      res
  )
  const;

  //-------------------------------------------------------------------------//
  // Method 2:                                                               //
  //-------------------------------------------------------------------------//
  template double Grid2D<double>::Quadrature2D<ProbMassKern<double>>
  (
    ProbMassKern<double> const& kern,
    double const*               f
  )
  const;

  template float  Grid2D<float>::Quadrature2D<ProbMassKern<float>>
  (
    ProbMassKern<float> const&  kern,
    float const*                f
  )
  const;

  //-------------------------------------------------------------------------//
  // Method 3:                                                               //
  //-------------------------------------------------------------------------//
  template void   Grid2D<double>::Quadrature2D<ProbMassKern<double>>
  (
    ProbMassKern<double> const& kern,
    double const*               f,
    Vector<double>*             res
  )
  const;

  template void   Grid2D<float>::Quadrature2D<ProbMassKern<float>>
  (
    ProbMassKern<float> const&  kern,
    float const*                f,
    Vector<float>*              res
  )
  const;
}
