// vim:ts=2:et
//===========================================================================//
//                           "TimeStepper_Fact.hpp":                         //
//     Factory Producing Time Steppers for Explicit ODE / PDE Integration    //
//===========================================================================//
#pragma once

#include "Grids/TimeStepper_Fact.h"
#include "Grids/TimeStepper_RKC1.hpp"
#include "Grids/TimeStepper_RKC2.hpp"
#include "Grids/TimeStepper_RKF5.hpp"
#include <stdexcept>

namespace Arbalete
{
  using namespace std;

  template<typename F>
  TimeStepper<F>* MkTimeStepper
  (
    string const& stepperName,
    int  K,
    shared_ptr<CUDAEnv> const& cudaEnv
  )
  {
    if (stepperName.substr(0, 5) == "RKC1_")
    {
      //---------------------------------------------------------------------//
      // Runge-Kutta-Chebyshev propagator of order 1:                        //
      //---------------------------------------------------------------------//
      int s = atoi(stepperName.substr(5).c_str());
      if (s < 2)
        throw invalid_argument("MkTimeStepper: Invalid RKC1");
      return new TimeStepper_RKC1<F>(K, s, cudaEnv);
    }
    else
    if (stepperName.substr(0, 5) == "RKC2_")
    {
      //---------------------------------------------------------------------//
      // Runge-Kutta-Chebyshev propagator of order 2:                        //
      //---------------------------------------------------------------------//
      int s = atoi(stepperName.substr(5).c_str());
      if (s < 2)
        throw invalid_argument("MkTimeStepper: Invalid RKC2");
      return new TimeStepper_RKC2<F>(K, s, cudaEnv);
    }
    else
    if (stepperName == "RKF5")
      //---------------------------------------------------------------------//
      // Runge-Kutta-Fehlberg propagator of order 5:                         //
      //---------------------------------------------------------------------//
      return new TimeStepper_RKF5<F>(K, cudaEnv);
    else
    throw invalid_argument("MkTimeStepper: Unknown stepper: "+ stepperName);
  }
}
