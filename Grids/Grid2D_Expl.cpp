// vim:ts=2:et
//===========================================================================//
//                               "Grid2D_Expl.cpp":                          //
//                Explicit Instances of the "Grid2D_Expl" Class              //
//===========================================================================//
#include "Grid2D_Expl.hpp"

namespace Arbalete
{
  template class Grid2D_Expl<double, 1>;
  template class Grid2D_Expl<double, 2>;

  template class Grid2D_Expl<float,  1>;
  template class Grid2D_Expl<float,  2>;
}
