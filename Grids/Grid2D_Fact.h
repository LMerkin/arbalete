// vim:ts=2:et
//===========================================================================//
//                              "Grid2D_Fact.h":                             //
//                       Factory of Various 2D Grids                         //
//===========================================================================//
#pragma once

#include "Grids/Grid2D.h"

namespace Arbalete
{
  //=========================================================================//
  // "MkGrid2D":                                                             //
  //=========================================================================//
  // NB: The actual grid to be constructed is determined by the actual type and
  // contents of the "params".
  // Currently accepted method names:
  // (*) "PR-ADI"             (classical Peaceman-Rachford ADI)
  // (*) "Yanenko"            (fractional-step)
  // (*) "CN"                 (Cranck-Nicolson)
  // (*) "FullImpl"           (fully-implicit)
  // (*) "Chapman-Kolmogorov" (analytical)
  // (*) "BayesOptFilt"       (analytical)
  // (*) "RKC1_s"             (Runge-Kutta-Chebyshev, 1st ord, "s"-pt, s >= 2)
  // (*) "RKC2_s"             (Runge-Kutta-Chebyshev, 2nd ord, "s"-pt, s >= 2)
  // (*) "RKF5"               (Runge-Kutta-Fehlberg,  5th ord)
  //
  template<typename F>
  Grid2D<F>* MkGrid2D
  (
    std::shared_ptr<Diffusion2D<F>> const& diff,
    typename Grid2D<F>::Params      const& params
  );
}
