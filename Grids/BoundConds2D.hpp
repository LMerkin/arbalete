// vim:ts=2:et
//===========================================================================//
//                             "BoundConds2D.h":                             //
//                   Boundary Conditions for 2D PDE Grids                    //
//===========================================================================//
#pragma once

#include "Grids/BoundConds2D.h"
#include "Grids/BoundConds2D_Core.hpp"

namespace Arbalete
{
  //=========================================================================//
  // "BCPtrs2D":                                                             //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // Pseudo-Default Ctor:                                                    //
  //-------------------------------------------------------------------------//
  // Only sets the types of BCs, no actual data:
  //
  template<typename F, int M>
  BCPtrs2D<F,M>::BCPtrs2D(BCType bct)
  : m_m(0),
    m_n(0)
  {
    m_LoST = m_UpST = m_LoVT = m_UpVT = bct;

    for (int b = 0; b < M; ++b)
      m_LoS[b] = m_UpS[b] = m_LoV[b] = m_UpV[b] = nullptr;
  }

  //-------------------------------------------------------------------------//
  // Non-Default Ctor:                                                       //
  //-------------------------------------------------------------------------//
  template<typename F, int M>
  BCPtrs2D<F,M>::BCPtrs2D
  (
    int m,     int n,
    F* loS[M], F* upS[M], F* loV[M], F* upV[M], BCType bct
  )
  : m_m(m),
    m_n(n)
  {
    assert(m_m >= 2*M && m_n >= 2*M);
    m_LoST = m_UpST = m_LoVT = m_UpVT = bct;

    for (int b = 0; b < M; ++b)
    {
      m_LoS[b] = loS[b];
      m_UpS[b] = upS[b];
      m_LoV[b] = loV[b];
      m_UpV[b] = upV[b];
    }
  }

  //=========================================================================//
  // "BoundConds2D":                                                         //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // "PreCompute":                                                           //
  //-------------------------------------------------------------------------//
  template<typename F>
  template<int M>
  void BoundConds2D<F>::PreCompute
    (int m, F const* S, int n, F const* V, DateTime t, BCPtrs2D<F,M>* res)
  const
  {
    static_assert(M == 1 || M == 2, "Invalid stencil size");
    assert(res != nullptr && m >= 2*M && n >= 2*M);

    F hS = S[1] - S[0];
    F hV = V[1] - V[0];

    // Save the sizes and BC Types in "res":
    res->m_m    = m;
    res->m_n    = n;
    res->m_LoST = m_LoST;
    res->m_UpST = m_UpST;
    res->m_LoVT = m_LoVT;
    res->m_UpVT = m_UpVT;

    //----------------------------------------------------------------------//
    // Bound Conds wrt "S":                                                 //
    //----------------------------------------------------------------------//
    for (int j = 0; j < n; ++j)
    {
      //------//
      // LoS: //
      //------//
      if (m_LoST == BCType::DirichletBCT)
        // Dirichlet: evaluate at S[0..M-1]:
        for (int b = 0; b < M; ++b)
          res->m_LoS[b][j] = m_LoSF(S[b], V[j], t);
      else
        // Neumann:   evaluate at S[M]:
        res->m_LoS[0][j] = - m_LoSF(S[M], V[j], t) * hS;  // NB: "-"!

      //------//
      // UpS: //
      //------//
      if (m_UpST == BCType::DirichletBCT)
        // Dirichlet: evaluate at S[m-M..m-1]:
        for (int b = 0; b < M; ++b)
          res->m_UpS[b][j] = m_UpSF(S[m-1-b], V[j], t);
      else
        // Neumann:   evaluate at S[m-1-M]:
        res->m_UpS[0][j] = m_UpSF(S[m-1-M], V[j], t) * hS;
    }
    //-----------------------------------------------------------------------//
    // Bound Conds wrt "V":                                                  //
    //-----------------------------------------------------------------------//
    for (int i = 0; i < m; ++i)
    {
      //------//
      // LoV: //
      //------//
      if (m_LoVT == BCType::DirichletBCT)
        // Dirichlet: evaluate at V[0..M-1]:
        for (int b = 0; b < M; ++b)
          res->m_LoV[b][i] = m_LoVF(S[i], V[b], t);
      else
        // Neumann:   evaluate at V[M]:
        res->m_LoV[0][i] = - m_LoVF(S[i], V[M], t) * hV;  // NB: "-"!

      //------//
      // UpV: //
      //------//
      if (m_UpVT == BCType::DirichletBCT)
        // Dirichlet: evaluate at V[n-M..n-1]:
        for (int b = 0; b < M; ++b)
          res->m_UpV[b][i] = m_UpVF(S[i], V[n-1-b], t);
      else
        // Neumann:   evaluate at V[n-1-M]:
        res->m_UpV[0][i] = m_UpVF(S[i], V[n-1-M], t) * hV;
    }
  }

  //-------------------------------------------------------------------------//
  // "ApplyDirect":                                                          //
  //-------------------------------------------------------------------------//
  // Compute Bound Conds and apply them directly to "f":
  //
  template<typename F>
  template<int M>
  void BoundConds2D<F>::ApplyDirect
    (int m, F const* S, int n, F const* V, DateTime t, F* f) const
  {
    static_assert(M == 1 || M == 2, "Invalid Stencil Size");
    assert(f != nullptr && m >= 2*M && n >= 2*M);

    // XXX: We currently do NOT verify that the boundary conds are consistent
    // in the grid corners; if not, the "S" conds prevail over the "V" ones:
    //
    F  hS = S[1] - S[0];
    F  hV = V[1] - V[0];
    assert(hS > F(0.0) && hV > F(0.0));

    //-----------------------------------------------------------------------//
    // Bound Conds wrt "S": Traverse all "V"s:                               //
    //-----------------------------------------------------------------------//
    for (int j = 0; j < n; ++j)
    {
      //------//
      // LoS: //
      //------//
      if (m_LoST == BCType::DirichletBCT)
        // Dirichlet:
        for (int b = 0; b < M; ++b)
          f[m*j+b] = m_LoSF(S[b], V[j], t);           // (b,j)
      else
      {
        // Neumann:
        F f0 = f[m*j+M];                              // (M,j)
        F df = - m_LoSF(S[M], V[j], t) * hS;          // NB:   "-"...

        for (int b = 0; b < M; ++b)
          f[m*j+b] = f0 + F(M-b) * df;                // (b,j) ...with "+"
      }
      //------//
      // UpS: //
      //------//
      if (m_UpST == BCType::DirichletBCT)
        // Dirichlet:
        for (int b = 0; b < M; ++b)
          f[m*j+m-1-b] = m_UpSF(S[m-1-b], V[j], t);   // (m-1-b,j)
      else
      {
        // Neumann:
        F f0 = f[m*j+m-1-M];                          // (m-1-M,j)
        F df = m_UpSF(S[m-1-M], V[j], t) * hS;

        for (int b = 0; b < M; ++b)
          f[m*j+m-1-b] = f0 + F(M-b) * df;            // (m-1-b,j)
      }
    }
    //-----------------------------------------------------------------------//
    // Bound Conds wrt "V": Traverse all "S"s:                               //
    //-----------------------------------------------------------------------//
    for (int i = 0; i < m; ++i)
    {
      //------//
      // LoV: //
      //------//
      if (m_LoVT == BCType::DirichletBCT)
        // Dirichlet:
        for (int b = 0; b < M; ++b)
          f[m*b+i] = m_LoVF(S[i], V[b], t);           // (i,b)
      else
      {
        // Neumann:
        F f0 = f[m*M+i];                              // (i,M)
        F df = - m_LoVF(S[i], V[M], t) * hV;          // NB:   "-"...
        for (int b = 0; b < M; ++b)
          f[m*b+i] = f0 + F(M-b) * df;                // (i,b) ...with "+"
      }
      //------//
      // UpV: //
      //------//
      if (m_UpVT == BCType::DirichletBCT)
        // Dirichlet:
        for (int b = 0; b < M; ++b)
          f[m*(n-1-b)+i] = m_UpVF(S[i], V[n-1-b], t); // (i,n-1-b)
      else
      {
        // Neumann:
        F f0 = f[m*(n-1-M)+i];                        // (i,n-1-M)
        F df = m_UpVF(S[i], V[n-1-M], t) * hV;
        for (int b = 0; b < M; ++b)
          f[m*(n-1-b)+i] = f0 + F(M-b) * df;          // (i,n-1-b)
      }
    }
    // All done!
  }
}
