// vim:ts=2:et
//===========================================================================//
//                             "DiagSolvers35.h":                            //
//  Solving 3-Diagonal and 5-Diagonal Linear Systems (Host and CUDA Modes)   //
//===========================================================================//
#pragma once

#include "Common/Macros.h"
#ifndef  __CUDACC__
#include "Common/VecMatTypes.h"
#endif

namespace Arbalete
{
  //=========================================================================//
  // "DiagSolver" Template:                                                  //
  //=========================================================================//
  // NB:
  // (*) M==1 for 3-point Stencils, M==2 for 5-point Stencils;
  // (*) the "DiagSolver" functions destroy they argument arrays / Vectors;
  // (*) "rhs" is replaced by the solution;
  // (*) diags are numbered bottom-up:
  // (*) "L" is the size of the main diag and the RHS.
  //
  template<typename F, int M>
  struct DiagSolver
  {
    //-----------------------------------------------------------------------//
    // Solver in the Array Form:                                             //
    //-----------------------------------------------------------------------//
    // Pre-Conds:
    // (*) diags[2*M+1]
    // (*) L >= 2
    // (*) size(diags[1])   == L
    // (*) size(diags[0,2]) == L-1
    // (*) size(diags[0,4]) == L-2 (M==2 only)
    //
    DEVICE static void Solve(int L, F* diags[2*M+1], F* rhs);

    //-----------------------------------------------------------------------//
    // Solver in the Vector Form:                                            //
    //-----------------------------------------------------------------------//
    // For host code only:
    //
#   ifndef __CUDACC__
    DEVICE static void Solve
      (Vector<F> diags[2*M+1], Vector<F>* rhs);
#   endif
  };
}
