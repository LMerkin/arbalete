// vim:ts=2:et
//===========================================================================//
//                           "TimeStepper_RKF5.cu":                          //
//              CUDA Kernel Invocator for the RKF5 Time Stepper              //
//===========================================================================//
#include "Grids/TimeStepper_RKF5_Core.hpp"

namespace Arbalete
{
  //=========================================================================//
  // All Instances of the "RKF5_KernelInvocator":                            //
  //=========================================================================//
  template struct RKF5_KernelInvocator<double>;
  template struct RKF5_KernelInvocator<float>;
}
