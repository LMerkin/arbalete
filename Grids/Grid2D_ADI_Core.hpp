// vim:ts=2:et
//===========================================================================//
//                           "Grid2D_ADI_Core.hpp":                          //
//    Fractional Step Method for 2D PDEs (PR-ADI, Yanenko): Templated Core   //
//===========================================================================//
// To be included into "Grid2D_ADI.hpp" and "Grid2D_ADI.cu":
//
#pragma once

#include "Common/Maths.hpp"
#include "Common/CUDAEnv.hpp"
#include "Grids/Grid2D.h"
#include "Grids/DataADI.h"
#include "Grids/Grids_Common.h"
// NB:
// (*) For Host code, it is sufficient to include only "DiagSolvers35.h"  which
// contain template signatures, and do linking afterwards.
// (*) For CUDA device code, full template definitions must be seen -- there is
// no external CUDA linker!
//
#ifdef   __CUDACC__
#include "Grids/DiagSolvers35.hpp"
#else
#include "Grids/DiagSolvers35.h"
#endif
#include "Grids/BoundConds2D_Core.hpp"

namespace Arbalete
{
  //=========================================================================//
  // Utils:                                                                  //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // "ApplyExplBC":                                                          //
  //-------------------------------------------------------------------------//
  // Apply Bound Conds wrt the Expl Variable, for all vals of the Impl one:
  //
  template<typename F, int M, bool ImplS, bool IsLo>
  DEVICE void ApplyExplBC(BCPtrs2D<F,M> const& bcps, F* PI)
  {
    int m = bcps.m_m;
    int n = bcps.m_n;

    for (int bE = 0; bE < M; ++bE)
      if (ImplS)
      {
        // Expl Bound Conds are wrt "V",  so in the corner points, "S" conds
        // prevail -- the main loop wrt the "l=i" param must not go into the
        // corners:
        // bE = bV, bI = bS, LI = m:
        //
        for (int l = M; l <= m-M-1; ++l)
          if (IsLo)
            bcps.ApplyLoV(PI, l, bE);
          else
            bcps.ApplyUpV(PI, l, bE);

        // Now do the Lo and Up corners wrt i=bS=bI; BEWARE: e=j is then a
        // param which must be specified exactly; use n=LE:
        //
        for (int bI = 0; bI < M; ++bI)
        {
          bcps.ApplyLoS(PI, bI, IsLo ? bE : n-1-bE);
          bcps.ApplyUpS(PI, bI, IsLo ? bE : n-1-bE);
        }
      }
      else
        // Expl Bound Conds are "S" and they prevail -- can do all l=j (incl
        // corners) in one go:
        // bE = bS, l=j:
        //
        for (int l = 0; l < n; ++l)
          if (IsLo)
            bcps.ApplyLoS(PI, bE, l);
          else
            bcps.ApplyUpS(PI, bE, l);
    // All done!
  }

  //-------------------------------------------------------------------------//
  // "ApplyImplBCs":                                                         //
  //-------------------------------------------------------------------------//
  // Apply Bound Conds on the "PI" solution, given "e", wrt Implicit Variable:
  //
  template<typename F, int M, bool ImplS>
  DEVICE inline void ApplyImplBCs(BCPtrs2D<F,M> const& bcps, F* PI, int e)
  {
    for (int b = 0; b < M; ++b)
      if (ImplS)
      {
        // Impl Bound Conds are wrt "S"; j=e:
        bcps.ApplyLoS(PI, b, e);
        bcps.ApplyUpS(PI, b, e);
      }
      else
      {
        // Impl Bound Conds are wrt "V"; i=e:
        bcps.ApplyLoV(PI, e, b);
        bcps.ApplyUpV(PI, e, b);
      }
  }

  //=========================================================================//
  // "ADISubStepIter":                                                       //
  //=========================================================================//
  // For both Host and CUDA sides. NB: "AD" is passed BY COPY, as references
  // cannot be used in global (Host->CUDA) calls:
  //
  // NB: "static" is essential here -- otherwise Host code could get called
  // instead of CUDA code!
  //
  template<typename F, int M, bool IsFwd, bool IsPRADI, bool ImplS>
  GLOBAL static void ADISubStepIter_Core
  (
    typename Grid2D<F>::DRType DR,  DataADI<F,M> AD,
    CUDAEnv::Sched2D const*         CUDACC_OR_DEBUG(sched),
    F* PI,   F const* PE, F* Diags, F* RHSs
  )
  {
    //-----------------------------------------------------------------------//
    // Initialisation:                                                       //
    //-----------------------------------------------------------------------//
    // Individual ptrs to Coeff and Bound Cond arrays:
    //
    int m   = AD.m();
    int n   = AD.n();
    int LI  = ImplS ? m : n;  // Size of the Implicit system (incl Bounds)
    int LE  = ImplS ? n : m;  // Number of Explicit params   (incl Bounds)

    F const* muSI   = AD.template muSI  <ImplS>();
    F const* sigS1I = AD.template sigS1I<ImplS>();
    F const* sigS2I = AD.template sigS2I<ImplS>();
    F const* muVI   = AD.template muVI  <ImplS>();
    F const* sigVI  = AD.template sigVI <ImplS>();
    F const* irI    = AD.template irI   <ImplS>();

    F const* muSE   = AD.template muSE  <ImplS>();
    F const* sigS1E = AD.template sigS1E<ImplS>();
    F const* sigS2E = AD.template sigS2E<ImplS>();
    F const* muVE   = AD.template muVE  <ImplS>();
    F const* sigVE  = AD.template sigVE <ImplS>();
    F        rhoE   = AD.template rhoE  <ImplS>()[0];

    // Linear Eqns Coeffs:
    F const* LECs   = AD.LECs();
    F c1I           = LECs[0];
    F c2I           = LECs[1];
    F c1E           = LECs[2];
    F c2E           = LECs[3];
    F c3E           = LECs[4];
    F dt            = LECs[5];

    // Bound Conds:
    BCPtrs2D<F,M> const& bcps = AD.GetBCPtrs();

    // Differentiation coeffs via the macros provided:
    //
    F const  Der1M1[3] = DER1_M1(F);
    F const  Der1M2[5] = DER1_M2(F);
    F const* Der1      = (M==1) ? Der1M1 : Der1M2;
    F const  Der2M1[3] = DER2_M1(F);
    F const  Der2M2[5] = DER2_M2(F);
    F const* Der2      = (M==1) ? Der2M1 : Der2M2;

    //-----------------------------------------------------------------------//
    // Range of "e" Indices:                                                 //
    //-----------------------------------------------------------------------//
# ifdef __CUDACC__
    // Get the info from "sched" for the curr global Thread Number:
    assert(sched != nullptr);
    int Tn = blockDim.x * blockIdx.x + threadIdx.x;
    CUDAEnv::Sched2D schn = sched[Tn];

    int e  = schn.m_outer;
    if (e < 0)
      // This Thread (and the whole Block it belongs to) is not in use:
      return;

    // Correct "e" to make it base-"M" (rather than base-0):
    e += M;
    assert(M <= e && e <= LE-M-1);
# else
    // In the Host mode, do the full "e" range in a loop parallelised with
    // OpenMP (XXX: not on Mac OS X yet):
    assert(sched == nullptr);

#   ifndef __clang__
#   pragma omp parallel for
#   endif
    for (int e = M; e <= LE-M-1; ++e)
# endif
    //-----------------------------------------------------------------------//
    // "e" Processing (Explicit Var = Parameter):                            //
    //-----------------------------------------------------------------------//
    {
      // Select the Diags and the RHS to be used with this "e":
      //
      int const DS = LI-2*M;  // Max diag and RHS size (as allocated)
      F* eRHS = RHSs + (e-M) * DS;
      F* eDiags[2*M+1];

      int offd =  (e-M) * DS * (2*M+1);
      for (int d = 0; d <= 2*M; ++d, offd += DS)
        eDiags[d] = Diags + offd;

      //---------------------------------------------------------------------//
      // Loop wrt the Implicit Var: Construct the Linear Equations:          //
      //---------------------------------------------------------------------//
#   ifdef __CUDACC__
      // CUDA mode:
      int lFrom = schn.m_innerFrom;
      int lTo   = schn.m_innerTo;

      // Correct "lFrom" and "lTo" to make them base-"M" (rather than base-0):
      lFrom += M;
      lTo   += M;
      // NB: We do not return immediately if lFrom > lTo, as this Thread may
      // still be required for post-processing
      assert(M <= lFrom && lTo <= LI-M-1);

#   else
      // Host mode: Do the full range of "l" indices:
      int lFrom = M;
      int lTo   = LI-M-1;
#   endif

      for (int l = lFrom; l <= lTo; ++l)
      {
        //-------------------------------------------------------------------//
        // "l" Processing: Construct Eqn #l:                                 //
        //-------------------------------------------------------------------//
        //  ImplS: (i,j) = (l,e) = [m*e+l] = [LI*e+l];
        // !ImplS: (i,j) = (e,l) = [m*l+e] = [LE*l+e]:
        //
        int i  = ImplS ? l : e;
        int j  = ImplS ? e : l;
        int ko = m * j + i;
  
        // The  row being formed: slice of (2*M+1) diagonals (at most) in the
        // LHS, and the RHS. Initialise them with Parabolic Terms:
        F lhs[2*M+1];
        F rhs  = PE[ko];

        lhs[M] = F(1.0);
        for (int d = 1; d <= M; ++d)
          lhs[M-d] = lhs[M+d] = F(0.0);

        //-------------------------------------------------------------------//
        // In the PR-ADI scheme, install the Explicit terms in the RHS:      //
        //-------------------------------------------------------------------//
        // NB: Here differentiation is performed  wrt the Explicit var:
        //  ImplS: V is Explicit;
        // !ImplS: S is Explicit:
        //
        if (IsPRADI)
          for (int dr = 0; dr <= 2*M; ++dr)
          {
            int r    =  dr - M;   // -M .. +M
            int er   =  e  + r;
            assert(0 <= er && er <= LE-1);
  
            //  ImplS: (i,j)   = (l,er) = [m*er+l] = [LI*er+l];
            // !ImplS: (i,j)   = (er,l) = [m*l+er] = [LE*l+er]:
            int ir   =  ImplS  ? l  : er;
            int jr   =  ImplS  ? er : l;
            int kr   =  m * jr + ir;
  
            if (PE[kr] != F(0.0))
            rhs +=
              (
                IsFwd
                ? // Explicit term for the Fokker-Planck Equation:
                (  c2E * Der2[dr] *
                    Sqr(ImplS ? sigVE[jr] : sigS1E[ir] * sigS2E[jr])
                 - c1E * Der1[dr] * (ImplS ?  muVE[jr] : muSE  [ir])
                )
                : // Explicit term for the Feynman-Kac Equation:
                (- c2E * Der2[dr] *
                    Sqr(ImplS ? sigVE[j]  : sigS1E[i]  * sigS2E[j])
                 - c1E * Der1[dr] * (ImplS ?  muVE[j]  :  muSE [i])
                )
              ) * PE[kr];
            // NB: The reactive term (discounting) is always Implicit: it does
            // NOT come here!
          }
  
        //-------------------------------------------------------------------//
        // Now the LHS terms (but some of them come into the RHS):           //
        //-------------------------------------------------------------------//
        for (int dq = 0; dq <= 2*M ; ++dq)
        {
          int   q = dq - M;   // -M .. M
          int  lq =  l + q;
          assert(0 <= lq && lq <= LI-1);
  
          //  ImplS: (lq,e) = [m*e+lq] = [LI*e+lq];
          // !ImplS: (e,lq) = [m*lq+e] = [LE*lq+e]:
          int ic = ImplS  ? lq : e;
          int jc = ImplS  ? e  : lq;

          // The Coeff:
          F c =
            IsFwd
            ? // Fokker-Planck:
              (  c1I * Der1[dq] *  (ImplS ?  muSI[ic] :  muVI[jc])
               - c2I * Der2[dq] *
                  Sqr(ImplS ? sigS1I[ic] * sigS2I[jc] : sigVI[jc])
              )
            : // Feynman-Kac:
              (  c1I * Der1[dq] *  (ImplS ?  muSI[i]  :  muVI[j])
               + c2I * Der2[dq] *
                  Sqr(ImplS ? sigS1I[i]  * sigS2I[j]  : sigVI[j])
              );

          // At a boundary?
          bool isLo = lq <= M-1;
          bool isUp = lq >= LI-M;

          if (isLo || isUp)
          {
            //---------------------------------------------------------------//
            // Boundary "l"-points:                                          //
            //---------------------------------------------------------------//
            // Boundary cond type:
            BCType bct =
              ImplS
              ? (isLo ? bcps.m_LoST : bcps.m_UpST)
              : (isLo ? bcps.m_LoVT : bcps.m_UpVT);

            // Distance to the boundary:
            int b = isLo ? lq : (LI-1-lq);
            assert(0 <= b && b <= M-1);

            if (bct == DirichletBCT)
            {
              // Dirichlet BC: modifies the RHS, with the opposite sign:
              // "bv" is the boundary solution value, which depends on "b":
              F bv =
                ImplS
                ? (isLo ? bcps.m_LoS[b][e] : bcps.m_UpS[b][e])
                : (isLo ? bcps.m_LoV[b][e] : bcps.m_UpV[b][e]);

              rhs -= c * bv;
            }
            else
            {
              // Neumann BC: modifies both the LHS and the RHS:
              // "bv" is the (+-) boundary difference value (from b=0 only):
              F bv =
                ImplS
                ? (isLo ? bcps.m_LoS[0][e] : bcps.m_UpS[0][e])
                : (isLo ? bcps.m_LoV[0][e] : bcps.m_UpV[0][e]);

              // Get the "dq" corresponding to lq=M (Lo) or lq=LI-1-M (Up):
              //
              int dqM = isLo ? (2*M-l) : (LI-1-l);
              assert(0 <= dqM && dqM <= 2*M && abs(dq - dqM) <= 2);

              lhs[dqM] += c;
              rhs      -= c * F(M-b) * bv;
            }
          }
          else
            //---------------------------------------------------------------//
            // Generic "l"-points": Into the LHS:                            //
            //---------------------------------------------------------------//
            lhs[dq] += c;

          //-----------------------------------------------------------------//
          // Explicit Crossed Term (Mixed Derivative): Into the RHS:         //
          //-----------------------------------------------------------------//
          // NB: check for derivatives coeffs being 0, otherwise the formula
          // would be too much inefficient:
          //
          F DI = Der1[dq];
          if (DI != F(0.0) && rhoE != F(0.0))
          {
            F xt  = F(0.0);
            for (int dr = 0; dr <= 2*M; ++dr)
            {
              F DE = Der1[dr];
              if (DE != F(0.0))
              {
                int  r = dr - M;  // -M .. +M
                int er = e  + r;
                assert(0 <= er && er <= LE-1);
  
                //  ImplS: (lq,er) = [m*er+lq] = [LI*er+lq];
                // !ImplS: (er,lq) = [m*lq+er] = [LE*lq+er]:
                int irc = ImplS ? lq : er;
                int jrc = ImplS ? er : lq;
                int krc = m * jrc + irc;
  
                if (IsFwd)
                  // Fokker-Planck:
                  xt += DE * sigS1E[irc] * sigS2E[jrc] * sigVE[jrc] * PE[krc];
                else
                  // Feynman-Kac:
                  xt += DE * PE[krc];
              }
            }
            if (!IsFwd)
              xt *= sigS1E[i] * sigS2E[j] * sigVE[j];

            rhs += c3E * DI * rhoE * xt;
          }
          // End of mixed term
        }
        // End of "dq" loop
  
        if (DR != Grid2D<F>::NoDR)
        {
          //-----------------------------------------------------------------//
          // The Reactive Term: always implicit; does not touch boundaries:  //
          //-----------------------------------------------------------------//
          int iri = (DR == Grid2D<F>::VectorDR) ? i : 0;
          rhs +=
            IsPRADI
            ? // In the PR-ADI method, at each half-step we have full operator
              // approximation, so this term is always present,  is always put
              // into the implicit part, and is proportional to half-"dt":
              //
              (- F(0.5) * dt * irI[iri])
            :
              // In the Yanenko method, it is normally included into one oper
              // only (e.g. into the "S" part),  so it is present only in the
              // 1st half-step, with full "dt" factor:
              //
              (ImplS ? (- dt * irI[iri]) : F(0.0));
        }
        //-------------------------------------------------------------------//
        // Eqn for this "l" is done. Map it back into the whole system:      //
        //-------------------------------------------------------------------//
        // "row" and "col" are the indices in the linear system being built,
        // NOT in the original (m*n) space:
        //
        int row = l-M;
        assert(0 <= row && row <= LI-2*M-1);
  
        for (int d = 0; d <= 2*M; ++d)
        {
          int col = row + d - M;
          if (0 <= col && col <= LI-2*M-1)
          {
            // Offset in the diagonal:
            int off = (d <= M) ? col : row;
            assert(0 <= off && off < LI-2*M - abs(M-d));

            eDiags[d][off] = lhs[d];
          }
        }
        eRHS[row] = rhs;
      }
      // End of "l" loop

      //---------------------------------------------------------------------//
      // SOLVE the  3- or 5-diagonal  system:                                //
      //---------------------------------------------------------------------//
      // "RHS" is replaced with the solution.

      // All threads wait for the Diags and the RHS to be formed. The solver is
      // unfortunately single-threaded as yet.  Post-processing is also done in
      // a single thread (1st in the group of threads working on this "e"),  to
      // avoid another sync:
      //
#     ifdef __CUDACC__
      __syncthreads();

      if (!schn.m_outerStart)
        return;
#     endif

      DiagSolver<F,M>::Solve(LI-2*M, eDiags, eRHS);

      //---------------------------------------------------------------------//
      // Post-Processing:                                                    //
      //---------------------------------------------------------------------//
      // Copying the results back into "PI" from "RHS":                      //
      //
      // ImplS : "V" is the Expl param; put the result into the "e"th column
      //    of "pI":
      //    LI==m, LE == n, e < n, pI(i,e) = PI[m*e+i] = RHS[i-M], i=M..(m-M-1):
      // !ImplS: "S" is the Expl param; put the result into the "e"th row
      //    of "pI":
      //    LE==m, LI==n, e < m, pI(e,j) = PI[m*j+e] = RHS[j-M], j=M..(n-M-1) :
      //
      // Thus, l = {i,j} = M .. {m,n}-M-1 = M .. LI-M-1, so exactly the same
      // range l = lFrom .. lTo as above (in either host or device mode);
      // ko(l) = ImplS ? (LI * e + l) : (LE * l + e):
      //
      int ko = ImplS ? (LI * e + M) : (LE * M + e);
      int ks = ImplS ? 1            :  LE;

      for (int l = M; l <= LI-M-1; ++l, ko += ks)
        PI[ko] = eRHS[l-M];

      // Bound Conds:
      // apply the Implicit Bound Conds wrt "l" for this fixed "e":
      //
      ApplyImplBCs<F,M,ImplS>(bcps, PI, e);

      // Now apply the Explicit Bound Conds, but only if "e" is next to the
      // resp Boundary:
      //
      if (e == M)
        // IsLo=true:
        ApplyExplBC<F,M,ImplS,true> (bcps, PI);
      else
      if (e == LE-M-1)
        // IsLo=false:
        ApplyExplBC<F,M,ImplS,false>(bcps, PI);
    }
    // End of "e" processing
  }

  //=========================================================================//
  // "ADI_KernelInvocator":                                                  //
  //=========================================================================//
  // NB: This is a Host-invoked function, so it's OK to pass "DataADI" by ref:
  //
  template<typename F, int M, bool IsFwd, bool IsPRADI, bool ImplS>
  struct ADI_KernelInvocator
  {
    static void Run
    (
      CUDAEnv const& cudaEnv, typename Grid2D<F>::DRType DR,
      DataADI<F,M> const& AD, CUDAEnv::Sched2D const* sched,
      F* PI,  F const* PE,    F* Diags,     F* RHSs
    )
#   ifdef __CUDACC__
    {
      // Set the CUDA device:
      CUDAEnv::Lock();
      cudaEnv.SetCUDADevice();

      // Invoke the kernel:
      ADISubStepIter_Core<F, M, IsFwd, IsPRADI, ImplS>
        <<<cudaEnv.NThreadBlocks(), cudaEnv.ThreadBlockSize(), 0,
           cudaEnv.Stream()
        >>>
        (DR, AD, sched, PI, PE, Diags, RHSs);

      CUDAEnv::Unlock();
      CUDA_CHECK_STATUS("ADI Kernel invocation failed")
    }
#   else
    ; // Spec only
#   endif
  };
}
