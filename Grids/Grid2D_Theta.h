// vim:ts=2:et
//===========================================================================//
//                              "Grid2D_Theta.h":                            //
//           Fully-Implicit and Crank-Nicolson Methods for 2D PDEs           //
//===========================================================================//
#pragma once

#include "Common/VecMatTypes.h"
#include "Grids/Grid2D.h"
#include "Grids/DataTheta.h"
#include <string>

namespace Arbalete
{
  //=========================================================================//
  // "Grid2D_Theta":                                                         //
  //=========================================================================//
  template<typename F>
  class Grid2D_Theta final: public Grid2D<F>
  {
  private:
    //-----------------------------------------------------------------------//
    // Grid Params and Tmp Data:                                             //
    //-----------------------------------------------------------------------//
    // Currently, only Fully Implicit (Theta=1) and Crank-Nicolson (Theta=1/2)
    // methods are supported:
    bool                            m_isCN;

    // Compound scratch vector for Diffusion Coeffs:
    // See the ".hpp" file for the layout:
    mutable DataTheta<F>            m_HD;   // Data vectors (scratch)
    mutable TradingCalendar::CalReg m_regE; // Only for CN

    // Data structs for the Sparse Solver:
    // The Matrix and the RHS:
    int                             m_R;    // Number of rows
    int                             m_N;    // Number of non-0 matrix entries
    mutable Vector<F>               m_Avals;
    mutable std::vector<int>        m_Acols;
    mutable std::vector<int>        m_ArowIdx;
    mutable Vector<F>               m_rhs;
    mutable Vector<F>               m_res;

    // ParDiSo-specific internal data. NB: "m_pt" is not "mutable": holds
    // internal ParDiSo ptrs (XXX: not clear how to release them):
    void*                           m_pt    [64];
    mutable int                     m_params[64];

    // Default Ctor is hidden:
    Grid2D_Theta();

    //-----------------------------------------------------------------------//
    // "InitInduct":                                                         //
    //-----------------------------------------------------------------------//
    // Implementing the abstract method from "Grid2D":
    void InitInduct
    (
      bool                        isFwd,
      typename Grid2D<F>::DRType  DR,
      DateTime                    t_from,
      BoundConds2D<F> const&      bcs
    )
    const override;

    //-----------------------------------------------------------------------//
    // "DoTimeStepGen": The Actual Generic Time Step Method:                 //
    //-----------------------------------------------------------------------//
    template<bool IsFwd, bool IsCN, bool Rotator>
    void DoTimeStepGen
    (
      typename Grid2D<F>::DRType  DR,
      DateTime                    t_curr,
      F*                          p,
      DateTime                    t_next,
      TradingCalendar::CalReg     reg,
      BoundConds2D<F> const&      bcs
    )
    const;

    //-----------------------------------------------------------------------//
    // "DoOneStep":                                                          //
    //-----------------------------------------------------------------------//
    // Implementing the abstract method from "Grid2D":
    F* DoOneStep
    (
      bool                        isFwd,
      typename Grid2D<F>::DRType  DR,
      int                         step_k,
      DateTime                    t_curr,
      F*                          p,
      DateTime                    t_next,
      TradingCalendar::CalReg     reg,
      BoundConds2D<F> const&      bcs
    )
    const override;

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor & Dtor:                                              //
    //-----------------------------------------------------------------------//
    // Currently accepted values for "params.m_method":
    // (*) "CN"       (Crank-Nicolson)
    // (*) "FullImpl" (Fully-Implicit)
    // Only 3*3 stencils are currently accepted (params.m_sts == 3):
    //
    Grid2D_Theta
    (
      std::shared_ptr<Diffusion2D<F>> const& diff,
      typename Grid2D<F>::Params      const& params
    );

    ~Grid2D_Theta();

    // Is it a Crank-Nicolson grid (of Fully-Implicit)?
    bool IsCrankNicolson() const    { return m_isCN; }

    //-----------------------------------------------------------------------//
    // Override "UseCUDA()":                                                 //
    //-----------------------------------------------------------------------//
    bool UseCUDA() const { return false; }
  };
}
