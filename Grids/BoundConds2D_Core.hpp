// vim:ts=2:et
//===========================================================================//
//                         "BoundConds2D_Core.hpp":                          //
//                   Boundary Conditions for 2D PDE Grids                    //
//===========================================================================//
#pragma once

#include "Grids/BoundConds2D.h"
#include <cassert>

namespace Arbalete
{
  //=========================================================================//
  // "BCPtrs2D::Apply{Lo|Up}{S|V}":                                          //
  //=========================================================================//
  // The following funcs are for both Host and CUDA modes:
  //
  //-------------------------------------------------------------------------//
  // "ApplyLoS":                                                             //
  //-------------------------------------------------------------------------//
  template<typename F, int M>
  DEVICE inline void BCPtrs2D<F,M>::ApplyLoS(F* f, int b, int j) const
  {
    assert(f != nullptr && 0 <= b && b < M && 0 <= j && j < m_n);
    // i = b, (i,j) = [m*j + b]:
    f[m_m*j+b] =
      (m_LoST == DirichletBCT)
      ? m_LoS[b][j]
      : f[m_m*j+M] - F(M-b) * m_LoS[0][j];
  }

  //-------------------------------------------------------------------------//
  // "ApplyUpS":                                                             //
  //-------------------------------------------------------------------------//
  template<typename F, int M>
  DEVICE inline void BCPtrs2D<F,M>::ApplyUpS(F* f, int b, int j) const
  {
    assert(f != nullptr && 0 <= b && b < M && 0 <= j && j < m_n);
    // i = m-1-b, (i,j) = [m*j + m-1-b]:
    f[m_m*j+(m_m-1-b)] =
      (m_UpST == DirichletBCT)
      ? m_UpS[b][j]
      : f[m_m*j+(m_m-1-M)] + F(M-b) * m_UpS[0][j];
  }

  //-------------------------------------------------------------------------//
  // "ApplyLoV":                                                             //
  //-------------------------------------------------------------------------//
  template<typename F, int M>
  DEVICE inline void BCPtrs2D<F,M>::ApplyLoV(F* f, int i, int b) const
  {
    assert(f != nullptr && 0 <= b && b < M && 0 <= i && i < m_m);
    // j = b, (i,j) = [m*b + i]:
    f[m_m*b+i] =
      (m_LoVT == DirichletBCT)
      ? m_LoV[b][i]
      : f[m_m*M+i] - F(M-b) * m_LoV[0][i];
  }

  //-------------------------------------------------------------------------//
  // "ApplyUpV":                                                             //
  //-------------------------------------------------------------------------//
  template<typename F, int M>
  DEVICE inline void BCPtrs2D<F,M>::ApplyUpV(F* f, int i, int b) const
  {
    assert(f != nullptr && 0 <= b && b < M && 0 <= i && i < m_m);
    // j = n-1-b, (i,j) = [m*(n-1-b) + i]:
    f[m_m*(m_n-1-b)+i] =
      (m_UpVT == DirichletBCT)
      ? m_UpV[b][i]
      : f[m_m*(m_n-1-M)+i] + F(M-b) * m_UpV[0][i];
  }

  //-------------------------------------------------------------------------//
  // "ApplyAll":                                                             //
  //-------------------------------------------------------------------------//
  // NB: This function is used in both Host and CUDA modes; "*f" must be an
  // (m * n) matrix stored column-wise: (i,j) = [m*j+i]:
  //
  template<typename F, int M>
  DEVICE void BCPtrs2D<F,M>::ApplyAll(F* f) const
  {
    assert(f != nullptr);
    for (int b = 0; b < M; ++b)
    {
      // Bound Conds wrt "S": Traverse all "V"s:
      for (int j = 0; j < m_n; ++j)
      {
        ApplyLoS(f, b, j);
        ApplyUpS(f, b, j);
      }

      // Bound Conds wrt "V": Traverse all "S"s:
      for (int i = 0; i < m_m; ++i)
      {
        ApplyLoV(f, i, b);
        ApplyUpV(f, i, b);
      }
    }
  }
}
