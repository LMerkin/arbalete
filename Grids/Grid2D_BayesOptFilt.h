// vim:ts=2:et
//===========================================================================//
//                           "Grid2D_BayesOptFilt.h":                        //
//        Special Grid for Computation of Likelihood Functions Using         //
//                          Bayesian Optimal Filtering                       //
//===========================================================================//
#pragma once

#include "Grids/Grid2D.h"
#include <stdexcept>
#include <cassert>

namespace Arbalete
{
  //=========================================================================//
  // "Grid2D_BayesOptFilt":                                                  //
  //=========================================================================//
  template<typename F>
  class Grid2D_BayesOptFilt final: public Grid2D<F>
  {
  private:
    //-----------------------------------------------------------------------//
    // Grid Data:                                                            //
    //-----------------------------------------------------------------------//
    mutable F*        m_pa;       // Alternating solution space (Host or CUDA)
    mutable F         m_SYprev;   // Prev conditioning point
    mutable F         m_opt[3];   // [ V = ArgMax(P), Max(P), Denom ]
    F*                m_cudaOpt;  // Same in CUDA space (len = 3)
    CUDAEnv::Sched2D* m_cudaSch;  // Schedule of CUDA threads (or NULL)

    // Default ctor is hidden:
    Grid2D_BayesOptFilt();

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor and Dtor:                                            //
    //-----------------------------------------------------------------------//
    Grid2D_BayesOptFilt
    (
      std::shared_ptr<Diffusion2D<F>> const& diff,
      typename Grid2D<F>::Params      const& params
    );

    ~Grid2D_BayesOptFilt();

    //-----------------------------------------------------------------------//
    // Accessors:                                                            //
    //-----------------------------------------------------------------------//
    F GetOptV  () const { return m_opt[0]; }        // Optimal V = argmax(P)
    F GetMaxP  () const { return m_opt[1]; }        // Max(P)
    F GetDenomP() const { return m_opt[2]; }        // P normalising factor
    F GetSY    () const { return this->m_Sx[0]; }
    F GetPrevSY() const { return m_SYprev; }

    //-----------------------------------------------------------------------//
    // De-Configured Methods:                                                //
    //-----------------------------------------------------------------------//
    // XXX: Of course, this breaks the "IS-A" relationship between this class
    // and  its parent ("Grid2D")...
    //
    // "SolveFokkerPlanck":
    //
    void SolveFokkerPlanck
    (
      DateTime                            UNUSED_PARAM(t_from),
      Matrix<F>*                          UNUSED_PARAM(p),
      DateTime                            UNUSED_PARAM(t_to),
      BoundConds2D<F> const&              UNUSED_PARAM(bcs),
      typename Grid2D<F>::CallBack const* UNUSED_PARAM(call_back),
      std::vector<DateTime> const*        UNUSED_PARAM(special_times)
    )
    const override
    {
      throw std::runtime_error
            ("Grid2D_BayesOptFilt::SolveFokkerPlanck: Not Applicable");
    }

    // "SolveFeynmanKac":
    //
    void SolveFeynmanKac
    (
      bool                                UNUSED_PARAM(with_discount),
      DateTime                            UNUSED_PARAM(t_from),
      Matrix<F>*                          UNUSED_PARAM(p),
      DateTime                            UNUSED_PARAM(t_to),
      BoundConds2D<F> const&              UNUSED_PARAM(bcs),
      typename Grid2D<F>::CallBack const* UNUSED_PARAM(call_back),
      std::vector<DateTime> const*        UNUSED_PARAM(special_times)
    )
    const override
    {
      throw std::runtime_error
            ("Grid2D_BayesOptFilt::SolveFeynmanKac: Not Applicable");
    }

    // NB: "SolveBSM", "ArrowDebreu" and "GridTest" are then not applicable
    // either...

    //-----------------------------------------------------------------------//
    // "AdjustSx":                                                           //
    //-----------------------------------------------------------------------//
    // Sets a new conditioning point "SY":
    //
    int AdjustSx
    (
      DateTime  UNUSED_PARAM(t),
      F         SY,
      Time      UNUSED_PARAM(next_dt)
    )
    override
    {
      Vector<F>& Sx = this->m_Sx;   // Alias
      assert(int(Sx.size()) == 1);

      // Move the cond points (NB: in CUDA space, "Sx" axis was removed):
      m_SYprev = Sx[0];
      Sx[0]    = SY;
      return 0;
    }

    //-----------------------------------------------------------------------//
    // "RunFwd":                                                             //
    //-----------------------------------------------------------------------//
    // A new method implemented on top of "TimeMarshal", replacing "SolveFokker
    // Planck". This is the main purpoise of the "Grid2D_BayesOptFilt" class:
    //
    void RunFwd
    (
      DateTime                            t_from,
      Vector<F>*                          p,           // NB: len(p) = n
      DateTime                            t_to,
      typename Grid2D<F>::CallBack const* call_back     = nullptr,
      std::vector<DateTime> const*        special_times = nullptr
    )
    const
    {
      assert(p != nullptr);
      if (p->size() != this->m_Vx.size())
        throw std::invalid_argument
              ("Grid2D_BayesOptFilt::RunFwd: Invalid \"p\" size");

      // NB: IsFwd=true, DR=NoDR; BoundConds not actually used:
      this->template TimeMarshal<true>
        (Grid2D<F>::NoDR,   t_from,    &((*p)[0]),  t_to,
         BoundConds2D<F>(), call_back, special_times);
    }

  private:
    //-----------------------------------------------------------------------//
    // "InitInduct":                                                         //
    //-----------------------------------------------------------------------//
    // Implementation of the corresp abstract method from "Grid2D". Currently
    // nothing to do:
    //
    void InitInduct
    (
      bool                        UNUSED_PARAM(isFwd),
      typename Grid2D<F>::DRType  UNUSED_PARAM(DR),
      DateTime                    UNUSED_PARAM(t_from),
      BoundConds2D<F> const&      UNUSED_PARAM(bcs)
    )
    const override {}

    //-----------------------------------------------------------------------//
    // "DoOneStep":                                                          //
    //-----------------------------------------------------------------------//
    // Implementation of the corresp abstract method from "Grid2D":
    //
    F* DoOneStep
    (
      bool                        isFwd,
      typename Grid2D<F>::DRType  DR,
      int                         step_k,
      DateTime                    t_curr,
      F*                          p,
      DateTime                    t_next,
      TradingCalendar::CalReg     reg,
      BoundConds2D<F> const&      bcs
    )
    const override;
  };
}
