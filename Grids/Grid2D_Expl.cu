// vim:ts=2:et
//===========================================================================//
//                             "Grid2D_Expl.cu":                             //
//                   CUDA Kernels for Explicit PDE Methods                   //
//===========================================================================//
#include "Grids/Grid2D_Expl_Core.hpp"

namespace Arbalete
{
  //=========================================================================//
  // All Instances of "ExplDDt2D_KernelInvocator":                           //
  //=========================================================================//
  template struct ExplDDt2D_KernelInvocator<double, 1, false>;
  template struct ExplDDt2D_KernelInvocator<double, 1, true>;

  template struct ExplDDt2D_KernelInvocator<double, 2, false>;
  template struct ExplDDt2D_KernelInvocator<double, 2, true>;

  template struct ExplDDt2D_KernelInvocator<float,  1, false>;
  template struct ExplDDt2D_KernelInvocator<float,  1, true>;

  template struct ExplDDt2D_KernelInvocator<float,  2, false>;
  template struct ExplDDt2D_KernelInvocator<float,  2, true>;
}
