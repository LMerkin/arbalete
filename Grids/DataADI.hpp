// vim:ts=2:et
//===========================================================================//
//                                "DataADI.hpp":                             //
//                       Data Structures for "Grid2D_ADI"                    //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Common/CUDAEnv.hpp"
#include "Grids/DataADI.h"
#include "Grids/Grid2D_ADI.h"
#include <cassert>

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "DataADI":                                                              //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // Non-Default Ctor:                                                       //
  //-------------------------------------------------------------------------//
  // NB: "forCUDA" has no effect unless grid.UseCUDA() is "true":
  //
  template<typename F, int M>
  DataADI<F,M>::DataADI(Grid2D_ADI<F,M> const& grid, bool forCUDA)
  {
    static_assert(M == 1 || M == 2, "DataADI: Invalid Stencil Size");

    if (forCUDA && !grid.UseCUDA())
    {
      // Nothing to do, no allocations, all ptrs are NULLs:
      m_K    = 0;
      m_data = m_muS1 = m_sigS11 = m_sigS21 = m_muV1   = m_sigV1 = m_rho1  =
      m_ir1  = m_LECs = m_muS0   = m_sigS10 = m_sigS20 = m_muV0  = m_sigV0 =
      m_rho0 = m_ir0  = m_end    = nullptr;

      for (int b = 0; b < M; ++b)
        m_bcps.m_LoS[b] = m_bcps.m_UpS[b] = m_bcps.m_LoV[b] =
        m_bcps.m_UpV[b] = nullptr;
      return;
    }

    // GENERIC CASE:
    // Calculate the total length of "m_data" to be allocated:
    //
    int  m = int(grid.GetSx().size());
    int  n = int(grid.GetVx().size());
    bool isRateModel = grid.GetDiffusion().IsIRModel();

    m_bcps.m_m = m;
    m_bcps.m_n = n;
    m_K   =
      isRateModel
      ? ((6 + 2 * M) * (m + n) + 8)
      : ((4 + 2 * M) *  m + (6 + 2 * M) * n + 10);

    // Allocate CUDA or Host memory for "m_data":
    m_data      =
      forCUDA
      ? grid.GetCUDAEnv().template CUDAAlloc<F>(m_K)
      : new F[m_K];

    // Set up ptr aliases:
    // Block "1":
    m_muS1      = m_data    + 0;
    m_sigS11    = m_muS1    + m;
    m_sigS21    = m_sigS11  + m;
    m_muV1      = m_sigS21  + n;
    m_sigV1     = m_muV1    + n;
    m_rho1      = m_sigV1   + n;
    m_ir1       = m_rho1    + 1;

    // LECs:
    m_LECs      = m_ir1     + (isRateModel ? m : 1);

    // BoundConds (the types are not set yet):
    F* nextBCP  = m_LECs    + 6;
    for (int b = 0; b < M; ++b)
    {
      m_bcps.m_LoS[b] = nextBCP;
      m_bcps.m_UpS[b] = m_bcps.m_LoS[b] + n;
      m_bcps.m_LoV[b] = m_bcps.m_UpS[b] + n;
      m_bcps.m_UpV[b] = m_bcps.m_LoV[b] + m;
      nextBCP         = m_bcps.m_UpV[b] + m;
    }

    // Block "0":
    m_muS0      = nextBCP;
    m_sigS10    = m_muS0    + m;
    m_sigS20    = m_sigS10  + m;
    m_muV0      = m_sigS20  + n;
    m_sigV0     = m_muV0    + n;
    m_rho0      = m_sigV0   + n;
    m_ir0       = m_rho0    + 1;

    m_end       = m_ir0     + (isRateModel ? m : 1);
    assert(m_end - m_data == m_K);
  }

  //-------------------------------------------------------------------------//
  // Explicit Dtor:                                                          //
  //-------------------------------------------------------------------------//
  // XXX: The "DateADI" object itself does not have sufficient info to destroy
  // itself -- the info is provided by the caller:
  //
  template<typename F, int M>
  void DataADI<F,M>::Destroy(Grid2D_ADI<F,M> const& grid, bool forCUDA)
  {
    if (m_data != nullptr)
    {
      if (forCUDA)
      {
        CUDAEnv const& cudaEnv = grid.GetCUDAEnv();
        cudaEnv.Stop();   // Just in case...
        cudaEnv.template CUDAFree<F>(m_data);
      }
      else
        delete[] m_data;
    }
    // Just in case, re-set all ptrs to NULL:
    m_K = 0;
    m_data = m_muS1 = m_sigS11 = m_sigS21 = m_muV1   = m_sigV1 = m_rho1  =
    m_ir1  = m_LECs = m_muS0   = m_sigS10 = m_sigS20 = m_muV0  = m_sigV0 =
    m_rho0 = m_ir0  = m_end    = nullptr;

    for (int b = 0; b < M; ++b)
      m_bcps.m_LoS[b] = m_bcps.m_UpS[b] = m_bcps.m_LoV[b] =
      m_bcps.m_UpV[b] = nullptr;
  }

  //-------------------------------------------------------------------------//
  // "GetIBlockLen":                                                         //
  //-------------------------------------------------------------------------//
  template<typename F, int M>
  int DataADI<F,M>::GetIBlockLen() const
  {
    int    res =  int(m_muS0 - m_muS1);
    assert(res == int(m_end  - m_LECs));
    return res;
  }
}
