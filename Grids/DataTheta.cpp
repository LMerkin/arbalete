// vim:ts=2:et
//===========================================================================//
//                              "DataTheta.cpp":                             //
//           Pre-Defined Instances of "DataTheta" (Host-Based only):         //
//===========================================================================//
#include "Grids/DataTheta.hpp"

namespace Arbalete
{
  template class DataTheta<double>;
  template class DataTheta<float>;
}
