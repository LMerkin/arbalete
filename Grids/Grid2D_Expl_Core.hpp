// vim:ts=2:et
//===========================================================================//
//                         "Grid2D_Expl_Core.cpp":                           //
//                2D PDE Grids with Explicit Time Propagation                //
//===========================================================================//
// To be included into "Grid2D_Expl.hpp" and "Grid2D_Expl.cu":
//
#pragma once

#include "Common/Maths.hpp"
#include "Common/CUDAEnv.hpp"
#include "Grids/Grid2D.h"
#include "Grids/DataExpl.h"
#include "Grids/Grids_Common.h"
#include "Grids/BoundConds2D_Core.hpp"

namespace Arbalete
{
  //=========================================================================//
  // "ExplDDt2D_Core" for both Fwd and Bwd Equations:                        //
  //=========================================================================//
  // NB: "DataExpl" must be passed by value! "P" is not a "const" ptr because
  // bound conds may be set up on the "P" data:
  //
  // NB: "static" is essential here -- otherwise Host code could get called
  // instead of CUDA code!
  //
  template<typename F, int M, bool IsFwd>
  GLOBAL static void ExplDDt2D_Core
  (
    typename Grid2D<F>::DRType DR,                 DataExpl<F,M> ED,
    CUDAEnv::Sched1D const* CUDACC_OR_DEBUG(sched), F* P,     F* DDt
  )
  {
    //-----------------------------------------------------------------------//
    // Initialisation:                                                       //
    //-----------------------------------------------------------------------//
    // Get ptrs to SDE Coeffs from the compound "ED" struct:
    int m  = ED.m();
    int n  = ED.n();

    F const* muS    = ED.muS  ();
    F const* sigS1  = ED.sigS1();
    F const* sigS2  = ED.sigS2();
    F const* muV    = ED.sigV ();
    F const* sigV   = ED.muV  ();
    F const* rho    = ED.rho  ();
    F const* IR     = ED.ir   (); // May be unused, actually

    F const* LECs   = ED.LECs ();
    F hS            = LECs[0];
    F hV            = LECs[1];
    F hS2           = LECs[2];
    F hV2           = LECs[3];
    F hSV           = LECs[4];

    // Differentiation coeffs via the macros provided:
    //
    F const  Der1M1[3] =  DER1_M1(F);
    F const  Der1M2[5] =  DER1_M2(F);
    F const* Der1      =  (M==1) ? Der1M1 : Der1M2;
    F const  Der2M1[3] =  DER2_M1(F);
    F const  Der2M2[5] =  DER2_M2(F);
    F const* Der2      =  (M==1) ? Der2M1 : Der2M2;

    // Get the Bound Cond Ptrs: XXX: this is actually required if our (i,j)
    // pairs come near the bounds, but the overhead is minimal -- only the
    // ptrs are copied -- so we do it in any case:
    //
    BCPtrs2D<F,M> bcps = ED.GetBCPtrs();

#   ifdef __CUDACC__
    // The order number of this thread (across all blocks):
    int Tn  = blockDim.x * blockIdx.x + threadIdx.x;
    assert(sched != nullptr);

    int lFrom = sched[Tn].m_from;
    int lTo   = sched[Tn].m_to;
    if (lFrom > lTo)
      // This thread has nothing to do: XXX: inefficiency. NB: we can safely
      // exit the unused threads at this point because there is no post-pro-
      // cessing after the "l" loop:
      return;

#   ifndef NDEBUG
    int K1 = (m-2*M) * (n-2*M);
    assert(0 <= lFrom && lFrom <= lTo && lTo <= K1-1);
#   endif

#   else  // !__CUDACC__

    assert(sched == nullptr);
    int K1    = (m-2*M) * (n-2*M);
    int lFrom = 0;
    int lTo   = K1-1;
    //
    // Select the full range of indices and parallelise the loop with OpenMP:
    // XXX: Not on Mac OS X yet:
    //
#   ifndef __clang__
#   pragma omp parallel for
#   endif
#   endif  // __CUDACC__
    for (int l = lFrom; l <= lTo; ++l)
    {
    //-----------------------------------------------------------------------//
    // (i,j) Processing:
    //-----------------------------------------------------------------------//
    // Compute the time derivatives in the interior points.  NB : "DDt" is
    // never computed for boundary points  --  they are propagated via the
    // bound conds only, so "l" covers non-boundary points only:
      //---------------------------------------------------------------------//
      // From this "l", determine the proper (i,j):                          //
      //---------------------------------------------------------------------//
      // l  = j' * (m-2*M) + i' where (i', j') = (i,j) - M:
      int    m1  = m-2*M;
      assert(m1 >= 1);
      int j = l  / m1 + M;
      int i = l  % m1 + M;
      assert(M <= i && i <= m-M-1 && M <= j && j <= n-M-1);

      F convS  = F(0.0);      // Convective term in S
      F convV  = F(0.0);      // Convective term in V
      F diffS2 = F(0.0);      // Diffusive  term in S, S
      F diffV2 = F(0.0);      // Diffusive  term in V, V
      F diffSV = F(0.0);      // Diffusive  term in S, V

      //---------------------------------------------------------------------//
      // Derivatives wrt "S":                                                //
      //---------------------------------------------------------------------//
      for (int dq = 0; dq <= 2*M; ++dq)
      {
        int q  = dq - M;      // -M .. +M
        assert(-M <=  q &&  q <= M);

        int iq = i+q;
        assert( 0 <= iq && iq <= m-1);
        int kq = j*m + iq;    // (iq, j)

        // (iq) may touch the Bounds, then set up the corresp Bound Conds:
        if (iq < M)
          bcps.ApplyLoS(P, iq,     j);      // b=iq
        else
        if (iq >= m-M)
          bcps.ApplyUpS(P, m-1-iq, j);      // b=m-1-iq

        F incrConvS  = Der1[dq] * P[kq];
        F incrDiffS2 = Der2[dq] * P[kq];
        if (IsFwd)
        {
          // Fokker-Planck: diffusion coeffs are inside differentiation:
          incrConvS  *=       muS[iq];
          incrDiffS2 *= Sqr(sigS1[iq] * sigS2[j]);
        }
        convS  += incrConvS;
        diffS2 += incrDiffS2;

        //-------------------------------------------------------------------//
        // The mixed derivative:                                             //
        //-------------------------------------------------------------------//
        if (Der1[dq] != F(0.0) && (*rho) != F(0.0))
        {
          F incrDiffSV = F(0.0);
          for (int dr = 0; dr <= 2*M; ++dr)
          {
            int r   = dr - M;     // -M .. +M
            assert(-M <= r  &&  r <= M);

            int jr = j+r;
            assert( 0 <= jr && jr <= n-1);
            int kqr = jr*m + iq;  // (iq, jr)

            // (iq) or (jr) may touch the Bounds, then set up the corresp Bound
            // Conds -- the "S" conds prevail:
            if (iq < M)
              bcps.ApplyLoS(P, iq,     j);  // b=iq
            else
            if (iq >= m-M)
              bcps.ApplyUpS(P, m-1-iq, j);  // b=m-1-iq
            else
            if (jr < M)
              bcps.ApplyLoV(P, iq,     jr); // b=jr
            else
            if (jr >= n-M)
              bcps.ApplyUpV(P, iq, n-1-jr); // b=n-1-jr

            F   iiDSV = Der1[dr] * P[kqr];
            if (IsFwd)
              // Fokker-Planck: diffusion coeffs are inside differentiation:
              iiDSV  *= sigS1[iq] * sigS2[jr] * sigV[jr];
            incrDiffSV += iiDSV;
          }
          diffSV += Der1[dq] * incrDiffSV;
        }
      }
      //---------------------------------------------------------------------//
      // Derivatives wrt "V":                                                //
      //---------------------------------------------------------------------//
      for (int dr = 0; dr <= 2*M; ++dr)
      {
        int r  = dr - M;    // -M .. +M
        assert(-M <= r && r <= M);

        int jr = j+r;
        assert( 0 <= jr && jr <= n-1);
        int kr = jr*m + i;  // (i, jr)

        // XXX: unfortunately, need to check for near-bound points again  --
        // this may not have been done yet if the mixed derivative above was
        // not computed:
        if (jr < M)
          bcps.ApplyLoV(P, i,     jr);      // b=jr
        else
        if (jr >= n-M)
          bcps.ApplyUpV(P, i, n-1-jr);      // b=n-1-jr

        F incrConvV  = Der1[dr] * P[kr];
        F incrDiffV2 = Der2[dr] * P[kr];
        if (IsFwd)
        {
          // Fokker-Planck: diffusion coeffs are inside differentiation:
          incrConvV  *=      muV[jr];
          incrDiffV2 *= Sqr(sigV[jr]);
        }
        convV  += incrConvV;
        diffV2 += incrDiffV2;
      }

      if (!IsFwd)
      {
        // Feynman-Kac: diffusion coeffs are constant across the stencil:
        convS  *= muS[i];
        convV  *= muV[j];
        diffS2 *= Sqr(sigS1[i] * sigS2[j]);
        diffV2 *= Sqr(sigV [j]);
        diffSV *= sigS1[i] * sigS2[j] * sigV[j];
      }
      diffSV *= (*rho);

      //---------------------------------------------------------------------//
      // Assemble the result:                                                //
      //---------------------------------------------------------------------//
      // Apply the spatial steps:
      convS  /= hS;
      convV  /= hV;
      diffS2 /= hS2;
      diffV2 /= hV2;
      diffSV /= hSV;

      F res = F(0.0);
      if (IsFwd)
        // Fokker-Planck time derivative:
        res = - convS - convV + F(0.5) * (diffS2 + diffV2) + diffSV;
      else
      {
        // Feynman-Kac time derivative:
        res = - convS - convV - F(0.5) * (diffS2 + diffV2) - diffSV;

        // Discounting MAY be applied:
        if (DR == Grid2D<F>::ScalarDR)
          res -= IR[0] * P[j*m+i];
        else
        if (DR == Grid2D<F>::VectorDR)
          res -= IR[i] * P[j*m+i];
      }
      DDt[j*m+i] = res;

      //---------------------------------------------------------------------//
      // Boundary Points: DDt set to 0:                                      //
      //---------------------------------------------------------------------//
      // After j = n-M-1, do the boundary "j" points as well (by the same
      // thread which did j = n-M-1):
      //
      if (j == n-M-1)
        for (int b = 0; b < M; ++b)
          // j==b and j==n-1-b:
          DDt[b*m+i] = DDt[(n-1-b)*m+i] = F(0.0);

      // After the last "i", do the boundary "i" points as well, by the same
      // group of threads which did i = m-M-1, and with the same division of
      // labour between them, so do this inside the current "j" loop:
      //
      if (i == m-M-1)
        for (int b = 0; b < M; ++b)
          // i==b and i==m-1-b:
          DDt[j*m+b] = DDt[j*m+(m-1-b)] = F(0.0);
    }
    // End of the "l" loop
  }

  //=========================================================================//
  // "ExplDDt2D_KernelInvocator":                                            //
  //=========================================================================//
  // NB: this is a Host-invoked function so it's OK to pass "DataExpl" by ref:
  //
  template<typename F, int M, bool IsFwd>
  struct ExplDDt2D_KernelInvocator
  {
    static void Run
    (
      CUDAEnv const& cudaEnv,  typename Grid2D<F>::DRType  DR,
      DataExpl<F,M> const& ED, CUDAEnv::Sched1D const*  sched,
      F* P,                    F* DDt
    )
#   ifdef __CUDACC__
    {
      // Set the CUDA device:
      CUDAEnv::Lock();
      cudaEnv.SetCUDADevice();

      // Invoke the kernel:
      ExplDDt2D_Core<F, M, IsFwd>
        <<<cudaEnv.NThreadBlocks(), cudaEnv.ThreadBlockSize(), 0,
           cudaEnv.Stream()
        >>>
        (DR, ED, sched, P, DDt);

      CUDAEnv::Unlock();
      CUDA_CHECK_STATUS("ExplDDt Kernel invocation failed")
    }
#   else
    ; // Spec only
#   endif
  };
}
