// vim:ts=2:et
//===========================================================================//
//                       "Grid2D_ChapmanKolmogorov.h":                       //
//      Computation of PDFs using Transitional Densities of Diffusions       //
//                      and Chapman-Kolmogorov Equation                      //
//===========================================================================//
#pragma once

#include "Grids/Grid2D.h"

namespace Arbalete
{
  //=========================================================================//
  // "Grid2D_ChapmanKolmogorov":                                             //
  //=========================================================================//
  template<typename F>
  class Grid2D_ChapmanKolmogorov final: public Grid2D<F>
  {
  private:
    //-----------------------------------------------------------------------//
    // Grid Data:                                                            //
    //-----------------------------------------------------------------------//
    bool              m_normalise; // Re-normalise solution after each step
    mutable F*        m_pa;        // Alt solution space (Host | CUDA mem)
    CUDAEnv::Sched1D* m_cudaSch;   // Schedule or CUDA threads   (or NULL)

    // Default ctor is hidden:
    Grid2D_ChapmanKolmogorov();

    //-----------------------------------------------------------------------//
    // "InitInduct":                                                         //
    //-----------------------------------------------------------------------//
    // Implementation of the corresp abstract method from "Grid2D". Currently
    // nothing to do:
    //
    void InitInduct
    (
      bool                        UNUSED_PARAM(isFwd),
      typename Grid2D<F>::DRType  UNUSED_PARAM(DR),
      DateTime                    UNUSED_PARAM(t_from),
      BoundConds2D<F> const&      UNUSED_PARAM(bcs)
    )
    const override {}

    //-----------------------------------------------------------------------//
    // "DoOneStep":                                                          //
    //-----------------------------------------------------------------------//
    // Implementation of the corresp abstract method from "Grid2D":
    //
    F* DoOneStep
    (
      bool                        isFwd,
      typename Grid2D<F>::DRType  DR,
      int                         step_k,
      DateTime                    t_curr,
      F*                          p,
      DateTime                    t_next,
      TradingCalendar::CalReg     reg,
      BoundConds2D<F> const&      bcs
    )
    const override;

    //-----------------------------------------------------------------------//
    // De-Configured Methods:                                                //
    //-----------------------------------------------------------------------//
    // XXX: At the moment, only Fwd integration is supported;  Bwd integration
    // would require solving of Fredholm-type integral equations (???).  Hence,
    // "SolveFokkerPlanck" is perfectly OK, but "SolveFeynmanKac" is NOT;  the
    // latter is de-configured, and will cause "SolveBSM" and "ArrowDebreu" to
    // fail as well.   Of course, this breaks the "IS-A"  relationship between
    // this class and its parent ("Grid2D")...
    //
    void SolveFeynmanKac
    (
      bool                                UNUSED_PARAM(with_discount),
      DateTime                            UNUSED_PARAM(t_from),
      Matrix<F>*                          UNUSED_PARAM(p),
      DateTime                            UNUSED_PARAM(t_to),
      BoundConds2D<F> const&              UNUSED_PARAM(bcs),
      typename Grid2D<F>::CallBack const* UNUSED_PARAM(call_back),
      std::vector<DateTime> const*        UNUSED_PARAM(special_times)
    )
    const override
    {
      throw std::runtime_error
            ("Grid2D_ChapmanKolmogorov::SolveFeynmanKac: Not Applicable");
    }

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor and Dtor:                                            //
    //-----------------------------------------------------------------------//
    Grid2D_ChapmanKolmogorov
    (
      std::shared_ptr<Diffusion2D<F>> const& diff,
      typename Grid2D<F>::Params      const& params,
      bool                                   normalise
    );

    ~Grid2D_ChapmanKolmogorov();
  };
}
