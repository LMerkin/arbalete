// vim:ts=2:et
//===========================================================================//
//                      "Grid2D_ChapmanKolmogorov.hpp":                      //
//      Computation of PDFs using Transitional Densities of Diffusions       //
//                      and Chapman-Kolmogorov Equation                      //
//===========================================================================//
#pragma once

#include "Common/CUDAEnv.hpp"
#include "Diffusions/Diffusion2D_DeMulti.h"
#include "Grids/Grid2D_ChapmanKolmogorov.h"
#include "Grids/Grid2D_ChapmanKolmogorov_Core.hpp"
#include "Grids/ProbMassKern.h"
#include "Grids/Quadrature2D_Core.hpp"
#include "Grids/Normalise2D_Core.hpp"
#include <stdexcept>
#include <cassert>

namespace Arbalete
{
  using namespace std;

  //-------------------------------------------------------------------------//
  // Non-Default Ctor:                                                       //
  //-------------------------------------------------------------------------//
  template<typename F>
  Grid2D_ChapmanKolmogorov<F>::Grid2D_ChapmanKolmogorov
  (
    shared_ptr<Diffusion2D<F>> const& diff,
    typename Grid2D<F>::Params const& params,
    bool                              normalise
  )
  : Grid2D<F>  (diff, params),
    m_normalise(normalise)
  {
    // Re-write some base class flds:
    // No stencil at all ==> no explicit bound conds:
    this->m_sts = 0;

    // Allocate the alternating solution space (in Host or CUDA memory) and
    // CUDA Threads Schdule (if used):
    //
    int m  = int(this->m_Sx.size());
    int n  = int(this->m_Vx.size());
    int K  = m * n;
    assert(m >= 2 && n >= 2);

    if (!this->UseCUDA())
    {
      m_pa      = new F[K];
      m_cudaSch = nullptr;
    }
    else
    {
      m_pa      = this->m_cudaEnv->template CUDAAlloc<F>(K);
      m_cudaSch = this->m_cudaEnv->MkSched1D(K);
    }
  }

  //-------------------------------------------------------------------------//
  // Dtor:                                                                   //
  //-------------------------------------------------------------------------//
  template<typename F>
  Grid2D_ChapmanKolmogorov<F>::~Grid2D_ChapmanKolmogorov()
  {
    if (!this->UseCUDA())
    {
      delete[] m_pa;
      assert(m_cudaSch == nullptr);
    }
    else
    {
      this->m_cudaEnv->template CUDAFree<F>(m_pa);
      this->m_cudaEnv->template CUDAFree<CUDAEnv::Sched1D>(m_cudaSch);
    }
  }

  //-------------------------------------------------------------------------//
  // "DoOneStep":                                                            //
  //-------------------------------------------------------------------------//
  template<typename F>
  F* Grid2D_ChapmanKolmogorov<F>::DoOneStep
  (
    bool                        isFwd,
    typename Grid2D<F>::DRType  DEBUG_ONLY(DR),
    int                         step_k,
    DateTime                    t_curr,
    F*                          p,
    DateTime                    t_next,
    TradingCalendar::CalReg     reg,
    BoundConds2D<F> const&      UNUSED_PARAM(bcs)
  )
  const
  {
    // XXX: Bwd integration is currently unsupported (would require solving
    // Fredholm-type integral equations???).
    if (!isFwd)
      throw invalid_argument
        ("Grid2D_ChapmanKolmogorov::DoOneStep: Bwd mode is not implemented");

    // Fwd mode requires NoDR:
    assert(p != nullptr && DR == Grid2D<F>::NoDR && step_k >= 0);

    // Axes:
    Vector<F> const& Sx = this->GetSx();
    Vector<F> const& Vx = this->GetVx();
    int              m  = int(Sx.size());
    int              n  = int(Vx.size());
    int              K  = m * n;
    F                S0 = this->GetS0();
    F                V0 = this->GetV0();

    // Curr time in Years and Time step:
    F  ty0 = YF<F>(this->StartTime(), t_curr);
    F  dty = YF<F>(t_curr,            t_next);

    // Alternate the solution spaces between the caller-provided "p" (starting
    // at k=0) and the internal "m_pa":
    //
    F const* pSrc = (step_k % 2 == 0) ? p    : m_pa;
    F* pTrg       = (step_k % 2 == 0) ? m_pa : p;

    //-----------------------------------------------------------------------//
    // Diffusion De-Multiplexor:                                             //
    //-----------------------------------------------------------------------//
    // XXX: Currently, there is no generic copying mechanism for Diffusion objs
    // into the CUDA memory (problems occur with the VFT), so have to do it ex-
    // plicitly:
    //
    Diffusion2D<F> const* diff = this->m_diff.get();

    // NB: Do both static and dynamic checks for CUDA availability, to avoid
    // link errros:
    //
    if (!this->UseCUDA())
      DIFFUSION2D_DEMULTIPLEXOR(F, diff, ChapmanKolmogorov2D_Core,
        EMPTY_MACRO_ARG,     nullptr,
        step_k, m, &(Sx[0]), S0, n, &(Vx[0]), V0, ty0, dty, reg, pSrc, pTrg)
    else
    {
      // Start the CUDA section:
      this->m_cudaEnv->Start();

      DIFFUSION2D_DEMULTIPLEXOR(F, diff, ChapmanKolmogorov2D_KernelInvocator,
        ::Run,   *(this->m_cudaEnv),   m_cudaSch,
        step_k, m, this->m_cudaSx, S0, n, this->m_cudaVx, V0,  ty0, dty, reg,
        pSrc, pTrg)

      // Stop the CUDA section and timer:
      this->m_cudaEnv->Stop();
    }
    //-----------------------------------------------------------------------//
    // Normalise "pTrg" if required:                                         //
    //-----------------------------------------------------------------------//
    if (m_normalise)
    {
      // Compute the probability mass:
      //
      F pMass =
        this->template Quadrature2D<ProbMassKern<F>>(ProbMassKern<F>(), pTrg);

      if (pMass <= F(0.0))
        throw runtime_error("Grid2D_ChapmanKolmogorov::DoOneStep: "
                            "Non-positive probability mass");

      // Normalise the result; NB: "K" is consistent with "m_cudaSch":
      //
      if (!this->UseCUDA())
        Normalise2D_Core<F>(nullptr, K, pMass, pTrg);
      else
        Normalise2D_KernelInvocator<F>::Run
          (*(this->m_cudaEnv), m_cudaSch, K, pMass, pTrg);
    }

    // All done. In the CUDA mode, do NOT  copy the results back to Host memory
    // here: the User Call-Back would do that if required. NB: The result is in
    // "pTrg" (not always in "p"):
    return pTrg;
  }
}
