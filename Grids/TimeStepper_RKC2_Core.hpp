// vim:ts=2:et
//===========================================================================//
//                       "TimeStepper_RKC2_Core.hpp":                        //
//     2nd-Order Runge-Kutta-Chebyshev Time Propagation Method for ODEs      //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Common/CUDAEnv.hpp"
#include <cassert>

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "SubStepRKC2_Core":                                                     //
  //=========================================================================//
  // NB: "static" is essential here -- otherwise Host code could get called
  // instead of CUDA code!
  //
  template<typename F, bool IsInit>
  GLOBAL static void SubStepRKC2_Core
  (
    int K,       CUDAEnv::Sched1D const*   CUDACC_OR_DEBUG(sched),
    F const* p0, F const* F0, F const* F1, F const* Ym2, F const* Ym1, F*  Ym0,
    F       cP0, F       cF0, F       cF1, F       cYm2, F       cYm1
  )
  {
#   ifdef __CUDACC__
    // The order number of this thread (across all blocks):
    int Tn  = blockDim.x * blockIdx.x + threadIdx.x;
    assert(sched != nullptr);

    int lFrom = sched[Tn].m_from;
    int lTo   = sched[Tn].m_to;
    if (lFrom > lTo)
      // This thread has nothing to do: XXX: inefficiency. Can exit this thread
      // now as there is no post-processing after the "l" loop:
      return;
#   else
    // Host-based computation: Full range of indices:
    assert(sched == nullptr);
    int lFrom = 0;
    int lTo   = K-1;
#   endif

    assert(0 <= lFrom && lFrom <= lTo && lTo <= K-1);
    if (IsInit)
    {
      // "F1", "Ym2", "Ym1" and the corresp coeffs are ignored:
      assert( F1 == nullptr &&  Ym2 == nullptr &&  Ym1 == nullptr &&
             cF1 == F(0.0)  && cYm2 == F(0.0)  && cYm1 == F(0.0)  &&
             cP0 == F(1.0));
      for (int l = lFrom; l <= lTo; ++l)
        Ym0[l] = cF0 * F0[l] + p0[l];
    }
    else
      // Generic Case: Propagate the selected range of indices:
      for (int l = lFrom; l <= lTo; ++l)
        Ym0[l] = cP0  * p0 [l] + cF0  * F0 [l] + cF1 * F1[l] +
                 cYm2 * Ym2[l] + cYm1 * Ym1[l];
  }

  //=========================================================================//
  // "RKC2_KernelInvocator":                                                 //
  //=========================================================================//
  template<typename F, bool IsInit>
  struct RKC2_KernelInvocator
  {
    static void Run
    (
      CUDAEnv const& cudaEnv,   int K,   CUDAEnv::Sched1D const* sched,
      F const* p0, F const* F0, F const* F1, F const* Ym2,  F const* Ym1,
      F*      Ym0,
      F       cP0, F       cF0, F       cF1, F       cYm2,  F       cYm1
    )
#   ifdef __CUDACC__
    {
      // Set the CUDA device:
      CUDAEnv::Lock();
      cudaEnv.SetCUDADevice();

      // Invoke the kernel:
      SubStepRKC2_Core<F, IsInit>
        <<<cudaEnv.NThreadBlocks(), cudaEnv.ThreadBlockSize(), 0,
           cudaEnv.Stream()
        >>>
        (K, sched, p0, F0, F1, Ym2, Ym1, Ym0, cP0, cF0, cF1, cYm2, cYm1);

      CUDAEnv::Unlock();
      CUDA_CHECK_STATUS("RKC2 Kernel invocation failed")
    }
#   else
    ; // Spec only
#   endif
  };
}
