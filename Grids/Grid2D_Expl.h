// vim:ts=2:et
//===========================================================================//
//                              "Grid2D_Expl.h":                             //
//                 2D PDE Grids with Explicit Time Propagation               //
//===========================================================================//
#pragma once

#include "Grids/Grid2D.h"
#include "Grids/DataExpl.h"
#include "Grids/TimeStepper.h"
#include <string>

namespace Arbalete
{
  //=========================================================================//
  // "Grid2D_Expl":                                                          //
  //=========================================================================//
  // M = StencilSize / 2:
  // M = 1 for 3-Point Stencil;
  // M = 2 for 5-Point Stencil:
  //
  template<typename F, int M>
  class Grid2D_Expl final: public Grid2D<F>
  {
  private:
    //-----------------------------------------------------------------------//
    // Grid Params and Tmp Data:                                             //
    //-----------------------------------------------------------------------//
    TimeStepper<F> const* m_stepper;  // Time Stepper (Propagator)

    // RHS of the ODE system resulting from the Method of Lines:
    mutable typename TimeStepper<F>::RHS  m_odeRHS;

    mutable DataExpl<F,M> m_HD;       // Host Data (scratch)
    mutable DataExpl<F,M> m_CD;       // CUDA Data (scratch)

    CUDAEnv::Sched1D* m_cudaSch;      // CUDA threads schedule (NULL for Host)

    // Default Ctor is hidden:
    Grid2D_Expl();

    //-----------------------------------------------------------------------//
    // d/dt for both Fwd and Bwd PDEs:                                       //
    //-----------------------------------------------------------------------//
    // Common templated (for efficiency) method:
    // NB: No dependency on boundary conds!
    //
    template<bool IsFwd>
    void DDt
    (
      typename Grid2D<F>::DRType  DR,
      DateTime                    t,
      TradingCalendar::CalReg     reg,
      F*                          p,
      F*                          DpDt,
      BoundConds2D<F> const&      bcs
    )
    const;

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor, Access and Dtor:                                    //
    //-----------------------------------------------------------------------//
    // NB: The name of the Time Stepper is given by "params.m_method";
    // Currently accepted values are as in "TimeStepper_Fact.h".
    // Currently accepted values for "params.m_sts":
    // (*) 3 (spatial stencil is 3*3 points)
    // (*) 5 (spatial stencil is 5*5 points)
    //
    Grid2D_Expl(std::shared_ptr<Diffusion2D<F>> const& diff,
                         typename Grid2D<F>::Params      const& params);

    // Dtor:
    ~Grid2D_Expl();

  private:
    //-----------------------------------------------------------------------//
    // "InitInduct":                                                         //
    //-----------------------------------------------------------------------//
    // Implementation of the corresp abstract method from "Grid2D":
    //
    void InitInduct
    (
      bool                        isFwd,
      typename Grid2D<F>::DRType  DR,
      DateTime                    t_from,
      BoundConds2D<F> const&      bcs
    )
    const override;

    //-----------------------------------------------------------------------//
    // "DoOneStep":                                                          //
    //-----------------------------------------------------------------------//
    // Implementation of the corresp abstract method from "Grid2D":
    //
    F* DoOneStep
    (
      bool                        isFwd,
      typename Grid2D<F>::DRType  DR,
      int                         step_k,
      DateTime                    t_curr,
      F*                          p,
      DateTime                    t_next,
      TradingCalendar::CalReg     reg,
      BoundConds2D<F> const&      bcs
    )
    const override;
  };
}
