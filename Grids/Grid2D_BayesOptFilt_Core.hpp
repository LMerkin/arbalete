// vim:ts=2:et
//===========================================================================//
//                      "Grid2D_BayesOptFilt_Core.hpp":                      //
//                 Grid Core for Bayesian Optimal Filtering                  //
//===========================================================================//
#pragma once

#include "Common/TradingCalendar.h"
#include "Common/CUDAEnv.hpp"
#include <cassert>

namespace Arbalete
{
  using namespace std;

# ifdef  __CUDACC__
  extern __shared__ char BOF2D_ShRes[];

  // This is actually an array of "BOF2D_ThrRes" structs:
  template<typename F>
  struct BOF2D_ThrRes
  {
    int m_j;
    F   m_sumJ;
  };
# endif // __CUDACC__

  //=========================================================================//
  // "BayesOptFilt2D_Core1":                                                 //
  //=========================================================================//
  // Phase 1 (no normalisation / maximisation of "p") of Bayesian Optimal Fil-
  // tering:
  // NB: "diff" must be passed by copy (for Host->CUDA calls).
  // NB: "static" is essential here -- otherwise Host code could get called in-
  // stead of CUDA code!
  // This function computes an integral of the density p0(SY0,V[j0]) with the
  //    transition density diff.TransPDF(SY0,V[j0],SY,V[j]) to get the new den-
  //    sity p(SY,V[j]). Both densities are 1D (wrt V only: j0, j in [0..n-1]),
  //    "SY0" and "SY" are fixed (observed vals).
  // CUDA Threads scheduling: "Sched2D":
  // -- each Block processes a number of "j"s;
  // -- each "j" is processed by the Threads of just one Block (i.e. not shared
  //    between the Blocks, so each "j" can be synchronised);
  // -- each Thread processes a number of "j0"s within one "j";
  // -- again, each "j0" is processed by just one Thread (within a Block):
  //
  template<typename F, typename Diff>
  GLOBAL static void BayesOptFilt2D_Core1
  (
    Diff diff, CUDAEnv::Sched2D const* CUDACC_OR_DEBUG(sched),
    F SY0,     F ty0,       F SY, F dty,
    TradingCalendar::CalReg reg,
    int n,     F const* Vx, F const* p0, F* p
  )
  {
    // NB: Recursion base for "p0" is done BY THE CALLER. Here we deal with the
    // GENERIC CASE ONLY:  "p0" is fully-formed and duly normalised, i.e.:
    // p0 = p(V_{i-1} | y_{0:(i-1)}) :
    //
    assert(n >= 2 && p0 != nullptr && p != nullptr && dty > F(0.0));

    F hV = Vx[1] - Vx[0];
    assert(hV > F(0.0));

#   ifdef __CUDACC__
    // The order number of this thread (across all blocks):
    int Tn = blockDim.x * blockIdx.x + threadIdx.x;
    assert(sched != nullptr);

    CUDAEnv::Sched2D schn = sched[Tn];
    int j  = schn.m_outer;
    if (j < 0)
      // This Thread (and the whole Block it belongs to) is not in use:
      return;
    assert(j < n);
#   else
    // Host-based computations: Do full range of "j" indices, with OpenMP
    // parallelisation (XXX: not on Mac OS X yet):
    assert(sched == nullptr);

#   ifndef __clang__
#   pragma omp parallel for
#   endif
    for (int j = 0; j < n; ++j)
    {
#endif  //  __CUDACC__
#     ifdef __CUDACC__
      int j0From = schn.m_innerFrom + 1;
      int j0To   = schn.m_innerTo   + 1;

      // NB: "j0" indices are 1-based (and still < n).     It is possible that
      // j0From > j0To, so the curr thread will have nothing to do in the loop
      // below. However, do NOT exit such threads now: they may be needed  for
      // for post-processing!
      assert(1 <= j0From && j0To <= n-1);
#     else
      // Host mode: Do the full range of "j0" indices:  NB: starting from 1,
      // not 0 -- consistent with the Rectangular Integration Rule:
      int j0From = 1;
      int j0To   = n-1;
#     endif

      F Vj   = Vx[j];
      F sumJ = F(0.0);  // Partial sum for the curr "j", over the "j0" below:

      for (int j0 = j0From; j0 <= j0To; ++j0)
        // Compute the transition density and integrate it over "p0":
        sumJ += diff.TransPDF(SY0, Vx[j0], ty0, SY, Vj, dty, reg) * p0[j0];

      sumJ *= hV;       // NB!!!

#     ifdef __CUDACC__
      // Save (j,sumJ) in a Thread-specific shared memory area (within the curr
      // Block):
      BOF2D_ThrRes<F>* resB = (BOF2D_ThrRes<F>*)(BOF2D_ShRes);
      BOF2D_ThrRes<F>* resT = resB + threadIdx.x;
      resT->m_j    = j;
      resT->m_sumJ = sumJ;
#     else
      // Host mode -- update the result directly:
      p[j] = sumJ;
    }
#     endif

#   ifdef __CUDACC__
    // CUDA mode: Do post-processing by the 0th Thread of the curr Block:
    // Compute full "p" vals for all "j"s processed by this Block, and save the
    // results:
    __syncthreads();

    if (threadIdx.x != 0)
      return;

    // The initial "j" index of the Block is the one processed by Thread 0 of
    // that Block:
    int jCurr = resB[0].m_j;
    p[jCurr]  = resB[0].m_sumJ;
    assert(0 <= jCurr && jCurr < n);

    // Go through all other Threads of the Block. Compute "p" for the "j"s en-
    // countered:
    for (int tid = 1; tid < blockDim.x; ++tid)
    {
      // Which "j" was processed by this Thread? Is it a new one?
      int jNext  = resB[tid].m_j;
      if (jNext != jCurr)
      {
        // Starting a new "j" (must be monotonic):
        assert(1 <= jNext && jNext == jCurr + 1 && jNext < n);
        jCurr    =  jNext;
        p[jCurr] =  resB[tid].m_sumJ;
      }
      else
        // Update the partial sum for "jCurr":
        p[jCurr]+= resB[tid].m_sumJ;
    }
#   endif // __CUDACC__

    // Now p = p(y_i, V_i | y_{0:(i-1)}) -- ready for maximisation!
  }

  //=========================================================================//
  // "BayesOptFilt2D_Core2":                                                 //
  //=========================================================================//
  // Phase 2 of Bayesian Optimal Filtering:  Maximisation of "p" and normalisa-
  // tion (to make it ready for subsequent iterations):
  // NB: In the CUDA mode, this is done by just one Thread (and only 1 Block is
  // launched):
  //
  template<typename F>
  GLOBAL static void BayesOptFilt2D_Core2(int n, F const* Vx, F* p, F* opt)
  {
    assert(n >= 2 && p != nullptr && Vx != nullptr);

#   ifdef __CUDACC__
    assert(blockIdx.x == 0);
    if (threadIdx.x != 0)
      return;
#   endif

    // Compute "N" (the normalising factor -- integral of "p"). XXX: for nume-
    // rical stability, still apply Dirichlet-type bound conds:
    p[0]   = F(0.0);
    p[n-1] = F(0.0);

    F N  = F(0.0);
    F hV = Vx[1] - Vx[0];
    assert(hV > F(0.0));

    for (int j = 1; j <= n-2; ++j)
      N += p[j];

    N *= hV;
    assert(N > F(0.0));

    // Now do maximisation of "p" and normalisation (in one run, but maximisa-
    // tion is always done FIRST):
    //
    int je = 0;         // Index of the Estimated (Optimal) "V"
    F   pe = F(0.0);    // The Max Density

    for (int j = 0; j < n; ++j)
    {
      if (p[j] > pe)    // Maximisation
      {
        je = j;
        pe = p[j];
      }
      p[j] /= N;        // Normalisation
    }
    // Now p = p(V_i | y_{0:i}), ready for the next iteration!

    // Store the result:
    opt[0] = Vx[je];
    opt[1] = pe;
    opt[2] = N;
  }

  //=========================================================================//
  // "BayesOptFilt2D_KernelsInvocator":                                      //
  //=========================================================================//
  // NB: This is a Host-invoked function, so it's OK to pass "diff" by ref.
  // Invokes 2 CUDA kernels sequentially:
  //
  template<typename F, typename Diff>
  struct BayesOptFilt2D_KernelsInvocator
  {
    static void Run
    (
      Diff    const& diff,
      CUDAEnv const& cudaEnv,  CUDAEnv::Sched2D const* sched,
      F SY0,  F ty0,       F SY,        F dty,  TradingCalendar::CalReg reg,
      int n,  F const* Vx, F const* p0, F* p,   F* opt
    )
#   ifdef __CUDACC__
    {
      // Set the shared memory size for Phase 1:
      int ShMemSz = int(cudaEnv.ThreadBlockSize() * sizeof(BOF2D_ThrRes<F>));

      // Check for excessively-large "ShMemSz" values -- this would result in
      // major inefficiencies:
      if (ShMemSz * cudaEnv.ThreadBlocksPerSM() > cudaEnv.ShMemPerSM())
        throw invalid_argument("BayesOptFilt2D Kernel: ShMemSz too large");

      // Set the CUDA device:
      CUDAEnv::Lock();
      cudaEnv.SetCUDADevice();

      // Invoke the kernel for Phase 1:
      BayesOptFilt2D_Core1<F, Diff>
        <<<cudaEnv.NThreadBlocks(), cudaEnv.ThreadBlockSize(), ShMemSz,
           cudaEnv.Stream()
        >>>
        (diff, sched, SY0, ty0, SY, dty, reg, n, Vx, p0, p);

      if (cudaGetLastError() == cudaSuccess)
        // Invoke the kernel for Phase 2: Only 1 Block of 32 Threads:
        BayesOptFilt2D_Core2<F><<<1, 32, 0, cudaEnv.Stream()>>>
          (n, Vx, p, opt);

      CUDAEnv::Unlock();
      CUDA_CHECK_STATUS("BayesOptFilt2D Kernel invocation failed")
    }
#   else
    ; // Spec only
#   endif
  };
}
