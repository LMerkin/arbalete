// vim:ts=2:et
//===========================================================================//
//                             "Normalise2D.cu":                             //
//           Pre-Defined Instances of "Normalise2D_KernelInvocator"          //
//===========================================================================//
#include "Grids/Normalise2D_Core.hpp"

namespace Arbalete
{
  template struct Normalise2D_KernelInvocator<double>;
  template struct Normalise2D_KernelInvocator<float>;
}
