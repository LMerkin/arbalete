// vim:ts=2:et
//===========================================================================//
//                           "TimeStepper_RKF5.h":                           //
//          5th-Order Runge-Kutta-Fehlberg Time Propagation Method           //
//===========================================================================//
// For Host compilation only:
//
#pragma once

#include "Common//CUDAEnv.hpp"
#include "Grids/TimeStepper_RKF5.h"
#include "Grids/TimeStepper_RKF5_Core.hpp"

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "DataRKF5":                                                             //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // Non-Default Ctor:                                                       //
  //-------------------------------------------------------------------------//
  // NB: Unlike e.g. "DataADI" and "DataExpl", "DataRKF5" is created in the
  // CUDA mode IFF the parent Grid uses CUDA (whereas "DataADI" and "DataExpl"
  // may be required in BOTH Host and CUDA modes).
  // In the following, the "cudaEnv" ptr  may be NULL:
  //
  template<typename F>
  DataRKF5<F>::DataRKF5(int K, CUDAEnv const* cudaEnv)
  : m_K(K)
  {
    assert(m_K >= 1);

    // Allocate the scatch matrices:
    for (int k  = 0; k < 6; ++k)
      if (cudaEnv == nullptr)
        // Host allocations:
        m_MK[k] = new F[m_K];
      else
        // CUDA allocations:
        m_MK[k] = cudaEnv->CUDAAlloc<F>(m_K);
  }

  //-------------------------------------------------------------------------//
  // Explicit Dtor:                                                          //
  //-------------------------------------------------------------------------//
  // Also requires a "cudaEnv" ptr (possibly NULL):
  //
  template<typename F>
  void DataRKF5<F>::Destroy(CUDAEnv const* cudaEnv)
  {
    for (int k = 0; k < 6; ++k)
      if (cudaEnv == nullptr)
        // Host mode:
        delete[] m_MK[k];
      else
      {
        // CUDA mode:
        cudaEnv->Stop();  // Just in case...
        cudaEnv->CUDAFree<F>(m_MK[k]);
      }
  }

  //=========================================================================//
  // "TimeStepper_RKF5":                                                     //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // Non-Default Ctor:                                                       //
  //-------------------------------------------------------------------------//
  template<typename F>
  TimeStepper_RKF5<F>::TimeStepper_RKF5
  (
    int K, shared_ptr<CUDAEnv> const& cudaEnv
  )
  : TimeStepper<F>(K, cudaEnv),
    m_data(K, cudaEnv.get())
  {
    // Also need to allocate "m_Y" which is separate from "m_data":
    m_Y = (!this->UseCUDA()) ? new F[K] : cudaEnv->CUDAAlloc<F>(K);
  }

  //-------------------------------------------------------------------------//
  // Dtor:                                                                   //
  //-------------------------------------------------------------------------//
  template<typename F>
  TimeStepper_RKF5<F>::~TimeStepper_RKF5()
  {
    m_data.Destroy(this->m_cudaEnv.get());

    if (!this->UseCUDA())
      delete[] m_Y;
    else
    {
      this->m_cudaEnv->Stop();  // Just in case...
      this->m_cudaEnv->template CUDAFree<F>(m_Y);
    }
  }

  //=========================================================================//
  // "Propagate":                                                            //
  //=========================================================================//
  template<typename F>
  void TimeStepper_RKF5<F>::Propagate
  (
    DateTime                t_from,
    TradingCalendar::CalReg reg,
    F*                      p,
    DateTime                t_to,
    typename TimeStepper<F>::RHS const& rhs
  )
  const
  {
    //-----------------------------------------------------------------------//
    // Init and MK[0]:                                                       //
    //-----------------------------------------------------------------------//
    assert(p != nullptr);

    typename DataRKF5<F>::F6 &  c = m_data.c ();
    typename DataRKF5<F>::FP6& MK = m_data.MK();

    F tau = YF<F>(t_from, t_to);  // NB: "tau" may be negative, but not 0:
    assert(tau != F(0.0));

    DateTime  t = t_from;
    rhs(t, reg, p, MK[0]);

    //-----------------------------------------------------------------------//
    // MK[1]:                                                                //
    //-----------------------------------------------------------------------//
    c[0] = F(0.25);

    // NB: Do both static and dynamic checks for CUDA availability, to avoid
    // link errors:
    //
    if (!this->UseCUDA())
      SubStepRKF5_Core<F>(nullptr, 1, p, m_data, tau, m_Y);
    else
    {
      this->m_cudaEnv->Start();
      RKF5_KernelInvocator<F>::Run
        (*(this->m_cudaEnv), this->m_cudaSch, 1, p, m_data, tau, m_Y);
      this->m_cudaEnv->Stop();
    }
    t = t_from + TimeOfYF<F>(F(0.25) * tau);
    rhs(t, reg, m_Y, MK[1]);

    //-----------------------------------------------------------------------//
    // MK[2]:                                                                //
    //-----------------------------------------------------------------------//
    c[0] = F(0.09375);
    c[1] = F(0.28125);

    // NB: Do both static and dynamic checks for CUDA availability, to avoid
    // link errors:
    //
    if (!this->UseCUDA())
      SubStepRKF5_Core<F>(nullptr, 2, p, m_data, tau, m_Y);
    else
    {
      this->m_cudaEnv->Start();
      RKF5_KernelInvocator<F>::Run
        (*(this->m_cudaEnv), this->m_cudaSch, 2, p, m_data, tau, m_Y);
      this->m_cudaEnv->Stop();
    }
    t = t_from + TimeOfYF<F>(F(0.375) * tau);
    rhs(t, reg, m_Y, MK[2]);

    //-----------------------------------------------------------------------//
    // MK[3]:                                                                //
    //-----------------------------------------------------------------------//
    c[0] =   F(1932.0 / 2197.0);
    c[1] = - F(7200.0 / 2197.0);
    c[2] =   F(7296.0 / 2197.0);

    // NB: Do both static and dynamic checks for CUDA availability, to avoid
    // link errors:
    //
    if (!this->UseCUDA())
      SubStepRKF5_Core<F>(nullptr, 3, p, m_data, tau, m_Y);
    else
    {
      this->m_cudaEnv->Start();
      RKF5_KernelInvocator<F>::Run
        (*(this->m_cudaEnv), this->m_cudaSch, 3, p, m_data, tau, m_Y);
      this->m_cudaEnv->Stop();
    }
    t = t_from + TimeOfYF<F>(F(12.0 / 13.0) * tau);
    rhs(t, reg, m_Y, MK[3]);

    //-----------------------------------------------------------------------//
    // MK[4]:                                                                //
    //-----------------------------------------------------------------------//
    c[0] =   F( 439.0 /  216.0);
    c[1] = - F(   8.0);
    c[2] =   F(3680.0 /  513.0);
    c[3] = - F( 845.0 / 4104.0);

    // NB: Do both static and dynamic checks for CUDA availability, to avoid
    // link errors:
    //
    if (!this->UseCUDA())
      SubStepRKF5_Core<F>(nullptr, 4, p, m_data, tau, m_Y);
    else
    {
      this->m_cudaEnv->Start();
      RKF5_KernelInvocator<F>::Run
        (*(this->m_cudaEnv), this->m_cudaSch, 4, p, m_data, tau, m_Y);
      this->m_cudaEnv->Stop();
    }
    t = t_to;
    rhs(t, reg, m_Y, MK[4]);

    //-----------------------------------------------------------------------//
    // MK[5]:                                                                //
    //-----------------------------------------------------------------------//
    c[0] = - F(   8.0 /   27.0);
    c[1] =   F(   2.0);
    c[2] = - F(3544.0 / 2565.0);
    c[3] =   F(1859.0 / 4104.0);
    c[4] = - F(   0.275);

    // NB: Do both static and dynamic checks for CUDA availability, to avoid
    // link errors:
    //
    if (!this->UseCUDA())
      SubStepRKF5_Core<F>(nullptr, 5, p, m_data, tau, m_Y);
    else
    {
      this->m_cudaEnv->Start();
      RKF5_KernelInvocator<F>::Run
        (*(this->m_cudaEnv), this->m_cudaSch, 5, p, m_data, tau, m_Y);
      this->m_cudaEnv->Stop();
    }
    t = t_from + TimeOfYF<F>(F(0.5) * tau);
    rhs(t, reg, m_Y, MK[5]);

    //-----------------------------------------------------------------------//
    // 5th-order solution approximation is written back into "p":            //
    //-----------------------------------------------------------------------//
    c[0] =   F(   16.0 /   135.0);
    c[1] =   F(    0.0);
    c[2] =   F( 6656.0 / 12825.0);
    c[3] =   F(28561.0 / 56430.0);
    c[4] = - F(    0.18);
    c[5] =   F(    2.0 /    55.0);

    // NB: Do both static and dynamic checks for CUDA availability, to avoid
    // link errors:
    //
    if (!this->UseCUDA())
      SubStepRKF5_Core<F>(nullptr, 6, p, m_data, tau, p);
    else
    {
      this->m_cudaEnv->Start();
      RKF5_KernelInvocator<F>::Run
        (*(this->m_cudaEnv), this->m_cudaSch, 6, p, m_data, tau, p);
      this->m_cudaEnv->Stop();
    }
  }
}
