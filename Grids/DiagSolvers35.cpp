// vim:ts=2:et
//===========================================================================//
//                           "DiagSolvesr35.cpp":                            //
//        Pre-Defined Instances of 3- and 5-Diagonal Linear Solvers          //
//===========================================================================//
#include "Grids/DiagSolvers35.hpp"

namespace Arbalete
{
  template struct DiagSolver<double, 1>;  // 3-diag
  template struct DiagSolver<double, 2>;  // 5-diag

  template struct DiagSolver<float,  1>;  // 3-diag
  template struct DiagSolver<float,  2>;  // 5-diag
}
