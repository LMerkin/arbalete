// vim:ts=2:et
//===========================================================================//
//                           "TimeStepper_RKC1.h":                           //
//          1st-Order Runge-Kutta-Chebyshev Time Propagation Method          //
//===========================================================================//
#pragma once

#include "Grids/TimeStepper.h"

namespace Arbalete
{
  //=========================================================================//
  // "TimeStepper_RKC1":                                                     //
  //=========================================================================//
  template<typename F>
  class TimeStepper_RKC1: public TimeStepper<F>
  {
  private:
    //-----------------------------------------------------------------------//
    // Configs & Runge-Kutta-Chebyshev coeffs:                               //
    //-----------------------------------------------------------------------//
    int m_s;          // >= 2

    // The Coeff Vectors are always allocated in Host memory:
    Vector<F> m_c;    // 0 ..  s
    Vector<F> m_u;    // 0 .. (s-1)
    Vector<F> m_mu;   // 0 .. (s-2)
    Vector<F> m_nu;   // 0 .. (s-2)

    // Scratch matrices of size (m * n) are allocated either in Host or in
    // CUDA memory:
    mutable F*  m_Y[2];
    mutable F*  m_F;

    // Default Ctor is hidden:
    TimeStepper_RKC1();

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor and Dtor:                                            //
    //-----------------------------------------------------------------------//
    // "K"  : linear size of the solution space (K >= 1)
    // "s"  : stability parameter (s >= 2)
    // "eps": damping factor:
    //
    TimeStepper_RKC1
    (
      int K,
      int s,
      std::shared_ptr<CUDAEnv> const& cudaEnv,
      F eps = F(0.05)
    );

    ~TimeStepper_RKC1();

    //-----------------------------------------------------------------------//
    // "Propagate":                                                          //
    //-----------------------------------------------------------------------//
    void Propagate
    (
      DateTime                t_from,
      TradingCalendar::CalReg reg,
      F*                      p,
      DateTime                t_to,
      typename TimeStepper<F>::RHS const& rhs
    )
    const;
  };
}
