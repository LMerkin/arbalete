// vim:ts=2:et
//===========================================================================//
//                               "Grid2D_ADI.cpp":                           //
//                Explicit Instances of the "Grid2D_ADI" Class               //
//===========================================================================//
#include "Grid2D_ADI.hpp"

namespace Arbalete
{
  template class Grid2D_ADI<double, 1>;
  template class Grid2D_ADI<double, 2>;

  template class Grid2D_ADI<float,  1>;
  template class Grid2D_ADI<float,  2>;
}
