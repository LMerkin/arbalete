// vim:ts=2:et
//===========================================================================//
//                             "Grid2D_Fact.cpp":                            //
//                        Factory of Various 2D Grids                        //
//===========================================================================//
#pragma once

#include "Grids/Grid2D_Fact.h"
#include "Grids/Grid2D_Expl.hpp"
#include "Grids/Grid2D_ADI.hpp"
#include "Grids/Grid2D_Theta.hpp"
#include "Grids/Grid2D_ChapmanKolmogorov.hpp"
#include "Grids/Grid2D_BayesOptFilt.hpp"
#include <stdexcept>

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "MkGrid2D":                                                             //
  //=========================================================================//
  template<typename F>
  Grid2D<F>* MkGrid2D
  (
    shared_ptr<Diffusion2D<F>> const& diff,
    typename Grid2D<F>::Params const& params
  )
  {
    if (params.m_method == "PR-ADI" || params.m_method == "Yanenko")
      //---------------------------------------------------------------------//
      // A Fractional Step (ADI-Type) Grid                                   //
      //---------------------------------------------------------------------//
      if (params.m_sts == 3)
        return new Grid2D_ADI<F,1>  (diff, params);
      else
      if (params.m_sts == 5)
        return new Grid2D_ADI<F,2>  (diff, params);
      else
        throw invalid_argument("Grid2D_ADI: Stencil Size must be 3 or 5");
    else
    if (params.m_method == "CN" || params.m_method == "FullImpl")
      //---------------------------------------------------------------------//
      // Cranck-Nicolson or Fullt-Implicit Method:                           //
      //---------------------------------------------------------------------//
      if (params.m_sts == 3)
        return new Grid2D_Theta<F>(diff, params);
      else
        throw invalid_argument("Grid2D_Theta: Stencil Size must be 3");
    else
    if (params.m_method == "Chapman-Kolmogorov")
      //---------------------------------------------------------------------//
      // Analytical or Heat Kernel-based Transition Density:                 //
      //---------------------------------------------------------------------//
      return new Grid2D_ChapmanKolmogorov<F>(diff, params, false);
    else
    if (params.m_method == "Chapman-Kolmogorov-N")
      // As above, but with re-normalisation after each step (to prevent
      // probability mass leaks):
      return new Grid2D_ChapmanKolmogorov<F>(diff, params, true);
    else
    if (params.m_method == "BayesOptFilt")
      //---------------------------------------------------------------------//
      // As above, but 1D (Bayesian Optimal Filtering):                      //
      //---------------------------------------------------------------------//
      return new Grid2D_BayesOptFilt<F>(diff, params);
    else
      //---------------------------------------------------------------------//
      // Assume an Explicit Method:                                          //
      //---------------------------------------------------------------------//
      if (params.m_sts == 3)
        return new Grid2D_Expl<F,1> (diff, params);
      else
      if (params.m_sts == 5)
        return new Grid2D_Expl<F,2> (diff, params);
      else
        throw invalid_argument("Grid2D_Expl: Stencil Size must be 3 or 5");
  }
}
