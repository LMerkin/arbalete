// vim:ts=2:et
//===========================================================================//
//                           "ProbMassQuadr.cu":                             //
//          Pre-Defined Instances of "Quadrature2D_KernelInvocator"          //
//                           with "ProbMassKern"                             //
//===========================================================================//
#include "Grids/ProbMassKern.h"
#include "Grids/Quadrature2D_Core.hpp"

namespace Arbalete
{
  template struct Quadrature2D_KernelInvocator<double, ProbMassKern<double> >;
  template struct Quadrature2D_KernelInvocator<float,  ProbMassKern<float>  >;
}
