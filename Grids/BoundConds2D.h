// vim:ts=2:et
//===========================================================================//
//                             "BoundConds2D.h":                             //
//                   Boundary Conditions for 2D PDE Grids                    //
//===========================================================================//
#pragma once

#include "Common/Macros.h"
#ifndef  __CUDACC__
#include "Common/DateTime.h"
#include "Common/VecMatTypes.h"
#include <functional>
#endif

namespace Arbalete
{
  //=========================================================================//
  // "BCType":                                                               //
  //=========================================================================//
  enum BCType
  {
    DirichletBCT, // Const value             at boundary
    NeumannBCT    // Const normal derivative at boundary
  };

  //=========================================================================//
  // "BCPtrs2D":                                                             //
  //=========================================================================//
  // The type of evaluated Bound Conds, as they are passed to the Grids:
  // M is 1 or 2. If M=2, "0" is the outer boundary and "1" the inner boundary.
  // Ptrs can point to any memory (e.g. Host of CUDA):
  //
  template<typename F, int M>
  struct BCPtrs2D
  {
    //-----------------------------------------------------------------------//
    // Data Ptrs:                                                            //
    //-----------------------------------------------------------------------//
    int    m_m;
    int    m_n;

    // At LoS Boundary: Len=n for each array:
    BCType m_LoST;
    F*     m_LoS[M];

    // At UpS Boundary: Len=n for each array:
    BCType m_UpST;
    F*     m_UpS[M];

    // At LoV Boundary: Len=m for each array:
    BCType m_LoVT;
    F*     m_LoV[M];

    // At UpV Boundary: Len=m for each array:
    BCType m_UpVT;
    F*     m_UpV[M];

#   ifndef __CUDACC__
    //-----------------------------------------------------------------------//
    // Ctors:                                                                //
    //-----------------------------------------------------------------------//
    // NB: The ctors only set ptrs to data arrays; no actual allocation is per-
    // formed -- all arrays must be pre-allocated. No "" here -- these
    // ctors are for use in "Grid2D*" only, and not for NVCC:
    //
    // Default ctor: all ptrs will be set to NULL
    //
    BCPtrs2D(BCType bct = BCType::DirichletBCT);

    // Non-Default Ctor:
    BCPtrs2D
    (
      int m,     int n,
      F* loS[M], F* upS[M], F* loV[M], F* upV[M],
      BCType bct =  BCType::DirichletBCT
    );
#   endif
    //-----------------------------------------------------------------------//
    // Elementary Applications:                                              //
    //-----------------------------------------------------------------------//
    // Applying BCs to each boundary separately, in both Host and CUDA modes;
    // "b" is the distance to the boundary (b=0..M-1):
    //
    DEVICE inline void ApplyLoS(F* f, int b, int j) const;
    DEVICE inline void ApplyUpS(F* f, int b, int j) const;
    DEVICE inline void ApplyLoV(F* f, int i, int b) const;
    DEVICE inline void ApplyUpV(F* f, int i, int b) const;

    //-----------------------------------------------------------------------//
    // Applying BCs to all boundaries at once:                               //
    //-----------------------------------------------------------------------//
    DEVICE inline void ApplyAll(F* f) const;
  };

#ifndef __CUDACC__
  // This is for non-NVCC compilations only, since NVCC does not support C++11
  // features yet:

  //=========================================================================//
  // "BoundConds2D":                                                         //
  //=========================================================================//
  // Top-level Bound Conds specification:
  //
  template<typename F>
  struct BoundConds2D
  {
    //-----------------------------------------------------------------------//
    // Types and Functions:                                                  //
    //-----------------------------------------------------------------------//
    // Function type implementing BCs:
    typedef std::function<F(F S, F V, DateTime t)> BCFunc;

    // "ZeroBC" can be used as a default BCFunc:
    static F ZeroBCF
      (F UNUSED_PARAM(S), F UNUSED_PARAM(V), DateTime UNUSED_PARAM(t))
    { return F(0.0); }

    // At LoS Boundary:  S = Smin:
    BCType  m_LoST;
    BCFunc  m_LoSF;

    // At UpS Boundary:  S = Smax:
    BCType  m_UpST;
    BCFunc  m_UpSF;

    // At LoV Boundary:  V = Vmin:
    BCType  m_LoVT;
    BCFunc  m_LoVF;

    // At UpV Boundary:  V = Vmax:
    BCType  m_UpVT;
    BCFunc  m_UpVF;

    //-----------------------------------------------------------------------//
    // Ctors:                                                                //
    //-----------------------------------------------------------------------//
    // (1) All-0 Bound Conds, Dirichlet by default:
    //
    BoundConds2D(BCType bct = BCType::DirichletBCT)
    : m_LoST(bct),  m_LoSF(ZeroBCF),
      m_UpST(bct),  m_UpSF(ZeroBCF),
      m_LoVT(bct),  m_LoVF(ZeroBCF),
      m_UpVT(bct),  m_UpVF(ZeroBCF)
    {}

    // (2) Arbitrary Bound Conds:
    //
    BoundConds2D
    (
      BCType loST,  BCFunc const& loSF,
      BCType upST,  BCFunc const& upSF,
      BCType loVT,  BCFunc const& loVF,
      BCType upVT,  BCFunc const& upVF
    )
    : m_LoST(loST), m_LoSF(loSF),
      m_UpST(upST), m_UpSF(upSF),
      m_LoVT(loVT), m_LoVF(loVF),
      m_UpVT(upVT), m_UpVF(upVF)
    {}

    //-----------------------------------------------------------------------//
    // Accessors:                                                            //
    //-----------------------------------------------------------------------//
    BCType        GetLoST() const { return m_LoST; }
    BCFunc const& GetLoSF() const { return m_LoSF; }

    BCType        GetUpST() const { return m_UpST; }
    BCFunc const& GetUpSF() const { return m_UpSF; }

    BCType        GetLoVT() const { return m_LoVT; }
    BCFunc const& GetLoVF() const { return m_LoVF; }

    BCType        GetUpVT() const { return m_UpVT; }
    BCFunc const& GetUpVF() const { return m_UpVF; }

    //-----------------------------------------------------------------------//
    // "PreCompute":                                                         //
    //-----------------------------------------------------------------------//
    // Fills in {Low,Up}{S,V} arrays which are later used with indirect form of
    // "ApplyBoundConds". Array sizes are as above. All arrays in "res" must be
    // pre-allocated:
    // NB: Not "" -- for use in "Grid2D*" only:
    //
    template<int M>
    void PreCompute
      (int m, F const* S, int n, F const* V, DateTime t, BCPtrs2D<F,M>* res)
    const;

    //-----------------------------------------------------------------------//
    // "ApplyDirect":                                                        //
    //-----------------------------------------------------------------------//
    // Compute bound conds and apply them directly to "f". "M" is the width of
    // the boundary zone (1 or 2).
    //
    // NB: We currently do NOT verify that the boundary conds are consistent in
    // the grid corners; if not, the "S" conds prevail over the "V" ones. M>=1
    // is the "boundary layer depth". If "bcs" is NULL, the conds are all-0:
    //
    template<int M>
    void ApplyDirect
      (int m, F const* S, int n, F const* V, DateTime t, F* f)
    const;
  };
# endif // !__CUDACC__
}
