// vim:ts=2:et
//===========================================================================//
//                              "ProbMassKern.h":                            //
//              Simple Kernel for the "Quadrature2D" Integrator:             //
//                        Probability Mass Computation                       //
//===========================================================================//
#pragma once

#include "Common/Macros.h"

namespace Arbalete
{
  //-------------------------------------------------------------------------//
  // "ProbMassKern": Always returns 1:                                       //
  //-------------------------------------------------------------------------//
  template<typename F>
  struct ProbMassKern
  {
    // This kernel is not parameterised:
    DEVICE int      GetNParams() const            { return 0;       }
    DEVICE F const* GetParams () const            { return nullptr; }

    DEVICE F operator()
    (
      int UNUSED_PARAM(i),
      int UNUSED_PARAM(j),
      F   UNUSED_PARAM(x)
    )
    const { return F(1.0); }
  };
}
