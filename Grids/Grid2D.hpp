// vim:ts=2:et
//===========================================================================//
//                              "Grid2D.hpp":                                //
//     Generic Integrators for Fokker-Planck, Feynman-Kac and BSM PDEs       //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Common/DateTime.h"
#include "Common/TradingCalendar.h"
#include "Common/CUDAEnv.hpp"
#include "Grids/Grid2D.h"
#include "Grids/ProbMassKern.h"
#include "Grids/Quadrature2D_Core.hpp"
#include <cstdio>
#include <algorithm>

namespace
{
  using namespace Arbalete;
  using namespace std;

  //=========================================================================//
  // "MkAxis":                                                               //
  //=========================================================================//
  // Xmin:    estimate for the min X value  (always honoured)
  // Xmax:    estimate for the max X value  (honoured if "XmaxFixed" is set)
  // X0:      initial X value (must lie directly on the grid)
  // np:      number of grid points (may be updated);
  // n0:      position of X0 on the grid  [0 .. np-1]; if n0 is NULL,  X0 is
  //          NOT required to lie on the Grid;
  // Xaxis:   the actual axis constructed:
  //
  template<typename F>
  inline void MkAxis
  (
    string const& Xname, bool X0OnGrid,    F Xmin, F Xmax, F X0, bool XmaxFixed,
    int* np,             Vector<F>* Xaxis, int* n0
  )
  {
    assert(np != nullptr && Xaxis != nullptr && n0 != nullptr);

    if (*np < 2 || Xmin >= Xmax)
      throw invalid_argument
            ("MkAxis: Invalid "+ Xname +" axis geometry: [" + to_string(Xmin) +
             " .. "+  to_string(Xmax));

    // Initial step size (may be adjusted later):
    F h = (Xmax - Xmin) / F(*np - 1);

    //-----------------------------------------------------------------------//
    // "X0" is to be put in the Grid:                                        //
    //-----------------------------------------------------------------------//
    if (X0OnGrid)
    {
      // For that, "X0" at least needs to be in the [Xmin .. Xmax] range:
      if (X0 < Xmin || X0 > Xmax)
        throw invalid_argument
              ("MkAxis: "+ Xname +"0 is out of [Min..Max] range");

      if (!XmaxFixed)
      {
        //-------------------------------------------------------------------//
        // "Xmax" is adjustable:                                             //
        //-------------------------------------------------------------------//
        // Adjust "h" (and then "Xmax") to place X0 on the grid:
        *n0 = int(Ceil((X0 - Xmin) / h));

        if (X0 != Xmin)
          h = (X0 - Xmin) / F(*n0);
        assert(h > F(0.0));

        // Do NOT resize "Xaxis" and/or change "np" if "Xaxis" already contains
        // exactly "np" points:
        if (int(Xaxis->size()) != *np)
        {
          *np = int(Ceil((Xmax - Xmin) / h)) + 1;
          Xaxis->resize(*np);
        }
#       ifndef NDEBUG
        // NB: "Xmin" is fixed but "Xmax" may be adjusted:
        Xmax = Xmin + h * F(*np - 1);
        assert(Xmin < Xmax);
#       endif
      }
      else
      {
        //-------------------------------------------------------------------//
        // "Xmax" (and "Xmin") are not adjustable:                           //
        //-------------------------------------------------------------------//
        // Then the only param which can be altered in order to get "X0" on the
        // grid is "np". Thus, (X0-Xmin)/(Xmax-Xmin) must be a rational number.
        // In general, it can be found my expanding this fraction into continu-
        // ed fractions. This mode will be used, e.g., for Barrier Options:
        //
        if (X0 == Xmin)
          *n0 = 0;
        else
        if (X0 == Xmax)
          *n0 = (*np)-1;
        else
        {
          F   x   = (X0 - Xmin) / (Xmax - Xmin);  // Approximated as (hm0/km0)
          int hm0, km0;
          assert(F(0.0) < x && x < F(1.0));

          int hm2 = 0;
          int hm1 = 1;
          int km2 = 1;
          int km1 = 0;
          int a   = 0;
          while (true)
          {
            hm0 = a * hm1 + hm2;
            km0 = a * km1 + km2;
            assert(hm0 >= 0 && km0 >= 1 && hm0 < km0);
  
            if (hm0 >= 2*(*np) || km0 >= 2*(*np))
              // Approximation would require too many points:
              throw runtime_error
                    ("MkAxis: Cannot position the Init Cond on the Grid");

            if (x == F(0.0))
              break;

            // Next iteration:
            x = F(1.0) / x;
            a = int(Floor(x));
            x -= F(a);

            hm2 = hm1;  hm1 = hm0;
            km2 = km1;  km1 = km0;
          }
          // "~hm0" intervals are for "X0", "~km0" intervals for "Xmax"; so
          // increase "np" to contain a multiple of "km0":
          //
          int sc = int(Ceil(F(*np)/F(km0)));
          *np    = sc * km0 + 1;
          *n0    = sc * hm0 + 1;

          // Adjust the step size:
          h      = (Xmax - Xmin) / F(*np - 1);
        }
      }
    }
    //-----------------------------------------------------------------------//
    // "X0" is NOT required to be on the Grid:                               //
    //-----------------------------------------------------------------------//
    else
    {
      // Create the axis with given "np". XXX: In general, it is even OK if
      // "X0" is not in the [Xmin..Xmax] range:
      Xaxis->resize(*np);

      // Set "n0" to an invalid value:
      *n0 = -1;
    }

    //-----------------------------------------------------------------------//
    // Fill in the (uniform) Grid points:                                    //
    //-----------------------------------------------------------------------//
    (*Xaxis)[0]     = Xmin;
    (*Xaxis)[*np-1] = Xmax;
    assert(h > F(0.0));

    for (int i = 1; i < (*np)-1; ++i)
      (*Xaxis)[i] = Xmin + F(i) * h;

    if (X0OnGrid)
    {
      // Verify that "X0" is placed correctly on the axis:
      assert(0 <= *n0  && *n0  < *np &&
             Abs((*Xaxis)[*n0] - X0) < F(10.0) * Eps<F>());

      // Make "X0" exact:
      (*Xaxis)[*n0] = X0;
    }
  }
}

namespace Arbalete
{
  //=========================================================================//
  // "Grid2D::Params" Default Ctor:                                          //
  //=========================================================================//
  // Creates invalid params, apart from "m_{Lo|Up}{S|V}"; the latter, if left
  // unchanged, indicate auto-selected values to the Grid2D ctor:
  //
  template<typename F>
  inline Grid2D<F>::Params::Params()
  : m_method    (),
    m_m         (0),
    m_n         (0),
    m_hS        (F(0.0)),     // NB: If set, used to construct "m_fineS"
    m_hV        (F(0.0)),     // NB: If set, used to construct "m_fineV"
    m_sts       (0),
    m_TS        (ZeroTime()),
    m_TV        (ZeroTime()),
    m_dtTH      (ZeroTime()),
    m_dtON      (ZeroTime()),
    m_dtWEH     (ZeroTime()),
    m_LoS       (-Inf<F>()),
    m_UpS       ( Inf<F>()),
    m_LoSFixed  (false),
    m_UpSFixed  (false),
    m_LoV       (-Inf<F>()),
    m_UpV       (Inf<F>()),
    m_LoVFixed  (false),
    m_UpVFixed  (false),
    m_NSigmasS  (F(7.0)),
    m_NSigmasV  (F(7.0)),
    m_minRelS   (F(0.0)),
    m_minRelV   (F(0.0)),
    m_maxRelS   (Inf<F>()),
    m_maxRelV   (Inf<F>()),
    m_S0OnGrid  (true),
    m_V0OnGrid  (true),
    m_debugLevel(0),
    m_cudaEnv   (),           // NULL by default
    m_cal       (),           // ditto
    m_fineS     (),           // ditto
    m_fineV     ()            // ditto
  {}

  //=========================================================================//
  // "Grid2D::Params::IsInvariant{S,V}":                                     //
  //=========================================================================//
  // NB: The following tests are based on the semantics of axes construction in
  // "Grid2D" Ctor and "MkAxis". A "degenerate" axis is considered  to be inva-
  // riant:
  //
  template<typename F>
  inline bool Grid2D<F>::Params::IsInvariantS() const
  {
    if ((m_LoSFixed && !IsFinite(m_LoS)) || (m_UpSFixed && !IsFinite(m_UpS)))
      throw invalid_argument
            ("Grid2D::Params::IsIvariantS: LoS/UpS fixed but not given");

    return (m_m <= 1) || (m_LoSFixed && m_UpSFixed);
  }

  template<typename F>
  inline bool Grid2D<F>::Params::IsInvariantV() const
  {
    if ((m_LoVFixed && !IsFinite(m_LoV)) || (m_UpVFixed && !IsFinite(m_UpV)))
      throw invalid_argument
            ("Grid2D::Params::IsIvariantV: LoV/UpV fixed but not given");

    return (m_n <= 1) || (m_LoVFixed && m_UpVFixed);
  }

  //=========================================================================//
  // "Grid2D" Non-Default Ctor:                                              //
  //=========================================================================//
  template<typename F>
  inline Grid2D<F>::Grid2D
  (
    shared_ptr<Diffusion2D<F>> const& diff,
    Params                     const& params
  )
  : m_sts       (params.m_sts),
    m_T         (max<Time>(params.m_TS, params.m_TV)),
    m_dtTH      (params.m_dtTH),
    m_dtON      (params.m_dtON),
    m_dtWEH     (params.m_dtWEH),
    m_NSigmasS  (params.m_NSigmasS),
    m_NSigmasV  (params.m_NSigmasV),
    m_minRelS   (params.m_minRelS),
    m_minRelV   (params.m_minRelV),
    m_maxRelS   (params.m_maxRelS),
    m_maxRelV   (params.m_maxRelV),
    m_cudaEnv   (params.m_cudaEnv),
    m_debugLevel(params.m_debugLevel),
    m_diff      (diff),
    m_cal       (params.m_cal)
  {
    //=======================================================================//
    // Get the Diffusion Params:                                             //
    //=======================================================================//
    // NB: Be careful: "S0" and/or "V0" may be needed even for Invariant Grids
    // (if the Grid is degenerate):
    //
    F S0     = diff->GetS0();
    F V0     = diff->GetV0();

    // XXX: This check for "S0", "V0" validity would much better be performed
    // in the "Diffusion2D" ctor, but we could not do it there because it
    // calls the virtual "IsIRModel()" method:
    //
    bool logNormalS = !(diff->IsIRModel());

    if (V0 < F(0.0) || (logNormalS && S0 <= F(0.0)))
      throw invalid_argument("Grid2D Ctor: Invalid S0, V0 in the Diffusion");

    bool invarS    = params.IsInvariantS();
    bool invarV    = params.IsInvariantV();

    //-----------------------------------------------------------------------//
    // Estimate the vols:                                                    //
    //-----------------------------------------------------------------------//
    // NB: This is required even when Grid axes are fixed -- we want to display
    // the grid step in terms of volatility of time step.

    // NB: Estimate the vols for both trading gours and non-trading hours:
    //
    F trendS_TH, volS1_TH, volS2_TH, trendV_TH, volV_TH, corrSV_any;
    F trendS_N,  volS1_N,  volS2_N,  trendV_N,  volV_N;

    DateTime tt = StartTime();
    TradingCalendar const* cal = m_cal.get(); // May be NULL, then 24/7 is used
    bool gotTH  = false;
    bool gotN   = false;

    while (!(gotTH && gotN))
    {
      DateTime t1;
      TradingCalendar::CalReg  reg;

      GetNextFwdEvent(tt, cal, &t1, &reg);

      if (reg == TradingCalendar::TradingHours && !gotTH)
      {
        // Get Trading Hours Coeffs:
        diff->GetSDECoeffs
        (
          S0,  V0, YF<F>(StartTime(), tt),  reg,
          &trendS_TH,  &volS1_TH, &volS2_TH, &trendV_TH, &volV_TH,
          &corrSV_any, nullptr
        );
        gotTH = true;
      }
      if (reg != TradingCalendar::TradingHours && !gotN)
      {
        // Get Non-Trading-Hours Coeffs:
        diff->GetSDECoeffs
        (
          S0,  V0, YF<F>(StartTime(), tt),  reg,
          &trendS_N,  &volS1_N,  &volS2_N,  &trendV_N, &volV_N,
          &corrSV_any, nullptr
        );
        gotN  = true;
      }
      assert(t1 > tt);
      tt = t1;
    }

    // Over-all normal vols:
    F volS_TH = volS1_TH * volS2_TH;
    F volS_N  = volS1_N  * volS2_N;

    //=======================================================================//
    // "S" bounds and "Sx" axis:                                             //
    //=======================================================================//
    int m = params.m_m;

    if (m <= 1)
    {
      //---------------------------------------------------------------------//
      // "Sx" is Degenerate:                                                 //
      //---------------------------------------------------------------------//
      // For consistency with "m_cudaP" etc below, need a non-0 size for "Sx":
      m_Sx.resize(1);
      m       = 1;
      m_Sx[0] = S0;   // XXX: So formally, it still depends on "S0"!
      m_i0    = 0;    // NB: this "m_i0" is still valid!
    }
    else
    {
      //---------------------------------------------------------------------//
      // "Sx" is Generic:                                                    //
      //---------------------------------------------------------------------//
      F loS = -Inf<F>();
      F upS =  Inf<F>();

      if (!invarS)
      {
        // The "S" axis is NOT invariant:  It will be scaled based on Diffusion
        // Coeffs and Initial Conds:
        //
        // Get the "Grid validity duration" wrt "S":
        //
        if (params.m_TS <= ZeroTime())
          throw invalid_argument("Grid2D Ctor: Invalid TS");

        F TSy    = YF<F>(params.m_TS);
        F sqrtTS = SqRt(TSy);
        assert(sqrtTS > F(0.0));

        if (logNormalS)
        {
          // Total vol for time "TS" (NB: need to remove the "S0" factor which
          // is already implicity in "volS" and "trendS"  -- but XXX  might be
          // problems here with mean-reverting trends).  NB: Use Trading Hours
          // vols and trends here:
          //
          F  volST = volS_TH   / S0 * sqrtTS;
          F  muST  = trendS_TH / S0 * TSy;

          // The amplitude factor for the stochastic exp:
          //
          F  Sampl = min<F>(max<F>(Exp(volST * m_NSigmasS), F(1.0) + m_minRelS),
                            F(1.0) + m_maxRelS);

          // The range of "S" (with the drift, including the "Ito component"):
          //
          F  S0Ito = S0 * Exp(muST - F(0.5) * volST * volST);
          loS      = S0Ito / Sampl;
          upS      = S0Ito * Sampl;
        }
        else
        {
          // Use normal bounds for both "loS" and "upS". This mode is used if
          // "S" is an Interest Rate process. NB: No "Ito drift" component here,
          // and no "S0" factors in "trendS" or "volS". Again, use Trading Hrs
          // vols and trends:
          //
          F volST  = volS_TH   * sqrtTS;
          F Sampl  = min<F>(max<F> (volST * m_NSigmasS, S0 * m_minRelS),
                            S0 * m_maxRelS);
          F S0dr   = S0   + trendS_TH * TSy;

          loS      = S0dr - Sampl;
          upS      = S0dr + Sampl;
        }
      }

      // If either or both of "S" bounds are given explicitly, use them. NB: by
      // itself, it does NOT mean thatthe corresp bounds are fixed -- the given
      // values may be used as an indication only (instead of probabilistic es-
      // timates). HOWEVER, they will be completely fixed if the corresp param
      // is set:
      //
      m_LoSFixed = params.m_LoSFixed;
      m_UpSFixed = params.m_UpSFixed;

      if (IsFinite(params.m_LoS))
        loS = params.m_LoS;

      if (IsFinite(params.m_UpS))
        upS = params.m_UpS;

      assert(!m_LoSFixed || IsFinite(params.m_LoS));
      assert(!m_UpSFixed || IsFinite(params.m_UpS));

      // If "fine-grade" control is provided, adjust "m" (but the Grid must
      // still be non-degenerate!):
      //
      if (params.m_fineS.get() != nullptr)
      {
        m = (*params.m_fineS)(loS, upS, m);
        if (m < 2)
          throw runtime_error("Grid2D Ctor: Degeneracy after FineS");
      }

      if (!invarS)
      {
        // Non-Invariant "S" axis:
        // Run "MkAxis" to actually construct the "Sx" axis; NB: "m_i0" is set
        // to (-1) if "S0" is not placed on the Grid:
        MkAxis<F>
        (
          "S", params.m_S0OnGrid, loS, upS, S0, m_UpSFixed,
          &m, &m_Sx, &m_i0
        );
        assert(m >= 2 && m == int(m_Sx.size()));
        assert(( params.m_S0OnGrid && 0 <= m_i0 && m_i0 < this->m()) ||
               (!params.m_S0OnGrid && m_i0 < 0));
      }
      else
      {
        // Generic Invariant Case: "S0" does not matter, "m_i0" is set to (-1):
        //
        assert(m >= 2            && !params.m_S0OnGrid   &&
               params.m_LoSFixed && IsFinite(params.m_LoS) &&
               params.m_UpSFixed && IsFinite(params.m_UpS));
        MkAxis<F>
        (
          "S", false, params.m_LoS, params.m_UpS, NaN<F>(), true,
          &m,  &m_Sx, &m_i0
        );
        assert(m >= 2 && m == int(m_Sx.size()) && m_i0 == -1);
      }
    }

    //=======================================================================//
    // "V" bounds and "Vx" axis:                                             //
    //=======================================================================//
    int n = params.m_n;

    if (n <= 1)
    {
      //---------------------------------------------------------------------//
      // "Vx" is Degenerate:                                                 //
      //---------------------------------------------------------------------//
      // For consistency with "m_cudaP" etc below, need a non-0 size for "Vx":
      m_Vx.resize(1);
      n       = 1;
      m_Vx[0] = V0;   // XXX: So formally, it still depends on "V0"!
      m_j0    = 0;    // NB: this "m_j0" is still valid!
    }
    else
    {
      //---------------------------------------------------------------------//
      // "Vx" is Generic:                                                    //
      //---------------------------------------------------------------------//
      F loV = -Inf<F>();
      F upV =  Inf<F>();

      if (!invarV)
      {
        // The "V" axis is NOT invariant:  It will be scaled based on Diffusion
        // Coeffs and Initial Conds:
        //
        // StocVol is always approximated as log-normal, so again, adjust its
        // trend and vol by "V0":
        //
        if (params.m_TV <= ZeroTime())
          throw invalid_argument("Grid2D Ctor: Invalid TV");

        F TVy    = YF<F>(params.m_TV);
        F sqrtTV = SqRt(TVy);

        // Total vol for time "TV" (NB: need to remove the "V0" factor which is
        // already implicity in "volV"). Use Trading Hours data:
        //
        F  volVT = volV_TH / V0 * sqrtTV;

        // The amplitude factor for the stochastic exp:
        //
        F  Vampl = min<F>(max<F>(Exp(volVT * m_NSigmasV), F(1.0) + m_minRelV),
                          F(1.0) + m_maxRelV);

        // The range of "V" (with the drift, including the "Ito component"):
        //
        F  V0Ito = V0 * Exp(trendV_TH * TVy - F(0.5) * volVT * volVT);
        loV      = V0Ito / Vampl;
        upV      = V0Ito * Vampl;
      }

      // If either or both of "V" bounds are given explicitly, use them. NB: by
      // itself, it does NOT mean thatthe corresp bounds are fixed -- the given
      // values may be used as an indication only (instead of probabilistic es-
      // timates). HOWEVER, they will be completely fixed if the corresp param
      // is set:
      //
      m_LoVFixed = params.m_LoVFixed;
      m_UpVFixed = params.m_UpVFixed;

      if (IsFinite(params.m_LoV))
        loV = params.m_LoV;

      if (IsFinite(params.m_UpV))
        upV = params.m_UpV;

      assert(!m_LoVFixed || IsFinite(params.m_LoV));
      assert(!m_UpVFixed || IsFinite(params.m_UpV));

      // If "fine-grade" control is provided, adjust "n" (but the Grid must
      // still be non-degenerate!):
      if (params.m_fineV.get() != nullptr)
      {
        n = (*params.m_fineV)(loV, upV, n);
        if (n < 2)
          throw runtime_error("Grid2D Ctor: Degeneracy after FineV");
      }

      if (!invarV)
      {
        // Non-Invariant "V" axis:
        // Run "MkAxis" to actually construct the "Vx" axis; NB: "m_j0" is set
        // to (-1) if "V0" is not placed on the Grid:
        MkAxis<F>
        (
          "V", params.m_V0OnGrid, loV, upV, V0, m_UpVFixed,
          &n, &m_Vx, &m_j0
        );
        assert(n >= 2 && n == int(m_Vx.size()));
        assert(( params.m_V0OnGrid && 0 <= m_j0  && m_j0 < this->n()) ||
               (!params.m_V0OnGrid && m_j0 < 0));
      }
      else
      {
        // Generic Invariant Case: "V0" does not matter, "m_j0" is set to (-1):
        //
        assert(n >= 2            && !params.m_V0OnGrid   &&
               params.m_LoVFixed && IsFinite(params.m_LoV) &&
               params.m_UpVFixed && IsFinite(params.m_UpV));
        MkAxis<F>
        (
          "V", false, params.m_LoV, params.m_UpV, NaN<F>(), true,
          &n,  &m_Vx, &m_j0
        );
        assert(n >= 2 && n == int(m_Vx.size()) && m_j0 == -1);
      }
    }

    //=======================================================================//
    // Some debugging output: Summary of Axes constructed:                   //
    //=======================================================================//
    if (m_debugLevel >= 1)
    {
      F hS = (m >= 2) ? (m_Sx[1] - m_Sx[0]) : F(0.0);
      cout << "Sx = ["  << m_Sx[0] << " .. "  << m_Sx[this->m()-1] << "] ("
           << this->m() << " point(s), hS = " << hS  << ", S0 = "  << S0
           << (IsS0OnGrid() ? " is on Grid" : " is not on Grid")   << ')'
           << endl;

      F hV = (n >= 2) ? (m_Vx[1] - m_Vx[0]) : F(0.0);
      cout << "Vx = ["  << m_Vx[0] << " .. " << m_Vx[this->n()-1] << "] ("
           << this->n() << " point(s), hV = " << hV  << ", V0 = " << V0
           << (IsV0OnGrid() ? " is on Grid" : " is not on Grid")  << ')'
           << endl;

      // Because we are dealing with delta-like functions, there may be proba-
      // bility mass leaks (if probability mass is integrated on the grid) if
      // the step is too coarse. Get some indications:
      //
      if (m >= 2)   // Non-degenerate "S" axes only:
      {
        if (params.m_etTH != ZeroTime())
        {
          // Express "hS" in terms of normal "S" vol over "m_etTH":
          F dS_TH = volS_TH * SqRt(YF<F>(params.m_etTH));
          cout << "Trading Hours:     hS / vol(S,dt) = "
               << (hS / dS_TH) << endl;
        }
        if (params.m_etN  != ZeroTime())
        {
          // Express "hS" in terms of normal "S" vol over "m_etN":
          F dS_N  = volS_N  * SqRt(YF<F>(params.m_etN));
          cout << "Non-Trading Hours: hS / vol(S,dt) = "
               << (hS / dS_N) << endl;
        }
      }
      // Save for "V": again, non-degenerate "V" axes only:
      if (n >= 2)
      {
        if (params.m_etTH != ZeroTime())
        {
          // Express "hV" in terms of normal "V" vol over "m_etTH":
          F dV_TH = volV_TH * SqRt(YF<F>(params.m_etTH));
          cout << "Trading Hours:     hV / vol(V,dt) = "
               << (hV / dV_TH) << endl;
        }
        if (params.m_etN  != ZeroTime())
        {
          // Express "hV" in terms of normal "V" vol over "m_etN":
          F dV_N  = volV_N  * SqRt(YF<F>(params.m_etN));
          cout << "Non-Trading Hours: hV / vol(V,dt) = "
               << (hV / dV_N) << endl;
        }
      }
    }
    //=======================================================================//
    // Allocate the CUDA-based data structs:                                 //
    //=======================================================================//
    if (UseCUDA())
    {
      m_cudaP   = m_cudaEnv->CUDAAlloc<F>(m * n);
      m_cudaSx  = m_cudaEnv->CUDAAlloc<F>(m);
      m_cudaVx  = m_cudaEnv->CUDAAlloc<F>(n);

      // Copy "Sx" and "Vx" points from Host to CUDA:
      m_cudaEnv->ToCUDA<F>(&(m_Sx[0]), m_cudaSx, m);
      m_cudaEnv->ToCUDA<F>(&(m_Vx[0]), m_cudaVx, n);

      // Quadratures are computed over "K1" nodes:
      int K1 = (m-1)*(n-1);
      m_cudaSchQuadr = (K1 > 0) ? m_cudaEnv->MkSched1D(K1) : nullptr;
    }
    else
    {
      m_cudaP = m_cudaSx = m_cudaVx = nullptr;
      m_cudaSchQuadr                = nullptr;
    }
  }

  //=========================================================================//
  // Dtor:                                                                   //
  //=========================================================================//
  template<typename F>
  inline Grid2D<F>::~Grid2D()
  {
    if (UseCUDA())
    {
      m_cudaEnv->CUDAFree<F>(m_cudaP);
      m_cudaEnv->CUDAFree<F>(m_cudaSx);
      m_cudaEnv->CUDAFree<F>(m_cudaVx);
      m_cudaEnv->CUDAFree<CUDAEnv::Sched1D>(m_cudaSchQuadr);
    }
  }

  //=========================================================================//
  // "GetDt", "GetCUDAEnv":                                                  //
  //=========================================================================//
  template<typename F>
  inline Time Grid2D<F>::GetDt(TradingCalendar::CalReg reg) const
  {
    switch (reg)
    {
      case TradingCalendar::TradingHours:
        return m_dtTH;
      case TradingCalendar::OverNight:
        return m_dtON;
      case TradingCalendar::WeekEndHol:
        return m_dtWEH;
      default:
        throw runtime_error("Grid2D::GetDt: Invalid CalReg");
    }
  }

  template<typename F>
  inline CUDAEnv const& Grid2D<F>::GetCUDAEnv() const
  {
    CUDAEnv const* res = m_cudaEnv.get();
    if (res == nullptr)
      throw runtime_error("Grid2D<F>::GetCUDAEnv: CUDA is not available");
    return *res;
  }

  //=========================================================================//
  // "GetIdxS0", "GetIdxV0":                                                 //
  //=========================================================================//
  template<typename F>
  inline int Grid2D<F>::GetIdxS0() const
  {
    if (m_i0 >= 0)
    {
      // "S0" is on the Grid:
      assert(m_i0 < m());
      return m_i0;
    }
    else
      throw runtime_error("Grid2D::GetIdxS0: S0 not on the Grid");
  }

  template<typename F>
  inline int Grid2D<F>::GetIdxV0() const
  {
    if (m_j0 >= 0)
    {
      // "V0" is on the Grid:
      assert(m_j0 < n());
      return m_j0;
    }
    else
      throw runtime_error("Grid2D::GetIdxV0: V0 not on the Grid");
  }

  //=========================================================================//
  // "GetS0", "GetV0":                                                       //
  //=========================================================================//
  // These values actually come from the underlying Diffusion; but if they are
  // placed on a NON-DEGENERATE Grid, same values (up to rounding errors) must
  // be found on the Grid:
  template<typename F>
  inline F Grid2D<F>::GetS0() const
  {
    F S0 = m_diff->GetS0();
#   ifndef NDEBUG
    if (m_i0 >= 0 && m() >= 2)
      // "S0" is on the Grid, and the Grid is non-degenerate:
      assert(m_i0 <  m() && m_Sx[m_i0] == S0);
#   endif
    return S0;
  }

  template<typename F>
  inline F Grid2D<F>::GetV0() const
  {
    F V0 = m_diff->GetV0();
#   ifndef NDEBUG
    if (m_j0 >= 0 && n() >= 2)
      // "V0" is on the Grid, and the Grid is non-degenerate:
      assert(m_j0 <  n() && m_Vx[m_j0] == V0);
#   endif
    return V0;
  }

  //=========================================================================//
  // "IsS0OnGrid", "IsV0OnGrid":                                             //
  //=========================================================================//
  template<typename F>
  inline bool Grid2D<F>::IsS0OnGrid() const
  {
    bool res = (m_i0 >= 0);
    assert(!res || m_i0 < m());
    return res;
  }

  template<typename F>
  inline bool Grid2D<F>::IsV0OnGrid() const
  {
    bool res = (m_j0 >= 0);
    assert(!res || m_j0 < n());
    return res;
  }

  //=========================================================================//
  // "TimeMarshal":                                                          //
  //=========================================================================//
  // For both Fokker-Planck (Fwd) and Feynman-Kac (Bwd) eqns. Can also be used
  // in a stand-alone mode (e.g., for non-Finite-Difference-based Grids).
  // Pre-condition: K = m * n:
  //
  template<typename F>
  template<bool IsFwd>
  inline void Grid2D<F>::TimeMarshal
  (
    DRType                  DR,
    DateTime                t_from,
    F*                      p,
    DateTime                t_to,
    BoundConds2D<F>  const& bcs,
    CallBack         const* call_back,
    vector<DateTime> const* special_times
  )
  const
  {
    //-----------------------------------------------------------------------//
    // Initialisation and Checks:                                            //
    //-----------------------------------------------------------------------//
    // "IsFwd" requires "NoDR":
    assert(!IsFwd || DR == DRType::NoDR);

    // Check the grid temporal bounds and the dates:
    bool timesOK =
      ( IsFwd &&
        StartTime() <= t_from && t_from <= t_to   && t_to   <= EndTime()
      ) ||
      (!IsFwd &&
        StartTime() <= t_to   && t_to   <= t_from && t_from >= EndTime()
      );
    if (!timesOK)
      throw invalid_argument("Grid2D::TimeMarshal: Invalid time(s)");

    // In case if the initial and bound conds are inconsistent, the latter
    // should prevail: apply them now:
    //
    int      m  = int(m_Sx.size());
    int      n  = int(m_Vx.size());
    int      K  = m * n;
    int      M  = m_sts / 2;

    F const* Sx = &(m_Sx[0]);
    F const* Vx = &(m_Vx[0]);

    if (M == 1)
      bcs.template ApplyDirect<1>(m, Sx, n, Vx, t_from, p);
    else
    if (M == 2)
      bcs.template ApplyDirect<2>(m, Sx, n, Vx, t_from, p);

    // NB: May also have M == 0  (no Bound Conds)

    // Invoke the Induction Initialiser (purely virtual -- provided by derived
    // classes):
    InitInduct(IsFwd, DR, t_from, bcs);

    // In the CUDA mode, copy "p" into the CUDA device memory:
    if (UseCUDA())
      m_cudaEnv->ToCUDA<F>(p, m_cudaP, K);

    // Get the Calendar (if any):
    TradingCalendar const* cal = m_cal.get();

    // Check "special_times" (if any) and determine the first index there   (in
    // the Fwd or Bwd direction) which is within the [t_from .. t_to] range (in
    // the respective order); "special_times",  if given, must be sorted in the
    // STRICTLY ASCENDING order anyway:
    //
    int sti =
      TimeMarshalUtils::ChkSpecialTimes<IsFwd>(special_times, t_from, t_to);
    // NB: may have sti=-1

    // "f0" is the initial solution space to be provided to the actual solver:
    //      Host or CUDA:
    // "f_curr" is the curr solution space (either initial, or returned by the
    //      last call to "DoOneStep" which may alternate solution spaces):
    //
    F* f0     = (!UseCUDA()) ? p : m_cudaP;
    F* f_curr = f0;

    // Initialise the curr time and compute the initial regime (it always Expl
    // as it is adjacent to the INITIAL time):
    DateTime t_curr = t_from;

    // Current time steps -- maintained separately for each regime:
    Time dts[3] = { m_dtTH, m_dtON, m_dtWEH };

    //-----------------------------------------------------------------------//
    // Main Fwd/Bwd Induction Loop:                                          //
    //-----------------------------------------------------------------------//
    for (int k = 0; ; ++k)
    {
      // "stk" is the index of "t_curr" in the "*special_times" if "t_curr" is
      // indeed a "special" time, and (-1) otherwise:
      //
      int stk =
        TimeMarshalUtils::IsSpecialTime<IsFwd>(t_curr, special_times, sti);

      // Get the next time instant ("t_next") and the corresp Regime applied
      // within [t_curr..t_next] (will never cross regime change boundaries).
      //
      DateTime                t_next;
      TradingCalendar::CalReg reg;

      if (IsFwd)
        TimeMarshalUtils::StepFwd
          (t_curr, dts, t_to, cal, special_times, &sti, &t_next, &reg);
      else
        TimeMarshalUtils::StepBwd
          (t_curr, dts, t_to, cal, special_times, &sti, &t_next, &reg);

      if (IsFwd)
        assert(t_curr < t_next && t_next <= t_to);
      else
        assert(t_curr > t_next && t_next >= t_to);

      // Debugging output (if enabled). Data needs to copied from CUDA to host
      // memory if it is not at "t_from":
      //
      if (m_debugLevel >= 2)
        GridDebug<IsFwd>(t_curr, f_curr);

      // Invoke the user call-back (if provided). It may return the suggested
      // alternative time step ("alt_dt") for the curr regime; negative value
      // means immediate termination, 0 value is to be ignored:
      //
      Time alt_dt =
        (call_back != nullptr)
        ? (*call_back)(*this, t_curr, stk, f_curr, t_next, reg)
        : ZeroTime();

      if (alt_dt.is_negative())
        // Return immediately:
        return;
      else
      if (alt_dt > ZeroTime())
      {
        // Adjust the time step for the curr regime.  If time step is reduced,
        // it takes effect immediately (this is safe -- we will not cross any
        // regime-switching points etc); otherwise,  it takes effect from the
        // next iteration.  HOWEVER,  the new time step will never exceed the
        // original default for the given regime:
        //
        int ireg = int(reg);
        assert(0 <= ireg && ireg < 3);

        // NB: Need special treament for the original default being 0, because
        // "0" actually means +oo:
        Time std_dt = GetDt(reg);
        dts[ireg]   = 
          (std_dt > ZeroTime()) ? min<Time>(alt_dt, std_dt) : alt_dt;

        Time new_dt = dts[ireg];

        // The actual curr time step:
        Time curr_dt = t_next - t_curr;

        // Possibly adjust "t_next" (with some safety margin):
        if (YF<F>(new_dt) < F(0.9) * YF<F>(curr_dt))
          t_next = IsFwd ? (t_curr + new_dt) : (t_curr - new_dt);
      }

      //---------------------------------------------------------------------//
      // MAKE THE ACTUAL STEP: t_curr -> t_next:                             //
      //---------------------------------------------------------------------//
      // NB: The actual solver always receives the "fixed" solution space "f0"
      // as an argument  (so it does not need to memoise it on the 1st call),
      // but it may alternate solution buffers, so it returned a potentially
      // different "f_curr" ptr (again, Host- or CUDA-based):
      //
      f_curr = DoOneStep(IsFwd, DR, k, t_curr, f0, t_next, reg, bcs);

      // Next step?
      if (t_next == t_to)
        break;

      t_curr = t_next;
    }
    //-----------------------------------------------------------------------//
    // After the final step:                                                 //
    //-----------------------------------------------------------------------//
    // Invoke the user call-back (if available) on the last solution  space
    // (with both time stamps set to "t_to" indicating that this is a final
    // step, "reg" is irrelevant then):
    //
    if (call_back != nullptr)
    {
      int stk =
        TimeMarshalUtils::IsSpecialTime<IsFwd>(t_to, special_times, sti);

      void((*call_back)
           (*this, t_to, stk, f_curr, t_to, TradingCalendar::TradingHours));
    }

    // Copy the solution into Host memory:
    if (UseCUDA())
      m_cudaEnv->ToHost<F>(f_curr, p, K);

    // Apply the Bound Conds (in case if they have not been applied already);
    // NB: for some methods, explicit Bound Conds are not used (say M==0):
    if (M == 1)
      bcs.template ApplyDirect<1>(m, Sx, n, Vx, t_from, p);
    else
    if (M == 2)
      bcs.template ApplyDirect<2>(m, Sx, n, Vx, t_from, p);

    // NB: May also have M==0 (no Bound Conds)

    // If "GridDebug" is called, mark it as "no-copy" -- "p" is already set up
    // in the Host memory:
    if (m_debugLevel >= 2)
      GridDebug<IsFwd>(t_to, f_curr);

    // All done!
  }

  //=========================================================================//
  // "DeltaSV": 2D Delta Func: Initial conds for the Fokker-Planck PDE:      //
  //=========================================================================//
  // XXX: Not for degenerate Grids:
  //
  template<typename F>
  inline void Grid2D<F>::DeltaSV(Matrix<F>* p) const
  {
    int m    = int(m_Sx.size());
    int n    = int(m_Vx.size());
    if (m < 2 || n < 2)
      throw runtime_error("Grid2D::DeltaSV: Degenerate Grid");

    if (m_i0 < 0 || m_j0 < 0)
      throw runtime_error("Grid2D::DeltaSV: S0 and/or V0 not on the Grid");

    assert(0 <= m_i0 && m_i0 < m   && 0 <= m_j0 && m_j0 < n &&
           p != nullptr && int(p->size1()) == m    && int(p->size2()) == n);

    F hS  = m_Sx[1] - m_Sx[0];
    F hV  = m_Vx[1] - m_Vx[0];
    F hSV = hS * hV;
    assert(hSV > F(0.0));

    ZeroOut<F>(p);
    (*p)(m_i0, m_j0)= F(1.0) / hSV;
  }

  //=========================================================================//
  // "DeltaS: 1D Delta Func: Initial conds for Arrow-Debreu:                 //
  //=========================================================================//
  // The result is constant in "j", and is a delta-function in "i". XXX: Again,
  // not for degenerate Grids:
  //
  template<typename F>
  inline void Grid2D<F>::DeltaS(Matrix<F>* f) const
  {
    int m       = int(m_Sx.size());
    int n       = int(m_Vx.size());
    if (m < 2 || n < 2)
      throw runtime_error("Grid2D::DeltaS: Degenerate Grid");

    if (m_i0 < 0)
      throw runtime_error("Grid2D::DeltaS: S0 not on the Grid");

    assert(0 <= m_i0 && m_i0 < m &&
           f != nullptr && int(f->size1()) == m && int(f->size2()) == n);

    // NB: "M" boundary points on each side of "Vx" are excluded; "m_i0" must
    // not be a boundary point (on "Sx") either:
    //
    int M = m_sts / 2;
    if (m_i0 <= M-1 || m_i0 >= m-M || n <= 2*M)
      throw runtime_error("Grid2D::DeltaS: Too close to the boundary");

    F hS  = m_Sx[1] - m_Sx[0];
    F hV  = m_Vx[1] - m_Vx[0];
    F val = F(1.0) / hS / hV / F(n-2*M);
    assert(val  > F(0.0));

    ZeroOut<F>(f);
    for (int j = M; j <= n-M-1; ++j)
      // Intergrated over "Vx" values -- same for all "j" ((n-2*M) points):
      (*f)(m_i0,j) = val;
  }

  //=========================================================================//
  // "AdjustSx":                                                             //
  //=========================================================================//
  // XXX: This implementation is NOT for degenerate or non-uniform Grids;
  // however, such Grids could override and re-implement this virtual method:
  //
  template<typename F>
  inline int Grid2D<F>::AdjustSx(DateTime t, F SY, Time next_dt)
  {
    int m = int(m_Sx.size());
    int n = int(m_Vx.size());
    if (m < 2 || n < 2)
      throw runtime_error("Grid2D::AdjustSx: Degenerate Grid");

    // In this procedure, "UpS" is almost always modified, so "S" must not be
    // fixed (e.g., must not be a barrier option grid...):
    //
    if (m_UpSFixed)
      throw runtime_error("Grid2D::AdjustSx: Cannot proceed as UpS is fixed");

    int M = m_sts / 2;
    if (m <= 2*M)
      throw runtime_error("Grid2D::AdjustSx: Grid size too small");

    // Currently, we do not allow "SY" to be in the boundary zone or close to
    // it. From above, we have m >= 2*M+1, so the checks below are valid:
    //
    if (SY < m_Sx[M+1] || SY > m_Sx[m-M-2])
      throw invalid_argument("Grid2D::AdjustSx: Invalid SY");

    int iY = -1;
    F loS  = F(0.0);
    F upS  = F(0.0);

    if (next_dt != Time() && next_dt > ZeroTime())
    {
      // Got a valid "next_dt", so can adjust "Sx" based on that time span. Get
      // the Coeffs at "t"; we assume that adjustment   is due to conditioning,
      // and that occurs during "TradingHours".  Use the stored "V0" as the "V"
      // estimate:
      F V0 = GetV0();

      F trendS, volS1, volS2, trendV, volV, corrSV;

      m_diff->GetSDECoeffs
        (SY,  V0, YF<F>(StartTime(), t), TradingCalendar::TradingHours,
         &trendS, &volS1, &volS2, &trendV, &volV, &corrSV, nullptr);

      F volS   = volS1 * volS2;
      F sqrtDt = SqRt(YF<F>(next_dt));
      assert(sqrtDt > F(0.0));
      F Sampl  = m_NSigmasS * volS * sqrtDt;

      if (!m_diff->IsIRModel() && SY != F(0.0))
      {
        // "S" is a price of a tradable asset with an approx log-normal distr.
        // In this case the ampl is to be corrected by factor S0:
        Sampl = min<F>(max<F>(Exp(Sampl / SY), F(1.0) + m_minRelS),
                       F(1.0) + m_maxRelS);

        // If "loS" is fixed, keep it unchanged (e.g. if it is 0):
        loS   = m_LoSFixed ? m_Sx[0] : (SY / Sampl);
        upS   = SY * Sampl;
      }
      else
      {
        // Use normal bounds for both "loS" and "upS". Again, ig "loS" is fixed,
        // keep it unchanged, although this is unlikely to matter in IR models:
        //
        Sampl = min<F>(max<F>(Sampl, SY * m_minRelS), SY * m_maxRelS);
        loS   = m_LoSFixed ? m_Sx[0] : (SY - Sampl);
        upS   = SY + Sampl;
      }
    }
    else
    {
      // Keep "loS" as it is on the current Axis, though "upS" may be adjusted
      // because "SY" needs to be exactly on the Grid:
      //
      loS = m_Sx[0];
      upS = m_Sx[m-1];
    }

    // Re-construct the axis (the number of points remains the same, but "upS"
    // is considered to be NOT fixed). NB: "SY" is to be placed on the Grid,
    // "iY" is the corresp index; "m_i0" is UNCHANGED:
    int mNew = m;
    MkAxis<F>("S", true, loS, upS, SY, false, &mNew, &m_Sx, &iY);

    // "SY" must now be in the "iY" node. XXX: we do NOT modify "m_i0":
    assert(mNew == m && int(m_Sx.size()) == m && 0 <= iY && iY < m &&
           m_Sx[iY] == SY);

    // IMPORTANT: If CUDA is enabled, modify "m_cudaSx" as well!
    if (UseCUDA())
    {
      assert(m_cudaSx != nullptr);
      m_cudaEnv->ToCUDA<F>(&(m_Sx[0]), m_cudaSx, m);
    }

    // Return the index of "Y" on the new axis:
    return iY;
  }

  //=========================================================================//
  // "GridTest":                                                             //
  //=========================================================================//
  // Bwd mode only: EndTime() --> StartTime().
  // Should produce the Identical "1" or Discounted "1"s:
  // XXX: Not for degenerate Grids:
  //
  template<typename F>
  inline void Grid2D<F>::GridTest
  (
    Matrix<F>* f,   bool with_discount,
    CallBack const* call_back
  )
  const
  {
    int m = int(m_Sx.size());
    int n = int(m_Vx.size());
    if (m < 2 || n < 2)
      throw runtime_error("Grid2D::GridTest: Degenerate Grid");

    assert(f != nullptr && int(f->size1()) == m && int(f->size2()) == n);

    F ir = F(0.0);
    DateTime gridStart = StartTime();
    DateTime gridEnd   = EndTime();

    if (with_discount)
      // XXX: assuming constant interest rate -- one prevailing at the
      // beginning; also, assume the day-time regime:
      //
      ir = m_diff->GetIR(GetS0(), F(0.0), TradingCalendar::TradingHours);

    // Boundary conds are all "1"s (possibly with discounting):
    typename BoundConds2D<F>::BCFunc one
    (
      [with_discount, ir, gridEnd]
      (F UNUSED_PARAM(S), F UNUSED_PARAM(V), DateTime t) -> F
        { return with_discount ? Exp(-ir * YF<F>(t, gridEnd)) : F(1.0); }
    );
    BoundConds2D<F> ones
    (
      BCType::DirichletBCT, one,
      BCType::DirichletBCT, one,
      BCType::DirichletBCT, one,
      BCType::DirichletBCT, one
    );

    // Initialise the solution space to "1"s:
    F* fdata = &((*f)(0,0));
    for (int k = 0; k < m*n; ++k)
      *(fdata++) = F(1.0);

    // Bwd induction from "gridEnd" to "gridStart", so "special_times":
    SolveFeynmanKac
      (with_discount, gridEnd, f, gridStart, ones, call_back, nullptr);
  }

  //=========================================================================//
  // "Quadrature2D":                                                         //
  //=========================================================================//
  // Integrate matrix "f" with the kernel "kern" (NB: "f" may point to the CUDA
  // memory):
  //
  // (0) The private method -- implementing (1) and (2) below. Pre-condition:
  //     len(res) = max(1, len(params));
  // XXX: "f" must point to CUDA or Host space depending on UseCUDA(); i.e., it
  //     is NOT possible to compute quadrature of a Host-based matrix  if  Use-
  //     CUDA() is enabled:
  // XXX: Not for degenerate Grids:
  //
  template<typename F>
  template<typename Kernel>
  inline void Grid2D<F>::Quadrature2D
  (
    Kernel const&   kern,
    F const*        f,
    int             rsize,
    F*              res
  )
  const
  {
    assert(f != nullptr && res != nullptr);

    // Get the Grid geometry:
    int m   = int(m_Sx.size());
    int n   = int(m_Vx.size());
    if (m < 2 || n < 2)
      throw runtime_error("Grid2D::Quadrature2D: Degenerate Grid");


    // The result is to be multiplied by hS * hV:
    F hS  = m_Sx[1] - m_Sx[0];
    F hV  = m_Vx[1] - m_Vx[0];
    F hSV = hS * hV;
    assert(hSV > F(0.0));

    // The params:
    assert(rsize == max<int>(kern.GetNParams(), 1));

    // Run the integrator:
    // NB: Do both static and dynamic checks for CUDA availablility, to avoid
    // link errors:
    if (!UseCUDA())
    {
      // Host-based computation:
      Quadrature2D_Core<F, Kernel>(nullptr, kern, m, n, f, rsize, res);
      *res *= hSV;
    }
    else
    {
      // CUDA-based computation: results first come into "cudaTmp" (per each
      // block, per each param / result entry). Layout  (consistent with the
      // one assumed by "Quadrature2D_Core"): params first:
      //
      m_cudaEnv->Start();

      int     KR = rsize * m_cudaEnv->NThreadBlocks();
      F* hostTmp = new F[KR];
      F* cudaTmp = m_cudaEnv->CUDAAlloc<F>(KR);

      Quadrature2D_KernelInvocator<F, Kernel>::Run
        (*m_cudaEnv,  m_cudaSchQuadr, kern, m, n, f, KR, cudaTmp);

      // Copy per-param, per-block results back into host memory:
      m_cudaEnv->ToHost<F>(cudaTmp, hostTmp, KR);

      // Sum up the results, for each param, across all blocks:
      F const* resPB = hostTmp;
      for (int r = 0; r < rsize; ++r)
      {
        F resP = F(0.0);
        for (int b = 0; b < m_cudaEnv->NThreadBlocks(); ++b)
          resP += *(resPB++);
        res[r] = resP * hSV;
      }

      // De-allocate the tmp arrays. XXX: this is somewhat inefficient, and is
      // currently not exception-safe (all exceptions are considered to be fa-
      // tal):
      delete[] hostTmp;
      m_cudaEnv->CUDAFree<F>(cudaTmp);
      m_cudaEnv->Stop();
    }
  }

  //------------------------------------------------------------------------//
  // (1) For Non-Parameterised Kernels:                                     //
  //------------------------------------------------------------------------//
  template<typename F>
  template<typename Kernel>
  inline F Grid2D<F>::Quadrature2D(Kernel const& kern, F const* f) const
  {
    assert(f != nullptr);
    if (kern.GetNParams() >= 2)
      // Cannot have just 1 result in this case:
      throw invalid_argument("Grid2D::Quadrature2D: 1 res, >= 2 params");

    F res = F(0.0);
    this->Quadrature2D<Kernel>(kern, f, 1, &res);
    return  res;
  }

  //------------------------------------------------------------------------//
  // (2) As above, but for Parameterised Kernels:                           //
  //------------------------------------------------------------------------//
  // NB: Even if the number of params is 0, there must be at least 1 element
  // in "res":
  //
  template<typename F>
  template<typename Kernel>
  inline void Grid2D<F>::Quadrature2D
  (
    Kernel const& kern,
    F const*      f,
    Vector<F>*    res
  )
  const
  {
    assert(f != nullptr && res != nullptr);
    int r  = int(res->size());

    if (r != max<int>(kern.GetNParams(), 1))
      throw invalid_argument
            ("Grid2D::Quadrature2D: Invalid res size vs params size");

    this->Quadrature2D<Kernel>(kern, f, r, &((*res)[0]));
  }

  //=========================================================================//
  // "GridDebug": Debugging Output Fwd and Bwd Solvers:                      //
  //=========================================================================//
  template<typename F>
  template<bool IsFwd>
  inline void Grid2D<F>::GridDebug
  (
    DateTime      t,
    F const*      f,
    string const& postfix
  )
  const
  {
    int m = int(m_Sx.size());
    int n = int(m_Vx.size());
    assert(f != nullptr);

    // Make the file name from the time stamp:
    //
    string fname = DateTimeToStr(t, true) + postfix;
    assert(fname.length() >= 19 && fname[10] == 'T');
    fname[10] = '_';
    // NB: on Windows, ':'s in the file name are not allowed, so replace them:
#   ifdef _WIN32
    assert(fname[13] == ':' && fname[16] == ':');
    fname[13] = fname[16] = '_';
#   endif
    // Also replace a possible ',' with '.':
    if (fname.length() > 19)
    {
      assert(fname[19] == ',');
      fname[19] = '.';
    }

    // Open / create the output file with the name constructed:
    // 
    FILE* of = fopen(fname.c_str(), "w");
    if (of == nullptr)
      throw runtime_error("Grid2D::Debug: Cannot open output file: "+ fname);

    // In the CUDA mode, the solution may not yet be in "p" -- so copy it over
    // from "f". In the Host mode, use "f" directly;
    // "fr"  is the solution ptr is the Host memory (for producing output):
    //
    F const*  fr = nullptr;
    Matrix<F> p;

    if (UseCUDA())
    {
      // "f" is assumed to be a CUDA-space ptr, so copy the solution to the
      // tmp Host memory:
      p.resize(m, n);
      fr = &(p(0, 0));
      m_cudaEnv->ToHost<F>(f, const_cast<F*>(fr), m*n);
    }
    else
      fr = f;
    assert(fr != nullptr);

    // Traverse the solution pointed to by "fr":
    F   pMin =   Inf<F>();
    F   pMax = - Inf<F>();
    int iMin =   0,  jMin = 0;
    int iMax =   0,  jMax = 0;

    // NB: The matrix is stored in a column-major format, but we output it in a
    // row-major format:
    for (int i = 0; i < m; ++i)
    {
      for (int j = 0; j < n; ++j)
      {
        F fij = fr[m*j + i];

        // Check for min/max (XXX: don't use "else if" -- will be wrong on 1st
        // iteration!):
        if (fij < pMin)
        {
          pMin  = fij;
          iMin  = i;
          jMin  = j;
        }
        if (fij > pMax)
        {
          pMax  = fij;
          iMax  = i;
          jMax  = j;
        }
        // Generate output in the Matlab format:
        fprintf(of, "%.9e  ", fij);
      }
      fputc('\n', of);
    }
    assert(pMin <= pMax);

    // Output Grid geometry:
    F hS = m_Sx[1] - m_Sx[0];
    F hV = m_Vx[1] - m_Vx[0];

    fprintf(of, "%% S = [%.9e .. %.9e], %d points, hS = %.9e\n"
                "%% V = [%.9e .. %.9e], %d points, hV = %.9e\n",
            m_Sx[0], m_Sx[m-1], m, hS, m_Vx[0], m_Vx[n-1], n, hV);

    // Output the range of solution values:
    fprintf(of, "%% Pmin = %.9e @ (%d,%d), Pmax = %.9e @ (%d,%d)",
            pMin, iMin, jMin, pMax, iMax, jMax);

    // Invoke the Quadrature for the ORIGINAL probability mass "f" (may be in
    // in the CUDA space!) -- only for non-degenerate Grids:
    //
    if (IsFwd && m >= 2 && n >= 2)
    {
      F pMass = Quadrature2D<ProbMassKern<F>>(ProbMassKern<F>(), f);

      fprintf(of, ", |1-mass| = %.9e", Abs(F(1.0) - pMass));
    }
    fputc('\n', of);
    fclose(of);
  }
}
