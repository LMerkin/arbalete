// vim:ts=2:et
//===========================================================================//
//                           "TimeStepper_RKF5.h":                           //
//          5th-Order Runge-Kutta-Fehlberg Time Propagation Method           //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Common/CUDAEnv.hpp"
#include "Grids/TimeStepper_RKF5.h" // Only for "DataRKF5" spec
#include <cassert>

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "SubStepRKF5_Core":                                                     //
  //=========================================================================//
  // NB: "rkf" is passed by copy (this function may run on the CUDA side):
  //
  // NB: "static" is essential here -- otherwise Host code could get called
  // instead of CUDA code!
  //
  template<typename F>
  GLOBAL static void SubStepRKF5_Core
  (
    CUDAEnv::Sched1D const*  CUDACC_OR_DEBUG(sched), int kUntil,
    F const* p0, DataRKF5<F> rkf,   F tau,    F* Y
  )
  {
    int  K = rkf.K();
    typename DataRKF5<F>::F6 &  c = rkf.c ();
    typename DataRKF5<F>::FP6& MK = rkf.MK();

#   ifdef __CUDACC__
    // The order number of this thread (across all blocks):
    int Tn  = blockDim.x * blockIdx.x + threadIdx.x;
    assert(sched != nullptr);

    int lFrom = sched[Tn].m_from;
    int lTo   = sched[Tn].m_to;
    if (lFrom > lTo)
      // This thread has nothing to do: XXX: inefficiency. Can exit this thread
      // now as there is no post-processing after the "l" loop:
      return;
#   else
    // Host-based computation: Full range of indices:
    assert(sched == nullptr);
    int lFrom = 0;
    int lTo   = K-1;
#   endif

    assert(0  <= lFrom   && lFrom <= lTo     && lTo    <= K-1);
    assert(p0 != nullptr && Y     != nullptr && kUntil <= 6);

    for (int l = lFrom; l <= lTo; ++l)
    {
      F res = F(0.0);
      for (int k = 0; k < kUntil; ++k)
        res += c[k] * MK[k][l];
      Y[l] = p0[l] + res * tau;
    }
  }

  //=========================================================================//
  // "RKF5_KernelInvocator":                                                 //
  //=========================================================================//
  template<typename F>
  struct RKF5_KernelInvocator
  {
    // NB: This function runs on the Host side, so "rkf" can be passed by ref:
    static void Run
    (
      CUDAEnv const& cudaEnv,  CUDAEnv::Sched1D const* sched, int kUntil,
      F const* p0, DataRKF5<F> const& rkf, F tau, F* Y
    )
#   ifdef __CUDACC__
    {
      // Set the CUDA device:
      CUDAEnv::Lock();
      cudaEnv.SetCUDADevice();

      // Invoke the kernel:
      SubStepRKF5_Core<F>
        <<<cudaEnv.NThreadBlocks(), cudaEnv.ThreadBlockSize(), 0,
           cudaEnv.Stream()
        >>>
        (sched, kUntil, p0, rkf, tau, Y);

      CUDAEnv::Unlock();
      CUDA_CHECK_STATUS("RKF5 Kernel invocation failed")
    }
#   else
    ; // Spec only
#   endif
  };
}
