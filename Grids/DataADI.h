// vim:ts=2:et
//===========================================================================//
//                                 "DataADI.h":                              //
//                       Data Structures for "Grid2D_ADI"                    //
//===========================================================================//
// "DataADI" class provides continuous stotage for Coeffs and Bound Conds used
// in "Grid2D_ADI". The main objective is efficient copying of these data into
// CUDA device and shared memory.
// XXX: This class is for internal use within "Grid2D_ADI" only. Its instances
// are passed into CUDA kernels by copy, but a proper copy ctor cannot be used
// in CUDA. Therefore, the default copy ctor (with ptr aliasling) is used, and
// no dtor; de-allocation of "m_data" is performed by the "Grid2D_ADI" dtor:
//
#pragma once

#include "Grids/BoundConds2D.h"

namespace Arbalete
{
  //-------------------------------------------------------------------------//
  // Fwd declaration of the enclosing class:                                 //
  //-------------------------------------------------------------------------//
  template<typename F, int M>
  class Grid2D_ADI;

  //=========================================================================//
  // "DataADI" Class:                                                        //
  //=========================================================================//
  template<typename F, int M>
  class DataADI
  {
  private:
    //-----------------------------------------------------------------------//
    // Data and its Components:                                              //
    //-----------------------------------------------------------------------//
    F*    m_data;         // The actual storage
    int   m_K;            // Length of "m_data" (in "F" sizes)

    // Alias ptrs to "m_data" components.
    // NB: The order of components is important as they are copied to CUDA
    // memory in contiguous chunks:

    // The "1" block:
    F*    m_muS1;         // len = m
    F*    m_sigS11;       // len = m
    F*    m_sigS21;       // len = n
    F*    m_muV1;         // len = n
    F*    m_sigV1;        // len = n
    F*    m_rho1;         // len = 1
    F*    m_ir1;          // len = m  (iff "isRateModel") or 1

    // Linear Eqns Coeffs:
    F*    m_LECs;         // len = 6: {c1I, c2I, c1E, c2E, c3E, dt}

    // BoundConds:
    BCPtrs2D<F,M> m_bcps; // len = 2*M*(m+n)

    // The "0" block:
    F*    m_muS0;         // len = m
    F*    m_sigS10;       // len = m
    F*    m_sigS20;       // len = n
    F*    m_muV0;         // len = n
    F*    m_sigV0;        // len = n
    F*    m_rho0;         // len = 1
    F*    m_ir0;          // len = m (iff "isRateModel") or 1

    F*    m_end;          // m_end - m_data == m_K

  public:
#   ifndef __CUDACC__
    //-----------------------------------------------------------------------//
    // Non-Default Ctor:                                                     //
    //-----------------------------------------------------------------------//
    // All required params are taken from the enclosing Grid2D (Host-Only):
    //
    DataADI(Grid2D_ADI<F,M> const& grid, bool forCUDA);

    //-----------------------------------------------------------------------//
    // Explicit Dtor:                                                        //
    //-----------------------------------------------------------------------//
    // XXX: To be invoked manually from the "Grid2D_ADI" dtor  (Host-Only).
    // Same args as for the Ctor:
    //
    void Destroy(Grid2D_ADI<F,M> const& grid, bool forCUDA);

#   endif // !__CUDACC__
    //-----------------------------------------------------------------------//
    // Access Methods:                                                       //
    //-----------------------------------------------------------------------//
    // Provide alternate access depending on the "ImplS" flag:
    //
    // Implicit ("*I):
    //
    template<bool ImplS>
    DEVICE F*   muSI()    const { return ImplS ? m_muS1   : m_muS0;   }

    template<bool ImplS>
    DEVICE F*   sigS1I()  const { return ImplS ? m_sigS11 : m_sigS10; }

    template<bool ImplS>
    DEVICE F*   sigS2I()  const { return ImplS ? m_sigS21 : m_sigS20; }

    template<bool ImplS>
    DEVICE F*   muVI()    const { return ImplS ? m_muV1   : m_muV0;   }

    template<bool ImplS>
    DEVICE F*   sigVI()   const { return ImplS ? m_sigV1  : m_sigV0;  }

    template<bool ImplS>
    DEVICE F*   rhoI()    const { return ImplS ? m_rho1   : m_rho0;   }

    template<bool ImplS>
    DEVICE F*   irI()     const { return ImplS ? m_ir1    : m_ir0;    }

    // Explicit ("*E"):
    //
    template<bool ImplS>
    DEVICE F*   muSE()    const { return ImplS ? m_muS0   : m_muS1;   }

    template<bool ImplS>
    DEVICE F*   sigS1E()  const { return ImplS ? m_sigS10 : m_sigS11; }

    template<bool ImplS>
    DEVICE F*   sigS2E()  const { return ImplS ? m_sigS20 : m_sigS21; }

    template<bool ImplS>
    DEVICE F*   muVE()    const { return ImplS ? m_muV0   : m_muV1;   }

    template<bool ImplS>
    DEVICE F*   sigVE()   const { return ImplS ? m_sigV0  : m_sigV1;  }

    template<bool ImplS>
    DEVICE F*   rhoE()    const { return ImplS ? m_rho0   : m_rho1;   }

    template<bool ImplS>
    DEVICE F*   irE()     const { return ImplS ? m_ir0    : m_ir1;    }

    // Linear Eqns Coeffs:
    //
    DEVICE F* LECs()      const { return m_LECs; }

    // Bound Conds: Returns a non-const ref, so the object can be modified by
    // the caller in any way:
    //
    DEVICE BCPtrs2D<F,M>& GetBCPtrs() { return m_bcps; }

    // Grid Geometry is stored in "BCPtrs":
    //
    DEVICE int m ()       const { return m_bcps.m_m; }
    DEVICE int n ()       const { return m_bcps.m_n; }

#   ifndef __CUDACC__
    // Other Host-Only Access Methods:
    //
    // The block of newly-computed "*I" coeffs and Bound Conds  for copying
    // into CUDA memory -- depending on the "DataADI" object, the following
    // functions provide both src and dest addresses:
    //
    template<bool ImplS>
    F*  GetIBlockPtr()    const { return ImplS  ? m_muS1 : m_LECs; }
    int GetIBlockLen()    const;

    // Same for "*0" coeffs computed at init time:
    //
    F*  Get0BlockPtr()    const { return m_muS0; }
    int Get0BlockLen()    const { return int(m_end - m_muS0); }

    // Full Data and Total Data Len:
    //
    F*  GetFullData()     const { return m_data; }
    int DataLen()         const { return m_K;    }
#   endif // !__CUDACC__
  };
}
