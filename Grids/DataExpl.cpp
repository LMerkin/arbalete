// vim:ts=2:et
//===========================================================================//
//                              "DataExpl.cpp":                              //
//           Pre-Defined Instances of "DataExpl" (Host-Based only):          //
//===========================================================================//
#include "Grids/DataExpl.hpp"

namespace Arbalete
{
  // 3-Point Stencil:
  template class DataExpl<double, 1>;
  template class DataExpl<float,  1>;

  // 5-Point Stencil:
  template class DataExpl<double, 2>;
  template class DataExpl<float,  2>;
}
