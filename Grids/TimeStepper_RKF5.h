// vim:ts=2:et
//===========================================================================//
//                           "TimeStepper_RKF5.h":                           //
//          5th-Order Runge-Kutta-Fehlberg Time Propagation Method           //
//===========================================================================//
#pragma once

#ifndef  __CUDACC__
#include "Grids/TimeStepper.h"
#endif

namespace Arbalete
{
  //=========================================================================//
  // "DataRKF5":                                                             //
  //=========================================================================//
  // Ptrs to intermediate solutions spaces and coeffs  of the 5th-order Runge-
  // Kutta-Fehlberg scheme. Visible to both Host and NVCC compilers, except for
  // its Ctor / Dtor. We do not create separate files for "DataRKF5" as it is
  // very simple -- use conditional compilation instead:
  //
  template<typename F>
  class DataRKF5
  {
  private:
    //-----------------------------------------------------------------------//
    // Data Flds:                                                            //
    //-----------------------------------------------------------------------//
    int   m_K;
    F     m_c [6];      // Coeffs
    F*    m_MK[6];      // Intermediate solutions (in Host or CUDA memory)

    DataRKF5();         // Default Ctor is hidden

  public:
#   ifndef __CUDACC__
    //-----------------------------------------------------------------------//
    // Non-Default Ctor, Dtor and Access:                                    //
    //-----------------------------------------------------------------------//
    // No "" here -- this class is for us in "TimeStepper_RKF5" only:
    //
    DataRKF5(int K, CUDAEnv const* cudaEnv);

    // The dtor is explicit, to be invoked from "~TimeStepper_RKF5()":
    void Destroy(CUDAEnv const* cudaEnv);
#   endif  // !__CUDACC__

    typedef F  F6 [6];
    typedef F* FP6[6];

    DEVICE int  K ()  { return m_K;  }
    DEVICE F6&  c ()  { return m_c;  }
    DEVICE FP6& MK()  { return m_MK; }  // XXX: no checks!
  };

# ifndef __CUDACC__
  //=========================================================================//
  // "TimeStepper_RKF5":                                                     //
  //=========================================================================//
  // Visible to the Host compiler only:
  //
  template<typename F>
  class TimeStepper_RKF5: public TimeStepper<F>
  {
  private:
    mutable DataRKF5<F> m_data;
    mutable F*          m_Y;

    // Default Ctor is hidden:
    TimeStepper_RKF5();

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor and Dtor:                                            //
    //-----------------------------------------------------------------------//
    // "K": linear size of the solution space (K >= 1):
    //
    TimeStepper_RKF5
      (int K, std::shared_ptr<CUDAEnv> const& cudaEnv);

    ~TimeStepper_RKF5();

    //-----------------------------------------------------------------------//
    // "Propagate":                                                          //
    //-----------------------------------------------------------------------//
    void Propagate
    (
      DateTime                t_from,
      TradingCalendar::CalReg reg,
      F*                      p,
      DateTime                t_to,
      typename TimeStepper<F>::RHS const& rhs
    )
    const;
  };
# endif // !__CUDACC__
}
