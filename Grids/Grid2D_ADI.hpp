// vim:ts=2:et
//===========================================================================//
//                              "Grid2D_ADI.hpp":                            //
//            Fractional Step Method for 2D PDEs (PR-ADI, Yanenko)           //
//===========================================================================//
#pragma once

#include "Common/CUDAEnv.hpp"         // Template bodies are required for CUDA
#include "Grids/Grid2D_ADI.h"
#include "Grids/DataADI.hpp"          // Need template bodies here
#include "Grids/BoundConds2D.h"
#include "Grids/Grid2D_ADI_Core.hpp"  // Template bodies are required for CUDA

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // Non-Default Ctor:                                                       //
  //=========================================================================//
  template<typename F, int M>
  Grid2D_ADI<F,M>::Grid2D_ADI
  (
    shared_ptr<Diffusion2D<F>> const& diff,
    typename Grid2D<F>::Params const& params
  )
  : Grid2D<F>(diff, params),
    m_HD     (*this, false),
    m_CD     (*this, true )
  {
    //-----------------------------------------------------------------------//
    // Method: Yanenko or Classical (Peaceman-Rachford) ADI:                 //
    //-----------------------------------------------------------------------//
    if (params.m_method == "PR-ADI")
      m_isPRADI = true;
    else
    if (params.m_method == "Yanenko")
      m_isPRADI = false;
    else
      throw invalid_argument
            ("Grid2D_ADI Ctor: Invalid scheme: "+ params.m_method);

    //-----------------------------------------------------------------------//
    // Solution Spaces, Diagonals, RHSs:                                     //
    //-----------------------------------------------------------------------//
    // NB: the Diagonals and RHSs are allocated EITHER in the Host memory or
    // in the CUDA device memory, but not in both.
    //
    // ImplS:  the param "V" is explicit (use all vals in M..(n-M-1); bound pts
    // are not used). For each "V", the RHS len and the max diagonal len is
    // (m-2*M).
    //
    // !ImplS; the param "S" is explicit (use all its vals in M..(m-M-1); bound
    // pts are not used). For each "S", the RHS len and the max diagonal len is
    // (n-2*M).
    //
    // Thus, the max allocation size per RHS or diagonal is (n-2*M)*(m-2*M) in
    // all cases. There are (m_sts = 2*M+1) diags:
    //
    static_assert(M == 1 || M == 2, "Grid2D_ADI: Invalid Stencil Size");
    assert(M == this->m_sts / 2);

    // NB: the "m" and "n" values may have changed, get them again:
    int m = int(this->m_Sx.size());
    int n = int(this->m_Vx.size());
    if (m <= 2*M || n <= 2*M)
      throw invalid_argument
            ("Grid2D_ADI Ctor: Grid size too small (degenerate?)");

    // Allocate the solution space at half-steps:
    m_fh      =
      (!this->UseCUDA())
      ? new F[m * n]
      : this->m_cudaEnv->template CUDAAlloc<F>(m * n);

    // Allocate the Diags and RHSs:
    int K = (m-2*M) * (n-2*M);
    assert(K >= 1);
    if (!this->UseCUDA())
    {
      // Host-based allocation:
      m_diags = new F[K * this->m_sts];
      m_rhss  = new F[K];
    }
    else
    {
      // CUDA-based allocation:
      m_diags = this->m_cudaEnv->template CUDAAlloc<F>(K * this->m_sts);
      m_rhss  = this->m_cudaEnv->template CUDAAlloc<F>(K);
    }

    //-----------------------------------------------------------------------//
    // Schedule for CUDA Threads:                                            //
    //-----------------------------------------------------------------------//
    m_cudaSch0 = nullptr;
    m_cudaSch1 = nullptr;

    if (this->UseCUDA())
    {
      // 0th Sub-Step:  ImplS: LI=m, LE=n:
      m_cudaSch0 = this->m_cudaEnv->MkSched2D(n-2*M, m-2*M);

      // 1st Sub-Step: !ImplS: LI=n, LE=m:
      m_cudaSch1 = this->m_cudaEnv->MkSched2D(m-2*M, n-2*M);
    }
  }

  //=========================================================================//
  // Dtor:                                                                   //
  //=========================================================================//
  template<typename F, int M>
  Grid2D_ADI<F,M>::~Grid2D_ADI()
  {
    // Explicitly destroy "DataADI" objects:
    m_HD.Destroy(*this, false);
    m_CD.Destroy(*this, true );

    if (!this->UseCUDA())
    {
      // Free up the Host memory:
      delete[] m_fh;
      delete[] m_diags;
      delete[] m_rhss;
    }
    else
    {
      // Free up the CUDA memory:
      this->m_cudaEnv->Stop();  // Just in case...
      this->m_cudaEnv->template CUDAFree<F>(m_fh);
      this->m_cudaEnv->template CUDAFree<F>(m_diags);
      this->m_cudaEnv->template CUDAFree<F>(m_rhss);
      this->m_cudaEnv->template CUDAFree<CUDAEnv::Sched2D>(m_cudaSch0);
      this->m_cudaEnv->template CUDAFree<CUDAEnv::Sched2D>(m_cudaSch1);
    }
  }

  //=========================================================================//
  // "DoSubStepGen": A Generic Fractional Step:                              //
  //=========================================================================//
  // NB:
  // (*) this method is templated for efficiency;
  // (*) 0th sub-step: "ImplS" is "true";  *p contains previous result;
  //     new result is stored  in "m_fh";
  //     1st sub-step: "ImplS" is "false"; previous result is in "m_fh";
  //     new result is stored  in "*p":
  // (*) in CUDA mode, "m_fh"  is CUDA-allocated;
  // (*) in "t_f{curr|next}", "f" stands for "fractional" -- this may not be
  //     the full step' beginning or end;
  // (*) however, "dty" is the Year Fraction for the FULL step:
  //
  template<typename F, int M>
  template<bool IsFwd, bool IsPRADI, bool ImplS>
  void Grid2D_ADI<F,M>::DoSubStepGen
  (
    typename Grid2D<F>::DRType  DR,
    DateTime                    t_fcurr,
    F*                          p,
    DateTime                    t_fnext,
    F                           dty,
    TradingCalendar::CalReg     reg,
    BoundConds2D<F> const&      bcs
  )
  const
  {
    //-----------------------------------------------------------------------//
    // Data in Host Memory:                                                  //
    //-----------------------------------------------------------------------//
    // Sub-step with  ImplS: 0 (expl) -> 1 (impl)
    // sub-step with !ImplS: 1 (expl) -> 0 (impl):
    //
    // Compute the Diffusion Coeffs at "t_fnext" ("*I"):
    //
    this->m_diff->GetSDECoeffs
    (
      this->m_Sx,   this->m_Vx,      YF<F>(this->StartTime(), t_fnext), reg,
      m_HD.template   muSI<ImplS>(), m_HD.template sigS1I<ImplS>(),
      m_HD.template sigS2I<ImplS>(), m_HD.template   muVI<ImplS>(),
      m_HD.template  sigVI<ImplS>(), m_HD.template   rhoI<ImplS>(),
      (DR != Grid2D<F>::NoDR) ? m_HD.template irI<ImplS>() : nullptr
    );

    // If there was a regime change, and the Diffusion is regime-switching, we
    // need to re-compute the "*E" coeffs at "t_fcurr" with the new regime:
    //
    if (reg != m_regE && this->m_diff->HasRegime())
    {
      // Whatever switch occurs, one of the regimes must be "TradingHours":
      // there is never a switch between "OverNight" and "WeekEndHol":
      //
      assert(reg    == TradingCalendar::TradingHours ||
             m_regE == TradingCalendar::TradingHours);

      // Re-compute the "*E" coeffs:
      this->m_diff->GetSDECoeffs
      (
        this->m_Sx,   this->m_Vx,      YF<F>(this->StartTime(), t_fcurr), reg,
        m_HD.template   muSE<ImplS>(), m_HD.template sigS1E<ImplS>(),
        m_HD.template sigS2E<ImplS>(), m_HD.template   muVE<ImplS>(),
        m_HD.template  sigVE<ImplS>(), m_HD.template   rhoE<ImplS>(),
        (DR != Grid2D<F>::NoDR) ? m_HD.template irE<ImplS>() : nullptr
      );
    }

    // Pre-compute the Bound Conds at "t_fnext" and store them in "m_HD":
    //
    int      m  = int(this->m_Sx.size());
    int      n  = int(this->m_Vx.size());
    assert(m == m_HD.m() && m == m_CD.m() && n == m_HD.n() && n == m_CD.n());

    F const* Sx = &(this->m_Sx[0]);
    F const* Vx = &(this->m_Vx[0]);

    BCPtrs2D<F,M>& bcps = m_HD.GetBCPtrs();       // Alias!
    bcs.template PreCompute<M>(m, Sx, n, Vx, t_fnext, &bcps);

    // Some linear coeffs. NB: "c1E" and "c2E" are used only in PR-ADI, and
    // they differ by the factor 0.5 from the corresp Implicit coeffs; "c3"
    // is Explicit only in any case:
    //
    F hS  = Sx[1] - Sx[0];
    F hV  = Vx[1] - Vx[0];

    F c1I = dty / (ImplS ? hS : hV);
    F c2I = c1I / (ImplS ? hS : hV) / F(2.0);
    F c1E = F(0.0);
    F c2E = F(0.0);
    F c3E = dty / hS / hV / F(2.0);
    if (IsPRADI)
    {
      c1I /= F(2.0);
      c2I /= F(2.0);
      c1E = dty / (ImplS ? hV : hS) / F(2.0);
      c2E = c1E / (ImplS ? hV : hS) / F(2.0);
      // c2E is unchanged
    }
    F* LECs = m_HD.LECs();
    LECs[0] = c1I;
    LECs[1] = c2I;
    LECs[2] = c1E;
    LECs[3] = c2E;
    LECs[4] = c3E;
    LECs[5] = dty;

    // "Implicit" (next) and "Explicit" (curr) solution spaces:
    F*       PI = ImplS ? m_fh : p;
    F const* PE = ImplS ? p    : m_fh;

    //-----------------------------------------------------------------------//
    // Run the Core Solver:                                                  //
    //-----------------------------------------------------------------------//
    // NB: Do both static and dynamic checks for CUDA availability, to avoid
    // link errors:
    //
    if (!this->UseCUDA())
    {
      //---------------------------------------------------------------------//
      // CPU-based solver:                                                   //
      //---------------------------------------------------------------------//
      // NB: CUDA Schedule is NULL:
      assert(p != this->m_cudaP);

      ADISubStepIter_Core<F, M,  IsFwd,   IsPRADI, ImplS>
        (DR, m_HD, nullptr, PI, PE, m_diags, m_rhss);
    }
    else
    {
      //---------------------------------------------------------------------//
      // GPU-based solver:                                                   //
      //---------------------------------------------------------------------//
      assert(p == this->m_cudaP);

      // Start the CUDA section:
      this->m_cudaEnv->Start();

      // Copy the recenty-computed "*I" coeffs and Bound Conds into the CUDA
      // device memory. NB: host and device coeffs are interleaved same way,
      // so the offset is also the same.
      // NB: The "*E" block is already in the CUDA memory, either from the
      // prev step, or from the initialisation code, UNLESS it was re-computed.
      // In the former case, copy only the "*I" block and bound conds; in the
      // latter case, copy all the data:
      //
      F*     from =
        (reg == m_regE)
        ? m_HD.template GetIBlockPtr<ImplS>()
        : m_HD.GetFullData();

      F*     to   =
        (reg == m_regE)
        ? m_CD.template GetIBlockPtr<ImplS>()
        : m_CD.GetFullData();

      int    len  =
        (reg == m_regE)
        ? m_HD.GetIBlockLen()
        : m_HD.DataLen();

      // Do the actual copying:
      this->m_cudaEnv->template ToCUDA<F>(from, to, len);

      // Get the CUDA Schedule: "0" for 0th sub-step (ImplS), "1" otherwise:
      CUDAEnv::Sched2D const* sched = ImplS ? m_cudaSch0 : m_cudaSch1;

      // Launch the kernel:
      //
      ADI_KernelInvocator<F, M, IsFwd, IsPRADI, ImplS>::Run
        (*(this->m_cudaEnv), DR, m_CD, sched,   PI, PE, m_diags, m_rhss);

      // NB: Do NOT copy the solution into Host memory: ths user call-back,
      // when invoked on Host side, may copy CUDA data back into Host mem:

      // Stop the CUDA timer. NB: Timing implies synchronisation:
      this->m_cudaEnv->Stop();
    }

    // Finally, memoise the last regime:
    m_regE = reg;
  }

  //=========================================================================//
  // "InitInduct":                                                           //
  //=========================================================================//
  template<typename F, int M>
  void Grid2D_ADI<F,M>::InitInduct
  (
    bool                        isFwd,
    typename Grid2D<F>::DRType  DR,
    DateTime                    t_from,
    BoundConds2D<F> const&      bcs
  )
  const
  {
    //-----------------------------------------------------------------------//
    // Generic Initialisations:                                              //
    //-----------------------------------------------------------------------//
    // "IsFwd" requires "NoDR":
    assert(!isFwd || DR == Grid2D<F>::NoDR);

    // "IsFwd" also requires (S0,V0) to be on the Grid, otherwise the initial
    // solution cannot be formed correctly:
    //
    if (isFwd && !(this->IsS0OnGrid() && this->IsV0OnGrid()))
      throw invalid_argument
            ("Grid2D_ADI::InitInduct: S0, V0 must be on Grid in Fwd mode");

    // Pre-evaluate the diffusion coeffs at "t_from".    They will be used as
    // Explicit coeffs in the first (ImplS == true) sub-step. Also initialise
    // the Regime:
    //
    F* muS0     = m_HD.template muSE  <true>();
    F* sigS10   = m_HD.template sigS1E<true>();
    F* sigS20   = m_HD.template sigS2E<true>();
    F* muV0     = m_HD.template muVE  <true>();
    F* sigV0    = m_HD.template sigVE <true>();
    F* corrSV0  = m_HD.template rhoE  <true>();
    F* ir0      = m_HD.template irE   <true>(); // May be unused
    m_regE      =
      isFwd
      ? GetRegE<true> (t_from, this->m_cal.get())
      : GetRegE<false>(t_from, this->m_cal.get());

    this->m_diff->GetSDECoeffs
    (
      this->m_Sx,   this->m_Vx,   YF<F>(this->StartTime(), t_from), m_regE,
      muS0, sigS10, sigS20, muV0, sigV0,  corrSV0,
      (DR != Grid2D<F>::NoDR) ? ir0 : nullptr
    );
    //-----------------------------------------------------------------------//
    // CUDA-Specific Initialisations:                                        //
    //-----------------------------------------------------------------------//
    if (this->UseCUDA())
    {
      // Put the initial coeffs ("*0") into the CUDA device memory (but NOT the
      // solution space -- this is done by the caller):
      //
      F*     from = m_HD.Get0BlockPtr();
      F*     to   = m_CD.Get0BlockPtr();
      int    len  = m_HD.Get0BlockLen();
      assert(len == m_CD.Get0BlockLen());

      this->m_cudaEnv->template ToCUDA<F>(from, to, len);

      // BEWARE: Do not forget to set Bound Cond Types on "m_CD"  (on "m_HD",
      // they will be set automatically when Bound Conds are computed at each
      // step -- XXX: we assume that Bound Cond Types do not change from one
      // step to another):
      //
      BCPtrs2D<F,M>& cd_bcps = m_CD.GetBCPtrs();
      cd_bcps.m_LoST = bcs.GetLoST();
      cd_bcps.m_UpST = bcs.GetUpST();
      cd_bcps.m_LoVT = bcs.GetLoVT();
      cd_bcps.m_UpVT = bcs.GetUpVT();
    }
  }

  //=========================================================================//
  // "DoOneStep":                                                            //
  //=========================================================================//
  template<typename F, int M>
  F* Grid2D_ADI<F,M>::DoOneStep
  (
    bool                        isFwd,
    typename Grid2D<F>::DRType  DR,
    int                         UNUSED_PARAM(step_k),
    DateTime                    t_curr,
    F*                          p,
    DateTime                    t_next,
    TradingCalendar::CalReg     reg,
    BoundConds2D<F> const&      bcs
  )
  const
  {
    // "IsFwd" requires "NoDR":
    assert(!isFwd || DR == Grid2D<F>::NoDR);

    // Full step and the mid-time:
    F dty =  YF<F>(t_curr, t_next);
    assert(dty != F(0.0));

    DateTime t_h = MidPoint<F>(t_curr, t_next);

    //-----------------------------------------------------------------------//
    // 0th Sub-Step: t_curr  -> t_h, Implicit solution wrt "S":              //
    //-----------------------------------------------------------------------//
    // Boundary conds are applied automatically:
    // <IsFwd, IsPRADI, ImplS = true>:
    //
    if (isFwd)
      if (m_isPRADI)
        DoSubStepGen<true,  true,  true> (DR, t_curr, p, t_h, dty, reg, bcs);
      else
        DoSubStepGen<true,  false, true> (DR, t_curr, p, t_h, dty, reg, bcs);
    else
      // !isFwd:
      if (m_isPRADI)
        DoSubStepGen<false, true,  true> (DR, t_curr, p, t_h, dty, reg, bcs);
      else
        DoSubStepGen<false, false, true> (DR, t_curr, p, t_h, dty, reg, bcs);

    //-----------------------------------------------------------------------//
    // 1st Sub-Step: t_h  -> t_next, Implicit solution wrt "V":              //
    //-----------------------------------------------------------------------//
    // Boundary conds are applied automatically:
    // <IsFwd, IsPRADI, ImplS = false>:
    //
    if (isFwd)
      if (m_isPRADI)
        DoSubStepGen<true,  true,  false>(DR, t_h, p, t_next, dty, reg, bcs);
      else
        DoSubStepGen<true,  false, false>(DR, t_h, p, t_next, dty, reg, bcs);
    else
      // !isFwd:
      if (m_isPRADI)
        DoSubStepGen<false, true,  false>(DR, t_h, p, t_next, dty, reg, bcs);
      else
        DoSubStepGen<false, false, false>(DR, t_h, p, t_next, dty, reg, bcs);

    // After 2 sub-steps, the result is back in "p":
    return p;
  }
};
