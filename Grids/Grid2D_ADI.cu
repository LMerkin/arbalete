// vim:ts=2:et
//===========================================================================//
//                             "Grid2D_ADI.cu":                              //
//               CUDA Kernels for PR-ADI and Yanenko Methods                 //
//===========================================================================//
#include "Grids/Grid2D_ADI_Core.hpp"

namespace Arbalete
{
  //=========================================================================//
  // All Instances of "ADI_KernelInvocator":                                 //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // Over "double":                                                          //
  //-------------------------------------------------------------------------//
  // With 3-point Stencils (M==1):
  //
  template struct ADI_KernelInvocator<double, 1, false, false, false>;
  template struct ADI_KernelInvocator<double, 1, false, false, true >;
  template struct ADI_KernelInvocator<double, 1, false, true,  false>;
  template struct ADI_KernelInvocator<double, 1, false, true,  true >;

  template struct ADI_KernelInvocator<double, 1, true,  false, false>;
  template struct ADI_KernelInvocator<double, 1, true,  false, true >;
  template struct ADI_KernelInvocator<double, 1, true,  true,  false>;
  template struct ADI_KernelInvocator<double, 1, true,  true,  true >;

  // With 5-point Stencils (M==2):
  //
  template struct ADI_KernelInvocator<double, 2, false, false, false>;
  template struct ADI_KernelInvocator<double, 2, false, false, true >;
  template struct ADI_KernelInvocator<double, 2, false, true,  false>;
  template struct ADI_KernelInvocator<double, 2, false, true,  true >;

  template struct ADI_KernelInvocator<double, 2, true,  false, false>;
  template struct ADI_KernelInvocator<double, 2, true,  false, true >;
  template struct ADI_KernelInvocator<double, 2, true,  true,  false>;
  template struct ADI_KernelInvocator<double, 2, true,  true,  true >;

  //-------------------------------------------------------------------------//
  // Over "float":                                                           //
  //-------------------------------------------------------------------------//
  // With 3-point Stencils (M==1):
  //
  template struct ADI_KernelInvocator<float,  1, false, false, false>;
  template struct ADI_KernelInvocator<float,  1, false, false, true >;
  template struct ADI_KernelInvocator<float,  1, false, true,  false>;
  template struct ADI_KernelInvocator<float,  1, false, true,  true >;

  template struct ADI_KernelInvocator<float,  1, true,  false, false>;
  template struct ADI_KernelInvocator<float,  1, true,  false, true >;
  template struct ADI_KernelInvocator<float,  1, true,  true,  false>;
  template struct ADI_KernelInvocator<float,  1, true,  true,  true >;

  // With 5-point Stencils (M==2):
  //
  template struct ADI_KernelInvocator<float,  2, false, false, false>;
  template struct ADI_KernelInvocator<float,  2, false, false, true >;
  template struct ADI_KernelInvocator<float,  2, false, true,  false>;
  template struct ADI_KernelInvocator<float,  2, false, true,  true >;

  template struct ADI_KernelInvocator<float,  2, true,  false, false>;
  template struct ADI_KernelInvocator<float,  2, true,  false, true >;
  template struct ADI_KernelInvocator<float,  2, true,  true,  false>;
  template struct ADI_KernelInvocator<float,  2, true,  true,  true >;
}
