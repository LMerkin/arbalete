// vim:ts=2:et
//===========================================================================//
//                                "DataTheta.h":                             //
//                      Data Structures for "Grid2D_Theta"                   //
//===========================================================================//
// See "DataADI.h" for the general explanation of features. Compared to "ADI"
// and "Expl", "Theta" is currently for M=1 only, without the CUDA mode, and
// without "LEC"s.
// The "DEVICE" modifiers are placeholders only -- this code is currently NOT
// compiled with NVCC:
//
#pragma once

#include "Grids/BoundConds2D.h"

namespace Arbalete
{
  //-------------------------------------------------------------------------//
  // Fwd declaration of the enclosing class:                                 //
  //-------------------------------------------------------------------------//
  template<typename F>
  class Grid2D_Theta;

  //=========================================================================//
  // "DataTheta" class:                                                      //
  //=========================================================================//
  template<typename F>
  class DataTheta
  {
  private:
    //-----------------------------------------------------------------------//
    // Data and its Components:                                              //
    //-----------------------------------------------------------------------//
    F*    m_data;         // The actual storage
    bool  m_isRateModel;  // Affects "ir*" sizes below
    int   m_K;            // Length of "m_data" (in "F" sizes)

    // Alias ptrs to "m_data" components:
    // The "1" block:
    F*    m_muS1;         // len = m
    F*    m_sigS11;       // len = m
    F*    m_sigS21;       // len = n
    F*    m_muV1;         // len = n
    F*    m_sigV1;        // len = n
    F*    m_rho1;         // len = 1
    F*    m_ir1;          // len = m  (iff "isRateModel") or 1

    // BoundConds:
    BCPtrs2D<F,1> m_bcps; // len = 2*(m+n)

    // The "0" block (for Cranck-Nicolson only):
    F*    m_muS0;         // len = m
    F*    m_sigS10;       // len = m
    F*    m_sigS20;       // len = n
    F*    m_muV0;         // len = n
    F*    m_sigV0;        // len = n
    F*    m_rho0;         // len = 1
    F*    m_ir0;          // len = m (iff "isRateModel") or 1

    F*    m_end;          // m_end - m_data == m_K

  public:
    //-----------------------------------------------------------------------//
    // Non-Default Ctor:                                                     //
    //-----------------------------------------------------------------------//
    // All required params are taken from the enclosing Grid2D (Host-Only):
    //
    DataTheta(Grid2D_Theta<F> const& grid);

    //-----------------------------------------------------------------------//
    // Explicit Dtor:                                                        //
    //-----------------------------------------------------------------------//
    // XXX: To be invoked manually from the "Grid2D_Theta" dtor (Host-Only):
    //
    void Destroy();

    //-----------------------------------------------------------------------//
    // Access Methods:                                                       //
    //-----------------------------------------------------------------------//
    // Provide alternate access depending on the "Rotator" flag:
    //
    // Implicit ("*I): for the Fully-Implicit method (!IsCN), always return the
    // corresp "*1" ptrs; otherwise, act according to the "Rotator":
    //
    template<bool IsCN, bool Rotator>
    DEVICE F* muSI()    const
      { return  (!IsCN || Rotator) ? m_muS1   : m_muS0;   }

    template<bool IsCN, bool Rotator>
    DEVICE F* sigS1I()  const
      { return  (!IsCN || Rotator) ? m_sigS11 : m_sigS10; }

    template<bool IsCN, bool Rotator>
    DEVICE F* sigS2I()  const
      { return  (!IsCN || Rotator) ? m_sigS21 : m_sigS20; }

    template<bool IsCN, bool Rotator>
    DEVICE F* muVI()    const
      { return  (!IsCN || Rotator) ? m_muV1   : m_muV0;   }

    template<bool IsCN, bool Rotator>
    DEVICE F* sigVI()   const
      { return  (!IsCN || Rotator) ? m_sigV1  : m_sigV0;  }

    template<bool IsCN, bool Rotator>
    DEVICE F* rhoI()    const
      { return  (!IsCN || Rotator) ? m_rho1   : m_rho0;   }

    template<bool IsCN, bool Rotator>
    DEVICE F* irI()     const
      { return  (!IsCN || Rotator) ? m_ir1    : m_ir0;    }

    // Explicit ("*E"): for the Fully-Implicit method (!IsCN),  they are not
    // used, so always return NULL; otherwise, act according to the "Rotator":
    //
    template<bool IsCN, bool Rotator>
    DEVICE F* muSE()    const
      { return IsCN ? (Rotator ? m_muS0   : m_muS1)   : nullptr; }

    template<bool IsCN, bool Rotator>
    DEVICE F* sigS1E()  const
      { return IsCN ? (Rotator ? m_sigS10 : m_sigS11) : nullptr; }

    template<bool IsCN, bool Rotator>
    DEVICE F* sigS2E()  const
      { return IsCN ? (Rotator ? m_sigS20 : m_sigS21) : nullptr; }

    template<bool IsCN, bool Rotator>
    DEVICE F* muVE()    const
      { return IsCN ? (Rotator ? m_muV0   : m_muV1)   : nullptr; }

    template<bool IsCN, bool Rotator>
    DEVICE F* sigVE()   const
      { return IsCN ? (Rotator ? m_sigV0  : m_sigV1)  : nullptr; }

    template<bool IsCN, bool Rotator>
    DEVICE F* rhoE()    const
      { return IsCN ? (Rotator ? m_rho0   : m_rho1)   : nullptr; }

    template<bool IsCN, bool Rotator>
    DEVICE F* irE()     const
      { return IsCN ? (Rotator ? m_ir0    : m_ir1)    : nullptr; }

    // Bound Conds: Returns a non-const ref, so the object can be modified by
    // the caller in any way:
    //
    DEVICE BCPtrs2D<F,1>& GetBCPtrs()   { return m_bcps; }

    // Grid Geometry is stored in "BCPtrs":
    //
    DEVICE int m() const { return m_bcps.m_m; }
    DEVICE int n() const { return m_bcps.m_n; }
  };
}
