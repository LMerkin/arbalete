// vim:ts=2:et
//===========================================================================//
//                            "DiagSolvers35.hpp":                           //
//  Solving 3-Diagonal and 5-Diagonal Linear Systems (Host and CUDA Modes)   //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Grids/DiagSolvers35.h"
#include <cassert>
#include <stdexcept>

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // 3-Diagonal Solver (M==1):                                               //
  //=========================================================================//
  template<typename F>
  struct DiagSolver<F,1>
  {
    //-----------------------------------------------------------------------//
    // Solver in the Array Form:                                             //
    //-----------------------------------------------------------------------//
    // Pre-Conds:
    // (*) L >= 2
    // (*) size(diags[1])   == L
    // (*) size(diags[0,2]) == L-1
    //
    DEVICE static void Solve(int L, F* diags[3], F* rhs)
    {
#     ifndef __CUDACC__
      F const eps = F(100.0) * Eps<F>();
#     endif
      assert(L >= 2  && diags  != nullptr && rhs != nullptr);

      F* d0 = diags[0];
      F* d1 = diags[1];
      F* d2 = diags[2];
      assert(d0 != nullptr && d1 != nullptr  && d2 != nullptr);

      // Forward run: Eliminate the lower diag:
      //
      for (int i = 1; i < L; ++i)
      {
#       ifndef __CUDACC__
        if (Abs(d1[i-1]) < eps)
          throw domain_error("DiagSolver3: Singularity-1");
#       endif

        F c = d0 [i-1] / d1[i-1];
        d1 [i]  -= c * d2 [i-1];
        rhs[i]  -= c * rhs[i-1];
      }

      // Back-tracking: the result comes into "rhs":
      //
#     ifndef __CUDACC__
      if (Abs(d1[L-1]) < eps)
        throw domain_error("DiagSolver3: Singularity-2");
#     endif

      rhs[L-1] /= d1[L-1];

      for (int i = L-2; i >= 0; --i)
      {
#       ifndef __CUDACC__
        if (Abs(d1[i]) < eps)
          throw domain_error("DiagSolver3: Singularity-3");
#       endif

        rhs[i] -= d2[i] * rhs[i+1];
        rhs[i] /= d1[i];
      }
    }

    //-----------------------------------------------------------------------//
    // Solver in the Vector Form:                                            //
    //-----------------------------------------------------------------------//
    // For host code only:
    //
#   ifndef __CUDACC__
    static void Solve
      (Vector<F> diags[3], Vector<F>* rhs)
    {
      assert(diags != nullptr && rhs != nullptr);
      int const L = int(rhs->size());

      assert(int(diags[0].size()) == L-1 && int(diags[1].size()) == L &&
             int(diags[2].size()) == L-1);

      F* ds[3] = { &(diags[0][0]), &(diags[1][0]), &(diags[2][0]) };
      F* r     =   &(*rhs)[0];

      // Run the actual (array) solver:
      Solve(L, ds, r);
    }
#   endif
  };

  //=========================================================================//
  // 5-Diagonal Solvers (M==2):                                              //
  //=========================================================================//
  template<typename F>
  struct DiagSolver<F,2>
  {
    //-----------------------------------------------------------------------//
    // Solver in the Array Form:                                             //
    //-----------------------------------------------------------------------//
    // Pre-conds:
    // (*) L >= 2
    // (*) size(diags[2])   == L
    // (*) size(diags[1,3]) == L-1
    // (*) size(diags[0,4]) == L-2
    //
    DEVICE static void Solve(int L, F* diags[5], F* rhs)
    {
#     ifndef __CUDACC__
      F const eps = F(100.0) * Eps<F>();
#     endif
      assert(L >= 2 && diags != nullptr && rhs != nullptr);

      F* d0 = diags[0];
      F* d1 = diags[1];
      F* d2 = diags[2];
      F* d3 = diags[3];
      F* d4 = diags[4];
      assert
        (d0 != nullptr && d1 != nullptr && d2 != nullptr && d3 != nullptr && d4 != nullptr);

      // Forward run: Modifies d1, d2, d3 in-place; d0, d4 are preserved:
      //
#     ifndef __CUDACC__
      if (Abs(d2[0]) < eps)
        throw domain_error("DiagSolver5: Singularity-1");
#     endif

      // d2[0], d3[0] are unchanged
      d1[0] /= d2[0];
      d2[1] -= d3[0] * d1[0];

      for (int i = 1; i <= L-2; ++i)
      {
#       ifndef __CUDACC__
        if (Abs(d2[i]) < eps)
          throw domain_error("DiagSolver5: Singularity-2");
        assert(d2[i-1] != F(0.0));
#       endif

        d1[i]    = (d1[i] - d0[i-1] / d2[i-1] * d3[i-1]) / d2[i];
        d3[i]   -=  d1[i-1] * d4[i-1];
        d2[i+1] -=  d0[i-1] / d2[i-1] * d4[i-1]  + d3[i] * d1[i];
      }
#     ifndef __CUDACC__
      if (Abs(d2[L-1]) < eps)
        throw domain_error("DiagSolver5: Singularity-3");
#     endif

      // Back-tracking: Modifies "rhs" in-place;
      // rhs[0] is unchanged:
      rhs[1] -= d1[0] * rhs[0];

      for (int i = 2; i <= L-1; ++i)
      {
        assert(d2[i-2] != F(0.0));
        rhs[i] -= d0[i-2] / d2[i-2] * rhs[i-2] + d1[i-1] * rhs[i-1];
      }

      // The solution is formed in "rhs":
      //
      assert(d2[L-1] != F(0.0) && d2[L-2] != F(0.0));
      rhs[L-1] /= d2[L-1];
      rhs[L-2]  = (rhs[L-2] - d3[L-2] * rhs[L-1]) / d2[L-2];

      for (int i = L-3; i >= 0; --i)
      {
        assert(d2[i] != F(0.0));
        rhs[i] = (rhs[i] - d3[i] * rhs[i+1] - d4[i] * rhs[i+2]) / d2[i];
      }
      // All done!
    }

    //-----------------------------------------------------------------------//
    // Solver in the Vector Form:                                            //
    //-----------------------------------------------------------------------//
    // For host code only:
    //
#   ifndef __CUDACC__
    static void Solve(Vector<F> diags[5], Vector<F>* rhs)
    {
      assert(diags != nullptr && rhs != nullptr);
      int const L = int(rhs->size());

      assert(int(diags[0].size()) == L-2 && int(diags[1].size()) == L-1 &&
             int(diags[2].size()) == L   && int(diags[3].size()) == L-1 &&
             int(diags[4].size()) == L-2);

      F* ds[5] = { &(diags[0][0]), &(diags[1][0]), &(diags[2][0]),
                   &(diags[3][0]), &(diags[4][0]) };
      F* r     =   &(*rhs)[0];

      // Run the actual (array) solver:
      Solve(L, ds, r);
    }
#   endif
  };
}
