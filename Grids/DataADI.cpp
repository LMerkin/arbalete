// vim:ts=2:et
//===========================================================================//
//                              "DataADI.cpp":                               //
//           Pre-Defined Instances of "DataADI" (Host-Based only):           //
//===========================================================================//
#include "Grids/DataADI.hpp"

namespace Arbalete
{
  //=========================================================================//
  // "DataADI" Instances:                                                    //
  //=========================================================================//
  // 3-Point Stencil:
  template class DataADI<double, 1>;
  template class DataADI<float,  1>;

  // 5-Point Stencil:
  template class DataADI<double, 2>;
  template class DataADI<float,  2>;
}
