// vim:ts=2:et
//===========================================================================//
//                          "TimeStepper_Fact.h":                            //
//    Factory Producing Time Steppers for Explicit ODE / PDE Integration     //
//===========================================================================//
#pragma once

#include "Common/CUDAEnv.h"
#include "Grids/TimeStepper.h"
#include <string>

namespace Arbalete
{
  //=========================================================================//
  // "MkTimeStepper":                                                        //
  //=========================================================================//
  // Currently supported time steppers:
  // (*) RKC1_s (s >= 2) (1st-order Runge-Kutta-Chebyshev)
  // (*) RKC2_s (s >= 2) (2nd-order Runge-Kutta-Chebyshev)
  // (*) RKF5            (5th-order Runge-Kutta-Fehlberg )
  //
  // "K": dimensionality of the ODE system to solve (>= 1);
  //
  template<typename F>
  TimeStepper<F>* MkTimeStepper
  (
    std::string const& stepperName,
    int  K,
    std::shared_ptr<CUDAEnv> const& cudaEnv
  );
}
