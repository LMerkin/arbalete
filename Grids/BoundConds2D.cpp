// vim:ts=2:et
//===========================================================================//
//                             "BoundConds2D.cpp":                           //
//                   Pre-Defined Instants of "BoundConds2D"                  //
//===========================================================================//
#include "Grids/BoundConds2D.hpp"

namespace Arbalete
{
  //=========================================================================//
  // "BCPtrs2D":                                                             //
  //=========================================================================//
  template struct BCPtrs2D<double,1>;
  template struct BCPtrs2D<double,2>;

  template struct BCPtrs2D<float, 1>;
  template struct BCPtrs2D<float, 2>;

  //=========================================================================//
  // "BoundConds2D::ApplyDirect":                                            //
  //=========================================================================//
  // <double, M=1>:
  template void BoundConds2D<double>::ApplyDirect<1>
    (int m, double const* S, int n, double const* V, DateTime t, double* f)
  const;

  // <double, M=2>:
  template void BoundConds2D<double>::ApplyDirect<2>
    (int m, double const* S, int n, double const* V, DateTime t, double* f)
  const;

  // <float,  M=1>:
  template void BoundConds2D<float>::ApplyDirect<1>
    (int m, float  const* S, int n, float  const* V, DateTime t, float*  f)
  const;

  // <float, M=2>:
  template void BoundConds2D<float>::ApplyDirect<2>
    (int m, float  const* S, int n, float  const* V, DateTime t, float*  f)
  const;

  //=========================================================================//
  // "BoundConds2D::PreCompute":                                             //
  //=========================================================================//
  // <double, M=1>
  template void BoundConds2D<double>::PreCompute<1>
  (
    int m, double const* S, int n, double const* V, DateTime t,
    BCPtrs2D<double,1>*  res
  )
  const;

  // <double, M=2>
  template void BoundConds2D<double>::PreCompute<2>
  (
    int m, double const* S, int n, double const* V, DateTime t,
    BCPtrs2D<double,2>*  res
  )
  const;

  // <float, M=1>
  template void BoundConds2D<float>::PreCompute<1>
  (
    int m, float  const* S, int n, float  const* V, DateTime t,
    BCPtrs2D<float, 1>*  res
  )
  const;

  // <float, M=2>
  template void BoundConds2D<float>::PreCompute<2>
  (
    int m, float  const* S, int n, float  const* V, DateTime t,
    BCPtrs2D<float, 2>*  res
  )
  const;
}
