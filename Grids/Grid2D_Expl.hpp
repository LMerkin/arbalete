// vim:ts=2:et
//===========================================================================//
//                            "Grid2D_Expl.cpp":                             //
//                2D PDE Grids with Explicit Time Propagation                //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Common/CUDAEnv.hpp"
#include "Grids/Grid2D_Expl.h"
#include "Grids/DataExpl.h"
#include "Grids/TimeStepper_Fact.h"
#include "Grid2D_Expl_Core.hpp"

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // Non-Default Ctor:                                                       //
  //=========================================================================//
  template<typename F, int M>
  Grid2D_Expl<F,M>::Grid2D_Expl
  (
    shared_ptr<Diffusion2D<F>> const& diff,
    typename Grid2D<F>::Params const& params
  )
  : Grid2D<F>(diff,  params),
    m_HD     (*this, false),
    m_CD     (*this, true)
  {
    // The Grid must be equally-spaced, with stencil size of 3 or 5:
    static_assert(M == 1 || M == 2, "Grid2D_Expl: Invalid Stencil Size");
    assert(M == this->m_sts / 2);

    // NB: "m" and "n" may have changed, so get them again:
    int m = int(this->m_Sx.size());
    int n = int(this->m_Vx.size());
    int K = m * n;
    if (m <= 2*M || n <= 2*M)
      throw invalid_argument
            ("Grid2D_Expl Ctor: Grid size too small (degenerate?)");

    // Attach the time stepper:
    m_stepper =
      MkTimeStepper<F>(params.m_method, K, this->m_cudaEnv);
    assert(m_stepper != nullptr && m_stepper->GetK() == K);

    // LECs: they are initialised for "m_HD" and then copied into "m_CD" at
    // each step along with other data (XXX):
    //
    F* LECs = m_HD.LECs();
    F  hS   = this->m_Sx[1] - this->m_Sx[0];
    F  hV   = this->m_Vx[1] - this->m_Vx[0];
    LECs[0] = hS;
    LECs[1] = hV;
    LECs[2] = hS * hS;
    LECs[3] = hV * hV;
    LECs[4] = hS * hV;

    // Construct the CUDA Threads Schedule (NB: this is for "DDt" only,  using
    // the reduced "K1"; solving the corresp ODE system will have its own CUDA
    // schedule with full "K"):
    int    K1 = (m-2*M) * (n-2*M);
    m_cudaSch =
      (!this->UseCUDA())
      ? nullptr
      : this->m_cudaEnv->MkSched1D(K1);
  }

  //=========================================================================//
  // Dtor:                                                                   //
  //=========================================================================//
  template<typename F, int M>
  Grid2D_Expl<F,M>::~Grid2D_Expl()
  {
    // Explicitly destroy the Stepper and HD/CD:
    delete m_stepper;
    m_HD.Destroy(*this, false);
    m_CD.Destroy(*this, true );

    if (this->UseCUDA())
    {
      assert(m_cudaSch != nullptr);
      this->m_cudaEnv->template CUDAFree<CUDAEnv::Sched1D>(m_cudaSch);
    }
  }

  //=========================================================================//
  // "DDt" for both Fwd and Bwd Equations:                                   //
  //=========================================================================//
  // NB: This function is called from ODE RHS, so in the CUDA mode, "p" and
  // "DpDt" point to the CUDA memory:
  //
  template<typename F, int M>
  template<bool IsFwd>
  void Grid2D_Expl<F,M>::DDt
  (
    typename Grid2D<F>::DRType  DR,
    DateTime                    t,
    TradingCalendar::CalReg     reg,
    F*                          p,
    F*                          DpDt,
    BoundConds2D<F> const&      bcs
  )
  const
  {
    // Standard assertions. NB: Discounting cannot be applied in Fokker-Planck:
    static_assert(M == 1 || M == 2, "Invalid stencil size");
    assert(p != nullptr && DpDt != nullptr && (DR == Grid2D<F>::NoDR || !IsFwd));

    // Get ptrs to SDE Coeffs from the compound vector:
    F* muS    = m_HD.muS  ();
    F* sigS1  = m_HD.sigS1();
    F* sigS2  = m_HD.sigS2();
    F* muV    = m_HD.muV  ();
    F* sigV   = m_HD.sigV ();
    F* rho    = m_HD.rho  ();
    F* IR     = m_HD.ir   (); // May be unused, actually

    this->m_diff->GetSDECoeffs
    (
      this->m_Sx, this->m_Vx, YF<F>(this->StartTime(), t), reg,
      muS, sigS1, sigS2, muV, sigV, rho, (DR != Grid2D<F>::NoDR) ? IR : nullptr
    );

    // Pre-compute the Bound Conds at "t" and store them in "m_HD":
    //
    int      m  = int(this->m_Sx.size());
    int      n  = int(this->m_Vx.size());
    F const* Sx = &(this->m_Sx[0]);
    F const* Vx = &(this->m_Vx[0]);

    BCPtrs2D<F,M>& bcps = m_HD.GetBCPtrs();               // Alias!
    bcs.template PreCompute<M>(m, Sx, n, Vx, t, &bcps);

    // Compute the time derivatives in all interior points:
    // NB: Do both static and synamic checks for CUDA availability, to avoid
    // link errors:
    //
    if (!this->UseCUDA())
      // CPU-based computation:
      ExplDDt2D_Core<F, M, IsFwd>(DR, m_HD, nullptr, p, DpDt);
    else
    {
      // CUDA-based computation: "p" and "DpDt" ALREADY point to CUDA memory
      // locations. Start the CUAD section:
      this->m_cudaEnv->Start();

      // Copy the Coeffs data into CUDA memory:
      this->m_cudaEnv->template ToCUDA<F>
        (m_HD.GetFullData(), m_CD.GetFullData(), m_HD.DataLen());

      // Launch the kernel:
      ExplDDt2D_KernelInvocator<F,  M, IsFwd>::Run
        (*(this->m_cudaEnv), DR, m_CD, m_cudaSch, p, DpDt);

      // The result stays in CUDA memory, there is no need to copy it back!
      // Stop the timer (requires synchronisation):
      this->m_cudaEnv->Stop();
    }
    // All done!
  }

  //=========================================================================//
  // "InitInduct":                                                           //
  //=========================================================================//
  // NB: "p" is always a Host-based Matrix:
  //
  template<typename F, int M>
  void Grid2D_Expl<F,M>::InitInduct
  (
    bool                        isFwd,
    typename Grid2D<F>::DRType  DR,
    DateTime                    UNUSED_PARAM(t_from),
    BoundConds2D<F> const&      bcs
  )
  const
  {
    // "isFwd" requires "NoDR":
    assert(!isFwd || DR == Grid2D<F>::NoDR);

    // "IsFwd" also requires (S0,V0) to be on the Grid, otherwise the initial
    // solution cannot be formed correctly:
    //
    if (isFwd && !(this->IsS0OnGrid() && this->IsV0OnGrid()))
      throw invalid_argument
            ("Grid2D_Expl::InitInduct: S0, V0 must be on Grid in Fwd mode");

    // RHS for the system of ODEs resuting from applying the method of lines to
    // the corresp PDE.
    // NB: In the CUDA mode, "pt" and "DpDt" would point to CUDA memory whereas
    // "bcs" are always located in Host memory. The "DDt" function takes care
    // of data transfers:
    // "DDt" will actually compute Bound Conds from "bcs" at each sub-step:
    //
    m_odeRHS =
      // XXX: "bcs" is a ref -- when it is copied into the Lambda below, would
      // the ref or the whole object be copied???
      [this, isFwd, DR, bcs]
      (DateTime t, TradingCalendar::CalReg reg, F* pt, F* DpDt)
      {
        if (isFwd)
          this->DDt<true> (DR, t, reg, pt, DpDt, bcs);
        else
          this->DDt<false>(DR, t, reg, pt, DpDt, bcs);
      };
  }

  //=========================================================================//
  // "DoOneStep":                                                            //
  //=========================================================================//
  template<typename F, int M>
  F* Grid2D_Expl<F,M>::DoOneStep
  (
    bool                        DEBUG_ONLY  (isFwd),
    typename Grid2D<F>::DRType  DEBUG_ONLY  (DR),
    int                         UNUSED_PARAM(step_k),
    DateTime                    t_curr,
    F*                          p,
    DateTime                    t_next,
    TradingCalendar::CalReg     reg,
    BoundConds2D<F> const&      UNUSED_PARAM(bcs)
  )
  const
  {
    // "isFwd" requires "NoDR":
    assert(!isFwd || DR == Grid2D<F>::NoDR);

    // Invoke the actual ODE Propagator: t_curr -> t_next; "p" points to either
    // Host or CUDA memory, depending on the curr mode:
    //
    m_stepper->Propagate(t_curr, reg, p, t_next, m_odeRHS);

    // NB: call seq: Propagate -> m_odeRHS -> DDt -> ExplDDt2D_Core;
    // "DDt" computes bound conds at "t", "ExplDDt2D_Core" applies them -- ie,
    // BCs are applied AT THE BEGINNING of each step and sub-step. At the very
    // end, they are applied by "Grid2D::TimeMarshal".

    // The result in "p" has been updated point-wise in-place:
    return p;
  }
}
