// vim:ts=2:et
//===========================================================================//
//                                "Grid2D.h":                                //
//                  Base Class for Uniform 2D PDE Grids                      //
//===========================================================================//
#pragma once

#include "Common/Macros.h"
#ifndef  __CUDACC__
#include "Common/CUDAEnv.h"
#include "Diffusions/Diffusion2D.h"
#include "Grids/BoundConds2D.h"
#include <memory>
#include <cassert>
#endif  // !__CUDACC__

namespace Arbalete
{
  //=========================================================================//
  // "Grid2D":                                                               //
  //=========================================================================//
  template<typename F>
  class Grid2D
  {
  public:
    //=======================================================================//
    // Public Data Types:                                                    //
    //=======================================================================//
    //-----------------------------------------------------------------------//
    // "DRType":                                                             //
    //-----------------------------------------------------------------------//
    enum DRType
    {
      NoDR      = 0,  // No discounting: Fwd|Bwd integrating for prob density
      ScalarDR  = 1,  // A scalar DR -- BSM in non-IR model
      VectorDR  = 2   // A vector DR (depending on "S") -- BSM in IR model
    };

    // The rest is for the Host compiler only:

# ifndef __CUDACC__
    //-----------------------------------------------------------------------//
    // "Params":                                                             //
    //-----------------------------------------------------------------------//
    // When the grid is constructed, the number of points in "S" and "V" axes
    // may be LARGER (but bever smaller) than those specified in the Params.
    // This is to make sure that the initial points "S0", "V0" lie  directly
    // on the grid:
    //
    // Grid time horisons wrt "S" and "V" may be different if the Grid  is used
    // for computation of conditional densities (conditioning is typically done
    // on "S" but not on "V");     apart from those special cases, they are the
    // same, and equal to option expiration time:
    //
    struct Params
    {
      // Function type for "fine-grade" calculation of Grid sizes: "X" stands
      // for either "S" or "V"; if provided, overrides "m" / "n" values:
      //
      typedef std::function<int(F Xmin, F Xmax, int orig_nX)> FineGrade;

      // NB: "m_hS" and m_hV" currently have no direct effect. For some Grids,
      // they may cause the corresponding "m_fineS" and "m_fineV" funcs to be
      // generated which will in turn modify "m" and "n" values.
      // Also, there is a difference between "m_dt{TH,ON,WEH}" (which, if non-
      // zero, are directly used in TimeLine generation) and "m_et{TH,N}"
      // which are only indications used in grid step checks:
      //
      std::string m_method;
      int         m_m;          // Suggested number of points in the "S" axis
      int         m_n;          // Suggested number of points in the "V" axis
      F           m_hS;         // Suggested "S" step (to construct "m_fineS")
      F           m_hV;         // Suggested "V" step (to construct "m_fineV")
      int         m_sts;        // Stencil size in both S & V dims
      Time        m_TS;         // Time horison from diff start time, for "S"
      Time        m_TV;         // Same for "V"
      Time        m_dtTH;       // Suggested time step, trading hours
      Time        m_dtON;       // Suggested time step, over-night
      Time        m_dtWEH;      // Suggested time step, week-ends/holidays
      Time        m_etTH;       // Another indicated time step, trading hours
      Time        m_etN;        // ... and non-trading hours (see above)
      F           m_LoS;        // Lower "S" bound (default = auto-select)
      F           m_UpS;        // Upper "S" bound (ditto)
      bool        m_LoSFixed;   // "S" bounds are not auto-adjustable
      bool        m_UpSFixed;   //
      F           m_LoV;        // Lower "V" bound (ditto)
      F           m_UpV;        // Upper "V" bound (ditto)
      bool        m_LoVFixed;   // "V: bounds are not auto-adjustable
      bool        m_UpVFixed;   //
      F           m_NSigmasS;   // #StdDevs      in Grid half-width wrt "S"
      F           m_NSigmasV;   // same for "V" (default: 7.0 for both)
      F           m_minRelS;    // Minimum relative Grid half-width wrt "S"
      F           m_minRelV;    // same for "V" (default: no minimum for S, V)
      F           m_maxRelS;    // As above, but max
      F           m_maxRelV;    // ditto
      bool        m_S0OnGrid;   // "S0" to be placed directly on Grid
      bool        m_V0OnGrid;   // "V0" to be placed directly on Grid
      int         m_debugLevel; // Debug mode (0: no debugging;
                                //             1: over-all info;
                                //             2: solution output at each step)
      std::shared_ptr<CUDAEnv>          m_cudaEnv;  // May be NULL here
      std::shared_ptr<TradingCalendar>  m_cal;      // ditto
      std::shared_ptr<FineGrade>        m_fineS;    // "Fine-grade m" (or NULL)
      std::shared_ptr<FineGrade>        m_fineV;    // "Fine-grade n" (or NULL)

      // Default Ctor:
      Params();

      // Test of whether the Grid Axes specified by these Params are INVARIANT,
      // that is, independent of the Diffusion Params and Initial Conds:
      //
      bool IsInvariantS() const;
      bool IsInvariantV() const;
    };

  protected:
    //-----------------------------------------------------------------------//
    // Common Grid Data Flds:                                                //
    //-----------------------------------------------------------------------//
    Vector<F>                 m_Sx;           // The "S" axis
    int                       m_i0;           // Idx of "S0" on "m_Sx" (if >=0)
    Vector<F>                 m_Vx;           // The "V" axis
    int                       m_j0;           // Idx of "V0" on "m_Vx" (if >=0)
    int                       m_sts;          // XXX: Not for all Grids?
    Time                      m_T;            // Max time horison of Grid
    Time                      m_dtTH;         // Recommended time step, day
    Time                      m_dtON;         // Same, over-night
    Time                      m_dtWEH;        // Same, week-ends/holidays

    // Grid Axes construction params:
    bool                      m_LoSFixed;
    bool                      m_UpSFixed;
    bool                      m_LoVFixed;
    bool                      m_UpVFixed;
    F                         m_NSigmasS;
    F                         m_NSigmasV;
    F                         m_minRelS;
    F                         m_minRelV;
    F                         m_maxRelS;
    F                         m_maxRelV;

    // CUDA Support:
    std::shared_ptr<CUDAEnv>  m_cudaEnv;      // CUDA Env,  always non-NULL
    mutable F*                m_cudaP;        // CUDA solution space | NULL
    F*                        m_cudaSx;       // "S" axis in CUDA    | NULL
    F*                        m_cudaVx;       // "V" axis in CUDA    | NULL
    CUDAEnv::Sched1D*         m_cudaSchQuadr; // CUDA SChedule for Quadrature

    int                       m_debugLevel;

    // Diffusion for this Grid:
    std::shared_ptr<Diffusion2D<F>>   m_diff; // Always non-NULL

    // Trading Calendar:
    std::shared_ptr<TradingCalendar>  m_cal;  // May be NULL

  public:
    //=======================================================================//
    // Non-Default Ctor, Dtor and Access:                                    //
    //=======================================================================//
    Grid2D
    (
      std::shared_ptr<Diffusion2D<F>> const& diff,
      Params                          const& params
    );

    virtual ~Grid2D();

    Vector<F> const&              GetSx() const { return m_Sx;    }
    Vector<F> const&              GetVx() const { return m_Vx;    }
    int                           GetSTS()        const { return m_sts;   }
    Time                          GetT()          const { return m_T;     }
    Diffusion2D<F>  const&        GetDiffusion()  const { return *m_diff; }
    TradingCalendar const*        GetCal()        const { return m_cal.get(); }

    int             m()           const { return  int(m_Sx.size()); }
    int             n()           const { return  int(m_Vx.size()); }

    F      GetS0()       const;
    F      GetV0()       const;
    bool   IsS0OnGrid()  const;
    bool   IsV0OnGrid()  const;
    int    GetIdxS0()    const;
    int    GetIdxV0()    const;


    // Obtaining the default time step for different regimes:
    Time            GetDt     (TradingCalendar::CalReg reg) const;

    // NB: The followig method throws an exception if CUDA is unavailable (if
    // UseCUDA() below returns false):
    CUDAEnv  const& GetCUDAEnv() const;

    // NB: "UseCUDA" may be overridden by derived "Grid2D*" classes if they
    // do not support CUDA (yet):
    virtual bool   UseCUDA()     const
      { return m_cudaEnv.get() != nullptr; }

    // Time span covered by this Grid:
    //
    DateTime StartTime()        const { return m_diff->GetT0();   }
    DateTime EndTime  ()        const { return StartTime() + m_T; }

    //-----------------------------------------------------------------------//
    // "CallBack":                                                           //
    //-----------------------------------------------------------------------//
    // User Call-Back Function Type.
    // Args:
    // "grid"   : *this;
    // "t"      : curr time;
    // "stk"    : index of "t" in the "special_times" array provided to the
    //            top-level integrator  if "t" is indeed  a "special" time;
    //            otherwise, (-1);
    // "f"      : curr solution; in the CUDA mode (UseCUDA() returns "true"),
    //            "f" points to the solution in the CUDA device memory;  the
    //            solution IS modifyable by the call-back if necessary;  XXX:
    //            the geometry of "f" space  is currently NOT  passed to the
    //            call-back -- assumed to be known anyway;
    // "next_t" : the next time point  (same as "t" at the last step);
    // "reg"    : Regime for the interval [t .. next_t] (does not change within
    //            the interval!);
    // ReturnVal: if > 0, interpreted as a *suggestion* for the next time step
    //            (i.e., adaptive time stepping can be implemented this way; it
    //            is still subject to standard constraints wrt regime switching
    //            points and "special times");
    //            if = 0, continue with the current time step;
    //            if < 0, terminate immediately:
    typedef
      std::function
        <Time(Grid2D const& grid, DateTime t,  int stk,   F* f,
              DateTime next_t,    TradingCalendar::CalReg reg)>
      CallBack;

  protected:
    //=======================================================================//
    // Common Internal Methods:                                              //
    //=======================================================================//
    //-----------------------------------------------------------------------//
    // "TimeMarshal":                                                        //
    //-----------------------------------------------------------------------//
    // Provides time marshalling with time step adjustments for both Fokker-
    // Planck and Feynman-Kac solvers.    Internally calls "InitInduct" and
    // "DoOneStep" which are purely virtual and must be implemented by  the
    // derived classes;
    // NB: this method may be useful on its own for the derived classes, e.g.
    // for implementation of various non-Finite-Difference based integrators,
    // so it is not "private". For non-FD methods, make sure that m_sts = 0.
    // Pre-condition (always): size(f) = m * n:
    //
    template<bool IsFwd>
    void TimeMarshal
    (
      DRType                        DR,
      DateTime                      t_from,
      F*                            f,
      DateTime                      t_to,
      BoundConds2D<F> const&        bcs,
      CallBack const*               call_back,
      std::vector<DateTime> const*  special_times
    )
    const;

  public:
    //-----------------------------------------------------------------------//
    // "SolveFokkerPlanck":                                                  //
    //-----------------------------------------------------------------------//
    // Solves the Forward Kolmogorov (Fokker-Planck) PDE for the configured
    // Diffusion:
    // "p" is an in-out arg: contains the initial joint density at "t_from" on
    //    input, replaced with joint density at "t_to" on output;  the initial
    //    density should be set to a 2D Delta function.
    // Pre-codition: t_from <= t_to (forward induction):
    //
    // NB: This method is made virtual as may be modified or de-configured  in
    // some derived Grids:
    //
    virtual void SolveFokkerPlanck
    (
      DateTime                     t_from,
      Matrix<F>*                   p,
      DateTime                     t_to,
      BoundConds2D<F> const&       bcs,
      CallBack const*              call_back      = nullptr,
      std::vector<DateTime> const* special_times  = nullptr
    )
    const
    {
      // NB: IsFwd=true, DR=NoDR:
      assert(p != nullptr);
      TimeMarshal<true>
      (
        DRType::NoDR, t_from, &((*p)(0,0)), t_to, bcs, call_back, special_times
      );
    }

    //-----------------------------------------------------------------------//
    // "DeltaSV":                                                            //
    //-----------------------------------------------------------------------//
    // Initialiser for "p" at t=0 in "SolveFokkerPlanck": 2D delta function:
    //
    void DeltaSV(Matrix<F>* p) const;

    //-----------------------------------------------------------------------//
    // "SolveFeynmanKac":                                                    //
    //-----------------------------------------------------------------------//
    // This includes the Black-Scholes-Merton PDE if the "with_discount" flag
    // is set.
    // Pre-condition: t_from >= t_to (backward induction).
    // Bound conds are as in "SolveFokkerPlanck":
    //
    // NB: This method is made virtual as may be modified or de-configured in
    // some derived Grids:
    //
    virtual void SolveFeynmanKac
    (
      bool                         with_discount,
      DateTime                     t_from,
      Matrix<F>*                   p,
      DateTime                     t_to,
      BoundConds2D<F> const&       bcs,
      CallBack const*              call_back      = nullptr,
      std::vector<DateTime> const* special_times  = nullptr
    )
    const
    {
      // NB: IsFwd=false, DR is variable:
      assert(p != nullptr);
      DRType DR =
        with_discount
        ? (m_diff->IsIRModel() ? DRType::VectorDR : DRType::ScalarDR)
        : DRType::NoDR;

      TimeMarshal<false>
        (DR, t_from, &((*p)(0,0)), t_to, bcs, call_back, special_times);
    }

    //=======================================================================//
    // Specific cases of "SolveFeynmanKac":                                  //
    //=======================================================================//
    //-----------------------------------------------------------------------//
    // "SolveBSM": Black-Scholes-Merton backward induction:                  //
    //-----------------------------------------------------------------------//
    void SolveBSM
    (
      DateTime                     t_from,
      Matrix<F>*                   f,
      DateTime                     t_to,
      BoundConds2D<F> const&       bcs,
      CallBack const*              call_back      = nullptr,
      std::vector<DateTime> const* special_times  = nullptr
    )
    const
    { 
      // NB: with_discount=true:
      SolveFeynmanKac(true, t_from, f, t_to, bcs, call_back, special_times);
    }

    //-----------------------------------------------------------------------//
    // "ArrowDebreu":                                                        //
    //-----------------------------------------------------------------------//
    // Computes the prob density function by backward induction, as a function
    // of state which the PDF is conditioned upon.
    //
    void ArrowDebreu
    (
      DateTime                     t_from,
      Matrix<F>*                   f,
      DateTime                     t_to,
      BoundConds2D<F> const&       bcs,
      CallBack const*              call_back      = nullptr,
      std::vector<DateTime> const* special_times  = nullptr
    )
    const
    {
      // NB: with_discount=false:
      SolveFeynmanKac(false, t_from, f, t_to, bcs, call_back, special_times);
    }

    // "DeltaS":
    // Initialiser for "f" at t=T in "ArrowDebrew": 1D delta function in "S",
    // constant in "V":
    //
    void DeltaS(Matrix<F>* p) const;

    //-----------------------------------------------------------------------//
    // "GridTest":                                                           //
    //-----------------------------------------------------------------------//
    // Run the Bwd (Feynman-Kac) Grid Solver for the specified Diffusion on the
    // initial and boundary conds being 1 (possibly with discounting), over the
    // time interval EndTime() -> StartTime().
    // If the Diffusion is linear, this produces a const=1 solution (for all S,
    // V, t) with no discounting, or solution being function of t only  with
    // discounting.
    // This method is intended for testing and validation of Grid implements.
    // NB: "f" is auto-initialised:
    // NB: This method is made virtual as may be modified or de-configured  in
    // some derived Grids:
    //
    void GridTest
    (
      Matrix<F>*      f,
      bool            with_discount,
      CallBack const* call_back = nullptr
    )
    const;

    //=======================================================================//
    // Utils / Helpers:                                                      //
    //=======================================================================//
    //-----------------------------------------------------------------------//
    // "GridDebug":                                                          //
    //-----------------------------------------------------------------------//
    // (*) Produces debugging output (into a file with the name derived  from
    //     the current time "t", located in the curr dir, with the optional
    //     "postfix");
    // (*) "is_pdf" is to be set iff the solutn is a probability density func
    //     which should normalise to 1 (normalisation is then checked);
    // (*) "f" is the curr solutn (CUDA- or Host-based, depending on whether
    //     UseCUDA() is set).
    // This method is automatically invoked from "TimeMarshal" if the "debug"
    // flag was provided to the Grid ctor.  It can also be invoked explicitly
    // by the user, e.g. from the Call-Back:
    //
    template<bool IsFwd>
    void GridDebug
    (
      DateTime            t,
      F const*            f,
      std::string const&  postfix = std::string()
    )
    const;

    //-----------------------------------------------------------------------//
    // "Quadrature2D":                                                       //
    //-----------------------------------------------------------------------//
    // Integrate p(S,V) (given as a matrix) with the given Kernel (must provide
    // overloaded operator()(S,V)).
    // XXX: "f" must point to CUDA or Host space depending on UseCUDA(); ie, it
    //      is currently NOT possible to compute quadrature of a Host-based mtx
    //      if UseCUDA() is enabled:
    //
    // (0): Internal Implementation:
    //
  private:
    template<typename Kernel>
    void Quadrature2D
      (Kernel const& kern, F const* f, int  rsize, F* res)
    const;

  public:
    // (1) For non-parameterised kernels: directly returns a value:
    //
    template<typename Kernel>
    F Quadrature2D(Kernel const& kern, F const* f) const;

    // (2) For parameterised kernels. Pre-condition:
    //     res->size() == max(1, kern.GetNParams()):
    //
    template<typename Kernel>
    void Quadrature2D
    (
      Kernel const& kern,
      F const*      f,
      Vector<F>*    res
    )
    const;

    //-----------------------------------------------------------------------//
    // "AdjustSx:"                                                           //
    //-----------------------------------------------------------------------//
    // Typically used when the PDF computed by Fokker-Planck is conditioned on
    // some S(t)=SY. Modifies "Sx" in Host and CUDA (if applicable) spaces.
    // NB: THIS IS A NON-CONST METHOD!
    // After adjustment, "SY" is placed directly on the new grid. Furthermore,
    // if the time interval to next conditioning point ("next_dt") is known, a
    // better-refined "S" axis is constructed.  The function returns the index
    // of "SY" on the new axis.
    // NB: This method is made virtual as it may be modified  or de-configured
    // in some derived Grids:
    //
    virtual int AdjustSx
    (
      DateTime  t,
      F         SY,
      Time      next_dt = Time()
    );

    //-----------------------------------------------------------------------//
    // "GetCUDATiming":                                                      //
    //-----------------------------------------------------------------------//
    // NB: The result will be inaccurate if CUDA Timing was disabled, or if
    // CUDA events have not (yet) been synchronised:
    //
    F GetCUDATiming() const
      { return UseCUDA() ?  F(m_cudaEnv->CumTimeSec()) : F(0.0); }

  protected:
    //=======================================================================//
    // Core Methods to be Implemented by Derived Classes:                    //
    //=======================================================================//
    //-----------------------------------------------------------------------//
    // "InitInduct":                                                         //
    //-----------------------------------------------------------------------//
    // Common induction initialiser invoked at the beginning of "TimeMarshal".
    // Implemented by derived classes; "p" always points to Host-based space:
    // XXX: "isFwd" cannot be made a template param here, as this is a virtual
    // method:
    //
    virtual void InitInduct
    (
      bool                   isFwd,
      DRType                 DR,
      DateTime               t_from,
      BoundConds2D<F> const& bcs
    )
    const = 0;

    //-----------------------------------------------------------------------//
    // "DoOneStep":                                                          //
    //-----------------------------------------------------------------------//
    // The core of "TimeMarshal" -- makes one step over time. Implemented by
    // derived classes. NB: "step_k" is the number of the curr step (from 0),
    // "p" is a solution space in Host or CUDA memory (depending on the curr
    // mode):
    // Returns a ptr to the solution space after this step (eg "p" itself if
    // it is updated in-place, or another one -- eg,  if alternating buffers
    // are used):
    // XXX: "isFwd" cannot be made a template param here, as this is a virtual
    // method:
    //
    virtual F* DoOneStep
    (
      bool                    isFwd,
      DRType                  DR,
      int                     step_k,
      DateTime                t_curr,
      F*                      p,
      DateTime                t_next,
      TradingCalendar::CalReg reg,
      BoundConds2D<F> const&  bcs
    )
    const = 0;

# endif // !__CUDACC__
  };
}
