// vim:ts=2:et
//===========================================================================//
//                                "DataExpl.h":                              //
//                       Data Structures for "Grid2D_Expl"                   //
//===========================================================================//
// "DataExpl" class provides continuous stotage for Coeffs of "Grid2D_Expl".
// The main objective is efficient copying of these data into  CUDA device
// memory.
// XXX: This class is for internal use within "Grid2D_Expl" only. Its instances
// are passed into CUDA kernels by copy, but a proper copy ctor cannot be used
// in CUDA. Therefore, the default copy ctor (with ptr aliasling) is used, and
// no dtor; de-allocation of "m_data" is performed by the "Grid2D_Expl" dtor:
//
#pragma once

#include "Grids/BoundConds2D.h"

namespace Arbalete
{
  //-------------------------------------------------------------------------//
  // Fwd declaration of the enclosing class:                                 //
  //-------------------------------------------------------------------------//
  template<typename F, int M>
  class Grid2D_Expl;

  //=========================================================================//
  // "DataExpl" Class:                                                       //
  //=========================================================================//
  template<typename F, int M>
  class DataExpl
  {
  private:
    //-----------------------------------------------------------------------//
    // Data and its Components:                                              //
    //-----------------------------------------------------------------------//
    F*    m_data;         // The actual storage
    int   m_K;            // Length of "m_data" (in "F" sizes)

    // Alias ptrs to "m_data" components.
    // NB: The order of components is important as they are copied to CUDA
    // memory in contiguous chunks:

    // SDE Coeffs:
    F*    m_muS;          // len = m
    F*    m_sigS1;        // len = m
    F*    m_sigS2;        // len = n
    F*    m_muV;          // len = n
    F*    m_sigV;         // len = n
    F*    m_rho;          // len = 1
    F*    m_ir;           // len = m (iff "isRateModel") or 1

    // Linear Eqns Coeffs:
    F*    m_LECs;         // len = 5: {hS, hV, hS2, hV2, hSV}

    // Bound Conds:
    BCPtrs2D<F,M> m_bcps; // len = 2*M*(m+n)

    F*    m_end;          // m_end - m_data == m_K

  public:
#   ifndef __CUDACC__
    //-----------------------------------------------------------------------//
    // Non-Default Ctor:                                                     //
    //-----------------------------------------------------------------------//
    // All required params are taken from the enclosing Grid2D (Host-Only):
    //
    DataExpl(Grid2D_Expl<F,M> const& grid, bool forCUDA);

    //-----------------------------------------------------------------------//
    // Explicit Dtor:                                                        //
    //-----------------------------------------------------------------------//
    // XXX: To be invoked manually from the "Grid2D_Expl" dtor (Host-Only).
    // Same args as for the Ctor:
    //
    void Destroy(Grid2D_Expl<F,M> const& grid, bool forCUDA);
#   endif // !__CUDACC__

    //-----------------------------------------------------------------------//
    // Access Methods:                                                       //
    //-----------------------------------------------------------------------//
    DEVICE F*   muS()   const { return m_muS;   }
    DEVICE F*   sigS1() const { return m_sigS1; }
    DEVICE F*   sigS2() const { return m_sigS2; }
    DEVICE F*   muV()   const { return m_muV;   }
    DEVICE F*   sigV()  const { return m_sigV;  }
    DEVICE F*   rho()   const { return m_rho;   }
    DEVICE F*   ir()    const { return m_ir;    }

    DEVICE F*   LECs()  const { return m_LECs;  }

    // Bound Conds: Returns a non-const ref, so the object can be modified by
    // the caller in any way:
    //
    DEVICE BCPtrs2D<F,M>& GetBCPtrs() { return m_bcps; }

    // Grid Geometry is stored in "BCPtrs":
    //
    DEVICE int m()      const { return m_bcps.m_m;   }
    DEVICE int n()      const { return m_bcps.m_n;   }

#   ifndef __CUDACC__
    // The following functions are Host-only:
    F*  GetFullData()   const { return m_data;  }
    int DataLen()       const { return m_K;     }
#   endif //!__CUDACC__
  };
}
