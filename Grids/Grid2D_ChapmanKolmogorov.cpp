// vim:ts=2:et
//===========================================================================//
//                      "Grid2D_ChapmanKolmogorov.cpp":                      //
//          Explicit Instances of the "Grid2D_ChapmanKolmogorov" Class       //
//===========================================================================//
#include "Grid2D_ChapmanKolmogorov.hpp"

namespace Arbalete
{
  template class Grid2D_ChapmanKolmogorov<double>;
  template class Grid2D_ChapmanKolmogorov<float>;
}
