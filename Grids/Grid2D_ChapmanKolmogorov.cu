// vim:ts=2:et
//===========================================================================//
//                        "Grid2D_ChapmanKolmogorov.cu":                     //
//       Pre-Defined Instances of "ChapmanKolmogorov_KernelInvocator"        //
//===========================================================================//
// XXX: This module breaks orthogonality between Diffusions and Grids (+ PDFs)
// by statically including those Diffusions for which Heat Kernel-based "Trans
// PDF" has been implemented. This is for efficiency reasons in CUDA: in versi-
// ons up to 4.X, dynamic linking of code is not supported:
//
#include "Diffusions/Diffusion2D_All.hpp"
#include "Grids/Grid2D_ChapmanKolmogorov_Core.hpp"

namespace Arbalete
{
  //-------------------------------------------------------------------------//
  // For Heston-CEV:                                                         //
  //-------------------------------------------------------------------------//
  template struct ChapmanKolmogorov2D_KernelInvocator
                  <double, Diffusion2D_HestonCEV<double>::DataHolder>;

  template struct ChapmanKolmogorov2D_KernelInvocator
                  <float,  Diffusion2D_HestonCEV<float>::DataHolder>;

  //-------------------------------------------------------------------------//
  // For "3/2"-CEV:                                                          //
  //-------------------------------------------------------------------------//
  template struct ChapmanKolmogorov2D_KernelInvocator
                  <double, Diffusion2D_3o2CEV<double>::DataHolder>;

  template struct ChapmanKolmogorov2D_KernelInvocator
                  <float,  Diffusion2D_3o2CEV<float>::DataHolder>;

  //-------------------------------------------------------------------------//
  // For SABR:                                                               //
  //-------------------------------------------------------------------------//
  template struct ChapmanKolmogorov2D_KernelInvocator
                  <double, Diffusion2D_SABR<double>::DataHolder>;

  template struct ChapmanKolmogorov2D_KernelInvocator
                  <float,  Diffusion2D_SABR<float>::DataHolder>;
}
