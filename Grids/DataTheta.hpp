// vim:ts=2:et
//===========================================================================//
//                              "DataTheta.hpp":                             //
//                     Data Structures for "Grid2D_Theta"                    //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Grids/DataTheta.h"
#include "Grids/Grid2D_Theta.h"
#include <cassert>

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "DataTheta":                                                            //
  //=========================================================================//
  //-------------------------------------------------------------------------//
  // Non-Default Ctor:                                                       //
  //-------------------------------------------------------------------------//
  template<typename F>
  DataTheta<F>::DataTheta(Grid2D_Theta<F> const& grid)
  : m_isRateModel (grid.GetDiffusion().IsIRModel())
  {
    assert(!grid.UseCUDA());

    // Calculate the total length of "m_data" to be allocated:
    //
    bool isCN = grid.IsCrankNicolson();

    int m = int(grid.GetSx().size());
    int n = int(grid.GetVx().size());
    m_bcps.m_m = m;
    m_bcps.m_n = n;
    m_K   =
      isCN
      ? (m_isRateModel ? (8*(m + n) + 2) : (6*m + 8*n + 4))
      : (m_isRateModel ? (5*(m + n) + 1) : (4*m + 5*n + 2));

    // Allocate memory (Host only) for "m_data":
    m_data      = new F[m_K];

    // Set up ptr aliases:
    // Block "1":
    m_muS1      = m_data    + 0;
    m_sigS11    = m_muS1    + m;
    m_sigS21    = m_sigS11  + m;
    m_muV1      = m_sigS21  + n;
    m_sigV1     = m_muV1    + n;
    m_rho1      = m_sigV1   + n;
    m_ir1       = m_rho1    + 1;

    // BoundConds (M==1 only):
    m_bcps.m_LoS[0] = m_ir1 + (m_isRateModel ? m : 1);
    m_bcps.m_UpS[0] = m_bcps.m_LoS[0] + n;
    m_bcps.m_LoV[0] = m_bcps.m_UpS[0] + n;
    m_bcps.m_UpV[0] = m_bcps.m_LoV[0] + m;

    // Block "0": for Cranck-Nicolson only:
    if (isCN)
    {
      m_muS0      = m_bcps.m_UpV[0] + m;
      m_sigS10    = m_muS0     + m;
      m_sigS20    = m_sigS10   + m;
      m_muV0      = m_sigS20   + n;
      m_sigV0     = m_muV0     + n;
      m_rho0      = m_sigV0    + n;
      m_ir0       = m_rho0     + 1;
      m_end       = m_ir0      + (m_isRateModel ? m : 1);
    }
    else
    {
      m_muS0 = m_sigS10 = m_sigS20 = m_muV0 = m_sigV0 = m_rho0 = m_ir0 = nullptr;
      m_end  = m_bcps.m_UpV[0] + m;
    }
    assert(m_end - m_data == m_K);
  }

  //-------------------------------------------------------------------------//
  // Explicit Dtor:                                                          //
  //-------------------------------------------------------------------------//
  template<typename F>
  void DataTheta<F>::Destroy()
  {
    if (m_data != nullptr)
      delete[] m_data;

    // Just in case, re-set all ptrs to NULL:
    m_K = 0;
    m_data = m_muS1 = m_sigS11 = m_sigS21 = m_muV1 = m_sigV1 = m_rho1 =
    m_ir1  = m_muS0 = m_sigS10 = m_sigS20 = m_muV0 = m_sigV0 = m_rho0 =
    m_ir0  = m_end  = nullptr;

    m_bcps.m_LoS[0] = m_bcps.m_UpS[0] = m_bcps.m_LoV[0] = m_bcps.m_UpV[0] =
    nullptr;
  }
}
