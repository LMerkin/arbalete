// vim:ts=2:et
//===========================================================================//
//                            "Grid2D_Theta.hpp":                            //
//          Fully-Implicit and Crank-Nicolson Methods for 2D PDEs            //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Grids/Grid2D_Theta.h"
#include "Grids/DataTheta.h"
#include "Grids/DiagSolvers35.h"
#include <mkl_pardiso.h>
#include "Grids/BoundConds2D_Core.hpp"

namespace
{
  using namespace Arbalete;
  using namespace std;

  //=========================================================================//
  // ParDiSo Error Msgs:                                                     //
  //=========================================================================//
  char const* const ParDiSo_ErrMsgs[11] =
  {
    "Inconsistent input",
    "Not enough memory",
    "Reordering problem",
    "Zero pivot",
    "Internal error",
    "Reordering failed",
    "Singular diagonal matrix",
    "32-bit integer overflow",
    "Not enough memory for OCC",
    "Cannot open OCC tmp file(s)",
    "Cannot read/write OCC data file"
  };

  //=========================================================================//
  // Bound Conds Application Utils:                                          //
  //=========================================================================//
  template<typename F>
  inline void ApplyLoS(BCPtrs2D<F,1> const& bcps, F C, int j, F* lhs, F* rhs)
  {
    // For both Dirichlet and Neumann:
    *rhs -= (C * bcps.m_LoS[0][j]);

    if (bcps.m_LoST == BCType::NeumannBCT)
      // Neumann: Also modify LHS for P(1,j): i:0->1, ent += 3:
      lhs[3] += C;
  }

  template<typename F>
  inline void ApplyUpS(BCPtrs2D<F,1> const& bcps, F C, int j, F* lhs, F* rhs)
  {
    // For both Dirichlet and Neumann:
    *rhs -= (C * bcps.m_UpS[0][j]);

    if (bcps.m_UpST == BCType::NeumannBCT)
      // Neumann: Also modify LHS for P(m-2,j): i:(m-1)->(m-2), ent -= 3:
      lhs[-3] += C;
  }

  template<typename F>
  inline void ApplyLoV(BCPtrs2D<F,1> const& bcps, F C, int i, F* lhs, F* rhs)
  {
    // For both Dirichlet and Neumann:
    *rhs -= (C * bcps.m_LoV[0][i]);

    if (bcps.m_LoVT == BCType::NeumannBCT)
      // Neumann: Also modify LHS for P(i,1): j:0->1, ent += 1:
      lhs[1] += C;
  }

  template<typename F>
  inline void ApplyUpV(BCPtrs2D<F,1> const& bcps, F C, int i, F* lhs, F* rhs)
  {
    // For both Dirichlet and Neumann:
    *rhs -= (C * bcps.m_UpV[0][i]);

    if (bcps.m_UpVT == BCType::NeumannBCT)
      // Neumann: Also modify LHS for P(i,n-2): j:(n-1)->(n-2), ent -= 1:
      lhs[-1] += C;
  }
}
  
namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // Non-Default Ctor:                                                       //
  //=========================================================================//
  template<typename F>
  Grid2D_Theta<F>::Grid2D_Theta
  (
    shared_ptr<Diffusion2D<F>> const& diff,
    typename Grid2D<F>::Params const& params
  )
  : Grid2D<F>(diff, params),
    m_HD     (*this)
  {
    if (this->m_sts != 3)
      throw invalid_argument("Grid2D_Theta Ctor: Stencil size must be 3");

    if (params.m_method == "CN")
      m_isCN = true;
    else
    if (params.m_method == "FullImpl")
      m_isCN = false;
    else
      throw invalid_argument
            ("Grid2D_Theta Ctor: Invalid scheme: "+ params.m_method);

    // NB: the "m" and "n" values may have changed, get them again:
    int m = int(this->m_Sx.size());
    int n = int(this->m_Vx.size());

    // The minimum number of grid points required: 3:
    if (m < 3 || n < 3)
      throw invalid_argument
            ("Grid2D_Theta Ctor: Grid size too small (degenerate?)");

    // Data structs for the Sparse Solver; "m_N" is the total number of non-0
    // entries in the sparse matrix; "m_R" is the number of rows:
    m_R     = (m-2)   * (n-2);
    m_N     = (3*m-8) * (3*n-8);
    assert(m_R > 0 && m_N > 0);

    m_Avals  .resize(m_N);
    m_Acols  .resize(m_N);
    m_ArowIdx.resize(m_R + 1);
    m_rhs    .resize(m_R);
    m_res    .resize(m_R);

    // Initialise the data structures specific to the ParDiSo solver: it sets
    // "m_pt" ("m_params" are later re-initialised anyway):
    //
    int mType  = 11;    // Real unsymmetric matrix
    pardisoinit(m_pt, &mType, m_params);
  }

  //=========================================================================//
  // Dtor:                                                                   //
  //=========================================================================//
  template<typename F>
  Grid2D_Theta<F>::~Grid2D_Theta()
  {
    // Explicitly destroy the "DataTheta" object:
    m_HD.Destroy();
    // XXX: Don't know how to finalise "m_pt"!
  }

  //=========================================================================//
  // "DoTimeStepGen":                                                        //
  //=========================================================================//
  template<typename F>
  template<bool IsFwd, bool IsCN, bool Rotator>
  void Grid2D_Theta<F>::DoTimeStepGen
  (
    typename Grid2D<F>::DRType  DR,
    DateTime                    t_curr,
    F*                          p,
    DateTime                    t_next,
    TradingCalendar::CalReg     reg,
    BoundConds2D<F> const&      bcs
  )
  const
  {
    //-----------------------------------------------------------------------//
    // Data Switches:                                                        //
    //-----------------------------------------------------------------------//
    // This method is NOT CUDA-enabled yet:
    assert(p != this->m_cudaP);

    F dt = YF<F>(t_curr, t_next);
    assert(dt != F(0.0) && p != nullptr);

    // Compute the Diffusion Coeffs at "t_next" ("*I"):
    //
    F* muSI     = m_HD.template muSI  <IsCN,Rotator>();
    F* sigS1I   = m_HD.template sigS1I<IsCN,Rotator>(); 
    F* sigS2I   = m_HD.template sigS2I<IsCN,Rotator>();
    F* muVI     = m_HD.template muVI  <IsCN,Rotator>();
    F* sigVI    = m_HD.template sigVI <IsCN,Rotator>();
    F* corrSVI  = m_HD.template rhoI  <IsCN,Rotator>();
    F* irI      = m_HD.template irI   <IsCN,Rotator>(); // May be unused

    this->m_diff->GetSDECoeffs
    (
      this->m_Sx, this->m_Vx, YF<F>(this->StartTime(), t_next), reg,
      muSI, sigS1I, sigS2I, muVI, sigVI, corrSVI,
      (DR != Grid2D<F>::NoDR) ? irI : nullptr
    );

    // Direct ptrs to Coeffs of the "*E" part (corresponding to "t_curr").
    // NB:
    // (*) They are NULLs in the Fully-Implicit method;
    // (*) In the Crank-Nicolson, they are taken from the prev step according
    //     to the "Rotator", UNLESS there was a regime change;
    //
    F* muSE     = m_HD.template muSE  <IsCN,Rotator>();
    F* sigS1E   = m_HD.template sigS1E<IsCN,Rotator>();
    F* sigS2E   = m_HD.template sigS2E<IsCN,Rotator>();
    F* muVE     = m_HD.template muVE  <IsCN,Rotator>();
    F* sigVE    = m_HD.template sigVE <IsCN,Rotator>();
    F* corrSVE  = m_HD.template rhoE  <IsCN,Rotator>();
    F* irE      = m_HD.template irE   <IsCN,Rotator>();

    // If there was a regime change, and the Diffusion is regime-switching, we
    //  need to re-compute the "*E" coeffs (for CN only) at "t_curr"  with the
    // new regime:
    //
    if (IsCN && reg != m_regE && this->m_diff->HasRegime())
    {
      // Whatever switch occurs, one of the regimes must be "TradingHours":
      // there is never a switch between "OverNight" and "WeekEndHol":
      //
      assert(reg    == TradingCalendar::TradingHours ||
             m_regE == TradingCalendar::TradingHours);

      // Debugging output:
      if (this->m_debugLevel >= 1)
        cout << DateTimeToStr(t_curr) << ": Regime Change: " << int(m_regE)
             << " -> " << int(reg)    << endl;

      // Re-compute the "t_curr" coeffs, but with new regime:
      this->m_diff->GetSDECoeffs
      (
        this->m_Sx, this->m_Vx, YF<F>(this->StartTime(), t_curr), reg,
        muSE, sigS1E, sigS2E, muVE, sigVE, corrSVE,
        (DR != Grid2D<F>::NoDR) ? irE : nullptr
      );
    }
    // Memoise the curr regime:
    m_regE = reg;

    // "pE" is a 1-dim ptr to the prev solution values (at "t_curr"). Also
    // get the "rho"s:
    //
    F const* pE = p;
    F rhoE      = *corrSVE;
    F rhoI      = *corrSVI;

    //
    // Some Linear Coeffs. XXX: there is no point in moving them into "m_HD"
    // now -- but for a CUDA version, we would have to do that:
    //
    F const* Sx = &(this->m_Sx[0]);
    F const* Vx = &(this->m_Vx[0]);

    F hS   = Sx[1] - Sx[0];
    F hV   = Vx[1] - Vx[0];

    F tt   = IsCN ? (F(0.5) * dt) : dt;   // theta     * dt
    F ot   = IsCN ? dt : F(0.0);          // (1-theta) * dt

    // "Implicit" coeffs:
    F KS2  = tt  / (F(2.0) * hS);
    F KSS  = tt  / (hS  * hS);
    F KSS2 = KSS /  F(2.0);
    F KV2  = tt  / (F(2.0) * hV);
    F KVV  = tt  / (hV  * hV);
    F KVV2 = KVV /  F(2.0);
    F KSV4 = tt  / (F(4.0) * hS * hV);

    // NB: the following "explicit" coeffs (with (1-theta) * dt) are required
    // for CN only, but in that case they coincide with "implicit" ones!
    //
    F CS2  = IsCN ? KS2  : F(0.0);
    F CSS  = IsCN ? KSS  : F(0.0);
    F CSS2 = IsCN ? KSS2 : F(0.0);
    F CV2  = IsCN ? KV2  : F(0.0);
    F CVV  = IsCN ? KVV  : F(0.0);
    F CVV2 = IsCN ? KVV2 : F(0.0);
    F CSV4 = IsCN ? KSV4 : F(0.0);

    F* LHS = &(m_Avals  [0]);
    int*    Rows= &(m_ArowIdx[0]);
    int*    Cols= &(m_Acols  [0]);
    F* RHS = &(m_rhs    [0]);
    F* res = &(m_res    [0]);

    // Zero-out the LHS and the RHS arrays:
    ZeroOut<F>(&m_Avals);
    ZeroOut<F>(&m_rhs);

    // Pre-compute the boundary values at "t_next" and store them in "m_HD".
    // NB: STS==3 so M==1:
    //
    int    m =  int(this->m_Sx.size());
    int    n =  int(this->m_Vx.size());
    assert(m == m_HD.m() && n == m_HD.n());

    BCPtrs2D<F,1>& bcps = m_HD.GetBCPtrs();       // Alias!
    bcs.template PreCompute<1>(m, Sx, n, Vx, t_next, &bcps);

    //-----------------------------------------------------------------------//
    // Loop over the inner (S, V) nodes:                                     //
    //-----------------------------------------------------------------------//
    int ent = 0;
    int row = 0;

    for (int i = 1; i <= m-2; ++i)
      for (int j = 1; j <= n-2; ++j, ++row)
      {
        //-------------------------------------------------------------------//
        // LHS and RHS ptrs:                                                 //
        //-------------------------------------------------------------------//
        // "row" is the number of the current equation being formed:
        // Curr "lhs" is (LHS + ent):
        //
        assert(row < m_R);
        Rows[row] = ent;

        F* rhs = RHS + row;

        //------------------------------------------------------------------//
        // Pre-compute various coeffs: For optimisation only:               //
        //------------------------------------------------------------------//
        // Pre-compute the indices in the SLE Solution Space, which is of size
        // (m-2)*(n-2).  NB: because "off" starts at (1,1), some of these inds
        // may be negative:
        //
        int k00 = row;            // ind(i,  j  ) = row
        int k0m = k00 - 1;        // ind(i,  j-1)
        int k0p = k00 + 1;        // ind(i,  j+1)

        int km0 = k00 - n + 2;    // ind(i-1,j  ) = row - (n-2);
        int kmm = km0 - 1;        // ind(i-1,j-1)
        int kmp = km0 + 1;        // ind(i-1,j+1)

        int kp0 = k00 + n - 2;    // ind(i+1,j  ) = row + (n-2)
        int kpm = kp0 - 1;        // ind(i+1,j-1)
        int kpp = kp0 + 1;        // ind(i+1,j+1)

        // Similar indices in the Coeffs and P Space, which is of size m*n and
        // is in the column-major format!
        //
        int c00 = j * m + i;
        int c0m = c00 - m;
        int c0p = c00 + m;

        int cm0 = c00 - 1;
        int cmm = cm0 - m;
        int cmp = cm0 + m;

        int cp0 = c00 + 1;
        int cpm = cp0 - m;
        int cpp = cp0 + m;

        F   C   = F(0.0);   // Current Coeff

        //-------------------------------------------------------------------//
        // (0) "--" terms:                                                   //
        //-------------------------------------------------------------------//
        C = IsFwd
            ? (- KSV4 * rhoI * sigS1I[i-1] * sigS2I[j-1] * sigVI[j-1])
            : (  KSV4 * rhoI * sigS1I[i]   * sigS2I[j]   * sigVI[j]  );

        // IMPLICIT PART:
        if (2 <= i && 2 <= j)
        {
          // Generic case (internal point): This term is part of the LHS:
          assert(ent < m_N && 0 <= kmm && kmm < m_R);
          Cols[ent]  = kmm;
          LHS [ent] += C;
          ++ent;
        }
        else
        {
          // Near the boundary: The "S" boundary conds prevail:
          assert(i == 1  || j == 1);
          if (i == 1)
            ApplyLoS<F>(bcps, C, j-1, LHS + ent, rhs);
          else
            ApplyLoV<F>(bcps, C, i-1, LHS + ent, rhs);
        }

        // EXPLICIT PART (CN only); NB: "pE" is over the whole grid, so there
        // is no need to consider boundary conds separately:
        if (IsCN)
          *rhs +=
            ( IsFwd
              ? (  CSV4 * rhoE * sigS1E[i-1] * sigS2E[j-1] * sigVE[j-1])
              : (- CSV4 * rhoE * sigS1E[i]   * sigS2E[j]   * sigVE[j]  )
            ) * pE[cmm];

        //-------------------------------------------------------------------//
        // (1) "-0" terms:                                                   //
        //-------------------------------------------------------------------//
        C = IsFwd
            ? (- KS2 * muSI[i-1] - KSS2 * Sqr(sigS1I[i-1] * sigS2I[j]))
            : (- KS2 * muSI[i]   + KSS2 * Sqr(sigS1I[i]   * sigS2I[j]));

        // IMPLICIT PART:
        if (2 <= i)
        {
          // Generic case (internal point): This term is part of the LHS:
          assert(ent < m_N && 0 <= km0 && km0 < m_R);
          Cols[ent]  = km0;
          LHS [ent] += C;
          ++ent;
        }
        else
        {
          // Near the boundary:
          assert(i == 1);
          ApplyLoS<F>(bcps, C, j, LHS + ent, rhs);
        }

        // EXPLICIT PART (CN only); NB: "pE" is over the whole grid, so there
        // is no need to consider boundary conds separately:
        if (IsCN)
          *rhs +=
            ( IsFwd
              ? (  CS2 * muSE[i-1] + CSS2 * Sqr(sigS1E[i-1] * sigS2E[j]))
              : (  CS2 * muSE[i]   - CSS2 * Sqr(sigS1E[i]   * sigS2E[j]))
            ) * pE[cm0];

        //-------------------------------------------------------------------//
        // (2) "-+" terms:                                                   //
        //-------------------------------------------------------------------//
        C = IsFwd
            ? (  KSV4 * rhoI * sigS1I[i-1] * sigS2I[j+1] * sigVI[j+1])
            : (- KSV4 * rhoI * sigS1I[i]   * sigS2I[j]   * sigVI[j]  );

        // IMPLICIT PART:
        if (2 <= i && j <= n-3)
        {
          // Generic case (internal point): This term is part of the LHS:
          assert(ent < m_N && 0 <= kmp && kmp < m_R);
          Cols[ent]  = kmp;
          LHS [ent] += C;
          ++ent;
        }
        else
        {
          // Near the boundary: The "S" boundary conds prevail:
          assert(i == 1 || j == n-2);
          if (i == 1)
            ApplyLoS<F>(bcps, C, j+1, LHS + ent, rhs);
          else
            ApplyUpV<F>(bcps, C, i-1, LHS + ent, rhs);
        }

        // EXPLICIT PART (CN only); NB: "pE" is over the whole grid, so there
        // is no need to consider boundary conds separately:
        if (IsCN)
          *rhs +=
            ( IsFwd
              ? (- CSV4 * rhoE * sigS1E[i-1] * sigS2E[j+1] * sigVE[j+1])
              : (  CSV4 * rhoE * sigS1E[i]   * sigS2E[j]   * sigVE[j]  )
            ) * pE[cmp];

        //-------------------------------------------------------------------//
        // (3) "0-" terms:                                                   //
        //-------------------------------------------------------------------//
        C = IsFwd
            ? (- KV2 * muVI[j-1] - KVV2 * Sqr(sigVI[j-1]))
            : (- KV2 * muVI[j]   + KVV2 * Sqr(sigVI[j]  ));

        // IMPLICIT PART:
        if (2 <= j)
        {
          // Generic case (internal point): This term is part of the LHS:
          assert(ent < m_N && 0 <= k0m && k0m < m_R);
          Cols[ent]  = k0m;
          LHS [ent] += C;
          ++ent;
        }
        else
        {
          // Near the boundary:
          assert(j == 1);
          ApplyLoV<F>(bcps, C, i, LHS + ent, rhs);
        }

        // EXPLICIT PART (CN only); NB: "pE" is over the whole grid, so there
        // is no need to consider boundary conds separately:
        if (IsCN)
          *rhs +=
            ( IsFwd
              ? (CV2 * muVE[j-1] + CVV2 * Sqr(sigVE[j-1]))
              : (CV2 * muVE[j]   - CVV2 * Sqr(sigVE[j]  ))
            ) * pE[c0m];

        //-------------------------------------------------------------------//
        // (4) "00" terms:                                                   //
        //-------------------------------------------------------------------//
        C = IsFwd
            ? (F(1.0) + KSS * Sqr(sigS1I[i] * sigS2I[j]) + KVV * Sqr(sigVI[j]))
            : (F(1.0) - KSS * Sqr(sigS1I[i] * sigS2I[j]) - KVV * Sqr(sigVI[j]));

        // IMPLICIT PART:
        assert(ent < m_N && 0 <= k00 && k00 < m_R);
        Cols[ent]  = k00;
        LHS [ent] += C;

        // EXPLICIT PART (CN only); NB: "pE" is over the whole grid, so there
        // is no need to consider boundary conds separately.   The RHS always
        // contains the prev solution value in this point:
        //
        *rhs += pE[c00];

        if (IsCN)
          *rhs +=
            ( IsFwd
              ? (- CSS * Sqr(sigS1E[i] * sigS2E[j]) - CVV * Sqr(sigVE[j]))
              : (  CSS * Sqr(sigS1E[i] * sigS2E[j]) + CVV * Sqr(sigVE[j]))
            ) * pE[c00];

        if (DR != Grid2D<F>::NoDR)
        {
          // THE REACTIVE TERM:
          assert(irI != nullptr && irE != nullptr);
          int kr = (DR == Grid2D<F>::ScalarDR) ? 0 : i;

          if (IsCN)
          {
            // Half-implicit, half-explicit:
            LHS[ent] -= tt * irI[kr];
            *rhs     += ot * irE[kr] * pE[c00];
          }
          else
            // Implicit only:
            LHS[ent] -= dt * irI[kr];
        }
        ++ent;

        //------------------------------------------------------------------//
        // (5) "0+" terms:                                                  //
        //------------------------------------------------------------------//
        C = IsFwd
            ? (  KV2 * muVI[j+1] - KVV2 * Sqr(sigVI[j+1]))
            : (  KV2 * muVI[j]   + KVV2 * Sqr(sigVI[j]  ));

        // IMPLICIT PART:
        if (j <= n-3)
        {
          // Generic case (internal point): This term is part of the LHS:
          assert(ent < m_N && 0 <= k0p && k0p < m_R);
          Cols[ent]  = k0p;
          LHS [ent] += C;
          ++ent;
        }
        else
        {
          // Near the boundary:
          assert(j == n-2);
          ApplyUpV<F>(bcps, C, i, LHS + ent, rhs);
        }

        // EXPLICIT PART (CN only); NB: "pE" is over the whole grid, so there
        // is no need to consider boundary conds separately:
        if (IsCN)
          *rhs +=
            ( IsFwd
              ? (- CV2 * muVE[j+1] + CVV2 * Sqr(sigVE[j+1]))
              : (- CV2 * muVE[j]   - CVV2 * Sqr(sigVE[j]  ))
            ) * pE[c0p];

        //-------------------------------------------------------------------//
        // (6) "+-" terms:                                                   //
        //-------------------------------------------------------------------//
        C = IsFwd
            ? (  KSV4 * rhoI * sigS1I[i+1] * sigS2I[j-1] * sigVI[j-1])
            : (- KSV4 * rhoI * sigS1I[i]   * sigS2I[j]   * sigVI[j]  );

        // IMPLICIT PART:
        if (i <= m-3 && 2 <= j)
        {
          // Generic case (internal point): This term is part of the LHS:
          assert(ent < m_N && 0 <= kpm && kpm < m_R);
          Cols[ent]  = kpm;
          LHS [ent] += C;
          ++ent;
        }
        else
        {
          // Near the boundary: The "S" boundary conds prevail:
          assert(i == m-2 || j == 1);
          if (i == m-2)
            ApplyUpS<F>(bcps, C, j-1, LHS + ent, rhs);
          else
            ApplyLoV<F>(bcps, C, i+1, LHS + ent, rhs);
        }

        // EXPLICIT PART (CN only); NB: "pE" is over the whole grid, so there
        // is no need to consider boundary conds separately:
        if (IsCN)
          *rhs +=
            ( IsFwd
              ? (- CSV4 * rhoE * sigS1E[i+1] * sigS2E[j-1] * sigVE[j-1])
              : (  CSV4 * rhoE * sigS1E[i]   * sigS2E[j-1] * sigVE[j]  )
            ) * pE[cpm];

        //-------------------------------------------------------------------//
        // (7) "+0" terms:                                                   //
        //-------------------------------------------------------------------//
        C = IsFwd
            ? (  KS2 * muSI[i+1] - KSS2 * Sqr(sigS1I[i+1] * sigS2I[j]))
            : (  KS2 * muSI[i]   + KSS2 * Sqr(sigS1I[i]   * sigS2I[j]));

        // IMPLICIT PART:
        if (i <= m-3)
        {
          // Generic case (internal point): This term is part of the LHS:
          assert(ent < m_N && 0 <= kp0 && kp0 < m_R);
          Cols[ent]  = kp0;
          LHS [ent] += C;
          ++ent;
        }
        else
        {
          // Near the boundary:
          assert(i == m-2);
          ApplyUpS<F>(bcps, C, j, LHS + ent, rhs);
        }

        // EXPLICIT PART (CN only); NB: "pE" is over the whole grid, so there
        // is no need to consider boundary conds separately:
        if (IsCN)
          *rhs +=
            ( IsFwd
              ? (- CS2 * muSE[i+1] + CSS2 * Sqr(sigS1E[i+1] * sigS2E[j]))
              : (- CS2 * muSE[i]   - CSS2 * Sqr(sigS1E[i]   * sigS2E[j]))
            ) * pE[cp0];

        //-------------------------------------------------------------------//
        // (8) "++" terms:                                                   //
        //-------------------------------------------------------------------//
        C = IsFwd
            ? (- KSV4 * rhoI * sigS1I[i+1] * sigS2I[j+1] * sigVI[j+1])
            : (  KSV4 * rhoI * sigS1I[i]   * sigS2I[j]   * sigVI[j]);

        // IMPLICIT PART:
        if (i <= m-3 && j <= n-3)
        {
          // Generic case (internal point): This term is part of the LHS:
          assert(ent < m_N && 0 <= kpp && kpp < m_R);
          Cols[ent]  = kpp;
          LHS [ent] += C;
          ++ent;
        }
        else
        {
          // Near the boundary: The "S" boundary conds prevail:
          assert(i == m-2 || j == n-2);
          if (i == m-2)
            ApplyUpS<F>(bcps, C, j+1, LHS + ent, rhs);
          else
            ApplyUpV<F>(bcps, C, i+1, LHS + ent, rhs);
        }

        // EXPLICIT PART (CN only); NB: "pE" is over the whole grid, so there
        // is no need to consider boundary conds separately:
        if (IsCN)
          *rhs +=
            ( IsFwd
              ? (  CSV4 * rhoE * sigS1E[i+1] * sigS2E[j+1] * sigVE[j+1])
              : (- CSV4 * rhoE * sigS1E[i]   * sigS2E[j]   * sigVE[j])
            ) * pE[cpp];

        // ALL DONE for this (i,j) index!
      }
    //
    // LHS and RHS have been formed. Install the Terminating entry in "Rows":
    //
    assert(ent == m_N && row == m_R);
    Rows[m_R]  =  m_N;  // NB: would be (m_N + 1) for 1-based indices

    //-----------------------------------------------------------------------//
    // Solve the Sparse Linear System:                                       //
    //-----------------------------------------------------------------------//
    // XXX: Currently, the Parallel Direct Solver (ParDiSo) is used. Later, the
    // solver will be made configurable:
    //
    int maxFct = 1;
    int mNum   = 1;
    int nRHSs  = 1;
    int mType  = 11;    // Real unsymmetric matrix
    int phases = 13;    // Analysis, numerical factorisation, solve, refinement
#   ifndef NDEBUG
    int msgLvl = 1;     // Debug   mode: Print statistics
#   else
    int msgLvl = 0;     // Release mode: Do not print statistics
#   endif
    int err    = 0;

    // Set the ParDiSo solver params:
    //
    for (int k = 0; k < 64; ++k)
      m_params[k] = 0;

    m_params[ 0] = 1;   // Use non-default params (specified below)
    m_params[ 1] = 3;   // Use OpenMP for fill-in reducing ordering
    m_params[ 9] = 13;  // Recommended default pivoting order
    m_params[10] = 1;   // Recommended default
    m_params[12] = 1;   // Recommended default
    m_params[20] = 1;   // Probably ignored anyway (for symm matrices only)
    m_params[26] = 1;   // Check the validity of the input matrix
    m_params[27] =      // IMPORTANT: "float" or "double" version!
      IsDouble<F>() ? 0 : 1;
    m_params[34] = 1;   // Use C-style (0-base) array indexing

    // Run the solver:
    //
    pardiso(m_pt[0], &maxFct, &mNum, &mType,  &phases, const_cast<int*>(&m_R),
            LHS,     Rows,    Cols,  nullptr, &nRHSs,  m_params,
            &msgLvl, RHS,     res,   &err);

    assert(-11 <= err && err <= 0);
    if (err < 0)
      throw domain_error(string("ERROR in ParDiSo: ")   + 
                         string(ParDiSo_ErrMsgs[-err-1]));

    //-----------------------------------------------------------------------//
    // Install the result back in "p":                                       //
    //-----------------------------------------------------------------------//
    int off = 0;
    for (int i = 1; i <= m-2; ++i)
      for (int j = 1; j <= n-2; ++j, ++off)
        // NB: "res" is "i"-first, but "p" is "j"-first!
        p[m*j+i] = res[off];
    assert(off == m_R);

    // Apply the Bound Conds to the solution (they were previously applied to
    // eqns under construction only):
    bcps.ApplyAll(p);
  }

  //=========================================================================//
  // "InitInduct":                                                           //
  //=========================================================================//
  template<typename F>
  void Grid2D_Theta<F>::InitInduct
  (
    bool                        isFwd,
    typename Grid2D<F>::DRType  DR,
    DateTime                    t_from,
    BoundConds2D<F> const&      UNUSED_PARAM(bcs)
  )
  const
  {
    // "isFwd" requires "NoDR":
    assert(!isFwd || DR == Grid2D<F>::NoDR);

    // "IsFwd" also requires (S0,V0) to be on the Grid, otherwise the initial
    // solution cannot be formed correctly:
    //
    if (isFwd && !(this->IsS0OnGrid() && this->IsV0OnGrid()))
      throw invalid_argument
            ("Grid2D_Theta::InitInduct: S0, V0 must be on Grid in Fwd mode");

    // Pre-evaluate the diffusion coeffs at "t_from" (into the "0" matrices) --
    // this is only required for Cranck-Nicolson:
    //
    if (m_isCN)
    {
      // "0" coeffs are the "E" coeffs when Rotator=true (CN mode only):
      F* muS0    = m_HD.template muSE  <true,true>();
      F* sigS10  = m_HD.template sigS1E<true,true>();
      F* sigS20  = m_HD.template sigS2E<true,true>();
      F* muV0    = m_HD.template muVE  <true,true>();
      F* sigV0   = m_HD.template sigVE <true,true>();
      F* corrSV0 = m_HD.template rhoE  <true,true>();
      F* ir0     = m_HD.template irE   <true,true>(); // May be unused
      m_regE     =
        isFwd
        ? GetRegE<true> (t_from, this->m_cal.get())
        : GetRegE<false>(t_from, this->m_cal.get());

      this->m_diff->GetSDECoeffs
      (
        this->m_Sx,   this->m_Vx,   YF<F>(this->StartTime(), t_from), m_regE,
        muS0, sigS10, sigS20, muV0, sigV0,  corrSV0,
        (DR != Grid2D<F>::NoDR) ? ir0 : nullptr
      );
    }
  }

  //=========================================================================//
  // "DoOneStep":                                                            //
  //=========================================================================//
  template<typename F>
  F* Grid2D_Theta<F>::DoOneStep
  (
    bool                        isFwd,
    typename Grid2D<F>::DRType  DR,
    int                         step_k,
    DateTime                    t_curr,
    F*                          p,
    DateTime                    t_next,
    TradingCalendar::CalReg     reg,
    BoundConds2D<F> const&      bcs
  )
  const
  {
    // "isFwd" requires "NoDR":
    assert(!isFwd || DR == Grid2D<F>::NoDR);

    // NB: iterations start from Rotator=true (similar to ImplS=true in ADI):
    bool Rotator = (step_k % 2 == 0);

    // <IsFwd, IsCN, Rotator>; bound conds are applied automatically:
    //
    if (isFwd)
      if (m_isCN)
        if (Rotator)
          DoTimeStepGen<true,  true,  true> (DR, t_curr, p, t_next, reg, bcs);
        else
          DoTimeStepGen<true,  true,  false>(DR, t_curr, p, t_next, reg, bcs);
      else
        if (Rotator)
          DoTimeStepGen<true,  false, true> (DR, t_curr, p, t_next, reg, bcs);
        else
          DoTimeStepGen<true,  false, false>(DR, t_curr, p, t_next, reg, bcs);
    else
      // !isFwd:
      if (m_isCN)
        if (Rotator)
          DoTimeStepGen<false, true,  true> (DR, t_curr, p, t_next, reg, bcs);
        else
          DoTimeStepGen<false, true,  false>(DR, t_curr, p, t_next, reg, bcs);
      else
        if (Rotator)
          DoTimeStepGen<false, false, true> (DR, t_curr, p, t_next, reg, bcs);
        else
          DoTimeStepGen<false, false, false>(DR, t_curr, p, t_next, reg, bcs);

    // After 2 sub-steps, the result is back in "p":
    return p;
  }
}
