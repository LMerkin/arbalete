// vim:ts=2:et
//===========================================================================//
//                          "TimeStepper_RKC1.hpp":                          //
//          1st-Order Runge-Kutta-Chebyshev Time Propagation Method          //
//===========================================================================//
#pragma once

#include "Common/CUDAEnv.hpp"
#include "Common/Chebyshev.h"
#include "Grids/TimeStepper_RKC1.h"
#include "Grids/TimeStepper_RKC1_Core.hpp"

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // Non-Default Ctor:                                                       //
  //=========================================================================//
  template<typename F>
  TimeStepper_RKC1<F>::TimeStepper_RKC1
  (
    int K, int s, shared_ptr<CUDAEnv> const& cudaEnv, F eps
  )
  : TimeStepper<F>(K, cudaEnv),
    m_s(s)
  {
    if (s < 2 || eps < F(0.0))
      throw invalid_argument("TimeStepper_RKC1 Ctor");

    // Pre-compute Chebyshev polynomials and their derivatives. NB: the "per-
    // missive" mode is used -- Chebyshev args may be slightly beyond +-1:
    //
    F w0 = F(1.0) + eps / F(s * s);

    Vector<F> ChebT (s + 1);
    Vector<F> ChebDT(s + 1);

    Chebyshev<F>::Ts (s, w0, &(ChebT [0]), true);
    Chebyshev<F>::DTs(s, w0, &(ChebDT[0]), true); // DTs[0] = 0

#   ifndef NDEBUG
    for (int i = 1; i <= s; ++i)
      assert(ChebT[i] != F(0.0) && ChebDT[i] != F(0.0));
#   endif

    F w1 = ChebT[s] / ChebDT[s];

    // Fill in "b":
    //
    Vector<F> b(s + 1);
    for (int i = 0; i <= s; ++i)
    {
      b[i] = F(1.0) / ChebT[i];
      assert(IsFinite(b[i]) && b[i] != F(0.0));
    }

    // Fill in "m_c":
    //
    m_c.resize(s + 1);
    m_c[0] = F(0.0);
    m_c[s] = F(1.0);
    for (int i = 1; i <= s-1; ++i)
      m_c[i] =  w1 * ChebDT[i] / ChebT[i];

    // Fill in "m_u":
    //
    m_u.resize(s);
    m_u[0] = w1 / w0;
    F cu = F(2.0) * w1;
    for (int i = 1; i <= s-1; ++i)
      m_u[i] = cu * b[i+1] / b[i];

    // Fill in "m_mu", "m_nu":
    //
    m_mu.resize(s - 1);
    m_nu.resize(s - 1);
    F  cm = F(2.0) * w0;
    for (int i = 0; i <= s-2; ++i)
    {
      m_mu[i]  = cm * b[i+2] / b[i+1];
      m_nu[i]  =    - b[i+2] / b[i];
    }

    // Allocate scratch matrices:
    //
    if (!this->UseCUDA())
    {
      // Host allocations:
      m_Y[0]    = new F[this->m_K];
      m_Y[1]    = new F[this->m_K];
      m_F       = new F[this->m_K];
    }
    else
    {
      // CUDA allocations:
      m_Y[0]    = this->m_cudaEnv->template CUDAAlloc<F>(this->m_K);
      m_Y[1]    = this->m_cudaEnv->template CUDAAlloc<F>(this->m_K);
      m_F       = this->m_cudaEnv->template CUDAAlloc<F>(this->m_K);
    }
    // All done!
  }

  //=========================================================================//
  // Dtor:                                                                   //
  //=========================================================================//
  template<typename F>
  TimeStepper_RKC1<F>::~TimeStepper_RKC1()
  {
    if (!this->UseCUDA())
    {
      delete[] m_Y[0];
      delete[] m_Y[1];
      delete[] m_F;
    }
    else
    {
      this->m_cudaEnv->Stop();  // Just in case...
      this->m_cudaEnv->template CUDAFree<F>(m_Y[0]);
      this->m_cudaEnv->template CUDAFree<F>(m_Y[1]);
      this->m_cudaEnv->template CUDAFree<F>(m_F);
    }
  }

  //=========================================================================//
  // "Propagate":                                                            //
  //=========================================================================//
  // NB: In the CUDA mode, "p" is assumed to be a CUDA memory ptr (as well as
  // all other data ptrs):
  //
  template<typename F>
  void TimeStepper_RKC1<F>::Propagate
  (
    DateTime                t_from,
    TradingCalendar::CalReg reg,
    F*                      p,
    DateTime                t_to,
    typename TimeStepper<F>::RHS const& rhs
  )
  const
  {
    assert(p != nullptr);
    F tau = YF<F>(t_from, t_to);  // NB: "tau" may be negative, but not 0:
    assert(tau != F(0.0));

    //-----------------------------------------------------------------------//
    // Initial "DpDt" from the RHS function:                                 //
    //-----------------------------------------------------------------------//
    rhs(t_from, reg, p, m_F);

    // Fill in "m_Y[0]"; NB: "SubStepRKC1_Core" is invoked in the "Init" mode:
    F cF = m_u[0] * tau;

    // NB: Do both static and dynamic checks for CUDA availability, to avoid
    // link errors:
    //
    if (!this->UseCUDA())
      SubStepRKC1_Core<F,true>
      (
        this->m_K,   nullptr,
        p,      m_F, nullptr,   nullptr,  m_Y[0],
        F(1.0), cF,  F(0.0), F(0.0)
      );
    else
    {
      this->m_cudaEnv->Start();

      RKC1_KernelInvocator<F,true>::Run
      (
        *(this->m_cudaEnv),   this->m_K, this->m_cudaSch,
        p,      m_F, nullptr, nullptr,   m_Y[0],
        F(1.0), cF,  F(0.0),  F(0.0)
      );

      this->m_cudaEnv->Stop();
    }

    //-----------------------------------------------------------------------//
    // Do all intermediate points:                                           //
    //-----------------------------------------------------------------------//
    for (int i = 1; i <= m_s-1; ++i)
    {
      F*       Yi   = (i == m_s-1) ? p : m_Y[i % 2];  // out
      F*       Yim1 = m_Y[(i-1) % 2];                 // in, BCs can be appld
      F const* Yim2 = (i == 1)     ? p : m_Y[i % 2];  // in; act. [(i-2)%2]

      // Compute "DpDt" at the intermediate time instant:
      //
      DateTime ti = t_from + TimeOfYF<F>(m_c[i] * tau);
      rhs(ti, reg, Yim1, m_F);

      // Make the sub-step (NB: NOT the "Init" mode):
      //
      F cP    = F(1.0) - m_mu[i-1] - m_nu[i-1];
      F cF    = m_u[i] * tau;
      F cYim2 = m_nu[i-1];
      F cYim1 = m_mu[i-1];
      
      // NB: Do both static and dynamic checks for CUDA availability, to avoid
      // link errors:
      //
      if (!this->UseCUDA())
        SubStepRKC1_Core<F,false>
        (
          this->m_K,  this->m_cudaSch,
          p, m_F, Yim2,  Yim1, Yi,
          cP,cF, cYim2, cYim1
        );
      else
      {
        this->m_cudaEnv->Start();

        RKC1_KernelInvocator<F,false>::Run
        (
          *(this->m_cudaEnv), this->m_K, this->m_cudaSch,
          p, m_F,  Yim2,  Yim1, Yi,
          cP, cF, cYim2, cYim1
        );
        this->m_cudaEnv->Stop();
      }
    }
  }
}
