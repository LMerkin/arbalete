// vim:ts=2:et
//===========================================================================//
//                         "Grid2D_BayesOptFilt.hpp":                        //
//        Special Grid for Computation of Likelihood Functions Using         //
//                         Bayesian Optimal Filtering                        //
//===========================================================================//
#pragma once

#include "Common/CUDAEnv.hpp"
#include "Common/Maths.hpp"
#include "Diffusions/Diffusion2D_DeMulti.h"
#include "Grids/Grid2D_BayesOptFilt.h"
#include "Grids/Grid2D_BayesOptFilt_Core.hpp"
#include <climits>

namespace
{
  using namespace Arbalete;
  using namespace std;

  //=========================================================================//
  // "MkBOFParams":                                                          //
  //=========================================================================//
  // Takes the generic "orig" params and projects them to a set suitable for
  // "Grid2D_BayesOptFilt" (so that the constructed parent class object will
  // NOT need to be altered explicitly by the "Grid2D_BayesOptFilt" ctor):
  //
  template<typename F>
  inline typename Grid2D<F>::Params MkBOFParams
    (typename Grid2D<F>::Params const& orig, F S0)
  {
    // Do a generic copy first. Typically, "orig" will contain "m_{Lo,Up}{S,V}"
    // which will make Grid boundaries fixed:
    //
    typename Grid2D<F>::Params res =   orig;

    // Method (not actually used since we already know which Grid we need):
    res.m_method    = "BayesOptFilt";

    // No stencil at all (and no explicit bound conds):
    res.m_sts       = 0;

    // "Sx" axis contains 1 element only, corresponding to "S0":
    res.m_m         = 1;
    res.m_S0OnGrid  = true;
    res.m_LoSFixed  = true;
    res.m_UpSFixed  = true;
    res.m_LoS       = S0;
    res.m_UpS       = S0;

    // "V0" does NOT need to be on Grid:
    res.m_V0OnGrid  = false;

    // If CUDA is used, "max_n" is bounded by the number of threads; and
    // make "n" a multiple of the ThreadBlockSize:
    int max_n = INT_MAX;
    int bs    = 1;
    if (res.m_cudaEnv.get() != nullptr)
    {
      max_n   = res.m_cudaEnv->NThreads();
      bs      = res.m_cudaEnv->ThreadBlockSize();
    }

    // "hV" is a suggested step wrt "V"; if given, construct a "FineGrade" func
    // which will actually adjust "n" (XXX: "hV" is not applied directly!):
    //
    F hV = res.m_hV;
    if (IsFinite(hV) && hV > F(0.0))
      // Use  "hV":
      res.m_fineV =
        shared_ptr<typename Grid2D<F>::Params::FineGrade>
        (
          new typename Grid2D<F>::Params::FineGrade
          (
            [hV, bs, max_n](F loV, F upV, int orig_n) -> int
            {
              int n1 = Max(orig_n, int(Round((upV - loV) / hV)));
              if (n1 % bs != 0)
                n1 = (n1 / bs + 1) * bs;
              n1 = Min(n1, max_n);
              assert(n1 > 0 && n1 <= max_n && n1 % bs == 0);
              return n1;
            }
          )
        );
    else
      // Still need an "m_fineV" func to account for CUDA "ThreadBlockSize":
      res.m_fineV =
        shared_ptr<typename Grid2D<F>::Params::FineGrade>
        (
          new typename Grid2D<F>::Params::FineGrade
          (
            [bs, max_n](F UNUSED_PARAM(loV), F UNUSED_PARAM(upV), int orig_n)
            -> int
            {
              int n1 = orig_n;
              if (n1 % bs != 0)
                n1 = (n1 / bs + 1) * bs;
              n1 = Min(n1, max_n);
              assert(n1 > 0 && n1 <= max_n && n1 % bs == 0);
              return n1;
            }
          )
        );

    // Time span for "Sx" does not matter as the axis is not actually made, as
    // well as "MinRelS":
    res.m_TS      = ZeroTime();
    res.m_minRelS = F(0.0);
    res.m_maxRelS = Inf<F>();

    // Time steps: keep them at 0 so that only "special_times" will have
    // effect:
    res.m_dtTH    = ZeroTime();
    res.m_dtON    = ZeroTime();
    res.m_dtWEH   = ZeroTime();

    return res;
  }
}

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // Non-Default Ctor:                                                       //
  //=========================================================================//
  template<typename F>
  Grid2D_BayesOptFilt<F>::Grid2D_BayesOptFilt
  (
    shared_ptr<Diffusion2D<F>> const& diff,
    typename Grid2D<F>::Params const& params
  )
  : Grid2D<F>(diff, MkBOFParams<F>(params, diff->GetS0()))
  {
    // For "S", we have only 2 values (the previous and the curr conditioning
    // points at each step). However,  we must still  maintain the invariant:
    // len(S) * len(V) = len(p),
    // so the prev "S" value is to be stored separately (initialised to 0) and
    // use  len(S) = m = 1. This is a "DEGENERATE" grid:
    //
    assert(this->m() == 1 && this->m_i0 == 0 &&
           this->m_Sx[0]  == this->GetS0());

    m_SYprev    = F(0.0);   // Will become S0 after the call-back is invoked
    m_opt[0]    = F(0.0);   // optV
    m_opt[1]    = F(0.0);   // maxP

    // [optV, maxP, denomP] in CUDA space:
    if (this->UseCUDA())
    {
      m_cudaOpt = this->m_cudaEnv->template CUDAAlloc<F>   (3);
      this->m_cudaEnv->template ToCUDA<F>(m_opt, m_cudaOpt, 3);
    }
    else
      m_cudaOpt = nullptr;

    // The "Sx" axis in the CUDA space is not used at all, so can de-allocate
    // it now:
    if (this->UseCUDA())
      this->m_cudaEnv->template CUDAFree<F>(this->m_cudaSx);

    // Allocate the alternating solution space (in Host or CUDA memory), of
    // size "n":
    int n = this->n();
    if (!this->UseCUDA())
    {
      m_pa      = new F[n];
      m_cudaSch = nullptr;
    }
    else
    {
      // NB: In Sched2D, the outer dim is "n" (all points "j" of "p"), but the
      // inner one is (n-1) (integration over "j0" starts from 1):
      //
      m_pa      = this->m_cudaEnv->template CUDAAlloc<F>(n);
      m_cudaSch = this->m_cudaEnv->MkSched2D(n, n-1);
    }
  }

  //=========================================================================//
  // Dtor:                                                                   //
  //=========================================================================//
  template<typename F>
  Grid2D_BayesOptFilt<F>::~Grid2D_BayesOptFilt()
  {
    if (!this->UseCUDA())
      delete[] m_pa;
    else
    {
      this->m_cudaEnv->template CUDAFree<F>(m_pa);
      this->m_cudaEnv->template CUDAFree<CUDAEnv::Sched2D>(m_cudaSch);
      this->m_cudaEnv->template CUDAFree<F>(m_cudaOpt);
    }
  }

  //=========================================================================//
  // "DoOneStep":                                                            //
  //=========================================================================//
  // Invocation sequence: RunFwd() -> TimeMarshal() -> DoOneStep():
  //
  template<typename F>
  F* Grid2D_BayesOptFilt<F>::DoOneStep
  (
    bool                        DEBUG_ONLY(isFwd),
    typename Grid2D<F>::DRType  DEBUG_ONLY(DR),
    int                         step_k,   // Making step from (k) to (k+1)
    DateTime                    t_curr,
    F*                          p,        // NB: len(p) = n
    DateTime                    t_next,
    TradingCalendar::CalReg     reg,
    BoundConds2D<F> const&      UNUSED_PARAM(bcs)
  )
  const
  {
    // Because "SolveFokkerPlanck" and "SolveFeynamnKac" are de-configured,
    // this method is only called from "RunFwd":
    assert(p != nullptr && isFwd && DR == Grid2D<F>::NoDR);

    // Diffusion:
    Diffusion2D<F> const* diff = this->m_diff.get();
    assert(diff != nullptr);

    assert(int(this->m_Sx.size()) == 1);
    int n   = int(this->m_Vx.size());
    assert(n >= 2);

    F const* Vx = &(this->m_Vx[0]);
    F const* Sx = &(this->m_Sx[0]);
    F   SY0 = m_SYprev;
    F   SY  = Sx[0];

    // Curr time in years and Time step:
    F   ty0 = YF<F>(this->StartTime(), t_curr);
    F   dty = YF<F>(t_curr,            t_next);

    assert(step_k >= 0);
    if (step_k == 0)
    {
      // INITIAL STEP: Iteration base for "p": Fill it in the Host memory using
      // the virtual "TransPDF" method on the Diffusion,   using (S0,V0) as the
      // initial point, and copy the result into the CUDA space (if reqd):
      //
      Vector<F> tmp;
      F*        f = nullptr;
      if (this->UseCUDA())
      {
        tmp.resize(n);
        f = &(tmp[0]);  // "f" is a ptr to a Host-based "tmp" buffer
      }
      else
        f = m_pa;       // NB: the target is "m_pa" (Host space), not "p"!

      F V0  = this->GetV0();

      // NB: "SY0" is from the Grid, and must be exactly the same as "GetS0()"
      // (eventually coming from the diff):
      //
      assert(SY0 == this->GetS0());

      // Compute Transition Density for all "V"s. NB: "f" is a Host-space ptr:
      //
      for (int j = 0; j < n; ++j)
        f[j] = diff->TransPDF(SY0, V0, ty0, SY, Vx[j], dty, reg);

      // Get [optV, maxP, denomP] into "m_opt" and normalise "f", to make the
      // latter ready for subsequent integration: Run Phase 2:
      //
      BayesOptFilt2D_Core2<F>(n, Vx, f, m_opt);

      // If CUDA is to be used, copy the Host-based "f" buffer into CUDA memory
      // (NB: once again, the target is CUDA-based "m_pa", not "p"!):
      //
      if (this->UseCUDA())
        this->m_cudaEnv->template ToCUDA<F>(f, m_pa, n);

      // All done in this (initial) case. Return the target ptr (Host or CUDA):
      return m_pa;
    }

    // OTHERWISE: GENERIC CASE:
    assert(step_k >= 1);

    // Alternate the solution spaces between the caller-provided "p" and the
    // internal "m_pa" (both are either Host- or CUDA-based):
    //
    F const* pSrc = (step_k % 2 == 0) ? p    : m_pa;
    F* pTrg       = (step_k % 2 == 0) ? m_pa : p;

    //-----------------------------------------------------------------------//
    // Diffusion De-Multiplexor:                                             //
    //-----------------------------------------------------------------------//
    // XXX: Currently, there is no generic copying mechanism for Diffusion objs
    // into the CUDA memory (problems occur with the VFT), so have to do it ex-
    // plicitly.
    // NB: Do both static and dynamic checks for CUDA availability, to avoid
    // link errors:
    //
    if (!this->UseCUDA())
    {
      // Phase 1:
      DIFFUSION2D_DEMULTIPLEXOR(F, diff, BayesOptFilt2D_Core1, EMPTY_MACRO_ARG,
        nullptr, SY0, ty0, SY, dty, reg, n, Vx, pSrc, pTrg)

      // Phase 2: Diffusion-Independent:
      BayesOptFilt2D_Core2<F>(n, Vx, pTrg, m_opt);
    }
    else
    {
      // Start the CUDA section:
      this->m_cudaEnv->Start();

      // Invoke kernels for both Phase 1 and Phase 2:
      DIFFUSION2D_DEMULTIPLEXOR(F, diff, BayesOptFilt2D_KernelsInvocator,
        ::Run, *(this->m_cudaEnv), m_cudaSch,
        SY0, ty0, SY, dty, reg, n, this->m_cudaVx, pSrc, pTrg, m_cudaOpt)

      // Stop the CUDA section and timer:
      this->m_cudaEnv->Stop();
    }

    // In the CUDA mode, need to copy [optV, maxP, denomP] back to Host mem:
    if (this->UseCUDA())
      this->m_cudaEnv->template ToHost<F>(m_cudaOpt, m_opt, 3);

    // All done. NB: The result is in "pTrg" (not always in "p"):
    return pTrg;
  }
}
