// vim:ts=2:et
//===========================================================================//
//                           "TimeStepper_RKC2.cu":                          //
//              CUDA Kernel Invocator for the RKC2 Time Stepper              //
//===========================================================================//
#include "Grids/TimeStepper_RKC2_Core.hpp"

namespace Arbalete
{
  //=========================================================================//
  // All Instances of the "RKC2_KernelInvocator":                            //
  //=========================================================================//
  template struct RKC2_KernelInvocator<double, false>;
  template struct RKC2_KernelInvocator<double, true>;

  template struct RKC2_KernelInvocator<float,  false>;
  template struct RKC2_KernelInvocator<float,  true>;
}
