// vim:ts=2:et
//===========================================================================//
//                        "Quadrature2D_Core.hpp":                           //
//                            2D Integration                                 //
//===========================================================================//
// NB: Using the simplest method -- a 2D generalisation of the Rectangle rule.
// The reason is: higher-order methods (e.g. Simpson) require sufficient diffe-
// rentiability of the integrand,  so they cannot be applied to a delta()-like
// function. The Trapezoidal 2D method is equivalent to Rectanle 2D apart from
// the treatment of boundary points, which are typically insignificant anyway:
//
#pragma once

#include "Common/CUDAEnv.hpp"
#include <cassert>

namespace Arbalete
{
  using namespace std;

# ifdef  __CUDACC__
  extern __shared__ char Q2D_ShRes[];
# endif

  //=========================================================================//
  // "Quadrature2D_Core": Simple 2D Integrator:                              //
  //=========================================================================//
  // NB: The factor (hS*hV) is omitted here, must be applied by the caller!
  // NB: "kern" must be passed by copy (for Host->CUDA calls):
  //
  // NB: "static" is essential here -- otherwise Host code could get called
  // instead of CUDA code!
  //
  template<typename F, typename Kernel>
  GLOBAL static void Quadrature2D_Core
  (
    CUDAEnv::Sched1D const*  CUDACC_OR_DEBUG(sched), Kernel kern,
    int m, int n,  F const*  f, 
    int CUDACC_OR_DEBUG(KR), F* res
  )
  {
    // "params" may be NULL, in which case "np" is set to 1 for correct loop
    // counting below:
    int np          = kern.GetNParams();
    F const* params = kern.GetParams ();
    assert(params == nullptr || np >= 1);
    np = Max<int>(np, 1);

    // Do NOT start integration from i=0 and/or j=0, as "kern" may have singu-
    // larities there (e.g., for each elementary area, the integrand is always
    // evaluated in its upper-right point).
    // The space of indices is multiplied by the size of "params" (if any):
    //
    assert(f != nullptr && res != nullptr && m >= 2 && n >= 2);

#   ifdef __CUDACC__
    // CUDA-based computation: "f" and "res" point to CUDA memory, "res" is an
    // accumulator of per-param, per-block results:
    assert(KR == np * gridDim.x && sched != nullptr);

    // In the CUDA mode, "res" is an array of results (for each block); inside
    // the block, use the "resB" array (for each thread):
    //
    F* resB = (F*)(Q2D_ShRes);

    // The order number of this thread (across all blocks):
    int Tn    = blockDim.x * blockIdx.x + threadIdx.x;
    int kFrom = sched[Tn].m_from;
    int kTo   = sched[Tn].m_to;

#   ifndef NDEBUG
    int K1    = (m-1) * (n-1);
    assert(0 <= kFrom && kTo <= K1-1);
#   endif
    // NB: However, it is still possible that kFrom > kTo, so the curr thread
    // will have nothing to do in the "k" loop.  Still, DO NOT exit it now as
    // it may be required for accumulation of results!

#   else
    // Host-based computation: XXX: here we do not use OpenMP parallelisation
    // because the loop wrt "k" is an inner one, and quite simplistic  --  so
    // the overhead of thread creation may outweigh the advantages:
    //
    assert(KR == np && sched == nullptr);
    int K1    = (m-1) * (n-1);
    int kFrom = 0;
    int kTo   = K1-1;
#   endif

    // For all params (if any):
    //
    for (int xi = 0; xi < np; ++xi)
    {
      // Get the curr value of the param "x" (use a placeholder if there are
      // no params):
      F x = (params != nullptr) ? params[xi] : F(0.0);

      // Integrate over all selected points:
      //
      F acc = F(0.0);
      for (int k = kFrom; k <= kTo; ++k)
      {
        int i    = k % (m-1) + 1;
        int j    = k / (m-1) + 1;
        assert(i < m && j < n);
        int off  = m *  j + i;      // Offset in the original "f"

        // NB: multiplication by hS * hV is to be performed by the caller!
        acc  += kern(i, j, x) * f[off];
      }
#     ifndef __CUDACC__
      // Host mode: store the full result for this "xi":
      res[xi] = acc;
#     else
      // CUDA mode: store the per-thread result for the curr "xi":
      resB[threadIdx.x] = acc;

      // Now wait for all threads to finish this "xi" and compute the per-block
      // sum:
      __syncthreads();

      if (threadIdx.x == 0)
      {
        // For tid=0, "acc" already contains its proper value; add the values
        // accumulated by all other threads:
        for (int tid = 1; tid < blockDim.x; ++tid)
          acc += resB[tid];

        // Store the global partial sum for this "xi" and the curr block:
        res[xi * gridDim.x + blockIdx.x] = acc;
      }
#     endif
    } // End of "xi" loop
  }

  //=========================================================================//
  // "Quadrature2D_KernelInvocator":                                         //
  //=========================================================================//
  // NB: "Run" is Host-invoked, so it's OK to pass "kern" to it by copy:
  //
  template<typename F, typename Kernel>
  struct Quadrature2D_KernelInvocator
  {
    static void Run
    (
      CUDAEnv const& cudaEnv, CUDAEnv::Sched1D const* sched,
      Kernel  const& kern,
      int m,  int n, F const* f, int KR, F* res
    )
#   ifdef __CUDACC__
    {
      assert(sched != nullptr);

      // Set the shared memory size:
      int ShMemSz = int(cudaEnv.ThreadBlockSize() * sizeof(F));

      // Check for excessively-large "ShMemSz values -- this would result in
      // major inefficiencies:
      if (ShMemSz * cudaEnv.ThreadBlocksPerSM() > cudaEnv.ShMemPerSM())
        throw invalid_argument("Quadrature2D Kernel: ShMemSz too large");

      // Set the CUDA device:
      CUDAEnv::Lock();
      cudaEnv.SetCUDADevice();

      // Invoke the kernel:
      Quadrature2D_Core<F, Kernel>
        <<<cudaEnv.NThreadBlocks(), cudaEnv.ThreadBlockSize(), ShMemSz,
           cudaEnv.Stream()
        >>>
        (sched, kern, m, n, f, KR, res);

      CUDAEnv::Unlock();
      CUDA_CHECK_STATUS("Quadrature2D Kernel invocation failed")
    }
#   else
    ; // Spec only
#   endif
  };
}
