//vim:ts=2:et
//===========================================================================//
//                         "Normalise2D_Core.hpp":                           //
//                          Normaliser of 2D PDFs                            //
//===========================================================================//
#pragma once

#include "Common/CUDAEnv.hpp"

namespace Arbalete
{
  using namespace std;

  //=========================================================================//
  // "Normalise2D_Core":                                                     //
  //=========================================================================//
  // NB: "static" is essential here -- otherwise Host code could get called in-
  // stead of CUDA code!
  //
  template<typename F>
  GLOBAL static void Normalise2D_Core
    (CUDAEnv::Sched1D const* CUDACC_OR_DEBUG(sched), int K, F pMass, F* p)
  {
    assert(K >= 4 && pMass > F(0.0) && p != nullptr);

#   ifdef __CUDACC__
    assert(sched != nullptr);

    // The order number of this thread (across all blocks):
    int Tn    = blockDim.x * blockIdx.x + threadIdx.x;
    int kFrom = sched[Tn].m_from;
    int kTo   = sched[Tn].m_to;
    if (kFrom > kTo)
      // This thread has nothing to do -- exit:
      return;
    assert(0 <= kFrom && kFrom <= kTo && kTo <= K-1);

#   else
    // Host-based computation. XXX: OpenMP is not available under Mac OS X yet:
    assert(sched == nullptr);
    int kFrom = 0;
    int kTo   = K-1;
#   ifndef __clang__
#   pragma omp parallel for
#   endif
#   endif  // __CUDACC__
    for (int k = kFrom; k <= kTo; ++k)
      p[k] /= pMass;
    // All done!
  }

  //=========================================================================//
  // "Normalise2D_KernelInvocator":                                          //
  //=========================================================================//
  template<typename F>
  struct Normalise2D_KernelInvocator
  {
    static void Run
    (
      CUDAEnv const& cudaEnv, CUDAEnv::Sched1D const* sched,
      int K, F pMass, F* p
    )
#   ifdef __CUDACC__
    {
      // Set the CUDA device:
      CUDAEnv::Lock();
      cudaEnv.SetCUDADevice();

      // Invoke the kernel:
      Normalise2D_Core<F>
        <<<cudaEnv.NThreadBlocks(), cudaEnv.ThreadBlockSize(), 0,
           cudaEnv.Stream()
        >>>
        (sched, K, pMass, p);

      CUDAEnv::Unlock();
      CUDA_CHECK_STATUS("Normalise2D Kernel invocation failed")
    }
#   else
    ; // Spec only
#   endif
  };
}
