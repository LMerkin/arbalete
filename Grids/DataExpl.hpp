// vim:ts=2:et
//===========================================================================//
//                               "DataExpl.hpp":                             //
//                       Data Structures for "Grid2D_Expl"                   //
//===========================================================================//
#pragma once

#include "Common/Maths.hpp"
#include "Common/CUDAEnv.hpp"
#include "Grids/DataExpl.h"
#include "Grids/Grid2D_Expl.h"
#include <cassert>

namespace Arbalete
{
  using namespace std;

  //-------------------------------------------------------------------------//
  // Non-Default Ctor:                                                       //
  //-------------------------------------------------------------------------//
  // NB: "cudaEnabled" has no effect unless "isCUDA" is set:
  //
  template<typename F, int M>
  DataExpl<F,M>::DataExpl(Grid2D_Expl<F,M> const& grid, bool forCUDA)
  {
    static_assert(M == 1 || M == 2, "DataExpl: Invalid Stencil Size");

    if  (forCUDA && !grid.UseCUDA())
    {
      // Nothing to do, no allocations, all ptrs are NULL:
      m_K    = 0;
      m_data = m_muS = m_sigS1 = m_sigS2 = m_muV = m_sigV = m_rho = m_ir =
      m_LECs = m_end  = nullptr;

      for (int b = 0; b < M; ++b)
        m_bcps.m_LoS[b] = m_bcps.m_UpS[b] = m_bcps.m_LoV[b] =
        m_bcps.m_UpV[b] = nullptr;
      return;
    }

    // GENERIC CASE:
    // Calculate the total length of "m_data" to be allocated:
    //
    int  m  = int(grid.GetSx().size());
    int  n  = int(grid.GetVx().size());
    bool isRateModel = grid.GetDiffusion().IsIRModel();

    m_bcps.m_m = m;
    m_bcps.m_n = n;
    m_K   =
      isRateModel
      ? ((3 + 2 * M) *(m + n) + 6)
      : ((2 + 2 * M) * m + (3 + 2 * M) * n + 7);

    // Allocate CUDA or Host memory for "m_data":
    //
    m_data    =
      forCUDA
      ? grid.GetCUDAEnv().template CUDAAlloc<F>(m_K)
      : new F[m_K];

    // Set up ptr aliases:
    // Coeffs:
    m_muS     = m_data  + 0;
    m_sigS1   = m_muS   + m;
    m_sigS2   = m_sigS1 + m;
    m_muV     = m_sigS2 + n;
    m_sigV    = m_muV   + n;
    m_rho     = m_sigV  + n;
    m_ir      = m_rho   + 1;

    // LECs:
    m_LECs    = m_ir    + (isRateModel ? m : 1);

    // Bound Conds:
    F* nextBCP= m_LECs  + 5;
    for (int b = 0; b < M; ++b)
    {
      m_bcps.m_LoS[b] = nextBCP;
      m_bcps.m_UpS[b] = m_bcps.m_LoS[b] + n;
      m_bcps.m_LoV[b] = m_bcps.m_UpS[b] + n;
      m_bcps.m_UpV[b] = m_bcps.m_LoV[b] + m;
      nextBCP         = m_bcps.m_UpV[b] + m;
    }
    m_end     = nextBCP;
    assert(m_end - m_data == m_K);
  }

  //-------------------------------------------------------------------------//
  // Explicit Dtor:                                                          //
  //-------------------------------------------------------------------------//
  // Same args as for the Ctor -- "DataExpl" cannot destroy itself using its
  // own info only:
  //
  template<typename F, int M>
  void DataExpl<F,M>::Destroy(Grid2D_Expl<F,M> const& grid, bool forCUDA)
  {
    if (m_data != nullptr)
    {
      if (forCUDA)
      {
        CUDAEnv const& cudaEnv = grid.GetCUDAEnv();
        cudaEnv.Stop();   // Just in case...
        cudaEnv.template CUDAFree<F>(m_data);
      }
      else
        delete[] m_data;
    }
    // Just in case, re-set all ptrs to NULL:
    m_K    = 0;
    m_data = m_muS = m_sigS1 = m_sigS2 = m_muV = m_sigV = m_rho = m_ir =
    m_LECs = m_end = nullptr;

    for (int b = 0; b < M; ++b)
      m_bcps.m_LoS[b] = m_bcps.m_UpS[b] = m_bcps.m_LoV[b] =
      m_bcps.m_UpV[b] = nullptr;
  }
}
