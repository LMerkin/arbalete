// vim:ts=2:et
//===========================================================================//
//                           "Grid2D_BayesOptFilt.cpp":                      //
//             Explicit Instances of the "Grid2D_BayesOptFilt" Class         //
//===========================================================================//
#include "Grid2D_BayesOptFilt.hpp"

namespace Arbalete
{
  template class Grid2D_BayesOptFilt<double>;
  template class Grid2D_BayesOptFilt<float>;
}
