// vim:ts=2:et
//===========================================================================//
//                             "Grid2D_Theta.cpp":                           //
//                Explicit Instances of the "Grid2D_Theta" Class             //
//===========================================================================//
#include "Grid2D_Theta.hpp"

namespace Arbalete
{
  // NB: Here the stencil size is 3 only:
  template class Grid2D_Theta<double>;
  template class Grid2D_Theta<float>;
}
