// vim:ts=2:et
//===========================================================================//
//                              "TimeStepper.h":                             //
//  Time Stepper for Explicit ODE Integration (and PDE with Method of Lines) //
//===========================================================================//
#pragma once

#include "Common/VecMatTypes.h"
#include "Common/TradingCalendar.h"
#include "Common/CUDAEnv.h"
#include <functional>
#include <cassert>

namespace Arbalete
{
  //=========================================================================//
  // "TimeStepper":                                                          //
  //=========================================================================//
  template<typename F>
  class TimeStepper
  {
  protected:
    //-----------------------------------------------------------------------//
    // Common Configs:                                                       //
    //-----------------------------------------------------------------------//
    int                       m_K;            // The order of the ODE system
    std::shared_ptr<CUDAEnv>  m_cudaEnv;
    CUDAEnv::Sched1D*         m_cudaSch;      // Schedule of CUDA threads

    // Is CUDA to be used?
    bool UseCUDA() const      { return m_cudaEnv.get() != nullptr; }

  private:
    TimeStepper();            // No default ctor

  public:
    //-----------------------------------------------------------------------//
    // "RHS":                                                                //
    //-----------------------------------------------------------------------//
    // Type of the RHS function: fills in the array of DfDt derivatives;  the
    // size of this array is assumed to be known from the context. NB: "f" is
    // NOT "const" in general -- e.g., Bound Conds may be applied to it:
    //
    typedef std::function
      <void(DateTime t, TradingCalendar::CalReg reg, F* f, F* DtDt)>
      RHS;

    //-----------------------------------------------------------------------//
    // Non-Default Ctor, Virtual Dtor and Access:                            //
    //-----------------------------------------------------------------------//
    TimeStepper(int K, std::shared_ptr<CUDAEnv> const& cudaEnv)
    : m_K(K),
      m_cudaEnv(cudaEnv),
      m_cudaSch(UseCUDA() ? cudaEnv->MkSched1D(K) : nullptr)
    {
      assert(m_K >= 1);
    }

    virtual ~TimeStepper()
    {
      if (UseCUDA())
      {
        assert(m_cudaSch != nullptr);
        m_cudaEnv->template CUDAFree<CUDAEnv::Sched1D>(m_cudaSch);
      }
    }

    int GetK() const  { return m_K; }

    //-----------------------------------------------------------------------//
    // "Propagate":                                                          //
    //-----------------------------------------------------------------------//
    // The actual time stepping method (purely virtual, to be implemented by
    // the sub-classes).
    // At invocation, "f" corresponds to time "t_from"; on return, it is re-
    // placed by the values for "t_to"; "rhs" is invoked as needed to compute
    // the time derivatives of "f":
    //
    virtual void Propagate
    (
      DateTime                t_from,
      TradingCalendar::CalReg reg,
      F*                      f,
      DateTime                t_to,
      RHS const&              rhs
    )
    const = 0;
  };
}
