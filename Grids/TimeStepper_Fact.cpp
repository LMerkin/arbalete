// vim:ts=2:et
//===========================================================================//
//                           "TimeStepper_Fact.cpp":                         //
//                Pre-Defined Instances of "TimeStepper_Fact"                //
//===========================================================================//
#include "Grids/TimeStepper_Fact.hpp"

namespace Arbalete
{
  using namespace std;

  //-------------------------------------------------------------------------//
  // Prevent aitomatic instantiation of the Time Steppers themselves:        //
  //-------------------------------------------------------------------------//
  // (They are explicitly instantiated in their respective .cpp files):
  //
  extern template class TimeStepper_RKC1<double>;
  extern template class TimeStepper_RKC1<float>;

  extern template class TimeStepper_RKC2<double>;
  extern template class TimeStepper_RKC2<float>;

  extern template class TimeStepper_RKF5<double>;
  extern template class TimeStepper_RKF5<float>;

  //-------------------------------------------------------------------------//
  // Instantiate the Factory:                                                //
  //-------------------------------------------------------------------------//
  template TimeStepper<double>* MkTimeStepper<double>
  (
    string const& stepperName,
    int K,
    shared_ptr<CUDAEnv> const& cudaEnv
  );

  template TimeStepper<float>*  MkTimeStepper<float>
  (
    string const& stepperName,
    int K,
    shared_ptr<CUDAEnv> const& cudaEnv
  );
}
